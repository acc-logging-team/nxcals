package cern.nxcals.performancetests.extraction;

import cern.rbac.client.authentication.AuthenticationClient;
import cern.rbac.client.authentication.AuthenticationException;
import cern.rbac.common.RbaToken;
import cern.rbac.util.holder.ClientTierTokenHolder;
import lombok.experimental.UtilityClass;

@UtilityClass
public class BenchmarksUtils {
    public static void loginToRBAC(String username, String password) {
        try {
            AuthenticationClient authenticationClient = AuthenticationClient.create();
            RbaToken token = authenticationClient.loginExplicit(username, password);
            ClientTierTokenHolder.setRbaToken(token);
        } catch (AuthenticationException e) {
            throw new IllegalArgumentException("Cannot login", e);
        }
    }
}
