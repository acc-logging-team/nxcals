package cern.nxcals.performancetests.extraction;

import cern.nxcals.api.config.SparkProperties;
import cern.nxcals.api.custom.config.SparkSessionFlavor;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.utils.SparkUtils;
import cern.nxcals.performancetests.common.AbstractPerformanceTest;
import cern.nxcals.performancetests.common.BenchmarkThreshold;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.infra.Blackhole;

import java.util.concurrent.TimeUnit;

import static cern.nxcals.performancetests.extraction.BenchmarksUtils.loginToRBAC;

@Warmup(iterations = 1, time = 100, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 5, time = 100, timeUnit = TimeUnit.MILLISECONDS)
public class SparkSessionStartTest extends AbstractPerformanceTest {
    @Setup
    @Override
    public void setup() {
        loginToRBAC(username, password);
    }

    private static final String SPARK_APP_NAME = "PERF-TEST";
    private static final String PERF_TEST_ENV_NAME = "perf_test";

    private static final SparkProperties localConfig = SparkProperties.defaults(SPARK_APP_NAME);
    private static final SparkProperties yarnSmallConfig = SparkProperties.remoteSessionProperties(SPARK_APP_NAME,
            SparkSessionFlavor.SMALL, PERF_TEST_ENV_NAME);
    private static final SparkProperties yarnMediumConfig = SparkProperties.remoteSessionProperties(SPARK_APP_NAME,
            SparkSessionFlavor.MEDIUM, PERF_TEST_ENV_NAME);
    private static final SparkProperties yarnLargeConfig = SparkProperties.remoteSessionProperties(SPARK_APP_NAME,
            SparkSessionFlavor.LARGE, PERF_TEST_ENV_NAME);

    private static final TimeWindow timeWindow = TimeWindow.between(
            TIMESTAMP,
            TIMESTAMP.plusSeconds(3600)
    );

    private void extractEntityData(SparkSession session, Blackhole blackhole) {
        Dataset<Row> dataset = DataQuery.builder(session).entities().system(MOCK_SYSTEM)
                .keyValuesEq(KEY_VALUES).timeWindow(timeWindow).build();
        blackhole.consume(dataset.collect());
    }

    @BenchmarkThreshold(threshold = 10)
    @Benchmark
    public void startLocalSparkSession(Blackhole blackhole) {
        sparkSession = SparkUtils.createSparkSession(localConfig);
        extractEntityData(sparkSession, blackhole);
        sparkSession.close();
    }

    @BenchmarkThreshold(threshold = 50)
    @Benchmark
    public void startYarnSmallSparkSession(Blackhole blackhole) {
        sparkSession = SparkUtils.createSparkSession(yarnSmallConfig);
        extractEntityData(sparkSession, blackhole);
        sparkSession.close();
    }

    @BenchmarkThreshold(threshold = 50)
    @Benchmark
    public void startYarnMediumSparkSession(Blackhole blackhole) {
        sparkSession = SparkUtils.createSparkSession(yarnMediumConfig);
        extractEntityData(sparkSession, blackhole);
        sparkSession.close();
    }

    @BenchmarkThreshold(threshold = 50)
    @Benchmark
    public void startYarnLargeSparkSession(Blackhole blackhole) {
        sparkSession = SparkUtils.createSparkSession(yarnLargeConfig);
        extractEntityData(sparkSession, blackhole);
        sparkSession.close();
    }
}
