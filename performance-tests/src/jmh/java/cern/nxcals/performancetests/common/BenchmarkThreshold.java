package cern.nxcals.performancetests.common;

import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Repeatable(value = BenchmarkThresholds.class)
public @interface BenchmarkThreshold {
    String[] params() default {};

    String[] paramsValues() default {};

    double threshold();
}
