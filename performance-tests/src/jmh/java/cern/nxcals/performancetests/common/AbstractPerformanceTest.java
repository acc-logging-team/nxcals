package cern.nxcals.performancetests.common;

import cern.nxcals.api.config.SparkProperties;
import cern.nxcals.api.utils.SparkUtils;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.common.utils.ConfigHolder;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.Test;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.infra.BenchmarkParams;
import org.openjdk.jmh.results.RunResult;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.lang.reflect.Method;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static cern.nxcals.performancetests.extraction.BenchmarksUtils.loginToRBAC;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Warmup(iterations = 5, time = 100, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 10, time = 100, timeUnit = TimeUnit.MILLISECONDS)
//@Fork(value = 0) // for debugging
@Fork(value = 1)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.SECONDS)
@State(org.openjdk.jmh.annotations.Scope.Benchmark)
@Slf4j
public abstract class AbstractPerformanceTest {
    static {
        //        System.setProperty("service.url",
        //                "https://cs-513-nxcals1.cern.ch:19093,https://cs-513-nxcals2.cern.ch:19093,https://cs-513-nxcals3.cern.ch:19093,https://cs-513-nxcals4.cern.ch:19093"); //Do not use localhost here.
        //        System.setProperty("rbac.env", "TEST");
        System.setProperty("kerberos.auth.disable", "true");
    }

    protected static final String MOCK_SYSTEM = "MOCK-SYSTEM";
    protected static final Map<String, Object> KEY_VALUES = Map.of("device", "NXCALS_MONITORING_DEV1");
    protected static final Map<String, Object> KEY_VALUES_PATTERN = Map.of("device", "NXCALS_MONITORING_DEV%");
    protected static final Instant TIMESTAMP = TimeUtils.getInstantFromString("2023-02-04 00:00:00.000");
    protected String username = ConfigHolder.getProperty("username", "");

    protected String password = ConfigHolder.getProperty("password", "");
    protected SparkProperties sparkProperties = SparkProperties.defaults("PERF-TEST");
    protected SparkSession sparkSession;

    @Setup
    public void setup() {
        loginToRBAC(username, password);
        sparkSession = SparkUtils.createSparkSession(sparkProperties);
    }

    @Test
    void verify() throws RunnerException {
        Collection<RunResult> results = runBenchmarks();
        assertFalse(results.isEmpty());
        for (RunResult runResult : results) {
            verifyResult(runResult);
        }
    }

    private Collection<RunResult> runBenchmarks() throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(this.getClass().getSimpleName())
                .build();
        return new Runner(opt).run();
    }

    private void verifyResult(RunResult result) {
        Method method = findMethod(result.getPrimaryResult().getLabel()).orElseThrow(
                () -> new RuntimeException("Cannot match benchmark result with any benchmark methods"));
        double threshold = findThreshold(method, result.getParams()).orElseThrow(
                () -> new RuntimeException("Cannot match threshold for benchmark result"));
        double score = result.getPrimaryResult().getScore();
        assertTrue(score < threshold, result.getPrimaryResult().getLabel() + ": " + score + " < " + threshold);
    }

    private Optional<Method> findMethod(String name) {
        Method[] methods = this.getClass().getMethods();
        return Arrays.stream(methods).filter(method -> method.getName().equals(name)).findFirst();
    }

    private Optional<Double> findThreshold(Method method, BenchmarkParams parameters) {
        BenchmarkThreshold[] annotations = method.getAnnotationsByType(BenchmarkThreshold.class);
        Optional<Double> maybeDefaultThreshold = findDefaultThreshold(annotations);
        Optional<Double> maybeMatchingThreshold = findMatchingAnnotation(annotations, parameters);
        return maybeMatchingThreshold.or(() -> maybeDefaultThreshold);
    }

    private Optional<Double> findDefaultThreshold(BenchmarkThreshold[] annotations) {
        Optional<BenchmarkThreshold> maybeDefaultAnnotation = Arrays.stream(annotations)
                .filter(annotation -> annotation.params().length == 0)
                .findFirst(); // annotation without params is treated as default one
        return maybeDefaultAnnotation.map(BenchmarkThreshold::threshold);
    }

    private Optional<Double> findMatchingAnnotation(BenchmarkThreshold[] annotations, BenchmarkParams benchmarkParams) {
        Map<String, String> benchmarkParameters = getParametersAsMap(benchmarkParams);
        Optional<BenchmarkThreshold> matchingBenchmarkThreshold = Arrays.stream(annotations)
                .filter(annotation -> checkIfParametersMatches(benchmarkParameters, annotation)).findFirst();
        return matchingBenchmarkThreshold.map(BenchmarkThreshold::threshold);
    }

    private static boolean checkIfParametersMatches(Map<String, String> benchmarkParameters,
            BenchmarkThreshold annotation) {
        Map<String, String> annotationParams = zipAsMap(annotation.params(), annotation.paramsValues());
        return annotationParams.entrySet().equals(benchmarkParameters.entrySet());
    }

    private static Map<String, String> getParametersAsMap(BenchmarkParams params) {
        HashMap<String, String> map = new HashMap<>();
        for (String key : params.getParamsKeys()) {
            map.put(key, params.getParam(key));
        }
        return map;
    }

    private static <K, V> Map<K, V> zipAsMap(K[] keys, V[] values) {
        if (keys.length != values.length) {
            throw new RuntimeException("Cannot create map from arrays, then don't match");
        }
        HashMap<K, V> map = new HashMap<>();
        for (int i = 0; i < keys.length; i++) {
            map.put(keys[i], values[i]);
        }
        return map;
    }
}
