package cern.nxcals.performancetests.metadata;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.extraction.metadata.EntityService;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.extraction.metadata.queries.Entities;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import cern.nxcals.api.metadata.queries.EntityQueryWithOptions;
import cern.nxcals.api.metadata.queries.VariableQueryWithOptions;
import cern.nxcals.common.utils.Lazy;
import cern.nxcals.performancetests.common.AbstractPerformanceTest;
import cern.nxcals.performancetests.common.BenchmarkThreshold;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.TearDown;
import org.openjdk.jmh.infra.Blackhole;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static cern.nxcals.performancetests.metadata.VariableCreateAndDeleteTest.createVariable;

public class MetadataReadoutTest extends AbstractPerformanceTest {
    private static final String CMW = "CMW";
    private static final String WINCCOA = "WINCCOA-PERF";
    private static final int numberOfVariablesToRegister = 10000;
    private final VariableService variableService = ServiceClientFactory.createVariableService();
    Set<Variable> createdVariables;
    private final Lazy<SystemSpec> mockSystemSpec = new Lazy<>(
            () -> ServiceClientFactory.createSystemSpecService()
                    .findByName(MOCK_SYSTEM)
                    .orElseThrow()
    );
    private final Lazy<SystemSpec> cmwSystemSpec = new Lazy<>(
            () -> ServiceClientFactory.createSystemSpecService()
                    .findByName(CMW)
                    .orElseThrow()
    );
    private final EntityService entityService = ServiceClientFactory.createEntityService();

    @Setup(Level.Trial)
    public void setup() {
        super.setup();
        Condition<Entities> entityQuery = Entities.suchThat().systemName().eq(WINCCOA);
        Set<Entity> winccoaEntities = entityService.findAll(entityQuery).stream().limit(numberOfVariablesToRegister)
                .collect(Collectors.toSet());
        if (winccoaEntities.size() != numberOfVariablesToRegister) {
            throw new RuntimeException("Not enough entities in WINCCOA system to run benchmark");
        }
        Set<Variable> variablesToRegister = winccoaEntities.stream()
                .map(entity -> createVariable(entity, getClass().getName()))
                .collect(Collectors.toSet());
        createdVariables = variableService.createAll(variablesToRegister);
    }

    @BenchmarkThreshold(threshold = 0.1)
    @Benchmark
    public void queryMonitoringEntities(Blackhole blackhole) {
        Condition<Entities> entityQuery = Entities.suchThat().systemName().eq(MOCK_SYSTEM).and().keyValues()
                .like(mockSystemSpec.get(), Map.of("device", "NXCALS_MONITORING_DEV%"));
        Set<Entity> entities = entityService.findAll(entityQuery);
        blackhole.consume(entities);
    }

    @BenchmarkThreshold(threshold = 0.1)
    @Benchmark
    public void queryCMWEntities(Blackhole blackhole) {
        Condition<Entities> entityQuery = Entities.suchThat().systemName().eq(CMW).and().keyValues()
                .like(cmwSystemSpec.get(), Map.of("device", "perf_test_dev%", "property", "perf_test_prop%"));
        Set<Entity> entities = entityService.findAll(entityQuery);
        blackhole.consume(entities);
    }

    @BenchmarkThreshold(threshold = 20)
    @Benchmark
    public void queryAllWinccoaEntities(Blackhole blackhole) {
        Condition<Entities> entityQuery = Entities.suchThat().systemName().eq(WINCCOA);
        Set<Entity> entities = entityService.findAll(entityQuery);
        blackhole.consume(entities);
    }

    @BenchmarkThreshold(threshold = 1)
    @Benchmark
    public void query100WinccoaEntities(Blackhole blackhole) {
        EntityQueryWithOptions entityQuery = newEntities().systemName().eq(WINCCOA).withOptions().limit(100);
        List<Entity> entities = entityService.findAll(entityQuery);
        blackhole.consume(entities);
    }

    @BenchmarkThreshold(threshold = 10)
    @Benchmark
    public void queryAllVariables(Blackhole blackhole) {
        Condition<Variables> variableQuery = Variables.suchThat().systemName().eq(WINCCOA);
        Set<Variable> variables = variableService.findAll(variableQuery);
        blackhole.consume(variables);
    }

    @BenchmarkThreshold(threshold = 5)
    @Benchmark
    public void queryAllVariablesWithoutConfig(Blackhole blackhole) {
        VariableQueryWithOptions variableQuery = newVariables().systemName().eq(WINCCOA).withOptions()
                .noConfigs();
        List<Variable> variables = variableService.findAll(variableQuery);
        blackhole.consume(variables);
    }

    @BenchmarkThreshold(threshold = 1)
    @Benchmark
    public void query100VariablesWithoutConfig(Blackhole blackhole) {
        VariableQueryWithOptions variableQuery = newVariables().systemName().eq(WINCCOA).withOptions()
                .noConfigs()
                .limit(100);
        List<Variable> variables = variableService.findAll(variableQuery);
        blackhole.consume(variables);
    }

    @TearDown(Level.Trial)
    public void cleanUp() {
        variableService.deleteAll(createdVariables.stream().map(Variable::getId).collect(Collectors.toSet()));
    }

    private static cern.nxcals.api.metadata.queries.Variables newVariables() {
        return cern.nxcals.api.metadata.queries.Variables.suchThat();
    }

    private static cern.nxcals.api.metadata.queries.Entities newEntities() {
        return cern.nxcals.api.metadata.queries.Entities.suchThat();
    }

}
