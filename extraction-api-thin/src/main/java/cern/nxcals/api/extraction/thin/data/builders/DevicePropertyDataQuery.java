/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.thin.data.builders;

import cern.nxcals.api.extraction.data.builders.fluent.DeviceStage;
import cern.nxcals.api.extraction.data.builders.fluent.EntityAliasStage;
import cern.nxcals.api.extraction.data.builders.fluent.QueryData;
import cern.nxcals.api.extraction.data.builders.fluent.StageSequenceDeviceProperty;
import cern.nxcals.api.extraction.data.builders.fluent.SystemStage;
import cern.nxcals.api.extraction.data.builders.fluent.TimeStartStage;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class DevicePropertyDataQuery {
    /**
     * @return device-property query builder
     */
    // @deprecated in favour of {@link ParameterDataQuery#builder()}
    //    @Deprecated
    public static SystemStage<TimeStartStage<EntityAliasStage<DeviceStage<String>, String>, String>, String> builder() {
        return StageSequenceDeviceProperty.<String>sequence().apply(new QueryData<>(new ScriptProducer()));
    }
}