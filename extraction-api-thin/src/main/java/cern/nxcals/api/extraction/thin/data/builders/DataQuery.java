/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.thin.data.builders;

import cern.nxcals.api.extraction.data.builders.fluent.QueryData;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class DataQuery extends LegacyBuildersDataQuery {
    public static DataQuery builder() {
        return new DataQuery();
    }

    protected QueryData<String> queryData() {
        return new QueryData<>(new ScriptProducer());
    }

}


