package cern.nxcals.api.extraction.thin;

import cern.nxcals.api.extraction.data.Avro;
import cern.nxcals.api.extraction.thin.data.builders.DataQuery;
import cern.rbac.client.authentication.AuthenticationClient;
import cern.rbac.client.authentication.AuthenticationException;
import cern.rbac.common.RbaToken;
import cern.rbac.util.holder.ClientTierTokenHolder;
import io.grpc.ManagedChannel;
import io.grpc.netty.shaded.io.grpc.netty.NettyChannelBuilder;
import lombok.SneakyThrows;
import org.apache.avro.generic.GenericRecord;

import javax.net.ssl.SSLException;
import java.util.List;

public class TestClient {
    private static final String CERT_FILE_PATH = "/path/to/my/cert.pem"; // set path to your generated cert.pem

    private static final String EXTRACTION_SERVICE_PORT = "9999"; // set server port
    private static final String EXTRACTION_SERVICE_HOST = "localhost"; //set valid hostname based on cert.pem

    public static final String EXTRACTION_SERVICE_URL = String
            .join(":", EXTRACTION_SERVICE_HOST, EXTRACTION_SERVICE_PORT);

    //Don't commit your password here!!!
    private static final String USERNAME = "acclog";
    private static final String PASSWORD = "";

    @SneakyThrows
    public static void main(String[] args) {
//        ExtractionServiceGrpc.ExtractionServiceBlockingStub extractionService = ServiceFactory
//                .createExtractionService("localhost:9999", new FileInputStream(new File("cert.pem")),
//                        ServiceFactory.DEFAULT_MAX_MESSAGE_SIZE, ServiceFactory.DEFAULT_MAX_METADATA_SIZE, ServiceFactory.DEFAULT_MAX_RETRY);

//        ExtractionServiceGrpc.ExtractionServiceBlockingStub extractionService = ServiceFactory
//                .createExtractionService(EXTRACTION_SERVICE_URL, new FileInputStream(new File(CERT_FILE_PATH)),
//                        ServiceFactory.DEFAULT_MAX_MESSAGE_SIZE, ServiceFactory.DEFAULT_MAX_METADATA_SIZE,
//                        ServiceFactory.DEFAULT_MAX_RETRY);


        ExtractionServiceGrpc.ExtractionServiceBlockingStub extractionService = ServiceFactory.createExtractionService("cs-ccr-nxcalstst5.cern.ch:15000");


        String query = DataQuery.builder()
                .byEntities()
                .system("MOCK-SYSTEM")
                .startTime("2020-12-15 09:35:00.0")
                .endTime("2020-12-15 09:35:59.999999999")
                .entity().keyValue("device", "NXCALS_MONITORING_DEV0")
                .build() + ".where(\"`special-character`='test'\")";

        System.out.println(query);

        login();

        for (int i = 0; i < 10000; i++) {
            long start = System.currentTimeMillis();

            AvroData data = extractionService
                    .query(AvroQuery.newBuilder().setScript(query).setCompression(AvroQuery.Codec.BZIP2).build());

            System.out.println(
                    "Received data after " + (System.currentTimeMillis() - start) + " ms, size " + data.getAvroBytes()
                            .size());

            List<GenericRecord> genericRecords = Avro.records(data);

            System.out.println("Converted to avro after " + (System.currentTimeMillis() - start) + " ms");
            System.out.println("Nb of records: " + genericRecords.size());
            System.out.println(genericRecords.get(0));
            if(genericRecords.size()<60) {
                System.err.println("Wrong number of records received " + genericRecords.size());
                System.exit(1);
            }
        }
    }

    private static void login() {
        try {
            AuthenticationClient authenticationClient = AuthenticationClient.create();
            //Don't commit your password here!!!
            RbaToken token = authenticationClient.loginExplicit(USERNAME, PASSWORD);
            ClientTierTokenHolder.setRbaToken(token);
        } catch (AuthenticationException e) {
            throw new IllegalArgumentException("Cannot login", e);
        }
    }

    private static ExtractionServiceGrpc.ExtractionServiceBlockingStub getExtractionServicePlain() throws SSLException {
        NettyChannelBuilder channelBuilder = NettyChannelBuilder.forTarget(EXTRACTION_SERVICE_URL).usePlaintext();
        ManagedChannel channel = channelBuilder
                .maxInboundMessageSize(100_000_000).build();
        return ExtractionServiceGrpc
                .newBlockingStub(channel);
    }

}
