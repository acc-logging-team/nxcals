package cern.nxcals.api.extraction.thin.data.builders;

import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.extraction.data.builders.fluent.QueryData;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.common.domain.EntityKeyValue;
import cern.nxcals.common.domain.EntityKeyValues;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static cern.nxcals.api.extraction.thin.data.builders.ScriptProducer.TIME_METHOD;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ScriptProducerTest {
    private final ScriptProducer producer = new ScriptProducer();

    @Test
    void toJSMapShouldCorrectlyConvertJavaMapToJSMapString() {
        Map<String, Object> map = Map.of("key", "value", "key2", "value2");
        String expString = "{\"key\":\"value\",\"key2\":\"value2\"}";
        assertEquals(expString, producer.toJS(map));
    }

    @Test
    void fieldAliasesShouldReturnCorrectInvokeOfFieldAliases() {
        Map<String, Set<String>> fieldAliases = Map.of("alias", new LinkedHashSet<>(List.of("field1", "field2")),
                "alias2", Set.of("field3"));
        String fieldAliasesStr = producer.fieldAliasesStr(fieldAliases);
        assertTrue(fieldAliasesStr.contains(
                ".fieldAliases({\"alias\":Set.of(\"field1\",\"field2\"),\"alias2\":Set.of(\"field3\")})"));
    }

    @Test
    void variablesStrShouldCreateCorrectCode() {
        Map<String, Boolean> variables = Map.of("var", false, "var2", true, "var3", false);
        String variablesStr = producer.variablesStr(variables);
        assertAll(
                () -> assertTrue(variablesStr.contains(".nameEq(\"var\")")),
                () -> assertTrue(variablesStr.contains(".nameEq(\"var3\")")),
                () -> assertTrue(variablesStr.contains(".nameLike(\"var2\")"))
        );
    }

    @Test
    void entitiesStrShouldCreateCorrectCode() {
        EntityKeyValue ekv1 = EntityKeyValue.builder().key("dev").value("dev%").wildcard(false).build();
        EntityKeyValue ekv2 = EntityKeyValue.builder().key("prop").value("prop").wildcard(false).build();
        EntityKeyValue ekv3 = EntityKeyValue.builder().key("key").value("value%").wildcard(true).build();
        EntityKeyValues ekvs1 = EntityKeyValues.builder().keyValue(ekv1).keyValue(ekv2).build();
        EntityKeyValues ekvs2 = EntityKeyValues.builder().keyValue(ekv1).keyValue(ekv3).build();
        String entitiesStr = producer.entitiesStr(List.of(ekvs1, ekvs2));
        assertAll(
                () -> assertTrue(entitiesStr.contains(".keyValuesEq({\"dev\":\"dev%\",\"prop\":\"prop\"})")),
                () -> assertTrue(entitiesStr.contains(".keyValuesLike({\"dev\":\"dev\\%\",\"key\":\"value%\"})"))
        );
    }

    @Test
    void timeStrShouldCreateCorrectCode() {
        String time = "2020-11-11 11:11:11.000000000";
        Instant timeAsInstant = TimeUtils.getInstantFromString(time);
        TimeWindow timeWindow = TimeWindow.between(timeAsInstant, timeAsInstant);
        String timeStr = producer.timeStr(timeWindow);
        assertTrue(timeStr.contains(String.format(TIME_METHOD, time, time)));
    }

    @Test
    void shouldProduceCorrectEntityScript() {
        QueryData<String> queryData = new QueryData<>(producer);
        String system = "CMW";
        queryData.setSystem(system);
        queryData.addDevice("dev", false);
        queryData.addProperty("prop", false);
        queryData.closeEntity();
        Instant time = Instant.now();
        queryData.setStartTime(time);
        queryData.setEndTime(time);
        queryData.addAlias("alias", "field");

        String resultScript = queryData.build();
        assertAll(
                () -> assertTrue(resultScript.contains("DataQuery.builder(sparkSession)")),
                () -> assertTrue(resultScript.contains(String.format(".system(\"%s\")", system))),
                () -> assertTrue(resultScript.contains(".entities()")),
                () -> assertTrue(resultScript.contains(".fieldAliases")),
                () -> assertTrue(resultScript.contains(".timeWindow")),
                () -> assertTrue(resultScript.contains(".keyValuesEq")),
                () -> assertTrue(resultScript.contains(".build()"))
        );
    }

    @Test
    void shouldProduceCorrectVariableScript() {
        QueryData<String> queryData = new QueryData<>(producer);
        String system = "CMW";
        queryData.setSystem(system);
        queryData.setVariableKey("var", false);
        queryData.closeEntity();
        Instant time = Instant.now();
        queryData.setStartTime(time);
        queryData.setEndTime(time);
        queryData.addAlias("alias", "field");

        String resultScript = queryData.build();
        assertAll(
                () -> assertTrue(resultScript.contains("DataQuery.builder(sparkSession)")),
                () -> assertTrue(resultScript.contains(String.format(".system(\"%s\")", system))),
                () -> assertTrue(resultScript.contains(".variables()")),
                () -> assertTrue(resultScript.contains(".fieldAliases")),
                () -> assertTrue(resultScript.contains(".timeWindow")),
                () -> assertTrue(resultScript.contains(".nameEq")),
                () -> assertTrue(resultScript.contains(".build()"))
        );
    }

    @Test
    void shouldReturnEmptyStringIfPassedEmptyIdsSet() {
        assertTrue(producer.idsStr(Set.of()).isEmpty());
    }

    @Test
    void shouldCorrectlyCreateStrInMethodCall() {
        Set<Long> ids = Set.of(1L, 2L, 3L);
        String resultScript = producer.idsStr(ids);
        assertEquals(".idIn(1,2,3)", resultScript);
    }
}
