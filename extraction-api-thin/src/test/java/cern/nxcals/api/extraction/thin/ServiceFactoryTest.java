package cern.nxcals.api.extraction.thin;

import cern.rbac.common.RbaToken;
import cern.rbac.util.holder.ClientTierTokenHolder;
import cern.rbac.util.holder.MiddleTierRbaTokenHolder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ServiceFactoryTest {

    @AfterEach
    public void tearDown() {
        MiddleTierRbaTokenHolder.clear();
        ClientTierTokenHolder.clear();
    }

    @Test
    public void shouldGetTokenFromMiddleTier() {
        //given
        MiddleTierRbaTokenHolder.set(RbaToken.EMPTY_TOKEN);
        //when
        byte[] token = ServiceFactory.getToken();

        //then
        assertNotNull(token);

    }

    @Test
    public void shouldGetTokenFromClientTier() {
        //given
        ClientTierTokenHolder.setRbaToken(RbaToken.EMPTY_TOKEN);
        //when
        byte[] token = ServiceFactory.getToken();

        //then
        assertNotNull(token);

    }

    @Test
    public void shouldThrowWhenTokenNotFound() {
        //given (no token set)
        //when
        assertThrows(IllegalArgumentException.class, () -> ServiceFactory.getToken());

        //then exception

    }

}
