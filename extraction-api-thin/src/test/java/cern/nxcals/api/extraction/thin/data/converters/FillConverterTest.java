package cern.nxcals.api.extraction.thin.data.converters;


import cern.nxcals.api.custom.converters.FillConverter;
import cern.nxcals.api.custom.domain.Fill;
import cern.nxcals.api.extraction.thin.BeamMode;
import cern.nxcals.api.extraction.thin.FillData;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FillConverterTest {

    @Test
    public void shouldConvertToFill() {
        //given
        String beamModeValue = "VALUE";
        //when
        Fill fill = FillConverter.toFill(createFillData(beamModeValue));
        //then
        assertEquals(beamModeValue, fill.getBeamModes().get(0).getBeamModeValue());
    }

    @Test
    public void shouldConvertToFillList() {
        //given
        String beamModeValue = "VALUE";
        List<FillData> fillsData = new ArrayList<>();
        fillsData.add(createFillData(beamModeValue));
        //then
        assertEquals(1, FillConverter.toFillList(fillsData).size());
    }

    private FillData createFillData(String beamModeValue) {
        BeamMode beamMode = BeamMode.newBuilder().setBeamModeValue(beamModeValue).build();
        return FillData.newBuilder().addBeamModes(beamMode).build();
    }
}