package cern.nxcals.monitoring.grok;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by jwozniak on 17/06/17.
 */
public class PatternsChecker {

    public static void main(String[] args) {
        testRowPattern();
        testJsonPattern();
    }

    public static void testRowPattern() {
        String[] linePatterns = { ".*ERROR.*", ".*ProcessorImpl - Errors found.*",
                ".*ProcessorImpl - ERROR: Entity.*" };
        final Pattern[] patterns = Arrays.stream(linePatterns).map(Pattern::compile).collect(Collectors.toList())
                .toArray(new Pattern[0]);

        Predicate<String> firstMatcher = l -> patterns[0].matcher(l).matches() &&
                Arrays.stream(Arrays.copyOfRange(patterns, 1, patterns.length)).noneMatch(p -> p.matcher(l).matches());

        String line = "ERROR ProcessorImpl1 - Errors found";

        List<Pattern> list = Arrays.asList(patterns);

        if (firstMatcher.test(line)) {
            System.err.println("row - Matches!!!!");
        } else {
            System.err.println("No match");
        }
    }

    public static void testJsonPattern() {
        String[] linePatterns = { ".*\"gc.*(young.*collection_time_in_millis\":([0-9]+)).*" };

        final Pattern[] patterns = Arrays.stream(linePatterns).map(Pattern::compile).collect(Collectors.toList())
                .toArray(new Pattern[0]);

        Predicate<String> firstMatcher = l -> patterns[0].matcher(l).matches() &&
                Arrays.stream(Arrays.copyOfRange(patterns, 1, patterns.length)).noneMatch(p -> p.matcher(l).matches());

        String line = "{old\":{\"young\":{\"peak_used_in_bytes\":286326784,\"used_in_bytes\":533586048,\"peak_max_in_bytes\":286326784,\"max_in_bytes\":572653568,\"committed_in_bytes\":572653568}}},\"gc\":{\"collectors\":{\"old\":{\"collection_time_in_millis\":210,\"collection_count\":2},\"young\":{\"collection_time_in_millis\":3816,\"collection_count\":235}}},\"uptime_in_millis\":10982015},\"process\":{\"open_file_descriptors\":191,\"peak_open_file_descriptors\":199,\"max_file_descriptors\":4096,\"mem\":{\"total_virtual_in_bytes\":7280472064},\"cpu\":{\"total_in_millis\":879740000000,\"percent\":0,\"load_average\":{\"1m\":0.09,\"5m\":0.17,\"15m\":0.17}}},\"pipeline\":{\"events\":{\"duration_in_millis\":1636178,\"in\":469488,\"filtered\":469488,\"out\":469488},\"plugins\":{\"inputs\":[],\"filters\":[],\"outputs\":[{\"id\":\"f5607bacc7bef96be237416c61c78522add6cd19-4\",\"events\":{\"duration_in_millis\":439738,\"in\":468758,\"out\":468758},\"name\":\"elasticsearch\"},{\"id\":\"f5607bacc7bef96be237416c61c78522add6cd19-3\",\"events\":{\"duration_in_millis\":2158,\"in\":730,\"out\":730},\"name\":\"file\"}]},\"reloads\":{\"last_error\":null,\"successes\":0,\"last_success_timestamp\":null,\"last_failure_timestamp\":null,\"failures\":0},\"queue\":{\"type\":\"memory\"}},\"reloads\":{\"successes\":0,\"failures\":0},\"os\":{\"cgroup\":{\"cpuacct\":{\"usage_nanos\":58817949271493,\"control_group\":\"/\"},\"cpu\":{\"cfs_quota_micros\":-1,\"control_group\":\"/\",\"stat\":{\"number_of_times_throttled\":0,\"time_throttled_nanos\":0,\"number_of_elapsed_periods\":0},\"cfs_period_micros\":100000}}}}";

        List<Pattern> list = Arrays.asList(patterns);

        if (firstMatcher.test(line)) {
            System.err.println("json row - Matches!!!!");
            Matcher matcher = patterns[0].matcher(line);
            matcher.matches();
            System.err.println(matcher.group(2));
        } else {
            System.err.println("No match");
        }
    }
}
