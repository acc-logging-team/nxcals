/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.monitoring.grok;

import cern.nxcals.monitoring.grok.service.FileProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class ApplicationDev {
    static {
        System.setProperty("org.apache.logging.log4j.simplelog.StatusLogger.level", "TRACE");
        System.setProperty("logging.config", "classpath:log4j2-test.yml");
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationDev.class);

    public static void main(String[] args) throws Exception {
        try {

            SpringApplication app = new SpringApplication(ApplicationDev.class);
            app.addListeners(new ApplicationPidFileWriter());
            ApplicationContext ctx = app.run();

            FileProcessor scanner = ctx.getBean(FileProcessor.class);
            scanner.start();

        } catch (Throwable e) {
            e.printStackTrace();
            LOGGER.error("Exception while running publisher app", e);
            System.exit(1);
        }
    }

}
