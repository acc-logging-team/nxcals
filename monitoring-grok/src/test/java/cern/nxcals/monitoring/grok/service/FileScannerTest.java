package cern.nxcals.monitoring.grok.service;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.FileSystem;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.PathMatcher;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FileScannerTest {
    @Test
    public void shouldAbsorbNoSuchFileExceptionCause() {
        Path pathMock = mock(Path.class);
        when(pathMock.getFileSystem()).thenThrow(new UncheckedIOException(new NoSuchFileException("")));

        FileSystem fsMock = mock(FileSystem.class);
        when(fsMock.getPathMatcher(anyString())).thenReturn(mock(PathMatcher.class));
        when(fsMock.getPath(anyString())).thenReturn(pathMock);

        new FileScanner(fsMock).scan("", "", 1);
    }

    @Test
    public void shouldThrowAnyOtherCause() {
        Path pathMock = mock(Path.class);
        when(pathMock.getFileSystem()).thenThrow(new UncheckedIOException(new IOException()));

        FileSystem fsMock = mock(FileSystem.class);
        when(fsMock.getPathMatcher(anyString())).thenReturn(mock(PathMatcher.class));
        when(fsMock.getPath(anyString())).thenReturn(pathMock);

        assertThrows(UncheckedIOException.class, () -> new FileScanner(fsMock).scan("", "", 1));
    }
}