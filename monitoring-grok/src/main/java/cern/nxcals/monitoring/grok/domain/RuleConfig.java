package cern.nxcals.monitoring.grok.domain;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jwozniak on 6/5/17.
 */
@Data
public class RuleConfig {
    private String name;
    private List<String> filePatternsInclude;
    private List<String> filePatternsExclude = new ArrayList<>();
    private List<String> linePatterns;
    private String initScript;
    private String conditionScript;
}
