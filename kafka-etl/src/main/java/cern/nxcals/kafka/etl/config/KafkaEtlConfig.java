package cern.nxcals.kafka.etl.config;

import cern.nxcals.api.config.AuthenticationContext;
import cern.nxcals.api.extraction.metadata.InternalServiceClientFactory;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.common.metrics.MetricsPulse;
import cern.nxcals.common.metrics.MetricsRegistry;
import cern.nxcals.internal.extraction.metadata.InternalEntitySchemaService;
import cern.nxcals.internal.extraction.metadata.InternalPartitionService;
import cern.nxcals.internal.extraction.metadata.InternalSystemSpecService;
import cern.nxcals.kafka.etl.service.DefaultPartitioner;
import cern.nxcals.kafka.etl.service.DefaultProcessor;
import cern.nxcals.kafka.etl.service.KafkaConsumerRunner;
import cern.nxcals.kafka.etl.service.Orchestrator;
import cern.nxcals.kafka.etl.service.Partitioner;
import cern.nxcals.kafka.etl.service.Processor;
import cern.nxcals.kafka.etl.service.Store;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Import;
import org.springframework.retry.backoff.ExponentialBackOffPolicy;
import org.springframework.retry.policy.TimeoutRetryPolicy;
import org.springframework.retry.support.RetryTemplate;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

@EnableConfigurationProperties(KafkaEtlProperties.class)
@Configuration
@Slf4j
@Import(AuthenticationContext.class)
public class KafkaEtlConfig {

    private final KafkaEtlProperties props;
    private final MetricsRegistry metricsRegistry;

    @Autowired
    public KafkaEtlConfig(KafkaEtlProperties props, MetricsRegistry metricsRegistry) {
        this.props = props;
        this.metricsRegistry = metricsRegistry;
    }

    @Bean(name = "orchestrator")
    @DependsOn("kerberos")
    public Orchestrator<byte[], byte[]> orchestrator(
            Supplier<KafkaConsumerRunner<byte[], byte[]>>  supplier,
            ScheduledExecutorService executor) {
        initMetric(DefaultProcessor.PROCESS_ERRORS_COUNT);
        initMetric(Orchestrator.RESCHEDULE_COUNT_METRIC_NAME);

        return new Orchestrator<>(executor, props, metricsRegistry, supplier);
    }

    @Bean
    public Supplier<KafkaConsumerRunner<byte[], byte[]>> getKafkaConsumerRunnerSupplier(
            ScheduledExecutorService executorService,
            ExecutorService executor,
            List<Store<byte[], byte[]>> stores,
            Partitioner<byte[], byte[]> partitioner) {
        return () -> new KafkaConsumerRunner<>(new KafkaConsumer<>(props.getConsumerProperties()), createProcessor(executor, stores, partitioner), executorService, props);
    }

    private Processor<byte[], byte[]> createProcessor(
            ExecutorService executor,
            List<Store<byte[], byte[]>> stores,
            Partitioner<byte[], byte[]> partitioner) {
        log.debug("Found Stores {} in Spring Context", stores);
        return new DefaultProcessor<>(partitioner, stores, executor, metricsRegistry, retryTemplate());
    }

    private RetryTemplate retryTemplate() {
        RetryTemplate retryTemplate = new RetryTemplate();

        ExponentialBackOffPolicy backOffPolicy = new ExponentialBackOffPolicy();
        backOffPolicy.setInitialInterval(props.getRetry().getBackoffInitialDelay().toMillis());
        backOffPolicy.setMaxInterval(props.getRetry().getBackoffMaxDelay().toMillis());

        retryTemplate.setBackOffPolicy(backOffPolicy);

        TimeoutRetryPolicy retryPolicy = new TimeoutRetryPolicy();
        retryPolicy.setTimeout(props.getRetry().getMaxExecutionTime().toMillis());
        retryTemplate.setRetryPolicy(retryPolicy);

        return retryTemplate;
    }

    private void initMetric(String metric) {
        //init the stats to be visible as zero at process startup
        metricsRegistry.incrementCounterBy(metric, 0);
    }

    @Bean
    public MetricsPulse exporter(ScheduledExecutorService service) {
        return new MetricsPulse(service, metricsRegistry);
    }

    @Bean
    public BlockingQueue<Runnable> queue() {
        return new ArrayBlockingQueue<>(props.getExecutorQueueSize());
    }

    @Bean
    public ExecutorService executor() {
        return new ThreadPoolExecutor(props.getExecutorThreadPoolSize(), props.getExecutorThreadPoolSize(), 10,
                TimeUnit.MINUTES, queue(), threadFactory("StoreThread-%d"));
    }

    @Bean
    public ScheduledExecutorService scheduledExecutorService() {
        //This is used in scheduling the KafkaRunners and Stores like HDFS. Must have more threads than just concurrency since KafkaRunners are not returning them.
        return Executors
                .newScheduledThreadPool(props.getConcurrency() * 3, threadFactory("ScheduledExecutorThread-%d"));
    }

    @Bean
    @DependsOn("kerberos")
    public InternalEntitySchemaService schemaService() {
        return InternalServiceClientFactory.createEntitySchemaService();
    }
    @Bean
    @DependsOn("kerberos")
    public InternalPartitionService partitionService() {
        return InternalServiceClientFactory.createPartitionService();
    }

    @Bean
    public Partitioner<byte[], byte[]> partitioner(InternalPartitionService partitionService) {
        long from = TimeUtils.getNanosFromInstant(
                TimeUtils.getInstantFromString(props.getTimeBucketSince()));
        return new DefaultPartitioner(props.getDailyPartitions(), from, partitionService);
    }

    @Bean
    @DependsOn("kerberos")
    public InternalSystemSpecService systemSpecService() {
        return InternalServiceClientFactory.createSystemSpecService();
    }

    private ThreadFactory threadFactory(String pattern) {
        return new ThreadFactoryBuilder().setNameFormat(pattern).build();
    }
}
