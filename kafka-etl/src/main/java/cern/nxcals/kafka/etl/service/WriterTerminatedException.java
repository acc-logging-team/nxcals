package cern.nxcals.kafka.etl.service;

public class WriterTerminatedException extends RuntimeException {
    public WriterTerminatedException(String message) {
        super(message);
    }
}
