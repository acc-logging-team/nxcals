package cern.nxcals.kafka.etl.hdfs;

import com.google.common.annotations.VisibleForTesting;
import lombok.extern.slf4j.Slf4j;
import org.apache.avro.Schema;
import org.apache.avro.file.DataFileWriter;

import java.io.Closeable;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;

import static cern.nxcals.common.utils.AvroUtils.createDataFileWriter;

/**
 * Avro stream writer.
 */
@Slf4j
class AvroOutputStreamWriter implements Closeable {
    private final String id;
    private final DataFileWriter<Object> dataFileWriter;

    @VisibleForTesting
    AvroOutputStreamWriter(String writerId, DataFileWriter<Object> writer) {
        this.id = writerId;
        this.dataFileWriter = writer;
    }

    AvroOutputStreamWriter(String writerId, Schema schema, OutputStream outstream, String codec, int syncIntervalBytes) throws IOException {
        this(writerId, createDataFileWriter(schema, outstream, codec, syncIntervalBytes));
    }


    void write(byte[] bytes) throws IOException {
        dataFileWriter.appendEncoded(ByteBuffer.wrap(bytes));
    }

    void flush() throws IOException {
        dataFileWriter.flush();
    }

    @Override
    public void close() throws IOException {
        try {
            dataFileWriter.close();
        } catch (IOException e) {
            log.error("Cannot close data writer for ", id, e);
            throw e;
        }
    }
}

