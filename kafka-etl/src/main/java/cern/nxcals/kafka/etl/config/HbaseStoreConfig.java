package cern.nxcals.kafka.etl.config;

import cern.nxcals.common.metrics.MetricsPulse;
import cern.nxcals.internal.extraction.metadata.InternalEntitySchemaService;
import cern.nxcals.internal.extraction.metadata.InternalSystemSpecService;
import cern.nxcals.kafka.etl.domain.PartitionInfo;
import cern.nxcals.kafka.etl.hbase.ConnectionSupplier;
import cern.nxcals.kafka.etl.hbase.HbaseStore;
import cern.nxcals.kafka.etl.hbase.HbaseTableProvider;
import cern.nxcals.kafka.etl.hbase.RecordConverter;
import cern.nxcals.kafka.etl.service.JMXController;
import cern.nxcals.kafka.etl.service.Store;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.validation.Validator;

import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;
import java.util.stream.Collectors;

import static cern.nxcals.common.Constants.HBASE_TABLE_NAME_DELIMITER;
import static cern.nxcals.common.Constants.PERMANENT_TABLE_PREFIX;

@EnableConfigurationProperties(HbaseProperties.class)
@Configuration
@Profile("hbase")
@DependsOn({ "kerberos" })
@AllArgsConstructor
public class HbaseStoreConfig {
    private final InternalSystemSpecService systemService;
    private final InternalEntitySchemaService schemaService;
    private final HbaseProperties properties;

    @Bean
    @Scope("prototype")
    @Profile("multi-store")
    public Store<byte[], byte[]> storeMulti(MetricsPulse metricsPulse,
            @Qualifier("connection-map") Map<String, ConnectionSupplier> connectionPools,
            @Qualifier("executor-map") Map<String, ExecutorService> threadPools,
            HbaseTableProvider tableProvider,
            RecordConverter recordConverter, JMXController jmxController) {
        return store(metricsPulse, connectionPools, threadPools, tableProvider, recordConverter, jmxController);
    }

    @Bean
    @Profile("!multi-store")
    public Store<byte[], byte[]> storeSingle(MetricsPulse metricsPulse,
            @Qualifier("connection-map") Map<String, ConnectionSupplier> connectionPools,
            @Qualifier("executor-map") Map<String, ExecutorService> threadPools,
            HbaseTableProvider tableProvider,
            RecordConverter recordConverter, JMXController jmxController) {
        return store(metricsPulse, connectionPools, threadPools, tableProvider, recordConverter, jmxController);
    }

    private HbaseStore store(MetricsPulse metricsPulse,
            Map<String, ConnectionSupplier> connectionPools,
            Map<String, ExecutorService> threadPools,
            HbaseTableProvider tableProvider,
            RecordConverter recordConverter, JMXController jmxController) {
        return new HbaseStore(systemService, schemaService, connectionPools, threadPools,
                tableProvider, properties, metricsPulse, recordConverter, jmxController);
    }

    @Bean
    public HbaseTableProvider tableProvider(Function<PartitionInfo, String> nameFunction, Map<String, ConnectionSupplier> connectionPools) {
        return new HbaseTableProvider(nameFunction, properties, connectionPools.get(properties.getTableProviderConnectionPoolName()));
    }

    @Bean
    @Profile("!single-table")
    public Function<PartitionInfo, String> multiTable() {
        return part -> {
            String tableName = String.join(HBASE_TABLE_NAME_DELIMITER,
                    String.valueOf(part.getSystemId()),
                    String.valueOf(part.getPartitionId()),
                    String.valueOf(part.getSchemaId()));
            if (part.isUpdatable()) {
                tableName = PERMANENT_TABLE_PREFIX + HBASE_TABLE_NAME_DELIMITER + tableName;
            }
            return tableName;
        };
    }

    @Bean
    @Profile("single-table")
    public Function<PartitionInfo, String> singleTable() {
        return part -> String.valueOf(part.getSystemId());
    }

    @Bean("connection-map")
    public Map<String, ConnectionSupplier> connectionManager() {
        return properties.getConnectionPools().entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, p -> newConnectionPool(p.getValue())));
    }

    private ConnectionSupplier newConnectionPool(HbaseProperties.ConnectionProperties connectionProperties) {
        return new ConnectionSupplier(connectionProperties);
    }

    @Bean("executor-map")
    public Map<String, ExecutorService> executorManager() {
        return properties.getExecutors().entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, p -> newExecutor(p.getValue())));
    }

    ExecutorService newExecutor(HbaseProperties.ExecutorProperties executorProperties) {
        return Executors.newFixedThreadPool(executorProperties.getThreadCount());
    }

    @Bean
    public RecordConverter converter() {
        return new RecordConverter(properties.getColumnFamilyAsBytes(), systemService, properties.getSaltBuckets(),
                properties.getOldSaltBuckets(), properties.getNewSaltingSince());
    }

    @Bean
    public static Validator configurationPropertiesValidator() {
        return new HbasePropertiesValidator();
    }
}
