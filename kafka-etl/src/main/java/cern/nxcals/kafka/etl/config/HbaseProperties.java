package cern.nxcals.kafka.etl.config;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.apache.hadoop.hbase.util.Bytes.toBytes;

@ConfigurationProperties("nxcals.kafka.etl.hbase")
@Data
@NoArgsConstructor
public class HbaseProperties {
    public static final String DEFAULT_WRITER_NAME = "default";
    public static final int NO_SALT_BUCKETS = -1;

    private String columnFamily;
    private String namespace;
    private int ttlHours;
    private String tableProviderConnectionPoolName;
    // -1 for old, not-salted logic; 1..n for custom number of buckets
    private int saltBuckets;
    private int oldSaltBuckets;
    private String newSaltingSince;
    private boolean mobEnabled = false;
    private long mobThreshold = 20971520L; // 20MB
    private long maxPutsSize = 1024 * 1024L;
    private long maxNumberOfRecordsInPut = 10 * 1024L;
    private int putsProcessingExecutorThreadPoolSize = 8;
    private int submitExecutorThreadPoolSize = 64;

    private Map<String, WriterProperties> writers;
    private Map<String, ExecutorProperties> executors;
    private Map<String, ConnectionProperties> connectionPools;

    public WriterProperties getWriterPropertiesFor(String id) {
        return writers.containsKey(id) ? writers.get(id) : writers.get(DEFAULT_WRITER_NAME);
    }

    public long getTtlHoursAsMillis() {
        return TimeUnit.HOURS.toMillis(ttlHours);
    }

    public byte[] getColumnFamilyAsBytes() {
        return toBytes(getColumnFamily());
    }

    @Data
    @NoArgsConstructor
    public static class WriterProperties {
        private int mutatorPoolSize = 1;
        private long writeBufferSize = -1;

        private String executorName;
        private String connectionPoolName;
        private long submitScaleUpThresholdNanos = 10_000_000;
    }

    @Data
    @NoArgsConstructor
    public static class ExecutorProperties {
        private int threadCount;
    }

    @Data
    @NoArgsConstructor
    public static class ConnectionProperties {
        private int maxConnections = 1;
        private long maxPerrequestHeapsize = -1;
        private long maxSubmitHeapsize = -1;
        private int maxPerserverTasks = -1;
        private int maxTotalTasks = -1;
        private int maxPerregionTasks = -1;
        private int maxKeyvalueSize = 10485760;
    }

}
