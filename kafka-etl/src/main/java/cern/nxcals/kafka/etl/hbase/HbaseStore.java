package cern.nxcals.kafka.etl.hbase;

import cern.nxcals.common.metrics.MetricsPulse;
import cern.nxcals.internal.extraction.metadata.InternalEntitySchemaService;
import cern.nxcals.internal.extraction.metadata.InternalSystemSpecService;
import cern.nxcals.kafka.etl.config.HbaseProperties;
import cern.nxcals.kafka.etl.domain.PartitionInfo;
import cern.nxcals.kafka.etl.service.AbstractStore;
import cern.nxcals.kafka.etl.service.JMXController;
import cern.nxcals.kafka.etl.service.Store;
import cern.nxcals.kafka.etl.utils.RecordsStats;
import cern.nxcals.kafka.etl.utils.RecordsStats.EntityStats;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.RemovalCause;
import lombok.extern.slf4j.Slf4j;
import org.apache.avro.Schema;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.TableNotFoundException;
import org.apache.hadoop.hbase.client.BufferedMutatorParams;
import org.apache.hadoop.hbase.client.Put;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.io.IOException;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static cern.nxcals.common.utils.ParallelUtils.IOParallelUtils.IORunnableExecutor.handleIO;
import static cern.nxcals.common.utils.ParallelUtils.createExecutor;
import static cern.nxcals.common.utils.ParallelUtils.processParallelIfNeeded;
import static cern.nxcals.kafka.etl.utils.KafkaUtils.splitIntoParts;

@Slf4j
public class HbaseStore extends AbstractStore implements Store<byte[], byte[]> {

    private static final String OPEN_TABLES_METRIC_NAME = "hbase_open_tables";
    private final long maxPutSize;
    private final long maxNumberOfRecordsInPut;
    private final ExecutorService putProcessingExecutor;
    private final ExecutorService submitExecutor;

    private final long ttlMilis;
    private final HbaseTableProvider tableProvider;
    private final RecordConverter recordConverter;

    private final Map<String, ConnectionSupplier> connectionPools;
    private final Map<String, ExecutorService> executorPools;

    private final Cache<TableName, HbaseWriter> writerCache;

    private final HbaseProperties properties;

    private final JMXController jmxController;

    @SuppressWarnings("squid:S00107") // too many parameters, but we need them all
    public HbaseStore(InternalSystemSpecService systemService, InternalEntitySchemaService schemaService,
            Map<String, ConnectionSupplier> connectionPools,
            Map<String, ExecutorService> executorPools, HbaseTableProvider tableProvider, HbaseProperties properties,
            MetricsPulse metricsPulse, RecordConverter recordConverter, JMXController jmxController) {
        super(schemaService, systemService);

        this.properties = properties;
        this.ttlMilis = properties.getTtlHoursAsMillis();

        this.tableProvider = Objects.requireNonNull(tableProvider);
        this.recordConverter = Objects.requireNonNull(recordConverter);
        this.connectionPools = Objects.requireNonNull(connectionPools);
        this.executorPools = Objects.requireNonNull(executorPools);
        this.jmxController = Objects.requireNonNull(jmxController);
        this.writerCache = Caffeine.newBuilder().removalListener(this::onWriterRemoval).build();

        this.putProcessingExecutor = createExecutor(properties.getPutsProcessingExecutorThreadPoolSize(),
                "PutProcessor-HbaseStore-" + this + "-Thread-%d");
        this.submitExecutor = createExecutor(properties.getSubmitExecutorThreadPoolSize(),
                "Submitter-HbaseStore-" + this + "-Thread-%d");
        this.maxNumberOfRecordsInPut = properties.getMaxNumberOfRecordsInPut();
        this.maxPutSize = properties.getMaxPutsSize();

        if (metricsPulse != null) {
            metricsPulse.register(OPEN_TABLES_METRIC_NAME, tableProvider::estimatedSize, 30, TimeUnit.SECONDS);
        }
        log.info("Created HbaseStore {}", this);
    }

    @SuppressWarnings("squid:S1172") // unused parameter
    private void onWriterRemoval(TableName key, HbaseWriter writer, RemovalCause removalCause) {
        log.debug("Cache eviction - removing writer for table ({}) with cause {}", writer.getName(), removalCause);
        try {
            writer.close();
        } catch (IOException e) {
            log.error("Failure on closing hbase writer", e);
        }
    }

    @Override
    public void save(PartitionInfo partition, List<ConsumerRecord<byte[], byte[]>> records) throws IOException {
        if (!accept(partition)) {
            return;
        }
        TableName table = tableProvider.tableFor(partition);
        HbaseWriter writer = writerCache.get(table, t -> createWriterFor(t, partition.getSystemId()));

        Schema schema = getSchema(partition);

        long start = System.currentTimeMillis();
        AtomicLong totalSubmitTime = new AtomicLong(0);
        AtomicLong totalDeserializeTime = new AtomicLong(0);
        AtomicLong totalSerializeTime = new AtomicLong(0);

        List<CompletableFuture<List<Put>>> futurePuts = convertRecordsToPuts(records, schema, partition,
                totalDeserializeTime, totalSerializeTime);

        List<CompletableFuture<Optional<IOException>>> futureSubmits = submitPuts(futurePuts, writer, totalSubmitTime);

        long spawningJobsTime = System.currentTimeMillis() - start;

        waitAndHandleExceptions(futureSubmits, partition, records, table);

        long end = System.currentTimeMillis();
        long totalSize = calculateTotalSize(records);

        warnOnLongSubmit(writer.getName(), records, totalSize, totalDeserializeTime.get() + totalSerializeTime.get(),
                totalSubmitTime.get());
        Map<String, Long> metrics = createMetrics(end - start, totalSize, spawningJobsTime,
                totalDeserializeTime.get(), totalSerializeTime.get(), totalSubmitTime.get(), futureSubmits.size());
        logAnalysis(writer.getName(), records, metrics);
    }

    private List<CompletableFuture<List<Put>>> convertRecordsToPuts(List<ConsumerRecord<byte[], byte[]>> records,
            Schema schema, PartitionInfo partition, AtomicLong totalDeserializeTime,
            AtomicLong totalSerializeTime) {
        List<List<ConsumerRecord<byte[], byte[]>>> recordsPartitioned = splitIntoParts(records, maxPutSize,
                maxNumberOfRecordsInPut);

        Function<List<ConsumerRecord<byte[], byte[]>>, List<Put>> mapperFunction = recordsPartition -> recordConverter.toPuts(
                recordsPartition, schema, partition, totalDeserializeTime, totalSerializeTime);

        return processParallelIfNeeded(recordsPartitioned, mapperFunction, putProcessingExecutor);
    }

    private List<CompletableFuture<Optional<IOException>>> submitPuts(List<CompletableFuture<List<Put>>> futurePuts,
            HbaseWriter writer, AtomicLong totalSubmitTime) {
        Function<List<Put>, Optional<IOException>> submitter = handleIO(
                putList -> submitPutsBatch(putList, writer, totalSubmitTime));

        return futurePuts.stream().map(future -> future.thenApplyAsync(submitter, submitExecutor))
                .collect(Collectors.toList());
    }

    private void submitPutsBatch(List<Put> puts, HbaseWriter writer, AtomicLong totalSubmitTime) throws IOException {
        long start = System.currentTimeMillis();
        writer.submit(puts);
        long end = System.currentTimeMillis();
        totalSubmitTime.addAndGet(end - start);
    }

    private void waitAndHandleExceptions(List<CompletableFuture<Optional<IOException>>> futures,
            PartitionInfo partition,
            List<ConsumerRecord<byte[], byte[]>> records, TableName table) throws IOException {
        for (CompletableFuture<Optional<IOException>> future : futures) {
            Optional<IOException> maybeException = future.join();
            if (maybeException.isPresent()) {
                IOException e = maybeException.get();
                if (e instanceof TableNotFoundException) {
                    tableProvider.removeTableFromCache(table.getNameAsString());
                    this.save(partition, records);
                } else {
                    throw e;
                }
            }
        }
    }

    private long calculateTotalSize(List<ConsumerRecord<byte[], byte[]>> records) {
        long totalSize = 0;
        for (ConsumerRecord<byte[], byte[]> record : records) {
            totalSize += record.value().length;
        }
        return totalSize;
    }

    private void warnOnLongSubmit(TableName tableName, List<ConsumerRecord<byte[], byte[]>> records, long totalSize,
            long putsTime, long submitTime) {
        if (log.isWarnEnabled() && submitTime > jmxController.getLongWriteThresholdMillis()) {
            log.warn("Long write to Hbase detected, saved {} records of totalSize={} bytes"
                            + " for table {}, created Puts in {} ms, sent to Hbase in {} ms",
                    records.size(), totalSize, tableName,
                    putsTime, submitTime);
        }
    }

    private Map<String, Long> createMetrics(long wallTime, long totalSize, long spawningJobsTime,
            long totalDeserializeTime, long totalSerializeTime, long totalSubmitTime, int numberOfJobs) {
        LinkedHashMap<String, Long> metrics = new LinkedHashMap<>();

        metrics.put("Total wall time (ms) ", wallTime);
        metrics.put("Total size of processed records (bytes) ", totalSize);
        metrics.put("Spawning jobs time (ms) ", spawningJobsTime);
        metrics.put("Total puts (deserialize + serialize) time (ms) ", totalDeserializeTime + totalSerializeTime);
        metrics.put("Total deserialize time (ms) ", totalDeserializeTime);
        metrics.put("Total serialize time (ms) ", totalSerializeTime);
        metrics.put("Total submit time (ms) ", totalSubmitTime);
        metrics.put("Number of jobs: ", (long) numberOfJobs);

        return metrics;
    }

    private void logAnalysis(TableName tableName, List<ConsumerRecord<byte[], byte[]>> records,
            Map<String, Long> metrics) {
        if (jmxController.getEnableRecordAnalysis()) {
            String statsAsString = getRecordsStatsAsString(tableName, records);

            log.debug(
                    "Saved {} records for table {}, metrics: {} {}{}",
                    records.size(), tableName, metrics,
                    System.lineSeparator(),
                    statsAsString);

        }
    }

    private String getRecordsStatsAsString(TableName name, List<ConsumerRecord<byte[], byte[]>> records) {
        try {
            Stream<EntityStats> stats = RecordsStats.from(records)
                    .sorted(Comparator.comparing(EntityStats::getTotalSize).reversed())
                    .limit(jmxController.getMaxRecordsToDisplay());

            return RecordsStats.asString(stats);
        } catch (Exception ex) {
            log.error("Cannot create record stats for table {}", name, ex);
            return "Cannot create record stats: " + ex.getMessage();
        }
    }

    private HbaseWriter createWriterFor(TableName table, long systemId) {
        String name = getSystem(systemId).getName();

        HbaseProperties.WriterProperties writerProperties = properties.getWriterPropertiesFor(name);

        BufferedMutatorParams params = new BufferedMutatorParams(table);
        if (writerProperties.getWriteBufferSize() > 0) {
            params.writeBufferSize(writerProperties.getWriteBufferSize());
        }
        ExecutorService executor = executorPools.get(writerProperties.getExecutorName());
        ConnectionSupplier connection = connectionPools.get(writerProperties.getConnectionPoolName());
        params.pool(executor);

        return new HbaseWriter(connection, params, writerProperties.getMutatorPoolSize(), table,
                writerProperties.getSubmitScaleUpThresholdNanos());
    }

    @Override
    public void flush(Set<PartitionInfo> usedPartitions) throws IOException {
        try {
            int errorCount = getForkJoinPool().submit(
                    () -> usedPartitions.parallelStream()
                            .map(tableProvider::tableFor)
                            .distinct()
                            .map(writerCache::getIfPresent)
                            .filter(Objects::nonNull)
                            .mapToInt(this::tryFlush)
                            .sum()
            ).get();
            if (errorCount > 0) {
                throw new IOException("Errors (" + errorCount + ") in flushing the writers, check log for exceptions");
            }
        } catch (InterruptedException e) {
            log.error("Interrupted!", e);
            // Restore interrupted state...
            Thread.currentThread().interrupt();
            throw new IllegalStateException(e);
        } catch (ExecutionException e) {
            throw new IllegalStateException(e);
        }
    }

    private int tryFlush(HbaseWriter writer) {
        long start = System.currentTimeMillis();
        try {
            writer.flush(submitExecutor);
            return 0;
        } catch (TableNotFoundException e) {
            String tableName = writer.getName().getNameAsString();
            log.warn("Table with name {} was not found. Invalidating cache to recreate it", tableName);
            tableProvider.removeTableFromCache(tableName);
            return this.tryFlush(writer);
        } catch (Exception e) {
            log.error("Error during flushing writer for table {}", writer.getName(), e);
            return 1;
        } finally {
            long flushTime = System.currentTimeMillis() - start;
            if (log.isWarnEnabled() && flushTime > jmxController.getLongWriteThresholdMillis()) {
                log.warn("Long flush time detected {} ms for table {}", flushTime, writer.getName());
            }
        }
    }

    @Override
    public void close() {
        recordConverter.close();
        writerCache.invalidateAll();
        writerCache.cleanUp();
        submitExecutor.shutdown();
        putProcessingExecutor.shutdown();
    }

    private boolean accept(PartitionInfo partition) {
        if (partition.isUpdatable()) {
            return true;
        } else {
            return partition.getTimestampMillis() > (System.currentTimeMillis() - ttlMilis);
        }
    }
}
