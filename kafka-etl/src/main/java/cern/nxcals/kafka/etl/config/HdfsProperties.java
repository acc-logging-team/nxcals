package cern.nxcals.kafka.etl.config;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.concurrent.TimeUnit;

/**
 * Created by msobiesz on 29/10/18.
 */
@ConfigurationProperties("nxcals.kafka.etl.hdfs")
@Data
@NoArgsConstructor
public class HdfsProperties {
    private String basePath;
    private String compressionCodec;
    private long fileExpireAfter;
    private TimeUnit fileExpireAfterUnit;
    private long maxFiles;
    private int syncIntervalBytes;
    private int fileBufferSize;
}
