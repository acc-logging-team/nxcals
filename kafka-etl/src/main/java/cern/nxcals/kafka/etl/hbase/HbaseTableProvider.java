package cern.nxcals.kafka.etl.hbase;

import cern.nxcals.kafka.etl.config.HbaseProperties;
import cern.nxcals.kafka.etl.domain.PartitionInfo;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableExistsException;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;

import javax.annotation.PreDestroy;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.time.Duration;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;
import static org.apache.hadoop.hbase.io.compress.Compression.Algorithm.SNAPPY;
import static org.apache.hadoop.hbase.io.encoding.DataBlockEncoding.FAST_DIFF;

@Slf4j
@SuppressWarnings({"java:S1874","squid:CallToDeprecatedMethod"})
public class HbaseTableProvider {

    private final Function<PartitionInfo, String> nameGenerator;
    private final Cache<String, ImmutablePair<TableName, Boolean>> tableCache;
    private final HbaseProperties props;
    private final Supplier<Connection> connectionSupplier;

    public HbaseTableProvider(Function<PartitionInfo, String> nameGenerator, HbaseProperties props,
            Supplier<Connection> connectionSupplier) {
        this.props = requireNonNull(props);
        this.connectionSupplier = requireNonNull(connectionSupplier);
        this.nameGenerator = requireNonNull(nameGenerator);
        this.tableCache = Caffeine.newBuilder().expireAfterAccess(Duration.ofHours(2))
                .removalListener((name, table, removalCause) -> log.debug("Removing table {} from cache", table))
                .build();
    }

    public TableName tableFor(PartitionInfo partition) {
        String tableName = nameGenerator.apply(partition);
        return requireNonNull(tableCache.get(tableName, t ->
                getTable(t, partition.isUpdatable()))).left;
    }

    private ImmutablePair<TableName, Boolean> getTable(String name, boolean isUpdatable) {
        TableName tableName = TableName.valueOf(props.getNamespace(), name);
        try (Admin admin = connectionSupplier.get().getAdmin()) {
            if (admin.tableExists(tableName)) {
                log.debug("Table {} exists", tableName);
                checkTable(admin, tableName, isUpdatable);
            } else {
                log.info("Table {} does not exist, so creating one.", name);
                admin.createTable(tableDescriptorFor(tableName, isUpdatable));
                log.info("Table {} has been created.", name);
            }
        } catch (TableExistsException e) {
            log.warn("Table {} has been meanwhile created by some other process", name);
        } catch (IOException e) {
            log.error("Exception while accessing HBase Admin", e);
            throw new UncheckedIOException(e);
        }
        return ImmutablePair.of(tableName, isUpdatable);
    }

    private HTableDescriptor tableDescriptorFor(TableName tableName, boolean isUpdatable) {
        HTableDescriptor table = new HTableDescriptor(tableName);
        table.setConfiguration("hbase.table.sanity.checks", "false");
        HColumnDescriptor cDesc = new HColumnDescriptor(props.getColumnFamily());
        cDesc.setCompressionType(SNAPPY);
        cDesc.setDataBlockEncoding(FAST_DIFF);
        if (props.isMobEnabled()) {
            cDesc.setMobEnabled(true);
            cDesc.setMobThreshold(props.getMobThreshold());
        }
        if (!isUpdatable) {
            cDesc.setTimeToLive((int) TimeUnit.HOURS.toSeconds(props.getTtlHours()));
        }
        table.addFamily(cDesc);
        return table;
    }

    long estimatedSize() {
        return tableCache.estimatedSize();
    }

    @PreDestroy
    public void destroy() {
        tableCache.invalidateAll();
        tableCache.cleanUp();
    }

    public void removeTableFromCache(String tableName) {
        tableCache.invalidate(tableName);
    }

    private void checkTable(Admin admin, TableName tableName, boolean isUpdatable) throws IOException {
        //Each table has one column family which TTL property can be changed
        HColumnDescriptor columnFamily = admin.getTableDescriptor(tableName).getColumnFamilies()[0];
        int ttl = columnFamily.getTimeToLive();
        if (isUpdatable && ttl != Integer.MAX_VALUE) {
            columnFamily.setTimeToLive(Integer.MAX_VALUE);
            admin.modifyColumn(tableName, columnFamily);
        } else if (!isUpdatable && ttl == Integer.MAX_VALUE) {
            columnFamily.setTimeToLive((int) TimeUnit.HOURS.toSeconds(props.getTtlHours()));
            admin.modifyColumn(tableName, columnFamily);
        }
    }
}
