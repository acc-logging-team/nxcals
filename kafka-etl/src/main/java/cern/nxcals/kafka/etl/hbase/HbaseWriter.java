package cern.nxcals.kafka.etl.hbase;

import com.google.common.base.Preconditions;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.BufferedMutator;
import org.apache.hadoop.hbase.client.BufferedMutatorParams;
import org.apache.hadoop.hbase.client.Put;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import static cern.nxcals.common.utils.ParallelUtils.IOParallelUtils.doIOOperations;

@Slf4j
public class HbaseWriter {
    private final ConcurrentHashMap<Integer, BufferedMutator> mutators = new ConcurrentHashMap<>();

    @Getter
    private final TableName name;
    private final ConnectionSupplier connectionSupplier;
    private final BufferedMutatorParams mutatorParams;

    private final AtomicLong recordCount = new AtomicLong(0);
    private final AtomicLong byteCount = new AtomicLong(0);
    private final AtomicInteger nextMutatorIndex = new AtomicInteger(0);
    private final AtomicInteger currentMutatorPoolSize = new AtomicInteger(1);
    private final int mutatorsMaxPoolSize;
    private final long submitUpScaleThreshold;

    public HbaseWriter(ConnectionSupplier connectionSupplier, BufferedMutatorParams params, int poolSize,
            TableName tableName, long submitUpScaleThreshold) {
        Preconditions.checkArgument(poolSize > 0, "poolSize must be positive");
        this.mutatorsMaxPoolSize = poolSize;
        this.connectionSupplier = connectionSupplier;
        this.mutatorParams = params;
        this.name = tableName;
        this.submitUpScaleThreshold = submitUpScaleThreshold;
    }

    public void submit(List<Put> puts) throws IOException {
        long start = System.nanoTime();
        getNextMutator().mutate(puts);
        long submitTime = System.nanoTime() - start;
        analyseSubmitTime(submitTime);
        recordCount.addAndGet(puts.size());
        long size = puts.stream().mapToLong(t -> t.getRow().length).sum();
        byteCount.addAndGet(size);
    }

    private void analyseSubmitTime(long submitTime) {
        if (submitTime > submitUpScaleThreshold) {
            int setValue = Math.min(mutatorsMaxPoolSize, currentMutatorPoolSize.get() + 1);
            currentMutatorPoolSize.set(setValue);
            log.debug("Increased HbaseWriter pool to " + setValue + "/" + mutatorsMaxPoolSize + ", current value: "
                    + currentMutatorPoolSize.get());
        }
    }

    private BufferedMutator getNextMutator() {
        int index = nextMutatorIndex.getAndUpdate(v -> (v + 1) % currentMutatorPoolSize.get());
        return mutators.computeIfAbsent(index, ind -> {
            try {
                return connectionSupplier.get().getBufferedMutator(this.mutatorParams);
            } catch (IOException e) {
                throw new UncheckedIOException("Cannot create mutator for table " + this.name, e);
            }
        });
    }

    public void flush(ExecutorService executorService) throws IOException {
        try {
            List<IOException> exceptions = doIOOperations(mutators.values(), BufferedMutator::flush, executorService);
            if (!exceptions.isEmpty()) {
                IOException exception = exceptions.get(0);
                for (int i = 1; i < exceptions.size(); i++) {
                    exception.addSuppressed(exceptions.get(i));
                }
                throw exception;
            }
            log.trace("Success flushing mutator {}, records written={}, size (bytes)={}", name, recordCount.get(),
                    byteCount.get());

            recordCount.set(0);
            byteCount.set(0);
        } catch (Exception e) {
            String message = String.format("Error flushing mutator %s, records written=%d, size (bytes)=%d",
                    name, recordCount.get(), byteCount.get());
            log.error(message); //kept to be symmetric with logs from successful run.
            throw e;
        }
    }

    public void close() throws IOException {
        for (BufferedMutator mutator : mutators.values()) {
            mutator.close();
        }
    }
}
