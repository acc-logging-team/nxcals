package cern.nxcals.kafka.etl;

import cern.nxcals.kafka.etl.service.Orchestrator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
@Slf4j
public class Application {
    static {
        Thread.setDefaultUncaughtExceptionHandler((thread, ex) ->
                log.error("Uncaught exception in thread {}", thread, ex)
        );
    }
    public static void main(String[] args) throws Exception {
        try {

            SpringApplication app = new SpringApplication(Application.class);
            app.addListeners(new ApplicationPidFileWriter());
            ConfigurableApplicationContext context = app.run();
            Orchestrator orchestrator = context.getBean(Orchestrator.class);
            orchestrator.start();

        } catch (Exception ex) {
            log.error("Exception while running publisher app", ex);
            System.exit(1);
        }

    }

}