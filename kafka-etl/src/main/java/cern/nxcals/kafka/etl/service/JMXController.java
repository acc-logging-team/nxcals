package cern.nxcals.kafka.etl.service;

import cern.nxcals.kafka.etl.config.KafkaEtlProperties;
import lombok.Data;
import lombok.NonNull;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Service;

/**
 * Used to manipulate some properties in the runtime (like analysis of records currently).
 */
@Service
@ManagedResource
@Data
public class JMXController {
    private int maxRecordsToDisplay;
    private boolean enableRecordAnalysis = false;
    private long longWriteThresholdMillis;

    public JMXController(@NonNull KafkaEtlProperties properties) {
        this.maxRecordsToDisplay = properties.getMaxRecordsToDisplay();
        this.longWriteThresholdMillis = properties.getLongWriteThreshold().toMillis();
    }

    @ManagedAttribute
    public int getMaxRecordsToDisplay() {
        return maxRecordsToDisplay;
    }

    @ManagedAttribute
    public void setMaxRecordsToDisplay(int maxRecordsToDisplay) {
        this.maxRecordsToDisplay = maxRecordsToDisplay;
    }

    @ManagedAttribute
    public boolean getEnableRecordAnalysis() {
        return this.enableRecordAnalysis;
    }

    @ManagedAttribute
    public void setEnableRecordAnalysis(boolean enableRecordAnalysis) {
        this.enableRecordAnalysis = enableRecordAnalysis;
    }

    @ManagedAttribute
    public long getLongWriteThresholdMillis() {
        return this.longWriteThresholdMillis;
    }

    @ManagedAttribute
    public void setLongWriteThresholdMillis(long value) {
        this.longWriteThresholdMillis = value;
    }

}
