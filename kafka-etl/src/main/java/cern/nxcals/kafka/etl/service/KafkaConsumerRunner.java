package cern.nxcals.kafka.etl.service;

import cern.nxcals.kafka.etl.config.KafkaEtlProperties;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Lists;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.Metric;
import org.apache.kafka.common.MetricName;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.errors.InvalidGroupIdException;
import org.apache.kafka.common.errors.WakeupException;
import org.jetbrains.annotations.NotNull;

import java.io.Closeable;
import java.io.IOException;
import java.time.Duration;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Pattern;

/**
 * Lives forever, handles interactions with KafkaConsumer, passes messages down to the MessageProcessor
 *
 * @param <K>
 * @param <V>
 */
@Slf4j
@RequiredArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(onlyExplicitlyIncluded = true)
public class KafkaConsumerRunner<K, V> implements Runnable, ConsumerRebalanceListener, Closeable {

    private static final AtomicLong runnerCount = new AtomicLong(1);
    @EqualsAndHashCode.Include
    @ToString.Include
    private final Long runnerId = runnerCount.getAndIncrement();
    private final Consumer<K, V> consumer;
    private final Processor<K, V> messageProcessor;
    private final ScheduledExecutorService scheduledExecutorService;
    private final KafkaEtlProperties props;
    private final OffsetHolder<K, V> offsetHolder = new OffsetHolder<>();
    private long totalRecords;
    private long totalTime;
    private String memberId;
    private String id;

    /**
     * This member id is not always available, it gets set up after partitions are assigned.
     * @return
     */
    private String getMemberId() {
        try {
            if (memberId == null || memberId.isEmpty()) {
                memberId = consumer.groupMetadata().memberId();
            }
        } catch(InvalidGroupIdException ex) {
            log.warn("runner-{} cannot obtain member id", runnerId, ex);
        }
        return memberId;
    }

    private String getId() {
        if(memberId == null || memberId.isEmpty() || id == null) {
            id = "runner-" + runnerId + " (" + getMemberId() + ")";
        }
        return id;
    }

    @SuppressWarnings("squid:S1764") //1==1 fails in Sonar while "while true" fails in IntelliJ...
    public void run() {

        try {
            log.debug("{} Start", getId());

            consumer.subscribe(Pattern.compile(props.getTopicPattern()), this);

            while (1 == 1) {
                processesMessages();
            }

        } catch (WakeupException e) {
            // Ignore exception, closing

        } catch (Exception ex) {
            log.error("{} Exception in a processing loop", getId(), ex);
            throw ex; //must throw to complete exceptionally the CompletableFuture in Orchestrator
        } finally {
            //Not committing offsets as this is reached usually when there is an exception.
            //Exception is usually from stores so committing at a store will likely result in another exception.
            //It might also be that we are trying to commit after a stale read when Kafka already reassigned the partitions to
            //some other consumer. This will also lead to an exception.
            consumer.close();
            log.debug("{} Consumer closed ", getId());
        }
    }

    private void processesMessages() {
        long startTime = System.currentTimeMillis();

        ConsumerRecords<K, V> records = consumer.poll(Duration.ofMillis(props.getPollTimeout()));
        long timeToPoll = System.currentTimeMillis() - startTime;

        long timeToProcess = 0;
        long timeToStoreOffsets = 0;

        if (!records.isEmpty()) {
            long startProcess = System.currentTimeMillis();
            messageProcessor.process(records);
            timeToProcess = System.currentTimeMillis() - startProcess;

            long startStoreOffsets = System.currentTimeMillis();
            offsetHolder.storeOffsets(records);
            timeToStoreOffsets = System.currentTimeMillis() - startStoreOffsets;
        }

        long startCommit = System.currentTimeMillis();
        checkAndCommit();
        long timeToCommit = System.currentTimeMillis() - startCommit;

        long lasted = System.currentTimeMillis() - startTime;
        long count = records.count();
        totalRecords += count;
        totalTime += lasted;
        log.debug("{} Processing {} records from partitions {} took {} ms, {} records/sec (kafka poll={} ms, processing={} ms, storing offsets={} ms, commit={} ms",
                getId(),count,
                records.partitions(), lasted, Math.round(totalTime == 0 ? 0 : totalRecords / (totalTime / 1000.0)), timeToPoll, timeToProcess, timeToStoreOffsets, timeToCommit);
    }

    private void checkAndCommit() {
        synchronized (offsetHolder) {
            if (offsetHolder.shouldCommitOffsets(props.getFlushInterval(),props.getFlushIntervalUnit())) {
                commitOffsets();
                flowControl();
            }
        }
    }

    /**
     * Introduces "sticky" partitions in case of too much lag. Should help with the Hadoop cluster load.
     */
    @VisibleForTesting
    void flowControl() {
        Set<TopicPartition> assignments = consumer.assignment();
        if (assignments.size() > 1) {
            TotalLag result = getTotalLag(assignments);
            if (result.total > props.getFlowControlThreshold()) {
                log.warn(
                        "{} Activating flow control as total lag {} greater than limit {}, active partition {} only with lag {}",
                        getId(), result.total, props.getFlowControlThreshold(), result.maxLagPartition,
                        result.maxLagForPartition);
                consumer.pause(assignments);
                consumer.resume(Lists.newArrayList(result.maxLagPartition));
                Set<TopicPartition> paused = consumer.paused();
                log.warn("runner-{}, paused {} partitions", runnerId, paused);
            } else {
                Set<TopicPartition> paused = consumer.paused();
                log.info(
                        "{} Flow control not needed, total lag {} less than limit {}, previously paused {} partitions, resuming all if necessary",
                        getId(), result.total, props.getFlowControlThreshold(), paused);
                if (!paused.isEmpty()) {
                    consumer.resume(paused);
                }
            }
        } else {
            log.info("{} Flow control not needed, only 1 partition assigned {}", getId(), assignments);
        }
    }

    @RequiredArgsConstructor
    private static class TotalLag {
        public final long total;
        public final TopicPartition maxLagPartition;
        public final long maxLagForPartition;
    }

    private @NotNull TotalLag getTotalLag(Set<TopicPartition> assignments) {
        log.info("{} Flow control check for partitions {}", getId(), assignments);
        Map<TopicPartition, Long> endOffsets = consumer.endOffsets(assignments);
        long totalLag = 0;
        TopicPartition maxLagPartition = null;
        long maxLagForPartition = 0;
        for (TopicPartition topicPartition : assignments) {
            long currentOffset = consumer.position(topicPartition);
            long endOffset = endOffsets.get(topicPartition);
            long lag = endOffset - currentOffset;
            log.info("{} Lag: {} for partition {}, current offset: {} end offset: {}", getId(), lag,
                    topicPartition, currentOffset, endOffset);
            if (lag > maxLagForPartition) {
                maxLagPartition = topicPartition;
                maxLagForPartition = lag;
            }
            totalLag += lag;
        }
        return new TotalLag(totalLag, maxLagPartition, maxLagForPartition);
    }

    private void commitOffsets() {
        log.debug("{} Committing offsets", getId());
        messageProcessor.flush();
        offsetHolder.commitOffsets(consumer);
    }

    @Override
    public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
        log.debug("{} Partitions {} revoked", getId(), partitions);
        //commitOffsets(); - trying to check if this is blocking the rebalance or not (jwozniak)
        //It might be that in case of slow ops on hbase this commit is long and blocks the whole
        //etl cluster from rebalancing as Kafka will wait for this method to finish before assigning
        //the partitions again.
        offsetHolder.clearOffsets();

    }

    @Override
    public void onPartitionsAssigned(Collection<TopicPartition> partitions) {
        log.debug("{} Partitions {} assigned", getId(), partitions);
        flowControl();
    }

    // Shutdown hook which can be called from a separate thread
    @Override
    public void close() throws IOException {
        consumer.wakeup();
    }

    @SuppressWarnings("squid:S1452") // generic type as return.
    public Map<MetricName, ? extends Metric> getMetrics() {
        return consumer.metrics();
    }

    @Slf4j
    static class OffsetHolder<K, V> {

        private final Map<TopicPartition, OffsetAndMetadata> offsets = new HashMap<>();
        private long lastCommitTime = System.currentTimeMillis();

        /**
         * Uses +-25% max random jitter to avoid flushing all at the same time.
         * This is an advice from Hadoop admins (Z.Baranowski) to flatten out the spikes of xceivers and
         * avoid going to maxes at flush times. This is important when returning from a large backlog of messages.
         */
        synchronized boolean shouldCommitOffsets(int flushInterval, TimeUnit flushIntervalUnit) {
            return !offsets.isEmpty() && (flushInterval == 0
                    || System.currentTimeMillis() - lastCommitTime >=
                    TimeUnit.MILLISECONDS.convert((long) (flushInterval * (1 + (2 * Math.random() - 1.0) / 4.0)),
                            flushIntervalUnit));
        }

        /*
         * Saving the last offset from each partition.
         */
        synchronized void storeOffsets(ConsumerRecords<K, V> records) {
            Set<TopicPartition> partitions = records.partitions();
            for (TopicPartition partition : partitions) {
                List<ConsumerRecord<K, V>> consumerRecords = records.records(partition);
                if (!consumerRecords.isEmpty()) {
                    long offset = consumerRecords.get(consumerRecords.size() - 1).offset();
                    //should add 1 to move the the next index for a given partition from which the reading will start
                    offsets.put(partition, new OffsetAndMetadata(offset + 1));
                }
            }
        }

        synchronized void commitOffsets(Consumer<K, V> consumer) {
            log.debug("Offsets to commit {}", this.offsets);
            consumer.commitSync(this.offsets);
            lastCommitTime = System.currentTimeMillis();
            this.offsets.clear();
        }

        synchronized void clearOffsets() {
            log.debug("Clearing offsets without commit {}", this.offsets);
            lastCommitTime = System.currentTimeMillis();
            this.offsets.clear();
        }
    }
}