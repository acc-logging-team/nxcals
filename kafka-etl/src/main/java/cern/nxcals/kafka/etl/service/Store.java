package cern.nxcals.kafka.etl.service;

import cern.nxcals.kafka.etl.domain.PartitionInfo;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.Set;

/**
 * Responsible for saving the records to the underlying storage.
 */
public interface Store<K, V> extends Closeable {
    void save(PartitionInfo partition, List<ConsumerRecord<K, V>> records) throws IOException;

    void flush(Set<PartitionInfo> usedPartitions) throws IOException;
}
