package cern.nxcals.kafka.etl.service;

import cern.nxcals.api.domain.PartitionProperties;
import cern.nxcals.api.domain.RecordKey;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.internal.extraction.metadata.InternalPartitionService;
import cern.nxcals.kafka.etl.domain.PartitionInfo;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.util.concurrent.TimeUnit;

@Slf4j
@RequiredArgsConstructor
public class DefaultPartitioner implements Partitioner<byte[], byte[]> {
    private final TimeUtils.TimeSplits dailyPartitions;
    //A date from which we will have the configured new time partitioning
    private final long newTimeSplitsFrom;
    private final InternalPartitionService partitionService;
    private static final Cache<Long, PartitionProperties> partitionsCache = Caffeine.newBuilder()
            .expireAfterWrite(2, TimeUnit.HOURS).build();
    @Override
    public PartitionInfo findPartition(ConsumerRecord<byte[], byte[]> record) {
        RecordKey key;
        try {
            key = RecordKey.deserialize(record.key());
        } catch (Exception e) {
            log.error("Cannot deserialize record key {} with value {}", record.key(), record.value(), e);
            //Has to return this to allow the process of partitioning to continue in the case of broken records (jwozniak)
            return PartitionInfo.INVALID_PARTITION;
        }
        long partitionId = key.getPartitionId();
        PartitionProperties properties = partitionsCache.get(partitionId, p -> partitionService.findById(partitionId)
                .orElseThrow(() -> new IllegalArgumentException("No partition with id " + partitionId + " found"))
                .getProperties());

        if (key.getTimestamp() >= newTimeSplitsFrom) {
            //if we have a specific staging partitioning type set we will use it.
            if (properties.getStagingPartitionType() != null && timestampInRange(key.getTimestamp(), properties)) {
                return PartitionInfo.from(key, properties.getStagingPartitionType(), properties.isUpdatable());
            }
            return PartitionInfo.from(key, dailyPartitions, properties.isUpdatable());
        } else {
            //old schema with 24 hours
            return PartitionInfo.from(key, TimeUtils.TimeSplits.ONE_HOUR_SPLIT, properties.isUpdatable());
        }
    }

    private boolean timestampInRange(long timestamp, PartitionProperties properties) {
        return properties.getStagingPartitionTypeValidity().isWithinRangeRightExcluded(timestamp);
    }
}
