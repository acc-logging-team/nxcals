package cern.nxcals.kafka.etl.hbase;

import cern.nxcals.kafka.etl.config.HbaseProperties;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HConstants;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionConfiguration;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.io.compress.GzipCodec;

import java.io.Closeable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

/**
 * The idea of this supplier works as round robin to server a fixed set of N connections among M mutators sequentially.
 */
@Slf4j
public class ConnectionSupplier implements Supplier<Connection>, Closeable {
    private final ConcurrentHashMap<Integer,Connection> connections = new ConcurrentHashMap<>();
    private final AtomicInteger current = new AtomicInteger(0);
    private final HbaseProperties.ConnectionProperties config;

    public ConnectionSupplier(HbaseProperties.ConnectionProperties config) {
        this.config = config;
    }

    @Override
    public Connection get() {
        int index = current.getAndUpdate(v -> (v + 1) % config.getMaxConnections());
        return connections.computeIfAbsent(index, ind -> createConnection(config));
    }

    private static Connection createConnection(HbaseProperties.ConnectionProperties properties) {
        org.apache.hadoop.conf.Configuration connectionConfig = HBaseConfiguration.create();

        if (properties.getMaxPerrequestHeapsize() > 0) {
            //defined in AsyncProcess.HBASE_CLIENT_MAX_PERREQUEST_HEAPSIZE but class not public
            connectionConfig.setLong("hbase.client.max.perrequest.heapsize", properties.getMaxPerrequestHeapsize());
        }
        if (properties.getMaxSubmitHeapsize() > 0) {
            //defined in AsyncProcess.HBASE_CLIENT_MAX_SUBMIT_HEAPSIZE but class not public
            connectionConfig.setLong("hbase.client.max.submit.heapsize", properties.getMaxSubmitHeapsize());
        }
        if (properties.getMaxPerserverTasks() > 0) {
            connectionConfig.setInt(HConstants.HBASE_CLIENT_MAX_PERSERVER_TASKS, properties.getMaxPerserverTasks());
        }
        if (properties.getMaxTotalTasks() > 0) {
            connectionConfig.setInt(HConstants.HBASE_CLIENT_MAX_TOTAL_TASKS, properties.getMaxTotalTasks());
        }
        if (properties.getMaxPerregionTasks() > 0) {
            connectionConfig.setInt(HConstants.HBASE_CLIENT_MAX_PERREGION_TASKS, properties.getMaxPerregionTasks());
        }
        connectionConfig.setInt(ConnectionConfiguration.MAX_KEYVALUE_SIZE_KEY, properties.getMaxKeyvalueSize());

        connectionConfig.set("hbase.client.rpc.compressor", GzipCodec.class.getCanonicalName());

        try {
            HBaseAdmin.available(connectionConfig);
            return ConnectionFactory.createConnection(connectionConfig);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void close() {
        for (Connection connection : connections.values()) {
            try {
                if(connection != null) {
                    connection.close();
                }
            } catch (Exception ex) {
                log.warn("Cannot close Hbase connection", ex);
            }
        }
    }
}
