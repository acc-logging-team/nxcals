package cern.nxcals.kafka.etl.service;

import cern.nxcals.api.domain.EntitySchema;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.extraction.metadata.queries.EntitySchemas;
import cern.nxcals.api.extraction.metadata.queries.SystemSpecs;
import cern.nxcals.internal.extraction.metadata.InternalEntitySchemaService;
import cern.nxcals.internal.extraction.metadata.InternalSystemSpecService;
import cern.nxcals.kafka.etl.domain.PartitionInfo;
import lombok.RequiredArgsConstructor;
import org.apache.avro.Schema;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ForkJoinPool;

@RequiredArgsConstructor
public class AbstractStore {

    private final InternalEntitySchemaService schemaService;
    private final InternalSystemSpecService systemService;
    private ConcurrentHashMap<Long, Schema> schemaCache = new ConcurrentHashMap<>();
    private ForkJoinPool forkJoinPool = new ForkJoinPool();

    protected Schema getSchema(PartitionInfo partition) {
        return schemaCache.computeIfAbsent(partition.getSchemaId(), this::findSchema);
    }

    private Schema findSchema(long schemaId) {
        EntitySchema schemaData = schemaService.findOne(EntitySchemas.suchThat().id().eq(schemaId)).orElseThrow(() -> new IllegalArgumentException("No such schema id " + schemaId));
        return new Schema.Parser().parse(schemaData.getSchemaJson());
    }

    protected SystemSpec getSystem(long systemId) {
        return this.systemService.findOne(SystemSpecs.suchThat().id().eq(systemId)).orElseThrow(() -> new IllegalArgumentException("No such system id " + systemId));
    }

    protected ForkJoinPool getForkJoinPool() {
        return this.forkJoinPool;
    }
}
