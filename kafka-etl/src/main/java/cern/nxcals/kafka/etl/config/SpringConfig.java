/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.kafka.etl.config;

import cern.nxcals.common.metrics.MetricsConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.support.DefaultConversionService;

@Configuration
@Import(MetricsConfig.class)
@SuppressWarnings("squid:S1118") //private constructor to avoid instantiation
public class SpringConfig {
    @Bean
    public static ConversionService conversionService() {
        return new DefaultConversionService();
    }
}
