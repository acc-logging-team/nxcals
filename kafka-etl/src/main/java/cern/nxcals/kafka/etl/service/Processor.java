package cern.nxcals.kafka.etl.service;

import org.apache.kafka.clients.consumer.ConsumerRecords;

public interface Processor<K, V> {
    void flush();

    void process(ConsumerRecords<K, V> data);
}
