package cern.nxcals.kafka.etl.service;

import cern.nxcals.common.metrics.MetricsRegistry;
import cern.nxcals.kafka.etl.config.KafkaEtlProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.Metric;
import org.apache.kafka.common.MetricName;
import org.apache.kafka.common.metrics.KafkaMetric;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;
import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.retry.listener.RetryListenerSupport;
import org.springframework.retry.policy.AlwaysRetryPolicy;
import org.springframework.retry.support.RetryTemplate;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

/**
 * A main class that runs all the KafkaConsumerRunners and reschedule them after their failures.
 */
@Slf4j
@RequiredArgsConstructor
public class Orchestrator<K, V> {

    public static final String RESCHEDULE_COUNT_METRIC_NAME = "reschedules";

    private final ScheduledExecutorService executorService;
    private final KafkaEtlProperties properties;
    private final MetricsRegistry metricsRegistry;
    private final Supplier<KafkaConsumerRunner<K, V>> kafkaConsumerRunnerSupplier;

    private final Set<KafkaConsumerRunner<K, V>> runners = Collections.newSetFromMap(new ConcurrentHashMap<>());
    private ScheduledFuture<?> future;

    private final RetryTemplate retryTemplate = new RetryTemplate();

    public void start() {
        future = executorService.scheduleAtFixedRate(this::readMetrics, 10, 60, TimeUnit.SECONDS);

        configureRetryTemplate();

        for (int i = 0; i < properties.getConcurrency(); ++i) {
            submit();
        }
    }

    private void configureRetryTemplate() {
        retryTemplate.setRetryPolicy(new AlwaysRetryPolicy());
        FixedBackOffPolicy policy = new FixedBackOffPolicy();
        policy.setBackOffPeriod(properties.getRescheduleDelayUnit().toMillis(properties.getRescheduleDelay()));
        retryTemplate.setBackOffPolicy(policy);

        retryTemplate.registerListener(new RetryListenerSupport() {
            @Override
            public <T, E extends Throwable> void onError(RetryContext context, RetryCallback<T, E> callback,
                    Throwable throwable) {
                metricsRegistry.incrementCounterBy(RESCHEDULE_COUNT_METRIC_NAME, 1);
            }
        });
    }

    private void submit() {
        executorService.execute(this::runWithRetryTemplate);
    }


    private void runWithRetryTemplate() {
        retryTemplate.execute(retryContext -> {
            run();
            return null;
        });
    }


    private void run() {
        KafkaConsumerRunner<K, V> runner = null;
        try {
            runner = kafkaConsumerRunnerSupplier.get();
            runners.add(runner);
            log.debug("Starting a new runner {}", runner);
            runner.run();
        } catch(Exception ex) {
            log.error("Runner {} failed with exception, will be retried", runner, ex);
            throw ex;
        } finally {
            if(runner != null) {
                runners.remove(runner);
            }
        }
    }

    /**
     * Called by Spring
     */
    public void shutdown() {
        log.debug("Shutting down, stopping runners");
        if (future != null) {
            future.cancel(false);
        }
        runners.forEach(runner -> {
            try {
                runner.close();
            } catch (Exception ex) {
                log.error("Exception while shutting down the runner", ex);
            }
        });
    }

    private void readMetrics() {
        runners.forEach(this::printMetrics);
    }

    private void printMetrics(KafkaConsumerRunner<K, V> runner) {
        try {
            Map<MetricName, ? extends Metric> metrics = runner.getMetrics();
            for (Map.Entry<MetricName, ? extends Metric> nameEntry : metrics.entrySet()) {
                Metric metric = nameEntry.getValue();
                if (metric instanceof KafkaMetric) {
                    KafkaMetric kafkaMetric = (KafkaMetric) metric;
                    if (kafkaMetric.metricName().name().contains("lag")) {
                        log.debug("Metrics for {} {}={} ({})", runner,
                                kafkaMetric.metricName().name(), kafkaMetric.metricValue()
                                , kafkaMetric.metricName().description());
                    }
                }
            }
        } catch (Exception ex) {
            log.error("Error reading metrics", ex);
        }
    }

}
