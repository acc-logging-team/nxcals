package cern.nxcals.kafka.etl.config;

import cern.nxcals.kafka.etl.domain.PartitionInfo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static cern.nxcals.common.Constants.HBASE_TABLE_NAME_DELIMITER;
import static cern.nxcals.common.Constants.PERMANENT_TABLE_PREFIX;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class HbaseStoreConfigTest {

    @Mock
    private HbaseStoreConfig instance;

    @Test
    void shouldReturnTableNameForNotUpdatablePartition() {
        //given
        PartitionInfo partitionInfo = PartitionInfo.from(1L, 2L, 3L, 100L, false);
        when(instance.multiTable()).thenCallRealMethod();
        //when
        String tableName = instance.multiTable().apply(partitionInfo);
        //then
        String expectedTableName = 1L + HBASE_TABLE_NAME_DELIMITER + 2L + HBASE_TABLE_NAME_DELIMITER + 3L;
        assertEquals(expectedTableName, tableName);
    }

    @Test
    void shouldReturnTableNameForUpdatablePartition() {
        //given
        PartitionInfo partitionInfo = PartitionInfo.from(1L, 2L, 3L, 100L, true);
        when(instance.multiTable()).thenCallRealMethod();
        //when
        String tableName = instance.multiTable().apply(partitionInfo);
        //then
        String expectedTableName =
                PERMANENT_TABLE_PREFIX + HBASE_TABLE_NAME_DELIMITER + 1L + HBASE_TABLE_NAME_DELIMITER + 2L
                        + HBASE_TABLE_NAME_DELIMITER + 3L;
        assertEquals(expectedTableName, tableName);
    }

}