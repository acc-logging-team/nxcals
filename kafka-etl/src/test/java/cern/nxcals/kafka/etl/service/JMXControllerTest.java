package cern.nxcals.kafka.etl.service;

import cern.nxcals.kafka.etl.config.KafkaEtlProperties;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class JMXControllerTest {

    @Test
    public void getMaxRecordsToDisplay() {
        //given
        KafkaEtlProperties props = new KafkaEtlProperties();
        props.setMaxRecordsToDisplay(10);
        JMXController controller = new JMXController(props);

        //when
        int maxRecordsToDisplay = controller.getMaxRecordsToDisplay();
        controller.setMaxRecordsToDisplay(20);
        int maxRecordsToDisplay2 = controller.getMaxRecordsToDisplay();

        //them
        assertEquals(10, maxRecordsToDisplay);
        assertEquals(20, maxRecordsToDisplay2);
    }

    @Test
    public void getEnableRecordAnalysis() {
        //given
        KafkaEtlProperties props = new KafkaEtlProperties();
        props.setMaxRecordsToDisplay(10);
        JMXController controller = new JMXController(props);

        //when
        boolean enabled = controller.getEnableRecordAnalysis();
        controller.setEnableRecordAnalysis(true);
        boolean enabled2 = controller.getEnableRecordAnalysis();

        //then
        assertFalse(enabled);
        assertTrue(enabled2);

    }

    @Test
    public void getLongWriteThresholdMillis() {
        //given
        KafkaEtlProperties props = new KafkaEtlProperties();
        props.setLongWriteThreshold(Duration.of(1, ChronoUnit.SECONDS));
        JMXController controller = new JMXController(props);

        //when
        long writeThresholdMillis = controller.getLongWriteThresholdMillis();
        controller.setLongWriteThresholdMillis(10);
        long writeThresholdMillis2 = controller.getLongWriteThresholdMillis();

        //then
        assertEquals(1000, writeThresholdMillis);
        assertEquals(10, writeThresholdMillis2);

    }

}