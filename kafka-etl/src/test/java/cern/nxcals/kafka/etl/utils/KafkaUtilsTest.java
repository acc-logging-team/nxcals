package cern.nxcals.kafka.etl.utils;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.jupiter.api.Test;

import java.util.List;

import static cern.nxcals.kafka.etl.utils.KafkaUtils.splitIntoParts;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class KafkaUtilsTest {
    @Test
    void splitIntoPartsShouldCorrectlySplitListWithRecordsBySizeWhenSmallRecordsAreAtBeginning() {
        // given
        ConsumerRecord<Integer, Integer> smallRecord = mock(ConsumerRecord.class);
        when(smallRecord.serializedValueSize()).thenReturn(100);
        ConsumerRecord<Integer, Integer> bigRecord = mock(ConsumerRecord.class);
        when(bigRecord.serializedValueSize()).thenReturn(100_000_000);

        List<ConsumerRecord<Integer, Integer>> records = List.of(smallRecord, smallRecord, bigRecord, smallRecord,
                smallRecord);

        // when
        List<List<ConsumerRecord<Integer, Integer>>> partitionedList = splitIntoParts(records, 1000, 100);

        // then
        assertEquals(2, partitionedList.size());
        assertEquals(3, partitionedList.get(0).size());
        assertEquals(2, partitionedList.get(1).size());
    }
}
