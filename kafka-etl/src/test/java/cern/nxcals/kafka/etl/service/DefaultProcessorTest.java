package cern.nxcals.kafka.etl.service;

import cern.nxcals.common.metrics.MicrometerMetricsRegistry;
import cern.nxcals.kafka.etl.TestDataGenerator;
import cern.nxcals.kafka.etl.domain.PartitionInfo;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.common.TopicPartition;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.retry.support.RetryTemplate;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DefaultProcessorTest {

    private TestDataGenerator generator = new TestDataGenerator();

    @Mock
    private Partitioner<String, String> partitioner;

    @Mock
    private Store<String, String> store1;

    @Mock
    private Store<String, String> store2;

    @Mock
    private MeterRegistry meterRegistry;

    private Processor<String, String> processor;

    @BeforeEach
    public void initMocks() {
        MockitoAnnotations.initMocks(this);

        when(meterRegistry.counter(anyString())).thenReturn(mock(Counter.class));

        //default 3 times retry
        RetryTemplate retryTemplate = new RetryTemplate();

        processor = new DefaultProcessor<>(partitioner,
                Lists.newArrayList(store1, store2),
                Executors.newSingleThreadExecutor(), new MicrometerMetricsRegistry(meterRegistry), retryTemplate);
    }

    @Test
    public void shouldProcessCorrectData() throws IOException {
        processor.process(dataPoint(10, 20));

        verify(store1, times(10)).save(any(), any());
        verify(store2, times(10)).save(any(), any());

        verify(partitioner, times(200)).findPartition(any());
    }

    @Test
    public void shouldThrowOnWriterTerminatedException() throws IOException {
        doThrow(new WriterTerminatedException("")).when(store2).save(any(), any());

        assertThrows(WriterTerminatedException.class, () -> processor.process(dataPoint(10, 20)));
    }

    @Test
    public void shouldFailIfMoreThanRetriesOnProcess() throws IOException {
        doThrow(new IOException("expected exception"))
                .doThrow(new IOException("expected exception"))
                .doThrow(new IOException("expected exception"))
                .when(store1).save(any(), any());

        assertThrows(IllegalStateException.class, () -> processor.process(dataPoint(1, 20)));
    }

    @Test
    public void shouldFailIfMoreThanRetriesOnFlush() throws IOException {
        doThrow(new IOException("expected exception"))
                .doThrow(new IOException("expected exception"))
                .doThrow(new IOException("expected exception"))
                .when(store1).flush(any());

        assertThrows(UncheckedIOException.class, () -> processor.flush());
    }

    @Test
    public void shouldRecoverIfLessThanRetries() throws IOException {
        doThrow(new IOException("expected exception"))
                .doThrow(new IOException("expected exception"))
                .doNothing()
                .when(store1).save(any(), any());

        processor.process(dataPoint(1, 20));
    }

    @Test
    public void shouldFlushEveryStore() throws IOException {
        processor.process(dataPoint(10, 20));
        processor.flush();

        verify(store1, times(1)).flush(any());
        verify(store2, times(1)).flush(any());
    }

    @Test
    public void shouldIgnoreEmptyData() throws IOException {
        processor.process(dataPoint(100, 0));

        verify(store1, never()).flush(any());
        verify(store2, never()).flush(any());

        verify(store1, never()).save(any(), any());
        verify(store2, never()).save(any(), any());

        verify(partitioner, never()).findPartition(any());
    }

    private ConsumerRecords<String, String> dataPoint(int partitionCount, int rowsPerPartition) {
        Map<TopicPartition, List<ConsumerRecord<String, String>>> map = new HashMap<>();

        for (int i = 0; i < partitionCount; i++) {
            List<ConsumerRecord<String, String>> records = generator.randomData(rowsPerPartition);
            map.put(new TopicPartition("TOPIC_" + i / 3, i % 3), records);

            PartitionInfo partitionInfo = PartitionInfo.from(i, i, i, 1000 * i, false);

            for (ConsumerRecord<String, String> record : records) {
                when(partitioner.findPartition(record)).thenReturn(partitionInfo);
            }
        }

        return new ConsumerRecords<>(map);
    }

}
