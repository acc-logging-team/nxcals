package cern.nxcals.kafka.etl.service;

import cern.nxcals.api.domain.Partition;
import cern.nxcals.api.domain.PartitionProperties;
import cern.nxcals.api.domain.RecordKey;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.internal.extraction.metadata.InternalPartitionService;
import cern.nxcals.kafka.etl.domain.PartitionInfo;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.Optional;
import java.util.stream.Stream;

import static cern.nxcals.api.utils.TimeUtils.TimeSplits.FIVE_MINUTE_SPLIT;
import static cern.nxcals.api.utils.TimeUtils.TimeSplits.ONE_HOUR_SPLIT;
import static cern.nxcals.api.utils.TimeUtils.TimeSplits.TEN_MINUTE_SPLIT;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@ExtendWith({ MockitoExtension.class })
@MockitoSettings(strictness = Strictness.LENIENT)
public class DefaultPartitionerTest {
    @Mock
    ConsumerRecord<byte[],byte[]> record;

    @Mock
    private InternalPartitionService partitionService;

    @Mock
    private Partition partition;

    @Mock
    private PartitionProperties partitionProperties;

    @Test
    public void shouldReturnInvalidPartitionForBrokenKeys() {
        DefaultPartitioner partitioner = new DefaultPartitioner(TimeUtils.TimeSplits.FIVE_MINUTE_SPLIT,
                TimeUtils.getNanosFromString("2023-01-01 00:00:00.000"),
                partitionService);

        when(record.key()).thenReturn(new byte[] {1,2,3});
        PartitionInfo partition = partitioner.findPartition(record);

        assertEquals(PartitionInfo.INVALID_PARTITION,partition);
    }

    @ParameterizedTest
    @MethodSource("valuesForKeys")
    public void shouldReturnValidPartitionForValidKeys(TimeUtils.TimeSplits defaultSplits,
            TimeUtils.TimeSplits explicitSplits, TimeWindow timeWindow, long timestamp,
            TimeUtils.TimeSplits outcomeSplits) {
        DefaultPartitioner partitioner = new DefaultPartitioner(defaultSplits,
                0,
                partitionService);

        byte[] key = RecordKey.serialize(1L, 1L, 1L, 1L, timestamp);
        when(record.key()).thenReturn(key);
        when(partitionService.findById(1L)).thenReturn(Optional.of(partition));
        when(partition.getProperties()).thenReturn(partitionProperties);
        when(partitionProperties.getStagingPartitionType()).thenReturn(explicitSplits);
        if (explicitSplits != null) {
            when(partitionProperties.getStagingPartitionTypeValidity()).thenReturn(timeWindow);
        }
        PartitionInfo partition = partitioner.findPartition(record);

        assertNotNull(partition);
        assertEquals(TimeUtils.getTimePartition(timestamp, outcomeSplits), partition.getTimePartition());
    }

    private static Stream<Arguments> valuesForKeys() {
        return Stream.of(
                Arguments.of(ONE_HOUR_SPLIT, null, null, 10L, ONE_HOUR_SPLIT),
                Arguments.of(ONE_HOUR_SPLIT, FIVE_MINUTE_SPLIT, TimeWindow.between(1, 20), 10L, FIVE_MINUTE_SPLIT),
                Arguments.of(ONE_HOUR_SPLIT, FIVE_MINUTE_SPLIT, TimeWindow.between(1, 20), 21L, ONE_HOUR_SPLIT),
                Arguments.of(ONE_HOUR_SPLIT, TEN_MINUTE_SPLIT, null, 10L, ONE_HOUR_SPLIT),
                Arguments.of(ONE_HOUR_SPLIT, TEN_MINUTE_SPLIT, TimeWindow.between(1, 20), 20L, ONE_HOUR_SPLIT),
                Arguments.of(ONE_HOUR_SPLIT, TEN_MINUTE_SPLIT, TimeWindow.between(1, 20), 21L, ONE_HOUR_SPLIT),
                Arguments.of(ONE_HOUR_SPLIT, TEN_MINUTE_SPLIT, TimeWindow.between(null, null), 20L, TEN_MINUTE_SPLIT),
                Arguments.of(ONE_HOUR_SPLIT, TEN_MINUTE_SPLIT,
                        TimeWindow.between(TimeUtils.getInstantFromNanos(1L), null), 20L, TEN_MINUTE_SPLIT)
        );

    }

}