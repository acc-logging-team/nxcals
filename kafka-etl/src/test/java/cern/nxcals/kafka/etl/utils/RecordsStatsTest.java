package cern.nxcals.kafka.etl.utils;

import cern.nxcals.api.domain.RecordKey;
import cern.nxcals.kafka.etl.utils.RecordsStats.EntityStats;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Comparator.comparingDouble;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class RecordsStatsTest {

    @Test
    public void from() {
        //given
        List<ConsumerRecord<byte[], byte[]>> records = createRecords();

        //when
        List<EntityStats> entityStats = RecordsStats
                .from(records)
                .sorted(comparingDouble(EntityStats::getEntityId)).collect(
                        Collectors.toList());

        System.out.println(entityStats);

        //then
        assertEquals(5, entityStats.size());
        assertEquals(1L, entityStats.get(0).getEntityId());
        assertEquals(40, entityStats.get(0).getTotalSize());
        assertEquals(10, entityStats.get(0).getBytesPerSecond(), 0.0001);
        assertEquals(1d, entityStats.get(0).getRecPerSecond(), 0.00001);

        assertEquals(2L, entityStats.get(1).getEntityId());
        assertEquals(60, entityStats.get(1).getTotalSize());
        assertEquals(3, entityStats.get(1).getRecCount());

        assertEquals(1, entityStats.get(2).getRecCount());
        assertEquals(0.0, entityStats.get(2).getBytesPerSecond(), 0.00001);

        assertEquals(300, entityStats.get(3).getBytesPerSecond(), 0.0001);

        assertEquals(2d, entityStats.get(4).getRecPerSecond(), 0.0001);

    }

    private List<ConsumerRecord<byte[], byte[]>> createRecords() {
        List<ConsumerRecord<byte[], byte[]>> ret = new ArrayList<>();

        ret.add(createRecord(1, new byte[10], 1_000_000_000L));
        ret.add(createRecord(1, new byte[10], 2_000_000_000L));
        ret.add(createRecord(1, new byte[10], 3_000_000_000L));
        ret.add(createRecord(1, new byte[10], 4_000_000_000L));

        ret.add(createRecord(2, new byte[10], 1_000_000_000L));
        ret.add(createRecord(2, new byte[20], 2_000_000_000L));
        ret.add(createRecord(2, new byte[30], 3_000_000_000L));

        ret.add(createRecord(3, new byte[200], 1_000_000_000L));

        ret.add(createRecord(4, new byte[200], 1_000_000_000L));
        ret.add(createRecord(4, new byte[400], 2_000_000_000L));

        ret.add(createRecord(5, new byte[200], 1_000_000_000L));
        ret.add(createRecord(5, new byte[200], 1_300_000_000L));
        ret.add(createRecord(5, new byte[400], 2_000_000_000L));
        ret.add(createRecord(5, new byte[400], 2_500_000_000L));

        return ret;

    }

    private ConsumerRecord<byte[], byte[]> createRecord(long entityId, byte[] value, long timestamp) {
        return new ConsumerRecord<byte[], byte[]>("T1", 0, 0, RecordKey.serialize(0, 0, 0, entityId, timestamp), value);
    }
}