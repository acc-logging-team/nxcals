package cern.nxcals.kafka.etl;

import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.BinaryEncoder;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.io.Decoder;
import org.apache.avro.io.DecoderFactory;
import org.apache.avro.io.EncoderFactory;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestDataGenerator {
    private Random random = new Random(System.currentTimeMillis());

    public long daysNanos(int count) {
        return TimeUnit.DAYS.toNanos(count);
    }

    public long nowNanos() {
        return Instant.now().toEpochMilli() * 1_000_000;
    }

    public List<ConsumerRecord<String, String>> randomData(int size) {

        List<ConsumerRecord<String, String>> result = new ArrayList<>();

        for (int i = 0; i < size; i++) {
            ConsumerRecord<String, String> record = mock(ConsumerRecord.class);
            when(record.key()).thenReturn("KEY_" + i);
            when(record.value()).thenReturn("VALUE_" + i);
            result.add(record);
        }

        return result;
    }

    public List<ConsumerRecord<byte[], byte[]>> randomData(int size, int byteOffset, Schema schema) {

        List<ConsumerRecord<byte[], byte[]>> result = new ArrayList<>();

        for (int i = 0; i < size; i++) {
            ConsumerRecord<byte[], byte[]> record = mock(ConsumerRecord.class);
            byte[] encode = encode(randomRecord(schema), schema);
            byte[] finalEncode = new byte[encode.length + byteOffset];

            System.arraycopy(encode, 0, finalEncode, byteOffset, encode.length);

            when(record.value()).thenReturn(finalEncode);
            result.add(record);
        }

        return result;
    }

    private GenericRecord randomRecord(Schema schema) {
        GenericRecord datum = new GenericData.Record(schema);
        for (Schema.Field field : schema.getFields()) {
            // timestamp should be positive, otherwise an exception is thrown on Put creation
            if (field.name().equals("timestamp")) {
                datum.put(field.name(), Math.abs(random.nextLong()));
            } else {
                datum.put(field.name(), randomValue(field.schema().getType()));
            }
        }
        return datum;
    }

    private Object randomValue(Schema.Type type) {
        switch (type) {
            case STRING:
                return randomString();
            case BYTES:
                return ByteBuffer.wrap(new byte[]{Long.valueOf(random.nextInt()).byteValue()});
            case INT:
                return random.nextInt();
            case LONG:
                return random.nextLong();
            case FLOAT:
                return random.nextFloat();
            case DOUBLE:
                return random.nextDouble();
            case BOOLEAN:
                return random.nextBoolean();
            default:
                return null;
        }

    }

    private String randomString() {
        byte[] array = new byte[7]; // length is bounded by 7
        random.nextBytes(array);
        return new String(array, Charset.forName("UTF-8"));
    }

    private byte[] encode(GenericRecord datum, Schema schema) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            DatumWriter<GenericRecord> writer = new GenericDatumWriter<GenericRecord>(schema);
            BinaryEncoder encoder = EncoderFactory.get().directBinaryEncoder(out, null);
            writer.write(datum, encoder);
            encoder.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return out.toByteArray();
    }

    private GenericRecord decode(byte[] in, Schema schema) throws IOException {
        DatumReader<GenericRecord> reader = new GenericDatumReader<GenericRecord>(schema);
        Decoder decoder = DecoderFactory.get().binaryDecoder(in, null);
        return reader.read(null, decoder);
    }
}
