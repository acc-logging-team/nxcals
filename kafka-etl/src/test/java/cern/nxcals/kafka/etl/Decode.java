package cern.nxcals.kafka.etl;

public class Decode {
    public static String schema1 = "{\"type\":\"record\",\"name\":\"data0\",\"namespace\":\"cern.nxcals\",\"fields\":[{\"name\":\"__sys_nxcals_system_id__\",\"type\":\"long\"},{\"name\":\"__sys_nxcals_entity_id__\",\"type\":\"long\"},{\"name\":\"__sys_nxcals_partition_id__\",\"type\":\"long\"}," +
    "{\"name\":\"__sys_nxcals_schema_id__\",\"type\":\"long\"},{\"name\":\"__sys_nxcals_timestamp__\",\"type\":\"long\"},{\"name\":\"__record_timestamp__\",\"type\":\"long\"},{\"name\":\"__record_version__\",\"type\":\"long\"}," +
    "{\"name\":\"acqStamp\",\"type\":[\"long\",\"null\"]},{\"name\":\"class\",\"type\":\"string\"},{\"name\":\"cyclestamp\",\"type\":[\"long\",\"null\"]},{\"name\":\"device\",\"type\":\"string\"},{\"name\":\"property\",\"type\":\"string\"}," +
    "{\"name\":\"selector\",\"type\":[\"string\",\"null\"]},{\"name\":\"value\",\"type\":[\"double\",\"null\"]}]}";


    public static String schema2 = "{\"type\":\"record\",\"name\":\"data0\",\"namespace\":\"cern.nxcals\",\"fields\":[{\"name\":\"__sys_nxcals_system_id__\",\"type\":\"long\"},{\"name\":\"__sys_nxcals_entity_id__\",\"type\":\"long\"},{\"name\":\"__sys_nxcals_partition_id__\",\"type\":\"long\"},\n"
            + "{\"name\":\"__sys_nxcals_schema_id__\",\"type\":\"long\"},{\"name\":\"__sys_nxcals_timestamp__\",\"type\":\"long\"},{\"name\":\"__record_timestamp__\",\"type\":\"long\"},{\"name\":\"__record_version__\",\"type\":\"long\"},\n"
            + "{\"name\":\"acqStamp\",\"type\":[\"long\",\"null\"]},{\"name\":\"class\",\"type\":\"string\"},{\"name\":\"crate\",\"type\":[\"string\",\"null\"]},{\"name\":\"cyclestamp\",\"type\":[\"long\",\"null\"]},{\"name\":\"device\",\"type\":\"string\"},\n"
            + "{\"name\":\"property\",\"type\":\"string\"},{\"name\":\"selector\",\"type\":[\"string\",\"null\"]},{\"name\":\"value\",\"type\":[\"double\",\"null\"]}]}";

//    static public void main(String [] args) throws Exception {
//        byte[] arr = new byte[] {4, -96, -11, -60, 1, -46, -20, 1, -14,
//                -37, 1, -128, -48, -12, -25, -47, -117, -10, -115, 43, -128, -76, -58, -70, -49, -117, -10, -115, 43, 0, 0, -128, -76, -58, -70, -49, -117, -10, -115, 43, 12, 81, 80, 83, 46, 77, 81, 0, -128, -112, -97, -32, -56, -117, -10, -115, 43, 14, 77, 81, 46, 49, 57, 76, 49, 14, 85, 95, 49, 95, 73, 78, 84, 0, 24, 76, 72, 67, 46, 85, 83, 69, 82, 46, 76, 72, 67, 0, 0, 0, 0, -64, -100, 113, 11, -64};
//
//        Schema schema = new Schema.Parser().parse(schema2);
//        DatumReader<GenericRecord> reader = new GenericDatumReader<>(schema);
//        Decoder decoder = DecoderFactory.get().binaryDecoder(arr, 0, arr.length, null);
//        reader.read(null, decoder);
//
//    }

    //decode broken key
    public static void main(String[] args) {
        byte[] key = new byte[] {49, 55, 56, 51, 54, 48};

        String str = new String(key);
        System.out.println(str);
    }




}
