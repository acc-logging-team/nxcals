/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.kafka.etl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class ApplicationDev {
    static {
        System.setProperty("org.apache.logging.log4j.simplelog.StatusLogger.level", "TRACE");
        System.setProperty("logging.config", "classpath:kafka-etl-log4j2.yml");
        String user = System.getProperty("user.name");
        //Kerberos is a must!
        System.setProperty("kerberos.keytab", "/opt/" + user + "/acclog.keytab");
        System.setProperty("kerberos.principal", "acclog");

        //User env
        System.setProperty("service.url",
                "http://nxcals-" + user + "1.cern.ch:19093,http://nxcals-" + user + "2.cern.ch:19093,http://nxcals-"
                        + user + "3.cern.ch:19093");
        System.setProperty("nxcals.kafka.etl.consumer-properties.bootstrap.servers",
                "nxcals-" + user + "3.cern.ch:9092");
        System.setProperty("nxcals.kafka.etl.hbase.namespace", "nxcals_dev_" + user);
        System.setProperty("nxcals.kafka.etl.hdfs.base-path",
                "/project/nxcals/nxcals_dev_" + user + "/staging_test/ETL1");
        //TEST Env
        //        System.setProperty("service.url", "https://nxcals-test4.cern.ch:19093,https://nxcals-test5.cern.ch:19093,https://nxcals-test6.cern.ch:19093");
        //TESTBED

        //        System.setProperty("service.url", "https://cs-ccr-testbed2.cern.ch:19093,https://cs-ccr-testbed3.cern.ch:19093,https://cs-ccr-nxcalstbs1.cern.ch:19093");
        //        PRO
        //        System.setProperty("nxcals.kafka.etl.consumer-properties.bootstrap.servers",
        //                "cs-ccr-nxcalsstr4.cern.ch:9092,cs-ccr-nxcalsstr5.cern.ch:9092,cs-ccr-nxcalsstr6.cern.ch:9092,cs-ccr-nxcalsstr7.cern.ch:9092");
        //        System.setProperty("service.url",
        //                "https://cs-ccr-nxcals6.cern.ch:19093,https://cs-ccr-nxcals7.cern.ch:19093,https://cs-ccr-nxcals8.cern.ch:19093");
        //        System.setProperty("nxcals.kafka.etl.hdfs.base-path",
        //                "/project/nxcals/nxcals_pro/staging_test_to_be_deleted/ETL1-jwozniak");
    }

    public static void main(String[] args) throws Exception {
        try {
            Application.main(args);
        } catch (Throwable e) {
            e.printStackTrace();
            log.error("Exception while running elt dev app", e);
            System.exit(1);
        }
    }

}
