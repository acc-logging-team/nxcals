package cern.nxcals.kafka.etl.hbase;

import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.internal.extraction.metadata.InternalSystemSpecService;
import org.apache.kafka.common.utils.Bytes;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static cern.nxcals.common.Constants.HBASE_ROW_KEY_DELIMITER;
import static cern.nxcals.kafka.etl.config.HbaseProperties.NO_SALT_BUCKETS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
class RecordConverterTest {

    @Mock
    private InternalSystemSpecService systemService;
    public static final int RECORDS_NUMBER = 100;

    @Test
    void shouldGenerateDifferentBuckets() {
        //given
        int saltBuckets = 4;
        int oldSaltBuckets = 10;
        String newSaltingSince = "2022-03-30 08:00:00";
        String datetimeForTestData = "2022-03-31 08:00:00";
        RecordConverter recordConverter = new RecordConverter(Bytes.EMPTY, systemService, saltBuckets, oldSaltBuckets,
                newSaltingSince);
        //when
        List<String> generatedBuckets = generateBuckets(datetimeForTestData, recordConverter, RECORDS_NUMBER);
        //then
        Set<Long> uniqueBuckets = extractUniqueBuckets(generatedBuckets);
        assertTrue(uniqueBuckets.size() > 1);
        assertTrue(uniqueBuckets.size() <= saltBuckets);
    }

    @Test
    void shouldGenerateOneBucket() {
        //given
        RecordConverter recordConverter = new RecordConverter(Bytes.EMPTY, systemService, 1, 1, "1970-01-01 08:00:00");
        String datetimeForTestData = "2022-03-31 08:00:00";
        //when
        List<String> generatedBuckets = generateBuckets(datetimeForTestData, recordConverter, RECORDS_NUMBER);
        //then
        Set<Long> uniqueBuckets = extractUniqueBuckets(generatedBuckets);
        assertEquals(1, uniqueBuckets.size());
    }

    @Test
    void shouldGenerateNoSalt() {
        //given
        RecordConverter recordConverter = new RecordConverter(Bytes.EMPTY, systemService, NO_SALT_BUCKETS, 1,
                "1970-01-01 00:00:00");
        //when
        String salt = recordConverter.getSaltingBasedOnTimestamp(1L);
        //then
        assertTrue(salt.isEmpty());
    }

    @Test
    void shouldGenerateBucketsWithOldAndNewSalt() {
        //given
        int saltBuckets = 4;
        int oldSaltBuckets = 10;
        String newSaltingSince = "2022-03-30 08:00:00";
        String datetimeForDataWithOldSalt = "2022-03-30 07:00:00";
        String datetimeForDataWithNewSalt = "2022-03-30 09:00:00";
        RecordConverter recordConverter = new RecordConverter(Bytes.EMPTY, systemService, saltBuckets, oldSaltBuckets,
                newSaltingSince);
        int recordsNumber = 100;

        //when
        List<String> generatedOldBuckets = generateBuckets(datetimeForDataWithOldSalt, recordConverter, recordsNumber);
        List<String> generatedNewBuckets = generateBuckets(datetimeForDataWithNewSalt, recordConverter, recordsNumber);

        //then
        Set<Long> uniqueOldBuckets = extractUniqueBuckets(generatedOldBuckets);
        assertTrue(uniqueOldBuckets.size() > saltBuckets);
        assertTrue(uniqueOldBuckets.size() <= oldSaltBuckets);

        Set<Long> uniqueNewBuckets = extractUniqueBuckets(generatedNewBuckets);
        assertTrue(uniqueNewBuckets.size() > 1);
        assertTrue(uniqueNewBuckets.size() <= saltBuckets);
    }

    private List<String> generateBuckets(String datetimeForTestData, RecordConverter recordConverter,
            int recordsNumber) {
        List<String> generatedBuckets = new ArrayList<>(recordsNumber);
        long nanos = TimeUtils.getNanosFromString(datetimeForTestData);
        for (int i = 0; i < recordsNumber; i++) {
            long recordNanos = nanos + i;
            generatedBuckets.add(recordConverter.getSaltingBasedOnTimestamp(recordNanos));
        }
        return generatedBuckets;
    }

    private Set<Long> extractUniqueBuckets(List<String> generatedBuckets) {
        return generatedBuckets.stream()
                .map(s -> Long.parseLong(s.split(HBASE_ROW_KEY_DELIMITER)[0])).collect(Collectors.toSet());
    }

}