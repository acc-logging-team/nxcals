package cern.nxcals.kafka.etl.hdfs;

import cern.nxcals.kafka.etl.service.WriterTerminatedException;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.ByteBuffer;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class HdfsFileWriterTest {
    private static final Path BASE_PATH = new Path("/base/");

    @Mock
    private AvroOutputStreamWriter streamWriter;

    @Mock
    private FileSystem fs;

    private HdfsFileWriter writer;

    @BeforeEach
    public void initMocks() throws IOException {
        MockitoAnnotations.initMocks(this);

        writer = spy(new HdfsFileWriter(fs, BASE_PATH, null, "codec", 512000, 128000));

        doReturn(streamWriter).when(writer).newStreamWriter();
        doReturn(mock(FSDataOutputStream.class)).when(fs).create(any(), eq(true), anyInt());
    }

    @Test
    public void shouldWriteSuccessfully() throws IOException {
        byte[] data = ByteBuffer.allocate(4).putInt(1234567).array();

        assertFalse(writer.isTerminated());

        writer.write(data);

        assertFalse(writer.isTerminated());
        assertTrue(writer.getFullPath().toString().startsWith(BASE_PATH.toString()));

        verify(fs, times(1)).mkdirs(BASE_PATH);
        verify(streamWriter, times(1)).write(data);
    }

    @Test
    public void shouldFailOnWriteFail() throws IOException {
        byte[] data = ByteBuffer.allocate(4).putInt(1234567).array();

        doThrow(new IOException()).when(streamWriter).write(data);

        assertFalse(writer.isTerminated());
        assertThrows(UncheckedIOException.class, () -> writer.write(data));
    }

    @Test
    public void shouldTerminate() throws IOException {
        assertFalse(writer.isTerminated());

        writer.terminate();

        assertTrue(writer.isTerminated());
    }

    @Test
    public void shouldTerminateAfterWrite() throws IOException {
        byte[] data = ByteBuffer.allocate(4).putInt(1234567).array();

        writer.write(data);

        assertFalse(writer.isTerminated());

        writer.terminate();

        assertTrue(writer.isTerminated());
    }

    @Test
    public void shouldFailOnTerminateAfterWrite() throws IOException {
        byte[] data = ByteBuffer.allocate(4).putInt(1234567).array();

        assertFalse(writer.isTerminated());

        writer.terminate();

        assertTrue(writer.isTerminated());
        assertThrows(WriterTerminatedException.class, () -> writer.write(data));
    }

    @Test
    public void shouldConsumeStreamClosingException() throws IOException {
        byte[] data = ByteBuffer.allocate(4).putInt(1234567).array();

        doThrow(new IOException()).when(streamWriter).close();

        writer.write(data);

        assertThrows(IllegalStateException.class, () -> writer.terminate());
    }

    @Test
    public void shouldFailOnFileCreateException() throws IOException {
        byte[] data = ByteBuffer.allocate(4).putInt(1234567).array();

        doThrow(new IOException()).when(fs).create(any(), eq(true), anyInt());

        assertThrows(UncheckedIOException.class, () -> writer.write(data));
    }
}