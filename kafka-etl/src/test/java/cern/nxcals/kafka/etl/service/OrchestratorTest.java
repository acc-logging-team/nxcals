package cern.nxcals.kafka.etl.service;

import cern.nxcals.common.metrics.MetricsRegistry;
import cern.nxcals.kafka.etl.config.KafkaEtlProperties;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Supplier;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class OrchestratorTest {
    private static final int COUNT = 5;

    @Mock
    private ScheduledExecutorService scheduledExecutorService;

    @Mock
    private Supplier<KafkaConsumerRunner<byte[], byte[]>> kafkaConsumerRunnerSupplier;

    @Mock
    private KafkaConsumerRunner<byte[], byte[]> runner;

    @Mock
    private MetricsRegistry metricsRegistry;

    @Test
    public void shouldStart() {
        //given
        Orchestrator<byte[], byte[]> orchestrator = createOrchestator(scheduledExecutorService, COUNT);
        //when
        orchestrator.start();

        //then
        verify(scheduledExecutorService, Mockito.times(COUNT)).execute(any());
    }

    private Orchestrator<byte[], byte[]> createOrchestator(ScheduledExecutorService service, int count) {
        KafkaEtlProperties props = new KafkaEtlProperties();
        props.setConcurrency(count);
        props.setRescheduleDelay(0);
        props.setRescheduleDelayUnit(TimeUnit.SECONDS);
        return new Orchestrator<>(service, props, metricsRegistry, kafkaConsumerRunnerSupplier);
    }

    @Test
    @Disabled("fix in NXCALS-2438")
    public void shouldShutdown() throws IOException, InterruptedException {
        //given
        Orchestrator<byte[], byte[]> orchestrator = createOrchestator(Executors.newScheduledThreadPool(10), 1);
        orchestrator.start();

        CountDownLatch closeLatch = new CountDownLatch(1);
        CountDownLatch runningLatch = new CountDownLatch(1);

        //Has to wait in the run method, otherwise the code will exit from run and remove the runner from the set in the Orchestrator
        //The method run is supposed to run forever or throw, if it exits with no exception it will not be repeated (only happens on shutdown).
        doAnswer(invocation -> {
            System.out.println("Running");
            runningLatch.countDown();
            closeLatch.await(2, TimeUnit.MINUTES);
            return null;
        }).when(runner).run();

        //when
        runningLatch.await(2, TimeUnit.MINUTES);
        orchestrator.shutdown();
        closeLatch.countDown();//let the run method finish...
        //then
        verify(runner).close();
    }

    @Test
    public void shouldRestartRunner() throws InterruptedException {
        //given
        CountDownLatch latch = new CountDownLatch(1);
        when(kafkaConsumerRunnerSupplier.get()).thenReturn(runner);

        AtomicBoolean isFirstInvocation = new AtomicBoolean(true);
        doAnswer((invocation) -> {
            if (isFirstInvocation.compareAndSet(true, false)) {
                throw new RuntimeException("test exception");
            } else {
                latch.countDown();
            }
            return null;
        }).when(runner).run();

        Orchestrator<byte[], byte[]> orchestrator = createOrchestator(Executors.newScheduledThreadPool(2), 1);

        //when
        orchestrator.start();

        latch.await(2, TimeUnit.MINUTES);

        //then
        verify(runner, atLeast(2)).run();
        verify(metricsRegistry).incrementCounterBy(Orchestrator.RESCHEDULE_COUNT_METRIC_NAME, 1);

    }

}
