package cern.nxcals.kafka.etl.hbase;

import cern.nxcals.api.domain.EntitySchema;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.internal.extraction.metadata.InternalEntitySchemaService;
import cern.nxcals.internal.extraction.metadata.InternalSystemSpecService;
import cern.nxcals.kafka.etl.TestDataGenerator;
import cern.nxcals.kafka.etl.config.HbaseProperties;
import cern.nxcals.kafka.etl.config.KafkaEtlProperties;
import cern.nxcals.kafka.etl.domain.PartitionInfo;
import cern.nxcals.kafka.etl.service.JMXController;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import org.apache.avro.Schema;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.TableNotFoundException;
import org.apache.hadoop.hbase.client.BufferedMutator;
import org.apache.hadoop.hbase.client.BufferedMutatorParams;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Put;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutorService;

import static cern.nxcals.common.Schemas.ENTITY_ID;
import static cern.nxcals.kafka.etl.config.HbaseProperties.DEFAULT_WRITER_NAME;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@ExtendWith(MockitoExtension.class)
public class HbaseStoreTest {
    private final String schemaString = "{"
            + "\"type\": \"record\", "
            + "\"name\": \"test_pair\","
            + "\"fields\":"
            + "["
            + "{\"name\": \"timestamp\", \"type\": \"long\"},"
            + "{\"name\": \"" + ENTITY_ID.getFieldName() + "\", \"type\": \"string\"},"
            + "{\"name\": \"mybyte\", \"type\": \"bytes\"}"
            + "]"
            + "}";

    private final Schema schema = new Schema.Parser().parse(schemaString);

    @Mock
    private Connection connectionMock;

    @Mock
    private ConnectionSupplier connectionSupplier;

    private Map<String, ConnectionSupplier> connectionPools;

    @Mock
    private InternalSystemSpecService systemServiceMock;

    @Mock
    private InternalEntitySchemaService schemaService;

    @Mock
    private BufferedMutator mutator;

    @Mock
    private SystemSpec systemData;

    @Mock
    private HbaseTableProvider provider;

    @Mock
    private EntitySchema schemaData;

    private RecordConverter recordConverter;
    private TestDataGenerator generator = new TestDataGenerator();

    @Mock
    private Map<String, ExecutorService> threadPools;

    @BeforeEach
    public void setUp() {
        connectionPools = ImmutableMap.of("MY_POOL", connectionSupplier);
        this.recordConverter = new RecordConverter("".getBytes(), systemServiceMock, 4, 10, "2022-03-30 08:00:00");
    }

    @Nested
    class HbaseStoreTestNested {

        @BeforeEach
        public void init() {
            when(systemServiceMock.findOne(any())).thenReturn(Optional.of(systemData));
            when(systemData.getTimeKeyDefinitions()).thenReturn(schemaString);
            when(systemData.getName()).thenReturn("CMW");
            when(schemaService.findOne(any())).thenReturn(Optional.of(schemaData));
            when(schemaData.getSchemaJson()).thenReturn(schemaString);
            when(connectionSupplier.get()).thenReturn(connectionMock);
        }

        @Test
        public void shouldDeserializeAndStoreNewData() throws IOException {
            HbaseStore hbaseStore = getHbaseStore(connectionPools, provider);

            testDeserializeAndStoreNewData(hbaseStore);
        }

        @Test
        public void shouldDeserializeAndStoreNewDataWithEnabledJMXLogging() throws IOException {
            HbaseStore hbaseStore = getHbaseStore(connectionPools, provider, true);

            testDeserializeAndStoreNewData(hbaseStore);
        }

        private void testDeserializeAndStoreNewData(HbaseStore hbaseStore) throws IOException {
            int size = 10;
            PartitionInfo partitionInfo = PartitionInfo.from(1, 2, 3, generator.nowNanos(), false);

            TableName tableName = TableName.valueOf("data", "1__2__3");

            when(provider.tableFor(partitionInfo)).thenReturn(tableName);
            when(connectionMock.getBufferedMutator(any(BufferedMutatorParams.class))).thenReturn(mutator);

            ArgumentCaptor<List> argumentCaptor = ArgumentCaptor.forClass(List.class);

            hbaseStore.save(partitionInfo, generator.randomData(size, 0, schema));

            verify(connectionMock, times(1)).getBufferedMutator(any(BufferedMutatorParams.class));
            verify(mutator, times(1)).mutate(argumentCaptor.capture());
            hbaseStore.flush(Sets.newHashSet(partitionInfo));
            verify(mutator, times(1)).flush();

            List<Put> list = (List<Put>) argumentCaptor.getValue();

            for (Put put : list) {
                assertTrue(put.toJSON().contains("__0"));
            }
        }

        @Test
        public void shouldFailOnFailedMutations() throws IOException {
            HbaseStore hbaseStore = getHbaseStore(connectionPools, provider);

            int size = 10;
            PartitionInfo partitionInfo = PartitionInfo.from(1, 2, 3, generator.nowNanos(), false);

            TableName tableName = TableName.valueOf("data", "1__2__3");

            doThrow(new IOException("This is an intentional exception for testing!")).when(mutator)
                    .mutate(any(List.class));
            when(provider.tableFor(partitionInfo)).thenReturn(tableName);
            when(connectionMock.getBufferedMutator(any(BufferedMutatorParams.class))).thenReturn(mutator);

            assertThrows(IOException.class,
                    () -> hbaseStore.save(partitionInfo, generator.randomData(size, 0, schema)));
            hbaseStore.flush(Sets.newHashSet(partitionInfo));
        }

        @Test
        public void shouldFailOnFailedFlush() throws IOException {
            HbaseStore hbaseStore = getHbaseStore(connectionPools, provider);

            int size = 10;
            PartitionInfo partitionInfo = PartitionInfo.from(1, 2, 3, generator.nowNanos(), false);

            TableName tableName = TableName.valueOf("data", "1__2__3");

            doThrow(new IOException()).when(mutator).flush();
            when(provider.tableFor(partitionInfo)).thenReturn(tableName);
            when(connectionMock.getBufferedMutator(any(BufferedMutatorParams.class))).thenReturn(mutator);

            hbaseStore.save(partitionInfo, generator.randomData(size, 0, schema));

            assertThrows(IOException.class, () -> hbaseStore.flush(Sets.newHashSet(partitionInfo)));
        }

        @Test
        public void shouldNotFailOnFailedClose() throws IOException {

            HbaseStore hbaseStore = getHbaseStore(connectionPools, provider);

            int size = 10;
            PartitionInfo partitionInfo = PartitionInfo.from(1, 2, 3, generator.nowNanos(), false);

            TableName tableName = TableName.valueOf("data", "1__2__3");

            when(provider.tableFor(partitionInfo)).thenReturn(tableName);
            when(connectionMock.getBufferedMutator(any(BufferedMutatorParams.class))).thenReturn(mutator);

            hbaseStore.save(partitionInfo, generator.randomData(size, 0, schema));
            hbaseStore.close();
        }

        @Test
        public void shouldRecreateTableIfNotFound() throws IOException {
            HbaseStore hbaseStore = getHbaseStore(connectionPools, provider);

            int size = 10;
            PartitionInfo partitionInfo = PartitionInfo.from(1, 2, 3, generator.nowNanos(), false);

            TableName tableName = TableName.valueOf("data", "1__2__3");

            doThrow(new TableNotFoundException()).doNothing().when(mutator).mutate(anyList());

            when(provider.tableFor(partitionInfo)).thenReturn(tableName);
            when(connectionMock.getBufferedMutator(any(BufferedMutatorParams.class))).thenReturn(mutator);

            hbaseStore.save(partitionInfo, generator.randomData(size, 0, schema));
            hbaseStore.close();
        }

        @Test
        public void shouldRecreateTableIfNotFoundWhenFlushing() throws IOException {
            HbaseStore hbaseStore = getHbaseStore(connectionPools, provider);

            int size = 10;
            PartitionInfo partitionInfo = PartitionInfo.from(1, 2, 3, generator.nowNanos(), false);

            TableName tableName = TableName.valueOf("data", "1__2__3");

            doThrow(new TableNotFoundException()).doNothing().when(mutator).flush();
            when(provider.tableFor(partitionInfo)).thenReturn(tableName);
            when(connectionMock.getBufferedMutator(any(BufferedMutatorParams.class))).thenReturn(mutator);

            hbaseStore.save(partitionInfo, generator.randomData(size, 0, schema));

            hbaseStore.flush(Sets.newHashSet(partitionInfo));

            verify(mutator, times(2)).flush();
        }

    }

    @Test
    public void shouldFailOnNullConnection() {
        assertThrows(NullPointerException.class, () -> getHbaseStore(null, provider));
    }

    @Test
    public void shouldFailOnNullProvider() {
        assertThrows(NullPointerException.class, () -> getHbaseStore(connectionPools, null));
    }

    @Test
    public void shouldAcceptNullMetrics() {
        getHbaseStore(connectionPools, provider);
    }

    @Test
    public void shouldDestroy() {
        HbaseStore hbaseStore = getHbaseStore(connectionPools, provider);
        hbaseStore.close();
    }

    @Test
    public void shouldIgnoreOldData() throws IOException {
        HbaseStore hbaseStore = getHbaseStore(connectionPools, provider);

        PartitionInfo partitionInfo = PartitionInfo
                .from(1, 2, 3, generator.nowNanos() - generator.daysNanos(10), false);

        hbaseStore.save(partitionInfo, null);

        verify(connectionMock, never()).getBufferedMutator(any(TableName.class));
        verify(connectionMock, never()).getAdmin();
    }

    private HbaseProperties getProps() {
        HbaseProperties props = new HbaseProperties();
        props.setColumnFamily("data");
        props.setTtlHours(24);
        props.setNamespace("namespace");
        props.setSaltBuckets(10);
        HbaseProperties.WriterProperties writerProperties = new HbaseProperties.WriterProperties();
        writerProperties.setConnectionPoolName("MY_POOL");
        props.setWriters(ImmutableMap.of(DEFAULT_WRITER_NAME, writerProperties));
        return props;
    }

    private HbaseStore getHbaseStore(Map<String, ConnectionSupplier> connectionPools, HbaseTableProvider provider) {
        return getHbaseStore(connectionPools, provider, false);
    }

    private HbaseStore getHbaseStore(Map<String, ConnectionSupplier> connectionPools, HbaseTableProvider provider,
            boolean enableJMX) {
        JMXController jmxController = new JMXController(new KafkaEtlProperties());
        if (enableJMX) {
            jmxController.setEnableRecordAnalysis(true);
            jmxController.setMaxRecordsToDisplay(10);
        }
        return new HbaseStore(systemServiceMock, schemaService, connectionPools, threadPools, provider, getProps(),
                null, recordConverter, jmxController);
    }
}