package cern.nxcals.api.extraction.data.spark;

import cern.nxcals.common.domain.ColumnMapping;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static cern.nxcals.api.utils.EncodingUtils.encodeFields;
import static cern.nxcals.common.spark.MoreFunctions.createNullableField;
import static cern.nxcals.common.spark.MoreFunctions.createTypeFromFields;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class DatasetCreatorUtilsTests {
    private static final String outerField = "veryUniqueName";
    private static final String outerFieldToEncode = "b.c";
    private static final String middleField = "`b.to.sanitize`";
    private static final String child1Field = "`c.d`";
    private static final String child2Field = "`anotherUniqueName`";
    private static final String fieldNameWithSpecialCharacter = "a.b.`spe.cial`.`chara.cters`.end";
    private static final String columnNameAsStringValue = "'String'";

    private final StructType sparkSchema = createTypeFromFields(
            createNullableField(child2Field, DataTypes.StringType),
            createNullableField(outerField, createTypeFromFields(
                    createNullableField(encodeFields(middleField), createTypeFromFields(
                            createNullableField(encodeFields(child1Field), DataTypes.StringType),
                            createNullableField(encodeFields(child2Field), DataTypes.StringType)
                    ))
            )),
            createNullableField(encodeFields("`" + outerFieldToEncode + "`"), DataTypes.StringType)
    );

    @Test
    void shouldDecodeColumnNamesInDatasetForAllColumnsInSchema() {
        // given
        Dataset<Row> dataset = mock(Dataset.class);
        when(dataset.select(any(Column.class))).thenReturn(dataset);
        when(dataset.schema()).thenReturn(sparkSchema);

        // when
        DatasetCreatorUtils.decodeColumnNames(dataset);

        // then
        verify(dataset, times(1)).schema();
        ArgumentCaptor<Column[]> columnsFromSelectionCaptor = ArgumentCaptor.forClass(Column[].class);
        verify(dataset, times(1)).select(columnsFromSelectionCaptor.capture());
        verifyNoMoreInteractions(dataset);
        Set<Column> columnsFromSelection = new HashSet<>(Arrays.asList(columnsFromSelectionCaptor.getValue()));

        // all column should be in dataset
        assertTrue(
                columnsFromSelection.stream().allMatch(column -> {
                    String columnExpr = column.expr().toString();
                    return columnExpr.contains(child2Field) || columnExpr.contains(outerField)
                            || columnExpr.contains(outerFieldToEncode);
                }));

        // get cast column with nested fields and check if is correct
        Optional<Column> maybeCastedColumn = Arrays.stream(columnsFromSelectionCaptor.getValue())
                .filter(column -> column.toString().contains(outerField)).findFirst();
        assertTrue(maybeCastedColumn.isPresent());
        Column castedColumn = maybeCastedColumn.get();
        assertNotNull(castedColumn.getField(middleField).getField(child1Field));
    }

    @ParameterizedTest(name = "should correctly decode column name if it {2}")
    @MethodSource("mappingsTestSource")
    void shouldNotModifyColumnNameIfIsString(String encodedColumnName, String decodedColumnName, String comment) {
        // given
        ColumnMapping mapping = ColumnMapping.builder()
                .fieldName(encodedColumnName).schemaJson("[]").alias("alias")
                .build();
        // when
        Collection<ColumnMapping> resultMappings = DatasetCreatorUtils.decodeColumnNames(List.of(mapping));
        // then
        Optional<ColumnMapping> maybeResultMapping = resultMappings.stream().findFirst();
        assertTrue(maybeResultMapping.isPresent());
        assertEquals(decodedColumnName, maybeResultMapping.get().getFieldName());
    }

    @MethodSource
    static Object[] mappingsTestSource() {
        return new Object[] {
                new Object[] { fieldNameWithSpecialCharacter, fieldNameWithSpecialCharacter,
                        "doesn't contain encoded parts" },
                new Object[] { encodeFields(fieldNameWithSpecialCharacter),
                        fieldNameWithSpecialCharacter,
                        "contains encoded parts" },
                new Object[] { columnNameAsStringValue, columnNameAsStringValue, "is string value" }
        };
    }
}
