package cern.nxcals.api.extraction.data.spark;

import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.extraction.data.ExtractionUtils;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.VariableService;
import org.apache.avro.Schema;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anySet;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

class ExtractionUtilsTest {
    private final Schema intSchema = Schema.create(Schema.Type.INT);
    private final Schema doubleSchema = Schema.create(Schema.Type.DOUBLE);

    private final Schema arrayUnionSchema = new Schema.Parser().parse(
            "[{\"type\":\"array\",\"items\":[\"float\",\"null\"]},\"null\"]");
    private final Schema recordSchema1 = new Schema.Parser().parse(
            "[{\"type\": \"record\", \"name\": \"test\", \"fields\" : [{\"name\": \"a\", \"type\": \"long\"}]},\"null\"]");
    private final Schema recordSchema2 = new Schema.Parser().parse(
            "[{\"type\": \"record\", \"name\": \"test\", \"fields\" : [{\"name\": \"a\", \"type\": \"string\"}]},\"null\"]");

    @Test
    void shouldGetTypeFor() {
        //given
        String intSchema = "[\"int\"]";
        String longSchema = "[\"long\"]";
        String floatSchema = "[\"float\"]";
        String doubleSchema = "[\"double\"]";
        String stringSchema = "[\"string\"]";
        String bytesSchema = "[\"bytes\"]";
        String booleanSchema = "[\"boolean\"]";
        String unionSchema = "[\"double\",\"null\"]";
        String arraySchema = "[{\"type\":\"array\",\"items\":[\"float\",\"null\"]},\"null\"]";
        String recordSchema = "[{\"type\":\"record\",\"name\":\"float_array_2d\",\"namespace\":\"cern.nxcals\","
                + "\"fields\":[{\"name\":\"elements\",\"type\":[{\"type\":\"array\",\"items\":[\"float\",\"null\"]},\"null\"]},"
                + "{\"name\":\"rowCount\",\"type\":[\"int\",\"null\"]},{\"name\":\"columnCount\",\"type\":[\"int\",\"null\"]}]},\"null\"]";

        List<StructField> recordFields = new ArrayList<>();
        recordFields.add(DataTypes.createStructField("elements", DataTypes.createArrayType(DataTypes.FloatType), true));
        recordFields.add(DataTypes.createStructField("rowCount", DataTypes.IntegerType, true));
        recordFields.add(DataTypes.createStructField("columnCount", DataTypes.IntegerType, true));
        DataType expectedRecordType = DataTypes.createStructType(recordFields);

        //when
        DataType intType = ExtractionUtils.getDataTypeFor(new Schema.Parser().parse(intSchema));
        DataType longType = ExtractionUtils.getDataTypeFor(new Schema.Parser().parse(longSchema));
        DataType floatType = ExtractionUtils.getDataTypeFor(new Schema.Parser().parse(floatSchema));
        DataType doubleType = ExtractionUtils.getDataTypeFor(new Schema.Parser().parse(doubleSchema));
        DataType stringType = ExtractionUtils.getDataTypeFor(new Schema.Parser().parse(stringSchema));
        DataType bytesType = ExtractionUtils.getDataTypeFor(new Schema.Parser().parse(bytesSchema));
        DataType booleanType = ExtractionUtils.getDataTypeFor(new Schema.Parser().parse(booleanSchema));
        DataType unionType = ExtractionUtils.getDataTypeFor(new Schema.Parser().parse(unionSchema));
        DataType arrayType = ExtractionUtils.getDataTypeFor(new Schema.Parser().parse(arraySchema));
        DataType recordType = ExtractionUtils.getDataTypeFor(new Schema.Parser().parse(recordSchema));

        //then
        assertNotNull(intType);
        assertEquals(DataTypes.IntegerType, intType);
        assertNotNull(longType);
        assertEquals(DataTypes.LongType, longType);
        assertNotNull(floatType);
        assertEquals(DataTypes.FloatType, floatType);
        assertNotNull(doubleType);
        assertEquals(DataTypes.DoubleType, doubleType);
        assertNotNull(stringType);
        assertEquals(DataTypes.StringType, stringType);
        assertNotNull(bytesType);
        assertEquals(DataTypes.IntegerType, bytesType);
        assertNotNull(booleanType);
        assertEquals(DataTypes.BooleanType, booleanType);
        assertNotNull(unionType);
        assertEquals(DataTypes.DoubleType, unionType);
        assertNotNull(arrayType);
        assertEquals(DataTypes.createArrayType(DataTypes.FloatType), arrayType);
        assertNotNull(recordSchema);
        assertEquals(expectedRecordType, recordType);
    }

    @Test
    void shouldFailForUnknownType() {
        //given
        Schema intSchema = new Schema.Parser().parse(
                "[{ \"type\": \"enum\"," + "  \"name\": \"Suit\", \"symbols\" : [\"TEST1\", \"TEST2\"]}]");

        //when
        assertThrows(RuntimeException.class, () -> ExtractionUtils.getDataTypeFor(intSchema));
    }

    @Test
    void shouldHaveCorrectHbaseTypeNames() {
        assertEquals("int", ExtractionUtils.getHbaseTypeNameFor(intSchema));
        assertEquals("double", ExtractionUtils.getHbaseTypeNameFor(doubleSchema));
        assertEquals("binary", ExtractionUtils.getHbaseTypeNameFor(arrayUnionSchema));
        assertEquals("binary", ExtractionUtils.getHbaseTypeNameFor(recordSchema1));
    }

    @Test
    void shouldGroupVariablesByType() {
        List<String> variableNames = List.of("int1", "array1", "double1", "record1", "int2", "record2");
        List<Variable> variables = variableNames.stream()
                .map(variableName -> Variable.builder().variableName(variableName).systemSpec(mock(
                        SystemSpec.class)).build()).collect(Collectors.toList());
        withMockedServiceClientFactory(mockedServiceClientFactory -> {
            VariableService variableServiceMock = mock(VariableService.class);
            mockedServiceClientFactory.when(ServiceClientFactory::createVariableService)
                    .thenReturn(variableServiceMock);
            Map<Variable, Map<TimeWindow, Schema>> res = Map.of(
                    variables.get(0), Map.of(TimeWindow.infinite(), intSchema),
                    variables.get(1), Map.of(TimeWindow.infinite(), arrayUnionSchema),
                    variables.get(2), Map.of(TimeWindow.infinite(), doubleSchema),
                    variables.get(3), Map.of(TimeWindow.infinite(), recordSchema1),
                    variables.get(4), Map.of(TimeWindow.infinite(), intSchema),
                    variables.get(5), Map.of(TimeWindow.infinite(), recordSchema2)
            );
            when(variableServiceMock.findSchemas(anySet(), any())).thenReturn(res);

            // when
            Map<DataType, Set<Variable>> groupedVariables = ExtractionUtils.groupVariablesByType(TestUtils.SESSION,
                    new HashSet<>(variables), TimeWindow.infinite());

            // then
            assertEquals(5, groupedVariables.size()); // there should be 4 different types

            // check in type
            assertTrue(
                    groupedVariables.containsKey(DataTypes.IntegerType));
            assertEquals(Set.of(variables.get(0), variables.get(4)), groupedVariables.get(DataTypes.IntegerType));

            assertTrue(groupedVariables.containsKey(DataTypes.DoubleType));
            assertEquals(Set.of(variables.get(2)), groupedVariables.get(DataTypes.DoubleType));

            assertTrue(groupedVariables.containsKey(DataTypes.createArrayType(DataTypes.FloatType)));
            assertEquals(Set.of(variables.get(1)),
                    groupedVariables.get(DataTypes.createArrayType(DataTypes.FloatType)));

            DataType recordType = ExtractionUtils.getDataTypeFor(recordSchema1);
            assertTrue(groupedVariables.containsKey(recordType));
            assertEquals(Set.of(variables.get(3)),
                    groupedVariables.get(recordType));

            recordType = ExtractionUtils.getDataTypeFor(recordSchema2);
            assertTrue(groupedVariables.containsKey(recordType));
            assertEquals(Set.of(variables.get(5)),
                    groupedVariables.get(recordType));
        });
    }

    private void withMockedServiceClientFactory(Consumer<MockedStatic<ServiceClientFactory>> consumer) {
        try (MockedStatic<ServiceClientFactory> clientFactory = mockStatic(ServiceClientFactory.class)) {
            consumer.accept(clientFactory);
        }
    }
}
