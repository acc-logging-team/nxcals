package cern.nxcals.api.extraction.data.spark;

import lombok.experimental.UtilityClass;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import java.util.ArrayList;
import java.util.List;

import static cern.nxcals.common.SystemFields.NXC_EXTR_ENTITY_ID;
import static cern.nxcals.common.SystemFields.NXC_EXTR_TIMESTAMP;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VALUE;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VARIABLE_FIELD_NAME;
import static org.apache.spark.sql.types.DataTypes.DoubleType;
import static org.apache.spark.sql.types.DataTypes.LongType;
import static org.apache.spark.sql.types.DataTypes.StringType;

@UtilityClass
public class TestUtils {
    public static final SparkSession SESSION = SparkSession.builder().master("local").appName("worker-junit-tests")
            .getOrCreate();
    static final String ENTITY_ID = NXC_EXTR_ENTITY_ID.getValue();
    static final String TIMESTAMP = NXC_EXTR_TIMESTAMP.getValue();
    static final String VALUE = NXC_EXTR_VALUE.getValue();
    static final String FIELD_NAME = NXC_EXTR_VARIABLE_FIELD_NAME.getValue();
    static final StructType VARIABLE_SCHEMA = variableSchema();

    static Dataset<Row> ds(StructType schema, Object[]... data) {
        List<Row> rows = new ArrayList<>(data.length);
        for (Object[] datum : data) {
            rows.add(RowFactory.create(datum));
        }
        return SESSION.createDataFrame(rows, schema);
    }

    private static StructType variableSchema() {
        StructField entityId = new StructField(ENTITY_ID, LongType, true, Metadata.empty());
        StructField timestamp = new StructField(TIMESTAMP, LongType, true, Metadata.empty());
        StructField value = new StructField(TestUtils.VALUE, DoubleType, true, Metadata.empty());
        StructField field = new StructField(FIELD_NAME, StringType, true, Metadata.empty());
        return new StructType(new StructField[] { entityId, value, timestamp, field });
    }

}
