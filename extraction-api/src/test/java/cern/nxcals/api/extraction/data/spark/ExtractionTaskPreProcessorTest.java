package cern.nxcals.api.extraction.data.spark;

import cern.nxcals.common.domain.ColumnMapping;
import cern.nxcals.common.domain.ExtractionTask;
import cern.nxcals.common.domain.ExtractionUnit;
import cern.nxcals.common.domain.HBaseExtractionTask;
import cern.nxcals.common.domain.HBaseResource;
import cern.nxcals.common.domain.HdfsExtractionTask;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Objects;
import java.util.function.Predicate;

import static java.util.Collections.emptySet;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ExtractionTaskPreProcessorTest {
    private static final String FIELD_NAME = "test";
    private ExtractionTaskPreProcessor instance;

    @BeforeEach
    void setUp() {
        ExtractionUnit unit = mock(ExtractionUnit.class);
        when(unit.isVariableSearch()).thenReturn(true);
        this.instance = new ExtractionTaskPreProcessor(unit);
    }

    @Test
    void shouldAddFieldColumnForHBaseTask() {
        ColumnMapping mapping = ColumnMapping.builder().fieldName(FIELD_NAME).build();
        HBaseExtractionTask task = hbaseTask(mapping, true);
        assertNewTaskHasBeenCreated(task, ("'" + FIELD_NAME + "'")::equals);
    }

    @Test
    void shouldAddFieldColumnForHdfsTask() {
        ColumnMapping mapping = ColumnMapping.builder().fieldName(FIELD_NAME).build();
        HdfsExtractionTask task = hdfsTask(mapping, true);
        assertNewTaskHasBeenCreated(task, ("'" + FIELD_NAME + "'")::equals);
    }

    @Test
    void shouldNotAddFieldColumnForHBaseTask() {
        ColumnMapping mapping = ColumnMapping.builder().fieldName(FIELD_NAME).build();
        HBaseExtractionTask task = hbaseTask(mapping, false);
        assertNewTaskHasBeenCreated(task, Objects::isNull);
    }

    @Test
    void shouldNotAddFieldColumnForHdfsTask() {
        ColumnMapping mapping = ColumnMapping.builder().fieldName(FIELD_NAME).build();
        HdfsExtractionTask task = hdfsTask(mapping, false);
        assertNewTaskHasBeenCreated(task, Objects::isNull);
    }

    private HBaseExtractionTask hbaseTask(ColumnMapping mapping, boolean setVariableFieldMapping) {
        return HBaseExtractionTask.builder().resource(mock(HBaseResource.class)).column(mapping).predicates(emptySet())
                .variableFieldBoundColumnMapping(setVariableFieldMapping ? mapping : null).build();
    }

    private HdfsExtractionTask hdfsTask(ColumnMapping mapping, boolean setVariableFieldMapping) {
        return HdfsExtractionTask.builder().paths(emptySet()).column(mapping).predicates(emptySet())
                .variableFieldBoundColumnMapping(setVariableFieldMapping ? mapping : null).build();
    }

    private void assertNewTaskHasBeenCreated(ExtractionTask task, Predicate<String> columnChecker) {
        assertEquals(1, task.getColumns().size());
        ExtractionTask newTask = task.processWith(this.instance);
        assertEquals(2, newTask.getColumns().size());
        assertTrue(newTask.getColumns().stream().anyMatch(c -> columnChecker.test(c.getFieldName())));
    }

}
