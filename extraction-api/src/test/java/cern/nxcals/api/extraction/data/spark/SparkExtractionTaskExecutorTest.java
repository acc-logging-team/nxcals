package cern.nxcals.api.extraction.data.spark;

import cern.nxcals.common.domain.ColumnMapping;
import cern.nxcals.common.domain.ExtractionTask;
import cern.nxcals.common.domain.ExtractionTaskProcessor;
import cern.nxcals.common.domain.ExtractionUnit;
import cern.nxcals.common.domain.HBaseExtractionTask;
import cern.nxcals.common.domain.HdfsExtractionTask;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.UnaryOperator;

import static cern.nxcals.api.extraction.data.ExtractionUtils.STRING_SCHEMA;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class SparkExtractionTaskExecutorTest {
    private SparkExtractionTaskProcessor processor;
    private Function<ExtractionUnit, UnaryOperator<Dataset<Row>>> postMaker;
    private Function<ExtractionUnit, ExtractionTaskProcessor<ExtractionTask>> preMaker;

    private SparkExtractionTaskExecutor instance;

    @BeforeEach
    void setUp() {
        this.processor = mock(SparkExtractionTaskProcessor.class);
        this.postMaker = mock(PostMaker.class);
        this.preMaker = mock(PreMaker.class);
        this.instance = new SparkExtractionTaskExecutor(TestUtils.SESSION, processor, preMaker, postMaker);
    }

    @Test
    void shouldGetEmptyDataSetForNoTasksUnit() {
        ExtractionUnit unit = mock(ExtractionUnit.class, RETURNS_DEEP_STUBS);
        when(unit.getTasks()).thenReturn(emptyList());
        String fieldName = "test";
        when(unit.getEmptyDatasetMapping()).thenReturn(singletonList(
                ColumnMapping.builder().fieldName(fieldName).schemaJson(STRING_SCHEMA.toString()).build()));
        Dataset<Row> ds = this.instance.execute(unit);
        assertEquals(0, ds.count());
        ArrayUtils.contains(ds.columns(), fieldName);
    }

    @Test
    void shouldGetDataSetForSomeTasks() {
        ExtractionUnit unit = mock(ExtractionUnit.class, RETURNS_DEEP_STUBS);

        Dataset<Row> mockDs = mock(Dataset.class);
        Optional<Dataset<Row>> ds = Optional.of(mockDs);
        when(mockDs.union(any())).thenReturn(mockDs);

        HdfsExtractionTask hdfsTask = mock(HdfsExtractionTask.class);
        HBaseExtractionTask hbaseTask = mock(HBaseExtractionTask.class);

        when(hdfsTask.processWith(processor)).thenReturn(ds);
        when(hbaseTask.processWith(processor)).thenReturn(ds);

        ExtractionTaskProcessor<ExtractionTask> dummyPreProcessor = getDummyTaskProcessor();
        when(preMaker.apply(unit)).thenReturn(dummyPreProcessor);

        when(hdfsTask.processWith(dummyPreProcessor)).thenReturn(hdfsTask);
        when(hbaseTask.processWith(dummyPreProcessor)).thenReturn(hbaseTask);

        when(processor.execute(any(HdfsExtractionTask.class))).thenReturn(ds);
        when(processor.execute(any(HBaseExtractionTask.class))).thenReturn(ds);

        when(unit.getTasks()).thenReturn(ImmutableList.of(hdfsTask, hbaseTask));

        when(postMaker.apply(unit)).thenReturn(d -> d);
        Dataset<Row> result = this.instance.execute(unit);
        assertEquals(mockDs, result);

    }

    private ExtractionTaskProcessor<ExtractionTask> getDummyTaskProcessor() {
        return new ExtractionTaskProcessor<ExtractionTask>() {
            @Override
            public ExtractionTask execute(HBaseExtractionTask task) {
                return task;
            }

            @Override
            public ExtractionTask execute(HdfsExtractionTask task) {
                return task;
            }
        };
    }

    private interface PostMaker extends Function<ExtractionUnit, UnaryOperator<Dataset<Row>>> {
    }

    private interface PreMaker extends Function<ExtractionUnit, ExtractionTaskProcessor<ExtractionTask>> {

    }
}
