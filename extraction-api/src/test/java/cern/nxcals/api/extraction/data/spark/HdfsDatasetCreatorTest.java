package cern.nxcals.api.extraction.data.spark;

import cern.nxcals.common.domain.ColumnMapping;
import cern.nxcals.common.domain.HdfsExtractionTask;
import org.apache.spark.sql.DataFrameReader;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.net.URI;
import java.util.Optional;
import java.util.Set;

import static cern.nxcals.api.extraction.data.ExtractionUtils.STRING_SCHEMA;
import static cern.nxcals.common.spark.MoreFunctions.createNullableField;
import static cern.nxcals.common.spark.MoreFunctions.createTypeFromFields;
import static java.util.Collections.emptySet;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class HdfsDatasetCreatorTest {
    private SparkSession sessionMock;
    private HdfsDatasetCreator instance;
    private Dataset<Row> datasetMock;
    private DataFrameReader readerMock;
    private static final StructType structType = createTypeFromFields(
            createNullableField("FIELD", DataTypes.IntegerType));

    @BeforeEach
    public void setUp() {
        datasetMock = mock(Dataset.class, invocation -> {
            String methodName = invocation.getMethod().getName();
            if (methodName.equals("select") || methodName.equals("where") || methodName.equals("selectExpr")) {
                return invocation.getMock();
            }
            return null;
        });
        when(datasetMock.schema()).thenReturn(structType);
        sessionMock = mock(SparkSession.class);
        readerMock = mock(DataFrameReader.class, invocation -> {
            if (invocation.getMethod().getName().equals("load")) { // mocking varargs doesn't work
                return datasetMock;
            } else {
                return null;
            }
        });

        when(sessionMock.read()).thenReturn(readerMock);
        this.instance = new HdfsDatasetCreator(sessionMock);
    }

    @Test
    void shouldCreateEmptyDatasetForNonExistingPaths() {
        HdfsExtractionTask task = HdfsExtractionTask.builder().paths(emptySet()).predicates(emptySet())
                .column(ColumnMapping.builder().fieldName("test").schemaJson(STRING_SCHEMA.toString()).build()).build();

        this.instance = new HdfsDatasetCreator(this.sessionMock, u -> false);
        Optional<Dataset<Row>> dataset = this.instance.apply(task);
        assertFalse(dataset.isPresent());
    }

    @Test
    void shouldCreateNonDatasetForProperlyMockedTaskAndSession() {
        String path = "hdfs:///project/nxcals";
        String predicate = "x > 0";
        HdfsExtractionTask task = HdfsExtractionTask.builder()
                .paths(Set.of(URI.create(path)))
                .predicates(Set.of(predicate))
                .column(ColumnMapping.builder().fieldName("test").schemaJson(STRING_SCHEMA.toString()).build())
                .build();

        this.instance = new HdfsDatasetCreator(this.sessionMock, u -> true);
        Optional<Dataset<Row>> dataset = this.instance.apply(task);
        assertTrue(dataset.isPresent());
    }

}
