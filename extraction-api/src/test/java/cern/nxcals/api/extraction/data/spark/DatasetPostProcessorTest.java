package cern.nxcals.api.extraction.data.spark;

import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.common.domain.ExtractionUnit;
import cern.nxcals.common.domain.ExtractionUnit.VariableMapping;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.LongStream;

import static cern.nxcals.api.extraction.data.spark.TestUtils.ENTITY_ID;
import static cern.nxcals.api.extraction.data.spark.TestUtils.FIELD_NAME;
import static cern.nxcals.api.extraction.data.spark.TestUtils.TIMESTAMP;
import static cern.nxcals.api.extraction.data.spark.TestUtils.VALUE;
import static cern.nxcals.api.extraction.data.spark.TestUtils.VARIABLE_SCHEMA;
import static cern.nxcals.api.extraction.data.spark.TestUtils.ds;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VARIABLE_NAME;
import static com.google.common.collect.Sets.difference;
import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class DatasetPostProcessorTest {
    private ExtractionUnit unit;
    private DatasetPostProcessor instance;

    @BeforeEach
    void setUp() {
        unit = mock(ExtractionUnit.class, RETURNS_DEEP_STUBS);
        when(unit.isVariableSearch()).thenReturn(true);
        when(unit.getCriteria().getAliasFields()).thenReturn(emptyMap());
        this.instance = new DatasetPostProcessor(unit);
    }

    @Test
    void shouldGetEmptyLookupSource() {
        Map<Object, Map<TimeWindow, Map<String, String>>> source = this.instance.lookupSourceFrom(emptyMap());
        assertTrue(source.isEmpty());
    }

    @Test
    void shouldGetLookupSource() {
        Map<Long, List<VariableMapping>> mappings = getTestMappings();
        Map<Object, Map<TimeWindow, Map<String, String>>> source = this.instance.lookupSourceFrom(mappings);
        assertFalse(source.isEmpty());
        assertTrue(difference(mappings.keySet(), source.keySet()).isEmpty());
        assertTrue(difference(source.keySet(), mappings.keySet()).isEmpty());
        assertEquals(mappings.size(), source.size());
        for (Long key : mappings.keySet()) {
            List<VariableMapping> variableMappings = mappings.get(key);
            assertNotNull(variableMappings);
            Map<TimeWindow, Map<String, String>> fieldMappings = source.get(key);
            assertEquals(variableMappings.size(), fieldMappings.size());
            assertEquals(variableMappings.stream().map(VariableMapping::getWindow).collect(toSet()),
                    fieldMappings.keySet());
            assertEquals(variableMappings.stream().map(VariableMapping::getFieldToVariableName).collect(toSet()),
                    new HashSet<>(fieldMappings.values()));

        }
    }

    @Test
    void shouldPostProcessDataset() {
        // { entityId, value, timestamp, field });
        Map<Long, List<VariableMapping>> mappings = new HashMap<>();
        String variableName = "ala_ma_kota";
        String fieldName = "fieldName";
        mappings.put(1L, singletonList(VariableMapping.builder().window(TimeWindow.between(0, 2))
                .fieldToVariableName(ImmutableMap.of(fieldName, variableName)).build()));
        when(unit.getMappings()).thenReturn(mappings);
        Dataset<Row> ds = ds(VARIABLE_SCHEMA,
                new Object[][] { { 1L, 1D, 1L, fieldName }, { 2L, null, 2L, fieldName } });

        assertEquals(2, ds.count());
        assertTrue(ArrayUtils.contains(ds.columns(), ENTITY_ID));
        assertTrue(ArrayUtils.contains(ds.columns(), TIMESTAMP));
        assertTrue(ArrayUtils.contains(ds.columns(), VALUE));
        assertTrue(ArrayUtils.contains(ds.columns(), FIELD_NAME));
        assertFalse(ArrayUtils.contains(ds.columns(), NXC_EXTR_VARIABLE_NAME.getValue()));

        ds.show();
        Dataset<Row> postProcessed = this.instance.apply(ds);
        assertEquals(1, postProcessed.count());

        assertTrue(ArrayUtils.contains(postProcessed.columns(), ENTITY_ID));
        assertTrue(ArrayUtils.contains(postProcessed.columns(), TIMESTAMP));
        assertTrue(ArrayUtils.contains(postProcessed.columns(), VALUE));
        assertFalse(ArrayUtils.contains(postProcessed.columns(), FIELD_NAME));
        assertTrue(ArrayUtils.contains(postProcessed.columns(), NXC_EXTR_VARIABLE_NAME.getValue()));

        postProcessed.show();

    }

    private Map<Long, List<VariableMapping>> getTestMappings() {
        return LongStream.rangeClosed(1, 10).mapToObj(i -> {
            List<VariableMapping> vm = new ArrayList<>();
            for (int j = 0; j < i; j++) {
                TimeWindow window = TimeWindow.between(j, j + 1);
                String key = String.valueOf(j);
                vm.add(VariableMapping.builder().window(window).fieldToVariableName(ImmutableMap.of(key, key)).build());
            }
            return Pair.of(i, vm);
        }).collect(toMap(Pair::getKey, Pair::getValue));
    }
}
