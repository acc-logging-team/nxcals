package cern.nxcals.api.extraction.data.builders;

import cern.nxcals.api.domain.TimeWindow;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Instant;

import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class DevicePropertyDataQueryTest {

    @Test
    public void shouldThrowIfSparkSessionIsNullOnGetFor() {
        SparkSession sparkSession = null;
        TimeWindow timeWindow = TimeWindow.between(Instant.ofEpochSecond(10), Instant.ofEpochSecond(50));
        assertThrows(NullPointerException.class, () -> DevicePropertyDataQuery.getFor(sparkSession, timeWindow,
                "testSystem", "testDevice", "testProperty"));
    }

    @Test
    public void shouldThrowIfSystemIsNullOnGetFor() {
        SparkSession sparkSession = Mockito.mock(SparkSession.class);
        TimeWindow timeWindow = TimeWindow.between(Instant.ofEpochSecond(10), Instant.ofEpochSecond(50));
        assertThrows(NullPointerException.class, () -> DevicePropertyDataQuery.getFor(sparkSession, timeWindow,
                null, "testDevice", "testProperty"));
    }

    @Test
    public void shouldThrowIfDeviceIsNullOnGetFor() {
        SparkSession sparkSession = Mockito.mock(SparkSession.class);
        TimeWindow timeWindow = TimeWindow.between(Instant.ofEpochSecond(10), Instant.ofEpochSecond(50));
        assertThrows(NullPointerException.class, () -> DevicePropertyDataQuery.getFor(sparkSession, timeWindow,
                "testSystem", null, "testProperty"));
    }

    @Test
    public void shouldThrowIfPropertyIsNullOnGetFor() {
        SparkSession sparkSession = Mockito.mock(SparkSession.class);
        TimeWindow timeWindow = TimeWindow.between(Instant.ofEpochSecond(10), Instant.ofEpochSecond(50));
        assertThrows(NullPointerException.class, () -> DevicePropertyDataQuery.getFor(sparkSession, timeWindow,
                "testSystem", "testDevice", null));
    }

}
