package cern.nxcals.api.authorization;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junitpioneer.jupiter.ClearEnvironmentVariable;
import org.junitpioneer.jupiter.ClearSystemProperty;
import org.junitpioneer.jupiter.SetEnvironmentVariable;
import org.junitpioneer.jupiter.SetSystemProperty;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ClearSystemProperty(key = RbacHadoopDelegationTokenProvider.FEATURE_TOGGLE)
@ClearEnvironmentVariable(key = RbacHadoopDelegationTokenProvider.FEATURE_TOGGLE)
class RbacHadoopDelegationTokenProviderTest {

    @BeforeAll
    static void setup() {
        // ensure that the class is loaded and static block is executed without call to the service
        RbacHadoopDelegationTokenProvider.obtainAndSetDelegationTokensViaRbacIfRequired();
    }

    @Test
    @SetEnvironmentVariable(key = RbacHadoopDelegationTokenProvider.FEATURE_TOGGLE, value = "true")
    void shouldBeEnabledIfEnvVarIsSpecified() {
        assertTrue(RbacHadoopDelegationTokenProvider.isEnabled());
    }

    @Test
    @SetSystemProperty(key = RbacHadoopDelegationTokenProvider.FEATURE_TOGGLE, value = "true")
    void shouldBeEnabledIfSystemPropertyIsSpecified() {
        assertTrue(RbacHadoopDelegationTokenProvider.isEnabled());
    }

    @Test
    void shouldBeDisabledIfSystemPropertyAndEnvVarIsMissing() {
        assertFalse(RbacHadoopDelegationTokenProvider.isEnabled());
    }

}