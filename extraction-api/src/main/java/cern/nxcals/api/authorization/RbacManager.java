package cern.nxcals.api.authorization;

import cern.rbac.client.authentication.AuthenticationException;
import cern.rbac.common.RbaToken;
import cern.rbac.common.authentication.LoginPolicy;
import cern.rbac.util.authentication.LoginServiceBuilder;
import cern.rbac.util.lookup.RbaTokenLookup;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RbacManager {

    protected RbaToken findRbaToken() {
        return RbaTokenLookup.findRbaToken();
    }

    protected void setupRbacIfMissing() {
        if (findRbaToken() == null) {
            // Setup RBAC if the user hasn't
            try {
                log.info("No RBAC token was set by the user, trying to get and set it with LoginPolicy.DEFAULT");
                LoginServiceBuilder.newInstance().loginPolicy(LoginPolicy.DEFAULT).autoRefresh(true).build();
            } catch (AuthenticationException e) {
                throw new IllegalStateException("Could not authenticate using RBAC", e);
            }
        }
    }
}
