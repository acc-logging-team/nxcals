package cern.nxcals.api.authorization;

import org.apache.hadoop.security.Credentials;
import org.apache.hadoop.security.UserGroupInformation;

public class UgiManager {
    protected void createAndSetUser(String userName, Credentials credentials) {
        UserGroupInformation ugi = UserGroupInformation.createRemoteUser(userName);
        ugi.addCredentials(credentials);
        UserGroupInformation.setLoginUser(ugi);
    }
}
