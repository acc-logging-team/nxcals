package cern.nxcals.api.extraction.data.spark;

import cern.nxcals.common.domain.ExtractionTask;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.util.Optional;
import java.util.function.Function;

@FunctionalInterface
public interface DatasetCreator<T extends ExtractionTask> extends Function<T, Optional<Dataset<Row>>> {
}
