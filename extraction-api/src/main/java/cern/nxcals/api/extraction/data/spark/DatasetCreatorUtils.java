package cern.nxcals.api.extraction.data.spark;

import cern.nxcals.common.domain.ColumnMapping;
import cern.nxcals.common.spark.MoreFunctions;
import lombok.experimental.UtilityClass;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static cern.nxcals.api.utils.EncodingUtils.decodeFields;
import static cern.nxcals.api.utils.EncodingUtils.desanitizeIfNeeded;
import static org.apache.commons.lang.StringUtils.defaultIfEmpty;

@UtilityClass
public class DatasetCreatorUtils {

    /**
     * Basing on dataset's schema, decode field names in all columns.
     *
     * @param dataset dataset with encoded field names
     * @return dataset with decoded field names
     */

    static Dataset<Row> decodeColumnNames(Dataset<Row> dataset) {
        Column[] columns = Arrays.stream(dataset.schema().fields())
                .map(DatasetCreatorUtils::decodeField).toArray(Column[]::new);
        return dataset.select(columns);
    }

    private static Column decodeField(StructField field) {
        String alias = desanitizeIfNeeded(field.name());
        Column column = new Column(field.name()).alias(alias).cast(field.dataType());
        if (field.dataType() instanceof StructType) {
            return column.cast(MoreFunctions.decodeColumnNames((StructType) field.dataType()));
        }
        return column;
    }

    static String[] getColumnsFrom(Collection<ColumnMapping> projection) {
        return projection.stream().map(DatasetCreatorUtils::toColumn).map(c -> c.expr().sql()).toArray(String[]::new);
    }

    private static Column toColumn(ColumnMapping mapping) {
        Column column = functions.expr(defaultIfEmpty(mapping.getFieldName(), "null"));
        return column.alias(mapping.getQualifiedName());
    }

    static List<ColumnMapping> decodeColumnNames(Collection<ColumnMapping> columnMappings) {
        return columnMappings.stream().map(DatasetCreatorUtils::decodeFieldNameInMapping).collect(Collectors.toList());
    }

    private static ColumnMapping decodeFieldNameInMapping(ColumnMapping mapping) {
        String columnName = mapping.getFieldName();
        if (columnName == null || columnName.startsWith("'")) {
            return mapping; // string, not column
        }
        return mapping.toBuilder().fieldName(decodeFields(columnName)).build();
    }

}
