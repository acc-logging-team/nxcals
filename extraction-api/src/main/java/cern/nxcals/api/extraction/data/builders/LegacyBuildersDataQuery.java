package cern.nxcals.api.extraction.data.builders;

import cern.nxcals.api.extraction.data.builders.fluent.EntityAliasStage;
import cern.nxcals.api.extraction.data.builders.fluent.KeyValueStage;
import cern.nxcals.api.extraction.data.builders.fluent.QueryData;
import cern.nxcals.api.extraction.data.builders.fluent.StageSequenceEntities;
import cern.nxcals.api.extraction.data.builders.fluent.StageSequenceVariables;
import cern.nxcals.api.extraction.data.builders.fluent.SystemStage;
import cern.nxcals.api.extraction.data.builders.fluent.TimeStartStage;
import cern.nxcals.api.extraction.data.builders.fluent.VariableAliasStage;
import cern.nxcals.api.extraction.data.builders.fluent.v2.AbstractDataQuery;
import lombok.RequiredArgsConstructor;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

@RequiredArgsConstructor

/**
 * This class has to be public for python stub generation.
 */
public abstract class LegacyBuildersDataQuery extends AbstractDataQuery<Dataset<Row>> {
    protected final SparkSession session;

    /**
     * @return query builder for variables queries
     */
    //     * @deprecated Use {@link DataQuery#variables()} ()} instead
    //    @Deprecated
    public SystemStage<TimeStartStage<VariableAliasStage<Dataset<Row>>, Dataset<Row>>, Dataset<Row>> byVariables() {
        return StageSequenceVariables.<Dataset<Row>>sequence()
                .apply(new QueryData<>(new SparkDatasetProducer(session)));
    }

    /**
     * @return query builder for entities queries
     */
    //    * @deprecated Use {@link DataQuery#entities()} instead
    //    @Deprecated
    public SystemStage<TimeStartStage<EntityAliasStage<KeyValueStage<Dataset<Row>>, Dataset<Row>>, Dataset<Row>>, Dataset<Row>> byEntities() {
        return StageSequenceEntities.<Dataset<Row>>sequence().apply(new QueryData<>(new SparkDatasetProducer(session)));
    }
}
