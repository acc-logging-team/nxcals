package cern.nxcals.api.extraction.data.exceptions;

public class IncompatibleSchemaPromotionException extends RuntimeException {

    public IncompatibleSchemaPromotionException(String message) {
        super(message);
    }

    public IncompatibleSchemaPromotionException(String message, Throwable cause) {
        super(message, cause);
    }
}
