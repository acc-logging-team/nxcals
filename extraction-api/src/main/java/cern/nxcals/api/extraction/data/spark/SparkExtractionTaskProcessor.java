package cern.nxcals.api.extraction.data.spark;

import cern.nxcals.common.domain.ExtractionTaskProcessor;
import cern.nxcals.common.domain.HBaseExtractionTask;
import cern.nxcals.common.domain.HdfsExtractionTask;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.util.Optional;

@RequiredArgsConstructor
class SparkExtractionTaskProcessor implements ExtractionTaskProcessor<Optional<Dataset<Row>>> {
    @NonNull
    private final DatasetCreator<HdfsExtractionTask> hdfsHandler;
    @NonNull
    private final DatasetCreator<HBaseExtractionTask> hbaseHandler;

    @Override
    public Optional<Dataset<Row>> execute(HBaseExtractionTask task) {
        return this.hbaseHandler.apply(task);
    }

    @Override
    public Optional<Dataset<Row>> execute(HdfsExtractionTask task) {
        return this.hdfsHandler.apply(task);
    }

}
