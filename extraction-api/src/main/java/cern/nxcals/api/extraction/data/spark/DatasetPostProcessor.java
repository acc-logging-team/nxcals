package cern.nxcals.api.extraction.data.spark;

import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.common.domain.ExtractionUnit;
import cern.nxcals.common.domain.ExtractionUnit.VariableMapping;
import com.google.common.annotations.VisibleForTesting;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.functions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

import static cern.nxcals.common.SystemFields.NXC_EXTR_ENTITY_ID;
import static cern.nxcals.common.SystemFields.NXC_EXTR_TIMESTAMP;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VALUE;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VARIABLE_FIELD_NAME;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VARIABLE_NAME;
import static cern.nxcals.common.spark.MoreFunctions.lookupWith;
import static org.apache.spark.sql.types.DataTypes.StringType;

@RequiredArgsConstructor
class DatasetPostProcessor implements UnaryOperator<Dataset<Row>> {
    private static final String VARIABLE_NAME = NXC_EXTR_VARIABLE_NAME.getValue();
    public static final String ENTITY_ID = NXC_EXTR_ENTITY_ID.getValue();
    public static final String TIMESTAMP = NXC_EXTR_TIMESTAMP.getValue();
    public static final String FIELD_NAME = NXC_EXTR_VARIABLE_FIELD_NAME.getValue();
    private final ExtractionUnit unit;

    @Override
    public Dataset<Row> apply(Dataset<Row> current) {
        Dataset<Row> ret = verifyColumns(current, unit.getCriteria().getAliasFields());
        if (unit.isVariableSearch()) {
            ret = ret.withColumn(VARIABLE_NAME,
                    functions.udf(lookupWith(lookupSourceFrom(unit.getMappings())), StringType)
                            .apply(ret.col(ENTITY_ID), ret.col(TIMESTAMP), ret.col(FIELD_NAME)));
            ret = ret.drop(FIELD_NAME);
        }
        return stripNullValues(ret);
    }

    private Dataset<Row> stripNullValues(Dataset<Row> dataset) {
        return ArrayUtils.contains(dataset.columns(), NXC_EXTR_VALUE.getValue()) ?
                dataset.where(dataset.col(NXC_EXTR_VALUE.getValue()).isNotNull()) :
                dataset;
    }

    private Dataset<Row> verifyColumns(Dataset<Row> dataset, Map<String, List<String>> aliases) {
        for (String alias : aliases.keySet()) {
            if (Stream.of(dataset.columns()).noneMatch(c -> c.equals(alias))) {
                dataset = dataset.withColumn(alias, functions.lit(null));
            }
        }
        return dataset;
    }

    @VisibleForTesting
    Map<Object, Map<TimeWindow, Map<String, String>>> lookupSourceFrom(Map<Long, List<VariableMapping>> mappings) {
        Map<Object, Map<TimeWindow, Map<String, String>>> source = new HashMap<>();
        for (Map.Entry<Long, List<VariableMapping>> mapping : mappings.entrySet()) {
            for (VariableMapping variableMapping : mapping.getValue()) {
                source.computeIfAbsent(mapping.getKey(), k -> new HashMap<>())
                        .computeIfAbsent(variableMapping.getWindow(), k -> new HashMap<>())
                        .putAll(variableMapping.getFieldToVariableName());
            }
        }
        return source;
    }

}
