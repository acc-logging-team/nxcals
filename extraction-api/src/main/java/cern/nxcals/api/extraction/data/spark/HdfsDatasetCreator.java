package cern.nxcals.api.extraction.data.spark;

import cern.nxcals.common.domain.HdfsExtractionTask;
import cern.nxcals.common.utils.ConfigHolder;
import cern.nxcals.common.utils.Lazy;
import cern.nxcals.common.utils.StreamUtils;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URI;
import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

import static cern.nxcals.api.extraction.data.spark.DatasetCreatorUtils.decodeColumnNames;
import static cern.nxcals.api.extraction.data.spark.DatasetCreatorUtils.getColumnsFrom;
import static cern.nxcals.common.Constants.DATASET_BUILD_PARALLELIZE_ABOVE_SIZE;
import static cern.nxcals.common.Constants.DEFAULT_PARALLELIZE_ABOVE_SIZE;
import static lombok.AccessLevel.PACKAGE;

@Slf4j
@RequiredArgsConstructor(access = PACKAGE)
class HdfsDatasetCreator implements DatasetCreator<HdfsExtractionTask> {
    private static final Lazy<FileSystem> fileSystem = new Lazy<>(HdfsDatasetCreator::getFileSystem);
    @NonNull
    private final SparkSession session;
    @NonNull
    private final Predicate<URI> pathPredicate;

    public HdfsDatasetCreator(@NonNull SparkSession session) {
        this(session, HdfsDatasetCreator::exists);
    }

    @Override
    public Optional<Dataset<Row>> apply(HdfsExtractionTask task) {
        String[] paths = getValidPaths(task.getPaths());
        if (paths.length == 0) {
            log.warn("Nothing to be queried as the relevant paths {} do not exists", task.getPaths());
            return Optional.empty();
        }
        String[] projection = getColumnsFrom(decodeColumnNames(task.getColumns()));
        return StreamUtils.of(task.getPredicates(), ConfigHolder
                .getInt(DATASET_BUILD_PARALLELIZE_ABOVE_SIZE, DEFAULT_PARALLELIZE_ABOVE_SIZE)).map(p -> {
            if (log.isDebugEnabled()) {
                log.debug("Querying {} with condition: {} and columns: {}", Arrays.toString(paths), p,
                        Arrays.toString(projection));
            }
            Dataset<Row> datasetWithEncodedFieldNames = session.read().load(paths);
            Dataset<Row> datasetWithDecodedFieldNames = decodeColumnNames(datasetWithEncodedFieldNames);
            return datasetWithDecodedFieldNames.selectExpr(projection).where(functions.expr(p));
        }).reduce(Dataset::union);
    }

    private String[] getValidPaths(Set<URI> paths) {
        return paths.stream().filter(pathPredicate).map(URI::toString).toArray(String[]::new);
    }

    private static FileSystem getFileSystem() {
        try {
            return FileSystem.get(new org.apache.hadoop.conf.Configuration());
        } catch (IOException e) {
            throw new UncheckedIOException("Cannot access hdfs filesystem", e);
        }
    }

    private static boolean exists(URI glob) {
        try {
            return fileSystem.get().globStatus(new Path(glob)).length > 0;
        } catch (IOException e) {
            throw new UncheckedIOException("Cannot access hdfs file system to check for path " + glob, e);
        }
    }
}
