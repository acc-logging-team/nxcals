package cern.nxcals.api.extraction.data;

import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.common.domain.ColumnMapping;
import cern.nxcals.common.utils.AvroUtils;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;
import lombok.NonNull;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.avro.Schema;
import org.apache.commons.lang3.StringUtils;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;
import static org.apache.spark.sql.types.DataTypes.BooleanType;
import static org.apache.spark.sql.types.DataTypes.DoubleType;
import static org.apache.spark.sql.types.DataTypes.FloatType;
import static org.apache.spark.sql.types.DataTypes.IntegerType;
import static org.apache.spark.sql.types.DataTypes.LongType;
import static org.apache.spark.sql.types.DataTypes.NullType;
import static org.apache.spark.sql.types.DataTypes.StringType;

@Slf4j
@UtilityClass
public final class ExtractionUtils {
    public static final Schema STRING_SCHEMA = new Schema.Parser().parse("[\"string\",\"null\"]");

    public static String getTimestampFieldName(@NonNull SystemSpec systemData) {
        return extractSchemaFields(systemData.getTimeKeyDefinitions()).stream().findFirst().map(Schema.Field::name)
                .orElseThrow(
                        () -> new IllegalStateException("Cannot obtain time field for system:" + systemData.getName()));
    }

    public static List<Schema.Field> extractSchemaFields(String schemaContent) {
        if (StringUtils.isBlank(schemaContent)) {
            return Collections.emptyList();
        }
        Schema entityKeyDefinitionSchema = new Schema.Parser().parse(schemaContent);
        List<Schema.Field> fields = entityKeyDefinitionSchema.getFields();
        Preconditions.checkArgument(!fields.isEmpty(),
                "Cannot extract timestamp field. No fields in time key definition.");
        return fields;
    }

    public static DataType getDataTypeFor(Schema schema) {
        switch (schema.getType()) {
        case STRING:
            return StringType;
        case BYTES:
        case INT:
            return IntegerType;
        case LONG:
            return LongType;
        case FLOAT:
            return FloatType;
        case DOUBLE:
            return DoubleType;
        case BOOLEAN:
            return BooleanType;
        case NULL:
            return NullType;
        case ARRAY:
            DataType type = getDataTypeFor(schema.getElementType());
            return DataTypes.createArrayType(type);
        case RECORD:
            return DataTypes.createStructType(createStructFields(schema));
        case UNION:
            return getDataTypeFor(AvroUtils.getSchemaUnionType(schema));
        default:
            throw new IllegalArgumentException("Unknown schema type = " + schema.getType());
        }
    }

    public static String getHbaseTypeNameFor(Schema schema) {
        if (AvroUtils.isUnion(schema.getType())) {
            return getHbaseTypeNameFor(AvroUtils.getSchemaUnionType(schema));
        } else if (AvroUtils.isPrimitiveType(schema.getType())) {
            return schema.getType().getName().toLowerCase();
        } else {
            return "binary";
        }
    }

    public static StructType getStructSchemaFor(Collection<ColumnMapping> fields) {
        StructType schema = new StructType();
        for (ColumnMapping field : fields) {
            String fieldName = field.getQualifiedName();
            schema = schema.add(fieldName, getDataTypeFor(field.getSchema().get()), true);
        }
        return schema;
    }

    private static List<StructField> createStructFields(Schema schema) {
        //Should we check for nullability?, currently I've set all endTime nullable (jwozniak)
        return schema.getFields().stream()
                .map(field -> DataTypes.createStructField(field.name(), getDataTypeFor(field.schema()), true))
                .collect(Collectors.toList());
    }

    public static Dataset<Row> createEmptyDataFrame(SparkSession session, Collection<ColumnMapping> mappings) {
        return session.createDataFrame(emptyList(), getStructSchemaFor(mappings));
    }

    /**
     * Method returns types that will be resolved by Spark, if there will be done an extraction of given variables
     *
     * @param spark
     * @param variables  variables to be checked
     * @param timeWindow time window for which variables type must be checked
     * @return
     */
    public static Map<DataType, Set<Variable>> groupVariablesByType(SparkSession spark, Set<Variable> variables,
            TimeWindow timeWindow) {
        VariableService vs = ServiceClientFactory.createVariableService();
        Map<Variable, Map<TimeWindow, Schema>> variableToSchema = vs.findSchemas(variables, timeWindow);
        Map<DataType, Set<Variable>> variablesBySchema = new HashMap<>();
        for (Map.Entry<Variable, Map<TimeWindow, Schema>> entry : variableToSchema.entrySet()) {
            Variable variable = entry.getKey();
            Map<TimeWindow, Schema> schemas = entry.getValue();
            if (!schemas.isEmpty()) {
                variablesBySchema.computeIfAbsent(findCommonType(spark, schemas.values()), k -> new HashSet<>())
                        .add(variable);
            }
        }
        return variablesBySchema;
    }

    /**
     * Return one type that will be resolved by Spark, if done union
     */
    @VisibleForTesting
    static DataType findCommonType(SparkSession spark, Collection<Schema> schemas) {
        List<Dataset<Row>> datasets = new ArrayList<>();
        for (Schema schema : schemas) {
            // reuse ColumnMapping to create an empty DSes
            ColumnMapping c = ColumnMapping.builder().fieldName("col").schemaJson(schema.toString()).build();
            Dataset<Row> ds = ExtractionUtils.createEmptyDataFrame(spark, List.of(c));
            datasets.add(ds);
        }
        Dataset<Row> finalDs = datasets.stream().reduce(Dataset::union).orElseThrow();
        return finalDs.schema().apply("col").dataType();
    }
}
