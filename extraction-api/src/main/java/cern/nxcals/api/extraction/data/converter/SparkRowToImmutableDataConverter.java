/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.api.extraction.data.converter;

import cern.cmw.datax.ImmutableData;
import lombok.NonNull;
import lombok.experimental.UtilityClass;
import org.apache.spark.sql.Row;

@UtilityClass
public class SparkRowToImmutableDataConverter {
    /**
     * @deprecated please use {@link cern.nxcals.api.converters.SparkRowToImmutableDataConverter}
     */
    @Deprecated
    public static ImmutableData convert(@NonNull Row source) {
        return cern.nxcals.api.converters.SparkRowToImmutableDataConverter.convert(source);
    }
}