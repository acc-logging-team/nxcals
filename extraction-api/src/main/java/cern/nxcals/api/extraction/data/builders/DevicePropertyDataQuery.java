/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.data.builders;

import cern.nxcals.api.extraction.data.builders.fluent.DeviceStage;
import cern.nxcals.api.extraction.data.builders.fluent.EntityAliasStage;
import cern.nxcals.api.extraction.data.builders.fluent.QueryData;
import cern.nxcals.api.extraction.data.builders.fluent.StageSequenceDeviceProperty;
import cern.nxcals.api.extraction.data.builders.fluent.SystemStage;
import cern.nxcals.api.extraction.data.builders.fluent.TimeStartStage;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class DevicePropertyDataQuery extends DevicePropertyStaticMethods {
    /**
     * @param session spark session
     * @return query builder for device-parameter queries
     */
    //    * @deprecated Use {@link ParameterDataQuery#builder(SparkSession)}
    //    @Deprecated
    public static SystemStage<TimeStartStage<EntityAliasStage<DeviceStage<Dataset<Row>>, Dataset<Row>>, Dataset<Row>>, Dataset<Row>> builder(
            SparkSession session) {
        return StageSequenceDeviceProperty.<Dataset<Row>>sequence()
                .apply(new QueryData<>(new SparkDatasetProducer(session)));
    }
}
