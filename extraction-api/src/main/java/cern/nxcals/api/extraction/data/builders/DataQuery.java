/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.data.builders;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.EntityQuery;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.extraction.data.ExtractionUtils;
import cern.nxcals.api.extraction.data.builders.fluent.QueryData;
import cern.nxcals.api.extraction.data.builders.fluent.v2.KeyValueStage;
import cern.nxcals.api.extraction.data.builders.fluent.v2.KeyValueStageLoop;
import cern.nxcals.api.extraction.data.builders.fluent.v2.VariableStageLoop;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import cern.nxcals.common.annotation.Experimental;
import cern.nxcals.common.domain.ColumnMapping;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Sets;
import lombok.NonNull;
import org.apache.avro.Schema;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.DataType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static cern.nxcals.common.SystemFields.NXC_EXTR_TIMESTAMP;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VALUE;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VARIABLE_NAME;
import static java.util.Arrays.asList;

public class DataQuery extends LegacyBuildersDataQuery {
    private DataQuery(SparkSession session) {
        super(session);
    }

    public static DataQuery builder(@NonNull SparkSession session) {
        return new DataQuery(session);
    }

    public static Dataset<Row> getFor(@NonNull SparkSession sparkSession, @NonNull TimeWindow timeWindow,
            @NonNull Variable variable) {
        return DataQuery.getFor(sparkSession, timeWindow, variable.getSystemSpec().getName(),
                variable.getVariableName());
    }

    public static Dataset<Row> getFor(@NonNull SparkSession sparkSession, @NonNull TimeWindow timeWindow,
            @NonNull Entity entity) {
        return DataQuery.getFor(sparkSession, timeWindow, entity.getSystemSpec().getName(),
                new EntityQuery(entity.getEntityKeyValues()));
    }

    @SafeVarargs
    public static Dataset<Row> getFor(@NonNull SparkSession spark, @NonNull TimeWindow timeWindow,
            @NonNull String system, @NonNull Map<String, Object>... keyValuesArr) {
        return DataQuery.getFor(spark, timeWindow, system,
                Arrays.stream(keyValuesArr).map(EntityQuery::new).toArray(EntityQuery[]::new));
    }

    public static Dataset<Row> getFor(@NonNull SparkSession spark, @NonNull TimeWindow timeWindow,
            @NonNull String system, @NonNull EntityQuery... entitiesQueries) {
        KeyValueStage<Dataset<Row>> dataQuery = DataQuery.builder(spark)
                .entities().system(system);
        KeyValueStageLoop<Dataset<Row>> entities = null;
        for (EntityQuery query : entitiesQueries) {
            entities = query.hasPatterns() ?
                    dataQuery.keyValuesLike(query.toMap()) :
                    dataQuery.keyValuesEq(query.getKeyValues());
        }
        if (entities == null) {
            throw new IllegalArgumentException("No entity query passed");
        }
        return entities.timeWindow(timeWindow).build();
    }

    public static Dataset<Row> getFor(@NonNull SparkSession spark, @NonNull TimeWindow timeWindow,
            @NonNull String system, @NonNull String... variables) {
        return DataQuery.getFor(spark, timeWindow, system, asList(variables));
    }

    public static Dataset<Row> getFor(@NonNull SparkSession spark, @NonNull TimeWindow timeWindow,
            @NonNull String system, @NonNull List<String> variables) {
        return DataQuery.getFor(spark, timeWindow, system, variables, Collections.emptyList());
    }

    public static Dataset<Row> getFor(@NonNull SparkSession spark, @NonNull TimeWindow timeWindow,
            @NonNull String system, @NonNull List<String> variables,
            @NonNull List<String> variablesLike) {
        if (variables.isEmpty() && variablesLike.isEmpty()) {
            throw new IllegalArgumentException("No variable names nor variable name patterns given");
        }
        VariableStageLoop<Dataset<Row>> builder = DataQuery.builder(spark).variables()
                .system(system)
                .nameIn(variables);

        variablesLike.forEach(builder::nameLike);

        return builder.timeWindow(timeWindow).build();
    }

    /**
     * Create a dataset with variable names as columns. Values are joined on timestamps. Experimental, may be changed in the future.
     * Doesn't support extraction of variables pointing to whole entities.
     *
     * @param spark         - active spark session
     * @param timeWindow    - for data extraction
     * @param system        - system where variables are registered
     * @param variableNames - requested variables. All variables must exist - throws IllegalArgumentException otherwise.
     * @return - dataset with one column "nxcals_timestamp" and other columns named as variables
     */
    @Experimental
    public static Dataset<Row> getAsPivot(@NonNull SparkSession spark, @NonNull TimeWindow timeWindow,
            @NonNull String system, @NonNull Collection<String> variableNames) {
        return getAsPivot(spark, timeWindow, system, variableNames, Collections.emptyList());
    }

    /**
     * Create a dataset with variable names as columns. Values are joined on timestamps. Experimental, may be changed in the future.
     * Doesn't support extraction of variables pointing to whole entities.
     *
     * @param spark         - active spark session
     * @param timeWindow    - for data extraction
     * @param system        - system where variables are registered
     * @param variableNames - requested variables. All variables must exist - throws IllegalArgumentException otherwise.
     * @param variablesLike - list of variable names pattens
     * @return - dataset with one column "nxcals_timestamp" and other columns named as variables
     */
    public static Dataset<Row> getAsPivot(@NonNull SparkSession spark, @NonNull TimeWindow timeWindow,
            @NonNull String system, @NonNull Collection<String> variableNames,
            @NonNull Collection<String> variablesLike) {
        Set<Variable> variables = findVariables(system, variableNames, variablesLike);
        if (variables.isEmpty()) {
            return emptyPivotDataset(spark, system);
        }

        Map<DataType, Set<Variable>> variablesBySchema = ExtractionUtils.groupVariablesByType(spark,
                new HashSet<>(variables), timeWindow);

        List<Dataset<Row>> pivoted = new ArrayList<>();
        for (Set<Variable> vars : variablesBySchema.values()) {
            List<String> varNames = vars.stream()
                    .map(Variable::getVariableName)
                    .collect(Collectors.toList());
            Dataset<Row> dataset = getFor(spark, timeWindow, system, varNames);

            Dataset<Row> pivot = doPivotOnVariables(dataset, varNames);
            pivoted.add(pivot);
        }

        return joinOnTimestamp(pivoted)
                .orElseThrow(() -> new RuntimeException("No datasets to join"));
    }

    @VisibleForTesting
    static Dataset<Row> emptyPivotDataset(SparkSession spark, String systemName) {
        SystemSpec systemSpec = ServiceClientFactory.createSystemSpecService().findByName(systemName).orElseThrow(
                () -> new IllegalArgumentException("System " + systemName + " doesn't exist")
        );

        ColumnMapping timeFieldMapping = ColumnMapping.builder()
                .fieldName(timeFieldName(systemSpec))
                .schemaJson(systemSpec.getTimeKeyDefinitions())
                .alias(NXC_EXTR_TIMESTAMP.getValue())
                .build();

        return ExtractionUtils.createEmptyDataFrame(spark, List.of(timeFieldMapping));
    }

    @VisibleForTesting
    static String timeFieldName(SystemSpec systemSpec) {
        Schema timeKeySchema = new Schema.Parser().parse(systemSpec.getTimeKeyDefinitions());
        return timeKeySchema.getFields().stream()
                .map(Schema.Field::name)
                .findFirst()
                .orElseThrow(
                        () -> new RuntimeException("Cannot obtain time schema for system " + systemSpec.getName()));
    }

    @VisibleForTesting
    static Set<Variable> findVariables(@NonNull String system, @NonNull Collection<String> variableNames,
            @NonNull Collection<String> variablesLike) {
        VariableService service = ServiceClientFactory.createVariableService();
        List<Condition<Variables>> conditions = new ArrayList<>(1 + variableNames.size());
        if (!variableNames.isEmpty()) {
            conditions.add(Variables.suchThat().systemName().eq(system).and().variableName().in(variableNames));
        }
        for (String variableName : variablesLike) {
            conditions.add(Variables.suchThat().systemName().eq(system).and().variableName().like(variableName));
        }
        Set<Variable> variables = service.findAll(Variables.suchThat().or(conditions));
        Set<String> foundVariableNames = variables.stream().map(Variable::getVariableName).collect(Collectors.toSet());
        Set<String> missingVariableNames = Sets.difference(new HashSet<>(variableNames), foundVariableNames);
        if (!missingVariableNames.isEmpty()) {
            throw new IllegalArgumentException("Variables not found: " + missingVariableNames);
        }

        return variables;
    }

    @VisibleForTesting
    static Dataset<Row> doPivotOnVariables(Dataset<Row> dataset, List<String> variableNames) {
        return dataset.groupBy(dataset.col(NXC_EXTR_TIMESTAMP.getValue()))
                .pivot(dataset.col(NXC_EXTR_VARIABLE_NAME.getValue()), (List<Object>) (List<?>) variableNames)
                .agg(functions.any_value(dataset.col(NXC_EXTR_VALUE.getValue()), functions.lit(true)));
    }

    @VisibleForTesting
    static Optional<Dataset<Row>> joinOnTimestamp(Collection<Dataset<Row>> datasets) {
        return datasets.stream().reduce((x, y) ->
                x.join(y, NXC_EXTR_TIMESTAMP.getValue(), "fullouter"));
    }

    protected QueryData<Dataset<Row>> queryData() {
        return new QueryData<>(new SparkDatasetProducer(session));
    }

}
