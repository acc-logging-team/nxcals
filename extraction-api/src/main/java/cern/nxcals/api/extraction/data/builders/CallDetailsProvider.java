package cern.nxcals.api.extraction.data.builders;

import cern.nxcals.api.extraction.data.builders.fluent.ThreadLocalStorage;
import cern.nxcals.common.domain.CallDetails;
import lombok.RequiredArgsConstructor;
import org.apache.spark.sql.SparkSession;

import java.util.function.Supplier;

@RequiredArgsConstructor
public class CallDetailsProvider {

    private final Supplier<StackTraceElement[]> stackTraceSupplier;

    public CallDetailsProvider() {
        this(() -> Thread.currentThread().getStackTrace());
    }

    public void setCallDetails(SparkSession session) {
        StackTraceElement[] stackTrace = stackTraceSupplier.get();
        StackTraceElement element;

        int nthElementFromEnd = 2;
        int stackLength = stackTrace.length;

        if (stackLength > 0) {
            element = stackTrace[nthElementFromEnd > stackLength ? 0 : stackTrace.length - nthElementFromEnd];
        } else {
            element = new StackTraceElement("", "", null, 0);
        }

        CallDetails callDetails = CallDetails.builder().applicationName(session.sparkContext().appName())
                .declaringClass(element.getClassName()).methodName(element.getMethodName()).build();

        ThreadLocalStorage.setCallDetails(callDetails);
    }
}