package cern.nxcals.api.extraction.data.spark;

import cern.nxcals.common.annotation.Experimental;
import cern.nxcals.common.domain.ExtractionTask;
import cern.nxcals.common.domain.ExtractionTaskProcessor;
import cern.nxcals.common.domain.ExtractionUnit;
import cern.nxcals.common.utils.ConfigHolder;
import cern.nxcals.common.utils.StreamUtils;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

import static cern.nxcals.api.extraction.data.ExtractionUtils.createEmptyDataFrame;
import static cern.nxcals.common.Constants.DATASET_BUILD_PARALLELIZE_ABOVE_SIZE;
import static cern.nxcals.common.Constants.DEFAULT_PARALLELIZE_ABOVE_SIZE;
import static java.util.stream.Collectors.toList;
import static lombok.AccessLevel.PACKAGE;

@Slf4j
@RequiredArgsConstructor(access = PACKAGE)
public class SparkExtractionTaskExecutor {
    @NonNull
    private final SparkSession session;
    @NonNull
    private final SparkExtractionTaskProcessor processor;

    @NonNull
    private final Function<ExtractionUnit, ExtractionTaskProcessor<ExtractionTask>> preProcessorMaker;

    @NonNull
    private final Function<ExtractionUnit, UnaryOperator<Dataset<Row>>> postProcessorMaker;

    public SparkExtractionTaskExecutor(SparkSession session) {
        //@formatter:off
        this(session, new SparkExtractionTaskProcessor(
                        new HdfsDatasetCreator(session),
                        new HBaseDatasetCreator(session)),
                ExtractionTaskPreProcessor::new,
                DatasetPostProcessor::new
        );
        //@formatter:on
    }

    public Dataset<Row> execute(ExtractionUnit unit) {
        //@formatter:off
        return createDatasetStream(preProcessUnit(unit))
                .reduce(Dataset::union)
                .map(d -> postProcessorMaker.apply(unit).apply(d))
                .orElseGet(() -> emptyFrameFrom(unit));
        //@formatter:on
    }

    @Experimental
    public List<Dataset<Row>> executeExpanded(ExtractionUnit unit) {
        return createDatasetStream(preProcessUnit(unit))
                .map(d -> postProcessorMaker.apply(unit).apply(d)).collect(toList());
    }

    private List<ExtractionTask> preProcessUnit(ExtractionUnit unit) {
        ExtractionTaskProcessor<ExtractionTask> preProcessor = preProcessorMaker.apply(unit);
        return unit.getTasks().stream().map(t -> t.processWith(preProcessor)).collect(toList());
    }

    private Stream<Dataset<Row>> createDatasetStream(List<ExtractionTask> tasks) {
        log.debug("Getting data set for {} extraction tasks", tasks.size());
        //@formatter:off
        return StreamUtils
                .of(tasks, ConfigHolder.getInt(DATASET_BUILD_PARALLELIZE_ABOVE_SIZE, DEFAULT_PARALLELIZE_ABOVE_SIZE))
                .map(this::toOptionalDataset)
                .filter(Optional::isPresent)
                .map(Optional::get);
        //@formatter:on
    }

    private Optional<Dataset<Row>> toOptionalDataset(ExtractionTask t) {
        return t.processWith(this.processor);
    }

    private Dataset<Row> emptyFrameFrom(ExtractionUnit unit) {
        log.info("Received null or empty list of extraction tasks, will create dataset with default columns!");
        return createEmptyDataFrame(session, unit.getEmptyDatasetMapping());
    }

}
