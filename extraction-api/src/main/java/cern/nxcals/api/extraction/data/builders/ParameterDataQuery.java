package cern.nxcals.api.extraction.data.builders;

import cern.nxcals.api.extraction.data.builders.fluent.QueryData;
import cern.nxcals.api.extraction.data.builders.fluent.v2.DeviceStage;
import cern.nxcals.api.extraction.data.builders.fluent.v2.StageSequenceDeviceProperty;
import cern.nxcals.api.extraction.data.builders.fluent.v2.SystemStage;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ParameterDataQuery extends DevicePropertyStaticMethods {
    public static SystemStage<DeviceStage<Dataset<Row>>, Dataset<Row>> builder(
            SparkSession session) {
        return StageSequenceDeviceProperty.<Dataset<Row>>sequence()
                .apply(new QueryData<>(new SparkDatasetProducer(session)));
    }
}
