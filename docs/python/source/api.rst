.. _API_docs:

NXCALS Data Extraction API documentation
========================================

.. rubric:: Modules

.. autosummary::
   :toctree: api

   .. Add the sub-packages that you wish to document below

   nxcals.api.extraction.data.builders
   pytimber
