package cern.nxcals.docs.metadataapi;

import cern.nxcals.api.custom.domain.Snapshot;
import cern.nxcals.api.custom.domain.Snapshot.TimeDefinition;
import cern.nxcals.api.custom.extraction.metadata.SnapshotService;
import cern.nxcals.api.custom.extraction.metadata.SnapshotServiceFactory;
import cern.nxcals.api.custom.extraction.metadata.queries.Snapshots;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.extraction.metadata.EntityService;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.extraction.metadata.queries.SystemSpecs;
import cern.nxcals.api.metadata.queries.Entities;
import cern.nxcals.api.metadata.queries.Variables;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import java.time.Instant;
import java.util.Map;
import java.util.Set;

public class SnapshotServiceSnippets {
    public void snapshotSearch() {
        // @snippet:1:on
        SnapshotService snapshotService = SnapshotServiceFactory.createSnapshotService();
        SystemSpecService systemService = ServiceClientFactory.createSystemSpecService();

        SystemSpec system = systemService.findOne(SystemSpecs.suchThat().name().eq("CMW"))
                .orElseThrow(() -> new IllegalArgumentException("System not found"));

        Snapshot snapshot = snapshotService.findOne(
                        Snapshots.suchThat().systemId().eq(system.getId()).and().name().eq("Demo Snapshot")
                                .and().variableName().like("%"))
                .orElseThrow(() -> new IllegalArgumentException("Snapshot not found"));

        // Retrieve attached variables per association
        Map<String, Set<Variable>> associationVariableMap = snapshotService.getVariables(snapshot.getId());
        // @snippet:1:off
    }

    public void creatSnapshot() {
        // @snippet:2:on
        SnapshotService snapshotService = SnapshotServiceFactory.createSnapshotService();
        SystemSpecService systemService = ServiceClientFactory.createSystemSpecService();
        EntityService entityService = ServiceClientFactory.createEntityService();
        VariableService variableService = ServiceClientFactory.createVariableService();

        SystemSpec system = systemService.findOne(SystemSpecs.suchThat().name().eq("CMW"))
                .orElseThrow(() -> new IllegalArgumentException("System not found"));

        TimeDefinition timeDefinition = TimeDefinition.fixedDates(
                Instant.parse("2023-05-15T20:17:30.151238525Z"),
                Instant.parse("2023-05-16T05:15:03.533738525Z"));

        Snapshot snapshot = Snapshot.builder().timeDefinition(timeDefinition).build();

        Map<String, Object> keyValues = ImmutableMap.of(
                "device", "LHC.LUMISERVER",
                "property", "CrossingAngleIP1");

        Entity entity = entityService.findOne(
                        Entities.suchThat().keyValues().eq(system, keyValues))
                .orElseThrow(() -> new IllegalArgumentException("Entity not found"));

        Variable variable = variableService.findOne(
                Variables.suchThat().variableName().eq("DEMO:VARIABLE")
        ).orElseThrow(() -> new IllegalArgumentException("Variable not found"));

        Snapshot created = snapshotService.create(snapshot,
                ImmutableSet.of(entity), ImmutableSet.of(variable), ImmutableSet.of(), ImmutableSet.of(), ImmutableSet.of());
        // @snippet:2:off
    }
}
