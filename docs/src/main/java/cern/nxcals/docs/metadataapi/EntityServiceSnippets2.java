package cern.nxcals.docs.metadataapi;

import avro.shaded.com.google.common.collect.ImmutableMap;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.extraction.metadata.EntityService;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.metadata.queries.Entities;
import cern.nxcals.api.metadata.queries.EntityQueryWithOptions;

import java.util.List;
import java.util.Map;

public class EntityServiceSnippets2 {
    public void entitiesSearchWithOptions() {
        // @snippet:1:on
        Entities.suchThat().systemId().eq(1L).withOptions().limit(10);
        // @snippet:1:off
        // @snippet:2:on
        Entities.suchThat().systemId().eq(1L).withOptions().offset(10).limit(10);
        // @snippet:2:off
        // @snippet:3:on
        Entities.suchThat().systemId().eq(1L).withOptions().orderBy().keyValues();
        // @snippet:3:off
        // @snippet:4:on
        SystemSpec systemData = ServiceClientFactory.createSystemSpecService()
                .findByName("CMW")
                .orElseThrow(() -> new IllegalArgumentException("System not found"));
        Map<String, Object> keyValues = ImmutableMap.of("device", "LHC.LUMISERVER", "property", "CrossingAngleIP%");

        EntityService entityService = ServiceClientFactory.createEntityService();

        EntityQueryWithOptions query = Entities.suchThat()
                .keyValues().like(systemData, keyValues)
                .withOptions()
                .withHistory("2017-10-10 14:15:00.000000000", "2020-10-26 14:15:00.000000000")
                .limit(2)
                .orderBy().keyValues().desc();

        List<Entity> entities = entityService.findAll(query);
        // @snippet:4:off
    }
}
