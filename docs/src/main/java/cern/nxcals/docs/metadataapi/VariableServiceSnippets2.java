package cern.nxcals.docs.metadataapi;

import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.metadata.queries.VariableQueryWithOptions;
import cern.nxcals.api.metadata.queries.Variables;

import java.util.List;

public class VariableServiceSnippets2 {
    public void getSortedListOfVariables() {
        // @snippet:1:on
        VariableService variableService = ServiceClientFactory.createVariableService();

        VariableQueryWithOptions query = Variables.suchThat()
                .variableName().like("SPS%")
                .withOptions()
                .noConfigs()
                .orderBy().variableName().asc()
                .limit(10);

        List<Variable> variables = variableService.findAll(query);
        // @snippet:1:off
    }
}
