package cern.nxcals.docs.metadataapi;

import cern.nxcals.api.custom.domain.Tag;
import cern.nxcals.api.custom.extraction.metadata.TagService;
import cern.nxcals.api.custom.extraction.metadata.TagServiceFactory;
import cern.nxcals.api.custom.extraction.metadata.queries.Tags;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.Visibility;
import cern.nxcals.api.extraction.metadata.EntityService;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.extraction.metadata.queries.SystemSpecs;
import cern.nxcals.api.metadata.queries.Entities;
import cern.nxcals.api.metadata.queries.Variables;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static cern.nxcals.api.custom.extraction.metadata.TagService.DEFAULT_RELATION;

public class TagServiceSnippets {

    public void tagSearch() {
        // @snippet:1:on
        TagService tagService = TagServiceFactory.createTagService();
        SystemSpecService systemService = ServiceClientFactory.createSystemSpecService();

        SystemSpec system = systemService.findOne(SystemSpecs.suchThat().name().eq("CMW"))
                .orElseThrow(() -> new IllegalArgumentException("System not found"));

        Optional<Tag> maybeTag = tagService.findOne(Tags.suchThat().name().eq("Demo Tag"));
        // @snippet:1:off
    }

    public void tagCreate() {
        // @snippet:2:on
        SystemSpecService systemService = ServiceClientFactory.createSystemSpecService();
        EntityService entityService = ServiceClientFactory.createEntityService();
        VariableService variableService = ServiceClientFactory.createVariableService();
        TagService tagService = TagServiceFactory.createTagService();

        String tagOwner = "owner";
        String systemName = "CMW";

        SystemSpec system = systemService.findOne(SystemSpecs.suchThat().name().eq(systemName))
                .orElseThrow(() -> new IllegalArgumentException("System not found"));

        Instant tagTimestamp = Instant.now();

        Map<String, Object> keyValues = ImmutableMap.of(
                "device", "LHC.LUMISERVER",
                "property", "CrossingAngleIP1");

        Entity entity = entityService.findOne(
                        Entities.suchThat().keyValues().eq(system, keyValues))
                .orElseThrow(() -> new IllegalArgumentException("Entity not found"));

        Variable variable = variableService.findOne(
                        Variables.suchThat().systemId().eq(system.getId()).and().variableName().eq("DEMO:VARIABLE"))
                .orElseThrow(() -> new IllegalArgumentException("Variable not found"));

        ImmutableSet<Entity> entities = ImmutableSet.of(entity);
        ImmutableSet<Variable> variables = ImmutableSet.of(variable);

        Tag tag = tagService.create("My Tag", "Demo Tag", Visibility.PUBLIC,
                tagOwner, systemName, tagTimestamp, entities, variables);

        // Extract entities and variables from the tag for future data extraction
        Set<Entity> entitiesSet = tagService.getEntities(tag.getId()).get(DEFAULT_RELATION);
        Set<Variable> variablesSet = tagService.getVariables(tag.getId()).get(DEFAULT_RELATION);
        // @snippet:2:off
    }

    public void tagUpdate() {
        // @snippet:3:on
        SystemSpecService systemService = ServiceClientFactory.createSystemSpecService();
        VariableService variableService = ServiceClientFactory.createVariableService();
        TagService tagService = TagServiceFactory.createTagService();

        SystemSpec system = systemService.findByName("CMW")
                .orElseThrow(() -> new IllegalArgumentException("System not found"));

        Tag tag = tagService.findOne(Tags.suchThat()
                        .systemId().eq(system.getId())
                        .and().name().eq("Demo Tag"))
                .orElseThrow(() -> new IllegalArgumentException("Tag not found"));

        // Change tag description
        Tag newTag = tag.toBuilder().description("New description").build();

        // Perform the update
        Tag updatedTag = tagService.update(newTag);

        // Add variables
        Map<String, Set<Long>> tagVariables = new HashMap<>();

        Variable variable = variableService.findOne(Variables.suchThat().systemId().eq(system.getId()).and().variableName().eq("DEMO:VARIABLE"))
                .orElseThrow(() -> new IllegalArgumentException("Variable not found"));

        tagVariables.put(DEFAULT_RELATION, Sets.newHashSet(variable.getId()));

        tagService.addVariables(newTag.getId(), tagVariables);
        // @snippet:3:off
    }
}
