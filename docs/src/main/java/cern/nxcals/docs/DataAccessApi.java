package cern.nxcals.docs;

import cern.nxcals.api.domain.EntityQuery;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.extraction.data.builders.DevicePropertyDataQuery;
import cern.nxcals.api.extraction.data.builders.ParameterDataQuery;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

class DataAccessApi {
    private final SparkSession spark;

    DataAccessApi(SparkSession spark) {
        this.spark = spark;
    }

    void dataQueryForKeyValues() {

        // @snippet:1:on
        Dataset<Row> df1 = DataQuery.builder(spark).byEntities()
                .system("WINCCOA")
                .startTime("2018-06-15 00:00:00.000").endTime("2018-06-17 00:00:00.000")
                .entity().keyValue("variable_name", "MB.C16L2:U_HDS_3")
                .build();

        Dataset<Row> df2 = DataQuery.builder(spark).byEntities()
                .system("CMW")
                .startTime("2018-04-29 00:00:00.000").endTime("2018-04-30 00:00:00.000")
                .entity().keyValue("device", "LHC.LUMISERVER").keyValue("property", "CrossingAngleIP1")
                .build();

        Map<String, Object> keyValues = new HashMap<>();
        keyValues.put("device", "LHC.LUMISERVER");
        keyValues.put("property", "CrossingAngleIP1");

        Dataset<Row> df3 = DataQuery.builder(spark).byEntities()
                .system("CMW")
                .startTime("2018-04-29 00:00:00.000").endTime("2018-04-30 00:00:00.000")
                .entity()
                .keyValues(keyValues)
                .build();

        Map<String, Object> keyValuesLike = new HashMap<>();
        keyValuesLike.put("device", "LHC.LUMISERVER");
        keyValuesLike.put("property", "CrossingAngleIP%");

        Dataset<Row> df4 = DataQuery.builder(spark).byEntities()
                .system("CMW")
                .startTime("2018-04-29 00:00:00.000").endTime("2018-04-30 00:00:00.000")
                .entity().keyValuesLike(keyValuesLike)
                .build();
        // @snippet:1:off
    }

    void dataQueryForVariables() {
        // @snippet:2:on
        Dataset<Row> df1 = DataQuery.builder(spark).byVariables()
                .system("CMW")
                .startTime("2018-04-29 00:00:00.000").endTime("2018-04-30 00:00:00.000")
                .variable("LTB.BCT60:INTENSITY")
                .build();

        Dataset<Row> df2 = DataQuery.builder(spark).byVariables()
                .system("CMW")
                .startTime("2018-04-29 00:00:00.000").endTime("2018-04-30 00:00:00.000")
                .variableLike("LTB.BCT%:INTENSITY")
                .build();

        Dataset<Row> df3 = DataQuery.builder(spark).byVariables()
                .system("CMW")
                .startTime("2018-04-29 00:00:00.000").endTime("2018-04-30 00:00:00.000")
                .variableLike("LTB.BCT%:INTENSITY")
                .variable("LTB.BCT60:INTENSITY")
                .build();
        // @snippet:2:off
    }

    void dataQueryForDeviceProperty() {
        // @snippet:3:on
        Dataset<Row> df1 = DevicePropertyDataQuery.builder(spark)
                .system("CMW")
                .startTime("2017-08-29 00:00:00.000").duration(10000000000l)
                .entity().parameter("RADMON.PS-10/ExpertMonitoringAcquisition")
                .build();

        List<String> fieldAliasesList = new ArrayList<>();
        fieldAliasesList.add("current_18V");
        fieldAliasesList.add("voltage_18V");

        Map<String, List<String>> fieldAliases = new HashMap<>();
        fieldAliases.put("CURRENT 18V", fieldAliasesList);

        Dataset<Row> df2 = DevicePropertyDataQuery.builder(spark)
                .system("CMW")
                .startTime("2018-04-29 00:00:00.000").endTime("2018-04-30 00:00:00.000")
                .fieldAliases(fieldAliases)
                .entity().device("RADMON.PS-1").property("ExpertMonitoringAcquisition")
                .entity().parameter("RADMON.PS-10/ExpertMonitoringAcquisition")
                .build();
        df2.printSchema();

        Dataset<Row> df3 = DevicePropertyDataQuery.builder(spark)
                .system("CMW")
                .startTime("2017-08-29 00:00:00.000").duration(10000000000l)
                .entity().parameterLike("RADMON.PS-%/ExpertMonitoringAcquisition")
                .build();
        // @snippet:3:off
    }

    void dataQueryForKeyValuesUsingStaticFunctions() {
        // @snippet:4:on
        TimeWindow timeWindow1 = TimeWindow.fromStrings(
                "2018-06-15 00:00:00.000", "2018-06-17 00:00:00.000");
        Dataset<Row> df1 = DataQuery.builder(spark).byEntities()
                .system("WINCCOA")
                .startTime(timeWindow1.getStartTime()).endTime(timeWindow1.getEndTime())
                .entity().keyValue("variable_name", "MB.C16L2:U_HDS_3")
                .build();

        // or
        Dataset<Row> df1static = DataQuery.getFor(spark, timeWindow1, "WINCCOA",
                ImmutableMap.of("variable_name", "MB.C16L2:U_HDS_3"));
        // @snippet:4:off
        // @snippet:5:on
        TimeWindow timeWindow2 = TimeWindow.fromStrings(
                "2018-04-29 00:00:00.000", "2018-04-30 00:00:00.000");
        Dataset<Row> df2 = DataQuery.builder(spark).byEntities()
                .system("CMW")
                .startTime(timeWindow2.getStartTime()).endTime(timeWindow2.getEndTime())
                .entity().keyValue("device", "LHC.LUMISERVER")
                .keyValue("property", "CrossingAngleIP1").build();

        // or
        Dataset<Row> df2static = DataQuery.getFor(spark, timeWindow2, "CMW",
                ImmutableMap.of("device", "LHC.LUMISERVER",
                        "property", "CrossingAngleIP1"));
        // @snippet:5:off
        // @snippet:6:on
        TimeWindow timeWindow3 = TimeWindow.fromStrings(
                "2018-04-29 00:00:00.000", "2018-04-30 00:00:00.000");
        Map<String, Object> keyValues = ImmutableMap.of(
                "device", "LHC.LUMISERVER",
                "property", "CrossingAngleIP1"
        );

        Dataset<Row> df3 = DataQuery.builder(spark).entities()
                .system("CMW")
                .keyValuesEq(keyValues)
                .timeWindow(timeWindow3)
                .build();

        //or
        Dataset<Row> df3static = DataQuery.getFor(spark, timeWindow3, "CMW", keyValues);
        // @snippet:6:off
        // @snippet:7:on
        TimeWindow timeWindow4 = TimeWindow.fromStrings(
                "2018-04-29 00:00:00.000", "2018-04-30 00:00:00.000");
        Map<String, Object> keyValuesLike = ImmutableMap.of(
                "device", "LHC.LUMISERVER",
                "property", "CrossingAngleIP%"
        );

        Dataset<Row> df4 = DataQuery.builder(spark).entities()
                .system("CMW")
                .keyValuesLike(keyValuesLike)
                .timeWindow(timeWindow4)
                .build();

        // or
        Dataset<Row> df4static = DataQuery.getFor(spark, timeWindow4, "CMW",
                new EntityQuery(Collections.emptyMap(), keyValuesLike));
        // @snippet:7:off
        // @snippet:8:on
        TimeWindow timeWindow5 = TimeWindow.fromStrings(
                "2018-04-29 00:00:00.000", "2018-04-30 00:00:00.000");
        Dataset<Row> df5 = DataQuery.builder(spark).entities()
                .system("CMW")
                .keyValuesLike(keyValuesLike)
                .keyValuesEq(keyValues)
                .timeWindow(timeWindow5)
                .build();

        // or
        Dataset<Row> df5static = DataQuery.getFor(spark, timeWindow5, "CMW",
                new EntityQuery(keyValues),
                new EntityQuery(new HashMap<>(), keyValuesLike));
        // @snippet:8:off
    }

    void dataQueryForVariablesUsingStaticFunctions() {
        // @snippet:9:on
        TimeWindow timeWindow1 = TimeWindow.fromStrings(
                "2018-04-29 00:00:00.000", "2018-04-30 00:00:00.000");
        Dataset<Row> df1 = DataQuery.builder(spark).variables()
                .system("CMW")
                .nameEq("LTB.BCT60:INTENSITY")
                .timeWindow(timeWindow1)
                .build();

        // or
        Dataset<Row> df1static =
                DataQuery.getFor(spark, timeWindow1, "CMW", "LTB.BCT60:INTENSITY");
        // @snippet:9:off
        // @snippet:10:on
        TimeWindow timeWindow2 = TimeWindow.fromStrings(
                "2018-04-29 00:00:00.000", "2018-04-30 00:00:00.000");
        Dataset<Row> df2 = DataQuery.builder(spark).variables()
                .system("CMW")
                .nameLike("LTB.BCT%:INTENSITY")
                .timeWindow(timeWindow2)
                .build();

        // or
        List<String> variablePatterns = ImmutableList.of("LTB.BCT%:INTENSITY");
        Dataset<Row> df2static = DataQuery.getFor(spark, timeWindow2, "CMW",
                Collections.emptyList(), variablePatterns);
        // @snippet:10:off
        // @snippet:11:on
        TimeWindow timeWindow3 = TimeWindow.fromStrings(
                "2018-04-29 00:00:00.000", "2018-04-30 00:00:00.000");
        Dataset<Row> df3 = DataQuery.builder(spark).variables()
                .system("CMW")
                .nameLike("LTB.BCT%:INTENSITY")
                .nameEq("LTB.BCT60:INTENSITY")
                .timeWindow(timeWindow3)
                .build();

        // or
        List<String> variables = ImmutableList.of("LTB.BCT60:INTENSITY");
        Dataset<Row> df3static =
                DataQuery.getFor(spark, timeWindow3, "CMW", variables, variablePatterns);
        // @snippet:11:off
    }

    void dataQueryForKeyValues_v2() {
        // @snippet:21:on
        Map<String, Object> keyValues = new HashMap<>();
        keyValues.put("device", "LHC.LUMISERVER");
        keyValues.put("property", "CrossingAngleIP1");

        Dataset<Row> df1 = DataQuery.builder(spark).entities()
                .system("CMW")
                .keyValuesEq(keyValues)
                .timeWindow("2018-04-29 00:00:00.000", "2018-04-30 00:00:00.000")
                .build();

        Map<String, Object> keyValuesLike = new HashMap<>();
        keyValuesLike.put("device", "LHC.LUMISERVER");
        keyValuesLike.put("property", "CrossingAngleIP%");

        Dataset<Row> df2 = DataQuery.builder(spark).entities()
                .system("CMW")
                .keyValuesLike(keyValuesLike)
                .timeWindow("2018-04-29 00:00:00.000", "2018-04-30 00:00:00.000")
                .build();

        Dataset<Row> df3 = DataQuery.builder(spark).entities()
                .system("CMW")
                .idEq(57336)
                .timeWindow("2018-04-29 00:00:00.000", "2018-04-30 00:00:00.000")
                .build();
        // @snippet:21:off
    }

    void dataQueryForVariables_v2() {
        // @snippet:22:on
        Dataset<Row> df1 = DataQuery.builder(spark).variables()
                .system("CMW")
                .nameEq("LTB.BCT60:INTENSITY")
                .timeWindow("2018-04-29 00:00:00.000", "2018-04-30 00:00:00.000")
                .build();

        Dataset<Row> df2 = DataQuery.builder(spark).variables()
                .system("CMW")
                .nameLike("LTB.BCT%:INTENSITY")
                .timeWindow("2018-04-29 00:00:00.000", "2018-04-30 00:00:00.000")
                .build();

        Dataset<Row> df3 = DataQuery.builder(spark).variables()
                .system("CMW")
                .nameLike("LTB.BCT%:INTENSITY")
                .nameEq("LTB.BCT60:INTENSITY")
                .timeWindow("2018-04-29 00:00:00.000", "2018-04-30 00:00:00.000")
                .build();

        Dataset<Row> df4 = DataQuery.builder(spark).variables()
                .system("CMW")
                .idEq(1005050)
                .idIn(Sets.newHashSet(1011562L))
                .timeWindow("2018-04-29 00:00:00.000", "2018-04-30 00:00:00.000")
                .build();
        // @snippet:22:off
    }

    void dataQueryForParameter() {
        // @snippet:23:on
        Dataset<Row> df1 = ParameterDataQuery.builder(spark)
                .system("CMW")
                .parameterEq("RADMON.PS-10/ExpertMonitoringAcquisition")
                .timeWindow("2017-08-29 00:00:00.000", "2017-08-29 00:00:10.000")
                .build();

        Set<String> fieldAliasesSet = new HashSet<>();
        fieldAliasesSet.add("current_18V");
        fieldAliasesSet.add("voltage_18V");

        Map<String, Set<String>> fieldAliases = new HashMap<>();
        fieldAliases.put("CURRENT 18V", fieldAliasesSet);

        Dataset<Row> df2 = ParameterDataQuery.builder(spark)
                .system("CMW")
                .parameterIn("RADMON.PS-1/ExpertMonitoringAcquisition", "RADMON.PS-10/ExpertMonitoringAcquisition")
                .timeWindow("2018-04-29 00:00:00.000", "2018-04-30 00:00:00.000")
                .fieldAliases(fieldAliases)
                .build();
        df2.printSchema();

        Dataset<Row> df3 = ParameterDataQuery.builder(spark)
                .system("CMW")
                .parameterLike("RADMON.PS-%/ExpertMonitoringAcquisition")
                .timeWindow("2018-04-29 00:00:00.000", "2018-04-30 00:00:00.000")
                .build();
        // @snippet:23:off
    }

    void dataQueryPivot() {
        // @snippet:24:on
        TimeWindow timeWindow = TimeWindow.fromStrings("2018-04-29 00:00:00.000", "2018-04-30 00:00:00.000");

        Dataset<Row> dataset = DataQuery.getAsPivot(spark, timeWindow, "CMW", Collections.emptyList(),
                List.of("LTB.BCT%:INTENSITY"));
        dataset.printSchema();
        // @snippet:24:off
    }

    void dataQueryPivotByName() {
        // @snippet:25:on
        TimeWindow timeWindow = TimeWindow.fromStrings("2018-04-29 00:00:00.000", "2018-04-30 00:00:00.000");

        Dataset<Row> dataset = DataQuery.getAsPivot(spark, timeWindow, "CMW", List.of("LTB.BCT50:INTENSITY"));
        dataset.printSchema();
        // @snippet:25:off
    }
}
