package cern.nxcals.docs.thindataaccess1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// @snippet:1:on
public class ThinDataAccessExample {

    static {
        System.setProperty("logging.config", "classpath:log4j2.yml");
        System.setProperty("service.url",
                "https://cs-ccr-nxcals6.cern.ch:19093,https://cs-ccr-nxcals7.cern.ch:19093,https://cs-ccr-nxcals8.cern.ch:19093");

    }

    private static final Logger log = LoggerFactory.getLogger(ThinDataAccessExample.class);
    private static final String SYSTEM_NAME = "CMW";
    //NXCALS PRO, Please use all or subset of Spark Servers addresses
    private static String SPARK_SERVERS_URL = "nxcals-spark-thin-api-lb:14500,cs-ccr-nxcals5.cern.ch:15000,cs-ccr-nxcals6.cern.ch:15000,cs-ccr-nxcals7.cern.ch:15000,cs-ccr-nxcals8.cern.ch:15000";


    public static void main(String[] args) {

        ThinDataAccessExample thinDataAccessExample = new ThinDataAccessExample();

    }
}
// @snippet:1:off
