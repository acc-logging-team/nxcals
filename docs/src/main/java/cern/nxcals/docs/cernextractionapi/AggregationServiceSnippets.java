package cern.nxcals.docs.cernextractionapi;

import cern.nxcals.api.config.SparkProperties;
import cern.nxcals.api.custom.service.AggregationService;
import cern.nxcals.api.custom.service.Services;
import cern.nxcals.api.custom.service.aggregation.AggregationFunction;
import cern.nxcals.api.custom.service.aggregation.AggregationFunctions;
import cern.nxcals.api.custom.service.aggregation.DatasetAggregationProperties;
import cern.nxcals.api.custom.service.aggregation.WindowAggregationProperties;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.extraction.metadata.EntityService;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.extraction.metadata.queries.Entities;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import cern.nxcals.api.utils.TimeUtils;
import com.google.common.collect.ImmutableMap;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Map;

import static cern.nxcals.api.custom.domain.CmwSystemConstants.DEVICE_KEY_NAME;
import static cern.nxcals.api.custom.domain.CmwSystemConstants.PROPERTY_KEY_NAME;

public class AggregationServiceSnippets {
    private static final VariableService variableService = ServiceClientFactory.createVariableService();
    private static final EntityService entityService = ServiceClientFactory.createEntityService();
    private static final SystemSpecService systemService = ServiceClientFactory.createSystemSpecService();

    private static final AggregationService aggregationService = getService();

    public static AggregationService getService() {
        // @snippet:1:on
        AggregationService aggregationService = Services.newInstance(SparkProperties.defaults("MY_APP"))
                .aggregationService();
        // @snippet:1:off
        return aggregationService;
    }

    public static void getAggregatedVariableDataAvg() {
        // @snippet:2:on
        Variable myVariable = variableService.findOne(Variables.suchThat()
                .variableName().eq("CPS.TGM:CYCLE"))
                .orElseThrow(() -> new IllegalArgumentException("Could not obtain variable from service"));
        Instant startTime = TimeUtils.getInstantFromString("2020-04-25 00:00:00.000000000");
        Instant endTime = TimeUtils.getInstantFromString("2020-04-27 00:00:00.000000000");

        WindowAggregationProperties properties = WindowAggregationProperties.builder()
                .timeWindow(startTime, endTime)
                .interval(8, ChronoUnit.HOURS)
                .function((AggregationFunctions.AVG)).build();
        Dataset<Row> dataset = aggregationService.getData(myVariable, properties);
        dataset.show();
        // @snippet:2:off
    }

    public static void getAggregatedEntityDataMax() {
        SystemSpec cmwSystemSpec = systemService.findByName("CMW")
                .orElseThrow(() -> new IllegalArgumentException("No such system"));
        // @snippet:3:on
        Map<String, Object> keyValues = ImmutableMap
                .of(DEVICE_KEY_NAME, "CPS.TGM", PROPERTY_KEY_NAME, "FULL-TELEGRAM.STRC");

        Entity myEntity = entityService.findOne(Entities.suchThat()
                .keyValues().eq(cmwSystemSpec, keyValues))
                .orElseThrow(() -> new IllegalArgumentException("Could not obtain entity from service"));
        Instant startTime = TimeUtils.getInstantFromString("2020-04-10 00:00:00.000000000");
        Instant endTime = TimeUtils.getInstantFromString("2020-04-14 00:00:10.000000000");

        WindowAggregationProperties properties = WindowAggregationProperties.builder()
                .timeWindow(startTime, endTime)
                .interval(1, ChronoUnit.DAYS)
                .function((AggregationFunctions.MAX))
                .aggregationField("CYCLE")
                .build();
        Dataset<Row> dataset = aggregationService.getData(myEntity, properties);
        dataset.show();
        // @snippet:3:off
    }

    public static void getAggregatedVariableDataRepeat() {
        // @snippet:4:on
        Variable myVariable = variableService.findOne(Variables.suchThat()
                .variableName().eq("CPS.TGM:CYCLE"))
                .orElseThrow(() -> new IllegalArgumentException("Could not obtain variable from service"));
        Instant startTime = TimeUtils.getInstantFromString("2020-04-25 00:00:00.000000000");
        Instant endTime = TimeUtils.getInstantFromString("2020-04-25 00:05:00.000000000");

        WindowAggregationProperties properties = WindowAggregationProperties.builder()
                .timeWindow(startTime, endTime)
                .interval(20, ChronoUnit.SECONDS)
                .function((AggregationFunctions.REPEAT)).build();
        Dataset<Row> dataset = aggregationService.getData(myVariable, properties);
        dataset.show();
        // @snippet:4:off
    }

    public static void getAggregatedEntityDataInterpolate() {
        SystemSpec cmwSystemSpec = systemService.findByName("CMW")
                .orElseThrow(() -> new IllegalArgumentException("No such system"));
        // @snippet:5:on
        Map<String, Object> keyValues = ImmutableMap
                .of(DEVICE_KEY_NAME, "CPS.TGM", PROPERTY_KEY_NAME, "FULL-TELEGRAM.STRC");

        Entity myEntity = entityService.findOne(Entities.suchThat()
                .keyValues().eq(cmwSystemSpec, keyValues))
                .orElseThrow(() -> new IllegalArgumentException("Could not obtain entity from service"));
        Instant startTime = TimeUtils.getInstantFromString("2020-02-10 00:00:00.000000000");
        Instant endTime = TimeUtils.getInstantFromString("2020-08-10 00:00:00.000000000");

        AggregationFunction customInterpolateFunc = AggregationFunctions.INTERPOLATE
                .expandTimeWindowBy(1, 0, ChronoUnit.MONTHS);

        WindowAggregationProperties properties = WindowAggregationProperties.builder()
                .timeWindow(startTime, endTime)
                .interval(2, ChronoUnit.MONTHS)
                .function(customInterpolateFunc)
                .aggregationField("CYCLE")
                .build();
        Dataset<Row> dataset = aggregationService.getData(myEntity, properties);
        dataset.show();
        // @snippet:5:off
    }

    public static void getVariableDataAlignedToDataset() {
        // @snippet:6:on
        SparkSession sparkSession = SparkSession.active();

        Variable drivingVariable = variableService.findOne(Variables.suchThat()
                .variableName().eq("HX:FILLN"))
                .orElseThrow(() -> new IllegalArgumentException("Could not obtain variable from service"));
        Instant startTime = TimeUtils.getInstantFromString("2016-03-01 00:00:00.000000000");
        Instant endTime = TimeUtils.getInstantFromString("2016-05-01 00:00:00.000000000");
        Dataset<Row> drivingDataset = DataQuery.getFor(sparkSession,
                TimeWindow.between(startTime, endTime), drivingVariable);

        Variable myVariable = variableService.findOne(Variables.suchThat().variableName().eq("CPS.TGM:CYCLE"))
                .orElseThrow(() -> new IllegalArgumentException("Could not obtain variable from service"));

        DatasetAggregationProperties properties = DatasetAggregationProperties.builder()
                .drivingDataset(drivingDataset).build();
        Dataset<Row> dataset = aggregationService.getData(myVariable, properties);
        dataset.show();
        // @snippet:6:off
    }

    public static void getEntityDataAlignedToDataset() {
        Variable drivingVariable = variableService.findOne(Variables.suchThat()
                .variableName().eq("HX:FILLN"))
                .orElseThrow(() -> new IllegalArgumentException("Could not obtain variable from service"));
        Instant startTime = TimeUtils.getInstantFromString("2016-03-01 00:00:00.000000000");
        Instant endTime = TimeUtils.getInstantFromString("2016-05-01 00:00:00.000000000");
        Dataset<Row> drivingDataset = DataQuery.getFor(SparkSession.active(),
                TimeWindow.between(startTime, endTime), drivingVariable);

        SystemSpec cmwSystemSpec = systemService.findByName("CMW")
                .orElseThrow(() -> new IllegalArgumentException("No such system"));
        // @snippet:7:on
        Map<String, Object> keyValues = ImmutableMap
                .of(DEVICE_KEY_NAME, "CPS.TGM", PROPERTY_KEY_NAME, "FULL-TELEGRAM.STRC");

        Entity myEntity = entityService.findOne(Entities.suchThat()
                .keyValues().eq(cmwSystemSpec, keyValues))
                .orElseThrow(() -> new IllegalArgumentException("Could not obtain entity from service"));

        DatasetAggregationProperties properties = DatasetAggregationProperties.builder()
                .drivingDataset(drivingDataset)
                .aggregationField("CYCLE")
                .build();
        Dataset<Row> dataset = aggregationService.getData(myEntity, properties);
        dataset.show();
        // @snippet:7:off
    }

}
