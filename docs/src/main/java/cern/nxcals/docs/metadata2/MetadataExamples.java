package cern.nxcals.docs.metadata2;

// @snippet:1:on
import cern.nxcals.api.backport.client.service.ServiceBuilder;
import cern.nxcals.api.backport.client.service.TimeseriesDataService;
import cern.nxcals.api.backport.domain.core.metadata.Variable;
import cern.nxcals.api.backport.domain.core.metadata.VariableSet;
import cern.nxcals.api.backport.domain.core.timeseriesdata.SparkTimeseriesData;
import cern.nxcals.api.backport.domain.core.timeseriesdata.SparkTimeseriesDataSet;
import cern.nxcals.api.backport.domain.core.timeseriesdata.TimeseriesData;
import cern.nxcals.api.backport.domain.core.timeseriesdata.TimeseriesDataSet;
import cern.nxcals.api.backport.domain.core.timeseriesdata.VariableStatistics;
import cern.nxcals.api.backport.domain.core.timeseriesdata.VariableStatisticsSet;
import cern.nxcals.api.backport.domain.util.TimestampFactory;
import com.google.common.collect.ImmutableList;

import java.util.Iterator;

public class MetadataExamples {
    static {
        // NXCALS PRO
        System.setProperty("service.url", "https://cs-ccr-nxcals5.cern.ch:19093,https://cs-ccr-nxcals6.cern.ch:19093,https://cs-ccr-nxcals7.cern.ch:19093,https://cs-ccr-nxcals8.cern.ch:19093,https://cs-ccr-nxcals5.cern.ch:19094,https://cs-ccr-nxcals6.cern.ch:19094,https://cs-ccr-nxcals7.cern.ch:19094,https://cs-ccr-nxcals8.cern.ch:19094");
    }

    // @snippet:2:on
    public static void main(String[] args) {

        getDataAlignedToTimestamps();
        getDataInTimeWindow();
        getDataInTimeWindowAndLastDataPriorToTimeWindowWithinDefaultInterval();
        getVariableStatisticsOverMultipleVariablesInTimeWindow();
    }
    // @snippet:2:off

    private static Variable getVariable(String variableName) {
        return ServiceBuilder.getInstance().createMetaService()
                .getVariablesWithNameInListofStrings(ImmutableList.of(variableName)).iterator().next();
    }

    private static void printData(TimeseriesDataSet data) {
        data.forEach(MetadataExamples::printDatapoint);
    }

    private static void printDatapoint(TimeseriesData dataPoint) {
        try {
            if (dataPoint == null) {
                System.out.println("Absent data");
            } else {
                System.out.println(dataPoint.getStamp() + ": " + dataPoint.getDoubleValue());
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    private static void printStatistics(VariableStatisticsSet statisticsSet) {
        Iterator<VariableStatistics> iterator = statisticsSet.iterator();
        while (iterator.hasNext()) {
            final VariableStatistics next = iterator.next();
            System.out.println("Values for variable : " + next.getVariableName() + " max: " + next.getMaxValue() + " min: " + next.getMinValue());
        }
    }

    public static void getDataAlignedToTimestamps() {
        TimeseriesDataService timeseriesDataService = ServiceBuilder.getInstance().createTimeseriesService();
        Variable variable = getVariable("PR.DCAFTINJ_1:INTENSITY");

        TimeseriesDataSet drivingSet = SparkTimeseriesDataSet.of(variable,
                ImmutableList.of(
                        SparkTimeseriesData.of(TimestampFactory.parseUTCTimestamp("2018-06-19 00:00:00.000"), null),
                        SparkTimeseriesData.of(TimestampFactory.parseUTCTimestamp("2018-06-19 00:01:00.000"), null),
                        SparkTimeseriesData.of(TimestampFactory.parseUTCTimestamp("2018-06-19 00:02:00.000"), null))
        );

        TimeseriesDataSet data = timeseriesDataService.getDataAlignedToTimestamps(variable, drivingSet);

        System.out.println("Values for variable : " + variable.getVariableName() + " size: " + data.size());
        printData(data);
    }

    public static void getDataInTimeWindow() {
        TimeseriesDataService timeseriesDataService = ServiceBuilder.getInstance().createTimeseriesService();
        Variable variable = getVariable("PR.DCAFTINJ_1:INTENSITY");

        TimeseriesDataSet data = timeseriesDataService.getDataInTimeWindow(variable,
                TimestampFactory.parseUTCTimestamp("2018-06-19 00:00:00.000"),
                TimestampFactory.parseUTCTimestamp("2018-06-19 00:05:00.000"));

        System.out.println("Values for variable : " + variable.getVariableName() + " size: " + data.size());
        printData(data);
    }

    public static void getDataInTimeWindowAndLastDataPriorToTimeWindowWithinDefaultInterval() {
        TimeseriesDataService timeseriesDataService = ServiceBuilder.getInstance().createTimeseriesService();
        Variable variable = getVariable("PR.DCAFTINJ_1:INTENSITY");

        TimeseriesDataSet data = timeseriesDataService.getDataInTimeWindowOrLastDataPriorToWindowWithinDefaultInterval(variable,
                TimestampFactory.parseUTCTimestamp("2018-06-19 00:00:00.000"),
                TimestampFactory.parseUTCTimestamp("2018-06-19 00:05:00.000")
        );

        System.out.println("Values for variable : " + variable.getVariableName() + " size: " + data.size());
        printData(data);
    }

    public static void getVariableStatisticsOverMultipleVariablesInTimeWindow() {
        TimeseriesDataService timeseriesDataService = ServiceBuilder.getInstance().createTimeseriesService();
        Variable variable = getVariable("PR.DCAFTINJ_1:INTENSITY");
        VariableSet variableSet = new VariableSet(variable);


        VariableStatisticsSet data = timeseriesDataService.getVariableStatisticsOverMultipleVariablesInTimeWindow(variableSet,
                TimestampFactory.parseUTCTimestamp("2018-06-19 00:00:00.000"),
                TimestampFactory.parseUTCTimestamp("2018-06-19 00:05:00.000")
        );

        printStatistics(data);
    }
}
// @snippet:1:off
