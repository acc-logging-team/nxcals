package cern.nxcals.docs.examples;

import cern.nxcals.api.extraction.data.builders.DataQuery;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

public class GenericQueries {
    private final SparkSession spark;

    public GenericQueries(SparkSession spark) {
        this.spark = spark;
    }

    public void queries() {
        // @snippet:1:on
        Dataset<Row> cmwData = DataQuery.builder(spark).byEntities().system("CMW")
                .startTime("2018-06-15 00:00:00.000").endTime("2018-06-17 00:00:00.000")
                .entity()
                .keyValue("device", "CPS.TGM")
                .keyValue("property", "FULL-TELEGRAM.STRC").build();

        Dataset<Row> cmwDataPoint = DataQuery.builder(spark).byEntities()
                .system("CMW").atTime("2018-06-16 11:34:42.000")
                .entity()
                .keyValue("device", "CPS.TGM")
                .keyValue("property", "FULL-TELEGRAM.STRC").build();
        // @snippet:1:off

        // @snippet:2:on
        Dataset<Row> winccoaData = DataQuery.builder(spark).byEntities().system("WINCCOA")
                .startTime("2018-06-15 00:00:00.000").endTime("2018-06-17 00:00:00.000")
                .entity().keyValue("variable_name", "MB.C16L2:U_HDS_3").build();
        // @snippet:2:off

        // @snippet:3:on
        Dataset<Row> data = DataQuery.builder(spark).byVariables().system("CMW")
                .startTime("2018-06-15 23:00:00.000").endTime("2018-06-16 00:00:00.000")
                .variable("CPS.TGM:CYCLE").build();

        Dataset<Row> dataPoint = DataQuery.builder(spark).byVariables().system("CMW")
                .atTime("2018-06-15 21:01:01.400").variable("CPS.TGM:CYCLE").build();
        // @snippet:3:off

        // @snippet:4:on
        data.printSchema();
        // @snippet:4:off

        // @snippet:5:on
        data.show(10);
        // @snippet:5:off

        // @snippet:6:on
        Dataset<Row> data2 = DataQuery.builder(spark).byVariables()
                .system("CMW")
                .startTime("2018-07-20 13:38:00.000").endTime("2018-07-20 13:39:00.000")
                .variable("LHC.BOFSU:TUNE_B1_H")
                .variable("LHC.BOFSU:TUNE_B1_V")
                .build();
        // @snippet:6:off

        // @snippet:7:on
        Dataset<Row> data3 = DataQuery.builder(spark).byVariables()
                .system("CMW")
                .startTime("2018-07-20 13:38:00.000").endTime("2018-07-20 13:39:00.000")
                .variableLike("LHC.BOFSU:TUNE_B1_%")
                .variable("LHC.BOFSU:OFC_DEFLECT_H")
                .build();
        // @snippet:7:off

        // @snippet:8:on
        Dataset<Row> df = DataQuery.builder(spark).byEntities().system("CMW")
                .startTime("2018-04-29 00:00:00.000").endTime("2018-04-30 00:00:00.000")
                .entity().keyValue("device", "LHC.LUMISERVER")
                .keyValueLike("property", "CrossingAngleIP%")
                .build();
        // @snippet:8:off
    }
}
