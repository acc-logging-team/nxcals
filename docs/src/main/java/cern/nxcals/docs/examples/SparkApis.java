package cern.nxcals.docs.examples;

import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.extraction.data.builders.DevicePropertyDataQuery;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

// @snippet:4:on
// @snippet:4:off
// @snippet:7:on
// @snippet:7:off

public class SparkApis {
    private SparkSession spark;

    public SparkApis(SparkSession spark) {
        this.spark = spark;
    }

    public void queries() {
        // @snippet:1:on
        Dataset<Row> df = DevicePropertyDataQuery.builder(spark).system("CMW")
                .startTime("2018-05-21 00:00:00.000").endTime("2018-05-23 13:30:00.000")
                .entity().device("ZT10.QFO03").property("Acquisition").build();

        df.select("acqStamp", "current").where("selector = 'CPS.USER.TOF'").show(10);
        // @snippet:1:off

        // @snippet:2:on
        df.createOrReplaceTempView("temp_table");
        Dataset<Row> df2 = spark.sql("SELECT selector, avg(t.current) avg_curr, min(t.current) min_curr, max(t.current) max_curr "
                + "FROM temp_table t "
                + "GROUP BY selector");
        df2.show();
        // @snippet:2:off

        // @snippet:3:on
        Dataset<Row> df_bpm = DataQuery.builder(spark).byVariables().system("CMW")
                .startTime("2017-05-22 00:00:00.000").endTime("2017-05-23 13:30:00.000")
                .variable("BPM_LHC_TEST")
                .build();

        df_bpm.createOrReplaceTempView("bpm_temp_table");
        df2 = spark.sql( "SELECT t1.boolField result, count(t1.boolField) cnt "
                + "FROM temp_table t1 "
                + "JOIN bpm_temp_table t2 ON t2.cycleStamp = t1.cycleStamp "
                + "WHERE t2.floatField < 0.1 "
                + "GROUP BY t1.boolField");
        df2.show();
        // @snippet:3:off

        // @snippet:6:on
        DateTimeFormatter FORMATTER =  DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.n");

        Instant startTime = LocalDateTime.parse("2018-05-23 00:05:53.500000000", FORMATTER).toInstant(ZoneOffset.UTC);
        Instant endTime = LocalDateTime.parse("2018-05-23 01:37:39.100000000", FORMATTER).toInstant(ZoneOffset.UTC);

        df2 = df.filter(df.col("acqStamp")
                .between(getNanosFromInstant(startTime), getNanosFromInstant(endTime)))
                .groupBy("selector").avg("current");
        df2.show();
        // @snippet:6:off

        // @snippet:8:on
        df2 = df.filter(df.col("acqStamp").between(1527033953500000000l, 1527039459100000000l))
                .groupBy("selector").avg("current").select(functions.col("selector"), functions.col("avg(current)").alias("avg_current"));
        df2.show();
        // @snippet:8:off

        // @snippet:9:on
        df2.where("selector like 'CPS.USER.S%'").show();
        // @snippet:9:off

        // @snippet:10:on
        df2.orderBy(functions.col("selector").desc()).show();
        // @snippet:10:off

        // @snippet:11:on
        df.select("selector").distinct().show();
        // @snippet:11:off

        // @snippet:12:on
        df.limit(50).stat().crosstab("current","selector").show();
        // @snippet:12:off

        // @snippet:13:on
        df2.select("selector").dropDuplicates().show();
        // @snippet:13:off

        // @snippet:14:on
        df.select("selector","current_max").na().drop().count();
        // @snippet:14:off

        // @snippet:15:on
        df2.select("selector","avg_current").withColumn("avg_exp", functions.exp("avg_current")).show();
        // @snippet:15:off

        // @snippet:16:on
        Dataset<Row> df3 = df2.drop("selector");
        // @snippet:16:off
    }


    // @snippet:5:on
    private static long getNanosFromInstant(Instant instantTime) {
        return TimeUnit.SECONDS.toNanos(instantTime.getEpochSecond()) + instantTime.getNano();
    }
    // @snippet:5:off
}
