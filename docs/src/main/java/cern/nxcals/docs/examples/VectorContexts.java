package cern.nxcals.docs.examples;

// @snippet:1:on
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.extraction.metadata.InternalServiceClientFactory;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import cern.nxcals.internal.extraction.metadata.InternalSystemSpecService;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.count;
import static org.apache.spark.sql.functions.max;
import static org.apache.spark.sql.functions.min;
// @snippet:1:off

public class VectorContexts {
    private SparkSession spark;

    public VectorContexts(SparkSession spark) {
        this.spark = spark;
    }

    public void queries() {
        // @snippet:2:on
        Map<String, Object> keyValuesLike = new HashMap<String, Object>();
        keyValuesLike.put("device", "nxcals_context");
        keyValuesLike.put("property", "%");

        Dataset<Row> df = DataQuery.builder(spark).byEntities().system("CMW")
                .startTime("2009-01-01 00:00:00.000").endTime("2020-12-31 23:59:59.999")
                .entity().keyValuesLike(keyValuesLike)
                .build().sort(col("property"))
                .select(col("nxcals_entity_id"), col("property"), col("__record_timestamp__").alias("tstamp"));

        Dataset<Row> c_df=df.groupBy("property", "nxcals_entity_id")
        .agg(min("tstamp").alias("min_t"), max("tstamp").alias("max_t"), count("tstamp").alias("nr"));

        c_df.show();
        // @snippet:2:off

        // @snippet:3:on
        Map<String, Object> keyValues = new HashMap<String, Object>();
        keyValues.put("device", "nxcals_context");
        keyValues.put("property", "LHC.BLMI");

        Dataset<Row> e_df = DataQuery.builder(spark).byEntities()
                .system("CMW")
                .startTime("2009-01-01 00:00:00.000").endTime("2020-12-31 23:59:59.999")
                .entity()
                .keyValues(keyValues)
                .build().select("__record_timestamp__", "context_value");

         e_df.show();
        // @snippet:3:off

        // @snippet:4:on
        final String CMW_SYSTEM_NAME = "CMW";
        final List<String> variableNames =  Arrays.asList("LHC.BLMI:CABLE_CONNECTED", "LHC.BLMI:LOSS_RS01");
        InternalSystemSpecService systemService = InternalServiceClientFactory.createSystemSpecService();
        SystemSpec systemData = systemService.findByName(CMW_SYSTEM_NAME)
                .orElseThrow(() -> new IllegalArgumentException("No such system name " + CMW_SYSTEM_NAME));

        VariableService variableService = InternalServiceClientFactory.createVariableService();

        Set<Variable> foundVariables = variableService
                .findAll(Variables.suchThat().systemName().eq(systemData.getName()).and().variableName().in(variableNames));

        foundVariables.forEach(v -> System.out.println(v.getVariableName() + " : " + v.getId()));
        // @snippet:4:off

        // @snippet:5:on
        Dataset<Row> v_df = DataQuery.builder(spark).byVariables()
                .system("CMW")
                .startTime("2015-04-29 00:00:00.000").endTime("2018-04-30 00:00:00.000")
                .variable("NXCALS_CONTEXT:85147928")
                .build().withColumn("elements", col("nxcals_value").getField("elements")).select("nxcals_timestamp", "elements");

        int ts_idx = v_df.schema().fieldIndex("nxcals_timestamp");
        int el_idx = v_df.schema().fieldIndex("elements");

        v_df.collectAsList().forEach(r -> {
            System.out.println(r.get(ts_idx) + " " + r.getList(el_idx).get(1023));
        });
        // @snippet:5:off
    }
}

