package cern.nxcals.docs.examples;

// @snippet:1:on
import org.apache.spark.SparkConf;
import org.apache.spark.sql.SparkSession;

public class NxcalsDemoNoSpring {

    static {
        // for the exact settings please refer to example project
        System.setProperty("kerberos.keytab", "keytab_file_location");
        System.setProperty("kerberos.principal", "principal");
        System.setProperty("service.url", "NXCALS_service_url");
    }

    private static final String HDP_CLUSTER_SPARK_JAVA_HOME = "/var/nxcals/jdk1.11";

    public static void main(String[] args) {

        SparkConf sparkConf = createSparkConf();
        SparkSession spark = SparkSession.builder().config(sparkConf).getOrCreate();

        // code snippets follow
    }

    // Creates Spark configuration
    private static SparkConf createSparkConf() {
        return new SparkConf().setAppName("Example-Application")
                .setMaster("local[*]")
                .set("spark.submit.deployMode", "client")
                .set("spark.ui.port", "4050")

                .set("spark.yarn.appMasterEnv.JAVA_HOME",  HDP_CLUSTER_SPARK_JAVA_HOME)
                .set("yarn.app.mapreduce.am.env.JAVA_HOME", HDP_CLUSTER_SPARK_JAVA_HOME)
                .set("mapreduce.map.env.JAVA_HOME", HDP_CLUSTER_SPARK_JAVA_HOME)
                .set("mapreduce.reduce.env.JAVA_HOME", HDP_CLUSTER_SPARK_JAVA_HOME)

                .set("spark.yarn.jars", "hdfs:///project/nxcals/lib/spark-{{sparkVersion}}/*.jar")
                .set("spark.yarn.am.extraClassPath", "/usr/hdp/hadoop/lib/native")

                .set("spark.executor.extraClassPath", "/usr/hdp/hadoop/lib/native")
                .set("spark.executor.instances", "4")
                .set("spark.executor.cores", "1")
                .set("spark.executor.memory", "1g")
                .set("spark.executorEnv.JAVA_HOME", HDP_CLUSTER_SPARK_JAVA_HOME)

                .set("sql.caseSensitive", "true")
                .set("spark.kerberos.access.hadoopFileSystems", "nxcals");
    }
}
// @snippet:1:off