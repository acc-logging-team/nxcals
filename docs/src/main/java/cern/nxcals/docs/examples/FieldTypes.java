package cern.nxcals.docs.examples;

// @snippet:1:on
import cern.nxcals.api.extraction.data.builders.DataQuery;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.StructField;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static org.apache.spark.sql.functions.col;
// @snippet:1:off

public class FieldTypes {
    private SparkSession spark;

    public FieldTypes(SparkSession spark) {
        this.spark = spark;
    }

    public void queries() {
        //@snippet:2:on
        Map<String, Object> keyValues = new HashMap<String, Object>();
        keyValues.put("device", "LHC.LUMISERVER");
        keyValues.put("property", "CrossingAngleIP1");

        Dataset<Row> df1 = DataQuery.builder(spark).byEntities().system("CMW")
            .startTime("2018-04-29 00:00:00.000").endTime("2018-04-30 00:00:00.000")
            .entity().keyValues(keyValues)
            .build();
        // @snippet:2:off

        // @snippet:3:on
        df1.printSchema(); // prints schema in the tree format
        // @snippet:3:off

        // @snippet:4:on
        df1.dtypes(); // DataFrame property returning names and data types of all the columns
        // @snippet:4:off

        // @snippet:5:on
        df1.schema(); // DataFrame property returning its schema as StructType(List(StructField(name, Spark dataType, nullable), ...))
        // @snippet:5:off

        // @snippet:6:on
        df1.schema().fields(); // Schema property returning List of StructField(name, Spark dataType, nullable)
        // @snippet:6:off

        // @snippet:7:on
        // Getting data types from schema in Spark as a dictionary
        Map<String, DataType> d = Arrays.stream(df1.schema().fields())
                .collect(Collectors.toMap(StructField::name, StructField::dataType));
        d.get("Moving");
        // @snippet:7:off

        // @snippet:8:on
        Dataset<Row> df2 = DataQuery.builder(spark).byVariables().system("CMW")
            .startTime("2018-05-21 00:00:00.000").endTime("2018-05-21 00:05:00.000")
            .variable("SPS.BCTDC.51895:TOTAL_INTENSITY")
            .build();
        // @snippet:8:off

        // @snippet:9:on
        df2.schema().fields();
        // @snippet:9:off

        // @snippet:10:on
        Dataset<Row> elements = df2.withColumn("nx_elements", col("nxcals_value.elements"))
                .withColumn("nx_dimensions", col("nxcals_value.dimensions")).select("nx_elements");
        elements.take(3);
        // @snippet:10:off

        // @snippet:11:on
        Dataset<Row> dimensions = df2.withColumn("nx_elements", col("nxcals_value").getField("elements"))
                .withColumn("nx_dimensions", col("nxcals_value").getField("dimensions")).select("nx_dimensions");
        dimensions.take(3);
        // @snippet:11:off

        // @snippet:12:on
        Dataset<Row> df3 = DataQuery.builder(spark).byVariables().system("CMW")
            .startTime("2018-08-15 00:00:00.000").endTime("2018-08-30 00:00:00.000")
            .variable("HIE-BCAM-T2M03:RAWMEAS#NPIXELS")
            .build();
        // @snippet:12:off

        // @snippet:13:on
        Dataset<Row> matrices = (Dataset<Row>) df3
                .withColumn("matrix", col("nxcals_value.elements"))
                .withColumn("dim1", col("nxcals_value.dimensions").getItem(0))
                .withColumn("dim2", col("nxcals_value.dimensions").getItem(1))
                .select("matrix", "dim1", "dim2");
        matrices.take(2);
        // @snippet:13:off
    }
}
