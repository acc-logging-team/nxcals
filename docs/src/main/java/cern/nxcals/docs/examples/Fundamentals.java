package cern.nxcals.docs.examples;
// @snippet:1:on
import cern.nxcals.api.extraction.data.builders.DataQuery;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.apache.spark.sql.functions.broadcast;

// @snippet:1:off

public class Fundamentals {
    private SparkSession spark;

    public Fundamentals(SparkSession spark) {
        this.spark = spark;
    }

    public void queries() {
        // @snippet:2:on
        List<String> fieldAliasesList = new ArrayList<>();
        fieldAliasesList.add("__LSA_CYCLE__");
        fieldAliasesList.add("lsaCycleName");

        Map<String, List<String>> fieldAliases = new HashMap<>();
        fieldAliases.put("LSA_CYCLE", fieldAliasesList);

        Dataset<Row> funds = DataQuery.builder(spark).byVariables()
            .system("CMW")
            .startTime("2018-09-03 00:00:00.000").endTime("2018-09-03 01:00:00.000")
            .fieldAliases(fieldAliases)
            .variable("SPS:NXCALS_FUNDAMENTAL")
            .buildDataset();

        funds.printSchema();
        funds.show(10);

        Dataset<Row> funds_filtered = funds.select("nxcals_timestamp").where("LSA_CYCLE = 'AWAKE_1INJ_FB60_FT850_Q20_2018_V1' and USER='AWAKE1' ");

        System.out.println("fundamentals = " + funds.count());
        System.out.println("fundamentals filtered = " + funds_filtered.count());

        // @snippet:2:off

        // @snippet:3:on
        Dataset<Row> data = DataQuery.builder(spark).byVariables()
            .system("CMW")
            .startTime("2018-09-01 00:00:00.000").endTime("2018-09-31 23:59:59.999")
            .variable("SPS.BQM:BUNCH_INTENSITIES")
            .buildDataset();

        Dataset<Row> data_filtered = data.join(broadcast(funds_filtered), "nxcals_timestamp");

        System.out.println("data = " + data.count());
        System.out.println("data filtered = " + data_filtered.count());

        data_filtered.sort("nxcals_timestamp").show();
        // @snippet:3:off
    }
}
