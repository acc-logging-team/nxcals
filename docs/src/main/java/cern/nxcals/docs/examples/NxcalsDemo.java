package cern.nxcals.docs.examples;

// @snippet:1:on
import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.extraction.data.builders.DevicePropertyDataQuery;
import cern.rbac.client.authentication.AuthenticationClient;
import cern.rbac.client.authentication.AuthenticationException;
import cern.rbac.common.RbaToken;
import cern.rbac.util.holder.ClientTierTokenHolder;
import org.apache.spark.SparkContext;
import org.apache.spark.ml.feature.Bucketizer;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

// source the nxcals query builders

@SpringBootApplication
@Import(SparkContext.class)
public class NxcalsDemo {

    static {
        // for the exact settings please refer to example project
        System.setProperty("kerberos.keytab", "keytab_file_location");
        System.setProperty("kerberos.principal", "principal");
        System.setProperty("service.url", "NXCALS_service_url");
    }

    public static void main(String[] args) {

        ConfigurableApplicationContext context = SpringApplication.run(NxcalsDemo.class, args);
        SparkSession spark = context.getBean(SparkSession.class);

        // code snippets follow
    }
// @snippet:1:off


    void lastDataPriorToTimestampWithinUserInterval(SparkSession spark) {
        // @snippet:2:on
        DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.n");
        Instant refTime = LocalDateTime.parse("2018-05-23 00:05:54.500000000", FORMATTER).toInstant(ZoneOffset.UTC);
        Instant intervalStart = refTime.minus((Duration.ofDays(1)));

        Dataset<Row> df = DevicePropertyDataQuery.builder(spark).system("CMW")
                .startTime(intervalStart).endTime(refTime)
                .entity().device("ZT10.QFO03").property("Acquisition").build()
                .select("cyclestamp", "current").orderBy(functions.desc("cyclestamp")).limit(1);

        df.show();
        // @snippet:2:off
    }

    void dataInTimeWindowAndLastDataPriorToTimeWindowWithinDefaultInterval(SparkSession spark) {
        // @snippet:3:on
        DateTimeFormatter FORMATTER =  DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.n");
        Instant startTime = LocalDateTime.parse("2018-05-21 00:00:00.000000000", FORMATTER).toInstant(ZoneOffset.UTC);
        Instant intervalStart = startTime.atOffset(ZoneOffset.UTC).minus(1, ChronoUnit.MONTHS).toInstant();

        Dataset<Row> df= DevicePropertyDataQuery.builder(spark).system("CMW")
                .startTime(startTime).endTime("2018-05-23 13:30:00.000")
                .entity().device("ZT10.QFO03").property("Acquisition").build()
                .select("cyclestamp", "current").orderBy(functions.desc("cyclestamp"))
                .unionAll (
                        DevicePropertyDataQuery.builder(spark).system("CMW")
                                .startTime(intervalStart).endTime(startTime)
                                .entity().device("ZT10.QFO03").property("Acquisition").build()
                                .select("cyclestamp", "current").orderBy(functions.desc("cyclestamp")).limit(1));

        df.sort("cyclestamp").show(5);
        // @snippet:3:off
    }

    void DataFilteredByTimestamps(SparkSession spark) {
        // @snippet:4:on
        DateTimeFormatter FORMATTER =  DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.n");

        Dataset<Row> df = DevicePropertyDataQuery.builder(spark).system("CMW")
                .startTime("2018-05-21 00:00:00.000").endTime("2018-05-23 13:30:00.000")
                .entity().device("ZT10.QFO03").property("Acquisition").build();

        df.select("cyclestamp","current").show();
        // @snippet:4:off

        // @snippet:6:on
        StructField[] structFields = new StructField[]{
                new StructField("times", DataTypes.LongType, false, Metadata.empty())
        };

        StructType structType = new StructType(structFields);

        List<Row> rows = new ArrayList<>();
        rows.add(createRowWithTimeinNanos("2018-05-23 00:05:53.500000000", FORMATTER));
        rows.add(createRowWithTimeinNanos("2018-05-23 01:15:53.500000000", FORMATTER));
        rows.add(createRowWithTimeinNanos("2018-05-23 02:21:19.900000000", FORMATTER));

        Dataset<Row> df2 = spark.createDataFrame(rows, structType);
        // @snippet:6:off

        // @snippet:7:on
        df.join(df2, df.col("cyclestamp").equalTo(df2.col("times")))
                .select("cyclestamp", "current").show();
        // @snippet:7:off

        // @snippet:8:on
        System.out.println(Instant.ofEpochMilli(1527038153500l).plusNanos(0));
        // @snippet:8:off

    }

    void dataDistributionInTimewindowForBinsNumber(SparkSession spark) {
        // @snippet:9:on
        Dataset<Row> data = DevicePropertyDataQuery.builder(spark).system("CMW")
                .startTime("2018-05-21 00:00:00.000").endTime("2018-05-23 13:30:00.000")
                .entity().device("ZT10.QFO03").property("Acquisition").build()
                .select("current");
        // @snippet:9:off

        // @snippet:10:on
        Row minmax = ((Row[])(data.select(functions.min("current")
                .alias("minval"), functions.max("current").alias("maxval"))).collect())[0];
        // @snippet:10:off

        // @snippet:11:on
        int nrBuckets = 10;
        float minVal = minmax.getFloat(0);
        float maxVal = minmax.getFloat(1);

        float inf = Float.MAX_VALUE;

        double width = (maxVal - minVal) / nrBuckets;
        System.out.print(width);

        double[] splits = new double[nrBuckets + 3];
        splits[0] = -inf;
        splits[ nrBuckets +2 ] = inf;
        for (int x = 0; x<=nrBuckets; x++) {
            splits[x+1] = minVal + width * x;
        }

        Bucketizer bucketizer = new Bucketizer()
                .setSplits(splits).setInputCol("current").setOutputCol("bucketedFeatures");
        Dataset<Row> bucketedData = bucketizer.transform(data);
        // @snippet:11:off

        // @snippet:12:on
        bucketedData.select("bucketedFeatures").sort("bucketedFeatures")
                .groupBy("bucketedFeatures").count().show();
        // @snippet:12:off
    }

    void vectornumericDataInTimeWindowFilteredByVectorIndices(SparkSession spark) {
        // @snippet:13:on
        Dataset<Row> df = DataQuery.builder(spark).byVariables().system("CMW")
                .startTime("2018-05-21 00:00:00.000").endTime("2018-05-21 00:05:00.000")
                .variable("SPS.BCTDC.51895:TOTAL_INTENSITY")
                .build();
        // @snippet:13:off

        // @snippet:14:on
        List<Row> inplist= df.select("nxcals_timestamp","nxcals_value").collectAsList();
        // @snippet:14:off

        // @snippet:16:on
        df.sort("nxcals_timestamp").select("nxcals_timestamp","nxcals_value").limit(5).show();
        // @snippet:16:off

        // @snippet:17:on
        df.sort("nxcals_timestamp").limit(5).show();
        // @snippet:17:off
    }

    void multiColumnDataInTimeWindow(SparkSession spark) {
        // @snippet:18:on
        Dataset<Row> vdf1 = DataQuery.builder(spark).byVariables().system("CMW")
                .startTime("2018-05-20 00:00:00.000").endTime("2018-05-20 00:12:00.000")
                .variable("SPS.BCTDC.41435:INT_FLATTOP")
                .build()
                .withColumnRenamed("nxcals_value", "value1");
        Dataset<Row> vdf2 = DataQuery.builder(spark).byVariables().system("CMW")
                .startTime("2018-05-20 00:00:00.000").endTime("2018-05-20 00:12:00.000")
                .variable("SPS.BCTDC.51895:SBF_INTENSITY")
                .build()
                .withColumnRenamed("nxcals_value", "value2");
        // @snippet:18:off

        // @snippet:19:on
        Dataset<Row> tsdf = vdf1.select("nxcals_timestamp")
                .union( vdf2.select("nxcals_timestamp")).distinct();
        // @snippet:19:off

        // @snippet:20:on
        vdf1 = vdf1.withColumnRenamed("nxcals_timestamp", "ts1").na().fill("N/A");
        vdf2 = vdf2.withColumnRenamed("nxcals_timestamp", "ts2");
        // @snippet:20:off

        // @snippet:21:on
        Dataset<Row> outdf=tsdf.join(vdf1, tsdf.col("nxcals_timestamp").equalTo(vdf1.col("ts1")), "leftouter")
                .join(vdf2, tsdf.col("nxcals_timestamp").equalTo(vdf2.col("ts2")), "leftouter");

        outdf.select("nxcals_timestamp","value1","value2")
                .sort(functions.desc("nxcals_timestamp")).show(2);
        // @snippet:21:off
    }

    // @snippet:5:on
    private static Row createRowWithTimeinNanos(String localDateString, DateTimeFormatter formatter) {

        Instant instantTime = LocalDateTime.parse(localDateString, formatter).toInstant(ZoneOffset.UTC);
        return RowFactory
                .create(TimeUnit.SECONDS.toNanos(instantTime.getEpochSecond()) + instantTime.getNano());
    }
    // @snippet:5:off

    private static void loginWithRbac() {
        // @snippet:22:on
        String user = System.getProperty("user.name");
        String password = System.getProperty("user.password");

        // Enable NXCALS RBAC authentication in Hadoop delegation tokens generation, in Spark (for extraction)
        System.setProperty("NXCALS_RBAC_AUTH", "true");

        try {
            AuthenticationClient authenticationClient = AuthenticationClient.create();
            RbaToken token = authenticationClient.loginExplicit(user, password);
            ClientTierTokenHolder.setRbaToken(token);
        } catch (AuthenticationException e) {
            throw new IllegalArgumentException("Cannot login", e);
        }
        // @snippet:22:off
    }

}