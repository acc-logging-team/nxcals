package cern.nxcals.docs.examples;

import cern.nxcals.api.backport.client.service.MetaDataService;
import cern.nxcals.api.backport.client.service.ServiceBuilder;
import cern.nxcals.api.backport.domain.core.metadata.Variable;
import cern.nxcals.api.backport.domain.core.metadata.VectornumericElementsSet;
import cern.nxcals.api.backport.domain.util.TimestampFactory;
import com.google.common.collect.ImmutableList;

import java.util.Optional;
import java.util.StringJoiner;


public class BackportContexts {

    public static void main(String[] args) {
        getVectorElements();
        getVectorElementsInTimeWindow();
    }

    // @snippet:1:on
    private static void getVectorElements() {
        final String variableName = "LHC.BLMI:LOSS_RS01";

        StringBuilder outputMsg = new StringBuilder();

        MetaDataService metaDataService = ServiceBuilder.getInstance().createMetaService();
        Variable variable = getVariable(variableName);
        VectornumericElementsSet vectornumericElementsSet = metaDataService.getVectorElements(variable);

        outputMsg.append("Listing number of elements for all the contexts attached to " + variableName + ":\n");
        vectornumericElementsSet.getVectornumericElements().forEach((key, value) -> {
            outputMsg.append(key).append(": ").append(value.size()).append("\n");
        });

        System.out.println(outputMsg.toString());
    }
    // @snippet:1:off

    // @snippet:2:on
    private static void getVectorElementsInTimeWindow() {
        final String variableName = "LHC.BLMI:LOSS_RS02";

        StringBuilder outputMsg = new StringBuilder();

        MetaDataService metaDataService = ServiceBuilder.getInstance().createMetaService();
        Variable variable = getVariable(variableName);
        VectornumericElementsSet vectornumericElementsSet = metaDataService.getVectorElementsInTimeWindow(variable,
                TimestampFactory.parseUTCTimestamp("2016-01-01 00:00:00.000"),
                TimestampFactory.parseUTCTimestamp("2017-01-01 00:00:00.000"));

        outputMsg.append("Listing first 10 elements for contexts attached to " + variableName + " in 2016:\n");
        vectornumericElementsSet.getVectornumericElements().forEach((key, value) -> {
            outputMsg.append(key).append(": ");

            StringJoiner elements = new StringJoiner(",");
            for (int idx = 0; idx < value.size() && idx < 10; idx++) {
                elements.add(value.getElementName(idx));
            }
            outputMsg.append(elements).append("\n");
        });

        System.out.println(outputMsg.toString());
    }
    // @snippet:2:off

    private static Variable getVariable(String variableName) {

        return Optional.ofNullable(ServiceBuilder.getInstance().createMetaService()
                .getVariablesWithNameInListofStrings(ImmutableList.of(variableName))
                .getVariable(variableName))
                .orElseThrow(() -> new IllegalArgumentException("Could not find: " + variableName));
    }
}
