/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.docs;

import cern.nxcals.docs.examples.CmwQueries;
import cern.nxcals.docs.examples.GenericQueries;
import cern.nxcals.docs.examples.SparkApis;
import cern.nxcals.docs.examples.VectorContexts;
import cern.nxcals.docs.metadataapi.EntityServiceSnippets;
import cern.nxcals.docs.metadataapi.EntityServiceSnippets2;
import cern.nxcals.docs.metadataapi.HierarchyServiceSnippets;
import cern.nxcals.docs.metadataapi.SystemServiceSnippets;
import cern.nxcals.docs.metadataapi.VariableServiceSnippets;
import cern.nxcals.docs.metadataapi.VariableServiceSnippets2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(cern.nxcals.api.config.SparkContext.class)
public class CodeSnippets {

    static {
        System.setProperty("logging.config", "classpath:log4j2.yml");
    }

    // TESTBED
    // @snippet:1:on
    static {
        System.setProperty("service.url",
                "https://cs-ccr-testbed2.cern.ch:19093,https://cs-ccr-testbed3.cern.ch:19093,https://cs-ccr-nxcalstbs1.cern.ch:19093");
    }
    // @snippet:1:off

    // NXCALS PRO
    // @snippet:2:on
    static {
        System.setProperty("service.url",
                "https://cs-ccr-nxcals5.cern.ch:19093,https://cs-ccr-nxcals6.cern.ch:19093,https://cs-ccr-nxcals7.cern.ch:19093,https://cs-ccr-nxcals8.cern.ch:19093,https://cs-ccr-nxcals5.cern.ch:19094,https://cs-ccr-nxcals6.cern.ch:19094,https://cs-ccr-nxcals7.cern.ch:19094,https://cs-ccr-nxcals8.cern.ch:19094");
    }
    // @snippet:2:off

    private static final Logger log = LoggerFactory.getLogger(CodeSnippets.class);

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(CodeSnippets.class, args);
        org.apache.spark.sql.SparkSession spark = context.getBean(org.apache.spark.sql.SparkSession.class);

        log.info("DataAccessApi...");
        DataAccessApi dataAccessApi = new DataAccessApi(spark);

        dataAccessApi.dataQueryForKeyValues();
        dataAccessApi.dataQueryForVariables();
        dataAccessApi.dataQueryForDeviceProperty();
        dataAccessApi.dataQueryForKeyValuesUsingStaticFunctions();
        dataAccessApi.dataQueryForVariablesUsingStaticFunctions();
        dataAccessApi.dataQueryForKeyValues_v2();
        dataAccessApi.dataQueryForVariables_v2();
        dataAccessApi.dataQueryForParameter();
        dataAccessApi.dataQueryPivot();
        dataAccessApi.dataQueryPivotByName();

        log.info("ServiceClientApi...");
        SystemServiceSnippets systemServiceSnippets = new SystemServiceSnippets();
        systemServiceSnippets.systemSearch();

        EntityServiceSnippets entityServiceSnippets = new EntityServiceSnippets();
        entityServiceSnippets.entitySearch();
        entityServiceSnippets.entitiesSearchWithHistory();
        entityServiceSnippets.entityUpdate();

        EntityServiceSnippets2 entityServiceSnippets2 = new EntityServiceSnippets2();
        entityServiceSnippets2.entitiesSearchWithOptions();

        VariableServiceSnippets variableServiceSnippets = new VariableServiceSnippets();
        variableServiceSnippets.variableSearchPatternOnName();
        variableServiceSnippets.variableSearchPatternOnDescription();
        variableServiceSnippets.variableRegisterOrUpdate();

        VariableServiceSnippets2 variableServiceSnippets2 = new VariableServiceSnippets2();
        variableServiceSnippets2.getSortedListOfVariables();

        HierarchyServiceSnippets hierarchyServiceSnippets = new HierarchyServiceSnippets();
        hierarchyServiceSnippets.hierarchySearch();
        hierarchyServiceSnippets.hierarchyUpdateVariables();

        GenericQueries genericQueries = new GenericQueries(spark);
        genericQueries.queries();

        CmwQueries cmwQueries = new CmwQueries(spark);
        cmwQueries.queries();

        SparkApis sparkApis = new SparkApis(spark);
        sparkApis.queries();

        VectorContexts vectorContexts = new VectorContexts(spark);
        vectorContexts.queries();

        ServiceBuilderBackportDemo sparkSessionDemo = new ServiceBuilderBackportDemo();
        sparkSessionDemo.runDemos();
    }
}
