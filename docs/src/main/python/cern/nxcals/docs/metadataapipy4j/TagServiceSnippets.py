class TagServiceSnippets:

    def __init__(self):
        pass

    @staticmethod
    def find_one_tag():
        # @snippet:1:on
        _nxcals_api = spark._jvm.cern.nxcals.api

        TagServiceFactory = _nxcals_api.custom.extraction.metadata.TagServiceFactory
        ServiceClientFactory = _nxcals_api.extraction.metadata.ServiceClientFactory

        Tags = _nxcals_api.custom.extraction.metadata.queries.Tags

        tagService = TagServiceFactory.createTagService()
        systemName = "CMW"

        systemSpec = ServiceClientFactory.createSystemSpecService().findByName(systemName)

        if systemSpec.isEmpty():
            raise ValueError("System not found")

        tag = tagService.findOne(getattr(getattr(Tags.suchThat().systemName().eq(systemName), 'and')().name().eq("Demo Tag"), 'and')().variableName().like("DEMO:VARIABLE" + "%"))

        if tag.isEmpty():
            raise ValueError("Tag not found")
        # @snippet:1:off


    @staticmethod
    def create_tag():
        # @snippet:2:on
        _nxcals_api = spark._jvm.cern.nxcals.api

        TagServiceFactory = _nxcals_api.custom.extraction.metadata.TagServiceFactory
        ServiceClientFactory = _nxcals_api.extraction.metadata.ServiceClientFactory

        Visibility = _nxcals_api.domain.Visibility
        DEFAULT_RELATION = _nxcals_api.custom.extraction.metadata.TagService.DEFAULT_RELATION

        Entities = _nxcals_api.extraction.metadata.queries.Entities
        Variables = _nxcals_api.extraction.metadata.queries.Variables

        ImmutableSet = spark._jvm.com.google.common.collect.ImmutableSet

        tagService = TagServiceFactory.createTagService()
        entityService = ServiceClientFactory.createEntityService()
        variableService = ServiceClientFactory.createVariableService()

        tagOwner = "owner"
        systemName = "CMW"
        system = ServiceClientFactory.createSystemSpecService().findByName(systemName)

        if system.isEmpty():
            raise ValueError("System not found")

        system = system.get()

        tagTimestamp = spark._jvm.java.time.Instant.now()
        keyValues = {"device": "LHC.LUMISERVER", "property": "CrossingAngleIP1"}

        entity = entityService.findOne(
            getattr(Entities.suchThat().systemId().eq(system.getId()), 'and')().keyValues().eq(system, keyValues))

        if entity.isEmpty():
            raise ValueError("Entity not found")
        entity = entity.get()

        variable = variableService.findOne(
            getattr(Variables.suchThat().systemId().eq(system.getId()), 'and')().variableName().eq("DEMO:VARIABLE"))

        if variable.isEmpty():
            raise ValueError("Variable not found")
        variable = variable.get()

        entities = ImmutableSet.of(entity)
        variables = ImmutableSet.of(variable)

        tag = tagService.create("My Tag", "Demo Tag", Visibility.PUBLIC,
                                tagOwner, systemName, tagTimestamp, entities, variables)

        # Extract entities and variables from the tag for future data extraction
        entitiesSet = tagService.getEntities(tag.getId()).get(DEFAULT_RELATION)
        variablesSet = tagService.getVariables(tag.getId()).get(DEFAULT_RELATION)
        # @snippet:2:off

    @staticmethod
    def update_tag():
        # @snippet:3:on
        variableService = ServiceClientFactory.createVariableService()
        tagService = TagServiceFactory.createTagService()
        systemService = ServiceClientFactory.createSystemSpecService()

        Tags = _nxcals_api.custom.extraction.metadata.queries.Tags

        HashMap = spark._jvm.java.util.HashMap

        system = systemService.findByName("CMW")
        if system.isEmpty():
            raise ValueError("System not found")
        system = system.get()

        tag = tagService.findOne(
            getattr(Tags.suchThat().systemId().eq(system.getId()), 'and')().name().eq("My Tag7"))

        if tag.isEmpty():
            raise ValueError("Tag not found")
        tag = tag.get()

        # Change tag description
        newTag = tag.toBuilder().description("New description").build()

        variable = variableService.findOne(
            getattr(Variables.suchThat().systemId().eq(system.getId()), 'and')().variableName().eq("DEMO:VARIABLE"))

        if variable.isEmpty():
            raise ValueError("Variable not found")
        variable = variable.get()

        # Perform the update
        updatedTag = tagService.update(newTag)

        tagService.addVariables(newTag.getId(), {DEFAULT_RELATION: {variable.getId()}})
        # @snippet:3:off

