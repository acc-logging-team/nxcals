class EntityServiceSnippets:

    def __init__(self):
        pass

    @staticmethod
    def find_one_entity():
        # @snippet:1:on
        _nxcals_api = spark._jvm.cern.nxcals.api
        entityService = _nxcals_api.extraction.metadata.ServiceClientFactory.createEntityService()
        systemService = _nxcals_api.extraction.metadata.ServiceClientFactory.createSystemSpecService()
        SystemSpecs = _nxcals_api.extraction.metadata.queries.SystemSpecs
        Entities = _nxcals_api.extraction.metadata.queries.Entities

        systemData = systemService.findOne(SystemSpecs.suchThat().name().eq("CMW"))


        if systemData.isEmpty():
            raise ValueError("System not found")

        keyValues = {"device": "LHC.LUMISERVER", "property": "CrossingAngleIP1"}

        entityData = entityService.findOne(getattr(Entities.suchThat().systemId().eq(systemData.get().getId()),'and')().keyValues().eq(systemData.get(), keyValues))

        if entityData.isEmpty():
            raise ValueError("Entity not found")
        # @snippet:1:off


    @staticmethod
    def find_entity_with_history():
        # @snippet:2:on
        _nxcals_api = spark._jvm.cern.nxcals.api
        ServiceClientFactory = _nxcals_api.extraction.metadata.ServiceClientFactory
        SystemSpecs = _nxcals_api.extraction.metadata.queries.SystemSpecs
        Entities = _nxcals_api.extraction.metadata.queries.Entities
        TimeUtils = _nxcals_api.utils.TimeUtils

        systemData = ServiceClientFactory.createSystemSpecService().findOne(SystemSpecs.suchThat().name().eq("CMW"))

        if systemData.isEmpty():
            raise ValueError("System not found")

        keyValues = {"device": "LHC.LUMISERVER", "property": "CrossingAngleIP1"}

        entityService = ServiceClientFactory.createEntityService()

        entities = entityService.findOneWithHistory(getattr(
            Entities.suchThat().systemName().eq(systemData.get().getName()), 'and')().keyValues().eq(systemData.get(), keyValues),
                                                    TimeUtils.getNanosFromString("2017-10-10 14:15:00.000000000"),
                                                    TimeUtils.getNanosFromString("2020-10-26 14:15:00.000000000"))

        if entities.isEmpty():
            raise ValueError("Entity not found")

        for entityHistory in entities.get().getEntityHistory():
            print(entityHistory)
        # @snippet:2:off

    @staticmethod
    def update_entitiy():
        # @snippet:3:on
        _nxcals_api = spark._jvm.cern.nxcals.api
        ServiceClientFactory = _nxcals_api.extraction.metadata.ServiceClientFactory
        Entities = _nxcals_api.extraction.metadata.queries.Entities

        entityService = ServiceClientFactory.createEntityService()
        systemService = ServiceClientFactory.createSystemSpecService()

        systemData = systemService.findByName("CMW")

        if systemData.isEmpty():
            raise ValueError("No such system CMW")

        keyValues = {"device": "MY.DEVICE", "property": "MyProperty"}

        entityData = entityService.findOne(getattr(Entities.suchThat().systemId().eq(systemData.get().getId()), 'and')().keyValues().eq(systemData.get(), keyValues))

        if entityData.isEmpty():
            raise ValueError("Entity not found")

        # Change entity to set new keyValues
        newKeyValues = {"device": "Example.Device", "property": "ExampleNewProperty"}
        newEntityData = entityData.get().toBuilder().entityKeyValues(newKeyValues).build()

        # Perform the update
        updatedEntities = entityService.updateEntities({newEntityData})
        # @snippet:3:off

    @staticmethod
    def entities_search_with_options():
        # @snippet:11:on
        _nxcals_api = spark._jvm.cern.nxcals.api
        ServiceClientFactory = _nxcals_api.extraction.metadata.ServiceClientFactory
        Entities = _nxcals_api.metadata.queries.Entities  # New package!

        entity_service = ServiceClientFactory.createEntityService()
        system_service = ServiceClientFactory.createSystemSpecService()

        system_data = system_service.findByName("CMW")

        if system_data.isEmpty():
            raise ValueError("No such system CMW")

        key_values = {"device": "LHC.LUMISERVER", "property": "CrossingAngleIP%"}
        query = Entities.suchThat() \
            .keyValues().like(system_data.get(), key_values) \
            .withOptions() \
            .withHistory("2017-10-10 14:15:00.000000000", "2020-10-26 14:15:00.000000000") \
            .limit(2) \
            .orderBy().keyValues().desc()

        entities = entity_service.findAll(query)
        # @snippet:11:off
