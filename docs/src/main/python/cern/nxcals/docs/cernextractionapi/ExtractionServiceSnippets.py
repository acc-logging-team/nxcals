class ExtractionServiceSnippets:

    def __init__(self):
        pass

    @staticmethod
    def getService():
        # @snippet:1:on
        from cern.nxcals.api.custom.service import Services
        from cern.nxcals.api.config import SparkProperties

        extractionService = Services.newInstance(SparkProperties.defaults("MY_APP")).extractionService()
        # @snippet:1:off
        return extractionService

    @staticmethod
    def getServicePy4j():
        # @snippet:11:on
        from nxcals.spark_session_builder import get_or_create # on swan create spark session from UI (star icon)
        spark = get_or_create()

        Services = spark._jvm.cern.nxcals.api.custom.service.Services

        extractionService = Services.newInstance(spark._jsparkSession).extractionService()
        # @snippet:11:off
        return spark, extractionService

    @staticmethod
    def getVariableDataWhenDataAvailableNoLookup():
        from cern.nxcals.api.extraction.metadata import ServiceClientFactory

        variableService = ServiceClientFactory.createVariableService()
        extractionService = ExtractionServiceSnippets.getService()

        # @snippet:2:on
        from cern.nxcals.api.extraction.metadata.queries import Variables
        from cern.nxcals.api.utils import TimeUtils
        from cern.nxcals.api.custom.service.extraction import ExtractionProperties, LookupStrategy

        myVariable = variableService.findOne(Variables.suchThat().variableName().eq("CPS.TGM:CYCLE"))

        if myVariable.isEmpty():
            raise ValueError("Could not obtain variable from service")

        startTime = TimeUtils.getInstantFromString("2020-04-25 00:00:00.000000000")
        endTime = TimeUtils.getInstantFromString("2020-04-26 00:00:00.000000000")

        properties = ExtractionProperties.builder().timeWindow(startTime, endTime).lookupStrategy(LookupStrategy.LAST_BEFORE_START_IF_EMPTY).build()

        dataset = extractionService.getData(myVariable.get(), properties)

        print(dataset.count())
        dataset.show()
        # @snippet:2:off

    @staticmethod
    def getVariableDataWhenDataAvailableNoLookupPy4j():
        spark, extractionService = ExtractionServiceSnippets.getServicePy4j()
        # @snippet:12:on
        ServiceClientFactory = spark._jvm.cern.nxcals.api.extraction.metadata.ServiceClientFactory
        Variables = spark._jvm.cern.nxcals.api.extraction.metadata.queries.Variables
        TimeUtils = spark._jvm.cern.nxcals.api.utils.TimeUtils
        ExtractionProperties = spark._jvm.cern.nxcals.api.custom.service.extraction.ExtractionProperties
        LookupStrategy = spark._jvm.cern.nxcals.api.custom.service.extraction.LookupStrategy

        variableService = ServiceClientFactory.createVariableService()

        myVariable = variableService.findOne(Variables.suchThat().variableName().eq("CPS.TGM:CYCLE"))

        if myVariable.isEmpty():
            raise ValueError("Could not obtain variable from service")

        startTime = TimeUtils.getInstantFromString("2020-04-25 00:00:00.000000000")
        endTime = TimeUtils.getInstantFromString("2020-04-26 00:00:00.000000000")

        properties = ExtractionProperties.builder().timeWindow(startTime, endTime).lookupStrategy(LookupStrategy.LAST_BEFORE_START_IF_EMPTY).build()

        dataset = extractionService.getData(myVariable.get(), properties)

        print(dataset.count())
        dataset.show()
        # @snippet:12:off

    @staticmethod
    def getVariableDataAlwaysDoLookup():
        from cern.nxcals.api.extraction.metadata import ServiceClientFactory

        variableService = ServiceClientFactory.createVariableService()
        extractionService = ExtractionServiceSnippets.getService()

        # @snippet:3:on
        from cern.nxcals.api.extraction.metadata.queries import Variables
        from cern.nxcals.api.utils import TimeUtils
        from cern.nxcals.api.custom.service.extraction import ExtractionProperties, LookupStrategy

        myVariable = variableService.findOne(Variables.suchThat().variableName().eq("CPS.TGM:CYCLE"))

        if myVariable.isEmpty():
            raise ValueError("Could not obtain variable from service")

        startTime = TimeUtils.getInstantFromString("2020-04-25 00:00:00.000000000")
        endTime = TimeUtils.getInstantFromString("2020-04-26 00:00:00.000000000")

        properties = ExtractionProperties.builder().timeWindow(startTime, endTime).lookupStrategy(LookupStrategy.LAST_BEFORE_START).build()
        dataset = extractionService.getData(myVariable.get(), properties)

        print(dataset.count())
        dataset.show()
        # @snippet:3:off

    @staticmethod
    def getVariableDataAlwaysDoLookupPy4j():
        spark, extractionService = ExtractionServiceSnippets.getServicePy4j()
        # @snippet:13:on
        ServiceClientFactory = spark._jvm.cern.nxcals.api.extraction.metadata.ServiceClientFactory
        Variables = spark._jvm.cern.nxcals.api.extraction.metadata.queries.Variables
        TimeUtils = spark._jvm.cern.nxcals.api.utils.TimeUtils
        ExtractionProperties = spark._jvm.cern.nxcals.api.custom.service.extraction.ExtractionProperties
        LookupStrategy = spark._jvm.cern.nxcals.api.custom.service.extraction.LookupStrategy

        variableService = ServiceClientFactory.createVariableService()

        myVariable = variableService.findOne(Variables.suchThat().variableName().eq("CPS.TGM:CYCLE"))

        if myVariable.isEmpty():
            raise ValueError("Could not obtain variable from service")

        startTime = TimeUtils.getInstantFromString("2020-04-25 00:00:00.000000000")
        endTime = TimeUtils.getInstantFromString("2020-04-26 00:00:00.000000000")

        properties = ExtractionProperties.builder().timeWindow(startTime, endTime).lookupStrategy(LookupStrategy.LAST_BEFORE_START).build()
        dataset = extractionService.getData(myVariable.get(), properties)

        print(dataset.count())
        dataset.show()
        # @snippet:13:off


    @staticmethod
    def getEntityDataWhenDataAvailableNoLookup():
        from cern.nxcals.api.extraction.metadata import ServiceClientFactory

        entityService = ServiceClientFactory.createEntityService()
        systemService = ServiceClientFactory.createSystemSpecService()
        extractionService = ExtractionServiceSnippets.getService()

        cmwSystemSpec = systemService.findByName("CMW")

        if cmwSystemSpec.isEmpty():
            raise ValueError("No such system")
        # @snippet:4:on
        from cern.nxcals.api.extraction.metadata.queries import Entities
        from cern.nxcals.api.utils import TimeUtils
        from cern.nxcals.api.custom.service.extraction import ExtractionProperties, LookupStrategy
        from cern.nxcals.api.custom.domain.CmwSystemConstants import DEVICE_KEY_NAME, PROPERTY_KEY_NAME
        from com.google.common.collect import ImmutableMap

        keyValues = ImmutableMap.of(DEVICE_KEY_NAME, "CPS.TGM", PROPERTY_KEY_NAME, "FULL-TELEGRAM.STRC")

        myEntity = entityService.findOne(Entities.suchThat().keyValues().eq(cmwSystemSpec.get(), keyValues))

        if myEntity.isEmpty():
            raise ValueError("Could not obtain entity from service")

        startTime = TimeUtils.getInstantFromString("2020-04-10 00:00:00.000000000")
        endTime = TimeUtils.getInstantFromString("2020-04-11 00:00:10.000000000")

        properties = ExtractionProperties.builder().timeWindow(startTime, endTime).lookupStrategy(LookupStrategy.LAST_BEFORE_START_IF_EMPTY).build()
        dataset = extractionService.getData(myEntity.get(), properties)

        print(dataset.count())

        from jpype.types import JInt
        dataset.show(JInt(2))
        # @snippet:4:off

    @staticmethod
    def getEntityDataWhenDataAvailableNoLookupPy4j():
        spark, extractionService = ExtractionServiceSnippets.getServicePy4j()
        # @snippet:14:on
        ServiceClientFactory = spark._jvm.cern.nxcals.api.extraction.metadata.ServiceClientFactory
        Entities = spark._jvm.cern.nxcals.api.extraction.metadata.queries.Entities
        TimeUtils = spark._jvm.cern.nxcals.api.utils.TimeUtils
        ExtractionProperties = spark._jvm.cern.nxcals.api.custom.service.extraction.ExtractionProperties
        LookupStrategy = spark._jvm.cern.nxcals.api.custom.service.extraction.LookupStrategy

        DEVICE_KEY_NAME = spark._jvm.cern.nxcals.api.custom.domain.CmwSystemConstants.DEVICE_KEY_NAME
        PROPERTY_KEY_NAME = spark._jvm.cern.nxcals.api.custom.domain.CmwSystemConstants.PROPERTY_KEY_NAME
        ImmutableMap = spark._jvm.com.google.common.collect.ImmutableMap

        entityService = ServiceClientFactory.createEntityService()
        systemService = ServiceClientFactory.createSystemSpecService()

        cmwSystemSpec = systemService.findByName("CMW")

        if cmwSystemSpec.isEmpty():
            raise ValueError("No such system")

        keyValues = ImmutableMap.of(DEVICE_KEY_NAME, "CPS.TGM", PROPERTY_KEY_NAME, "FULL-TELEGRAM.STRC")

        myEntity = entityService.findOne(Entities.suchThat().keyValues().eq(cmwSystemSpec.get(), keyValues))

        if myEntity.isEmpty():
            raise ValueError("Could not obtain entity from service")

        startTime = TimeUtils.getInstantFromString("2020-04-10 00:00:00.000000000")
        endTime = TimeUtils.getInstantFromString("2020-04-11 00:00:10.000000000")

        properties = ExtractionProperties.builder().timeWindow(startTime, endTime).lookupStrategy(LookupStrategy.LAST_BEFORE_START_IF_EMPTY).build()
        dataset = extractionService.getData(myEntity.get(), properties)

        print(dataset.count())

        dataset.show(2)
        # @snippet:14:off

    @staticmethod
    def getEntityDataAlwaysDoLookup():
        from cern.nxcals.api.extraction.metadata import ServiceClientFactory

        entityService = ServiceClientFactory.createEntityService()
        systemService = ServiceClientFactory.createSystemSpecService()
        extractionService = ExtractionServiceSnippets.getService()

        cmwSystemSpec = systemService.findByName("CMW")

        if cmwSystemSpec.isEmpty():
            raise ValueError("No such system")

        # @snippet:5:on
        from cern.nxcals.api.extraction.metadata.queries import Entities
        from cern.nxcals.api.utils import TimeUtils
        from cern.nxcals.api.custom.service.extraction import ExtractionProperties, LookupStrategy
        from cern.nxcals.api.custom.domain.CmwSystemConstants import DEVICE_KEY_NAME, PROPERTY_KEY_NAME
        from com.google.common.collect import ImmutableMap

        keyValues = ImmutableMap.of(DEVICE_KEY_NAME, "CPS.TGM", PROPERTY_KEY_NAME, "FULL-TELEGRAM.STRC")
        myEntity = entityService.findOne(Entities.suchThat().keyValues().eq(cmwSystemSpec.get(), keyValues))

        if myEntity.isEmpty():
            raise ValueError("Could not obtain entity from service")

        startTime = TimeUtils.getInstantFromString("2020-04-10 00:00:00.000000000")
        endTime = TimeUtils.getInstantFromString("2020-04-11 00:00:10.000000000")

        properties = ExtractionProperties.builder().timeWindow(startTime, endTime).lookupStrategy(LookupStrategy.LAST_BEFORE_START).build()
        dataset = extractionService.getData(myEntity.get(), properties)

        print(dataset.count())
        from jpype.types import JInt
        dataset.show(JInt(2))
        # @snippet:5:off

    @staticmethod
    def getEntityDataAlwaysDoLookupPy4j():
        spark, extractionService = ExtractionServiceSnippets.getServicePy4j()
        # @snippet:15:on
        ServiceClientFactory = spark._jvm.cern.nxcals.api.extraction.metadata.ServiceClientFactory
        Entities = spark._jvm.cern.nxcals.api.extraction.metadata.queries.Entities
        TimeUtils = spark._jvm.cern.nxcals.api.utils.TimeUtils
        ExtractionProperties = spark._jvm.cern.nxcals.api.custom.service.extraction.ExtractionProperties
        LookupStrategy = spark._jvm.cern.nxcals.api.custom.service.extraction.LookupStrategy

        DEVICE_KEY_NAME = spark._jvm.cern.nxcals.api.custom.domain.CmwSystemConstants.DEVICE_KEY_NAME
        PROPERTY_KEY_NAME = spark._jvm.cern.nxcals.api.custom.domain.CmwSystemConstants.PROPERTY_KEY_NAME
        ImmutableMap = spark._jvm.com.google.common.collect.ImmutableMap

        entityService = ServiceClientFactory.createEntityService()
        systemService = ServiceClientFactory.createSystemSpecService()

        cmwSystemSpec = systemService.findByName("CMW")

        if cmwSystemSpec.isEmpty():
            raise ValueError("No such system")

        keyValues = ImmutableMap.of(DEVICE_KEY_NAME, "CPS.TGM", PROPERTY_KEY_NAME, "FULL-TELEGRAM.STRC")
        myEntity = entityService.findOne(Entities.suchThat().keyValues().eq(cmwSystemSpec.get(), keyValues))

        if myEntity.isEmpty():
            raise ValueError("Could not obtain entity from service")

        startTime = TimeUtils.getInstantFromString("2020-04-10 00:00:00.000000000")
        endTime = TimeUtils.getInstantFromString("2020-04-11 00:00:10.000000000")

        properties = ExtractionProperties.builder().timeWindow(startTime, endTime).lookupStrategy(LookupStrategy.LAST_BEFORE_START).build()
        dataset = extractionService.getData(myEntity.get(), properties)

        print(dataset.count())
        dataset.show(2)
        # @snippet:15:off

    @staticmethod
    def getEntityDataWithCustomLookup():
        from cern.nxcals.api.extraction.metadata import ServiceClientFactory

        entityService = ServiceClientFactory.createEntityService()
        systemService = ServiceClientFactory.createSystemSpecService()
        extractionService = ExtractionServiceSnippets.getService()

        cmwSystemSpec = systemService.findByName("CMW")

        if cmwSystemSpec.isEmpty():
            raise ValueError("No such system")
        # @snippet:6:on
        from cern.nxcals.api.extraction.metadata.queries import Entities
        from cern.nxcals.api.utils import TimeUtils
        from java.time.temporal import ChronoUnit
        from cern.nxcals.api.custom.service.extraction import ExtractionProperties, LookupStrategy
        from cern.nxcals.api.custom.domain.CmwSystemConstants import DEVICE_KEY_NAME, PROPERTY_KEY_NAME
        from com.google.common.collect import ImmutableMap

        keyValues = ImmutableMap.of(DEVICE_KEY_NAME, "CPS.TGM", PROPERTY_KEY_NAME, "FULL-TELEGRAM.STRC")
        myEntity = entityService.findOne(Entities.suchThat().keyValues().eq(cmwSystemSpec.get(), keyValues))

        if myEntity.isEmpty():
            raise ValueError("Could not obtain entity from service")

        startTime = TimeUtils.getInstantFromString("2020-04-10 00:00:00.000000000")
        endTime = TimeUtils.getInstantFromString("2020-04-11 00:00:10.000000000")

        customLookupStrategy = LookupStrategy.LAST_BEFORE_START.withLookupDuration(8, ChronoUnit.HOURS)
        properties = ExtractionProperties.builder().timeWindow(startTime, endTime).lookupStrategy(customLookupStrategy).build()
        dataset = extractionService.getData(myEntity.get(), properties)

        print(dataset.count())
        from jpype.types import JInt
        dataset.show(JInt(2))
        # @snippet:6:off

    @staticmethod
    def getEntityDataWithCustomLookupPy4j():
        spark, extractionService = ExtractionServiceSnippets.getServicePy4j()
        # @snippet:16:on
        ServiceClientFactory = spark._jvm.cern.nxcals.api.extraction.metadata.ServiceClientFactory
        Entities = spark._jvm.cern.nxcals.api.extraction.metadata.queries.Entities
        TimeUtils = spark._jvm.cern.nxcals.api.utils.TimeUtils
        ChronoUnit = spark._jvm.java.time.temporal.ChronoUnit
        ExtractionProperties = spark._jvm.cern.nxcals.api.custom.service.extraction.ExtractionProperties
        LookupStrategy = spark._jvm.cern.nxcals.api.custom.service.extraction.LookupStrategy

        DEVICE_KEY_NAME = spark._jvm.cern.nxcals.api.custom.domain.CmwSystemConstants.DEVICE_KEY_NAME
        PROPERTY_KEY_NAME = spark._jvm.cern.nxcals.api.custom.domain.CmwSystemConstants.PROPERTY_KEY_NAME
        ImmutableMap = spark._jvm.com.google.common.collect.ImmutableMap

        entityService = ServiceClientFactory.createEntityService()
        systemService = ServiceClientFactory.createSystemSpecService()

        cmwSystemSpec = systemService.findByName("CMW")

        if cmwSystemSpec.isEmpty():
            raise ValueError("No such system")

        keyValues = ImmutableMap.of(DEVICE_KEY_NAME, "CPS.TGM", PROPERTY_KEY_NAME, "FULL-TELEGRAM.STRC")
        myEntity = entityService.findOne(Entities.suchThat().keyValues().eq(cmwSystemSpec.get(), keyValues))

        if myEntity.isEmpty():
            raise ValueError("Could not obtain entity from service")

        startTime = TimeUtils.getInstantFromString("2020-04-10 00:00:00.000000000")
        endTime = TimeUtils.getInstantFromString("2020-04-11 00:00:10.000000000")

        customLookupStrategy = LookupStrategy.LAST_BEFORE_START.withLookupDuration(8, ChronoUnit.HOURS)
        properties = ExtractionProperties.builder().timeWindow(startTime, endTime).lookupStrategy(customLookupStrategy).build()
        dataset = extractionService.getData(myEntity.get(), properties)

        print(dataset.count())
        dataset.show(2)
        # @snippet:16:off

    @staticmethod
    def test_py4j():
        ExtractionServiceSnippets.getEntityDataAlwaysDoLookupPy4j()
        ExtractionServiceSnippets.getEntityDataWhenDataAvailableNoLookupPy4j()
        ExtractionServiceSnippets.getEntityDataWithCustomLookupPy4j()
        ExtractionServiceSnippets.getServicePy4j()
        ExtractionServiceSnippets.getVariableDataAlwaysDoLookupPy4j()
        ExtractionServiceSnippets.getVariableDataWhenDataAvailableNoLookupPy4j()