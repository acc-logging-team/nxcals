class Py4jWithStubs:

    def __init__(self):
        pass

    @staticmethod
    def demo1():

        # @snippet:1:on
        # spark_session_builder is available after doing `pip install nxcals`
        from nxcals import spark_session_builder
        # Import the Python counterparts of the NXCALS APIs (automatically generated with code completion):
        from py4jgw.cern.nxcals.api.extraction.metadata import ServiceClientFactory
        from py4jgw.cern.nxcals.api.extraction.metadata.queries import Variables

        # initialize a session with NXCALS
        spark = spark_session_builder.get_or_create(app_name='nxcals-py4j-demo')

        # now you can use the NXCALS APIs imported above
        variableService = ServiceClientFactory.createVariableService()
        var = variableService.findOne(Variables.suchThat().variableName().eq('HX:BMODE')).get()
        print(var.getVariableName())
        # @snippet:1:off

    @staticmethod
    def demo2():

        # @snippet:2:on
        from nxcals import spark_session_builder
        from py4jgw.cern.nxcals.api.extraction.metadata import ServiceClientFactory
        from py4jgw.cern.nxcals.api.extraction.metadata.queries import Variables

        spark = spark_session_builder.get_or_create(app_name='nxcals-stubs-demo')

        variableService = ServiceClientFactory.createVariableService()
        var = variableService.findOne(Variables.suchThat().variableName().eq('HX:BMODE')).get()
        print(var.getVariableName())
        # @snippet:2:off

    @staticmethod
    def demo3():
        # @snippet:3:on
        from nxcals import spark_session_builder
        from py4jgw.cern.nxcals.api.custom.service import Services

        spark = spark_session_builder.get_or_create(app_name='nxcals-stubs-demo')

        fillservice = Services.newInstance().fillService()
        fill = fillservice.findFill(3000)
        print(fill)
        # @snippet:3:off

    @staticmethod
    def demo4():

        # @snippet:4:on
        from nxcals import spark_session_builder
        from py4jgw.cern.nxcals.api.extraction.metadata.queries import Variables

        spark = spark_session_builder.get_or_create(app_name='nxcals-stubs-demo')

        # please note the use of and_() with an underscore, like in jPype
        var_cond = Variables.suchThat().variableName().like("%MTG%").and_().description().exists()
        # @snippet:4:off

    @staticmethod
    def demo5():

        # @snippet:5:on
        from nxcals import spark_session_builder
        from py4jgw.cern.nxcals.api.custom.service import Services
        from py4jgw.cern.nxcals.api.custom.service.extraction import ExtractionProperties, LookupStrategy
        from py4jgw.cern.nxcals.api.extraction.metadata import ServiceClientFactory
        from py4jgw.cern.nxcals.api.extraction.metadata.queries import Variables
        from py4jgw.cern.nxcals.api.utils import TimeUtils

        spark = spark_session_builder.get_or_create(app_name='nxcals-stubs-demo')

        variableService = ServiceClientFactory.createVariableService()
        extractionService = Services.newInstance().extractionService()

        myVariable = variableService.findOne(Variables.suchThat().variableName().eq("CPS.TGM:CYCLE"))

        if myVariable.isEmpty():
            raise ValueError("Could not obtain variable from service")

        startTime = TimeUtils.getInstantFromString("2020-04-25 00:00:00.000000000")
        endTime = TimeUtils.getInstantFromString("2020-04-26 00:00:00.000000000")

        properties = ExtractionProperties.builder().timeWindow(startTime, endTime) \
            .lookupStrategy(LookupStrategy.LAST_BEFORE_START_IF_EMPTY).build()

        dataset = extractionService.getData(myVariable.get(), properties)

        print(dataset.count())
        dataset.show()
        # @snippet:5:off


    @staticmethod
    def demo6():

        # @snippet:6:on
        from py4jgw.cern.nxcals.api.extraction.metadata.queries import Variables

        var_cond = Variables.suchThat().variableName().like("%MTG%").and_().description().exists()
        # @snippet:6:off
