class Py4jInPyspark:

    def __init__(self):
        pass

    @staticmethod
    def demo1():

        # @snippet:1:on
        # Creating spark using spark session builder. On SWAN click in star icon
        from nxcals.spark_session_builder import get_or_create
        spark = get_or_create()
        # Use Py4J created in Spark instance
        Variables = spark._jvm.cern.nxcals.api.extraction.metadata.queries.Variables

        variableService = spark._jvm.cern.nxcals.api.extraction.metadata.ServiceClientFactory.createVariableService()
        var = variableService.findOne(Variables.suchThat().variableName().eq('HX:BMODE')).get()
        print(var.getVariableName())
        # @snippet:1:off

    @staticmethod
    def demo2():
        # @snippet:2:on
        # Using an existing SparkSession provided by PySpark
        fillservice = spark._jvm.cern.nxcals.api.custom.service.Services.newInstance().fillService()
        fill=fillservice.findFill(3000)
        print(fill)
        # @snippet:2:off

    @staticmethod
    def demo3():

        # @snippet:3:on
        cern_nxcals_api = spark._jvm.cern.nxcals.api

        variableService = cern_nxcals_api.extraction.metadata.ServiceClientFactory.createVariableService()
        extractionService = cern_nxcals_api.custom.service.Services.newInstance().extractionService()



        myVariable = variableService.findOne(cern_nxcals_api.extraction.metadata.queries.Variables.suchThat().variableName().eq("CPS.TGM:CYCLE"))

        if myVariable.isEmpty():
            raise ValueError("Could not obtain variable from service")

        startTime = cern_nxcals_api.utils.TimeUtils.getInstantFromString("2020-04-25 00:00:00.000000000")
        endTime = cern_nxcals_api.utils.TimeUtils.getInstantFromString("2020-04-26 00:00:00.000000000")

        properties = cern_nxcals_api.custom.service.extraction.ExtractionProperties.builder().timeWindow(startTime, endTime) \
            .lookupStrategy(cern_nxcals_api.custom.service.extraction.LookupStrategy.LAST_BEFORE_START_IF_EMPTY).build()

        dataset = extractionService.getData(myVariable.get(), properties)

        print(dataset.count())
        dataset.show()
        # @snippet:3:off