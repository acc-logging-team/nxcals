# @snippet:1:on
import pandas as pd
from nxcals.api.extraction.data.builders import DataQuery
from pyspark.sql.functions import min, max, count, col


# @snippet:1:off

class VectorContexts:

    def __init__(self):
        pass

    @staticmethod
    def query(spark):

        # @snippet:2:on
        df = DataQuery.builder(spark).byEntities().system('CMW') \
            .startTime('2009-01-01 00:00:00.000').endTime('2020-12-31 23:59:59.999') \
            .entity().keyValuesLike({'device': 'nxcals_context', 'property': '%'}) \
            .build().sort(col("property")).select('nxcals_entity_id', 'property',
                                                  col('__record_timestamp__').alias("tstamp"))

        pdf=df.groupBy('property', 'nxcals_entity_id')\
        .agg(min('tstamp').alias('min_t'), max('tstamp').alias('max_t'), count('tstamp').alias('nr'))\
        .toPandas()

        pdf['min_t'] = pd.to_datetime(pdf.min_t, unit='ns')
        pdf['max_t'] = pd.to_datetime(pdf.max_t, unit='ns')

        pd.options.display.width = 120
        print(pdf)
        # @snippet:2:off

        # @snippet:3:on
        e_df = DataQuery.builder(spark).byEntities() \
            .system('CMW') \
            .startTime('2009-01-01 00:00:00.000').endTime('2020-12-31 23:59:59.999') \
            .entity() \
            .keyValues({'device': 'nxcals_context', 'property': 'LHC.BLMI'}) \
            .build().select('__record_timestamp__', 'context_value').toPandas()

        e_df['__record_timestamp__'] = pd.to_datetime(e_df.__record_timestamp__, unit='ns')

        pd.options.display.width = 120
        print(e_df)
        # @snippet:3:off

        # @snippet:4:on
        # NXCALS Metadata API is not yet available for Python
        # @snippet:4:off

        # @snippet:5:on
        v_df = DataQuery.builder(spark).byVariables().system("CMW").startTime("2015-04-29 00:00:00.000").endTime("2018-04-30 00:00:00.000") \
            .variable("NXCALS_CONTEXT:85147928").build().withColumn("elements", col("nxcals_value")["elements"]) \
            .select('nxcals_timestamp', "elements")

        for row in v_df.collect():
            print(str(row.nxcals_timestamp) + " " + row.elements[1023])
        # @snippet:5:off