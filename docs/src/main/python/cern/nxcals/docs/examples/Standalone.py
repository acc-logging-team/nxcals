# @snippet:1:on
from nxcals.api.extraction.data.builders import DevicePropertyDataQuery
from pyspark import SparkContext
from pyspark.conf import SparkConf
from pyspark.sql import SparkSession


# @snippet:1:off

class Standalone:

    def __init__(self):
        pass

    @staticmethod
    def init1():
        # @snippet:2:on
        conf = (SparkConf()
                .setAppName("intensity_example")
                .setMaster('yarn')
                .set('spark.driver.memory', '16G')
                )
        sc = SparkContext(conf=conf)
        spark = SparkSession(sc)


        intensity = DevicePropertyDataQuery.builder(spark).system("CMW") \
            .startTime("2018-06-17 00:00:00.000").endTime("2018-06-20 00:00:00.000") \
            .entity().parameter("PR.BCT/HotspotIntensity").build()
        intensity.count()

        print("Found %d intensity data points." % intensity.count())
        # @snippet:2:off

    @staticmethod
    def init2():
        # @snippet:3:on
        spark = SparkSession.builder.master("local").appName('intensity_example').getOrCreate()
        # @snippet:3:off