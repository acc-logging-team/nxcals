# @snippet:8:on
import numpy as np
import pyspark.sql.functions as F
from nxcals.api.extraction.data.builders import DataQuery
from pyspark.sql.functions import col
from pyspark.sql.types import ArrayType
from pyspark.sql.types import DoubleType
from pyspark.sql.types import StructField
from pyspark.sql.types import StructType


# @snippet:8:off

class Udf:

    def __init__(self):
        pass

    @staticmethod
    def demo_udf(spark):

        # @snippet:7:on
        # @snippet:1:on
        data = DataQuery.builder(spark).byVariables().system('CMW') \
            .startTime('2019-01-01 16:00:00.000').endTime('2019-04-01 17:00:00.000') \
            .variable('L4LT.PULLER-I:WAVEFORM').build().select('nxcals_value')
        # @snippet:1:off

        # @snippet:2:on
        vectors = data.withColumn("nx_value", col("nxcals_value")["elements"]).select("nx_value")
        # @snippet:2:off
        vectors.printSchema()

        # @snippet:3:on
        schema = StructType([
            StructField("real", ArrayType(DoubleType()), False),
            StructField("imag", ArrayType(DoubleType()), False)
        ])
        # @snippet:3:off

        # @snippet:4:on
        def fft_arrays(x):

            tran = np.fft.fft(np.sin(x))
            r = tran.real
            i = tran.imag
            return r.tolist(), i.tolist()
        # @snippet:4:off

        # @snippet:5:on
        spark_fft_array = F.udf(fft_arrays, schema)
        # @snippet:5:off

        # @snippet:6:on
        result = vectors.withColumn('fft', spark_fft_array('nx_value'))
        result.show()

        a=result.select('nx_value','fft')\
            .withColumn("fft_real", result["fft"].getItem("real"))\
            .withColumn("fft_imag", result["fft"].getItem("imag"))\
            .show()
        # @snippet:6:off
        # @snippet:7:off
