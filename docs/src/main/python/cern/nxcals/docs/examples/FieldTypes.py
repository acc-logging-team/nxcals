# @snippet:1:on
from nxcals.api.extraction.data.builders import DataQuery
from pyspark.sql.functions import col


# @snippet:1:off

class FieldTypes:

    def __init__(self):
        pass


    @staticmethod
    def query(spark):
        # @snippet:2:on
        df1 = DataQuery.builder(spark).byEntities().system('CMW') \
            .startTime('2018-04-29 00:00:00.000').endTime('2018-04-30 00:00:00.000') \
            .entity().keyValues({'device': 'LHC.LUMISERVER', 'property': 'CrossingAngleIP1'}) \
            .build()
        # @snippet:2:off

        # @snippet:3:on
        df1.printSchema()   # prints schema in the tree format
        # @snippet:3:off

        # @snippet:4:on
        df1.dtypes # DataFrame property returning names and data types of all the columns
        # @snippet:4:off

        # @snippet:5:on
        df1.schema # DataFrame property returning its schema as StructType(List(StructField(name, Spark dataType, nullable), ...))
        # @snippet:5:off

        # @snippet:6:on
        df1.schema.fields # Schema property returning List of StructField(name, Spark dataType, nullable)
        # @snippet:6:off

        # @snippet:7:on
        # Getting data types from schema in Spark as a dictionary
        d = dict([f.name, f.dataType] for f in df1.schema.fields)
        d['Moving']
        # @snippet:7:off

        # @snippet:8:on
        df2 = DataQuery.builder(spark).byVariables().system('CMW') \
            .startTime('2018-05-21 00:00:00.000').endTime('2018-05-21 00:05:00.000') \
            .variable('SPS.BCTDC.51895:TOTAL_INTENSITY') \
            .build()
        # @snippet:8:off

        # @snippet:9:on
        df2.schema.fields
        # @snippet:9:off

        # @snippet:10:on
        elements = df2.withColumn("nx_elements", col("nxcals_value.elements")).withColumn("nx_dimensions", col("nxcals_value.dimensions")).select("nx_elements")
        elements.take(3)
        # @snippet:10:off

        # @snippet:11:on
        dimensions = df2.withColumn("nx_elements", col("nxcals_value")["elements"]).withColumn("nx_dimensions", col("nxcals_value")["dimensions"]).select("nx_dimensions")
        dimensions.take(3)
        # @snippet:11:off

        # @snippet:12:on
        df3 = DataQuery.builder(spark).byVariables().system('CMW') \
            .startTime('2018-08-15 00:00:00.000').endTime('2018-08-30 00:00:00.000') \
            .variable('HIE-BCAM-T2M03:RAWMEAS#NPIXELS') \
            .build()
        # @snippet:12:off

        # @snippet:13:on
        matrices = df3.withColumn("matrix", col("nxcals_value.elements")) \
                .withColumn("dim1", col("nxcals_value.dimensions")[0]) \
                .withColumn("dim2", col("nxcals_value.dimensions")[1]) \
                .select("matrix", "dim1", "dim2")
        matrices.take(2)
        # @snippet:13:off



