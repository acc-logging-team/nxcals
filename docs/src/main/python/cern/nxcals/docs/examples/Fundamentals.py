# @snippet:1:on
from nxcals.api.extraction.data.builders import *
from pyspark.sql.functions import broadcast
# @snippet:1:off

class Fundamentals:

    def __init__(self):
        pass

    @staticmethod
    def query(spark):

        # @snippet:2:on
        funds = DataQuery.builder(spark).byVariables() \
            .system('CMW') \
            .startTime('2018-09-03 00:00:00.000').endTime('2018-09-03 01:00:00.000') \
            .fieldAliases({'LSA_CYCLE': ['__LSA_CYCLE__', 'lsaCycleName']}) \
            .variable('SPS:NXCALS_FUNDAMENTAL') \
            .buildDataset()

        funds.printSchema()
        funds.show(10)

        funds_filtered = funds.select("nxcals_timestamp").where("LSA_CYCLE = 'AWAKE_1INJ_FB60_FT850_Q20_2018_V1' and USER='AWAKE1' ")

        print("fundamentals = ", funds.count())
        print("fundamentals filtered = ", funds_filtered.count())
        # @snippet:2:off

        # @snippet:3:on
        data = DataQuery.builder(spark).byVariables() \
            .system('CMW') \
            .startTime('2018-09-01 00:00:00.000').endTime('2018-09-31 23:59:59.999') \
            .variable('SPS.BQM:BUNCH_INTENSITIES') \
            .buildDataset()

        data_filtered = data.join(broadcast(funds_filtered), "nxcals_timestamp")

        print("data = ", data.count())
        print("data filtered = ", data_filtered.count())

        data_filtered.sort('nxcals_timestamp').show()
        # @snippet:3:off