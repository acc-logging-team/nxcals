import logging
import os
import sys
from pyspark.sql import SparkSession

log = logging.getLogger(__name__)


class PySparkCodeSnippets():

    @classmethod
    def setUpClass(self):
        os.environ['PYSPARK_PYTHON'] = sys.executable
        os.environ['PYSPARK_DRIVER_PYTHON'] = sys.executable

        self.spark = SparkSession.builder.appName('code_snippets').getOrCreate()
        log.debug('Acquired PySpark session on %s', self.__name__)

    @classmethod
    def tearDownClass(self):
        if hasattr(self, 'spark'):
            self.spark.stop()