import jpype
import jpype.imports

from metadataapi.SystemServiceSnippets import SystemServiceSnippets
from metadataapi.EntityServiceSnippets import EntityServiceSnippets
from metadataapi.VariableServiceSnippets import VariableServiceSnippets
from metadataapi.HierarchyServiceSnippets import HierarchyServiceSnippets

from cernextractionapi.FillServiceSnippets import FillServiceSnippets
from cernextractionapi.AggregationServiceSnippets import AggregationServiceSnippets
from cernextractionapi.ExtractionServiceSnippets import ExtractionServiceSnippets

from nxcalsjpype.Rsql import RsqlSnippets

class JavaFromPythonDemo():
    def __init__(self):

        jpype.startJVM(classpath=['venv/nxcals-bundle/jars/*', 'venv/nxcals-bundle/nxcals_jars/*'], convertStrings=True)

        # point metadata client to NXCALS PRO services
        from java.lang import System
        System.setProperty("service.url", "https://cs-ccr-nxcals6.cern.ch:19093,https://cs-ccr-nxcals7.cern.ch:19093,https://cs-ccr-nxcals8.cern.ch:19093")

    def run(self):

        # Follow steps in:
        # https://nxcals-docs.web.cern.ch/current/user-guide/data-access/py4j-jpype-installaton/
        # before executing

        # SystemServiceSnippets.find_system_info()

        # EntityServiceSnippets.find_one_entity()
        # EntityServiceSnippets.find_entity_with_history()
        # EntityServiceSnippets.update_entitiy()

        # VariableServiceSnippets.find_variables_using_pattern_on_name()
        # VariableServiceSnippets.find_variables_using_pattern_on_description()
        # VariableServiceSnippets.create_new_variable()

        # HierarchyServiceSnippets.find_hierarchies_using_pattern_on_path()
        # HierarchyServiceSnippets.attach_new_node()
        # HierarchyServiceSnippets.delete_node()
        # HierarchyServiceSnippets.attach_variable_to_node()
        # HierarchyServiceSnippets.replace_entities_with_single_entity()

        # FillServiceSnippets.getFillService()
        # FillServiceSnippets.getFillWithNumber()
        # FillServiceSnippets.getFillsInTimeWindow()

        # AggregationServiceSnippets.getService()
        # AggregationServiceSnippets.getAggregatedVariableDataAvg()
        # AggregationServiceSnippets.getAggregatedEntityDataMax()
        # AggregationServiceSnippets.getAggregatedVariableDataRepeat()
        # AggregationServiceSnippets.getAggregatedEntityDataInterpolate()
        # AggregationServiceSnippets.getVariableDataAlignedToDataset()
        # AggregationServiceSnippets.getEntityDataAlignedToDataset()
        #
        # ExtractionServiceSnippets.getService()
        # ExtractionServiceSnippets.getVariableDataWhenDataAvailableNoLookup()
        # ExtractionServiceSnippets.getVariableDataAlwaysDoLookup()
        # ExtractionServiceSnippets.getEntityDataWhenDataAvailableNoLookup()
        # ExtractionServiceSnippets.getEntityDataAlwaysDoLookup()
        # ExtractionServiceSnippets.getEntityDataWithCustomLookup()

        RsqlSnippets.find_variable


snippets = JavaFromPythonDemo()
snippets.run()