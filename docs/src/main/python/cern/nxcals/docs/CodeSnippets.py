from DataAccessApi import DataAccessApi
from docs.src.main.python.cern.nxcals.docs.cernextractionapi.AggregationServiceSnippets import \
    AggregationServiceSnippets
from docs.src.main.python.cern.nxcals.docs.cernextractionapi.ExtractionServiceSnippets import ExtractionServiceSnippets
from docs.src.main.python.cern.nxcals.docs.cernextractionapi.FillServiceSnippets import FillServiceSnippets
from examples.CmwQueries import CmwQueries
from examples.GenericQueries import GenericQueries
from examples.MultipleSchemas import MultipleSchemas
from examples.Timestamps import Timestamps
from examples.Udf import Udf
from examples.VectorContexts import VectorContexts
from . import PySparkCodeSnippets


class CodeSnippets(PySparkCodeSnippets):
    def __init__(self):
        DataAccessApi.data_query_for_key_values(self.spark)
        DataAccessApi.data_query_for_key_values_v2(self.spark)
        DataAccessApi.short_data_query(self.spark)
        DataAccessApi.data_query_for_variables(self.spark)
        DataAccessApi.data_query_for_variables_v2(self.spark)
        DataAccessApi.data_query_for_device_property(self.spark)
        DataAccessApi.data_query_for_device_property_v2(self.spark)
        DataAccessApi.data_queries_for_entities_using_static_functions(self.spark)
        DataAccessApi.data_queries_for_variables_using_static_functions(self.spark)
        DataAccessApi.data_query_for_pivot(self.spark)
        DataAccessApi.data_query_for_pivot_by_name(self.spark)

        GenericQueries.query(self.spark)
        CmwQueries.query(self.spark)
        Udf.demo_udf(self.spark)
        VectorContexts.query(self.spark)
        Timestamps.query(self.spark)
        MultipleSchemas.query(self.spark)

        AggregationServiceSnippets.test_py4j()
        FillServiceSnippets.test_py4j()
        ExtractionServiceSnippets.test_py4j()
