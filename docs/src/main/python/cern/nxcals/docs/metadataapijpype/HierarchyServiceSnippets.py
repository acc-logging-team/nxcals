class HierarchyServiceSnippets:

    def __init__(self):
        pass

    @staticmethod
    def find_hierarchies_using_pattern_on_path():
        # @snippet:1:on
        from cern.nxcals.api.extraction.metadata import ServiceClientFactory
        from cern.nxcals.api.extraction.metadata.queries import Hierarchies

        hierarchyService = ServiceClientFactory.createHierarchyService()

        hierarchies = hierarchyService.findAll(Hierarchies.suchThat().path().like("%example%"))
        # @snippet:1:off

    @staticmethod
    def attach_new_node():
        # @snippet:2:on
        from cern.nxcals.api.extraction.metadata import ServiceClientFactory
        from cern.nxcals.api.extraction.metadata.queries import Hierarchies
        from cern.nxcals.api.domain import Hierarchy

        hierarchyService = ServiceClientFactory.createHierarchyService()
        systemName = "CMW"

        systemSpec = ServiceClientFactory.createSystemSpecService().findByName(systemName)

        if systemSpec.isEmpty():
            raise ValueError("No such system specification")

        parent = hierarchyService.findOne(Hierarchies.suchThat().systemName().eq(systemName).and_().path().eq("/example"))

        if parent.isEmpty():
            raise ValueError("No such hierarchy")

        newHierarchy = Hierarchy.builder().name("new node").description("New node description").systemSpec(systemSpec.get()).parent(parent.get()).build()

        modified = hierarchyService.create(newHierarchy)

        modified.toBuilder().description("Modified node description").build()

        hierarchyService.update(modified)
        # @snippet:2:off

    @staticmethod
    def delete_node():
        from cern.nxcals.api.extraction.metadata import ServiceClientFactory
        from cern.nxcals.api.extraction.metadata.queries import Hierarchies

        hierarchyService = ServiceClientFactory.createHierarchyService()
        systemName = "CMW"

        # @snippet:3:on
        leaf = hierarchyService.findOne(Hierarchies.suchThat().systemName().eq(systemName).and_().path().eq("/example/new node"))
        if leaf.isEmpty():
            raise ValueError("No such hierarchy")

        hierarchyService.deleteLeaf(leaf.get())
        # @snippet:3:off

    @staticmethod
    def attach_variable_to_node():
        # @snippet:4:on
        from cern.nxcals.api.extraction.metadata import ServiceClientFactory
        from cern.nxcals.api.extraction.metadata.queries import Hierarchies, Variables
        from java.util import Collections

        hierarchyService = ServiceClientFactory.createHierarchyService()
        variableService = ServiceClientFactory.createVariableService()

        systemName = "CMW"

        hierarchy = hierarchyService.findOne(Hierarchies.suchThat().systemName().eq(systemName).and_().path().eq("/example"))

        if hierarchy.isEmpty():
            raise ValueError("No such hierarchy")

        # Prepare variable to be attached to the hierarchy node
        variable = variableService.findOne(Variables.suchThat().systemName().eq(systemName).and_().variableName().eq("DEMO:VARIABLE"))

        if variable.isEmpty():
            raise ValueError("No such variable")

        # The variable will be added to the set of already attached variables
        hierarchyService.addVariables(hierarchy.get().getId(), Collections.singleton(variable.get().getId()))

        variables = hierarchyService.getVariables(hierarchy.get().getId())
        # @snippet:4:off

    @staticmethod
    def replace_entities_with_single_entity():
        # @snippet:5:on
        from cern.nxcals.api.extraction.metadata import ServiceClientFactory
        from cern.nxcals.api.extraction.metadata.queries import Entities, Hierarchies
        from java.util import Collections
        from com.google.common.collect import Iterables

        hierarchyService = ServiceClientFactory.createHierarchyService()
        entityService = ServiceClientFactory.createEntityService()
        hierarchySystemName = "CERN"
        systemName = "CMW"

        hierarchy = hierarchyService.findOne(Hierarchies.suchThat().systemName().eq(hierarchySystemName).and_().path().eq("/example"))

        if hierarchy.isEmpty():
            raise ValueError("No such hierarchy")

        # Obtain an entity of your interest
        entity = Iterables.getLast(entityService
                                  .findAll(Entities.suchThat().systemName().eq(systemName)))
        print(entity)

        # The entity will replace a set of already attached entities
        hierarchyService.setEntities(hierarchy.get().getId(), Collections.singleton(entity.getId()))

        entities = hierarchyService.getEntities(hierarchy.get().getId())
        print(entities)
        # @snippet:5:off