class SystemServiceSnippets:

    def __init__(self):
       pass

    @staticmethod
    def find_system_info():
        # @snippet:1:on
        from cern.nxcals.api.extraction.metadata import ServiceClientFactory
        systemService = ServiceClientFactory.createSystemSpecService()

        systemData = systemService.findByName("CMW")

        if systemData.isEmpty():
            raise ValueError("No such system")
        # @snippet:1:off
