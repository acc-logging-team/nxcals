class VariableServiceSnippets:

    def __init__(self):
        pass

    @staticmethod
    def find_variables_using_pattern_on_name():
        # @snippet:1:on
        from cern.nxcals.api.extraction.metadata import ServiceClientFactory
        from cern.nxcals.api.extraction.metadata.queries import Variables

        variableService = ServiceClientFactory.createVariableService()

        variables = variableService.findAll(
            Variables.suchThat().systemName().eq("CMW").and_().variableName().like("SPS%"))
        # @snippet:1:off

    @staticmethod
    def find_variables_using_pattern_on_description():
        # @snippet:2:on
        from cern.nxcals.api.extraction.metadata import ServiceClientFactory
        from cern.nxcals.api.extraction.metadata.queries import Variables

        variableService = ServiceClientFactory.createVariableService()

        variables = variableService.findAll(
            Variables.suchThat().systemName().eq("CMW").and_().description().like("%Current Beam Energy%"))
        # @snippet:2:off

    @staticmethod
    def create_new_variable():
        # @snippet:3:on
        from cern.nxcals.api.extraction.metadata import ServiceClientFactory
        from cern.nxcals.api.extraction.metadata.queries import Variables, Entities
        from cern.nxcals.api.domain import VariableConfig, TimeWindow, Variable
        from com.google.common.collect import ImmutableSortedSet

        SYSTEM = "CMW"
        DEMO_VARIABLE = "DEMO:VARIABLE"

        variableService = ServiceClientFactory.createVariableService()

        variable = variableService.findOne(Variables.suchThat().systemName().eq(SYSTEM).and_().variableName().eq(DEMO_VARIABLE))

        if variable.isPresent():

            updatedVariable = variable.get().toBuilder().description("updated description").build()
            variableService.update(updatedVariable)

        else:
            entityService = ServiceClientFactory.createEntityService()

            entity = entityService.findOne(Entities.suchThat().systemName().eq(SYSTEM).and_().keyValues().like("%myUniqueDeviceProperty%"))

            if entity.isEmpty():
                raise ValueError("No such entity")

            config = VariableConfig.builder().entityId(entity.get().getId()).fieldName("myFieldName").validity(
                TimeWindow.between(None, None)).build()

            variableData = Variable.builder().variableName(DEMO_VARIABLE).configs(
                ImmutableSortedSet.of(config)).systemSpec(entity.get().getSystemSpec()).build()

            variableService.create(variableData)
        # @snippet:3:off

    @staticmethod
    def get_sorted_list_of_variables():
        # @snippet:4:on
        from cern.nxcals.api.extraction.metadata import ServiceClientFactory
        from cern.nxcals.api.metadata.queries import Variables  # New package!

        variableService = ServiceClientFactory.createVariableService()

        variables = variableService.findAll(Variables.suchThat()
                                            .variableName().like("SPS%")
                                            .withOptions()
                                            .noConfigs()
                                            .orderBy().variableName().asc()
                                            .limit(10))

        # @snippet:4:off
