class EntityServiceSnippets:

    def __init__(self):
        pass

    @staticmethod
    def find_one_entity():
        # @snippet:1:on
        from cern.nxcals.api.extraction.metadata import ServiceClientFactory
        from cern.nxcals.api.extraction.metadata.queries import SystemSpecs, Entities
        from com.google.common.collect import ImmutableMap

        entityService = ServiceClientFactory.createEntityService()
        systemService = ServiceClientFactory.createSystemSpecService()

        systemData = systemService.findOne(SystemSpecs.suchThat().name().eq("CMW"))

        if systemData.isEmpty():
            raise ValueError("System not found")

        keyValues = ImmutableMap.of("device", "LHC.LUMISERVER", "property", "CrossingAngleIP1")

        entityData = entityService.findOne( Entities.suchThat().systemId().eq(systemData.get().getId()).and_().keyValues().eq(systemData.get(), keyValues))

        if entityData.isEmpty():
            raise ValueError("Entity not found")
        # @snippet:1:off


    @staticmethod
    def find_entity_with_history():
        # @snippet:2:on
        from cern.nxcals.api.extraction.metadata import ServiceClientFactory
        from cern.nxcals.api.extraction.metadata.queries import SystemSpecs, Entities
        from com.google.common.collect import ImmutableMap
        from cern.nxcals.api.utils import TimeUtils

        systemData = ServiceClientFactory.createSystemSpecService().findOne(SystemSpecs.suchThat().name().eq("CMW"))

        if systemData.isEmpty():
            raise ValueError("System not found")

        keyValues = ImmutableMap.of("device", "LHC.LUMISERVER", "property", "CrossingAngleIP1")

        entityService = ServiceClientFactory.createEntityService()

        entities = entityService.findOneWithHistory(
            Entities.suchThat().systemName().eq(systemData.get().getName()).and_().keyValues().eq(systemData.get(), keyValues),
            TimeUtils.getNanosFromString("2017-10-10 14:15:00.000000000"),
            TimeUtils.getNanosFromString("2020-10-26 14:15:00.000000000"))

        if entities.isEmpty():
            raise ValueError("Entity not found")

        for entityHistory in entities.get().getEntityHistory():
            print(entityHistory)
        # @snippet:2:off

    @staticmethod
    def update_entitiy():
        # @snippet:3:on
        from cern.nxcals.api.extraction.metadata import ServiceClientFactory
        from cern.nxcals.api.extraction.metadata.queries import Entities
        from com.google.common.collect import ImmutableMap
        from com.google.common.collect import Sets

        entityService = ServiceClientFactory.createEntityService()
        systemService = ServiceClientFactory.createSystemSpecService()

        systemData = systemService.findByName("CMW")

        if systemData.isEmpty():
            raise ValueError("No such system CMW")

        keyValues = ImmutableMap.of("device", "MY.DEVICE", "property", "MyProperty")

        entityData = entityService.findOne( Entities.suchThat().systemId().eq(systemData.get().getId()).and_().keyValues().eq(systemData.get(), keyValues))

        if entityData.isEmpty():
            raise ValueError("Entity not found")

        # Change entity to set new keyValues
        newKeyValues = ImmutableMap.of("device", "Example.Device", "property", "ExampleNewProperty")
        newEntityData = entityData.get().toBuilder().entityKeyValues(newKeyValues).build()

        # Perform the update
        updatedEntities = entityService.updateEntities(Sets.newHashSet(newEntityData))
        # @snippet:3:off

    @staticmethod
    def entities_search_with_options():
        # @snippet:11:on
        from cern.nxcals.api.extraction.metadata import ServiceClientFactory
        from cern.nxcals.api.metadata.queries import Entities  # new package!
        from com.google.common.collect import ImmutableMap

        entityService = ServiceClientFactory.createEntityService()
        systemService = ServiceClientFactory.createSystemSpecService()

        systemData = systemService.findByName("CMW")

        if systemData.isEmpty():
            raise ValueError("No such system CMW")

        keyValues = ImmutableMap.of("device", "LHC.LUMISERVER", "property", "CrossingAngleIP%")

        query = Entities.suchThat() \
            .keyValues().like(systemData.get(), keyValues) \
            .withOptions() \
            .withHistory("2017-10-10 14:15:00.000000000", "2020-10-26 14:15:00.000000000") \
            .limit(2) \
            .orderBy().keyValues().desc()

        entityData = entityService.findAll(query)
        # @snippet:11:off
