class SparkSessionDemo:

    def __init__(self):
        pass


    @staticmethod
    def simpleSessionSpark():
        # @snippet:1:on
        from pyspark.sql import SparkSession
        import os
        import sys

        # make sure SPARK_HOME is set to our preconfigured bundle
        if "SPARK_HOME" not in os.environ:
            os.environ["SPARK_HOME"] = os.path.join(sys.prefix, "nxcals-bundle")

        # local session
        spark_session = SparkSession.builder.getOrCreate()
        # @snippet:1:off


    @staticmethod
    def simpleSessionSparkWithProps():
        # @snippet:2:on
        from pyspark.sql import SparkSession
        import os
        import sys

        # make sure SPARK_HOME is set to our preconfigured bundle
        if "SPARK_HOME" not in os.environ:
            os.environ["SPARK_HOME"] = os.path.join(sys.prefix, "nxcals-bundle")

        # must set this property if using Spark APIs directly for Yarn
        os.environ['PYSPARK_PYTHON'] = "./environment/bin/python"

        # Yarn session with executors on the cluster
        spark_session = SparkSession.builder \
            .appName("MY_APP") \
            .master("yarn") \
            .config("spark.submit.deployMode", "client") \
            .config("spark.yarn.appMasterEnv.JAVA_HOME", "/var/nxcals/jdk1.11") \
            .config("spark.executorEnv.JAVA_HOME", "/var/nxcals/jdk1.11") \
            .config("spark.yarn.jars", "hdfs:////project/nxcals/lib/spark-3.3.1/*.jar,hdfs:////project/nxcals/nxcals_lib/nxcals_pro/*.jar\"") \
            .config("spark.executor.instances", "4") \
            .config("spark.executor.cores", "1") \
            .config("spark.executor.memory", "1g") \
            .config("sql.caseSensitive", "true") \
            .config("spark.kerberos.access.hadoopFileSystems", "nxcals")\
            .getOrCreate()
        # @snippet:2:off

    @staticmethod
    def simpleSessionSparkWithNXCALSLocal():
       # @snippet:3:on
       # Defaults to local[*] mode
       from nxcals import spark_session_builder
       spark_session = spark_session_builder.get_or_create(app_name='MY_APP')
       # @snippet:3:off



    @staticmethod
    def simpleSessionSparkWithNXCALSYarnFlavor():
        # @snippet:4:on
        from nxcals.spark_session_builder import get_or_create, Flavor 
        # Using Flavor.YARN_SMALL, all properties pre-set but can be overwritten, running on Yarn
        spark_session = get_or_create(app_name='MY_APP', flavor=Flavor.YARN_SMALL,
                                      conf={'spark.executor.instances': '6'})

        # @snippet:4:off


