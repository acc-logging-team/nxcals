from nxcals.api.extraction.data.builders import DataQuery, DevicePropertyDataQuery, EntityQuery, ParameterDataQuery


class DataAccessApi:
    @staticmethod
    def data_query_for_key_values(spark):
        # @snippet:1:on
        df1 = DataQuery.builder(spark).byEntities().system('WINCCOA') \
            .startTime('2018-06-15 00:00:00.000').endTime('2018-06-17 00:00:00.000') \
            .entity().keyValue('variable_name', 'MB.C16L2:U_HDS_3') \
            .build()

        df2 = DataQuery.builder(spark).byEntities().system('CMW') \
            .startTime('2019-04-29 00:00:00.000').endTime('2019-04-30 00:00:00.000') \
            .entity().keyValue('device', 'LHC.LUMISERVER').keyValue('property', 'CrossingAngleIP1') \
            .build()

        df3 = DataQuery.builder(spark).byEntities().system('CMW') \
            .startTime('2019-04-29 00:00:00.000').endTime('2019-04-30 00:00:00.000') \
            .entity().keyValues({'device': 'LHC.LUMISERVER', 'property': 'CrossingAngleIP1'}) \
            .build()

        df4 = DataQuery.builder(spark).byEntities().system('CMW') \
            .startTime('2019-04-29 00:00:00.000').endTime('2019-04-30 00:00:00.000') \
            .entity().keyValuesLike({'device': 'LHC.LUMISERVER', 'property': 'CrossingAngleIP%'}) \
            .build()
        # @snippet:1:off

    def data_query_for_key_values_v2(spark):
        # @snippet:21:on
        df1 = DataQuery.builder(spark).entities().system('WINCCOA') \
            .keyValuesEq({'variable_name': 'MB.C16L2:U_HDS_3'}) \
            .timeWindow('2018-06-15 00:00:00.000', '2018-06-17 00:00:00.000') \
            .build()

        df2 = DataQuery.builder(spark).entities().system('CMW') \
            .keyValuesEq({'device': 'LHC.LUMISERVER', 'property': 'CrossingAngleIP1'}) \
            .timeWindow('2019-04-29 00:00:00.000', '2019-04-30 00:00:00.000') \
            .build()

        df3 = DataQuery.builder(spark).entities().system('CMW') \
            .keyValuesIn([{'device': 'LHC.LUMISERVER', 'property': 'CrossingAngleIP1'}]) \
            .timeWindow('2019-04-29 00:00:00.000', '2019-04-30 00:00:00.000') \
            .build()

        df4 = DataQuery.builder(spark).entities().system('CMW') \
            .keyValuesLike({'device': 'LHC.LUMISERVER', 'property': 'CrossingAngleIP%'}) \
            .timeWindow('2019-04-29 00:00:00.000', '2019-04-30 00:00:00.000') \
            .build()

        df5 = DataQuery.builder(spark).entities().system('CMW').idEq(57336) \
            .timeWindow('2019-04-29 00:00:00.000', '2019-04-30 00:00:00.000') \
            .build()
        # @snippet:21:off

    def short_data_query(spark):
        # @snippet:121:on
        df = DataQuery.builder(spark).entities().system('CMW') \
            .keyValuesEq({'device': 'LHC.LUMISERVER', 'property': 'CrossingAngleIP1'}) \
            .timeWindow('2022-04-22 00:00:00.000', '2022-04-23 00:00:00.000') \
            .build()
        # @snippet:121:off

    @staticmethod
    def data_query_for_variables(spark):
        # @snippet:2:on
        df1 = DataQuery.builder(spark).byVariables() \
            .system('CMW') \
            .startTime('2018-04-29 00:00:00.000').endTime('2018-04-30 00:00:00.000') \
            .variable('LTB.BCT60:INTENSITY') \
            .build()

        df2 = DataQuery.builder(spark).byVariables() \
            .system('CMW') \
            .startTime('2018-04-29 00:00:00.000').endTime('2018-04-30 00:00:00.000') \
            .variableLike('LTB.BCT%:INTENSITY') \
            .build()

        df3 = DataQuery.builder(spark).byVariables() \
            .system('CMW') \
            .startTime('2018-04-29 00:00:00.000').endTime('2018-04-30 00:00:00.000') \
            .variableLike('LTB.BCT50%:INTENSITY') \
            .variable('LTB.BCT60:INTENSITY') \
            .build()
        # @snippet:2:off

    @staticmethod
    def data_query_for_variables_v2(spark):
        # @snippet:22:on
        df1 = DataQuery.builder(spark).variables() \
            .system('CMW') \
            .nameEq('LTB.BCT60:INTENSITY') \
            .timeWindow('2018-04-29 00:00:00.000', '2018-04-30 00:00:00.000') \
            .build()

        df2 = DataQuery.builder(spark).variables() \
            .system('CMW') \
            .nameIn(['LTB.BCT60:INTENSITY', 'LTB.BCT50:INTENSITY']) \
            .timeWindow('2018-04-29 00:00:00.000', '2018-04-30 00:00:00.000') \
            .build()

        df3 = DataQuery.builder(spark).variables() \
            .system('CMW') \
            .nameLike('LTB.BCT%:INTENSITY') \
            .timeWindow('2018-04-29 00:00:00.000', '2018-04-30 00:00:00.000') \
            .build()

        df4 = DataQuery.builder(spark).variables() \
            .system('CMW') \
            .nameLike('LTB.BCT50%:INTENSITY') \
            .nameEq('LTB.BCT60:INTENSITY') \
            .timeWindow('2018-04-29 00:00:00.000', '2018-04-30 00:00:00.000') \
            .build()

        df5 = DataQuery.builder(spark).variables() \
            .system("CMW") \
            .idEq(1005050) \
            .idIn({1011562}) \
            .timeWindow('2018-04-29 00:00:00.000', '2018-04-30 00:00:00.000') \
            .build()
        # @snippet:22:off

    @staticmethod
    def data_query_for_device_property(spark):
        # @snippet:3:on
        df1 = DevicePropertyDataQuery.builder(spark) \
            .system('CMW').startTime('2017-08-29 00:00:00.000').duration(10000000000) \
            .entity().parameter('RADMON.PS-10/ExpertMonitoringAcquisition') \
            .build()

        df2 = DevicePropertyDataQuery.builder(spark) \
            .system('CMW').startTime('2018-04-29 00:00:00.000').endTime('2018-04-30 00:00:00.000') \
            .fieldAliases({'CURRENT 18V': {'current_18V', 'voltage_18V'}}) \
            .entity().device('RADMON.PS-1').property('ExpertMonitoringAcquisition') \
            .entity().parameter('RADMON.PS-10/ExpertMonitoringAcquisition') \
            .build()

        df3 = DevicePropertyDataQuery.builder(spark) \
            .system('CMW').startTime('2017-08-29 00:00:00.000').duration(10000000000) \
            .entity().parameterLike('RADMON.PS-%/ExpertMonitoringAcquisition') \
            .build()
        # @snippet:3:off

    @staticmethod
    def data_query_for_device_property_v2(spark):
        # @snippet:23:on 
        df1 = ParameterDataQuery.builder(spark).system('CMW') \
            .parameterEq('RADMON.PS-10/ExpertMonitoringAcquisition') \
            .timeWindow('2017-08-29 00:00:00.000', '2017-08-29 00:00:10.000') \
            .build()

        df2 = ParameterDataQuery.builder(spark).system('CMW') \
            .deviceEq('RADMON.PS-1').propertyEq('ExpertMonitoringAcquisition') \
            .parameterEq('RADMON.PS-10/ExpertMonitoringAcquisition') \
            .timeWindow('2018-04-29 00:00:00.000', '2018-04-30 00:00:00.000') \
            .fieldAliases({'CURRENT 18V': {'current_18V', 'voltage_18V'}}) \
            .build()

        df3 = ParameterDataQuery.builder(spark).system('CMW') \
            .parameterLike('RADMON.PS-%/ExpertMonitoringAcquisition') \
            .timeWindow('2017-08-29 00:00:00.000', '2017-08-29 00:00:10.000') \
            .build()
        # @snippet:23:off

    @staticmethod
    def data_queries_for_entities_using_static_functions(spark):
        # @snippet:4:on
        df1 = DataQuery.builder(spark).byEntities().system('WINCCOA') \
            .startTime('2018-06-15 00:00:00.000').endTime('2018-06-17 00:00:00.000') \
            .entity().keyValue('variable_name', 'MB.C16L2:U_HDS_3') \
            .build()

        # or using static functions
        df1static = DataQuery.getForEntities(spark, system='WINCCOA',
                                             start_time='2018-06-15 00:00:00.000',
                                             end_time='2018-06-17 00:00:00.000',
                                             entity_queries=[
                                                 EntityQuery(key_values={
                                                     'variable_name': 'MB.C16L2:U_HDS_3'
                                                 })])
        # @snippet:4:off
        # @snippet:5:on
        df2 = DataQuery.builder(spark).byEntities().system('CMW') \
            .startTime('2019-04-29 00:00:00.000').endTime('2019-04-30 00:00:00.000') \
            .entity() \
            .keyValue('device', 'LHC.LUMISERVER') \
            .keyValue('property', 'CrossingAngleIP1') \
            .build()

        # or using static functions
        df2static = DataQuery.getForEntities(spark, system='CMW',
                                             start_time='2019-04-29 00:00:00.000',
                                             end_time='2019-04-30 00:00:00.000',
                                             entity_queries=[
                                                 EntityQuery(key_values={
                                                     'device': 'LHC.LUMISERVER',
                                                     'property': 'CrossingAngleIP1'
                                                 })])
        # @snippet:5:off
        # @snippet:6:on
        key_values = {'device': 'LHC.LUMISERVER',
                      'property': 'CrossingAngleIP1'}
        df3 = DataQuery.builder(spark).entities().system('CMW') \
            .keyValuesEq(key_values) \
            .timeWindow('2019-04-29 00:00:00.000', '2019-04-30 00:00:00.000') \
            .build()

        # or using static functions
        df3static = DataQuery.getForEntities(spark, system='CMW',
                                             start_time='2019-04-29 00:00:00.000',
                                             end_time='2019-04-30 00:00:00.000',
                                             entity_queries=[EntityQuery(key_values=key_values)])
        # @snippet:6:off
        # @snippet:7:on
        key_values_like = {'device': 'LHC.LUMISERVER',
                           'property': 'CrossingAngleIP%'}
        df4 = DataQuery.builder(spark).entities().system('CMW') \
            .keyValuesLike(key_values_like) \
            .timeWindow('2019-04-29 00:00:00.000', '2019-04-30 00:00:00.000') \
            .build()

        # or using static functions
        df4static = DataQuery.getForEntities(spark, system='CMW',
                                             start_time='2019-04-29 00:00:00.000',
                                             end_time='2019-04-30 00:00:00.000',
                                             entity_queries=[
                                                 EntityQuery(key_values_like=key_values_like)])
        # @snippet:7:off
        # @snippet:8:on
        df5 = DataQuery.builder(spark).entities().system('CMW') \
            .keyValuesEq(key_values) \
            .keyValuesLike(key_values_like) \
            .timeWindow('2019-04-29 00:00:00.000', '2019-04-30 00:00:00.000') \
            .build()

        # or using static functions
        df5static = DataQuery.getForEntities(spark, system='CMW',
                                             start_time='2019-04-29 00:00:00.000',
                                             end_time='2019-04-30 00:00:00.000',
                                             entity_queries=[EntityQuery(key_values=key_values),
                                                             EntityQuery(key_values_like=key_values_like)])
        # @snippet:8:off

    @staticmethod
    def data_queries_for_variables_using_static_functions(spark):
        # @snippet:9:on
        df1 = DataQuery.builder(spark).variables() \
            .system('CMW') \
            .nameIn(['LTB.BCT60:INTENSITY', 'LTB.BCT50:INTENSITY']) \
            .timeWindow('2018-04-29 00:00:00.000', '2019-04-30 00:00:00.000') \
            .build()

        # or using static functions
        df1static = DataQuery.getForVariables(spark, system='CMW',
                                              start_time='2018-04-29 00:00:00.000',
                                              end_time='2018-04-30 00:00:00.000',
                                              variables=['LTB.BCT60:INTENSITY',
                                                         'LTB.BCT50:INTENSITY'])
        # @snippet:9:off
        # @snippet:10:on
        df2 = DataQuery.builder(spark).variables() \
            .system('CMW') \
            .nameLike('LTB.BCT%:INTENSITY') \
            .timeWindow('2018-04-29 00:00:00.000', '2018-04-30 00:00:00.000') \
            .build()

        # or using static functions
        df2static = DataQuery.getForVariables(spark, system='CMW',
                                              start_time='2018-04-29 00:00:00.000',
                                              end_time='2018-04-30 00:00:00.000',
                                              variables_like=['LTB.BCT%:INTENSITY'])
        # @snippet:10:off
        # @snippet:11:on
        df3 = DataQuery.builder(spark).variables() \
            .system('CMW') \
            .nameLike('LTB.BCT%:INTENSITY') \
            .nameIn(['LTB.BCT60:INTENSITY', 'LTB.BCT50:INTENSITY']) \
            .timeWindow('2018-04-29 00:00:00.000', '2018-04-30 00:00:00.000') \
            .build()

        # or using static functions
        df3static = DataQuery.getForVariables(spark, system='CMW',
                                              start_time='2018-04-29 00:00:00.000',
                                              end_time='2018-04-30 00:00:00.000',
                                              variables=['LTB.BCT60:INTENSITY',
                                                         'LTB.BCT50:INTENSITY'],
                                              variables_like=['LTB.BCT%:INTENSITY'])
        # @snippet:11:off

    @staticmethod
    def data_query_for_pivot(spark):
        # @snippet:12:on
        cern_nxcals_api = spark._jvm.cern.nxcals.api
        Variables = cern_nxcals_api.extraction.metadata.queries.Variables

        variableService = cern_nxcals_api.extraction.metadata.ServiceClientFactory.createVariableService()
        variables = variableService.findAll(Variables.suchThat().variableName().like("LTB.BCT%:INTENSITY"))

        dataset = DataQuery.getAsPivot(spark, '2018-04-29 00:00:00.000', '2018-04-30 00:00:00.000', variables)
        # @snippet:12:off

    @staticmethod
    def data_query_for_pivot_by_name(spark):
        # @snippet:13:on
        dataset = DataQuery.getAsPivot(spark, '2018-04-29 00:00:00.000', '2018-04-30 00:00:00.000',
                                       system="CMW", variable_names=["LTB.BCT50:INTENSITY"])
        # @snippet:13:off
