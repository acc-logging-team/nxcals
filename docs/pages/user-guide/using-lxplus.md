For optimal NXCALS operation on LXPLUS, you can follow below instructions for prepare and later activate your virtual environment with other needed configuration steps:

## Creating virtual environment (before first usage)
You should run it once, to create virtual env. Feel free to adjust `VENV_PATH` - but keep in mind, to use the same later to activate venv.

=== "LXPLUS9"
    ```bash
    VENV_PATH=~/nxcals-env
    python -m venv $VENV_PATH
    source $VENV_PATH/bin/activate
    python -m pip install -U --index-url https://acc-py-repo.cern.ch/repository/vr-py-releases/simple --trusted-host acc-py-repo.cern.ch nxcals
    ```
=== "LXPLUS7/8"
    ```bash
    source /cvmfs/sft.cern.ch/lcg/views/LCG_102b/x86_64-centos7-gcc11-opt/setup.sh
    VENV_PATH=~/nxcals-env
    unset PYTHONPATH
    python -m venv $VENV_PATH
    source $VENV_PATH/bin/activate
    python -m pip install -U --index-url https://acc-py-repo.cern.ch/repository/vr-py-releases/simple --trusted-host acc-py-repo.cern.ch nxcals
    ```

## Creating keytab file (optional, recommended for LXPLUS8/9)
If you are using LXPLUS8 and LXPLUS9, then you will have to login to kerberos every time, when you will start new terminal session (and you want to use NXCALS). To simplify logging, you can generate keytab file, which allows you to login without passing password every time. To do so, please follow instruction in [authentication chapter](./data-access/authentication.md#kerberos-keytab-file-generation).

## Running

=== "LXPLUS9 (with Keytab)"
    ```bash
    source ~/nxcals-env/bin/activate
    export KRB5CCNAME=$(mktemp)
    chmod 600 $KRB5CCNAME
    kinit -f -k -t $HOME/.keytab $USER
    python3
    ```
=== "LXPLUS9 (without Keytab)"
    ```bash
    source ~/nxcals-env/bin/activate
    export KRB5CCNAME=$(mktemp)
    chmod 600 $KRB5CCNAME
    kinit -f
    python3
    ```
=== "LXPLUS8 (with Keytab)"
    ```bash
    source /cvmfs/sft.cern.ch/lcg/views/LCG_102b/x86_64-centos7-gcc11-opt/setup.sh
    source ~/nxcals-env/bin/activate
    unset SPARK_HOME
    export JAVA_HOME=/usr/lib/jvm/jre-11
    export KRB5CCNAME=$(mktemp)
    chmod 600 $KRB5CCNAME
    kinit -f -k -t $HOME/.keytab $USER
    python3
    ```
=== "LXPLUS8 (without Keytab)"
    ```bash
    source /cvmfs/sft.cern.ch/lcg/views/LCG_102b/x86_64-centos7-gcc11-opt/setup.sh
    source ~/nxcals-env/bin/activate
    unset SPARK_HOME
    export JAVA_HOME=/usr/lib/jvm/jre-11
    export KRB5CCNAME=$(mktemp)
    chmod 600 $KRB5CCNAME
    kinit -f
    python3
    ```
=== "LXPLUS7"
    ```bash
    source /cvmfs/sft.cern.ch/lcg/views/LCG_102b/x86_64-centos7-gcc11-opt/setup.sh
    source ~/nxcals-env/bin/activate
    unset SPARK_HOME
    python3
    ```

## Example
You can run simple example to check if everything works.

```python
from nxcals.spark_session_builder import get_or_create
from nxcals.api.extraction.data.builders import DataQuery

spark = get_or_create()
query = DataQuery.builder(spark).entities().system('CMW').keyValuesEq({"property": "Acquisition", "device": "EA.XSEC"}).timeWindow("2023-08-08 00:00:00.000", "2023-08-08 00:01:00.000").build()
```