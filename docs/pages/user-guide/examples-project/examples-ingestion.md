This step-by-step-guide aims to walk you through the process of creating application demonstrating usage of [**NXCALS ingestion API.**](../../ingestion-api/)

#What you will build
An application which generates and stores 10 messages in NXCALS Testbed environment in a asynchronous manner.

#Create application

Create main class of the application:
```java
{% include 'generated/src/main/java/cern/nxcals/docs/ingestion1/IngestionExample.java.1' %}
}
```

add method for creation of a sample data to be published:

```java
{% include 'generated/src/main/java/cern/nxcals/docs/ingestion1/IngestionExample.java.2' %}
```

Create a "first iteration" of a method for data publishing:

```java
{% include 'generated/src/main/java/cern/nxcals/docs/ingestion1/IngestionExample.java.3' %}
```

and add it to the main method:
```java hl_lines="3"
{% include 'generated/src/main/java/cern/nxcals/docs/ingestion2/IngestionExample.java.1' %}
```

Adapt `#!java long runExample(long nrOfMessagesToSent)` method so it publishes the data in a asynchronous manner.
Start with adding `#!java ThreadPoolExecutor` to the main class:

```java
{% include 'generated/src/main/java/cern/nxcals/docs/ingestion2/IngestionExample.java.2' %}
```

and proceed to the modification of the method itself:

```java hl_lines="10 19 22 39 40"
{% include 'generated/src/main/java/cern/nxcals/docs/ingestion2/IngestionExample.java.3' %}
```

#Build application

Prepare `gradle.properties` file with the following content:

```properties
nxcalsVersion={% include 'generated/nxcals_version_no_suffix.txt' %}

log4jVersion={% include 'generated/log4j_version.txt' %}
fasterxmlJacksonVersion={% include 'generated/fasterxml_jackson_version.txt' %}
```

!!!important
    Make sure that you have the latest production version of NXCALS!

Prepare a "minimal" version of `build.gradle` file and place it in the main directory of your application.
```groovy
apply plugin:'application'
mainClassName = "cern.myproject.IngestionExample"

dependencies {
    compile group: 'cern.nxcals', name: 'nxcals-ingestion-api', version: nxcalsVersion

    compile group: 'org.apache.logging.log4j', name: 'log4j-1.2-api', version: log4jVersion
    compile group: 'org.apache.logging.log4j', name: 'log4j-slf4j-impl', version: log4jVersion

    //needed for log4j2 yaml file
    compile group: 'com.fasterxml.jackson.core', name: 'jackson-databind', version: fasterxmlJacksonVersion
    compile group: 'com.fasterxml.jackson.dataformat', name: 'jackson-dataformat-yaml', version: fasterxmlJacksonVersion
}
```

!!!important
    Please note required dependency for data ingestion:
    
    - `#!groovy compile group: 'cern.nxcals', name: 'nxcals-ingestion-api'`


Build and run the application:

```bash
../gradlew build run
```

??? note "Click to see expected application output..."
    ```
    > Task :ingestion-api-examples:run
    2019-11-21 15:09:40.488 [WARN ] [main] URLConfigurationSource - No URLs will be polled as dynamic configuration sources.
    2019-11-21 15:09:41.578 [INFO ] [main] IngestionExample - Will try to publish 10 messages with data records, via the NXCALS client publisher
    2019-11-21 15:09:42.068 [INFO ] [kafka-producer-network-thread | producer-1] IngestionExample - Published record for message #1 with timestamp: 1574345381578000000
    2019-11-21 15:09:42.069 [INFO ] [kafka-producer-network-thread | producer-1] IngestionExample - Published record for message #2 with timestamp: 1574345381584000000
    2019-11-21 15:09:42.069 [INFO ] [kafka-producer-network-thread | producer-1] IngestionExample - Published record for message #3 with timestamp: 1574345381584000000
    2019-11-21 15:09:42.069 [INFO ] [kafka-producer-network-thread | producer-1] IngestionExample - Published record for message #4 with timestamp: 1574345381584000000
    2019-11-21 15:09:42.070 [INFO ] [kafka-producer-network-thread | producer-1] IngestionExample - Published record for message #5 with timestamp: 1574345381584000000
    2019-11-21 15:09:42.070 [INFO ] [kafka-producer-network-thread | producer-1] IngestionExample - Published record for message #6 with timestamp: 1574345381584000000
    2019-11-21 15:09:42.070 [INFO ] [kafka-producer-network-thread | producer-1] IngestionExample - Published record for message #7 with timestamp: 1574345381584000000
    2019-11-21 15:09:42.070 [INFO ] [kafka-producer-network-thread | producer-1] IngestionExample - Published record for message #8 with timestamp: 1574345381584000000
    2019-11-21 15:09:42.070 [INFO ] [kafka-producer-network-thread | producer-1] IngestionExample - Published record for message #9 with timestamp: 1574345381584000000
    2019-11-21 15:09:42.070 [INFO ] [kafka-producer-network-thread | producer-1] IngestionExample - Published record for message #10 with timestamp: 1574345381584000000
    2019-11-21 15:09:42.071 [INFO ] [main] IngestionExample - Finished!
    ```

#Summary
You have successfully created application publishing 10 successive messages in a asynchronous way.

#See also

Other pages related to the presented example:

- [Quick guide for Java examples project](../examples-project)
- [Step-by-step guide for data extraction](../examples-extraction)
- [Step-by-step guide for backward compatible data retrieval](../examples-backport)
- [Step-by-step guide for data extraction via thin Spark server](../examples-extraction-thin)
