This step-by-step-guide aims to walk you through the process of creating application demonstrating usage of [**NXCALS Data Extraction Thin API**](../../extraction-api-thin/).
# What you will build 
An application which extracts sample data from two CMW device/property parameters for a given time range 
from NXCALS PRO environment. As well it will demonstrate some very basic Spark operations on the retrieved DataFrames 
including join operation.
It will be followed by extraction of a variable hierarchy with attached variables.

# Create application

Create main class of the application:

```java
{% include 'generated/src/main/java/cern/nxcals/docs/thindataaccess1/ThinDataAccessExample.java.1' %}
```

Add to the main class methods for logging:
```java
{% include 'generated/src/main/java/cern/nxcals/docs/thindataaccess2/ThinDataAccessExample.java.1' %}
```

Add method for getting data for a specific CMW signal, for data retrieval via variables located in a given hierarchy and
for retrieving data related to fills:

```java
{% include 'generated/src/main/java/cern/nxcals/docs/thindataaccess2/ThinDataAccessExample.java.2' %}
```

Add newly created methods to the main method:
```java hl_lines="3 8 11 14"
{% include 'generated/src/main/java/cern/nxcals/docs/thindataaccess2/ThinDataAccessExample.java.3' %}
```
    
# Build application

Prepare `gradle.properties` file with the following content:

```properties
nxcalsVersion={% include 'generated/nxcals_version_no_suffix.txt' %}

cmwDataxVersion={% include 'generated/cmw_datax_version.txt' %}
log4jVersion={% include 'generated/log4j_version.txt' %}
fasterxmlJacksonVersion={% include 'generated/fasterxml_jackson_version.txt' %}
```

!!!important
    Make sure that you have the latest production version of NXCALS!

Prepare a "minimal" version of `build.gradle` file and place it in the main directory of your application.
```groovy
apply plugin:'application'
mainClassName = "cern.myproject.ThinDataAccessExample"

configurations.all {
    resolutionStrategy {
        force 'com.google.code.gson:gson:' + gsonVersion
        
        eachDependency { DependencyResolveDetails details ->
            if (details.requested.group.startsWith('com.fasterxml.jackson')) {
                details.useVersion fasterxmlJacksonVersion
            }
        }

   }
// Depending on your gradle version you might have to do those exclusions (prior to gradle 5.x it did not honor the exclusions from POMs).
      exclude group: "org.springframework.boot", module: "spring-boot-starter-logging"
      exclude group: "org.slf4j", module: "slf4j-log4j12"
      exclude group: "log4j", module: "log4j"
      exclude group: "log4j", module: "apache-log4j-extras"
      exclude group: "ch.qos.logback", module: "logback-classic"
}

dependencies {

    compile group: 'cern.nxcals', name: 'nxcals-extraction-api-thin', version: nxcalsVersion
    compile group: 'cern.nxcals', name: 'nxcals-metadata-api', version: nxcalsVersion

    //in case you want to convert from Avro to ImmutableData
    compile group: 'cern.cmw', name: "cmw-datax", version: cmwDataxVersion


    compile group: 'org.apache.logging.log4j', name: 'log4j-1.2-api', version: log4jVersion
    compile group: 'org.apache.logging.log4j', name: 'log4j-slf4j-impl', version: log4jVersion

    //Required for Yaml in Log4j2
    compile group: 'com.fasterxml.jackson.core', name: 'jackson-databind', version: fasterxmlJacksonVersion
    compile group: 'com.fasterxml.jackson.dataformat', name: 'jackson-dataformat-yaml', version: fasterxmlJacksonVersion

//    compileOnly group: 'org.projectlombok', name: 'lombok', version: lombokVersion

    testCompile group: 'junit', name: 'junit', version: junitVersion

}

run {
    println "Setting properties"
    println System.getProperty('user.name')
    systemProperties['user.name'] = System.getProperty('user.name')
    systemProperties['user.password'] = System.getProperty('user.password')
    if(System.getProperty('user.password') == null) {
        println "Please set user.name and user.password properties with -Duser.password=<pass> when running those tests with gradle. They require RBAC login to work!!!"
    }
}
``` 

Build and run the application:

```bash
../gradlew build run -Duser.name="your.username" -Duser.password="your.password"
```

??? note "Click to see expected application output..."
    ```
    > Task :extraction-api-thin-examples:run
    2021-08-13 17:39:40.371 [INFO ] [main] ThinDataAccessExample - ############## Lets get some cmw data ############
    2021-08-13 17:39:40.841 [INFO ] [main] ThinDataAccessExample - Generated Script: DataQuery.builder(sparkSession)
    .byEntities().system("CMW")
            .startTime("2018-08-01 00:00:00.0").endTime("2018-08-01 01:00:00.0")
            .entity().keyValue("property", "Acquisition").keyValue("device", "SPSBQMSPSv1").build()
    2021-08-13 17:39:40.841 [INFO ] [main] ThinDataAccessExample - You can change the script or create your own, both quote types work (", '): DataQuery.builder(sparkSession)
    .byEntities().system("CMW")
            .startTime("2018-08-01 00:00:00.0").endTime("2018-08-01 01:00:00.0")
            .entity().keyValue("property", "Acquisition").keyValue("device", "SPSBQMSPSv1").build().select('acqStamp',"bunchIntensities")
    2021-08-13 17:39:42.728 [INFO ] [main] ThinDataAccessExample - What are the fields available?
    2021-08-13 17:39:42.729 [INFO ] [main] ThinDataAccessExample - Avro Schema: {"type":"record","name":"nxcals_record","namespace":"nxcals","fields":[{"name":"acqStamp","type":["long","null"]},{"name":"bunchIntensities","type":[{"type":"record","name":"bunchIntensities","namespace":"nxcals.nxcals_record","fields":[{"name":"elements","type":[{"type":"array","items":["double","null"]},"null"]},{"name":"dimensions","type":[{"type":"array","items":["int","null"]},"null"]}]},"null"]}]}
    2021-08-13 17:39:42.729 [INFO ] [main] ThinDataAccessExample - Number of records: 61 
    2021-08-13 17:39:42.730 [INFO ] [main] ThinDataAccessExample - Converting data bytes into Avro Generic Records
    2021-08-13 17:39:42.929 [INFO ] [main] ThinDataAccessExample - Record content: {"acqStamp": 1533084040935000000, "bunchIntensities": {"elements": [2079.5322], "dimensions": [1]}}
    2021-08-13 17:39:42.929 [INFO ] [main] ThinDataAccessExample - Accessing a record field null
    2021-08-13 17:39:42.929 [INFO ] [main] ThinDataAccessExample - Converting data into CMW ImmutableData objects
    2021-08-13 17:39:42.993 [INFO ] [main] ThinDataAccessExample - Immutable record: Name: acqStamp
    Type: Long
    Value: 1533084040935000000
    
    Name: bunchIntensities
    Type: double[]Dims: 1
    Value: 
    {2079.5322}
    2021-08-13 17:39:42.994 [INFO ] [main] ThinDataAccessExample - ############## Lets get some variable data ############
    2021-08-13 17:39:43.071 [WARN ] [main] URLConfigurationSource - No URLs will be polled as dynamic configuration sources.
    2021-08-13 17:39:43.291 [INFO ] [main] ThinDataAccessExample - Getting hierarchy for /EXAMPLE
    2021-08-13 17:39:44.346 [INFO ] [main] ThinDataAccessExample - Found hierarchy: /EXAMPLE
    2021-08-13 17:39:44.810 [INFO ] [main] ThinDataAccessExample - THE END, Happy Exploring Spark Thin API!
    ```

#Summary
You have successfully retrieved a CMW device/property parameter and data from variables attached to a hierarchy.

#See also

Other pages related to the presented example:

- [Quick guide for Java examples project](../examples-project)
- [Step-by-step guide for data ingestion](../examples-ingestion)
- [Step-by-step guide for data extraction](../examples-extraction)
- [Step-by-step guide for backward compatible data retrieval](../examples-backport)
