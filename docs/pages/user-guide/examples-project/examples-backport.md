This step-by-step-guide aims to walk you through the process of creating application demonstrating usage of [**NXCALS backport API.**](../../backport-api/)

#What you will build
An application which retirves timeseries data from NXCALS using CALS backport API (backward compatible).

#Create application

Create main class of the application:
```java
{% include 'generated/src/main/java/cern/nxcals/docs/metadata1/MetadataExamples.java.1' %}
}
```

Create some simple "helper" methods for retrieving Variable and printing datapoints:

```java
{% include 'generated/src/main/java/cern/nxcals/docs/metadata1/MetadataExamples.java.2' %}
```

Create different methods for extracting data via CALS backport API:
```java
{% include 'generated/src/main/java/cern/nxcals/docs/metadata1/MetadataExamples.java.3' %}
    }
```

and add them to the main method:

```java hl_lines="3 4 5 6"
{% include 'generated/src/main/java/cern/nxcals/docs/metadata2/MetadataExamples.java.2' %}
```

#Build application

Prepare `gradle.properties` file with the following content:

```properties
nxcalsVersion={% include 'generated/nxcals_version_no_suffix.txt' %}

springBootVersion={% include 'generated/spring_boot_version.txt' %}
log4jVersion={% include 'generated/log4j_version.txt' %}
fasterxmlJacksonVersion={% include 'generated/fasterxml_jackson_version.txt' %}
```

!!!important
    Make sure that you have the latest production version of NXCALS!

Prepare a "minimal" version of `build.gradle` file and place it in the main directory of your application.
```groovy
apply plugin: 'java'

task(runTimeseries, dependsOn: 'classes', type: JavaExec) {
    main = 'cern.myproject.TimeseriesExamples'
    classpath = sourceSets.main.runtimeClasspath
}

task(runMetadata, dependsOn: 'classes', type: JavaExec) {
    main = 'cern.myproject.MetadataExamples'
    classpath = sourceSets.main.runtimeClasspath
}

task run {
    description 'Run different Backport API examples'
    dependsOn 'runTimeseries'
    dependsOn 'runMetadata'
    tasks.findByName('runMetadata').mustRunAfter 'runTimeseries'
}

dependencies {
    compile group: 'cern.nxcals', name: 'nxcals-backport-api', version: nxcalsVersion

    compile group: 'cern.nxcals', name: 'nxcals-extraction-starter', version: nxcalsVersion
    compile group: 'cern.nxcals', name: 'nxcals-hadoop-pro-config', version: nxcalsVersion

    compile group: 'org.apache.logging.log4j', name: 'log4j-1.2-api', version: log4jVersion
    compile group: 'org.apache.logging.log4j', name: 'log4j-slf4j-impl', version: log4jVersion

    //needed for log4j2 yaml file
    compile group: 'com.fasterxml.jackson.core', name: 'jackson-databind', version: fasterxmlJacksonVersion
    compile group: 'com.fasterxml.jackson.dataformat', name: 'jackson-dataformat-yaml', version: fasterxmlJacksonVersion
}
```

!!!important
    Please note required dependencies for data extraction:
    
    - `#!groovy compile group: 'cern.nxcals', name: 'nxcals-backport-api'`
    - `#!groovy compile group: 'cern.nxcals', name: 'nxcals-extraction-starter'` 
    - `#!groovy compile group: 'cern.nxcals', name: 'nxcals-hadoop-pro-config'`


Build and run the application:

```bash
../gradlew build run
```

??? note "Click to see expected application output..."
    ```
    > Task :backport-api-examples:runTimeseries
    Using shared state token to login
    ERROR StatusLogger No log4j2 configuration file found. Using default configuration: logging only errors to the console.
    Values for variable : PR.DCAFTINJ_1:INTENSITY size: 3
    2018-06-19 02:00:00.0: 0.0
    2018-06-19 02:01:00.0: 0.0
    2018-06-19 02:02:00.0: 0.0
    Values for variable : PR.DCAFTINJ_1:INTENSITY size: 238
    2018-06-19 02:00:00.7: 8.809006690979004
    2018-06-19 02:00:03.1: 0.0
    2018-06-19 02:00:04.3: 0.0
    2018-06-19 02:00:05.5: 0.0
    2018-06-19 02:00:06.7: 0.0
    2018-06-19 02:00:07.9: 0.0
    2018-06-19 02:00:09.1: 0.0
    2018-06-19 02:00:10.3: 0.0
    2018-06-19 02:00:11.5: 0.0
    2018-06-19 02:00:12.7: 0.0
    2018-06-19 02:00:13.9: 0.0
    2018-06-19 02:00:15.1: 0.0
    2018-06-19 02:00:16.3: -0.07446788996458054
    2018-06-19 02:00:17.5: 0.0
    2018-06-19 02:00:18.7: 0.0
    2018-06-19 02:00:19.9: 0.0
    2018-06-19 02:00:21.1: 0.0
    2018-06-19 02:00:22.3: 0.0
    2018-06-19 02:00:23.5: 0.0
    2018-06-19 02:00:24.7: 0.0
    2018-06-19 02:00:25.9: 0.0
    2018-06-19 02:00:27.1: 9.570235252380371
    2018-06-19 02:00:29.5: 0.0
    2018-06-19 02:00:30.7: 0.0
    2018-06-19 02:00:31.9: 0.0
    2018-06-19 02:00:33.1: 0.0
    2018-06-19 02:00:34.3: 0.0
    2018-06-19 02:00:35.5: 0.0
    2018-06-19 02:00:36.7: 0.0
    2018-06-19 02:00:37.9: 0.0
    2018-06-19 02:00:39.1: 0.0
    2018-06-19 02:00:40.3: 0.0
    2018-06-19 02:00:41.5: 0.0
    2018-06-19 02:00:42.7: -0.06538643687963486
    2018-06-19 02:00:43.9: 0.0
    2018-06-19 02:00:45.1: 0.0
    2018-06-19 02:00:46.3: 0.0
    2018-06-19 02:00:47.5: 0.0
    2018-06-19 02:00:48.7: 0.0
    2018-06-19 02:00:49.9: 0.0
    2018-06-19 02:00:51.1: 0.0
    2018-06-19 02:00:52.3: 0.0
    2018-06-19 02:00:53.5: 9.14421558380127
    2018-06-19 02:00:55.9: 0.0
    2018-06-19 02:00:57.1: 0.0
    2018-06-19 02:00:58.3: 0.0
    2018-06-19 02:00:59.5: 0.0
    2018-06-19 02:01:00.7: 0.0
    2018-06-19 02:01:01.9: 0.0
    2018-06-19 02:01:03.1: 0.0
    2018-06-19 02:01:04.3: 0.0
    2018-06-19 02:01:05.5: 0.0
    
    ...
    
    
    2018-06-19 02:04:49.9: 0.0
    2018-06-19 02:04:51.1: 9.281149864196777
    2018-06-19 02:04:53.5: 0.0
    2018-06-19 02:04:54.7: 0.0
    2018-06-19 02:04:55.9: 0.0
    2018-06-19 02:04:57.1: 0.0
    2018-06-19 02:04:58.3: 0.0
    2018-06-19 02:04:59.5: 0.0
    Values for variable : PR.DCAFTINJ_1:INTENSITY max: 0 min: 0
    Last value before 2018-06-19 00:05:00.000
    2018-06-19 02:04:59.5: 0.0
    Last value at most one day before 2019-04-09 00:00:00.000
    2018-06-19 02:04:59.5: 0.0
    Last value at most one day before now
    Absent data
    No data at most one day before now
    Next value after 2018-06-19 00:05:00.000
    2018-06-19 02:05:00.7: 0.0
    Last value at most one day before 2018-06-19 00:05:00.000
    2018-06-19 02:05:00.7: 0.0
    
    > Task :backport-api-examples:runMetadata
    Using shared state token to login
    ERROR StatusLogger No log4j2 configuration file found. Using default configuration: logging only errors to the console.
    Found Variable:PR.DCAFTINJ_1:INTENSITY of type UNDETERMINED and system CMW
    Found Variable:DQQDS.B15R3.RQF.A34:ST_PWR_PERM of type UNDETERMINED and system WINCCOA
    Found Variable:DQQDS.B15R3.RQF.A34:U_REF_N1 of type UNDETERMINED and system WINCCOA
    ```
    
#Create Spark on YARN session (optional)
Depending on the resource requirements for the application, instead of default (local) spark session:

```java
{% include 'generated/src/main/java/cern/nxcals/docs/ServiceBuilderBackportDemo.java.1' %}
    }
```
one can use one of the 3 predefined Spark properties for YARN:

```java
{% include 'generated/src/main/java/cern/nxcals/docs/ServiceBuilderBackportDemo.java.6' %}
```
    
More information about different ways of creating Spark session in Backport API can be found at
[this location](../../spark-session).

#Summary
You have successfully retrieved timeseries data from NXCALS using different CALS backport API methods.

#See also

Other pages related to the presented example:

- [Quick guide for Java examples project](../examples-project)
- [Step-by-step guide for data ingestion](../examples-ingestion)
- [Step-by-step guide for data extraction](../examples-extraction)
- [Step-by-step guide for data extraction via thin Spark server](../examples-extraction-thin)
