#Using Ingestion API
## How to send data to NXCALS?
If you want to connect your system to NXCALS and send data directly 
from your applications/servers using our Java API please send us a mail on acc-logging-support@cern.ch.
We need to register your System and configure the necessary structure of the records (Entity, Partition, Timestamp definitions).

For how to use Ingestion API please look into our example: [Ingestion API Example](../examples-project/examples-ingestion).
All you need to do is to checkout the [example project](https://gitlab.cern.ch/acc-logging-team/nxcals-examples), navigate to the module ```ingestion-api-examples``` and explore.