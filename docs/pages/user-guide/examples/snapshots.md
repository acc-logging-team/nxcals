**Group API** allows to extract information related to *snapshots*  and *variable lists*  which will be presented in the code snippets.

Alternatively the same information can be retrieved using  **QuerySnapshotDataService** available in **Backport API** using the following methods:

- [getSnapshotsFor](./#method1)
- [getVariableListsOfUserWithNameLikeAndDescLike](./#method2)
- [getVariablesOfDataTypeInVariableList](./#method3)


Operation such as [Snapshot saving](./#saving) (creation and update) can only be performed using **Group API**.


Initialisation of services required for the code snippets:

```java
{% include 'generated/src/main/java/cern/nxcals/docs/examples/Snapshots.java.1' %}
```

<a name="method1"></a>
### Getting user snapshots with the given criteria.

Retrieval of *snapshots* owned by user "jjgras" having names beginning with "BI_BWS". Snapshot attributes are printed
including information about selected variables:
   
=== "Groups API"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/Snapshots.java.3' %}
    ```   
=== "Backport API"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/Snapshots.java.2' %}
    ```

???+ note "Click to see expected application output..."
    ```
    owner: jjgras name: BI_BWS_PSB-R2
    	Key: Chart Type, Value: Versus Time
    	Key: Data Source Preferences, Value: LDB_PRO
    	Key: Derivation Selection, Value: RAW
    	Key: Dynamic Duration, Value: 1
    	Key: Prior Time, Value: Start of day
    	Key: Selected Variables, Value: [BR2.BWS.2L1.H_ROT:ACQ_STATE, BR2.BWS.2L1.H_ROT:DEV_STATE, ... , BR2.BWS.2L1.V_ROT:WIRE_SPEED]
    	Key: Selection Output, Value: Statistics
    	Key: Time, Value: DAYS
    	Key: Time Zone, Value: LOCAL_TIME
    	Key: End Time Dynamic, Value: true
    owner: jjgras name: BI_BWS_SPS-414
    	Key: Chart Type, Value: Versus Time
    	Key: Data Source Preferences, Value: LDB_PRO
    	Key: Derivation Selection, Value: RAW
    	Key: Dynamic Duration, Value: 1
    	Key: Prior Time, Value: Start of day
    	Key: Selected Variables, Value: [SPS.BWS.414.H_ROT:ACQ_CLOCK_DIV, SPS.BWS.414.H_ROT:ACQ_STATE, ... , SPS.BWS.414.V_ROT:WIRE_SPEED]
    	Key: Selection Output, Value: Statistics
    	Key: Time, Value: DAYS
    	Key: Time Zone, Value: LOCAL_TIME
    	Key: End Time Dynamic, Value: true
    	...
    ```
    
!!!note
    Visibility search critieria present in the Group API determines whether a user can retrieve information.
    Only a group owner can access data with a visibility set to **PROTECTED**. Anyone can access groups with visibility set to **PUBLIC**.

<a name="method2"></a>
### Gets a List of VariableList objects belonging to the given user

Query for *variable lists* owned by user "cdroderi" having name starting with "MDB" and any description:

=== "Groups API"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/Snapshots.java.5' %}
    ```
=== "Backport API"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/Snapshots.java.4' %}
    ```

???+ note "Click to see expected application output..."
    ```
    Onwer cdroderi, List name: MDB-DL-PROC-BIS-LOGGING
    Onwer cdroderi, List name: MDB-DL-PROC-CALS-LOGGING-PROCESS-1
    Onwer cdroderi, List name: MDB-DL-PROC-CESAR-LOGGING
    Onwer cdroderi, List name: MDB-DL-PROC-CESAR-SERVER
    Onwer cdroderi, List name: MDB-DL-PROC-CNGS-LOGGING
    Onwer cdroderi, List name: MDB-DL-PROC-DEV1
    Onwer cdroderi, List name: MDB-DL-PROC-ISOLDE-LOGGING
    Onwer cdroderi, List name: MDB-DL-PROC-LHC-BI-LOGGING
    Onwer cdroderi, List name: MDB-DL-PROC-LHC-BLM-MDB
    Onwer cdroderi, List name: MDB-DL-PROC-LHC-BT-LOGGING
    Onwer cdroderi, List name: MDB-DL-PROC-LHC-FGC-LOGGING
    Onwer cdroderi, List name: MDB-DL-PROC-LHC-OP-LOGGING
    Onwer cdroderi, List name: MDB-DL-PROC-LHC-RADIATION-LOGGING
    ...
    ```

<a name="method3"></a>
### Get a set of variables with a given datatype

Extract all the variables of any type attached to "BI_BCT_LHCDC2" *variable list*:

=== "Groups API"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/Snapshots.java.7' %}
    ```
=== "Backport API"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/Snapshots.java.6' %}
    ```

???+ note "Click to see expected application output..."
    ```
    LHC.BCTDC.A6R4.B1:BEAM_INTENSITY
    LHC.BCTDC.A6R4.B2:BEAM_INTENSITY
    LHC.BCTDC.B6R4.B1:BEAM_INTENSITY
    LHC.BCTDC.B6R4.B2:BEAM_INTENSITY
    LHC.BCTFR.A6R4.B1:BEAM_INTENSITY
    LHC.BCTFR.A6R4.B2:BEAM_INTENSITY
    LHC.BCTFR.B6R4.B1:BEAM_INTENSITY
    LHC.BCTFR.B6R4.B2:BEAM_INTENSITY
    ```

<a name="saving"></a>    
### Snapshots saving    

In order to create a *snapshot* an ordinary group (labeled "SNAPSHOT") must be created. In the example below
we are checking for the presence of the "Test snapshot" in order to (re)create it. After creation of the snapshot
variables starting with ""LHCB:LUMI%"" will be attached.

**Snapshot creation and deletion:**

=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/Snapshots.java.10' %}
    ```

The result of the operation can be visualised using the following code snippet:

=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/Snapshots.java.12' %}
    ```

???+ note "Click to see expected application output..."
    ```
    Variable name: LHCB:LUMI_REC_INT_IONS Variable id: 153846
    Variable name: LHCB:LUMI_COLLISION_RATE_ERR Variable id: 153947
    Variable name: LHCB:LUMI_COLLISION_RATE Variable id: 153938
    Variable name: LHCB:LUMI_TOT_INST Variable id: 153946
    Variable name: LHCB:LUMI_DEL_STABLE_INT_IONS Variable id: 153845
    ```

**Snapshot update:**

Snapshot which needs to be modified (for example for changing of the description) must be retrieved in the first place:

=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/Snapshots.java.11' %}
    ```

