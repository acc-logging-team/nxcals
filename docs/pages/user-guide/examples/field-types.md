Based on the information originating from DataFrame, field data type can be determined in different ways.
Selected DataFrames will be used to illustrate data type for:

- [scalars](./#scalars)
- [vectors](./#vectors)
- [matrices](./#matrices)

Complete list of Spark data types is available on
[Spark Apache documentation pages](https://spark.apache.org/docs/{% include 'generated/spark_version.txt' %}/sql-reference.html)

<a name="scalars"></a>
<h5>Example of a scalar data type</h5>
Required libraries for the sample code:

=== "Python"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/examples/FieldTypes.py.1' %}
    ```
=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/FieldTypes.java.1' %}
    ```

Creation of DataFrame based on entity having scalar fields:

=== "Python"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/examples/FieldTypes.py.2' %}
    ```
=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/FieldTypes.java.2' %}
    ```

In order to visualise schema (including DataFrame **column types**) the following method can be used:

=== "Python"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/examples/FieldTypes.py.3' %}
    ```
=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/FieldTypes.java.3' %}
    ```

???+ note "Click to see/hide expected application output..."
    ```
    root
     |-- DeltaCrossingAngle: double (nullable = true)
     |-- Moving: long (nullable = true)
     |-- __record_timestamp__: long (nullable = true)
     |-- __record_version__: long (nullable = true)
     |-- acqStamp: long (nullable = true)
     |-- class: string (nullable = true)
     |-- cyclestamp: long (nullable = true)
     |-- device: string (nullable = true)
     |-- property: string (nullable = true)
     |-- selector: string (nullable = true)
     |-- nxcals_entity_id: long (nullable = true)
    ```

In order to retrieve that information in a form of array with all column names and their data types a **dtype** method can be used:

=== "Python"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/examples/FieldTypes.py.4' %}
    ```
=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/FieldTypes.java.4' %}
    ```

???+ note "Click to see/hide expected application output..."
    ```
    [('DeltaCrossingAngle', 'double'),
     ('Moving', 'bigint'),
     ('__record_timestamp__', 'bigint'),
     ('__record_version__', 'bigint'),
     ('acqStamp', 'bigint'),
     ('class', 'string'),
     ('cyclestamp', 'bigint'),
     ('device', 'string'),
     ('property', 'string'),
     ('selector', 'string'),
     ('nxcals_entity_id', 'bigint')]
    ```

To obtain *Spark data type* we can refer to the DataFrame schema:

=== "Python"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/examples/FieldTypes.py.5' %}
    ```
=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/FieldTypes.java.5' %}
    ```

???+ note "Click to see/hide expected application output..."
    ```
    StructType(List(
        StructField(DeltaCrossingAngle,DoubleType,true),
        StructField(Moving,LongType,true),StructField(__record_timestamp__,LongType,true),
        StructField(__record_version__,LongType,true),StructField(acqStamp,LongType,true),
        StructField(class,StringType,true),StructField(cyclestamp,LongType,true),
        StructField(device,StringType,true),StructField(property,StringType,true),
        StructField(selector,StringType,true),StructField(nxcals_entity_id,LongType,true)
    ))
    ```

or directly to the DataFrame schema fields:

=== "Python"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/examples/FieldTypes.py.6' %}
    ```
=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/FieldTypes.java.6' %}
    ```

???+ note "Click to see/hide expected application output..."
    ```
    [StructField(DeltaCrossingAngle,DoubleType,true),
     StructField(Moving,LongType,true),
     StructField(__record_timestamp__,LongType,true),
     StructField(__record_version__,LongType,true),
     StructField(acqStamp,LongType,true),
     StructField(class,StringType,true),
     StructField(cyclestamp,LongType,true),
     StructField(device,StringType,true),
     StructField(property,StringType,true),
     StructField(selector,StringType,true),
     StructField(nxcals_entity_id,LongType,true)]
    ```

For convenience we can create the field name <-> Spark data type mapping:

=== "Python"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/examples/FieldTypes.py.7' %}
    ```
=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/FieldTypes.java.7' %}
    ```

```
LongType
```
            
<a name="vectors"></a>
<h5>Example of a vector data type</h5>
Vector and matrix data are expressed in NXCALS as a **complex type** using two ArrayType types for holding of vectro/matrix data (called *elements*)
and for describing "shape" of the data through the list of *dimensions*. The concept is ilustrated through the sample code below:
 
Creation of DataFrame containing vector data:

=== "Python"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/examples/FieldTypes.py.8' %}
    ```
=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/FieldTypes.java.8' %}
    ```
 
having following schema:

=== "Python"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/examples/FieldTypes.py.9' %}
    ```
=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/FieldTypes.java.9' %}
    ```
 
???+ note "Click to see/hide expected application output..."
    ```
    StructType(List(
           StructField(nxcals_value,StructType(List(
               StructField(elements,ArrayType(DoubleType,true),true),
               StructField(dimensions,ArrayType(IntegerType,true),true)
               )),true),
           StructField(nxcals_entity_id,LongType,true),StructField(nxcals_timestamp,LongType,true),
           StructField(nxcals_variable_name,StringType,true)
       ))
    ```
 
Selecting first 3 vectors (elements):

=== "Python"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/examples/FieldTypes.py.10' %}
    ```
=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/FieldTypes.java.10' %}
    ```
  
??? note "Click to see/hide expected application output..."
    ```
    [Row(nx_elements=[0.2579849, 0.28976566, 0.30659077, 0.29101196, 0.27730262, 0.27481002, 0.25362283, ... , 0.27543315]), 
     Row(nx_elements=[0.22745048, 0.24552187, 0.24302925, 0.23118937, 0.2374209, 0.24552187, 0.22620416, ... , 0.24302925]),
     Row(nx_elements=[61.52985, 1040.4697, 1529.6321, 1572.7029, 1562.2429, 1557.9358, 1555.4746, 1554.244, ... , 2.461194])
    ]       
    ```

and their corresponding sizes (please note alternative notation for referencing field names):

=== "Python"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/examples/FieldTypes.py.11' %}
    ```
=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/FieldTypes.java.11' %}
    ```

??? note "Click to see/hide expected application output..."
    ```
    [Row(nx_dimensions=[1228]), Row(nx_dimensions=[1228]), Row(nx_dimensions=[1836])]
    ```

<a name="matrices"></a>
<h5>Example of a matrix data type</h5>
Creation of DataFrame containing Matrix data:

=== "Python"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/examples/FieldTypes.py.12' %}
    ```
=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/FieldTypes.java.12' %}
    ```

Retrieving matrix data present in the DataFrame:

=== "Python"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/examples/FieldTypes.py.13' %}
    ```
=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/FieldTypes.java.13' %}
    ```

??? note "Click to see/hide expected application output..."
    ```
    [Row(matrix=[14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15, 10, 17, 15, 0, 0, ... , 0], dim1=100, dim2=10),
     Row(matrix=[14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 14, 10, 16, 13, 0, 0, ... , 0], dim1=100, dim2=10)]
    ```
