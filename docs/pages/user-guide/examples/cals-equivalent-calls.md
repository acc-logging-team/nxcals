This document presents possible NXCALS implementations of selected CALS extraction API methods. NXCALS examples make usage of [Data Extraction API](../../extraction-api/)


!!!note 
    All CALS java examples used in the document assume initialization of metadata and timeseries services.
    NXCALS example requires Kerberos authentication and SparkSession being present
    (for more details refer to NXCALS [example projects](../../examples-project/examples-project/)).
    
    Corresponding code (for both projects) is presented below. In case of NXCALS there are 2 types of the
    initialization code. One making use of Spring Framework and the other based on vanilla Java:
  
=== "CALS Initialization"
    ```java
    import cern.accsoft.cals.extr.client.service.MetaDataService;
    import cern.accsoft.cals.extr.client.service.ServiceBuilder;
    import cern.accsoft.cals.extr.client.service.TimeseriesDataService;
    import cern.accsoft.cals.extr.domain.core.datasource.DataLocationPreferences;
    
    public class Demo {
    
        private MetaDataService metaDataService;
        private TimeseriesDataService timeseriesDataService;     
           
        public Demo() {
            final ServiceBuilder builder = ServiceBuilder
                .getInstance("TIMBER", "TIMBER", DataLocationPreferences.LDB_PRO);
            
            metaDataService = builder.createMetaService();
            timeseriesDataService = builder.createTimeseriesService();
        }
        
        public static void main(final String args[]) {
       
            final Demo demo = new Demo();
            
            // code snippets follow
        }
    }
    ```
=== "NXCALS Initialization with Spring"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/NxcalsDemo.java.1' %}
        ...
    }
    ```
=== "Without Spring"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/NxcalsDemoNoSpring.java.1' %}
    ```
    

# Getting data for the most recent value prior to the time window

**CALS implementation**

=== "Java"
    ```java
    Variable var = demo.metaDataService
      .getVariablesWithNameInListofStrings(Arrays.asList("ZT10.QFO03.ACQUISITION:CURRENT"))
      .getVariable(0);
    
    Timestamp refTime = Timestamp.from(Instant.parse("2018-05-23T00:05:54.500Z"));
    LoggingTimeInterval loggingTimeInterval = LoggingTimeInterval.DAY;
    
    demo.timeseriesDataService
      .getLastDataPriorToTimestampWithinUserInterval(var, refTime, loggingTimeInterval)
      .print(LoggingTimeZone.UTC_TIME);
    ```

```markdown
	Timestamp(UTC_TIME): "2018-05-23 00:05:53.500" Value: 298.55
```

**NXCALS equivalent**

Assuming that interval duration prior to the beginning of time window (reference time) is equal 1 day:

=== "Python"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/examples/NxcalsDemo.py.1' %}
    ```
=== "Java"
    ```java
    // source the nxcals query builders
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/NxcalsDemo.java.2' %}
    ```

```markdown
+-------------------+---------+
|         cycleStamp|  current|
+-------------------+---------+
|1527033953500000000|298.55392|
+-------------------+---------+
```

# Getting data for a variable within a time window and the most recent value prior to the time window

**CALS implementation**

=== "Java"
    ```java
    Variable var = demo.metaDataService
        .getVariablesWithNameInListofStrings(Arrays.asList("ZT10.QFO03.ACQUISITION:CURRENT"))
        .getVariable(0);
    
    Timestamp startTime = Timestamp.from(Instant.parse("2018-05-21T00:00:00.000Z"));
    Timestamp endTime = Timestamp.from(Instant.parse("2018-05-23T13:30:00.000Z"));
    
    demo.timeseriesDataService
            .getDataInTimeWindowAndLastDataPriorToTimeWindowWithinDefaultInterval(
                    var, startTime, endTime
            ).stream().sorted(Comparator.comparing(TimeseriesData::getStamp))
            .limit(5).forEach(Demo::printValue);
    
    ...
    
    private static void printValue(TimeseriesData data) {
        try {
            System.out.printf("%s -> %f\n",
                    data.getFormattedStamp(LoggingTimeZone.UTC_TIME), data.getDoubleValue());
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }
    ```

```markdown
2018-05-23 00:05:53.500 -> 298.550000
2018-05-23 00:05:54.700 -> 298.590000
2018-05-23 00:05:55.900 -> 298.600000
2018-05-23 00:05:57.100 -> 298.580000
2018-05-23 00:05:58.300 -> 298.610000	
```

**NXCALS equivalent**

Assuming that interval duration prior to the beginning of time window (start time) is equal 1 month.
It is **not** exactly the same implementation as done in CALS where we sample data before a reference time
starting with small data range and extending it in case the most recent values is still not found. 

The example below would have to use the iterations implemented directly in the code or through one
of the available timeseries libraries for Spark (no concrete recommendations at the moment of writing of this document).

=== "Python"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/examples/NxcalsDemo.py.2' %}    
    ```
=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/NxcalsDemo.java.3' %}
    ```

```markdown
+-------------------+---------+
|         cycleStamp|  current|
+-------------------+---------+
|1526860799500000000|298.55893|
|1526860801900000000|298.59607|
|1526860803100000000|298.61493|
|1526860804300000000| 298.5677|
|1526860805500000000|298.56143|
+-------------------+---------+
only showing top 5 rows
```     
     
# Getting data for a variable within a time window filtered by timestamps


!!!important
    For this particular example, we have selected a signal being cycle timestamped 
    in order to obtain the same values at the same timestamps for both CALS and NXCALS.
    That could not be the case for signals using acquisition timestamps (different for both systems).

**CALS implementation**

=== "Java"
    ```java
    final Demo demo = new Demo();
    
    Variable var = demo.metaDataService
        .getVariablesWithNameInListofStrings(Arrays.asList("ZT10.QFO03.ACQUISITION:CURRENT"))
        .getVariable(0);
    
    List<Timestamp> stamps = Arrays.asList(
        Timestamp.from(Instant.parse("2018-05-23T00:05:54.700Z")),
        Timestamp.from(Instant.parse("2018-05-23T04:35:59.500Z")),
        Timestamp.from(Instant.parse("2018-05-23T05:11:29.500Z"))
    );
    
    demo.timeseriesDataService.getDataFilteredByTimestamps(var, stamps)
        .print(LoggingTimeZone.UTC_TIME);
    ```

```markdown
Variable: ZT10.QFO03.ACQUISITION:CURRENT
	Timestamp(UTC_TIME): "2018-05-23 00:05:53.500" Value: 298.55
	Timestamp(UTC_TIME): "2018-05-23 01:15:53.500" Value: 298.58
	Timestamp(UTC_TIME): "2018-05-23 02:21:19.900" Value: 298.6
```
**NXCALS equivalent**

Getting initial data within specified time window which we would like to filter using 3 timestamps:

=== "Python"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/examples/NxcalsDemo.py.3' %}
    ```
=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/NxcalsDemo.java.4' %}
    ```

```markdown
+-------------------+---------+
|         cycleStamp|  current|
+-------------------+---------+
|1527033953500000000|298.55392|
|1527034067500000000|298.55743|
|1527034183900000000|298.59647|
|1527034625500000000|298.55054|
|1527034773100000000|      0.0|
|1527034803100000000|298.57617|
|1527034879900000000|298.61642|
|1527035437900000000| 298.5551|
|1527035665900000000|298.54538|
|1527035813500000000|298.59042|
|1527036504700000000|298.55646|
|1527037625500000000|298.59042|
|1527037686700000000|298.57977|
|1527038153500000000|298.57965|
|1527038187100000000|298.57898|
|1527038679100000000|298.56552|
|1527039022300000000| 298.5598|
|1527039244300000000|298.57782|
|1527039289900000000| 298.5763|
|1527039459100000000|298.57492|
+-------------------+---------+
only showing top 20 rows
```
Creation of dataframe containing 3 timestamps:

=== "Python"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/examples/NxcalsDemo.py.4' %}
    ```
=== "Java"
    ```java
    
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/NxcalsDemo.java.5' %}
    
    ...
    
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/NxcalsDemo.java.6' %}
    ```

Final result:

=== "Python"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/examples/NxcalsDemo.py.5' %}
    ```
=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/NxcalsDemo.java.7' %}  
    ```

```markdown
+-------------------+---------+
|         cycleStamp|  current|
+-------------------+---------+
|1527038153500000000|298.57965|
|1527033953500000000|298.55392|
|1527042079900000000|298.60202|
+-------------------+---------+
```
 
In order to verify correctness of timestamps we can use Numpy library again:

=== "Python"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/examples/NxcalsDemo.py.6' %}
    ```
=== "Java"
    ``` java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/NxcalsDemo.java.8' %}
    ```

```markdown                     
2018-05-23T01:15:53.500000000
```

# Getting data distribution for a variable within a time window

**CALS implementation**

=== "Java"
    ```java
    Variable var = demo.metaDataService
            .getVariablesWithNameInListofStrings(Arrays.asList("ZT10.QFO03.ACQUISITION:CURRENT"))
            .getVariable(0);
    Timestamp startTime = Timestamp.from(Instant.parse("2018-05-21T00:00:00.000Z"));
    Timestamp endTime = Timestamp.from(Instant.parse("2018-05-23T13:30:00.000Z"));
    
    DataDistributionBinSet dataDistributionBinSet = demo.timeseriesDataService
        .getDataDistributionInTimewindowForBinsNumber(var,startTime, endTime,10);
    
    for(int bucketNr=0; bucketNr<10; bucketNr++) {
        System.out.println(dataDistributionBinSet.get(bucketNr));
    }
    ```

```markdown
DataDistributionBinImpl [bottomLimit=-0.13, topLimit=30.129, valuesCount=615]
DataDistributionBinImpl [bottomLimit=30.129, topLimit=60.388, valuesCount=4]
DataDistributionBinImpl [bottomLimit=60.388, topLimit=90.647, valuesCount=0]
DataDistributionBinImpl [bottomLimit=90.647, topLimit=120.906, valuesCount=1]
DataDistributionBinImpl [bottomLimit=120.906, topLimit=151.165, valuesCount=28]
DataDistributionBinImpl [bottomLimit=151.165, topLimit=181.424, valuesCount=42]
DataDistributionBinImpl [bottomLimit=181.424, topLimit=211.683, valuesCount=32]
DataDistributionBinImpl [bottomLimit=211.683, topLimit=241.942, valuesCount=11]
DataDistributionBinImpl [bottomLimit=241.942, topLimit=272.201, valuesCount=0]
DataDistributionBinImpl [bottomLimit=272.201, topLimit=302.46, valuesCount=139055]
```

**NXCALS equivalent**

Importing necessary libraries including "functions" module for aggregates:

=== "Python"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/examples/NxcalsDemo.py.7' %}n
    ```

Retrieving sample data:

=== "Python"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/examples/NxcalsDemo.py.8' %}
    ```
=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/NxcalsDemo.java.9' %}
    ```

Retrieving min / max values:

=== "Python"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/examples/NxcalsDemo.py.9' %}
    ```
=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/NxcalsDemo.java.10' %}
    ```

```markdown
Row(minval=-0.12526702880859375, maxval=302.4573974609375)
```

Creating 10 buckets for values range:

=== "Python"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/examples/NxcalsDemo.py.10' %}
    ```
=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/NxcalsDemo.java.11' %}
    ```

```markdown
30.25826644897461
[-inf, -0.12526702880859375, 30.132999420166016, 60.391265869140625, 90.64953231811523, 120.90779876708984, 151.16606521606445, 181.42433166503906, 211.68259811401367, 241.94086456298828, 302.4573974609375, inf]
```
     
Verifying results:

=== "Python"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/examples/NxcalsDemo.py.11' %}
    ```
=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/NxcalsDemo.java.12' %}
    ```

```markdown
+----------------+------+
|bucketedFeatures| count|
+----------------+------+
|             1.0|   615|
|             2.0|     4|
|             4.0|     1|
|             5.0|    28|
|             6.0|    42|
|             7.0|    32|
|             8.0|    11|
|             9.0|139054|
|            10.0|     1|
+----------------+------+
```

# Getting data for a vector numeric variable within a time window filtered by indices

**CALS implementation**

=== "Java"
    ```java
    var = demo.metaDataService
            .getVariablesWithNameInListofStrings(Arrays.asList("SPS.BCTDC.51895:TOTAL_INTENSITY"))
            .getVariable(0);
    
    Timestamp startTime = Timestamp.from(Instant.parse("2018-06-21T00:15:00.000Z"));
    Timestamp endTime = Timestamp.from(Instant.parse("2018-06-21T00:20:00.000Z"));
    
    int[] indices = new int[] { 2, 4 };
    
    demo.timeseriesDataService
            .getVectornumericDataInTimeWindowFilteredByVectorIndices(
                    var, startTime, endTime, indices
            ).stream().sorted(Comparator.comparing(TimeseriesData::getStamp))
            .limit(5).forEach(Demo::printValues);
    
    ...
    
    private static void printValues(TimeseriesData data) {
        try {
            double[] values = data.getDoubleValues();
            System.out.printf("%s -> [%f, %f]\n", data.getFormattedStamp(LoggingTimeZone.UTC_TIME), values[0],
                    values[1]);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }
    ```

```markdown
2018-06-21 00:15:18.135 -> [0.216857, 0.182584]
2018-06-21 00:15:21.735 -> [540.232060, 836.190700]
2018-06-21 00:15:48.135 -> [0.219973, 0.189438]
2018-06-21 00:15:51.735 -> [531.002600, 823.269400]
2018-06-21 00:16:18.135 -> [0.292881, 0.284157]
```

**NXCALS equivalent**

Getting some vectornumeric data within a time window:

=== "Python"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/examples/NxcalsDemo.py.12' %}
    ```
=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/NxcalsDemo.java.13' %}
    ```

Getting rows with timestamp, value pairs:

=== "Python"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/examples/NxcalsDemo.py.13' %}
    ```
=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/NxcalsDemo.java.14' %}
    ```

Extracting columns 2 and 4 from each vector and returning it as a dataframe:

=== "Python"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/examples/NxcalsDemo.py.14' %}
    ```

As result from the above operation the following elements:

=== "Python"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/examples/NxcalsDemo.py.15' %}
    ```
=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/NxcalsDemo.java.16' %}
    ``` 

got converted to:

=== "Python"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/examples/NxcalsDemo.py.16' %}
    ```
=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/NxcalsDemo.java.17' %}
    ```

```markdown
[Row(nxcals_timestamp=1526860806135000000, nxcals_value=Row(dimensions=[2], elements=[1040.4697265625, 1572.702880859375])),
 Row(nxcals_timestamp=1526860816935000000, nxcals_value=None),
 Row(nxcals_timestamp=1526860824135000000, nxcals_value=None),
 Row(nxcals_timestamp=1526860834935000000, nxcals_value=Row(dimensions=[2], elements=[17.86440658569336, 33.05991744995117])),
 Row(nxcals_timestamp=1526860842135000000, nxcals_value=Row(dimensions=[2], elements=[1034.31689453125, 1561.6275634765625]))]
```

# Getting multi column timeseries data set

**CALS implementation**

=== "Java"
    ```java
    VariableSet vars = demo.metaDataService
        .getVariablesWithNameInListofStrings(
                Arrays.asList("SPS.BCTDC.41435:INT_FLATTOP", "SPS.BCTDC.51895:SBF_INTENSITY"));
    
    Timestamp startTime = Timestamp.from(Instant.parse("2018-05-21T00:00:00.000Z"));
    Timestamp endTime = Timestamp.from(Instant.parse("2018-05-21T00:24:00.000Z"));
    
    MultiColumnTimeseriesDataSet multiColumn = demo.timeseriesDataService
            .getMultiColumnDataInTimeWindow(vars, startTime, endTime);
    
    multiColumn.getTimestamps().stream().sorted(Comparator.reverseOrder()).limit(2)
            .forEach(t -> {
                System.out.println(multiColumn.getRowOfData(t));
            });
    ```

```markdown
[-9.17, 0.3198]
[7648.55, 2824.8354]
```

**NXCALS equivalent**

As an input we provide a list of variables (no restriction on datatype) and a time window.
For an output we expect to obtain a list of combined timestamps from all the variables with corresponding multiple values (if available for a given timestamp/variable).


Selection of 2 variables (textual and vectornumeric):

=== "Python"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/examples/NxcalsDemo.py.17' %}
    ``` 
=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/NxcalsDemo.java.18' %}
    ```
    
Creation of combined and unique timestamps:

=== "Python"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/examples/NxcalsDemo.py.18' %}
    ```
=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/NxcalsDemo.java.19' %}
    ```

Rename of identical column names (for the final selection). Since textual signals can be "nullable" in order to distinguish them from non-existent values for a given timestamp they are replaced with "N/A" literal:

=== "Python"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/examples/NxcalsDemo.py.19' %}
    ```
=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/NxcalsDemo.java.20' %}
    ```

Final join with timestamps:

=== "Python"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/examples/NxcalsDemo.py.20' %}
    ```
=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/examples/NxcalsDemo.java.21' %}
    
    ```

```markdown
+-------------------+----------+---------+
|   nxcals_timestamp|    value1|   value2|
+-------------------+----------+---------+
|1526775118935000000|-6.5800004|0.3047213|
|1526775108135000000|  7648.546|2883.2888|
+-------------------+----------+---------+

only showing top 2 rows
```