The [Controls Configuration Data Editor (CCDE)](https://ccde.cern.ch/dashboard) is a web-based application to interact with CCDB data.

Currently in order to **manage CMW subscriptions** it is enough to **self-subscribe (with approval) to the metadata related e-groups** 
as mentioned on the page [Getting access](../nxcals-access-request.md). 

You can still use the old panel available here [https://ccde.cern.ch/dashboard/loggingAccessRequest](https://ccde.cern.ch/dashboard/loggingAccessRequest) to request editor access to subscriptions from chosen accelerator (deprecated). 

After acceptance access to [search screen](https://ccde.cern.ch/nxcals/search) and subscription editor will be enabled.

!!! Important
    Depending on the environment, you should use the appropriate address for CCDE editor:
    
    - [https://ccde.cern.ch/dashboard](https://ccde.cern.ch/dashboard) for **PRO**
    - [https://ccde-testbed.cern.ch/dashboard](https://ccde-testbed.cern.ch/dashboard) for **TESTBED**

From that moment individual subscriptions for CMW data can be handled as described in [CMW subscriptions registration](../../logging-subscription/cmw-subscription-registration.md)
