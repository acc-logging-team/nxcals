## Introduction

There are several possibilities for users to access NXCALS data.

In this guide we will explain and give examples how to extract data using different methods:

- [Introduction](#introduction)
- [Java API](#java-api)
- [Python](#python)

!!!note
    Authorization is required for any of those methods in the form of a Kerberos token. [A Kerberos keytab file must be generated](../authentication/#kerberos-keytab-file-generation) and optionally the client machine must provide [a valid CERN grid CA certificate](../authentication/#cern-grid-ca-certificate-installation-optional).
    Moreover, the user must be  [registered on the NXCALS service](../../data-access/nxcals-access-request) to access the data.

!!!warning "Supported Java and Python versions"
    Please note that NXCALS requires a [specific version of Java and Python runtimes](../../supported-software-versions/).

!!!warning "Deprecated Access Methods"

    * python builders were moved from `cern.nxcals.pyquery.builders` to `nxcals.api.extraction.data.builders`
    * java builders were moved from `cern.nxcals.data.access.builders` to `cern.nxcals.api.extraction.data.builders`
    * `DevicePropertyQuery` has been renamed to `DevicePropertyDataQuery`
    * `KeyValuesQuery` and `VariableQuery` were unified into 'DataQuery', accessible via `byEntities()` and `byVariables()` respectively

## Java API
A Java examples project can be cloned from GitLab by following steps described [here](../../examples-project/examples-project).

## Python

To set up you python environment, please check [Setting up Python environments](../python-environments.md) page.
Currently, NXCALS provides a Python API only for [**Data Extraction API**](../../extraction-api/). There is a possibility of accessing from Python other NXCALS Java APIs such as: 

- [CERN Extraction API](../../cern-extraction-api/)
- [Metadata API](../../extraction-api/)
- [Ingestion API](../../ingestion-api/)
- [Backport API](../../backport-api/)

by [**using Py4J or JPype**](py4j-jpype-installaton.md).