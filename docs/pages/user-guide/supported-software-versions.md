Please take note of the following requirements:

## JDK (Java) versions

### JDK 11 (Java 11)
The **current version** supported by NXCALS.
Please update your configurations to use /var/nxcals/jdk1.11 for Spark executors running on YARN.

### JDK 8 (Java 1.8)
The **obsolete version** used by NXCALS.

## Python versions

### Python {% include 'generated/python_version.txt' %}
The **current version** supported by NXCALS.

```note
    This version is the one provided by acc-py. 
```

## Spark version

### Spark 3.5.3
The **current version** supported by NXCALS.
Please update your configurations to use jars from this version (property spark.yarn.jars) for Spark executors running on YARN.

### Spark 3.5.1
The **obsolete version** used by NXCALS.

### Spark 3.5.0
The **obsolete version** used by NXCALS.

### Spark 3.3.1
The **obsolete version** used by NXCALS.

### Spark 3.2.1
The **obsolete version** used by NXCALS.

### Spark 3.1.1
The **obsolete version** used by NXCALS.

### Spark 2.4.0
The **obsolete version** used by NXCALS.
