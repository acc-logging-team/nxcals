## Introduction

You can create Spark Session either by using methods provided by Spark directly or utility classes from NXCALS.
In either case you will have to provide some details (properties) in order to correctly configure the session.
You will have to chose if the session is run locally (on your computer, **Local Mode**) or on YARN (Hadoop cluster, **YARN Mode**).
In the later case you will have to select the number of executors, number of cores and memory sizes.
If you chose to use our utilities some helper classes are provided to make the process a little bit easier.
For YARN we provide so-called **Flavor** that helps selecting the desired size of the resources.
Those properties can also be overwritten if needed to match any specific case of yours.

## Creating Spark Session with NXCALS utilities in **Local Mode**

=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/sparksession/SparkSessionDemo.java.3' %}
    ```
=== "Python"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/sparksession/SparkSessionDemo.py.3' %}
    ```

## Creating Spark Session with NXCALS utilities in **YARN Mode** using predefined Flavor.

Predefined application size on YARN can be selected using the *Flavor* class.
Please note that any Spark property can be overwritten here according to the needs.

=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/sparksession/SparkSessionDemo.java.4' %}
    ```
=== "Python"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/sparksession/SparkSessionDemo.py.4' %}
    ```

There are three different configurations (Flavors) available (for application resources on the Hadoop cluster):

|Configuration type| Spark executor cores | Spark executor instances | Spark executor memory |
|:-----------------|:---------------------|:-------------------------|:----------------------|
|SMALL             | 2                    | 4                        | 6g                    |
|MEDIUM            | 4                    | 8                        | 6g                    |
|LARGE             | 8                    | 8                        | 8g                    |


Additionaly the flavors receive some predefined Spark properties as follows:

```properties
spark.sql.parquet.enableNestedColumnVectorizedReader = "true"
spark.task.maxDirectResultSize: "2047m"
spark.driver.maxResultSize: "1000g"
```

For more detailed information on tuning Spark sessions, please consult a [dedicated page](spark-tuning.md). 

## Creating Spark Session using Spark API in **Local Mode**

=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/sparksession/SparkSessionDemo.java.1' %}
    ```
=== "Python"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/sparksession/SparkSessionDemo.py.1' %}
    ```

## Creating Spark Session using Spark API in **YARN Mode**

Please note that in this case you have to specify all the required properties.

=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/sparksession/SparkSessionDemo.java.2' %}
    ```
=== "Python"
    ```python
    {% include 'generated/src/main/python/cern/nxcals/docs/sparksession/SparkSessionDemo.py.2' %}
    ```

!!!note
    More information about used properties for YARN can be found in [Spark Apache documentation pages](https://spark.apache.org/docs/{% include 'generated/spark_version.txt' %}/running-on-yarn.html)


## Modes and their names

Possible master values (the location to run the application):

- `local`: Run Spark locally with one worker thread (that is, no parallelism).
- `local[K]`: Run Spark locally with K worker threads. (Ideally, set this to the number of cores on your host.)
- `local[*]`: Run Spark locally with as many worker threads as logical cores on your host.
- `yarn`: Run using a YARN cluster manager.

## Using spark session suppliers

When you are implementing long-running service, then you have to handle life-cycle of `SparkSession`, which might be closed.
To handle this problem, services from `cern-extraction` package can accept `Supplier<SparkSession>`. Such supplier might be conveniently 
created with helper methods from `SparkUtils` class. Here is an example:

```java
Supplier<SparkSession> supplier = SparkUtils.createSparkSessionSupplier(
        SparkProperties.remoteSessionProperties("NXCALS-Aggregation", SparkSessionFlavor.MEDIUM, "pro"));

AggregationService aggregationService = Services.newInstance(supplier).aggregationService();
```

Supplier created in this way, will take care of recreating `SparkSession` when it is needed. Apart these services, it can be used with normal extraction calls:

```java
Dataset<Row> ds = DataQuery.builder(supplier.get()).variables().system("CMW")
        .nameEq("CPS:NXCALS_FUNDAMENTAL")
        .timeWindow("2023-06-11 00:00:00", "2023-06-11 02:00:00")
        .build();
```