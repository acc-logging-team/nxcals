There exists a number of already running datasources (as separate projects) that are configured to transfer the data from particular sources into NXCALS.
We are maintaining two separate projects that cover CMW & WinccOA domains. In order to use any of those datasources one has to *register* the signals inside its configuration
using existing tools (like CCDE) or contacting the appropriate support.

-  CMW

This datasource covers the existing device/property CSS Common Middleware model. The configuration of the subscriptions can be done via CCDE.
Please get [access rights](../data-access/CMW/ccde-access-request.md) in order to add devices & properties to be monitored and their data sent to NXCALS.
Configuratino application is available using the following link to [CCDE NXCALS Configuration](https://ccde.cern.ch/nxcals/).

-  WinccOA

Please contact Industrial Controls Support (BE-ICS).

-  Remus

Please contact Remus support.

-  LSA (Settings)

Please contact LSA Support.

-  Tim

Please contact Tim Support.

-  Post Mortem

Please contact Post Mortem system support (TE-MPE).
