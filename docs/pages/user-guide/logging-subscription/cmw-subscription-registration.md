
!!!important
    Steps which are required for CMW data logging in NXCALS are described in
    [CCDE documentation](https://confluence.cern.ch/display/config/NXCALS+and+Device+data+Logging).

After creation or modification of a subscription its state will be updated accordingly as it can be seen on a sample screen below:

 ![Screenshot](img/ccde_editor.png)
 
Please note the the validation process runs periodically and depending on the current load and amount of data in the "validation waiting room" 
it may take several minutes before it is triggered for a given subscription.

A subscription can have 3 possible **statuses**: <span style="color:blue">NEW</span>, <span style="color:green">VALIDATION_OK</span> and <span style="color:red">VALIDATION_IN_ERROR</span> (in that case accompanied by an error message).

Additional information concerning subscription state:

- **Status Time**: when the given status was set
- **In status since**: time duration for the current status
- **Last Check Time**: indicating when the subscription was validated for the last time

!!!note
    There is a possibility of switching off logging (but keeping subscription configuration), using a toggle button **Enabled**. Re-enabling subscription will launch validation process.
