#Backport API (CALS API via NXCALS) 

We have implemented an indirection layer that enables using NXCALS via the old CALS API. We refer to the project as **backport-api**.

!!! warning "Deprecated API"
    Please note that some methods, public APIs or classes do not conform easily to the new system. We have marked a number of methods with @Deprecated to signal this situation. 
    
    While me might not remove these methods, you use them at your own risk. You might run into a series of issues, be it very low performance, unpredictable results or even inability to run at all. 
    
    Please avoid deprecated methods if possible and treat **very** seriously every warning about using a deprecated method in your project. Notify us if you consider a deprecated method to be absolutely essential to your workflow.


##Dependencies
   
In order to start working with the **backport-api** first you have to import the required jars:

=== "Gradle"
    ```groovy
    
    dependencies {
        (...)   
        compile group: 'cern.nxcals', name: 'nxcals-backport-api', version: nxcalsVersion
    
        compile group: 'cern.nxcals', name: 'nxcals-hadoop-pro-config', version: nxcalsVersion
        (...)
    }
    ```   
=== "CBNG"
    ```xml
    <?xml version="1.0" encoding="UTF-8"?>
    
    <!-- !!! IMPORTANT: for CBNG configuration remove build.gradle files !!! -->
    <products>
            (...)
            <dependencies>
                (...)
                <dep product="nxcals-backport-api" />
                
                <dep product="nxcals-hadoop-pro-config" />
                (...)
            </dependencies>
        </product>
    </products>
    ```

!!! hint "Packages"
    We have renamed the java packages for all the public CALS classes:
    
    - cern.accsoft.cals.extr.client.service.* -> cern.nxcals.api.backport.client.service.*
    - cern.accsoft.cals.extr.domain.* -> cern.nxcals.api.backport.client.service.*
    
    This allows you to effortlessly use both, the old api and the new api in a single project.



##Configuration

From this point on all you need to configure is the service endpoint for NXCALS. This is done via a system property.

=== "NXCALS PRO"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/CodeSnippets.java.2' %}
    ``` 
=== "TESTBED"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/CodeSnippets.java.1' %}
    ```

##API differences

We have aimed at maintaining maximum API compatibility with old CALS API. As the systems differ deeply,
there is a number of small and unavoidable changes in the Backport project, both in the syntax and logic.
However, for the sake of this document the main concern is that we have replaced the old ServiceBuilder method:

=== "Java"
    ```java 
    public static ServiceBuilder newInstance(String app, String client, DataLocationPreferences prefs)
    ```

with:  

=== "Java"
    ```java 
    public static ServiceBuilder getInstance() 
    ```

The old method lost its meaning, as there is no DataLocationPreference anymore, and the authentication is not performed via app and client name.
The new method assumes reasonable defaults with respect to the Spark connection and abstracts the users completely from the spark layer.
It is sufficient for most simple use-cases.


## Getting ServiceBuilder instance

Instance of the ServiceBuilder is used to create particular services (as per CALS).
Please note that the Backport API have to use a Spark Session in order to perform queries for data.
This session is either created implicitly or can be provided as a parameter to the *getInstance()* method.

### Implicit Spark Session creation

When used without any parameters this method will internally create a Spark Session that will be run locally (not on the Hadoop cluster).
This is usually suitable for small data queries and will save some time when starting up the application.

=== "Java"
    ```java
    ServiceBuilder.getInstance();
    ```

### Explicit Spark Session creation

One can pass SparkProperties to the getInstance() method in order to create a desired SparkSession.

=== "Java"
    ```java
    SparkProperties props = ...; //create SparkProperties
    ServiceBuilder.getInstance(props);
    ```

There is also a method that accepts Spark Session created outside:

=== "Java"
    ```java
    SparkSession session = ...; //create Spark Session
    ServiceBuilder.getInstance(session);
    ```

In order to create Spark Session please consult the [following documentation](./spark-session.md).

##Putting it all together
At this point your project should look something like this:

=== "Java"
    ```java
    {% include 'generated/src/main/java/cern/nxcals/docs/BackportApi.java.1' %}
    ```

??? note "Click to see expected application output..."
    ```
    
    Number of values for variable: LTB.BCT60:INTENSITY size: 72000
    Values for parameter: 
    Name: japc-parameter-value
    Type: ImmutableData
    Value: 
    [Name: __record_timestamp__
    Type: Long
    Value: 1503964806659000000
    
    Name: __record_version__
    Type: Long
    Value: 0
    
    Name: acqStamp
    Type: Long
    Value: 1503964806659000000
    
    Name: class
    Type: String
    Value: "RadmonV6"
    
    Name: cyclestamp
    Type: Long
    Value: 0
    
    Name: device
    Type: String
    Value: "RADMON.PS-10"
    
    Name: nxcals_entity_id
    Type: Long
    Value: 55461
    
    Name: property
    Type: String
    Value: "ExpertMonitoringAcquisition"
    
    Name: pt100Value
    Type: Double
    Value: 106.1]
    ```
    
For many more examples please consult the [examples project](https://gitlab.cern.ch/acc-logging-team/nxcals-examples),
where you will find a fully configured and runnable classes with all the aspects of the backport api.
All you need to do is to checkout the project and navigate to the module ```backport-api-examples``` and explore.