# Welcome to NXCALS documentation
*(version: {% include 'generated/nxcals_version.txt' %})*

NXCALS is the new logging system based on Hadoop Big Data technologies using cluster computing power for the Data Analysis.

### Quickstart

New to NXCALS? Check [quickstart page](./user-guide/data-access/quickstart.md).

### NXCALS model

NXCALS accepts data from different data sources (***[systems](./#system)***) identified by their unique system identifiers.
Each ***[system](./#system)*** keeps the time-based data for its ***[entities](#entity)*** in NXCALS system i.e. each ***[entity](#entity)***
has a state modelled by a set of properties with their values at a given moment in time. This information forms a ***[record](#record)***
which is specific for each ***[system](#system)*** and consist of:

* Fields identifying the ***[entity](#entity)***
* Fields identifying the ***[partition](#partition)*** (a classifier used to physically put records in a disk folder and file)
* Fields identifying the ***[timestamp](#timestamp)***
* Fields identifying the ***[record version](#record_version)*** (optional)

Given set of all the ***[record's](#record)*** fields is known in NXCALS as ***[schema](#schema)***.
 
The ***[entity](#entity)*** record field set can also change in time. The state of the ***[entity](#entity)*** at a given moment is represented by the history of ***[schema](#schema)*** and ***[partition](#partition)*** changes, in short: ***[entity history](#entity_history)***.

***[Records](#record)*** are physically split (grouped in different files on disk) by: ***[system](#system)***, ***[partition](#partition)*** (classifier), ***[schema](#schema)***
and date (which is split by day and in case of big files also by hour).
It translates to the following location:

![Screenshot](img/data_partitioning.png)

On top of the raw data there are high level ***[variables](#variable)*** defined that point to the particular field of the particular ***[record](#record)*** type.
The mapping between the high level ***[variables](#variable)*** and the raw ***[record](#record)*** fields can change over time and the system remembers the history of the mapping changes.

The system allows to extract one or more fields from the stored ***[records](#record)*** as raw data. It also allows to extract the high level ***[variables](#variable)*** data in the form of a record containing only one field.

### System architecture overview

A simplified system architecture is presented in the diagram below. It shows flow of data from the stage when it is being acquired
until the moment when it is made available to users.

![Screenshot](img/NXCALS-architecture.png)

1. Acquired data from different data sources is sent to NXCALS ingestion API. 
1. Then it is temporarily stored in Kafka which acts as intermediate persistent storage buffer for further processing.
The current retention time is {% include 'user-guide/variables/retention-time.txt' %}. In case of processing failures (infrastructure problems, bugs, etc.) this time period allows us to fix things and the data is not lost.
1. From there it is collected by ETL process and being:
    * sent to HBase and nearly immediately made available to users. Lifetime of recent data stored at this location is {% include 'user-guide/variables/extraction-time.txt' %}.
    * compacted, deduplicated and stored in HDFS (this process might be time consuming)
1. At this stage, it can be acquired by NXCALS clients through [Data Extraction API](user-guide/extraction-api/) which "hides" origin of data (HBase or HDFS).
Combined with Apache Spark it provides means of data extraction and analytics.
It is made available in [different interfaces](user-guide/data-access/access-methods/) including Web (through [SWAN notebook](user-guide/data-access/access-methods/#swan-notebook)), in [NXCALS bundle](user-guide/data-access/access-methods/#nxcals-spark-bundle) (through Python API) or directly from [Java API](user-guide/data-access/access-methods/#java-api).

!!!Note ""
    NXCALS meta-data (available through Meta-data service) is stored in the Oracle database.

### NXCALS Data access

There are several possibilities for users to access NXCALS data using it's **Public APIs**: [Extraction API](user-guide/extraction-api/), [Backport API](user-guide/backport-api), [Metadata API](user-guide/metadata-api), [CERN Extraction API](user-guide/cern-extraction-api) and [Extraction Thin API](user-guide/extraction-api-thin).

In this site we will explain and give examples of the functionality and the usage of those APIs. As we have built Data Access to serve as a Spark custom format, we can benefit using all the Spark APIs and supported languages.

Examples presented in the documentation feature various query builders that data-access API provides such as CMW specific query builders, as well as a dictionary-like ones, in order to achieve a more generic, system-agnostic approach.

In case of **Extraction API**  data can be accessed through Java, Python 3 and Scala clients. It is easy to notice that the API in all cases is almost identical, in terms of functionality and syntax as explained on our [Extraction API page](user-guide/extraction-api/).


!!!note
    The easiest way to get started with accessing NXCALS data is to follow steps described in our [Quickstart](user-guide/data-access/quickstart/) guide.
   
### Dictionary
<a name="entity"></a>Entity
:  Entity is a representation of an object for which we store state changes over time. State is represented with a set of fields.

<a name="entity_key_defs"></a>Entity key definitions
: Set of field definitions uniquely identifying an entity. In case of CMW system:
```json
{"name": "device", "type": "string"}
{"name": "property",  "type": "string"}
```

<a name="entity_history"></a>Entity History
: History of entity evolution, reflecting changes in its schema and partition.

<a name="partition"></a>Partition
: A classifier allowing dividing data into some parts based on the values of the selected columns (partition keys) specified in the system definition.

<a name="partition_key_defs"></a>Partition key definitions
: Set of field definitions within a record uniquely identifying a partition. In case of CMW system:
```json
{"name": "class", "type": "string"}
{"name": "property", "type": "string"}
```

<a name="record"></a>Record
: Set of grouped fields (keys definitions) of which some are required by system.

<a name="record_version"></a>Record version
: Records with the same version and timestamp are deduplicated in NXCALS. The version is there to allow clients to add data having the same timestamp twice with different values.

<a name="record_version_key_defs"></a>Record version key definitions
: Optional set of field definitions within a record uniquely identifying a record version. In case of CMW system:
```json
{"name": "__record_version__", "type": "long"}
```

<a name="schema"></a>Schema
: Set of all the record’s fields with their types. In NXCALS schema is dynamically discovered based on data.

<a name="system"></a>System
: A data source identified by its unique id. It is described by entity, partition, time and record version (optional) key definitions.

<a name="time_key_defs"></a>Time key definitions
: Currently in NXCALS implemented as a single field of a type long uniquely identifying a timestamp. In case of CMW system:
```json
{"name": "__record_timestamp__", "type": "long"}
```

<a name="timestamp"></a>Timestamp
: Data and time having a nanosecond precision in NXCALS

<a name="variable"></a>Variable
: A pointer to a field or to the whole record (if no field is given in the config) characterized by name, description and unit.


<a name="variable_config"></a>Variable config
: Mapping between entity and variable for a given time period.