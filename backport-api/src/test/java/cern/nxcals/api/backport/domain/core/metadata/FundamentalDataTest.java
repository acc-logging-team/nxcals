package cern.nxcals.api.backport.domain.core.metadata;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class FundamentalDataTest {

    @Test
    public void shouldParsePatternWithThreeElements() {
        FundamentalData fundamentalData = FundamentalData.from("A:B:C");

        assertEquals("A", fundamentalData.getAccelerator());
        assertEquals("B", fundamentalData.getLsaCycle());
        assertEquals("C", fundamentalData.getTimingUser());

    }

    @Test
    public void shouldFailOnWrongPatternWithOneSeparatorOnly() {
        assertThrows(IllegalArgumentException.class, () -> FundamentalData.from("AB:C"));
    }

    @Test
    public void shouldFailOnWrongPatternWithNoSeparator() {
        assertThrows(IllegalArgumentException.class, () -> FundamentalData.from("%"));
    }

    @Test
    public void shouldBuildCorrectRealName() {
        FundamentalData fundamentalData = FundamentalData.from("A:B:C");
        assertEquals("A:NXCALS_FUNDAMENTAL", fundamentalData.getRealFundamentalName());
    }

}
