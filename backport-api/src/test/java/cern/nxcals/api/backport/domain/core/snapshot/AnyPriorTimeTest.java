package cern.nxcals.api.backport.domain.core.snapshot;

import org.joda.time.DateTime;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class AnyPriorTimeTest {

    @Test
    void shouldWorkWithConstructor() {
        //given
        DateTime time = DateTime.now();
        String desc = "My desc";
        //when
        PriorTime priorTime = new AnyPriorTime(time, desc);

        //then
        assertEquals(time, priorTime.time());
        assertEquals(desc, priorTime.description());
    }

    @Test
    void shouldThrowForNullAsTime() {
        assertThrows(NullPointerException.class, () -> new AnyPriorTime(null, "desc"));
    }

    @Test
    void shouldThrowForNullAsDescription() {
        assertThrows(NullPointerException.class, () -> new AnyPriorTime(DateTime.now(), null));
    }
}