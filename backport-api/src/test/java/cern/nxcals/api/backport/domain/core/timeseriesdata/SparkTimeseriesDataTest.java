package cern.nxcals.api.backport.domain.core.timeseriesdata;

import cern.cmw.datax.EntryType;
import cern.nxcals.api.utils.TimeUtils;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema;
import org.apache.spark.sql.types.ArrayType;
import org.apache.spark.sql.types.BooleanType;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.DoubleType;
import org.apache.spark.sql.types.FloatType;
import org.apache.spark.sql.types.IntegerType;
import org.apache.spark.sql.types.LongType;
import org.apache.spark.sql.types.ShortType;
import org.apache.spark.sql.types.StringType;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.sql.types.VarcharType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Collection;

import static cern.nxcals.api.backport.domain.core.constants.VariableDataType.MATRIX_NUMERIC;
import static cern.nxcals.api.backport.domain.core.constants.VariableDataType.NUMERIC;
import static cern.nxcals.api.backport.domain.core.constants.VariableDataType.UNDETERMINED;
import static cern.nxcals.api.backport.domain.core.constants.VariableDataType.VECTOR_STRING;
import static cern.nxcals.common.SystemFields.NXC_EXTR_TIMESTAMP;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VALUE;
import static cern.nxcals.common.avro.SchemaConstants.ARRAY_DIMENSIONS_FIELD_NAME;
import static cern.nxcals.common.avro.SchemaConstants.ARRAY_ELEMENTS_FIELD_NAME;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SparkTimeseriesDataTest {

    public static Collection<Object[]> getDoubleIncompatibleData() {
        //@formatter:off
        Object[][] data = new Object[][] {
            { SparkTimeseriesData.of(mockRow(1000000000, 1L, new LongType()))},
            { SparkTimeseriesData.of(mockRow(1000000000, new String[] {"True"}, new ArrayType()))}
        };
        //@formatter:on
        return Arrays.asList(data);
    }

    public static Collection<Object[]> getLongIncompatibleData() {
        //@formatter:off
        Object[][] data = new Object[][] {
            { SparkTimeseriesData.of(mockRow(1000000000, 1D, new DoubleType()))},
            { SparkTimeseriesData.of(mockRow(1000000000, new String[] {"True"}, new ArrayType()))}
        };
        //@formatter:on
        return Arrays.asList(data);
    }

    @Test
    public void getStamp() {
        int timestamp = 1000000000;
        Row row = mockRow(timestamp, 1.2d, new DoubleType());

        TimeseriesData data = SparkTimeseriesData.of(row);

        assertEquals(data.getStamp(), Timestamp.from(TimeUtils.getInstantFromNanos(timestamp)));
    }

    @Test
    public void shouldGetAdditionalDataInformation() {
        int timestamp = 1000000000;
        int[] dimensions1 = new int[] {};
        int[] dimensions2 = new int[] { 2, 3 };

        StructType arrayType = new StructType();
        arrayType = arrayType.add(ARRAY_ELEMENTS_FIELD_NAME, new ArrayType());
        arrayType = arrayType.add(ARRAY_DIMENSIONS_FIELD_NAME, new ArrayType());

        Row row1 = mockRow(timestamp, 1.2d, new DoubleType());

        Row array = new GenericRowWithSchema(
                new Object[] { new double[] { 1.2f, 1.3f, 1.4f, 1.5f, 1.6f, 1.7f }, dimensions2 }, arrayType);
        Row row2 = mockRow(1000000000, array, arrayType);

        TimeseriesData data1 = SparkTimeseriesData.of(row1);
        TimeseriesData data2 = SparkTimeseriesData.of(row2);

        assertEquals(data1.type(), Double.class);
        assertArrayEquals(dimensions1, data1.dimensions());

        assertEquals(data2.type(), double[].class);
        assertArrayEquals(dimensions2, data2.dimensions());
    }

    @Test
    public void shouldNotAllowAbsentValue() throws NoSuchMethodException {
        int timestamp = 1000000000;
        Row row = mockEmptyRow(timestamp);
        assertThrows(NullPointerException.class, () -> SparkTimeseriesData.of(row));
    }

    @Test
    public void shouldGetNumericValueIfCompatibleType() throws NoSuchMethodException {
        Row row = mockRow(1000000000, 1.2d, new DoubleType());

        TimeseriesData data = SparkTimeseriesData.of(row);

        assertEquals(new BigDecimal(1.2d).floatValue(), data.getNumericValue().floatValue(), 0.001d);
        assertFalse(data.isNullValue());
    }

    @Test
    public void shouldGetDoubleValueIfDouble() throws NoSuchMethodException {
        Row row = mockRow(1000000000, 1.2d, new DoubleType());

        TimeseriesData data = SparkTimeseriesData.of(row);

        assertEquals(1.2d, data.getDoubleValue(), 0.0001d);
        assertFalse(data.isNullValue());
    }

    @Test
    public void shouldGetDoubleValueIfCompatibleType() throws NoSuchMethodException {
        Row row = mockRow(1000000000, 1.2f, new FloatType());

        TimeseriesData data = SparkTimeseriesData.of(row);

        assertEquals(1.2d, data.getDoubleValue(), 0.0001d);
        assertFalse(data.isNullValue());
    }

    @Test
    public void shouldGetDoubleValueIfIncompatibleType() throws NoSuchMethodException {
        Row row = mockRow(1000000000, 2L, new LongType());

        TimeseriesData data = SparkTimeseriesData.of(row);

        assertEquals(2.0d, data.getDoubleValue(), 0.0001d);
        assertFalse(data.isNullValue());
    }

    @Test
    public void shouldGetLongIfLong() throws NoSuchMethodException {
        Row row = mockRow(1000000000, 1L, new LongType());

        TimeseriesData data = SparkTimeseriesData.of(row);

        assertEquals(1L, data.getLongValue());
        assertFalse(data.isNullValue());
    }

    @Test
    public void shouldGetLongValueIfCompatibleType() throws NoSuchMethodException {
        Row row = mockRow(1000000000, 1, new ShortType());

        TimeseriesData data = SparkTimeseriesData.of(row);

        assertEquals(1L, data.getLongValue());
        assertFalse(data.isNullValue());
    }   @Test

    public void shouldGetDoubleIfBoolean() throws NoSuchMethodException {
        Row row = mockRow(1000000000, false, new BooleanType());

        TimeseriesData data = SparkTimeseriesData.of(row);

        assertEquals(0d, data.getLongValue(), 0.1d);
        assertFalse(data.isNullValue());
    }

    @Test
    public void shouldGetLongIfBoolean() throws NoSuchMethodException {
        Row row = mockRow(1000000000, true, new BooleanType());

        TimeseriesData data = SparkTimeseriesData.of(row);

        assertEquals(1L, data.getLongValue());
        assertFalse(data.isNullValue());
    }

    @Test
    public void shouldFailGetLongIfIncompatibleType() throws NoSuchMethodException {
        Row row = mockRow(1000000000, 1.2d, new DoubleType());

        TimeseriesData data = SparkTimeseriesData.of(row);

        assertEquals(1L, data.getLongValue());
        assertFalse(data.isNullValue());
    }

    @Test
    public void shouldGetStringValueIfCompatibleType() throws NoSuchMethodException {
        Row row = mockRow(1000000000, "test", new VarcharType(4));

        TimeseriesData data = SparkTimeseriesData.of(row);

        assertEquals("test", data.getVarcharValue());
        assertFalse(data.isNullValue());
    }

    @Test
    public void shouldFailGetStringValueIfIncompatibleType() throws NoSuchMethodException {
        Row row = mockRow(1000000000, 1.2d, new DoubleType());

        TimeseriesData data = SparkTimeseriesData.of(row);

        assertThrows(NoSuchMethodException.class, () -> data.getVarcharValue());
    }

    @Test
    public void shouldGetVectorNumericValueIfDouble() throws NoSuchMethodException {
        Row row = mockRow(1000000000, new double[] { 1.2d }, new ArrayType());

        TimeseriesData data = SparkTimeseriesData.of(row);

        assertEquals(new BigDecimal(1.2d).doubleValue(), data.getVectorNumericValues()[0].doubleValue(),
                0.0001d);
        assertEquals(new BigDecimal(1.2d).doubleValue(), data.getNumericValueAtVectorIndex(0).doubleValue(),
                0.0001d);
        assertFalse(data.isNullValue());
    }

    @Test
    public void shouldGetVectorNumericValueIfFloat() throws NoSuchMethodException {
        Row row = mockRow(1000000000, new float[] { 1.2f }, new ArrayType());

        TimeseriesData data = SparkTimeseriesData.of(row);

        assertEquals(BigDecimal.valueOf(1.2f), data.getVectorNumericValues()[0]);
        assertEquals(BigDecimal.valueOf(1.2f), data.getNumericValueAtVectorIndex(0));
        assertFalse(data.isNullValue());
    }

    @Test
    public void shouldThrowIndexOutOfBounds() throws NoSuchMethodException {
        Row row = mockRow(1000000000, new float[] { 1.2f }, new ArrayType());

        TimeseriesData data = SparkTimeseriesData.of(row);

        assertThrows(IndexOutOfBoundsException.class, () -> data.isNullValue(2));
    }

    @Test
    public void shouldGetVectorNumericValueIfLong() throws NoSuchMethodException {
        Row row = mockRow(1000000000, new long[] { 1L }, new ArrayType());

        TimeseriesData data = SparkTimeseriesData.of(row);

        assertEquals(BigDecimal.valueOf(1.0d), data.getVectorNumericValues()[0]);
        assertEquals(BigDecimal.valueOf(1.0d), data.getNumericValueAtVectorIndex(0));
        assertFalse(data.isNullValue());
    }

    @Test
    public void shouldAlwaysAllowToString() throws NoSuchMethodException {
        assertNotNull(
                SparkTimeseriesData.of(mockRow(1000000000, new long[] { 1L }, new ArrayType())).toStringValue());
        assertNotNull(
                SparkTimeseriesData.of(mockRow(1000000000, new Long[] { 1L }, new ArrayType())).toStringValue());
        assertNotNull(
                SparkTimeseriesData.of(mockRow(1000000000, new double[] { 1.0d }, new ArrayType())).toStringValue());
        assertNotNull(
                SparkTimeseriesData.of(mockRow(1000000000, new Double[] { 1.0d }, new ArrayType())).toStringValue());
        assertNotNull(
                SparkTimeseriesData.of(mockRow(1000000000, new String[] { "text" }, new StringType())).toStringValue());
        assertNotNull(
                SparkTimeseriesData.of(mockRow(1000000000, new int[] { 1 }, new ArrayType())).toStringValue());
        assertNotNull(
                SparkTimeseriesData.of(mockRow(1000000000, new Integer[] { 1 }, new ArrayType())).toStringValue());
        assertNotNull(SparkTimeseriesData.of(mockRow(1000000000, 1L, new LongType())).toStringValue());
        assertNotNull(SparkTimeseriesData.of(mockRow(1000000000, "text", new StringType())).toStringValue());
        assertNotNull(SparkTimeseriesData.of(mockRow(1000000000, new Long(1L), new LongType())).toStringValue());
        assertNotNull(SparkTimeseriesData.of(mockRow(1000000000, (short) 1, new ShortType())).toStringValue());
        assertNotNull(
                SparkTimeseriesData.of(mockRow(1000000000, new Short((short) 1), new ShortType())).toStringValue());
        assertNotNull(SparkTimeseriesData.of(mockRow(1000000000, 1, new IntegerType())).toStringValue());
        assertNotNull(
                SparkTimeseriesData.of(mockRow(1000000000, new Integer(1), new IntegerType())).toStringValue());
        assertNotNull(SparkTimeseriesData.of(mockRow(1000000000, 1.0d, new DoubleType())).toStringValue());
        assertNotNull(
                SparkTimeseriesData.of(mockRow(1000000000, new Double(1.0d), new DoubleType())).toStringValue());
        assertNotNull(
                SparkTimeseriesData.of(mockRow(1000000000, (float) 1.0d, new FloatType())).toStringValue());
        assertNotNull(
                SparkTimeseriesData.of(mockRow(1000000000, new Float(1.0), new FloatType())).toStringValue());

        assertNotNull(SparkTimeseriesData.of(mockRow(1000000000, null, new StringType())).toStringValue());
        assertNotNull(SparkTimeseriesData.of(mockRow(1000000000, null, new LongType())).toStringValue());
        assertNotNull(SparkTimeseriesData.of(mockRow(1000000000, null, new StringType())).toStringValue());
        assertNotNull(SparkTimeseriesData.of(mockRow(1000000000, null, new LongType())).toStringValue());
        assertNotNull(SparkTimeseriesData.of(mockRow(1000000000, null, new ShortType())).toStringValue());
        assertNotNull(SparkTimeseriesData.of(mockRow(1000000000, null, new IntegerType())).toStringValue());
        assertNotNull(SparkTimeseriesData.of(mockRow(1000000000, null, new DoubleType())).toStringValue());
        assertNotNull(SparkTimeseriesData.of(mockRow(1000000000, null, new FloatType())).toStringValue());
    }

    @Test
    public void shouldGetVectorNumericValueIfInt() throws NoSuchMethodException {
        Row row = mockRow(1000000000, new int[] { 1 }, new ArrayType());

        TimeseriesData data = SparkTimeseriesData.of(row);

        assertEquals(BigDecimal.valueOf(1.0d), data.getVectorNumericValues()[0]);
        assertEquals(BigDecimal.valueOf(1.0d), data.getNumericValueAtVectorIndex(0));
        assertFalse(data.isNullValue());
    }

    @Test
    public void shouldFailGetVectorNumericValueIfIncompatibleType() throws NoSuchMethodException {
        Row row = mockRow(1000000000, 1.2d, new DoubleType());

        TimeseriesData data = SparkTimeseriesData.of(row);

        assertThrows(NoSuchMethodException.class, () -> data.getVectorNumericValues());
    }

    @Test
    public void shouldGetDoubleValueArrayIfCompatibleType() throws NoSuchMethodException {
        Row row = mockRow(1000000000, new double[] { 1.2d }, new ArrayType());

        TimeseriesData data = SparkTimeseriesData.of(row);

        assertArrayEquals(new double[] { 1.2d }, data.getDoubleValues(), 0.0001d);
        assertFalse(data.isNullValue());
    }

    @Test
    public void shouldGetDoubleValueArrayIfBoolean() throws NoSuchMethodException {
        Row row = mockRow(1000000000, new boolean[] { true }, new ArrayType());

        TimeseriesData data = SparkTimeseriesData.of(row);

        assertArrayEquals(new double[] { 1.0d }, data.getDoubleValues(), 0.0001d);
        assertFalse(data.isNullValue());
    }

    @ParameterizedTest
    @MethodSource("getDoubleIncompatibleData")
    public void shouldFailGetDoubleValueArrayIfIncompatibleType(TimeseriesData data) throws NoSuchMethodException {
        assertThrows(NoSuchMethodException.class, () -> data.getDoubleValues());
    }

    @Test
    public void shouldGetLongValueArrayIfCompatibleType() throws NoSuchMethodException {
        Row row = mockRow(1000000000, new long[] { 1L }, new ArrayType());

        TimeseriesData data = SparkTimeseriesData.of(row);

        assertArrayEquals(new long[] { 1L }, data.getLongValues());
        assertFalse(data.isNullValue());
    }

    @Test
    public void shouldGetLongValueArrayIfBoolean() throws NoSuchMethodException {
        Row row = mockRow(1000000000, new boolean[] { true }, new ArrayType());

        TimeseriesData data = SparkTimeseriesData.of(row);

        assertArrayEquals(new long[] { 1L }, data.getLongValues());
        assertFalse(data.isNullValue());
    }

    @ParameterizedTest
    @MethodSource("getLongIncompatibleData")
    public void shouldFailGetLongValueArrayIfIncompatibleType(TimeseriesData data) throws NoSuchMethodException {
        assertThrows(NoSuchMethodException.class, () -> data.getLongValues());
    }

    @Test
    public void shouldGetStringValueArrayIfCompatibleType() throws NoSuchMethodException {
        Row row = mockRow(1000000000, new String[] { "test" }, new ArrayType());

        TimeseriesData data = SparkTimeseriesData.of(row);
        data.getStringValueAtIndex(0);

        assertArrayEquals(new String[] { "test" }, data.getStringValues());
        assertFalse(data.isNullValue());
    }

    @Test
    public void shouldFailGetStringValueArrayIfIncompatibleType() throws NoSuchMethodException {
        Row row = mockRow(1000000000, 1.2d, new DoubleType());

        TimeseriesData data = SparkTimeseriesData.of(row);

        assertThrows(NoSuchMethodException.class, () -> data.getStringValues());
    }

    @Test
    public void shouldGetNumericValueAtIndexIfCompatibleType() throws NoSuchMethodException {
        Row row = mockRow(1000000000, new double[] { 1.2d }, new ArrayType());

        TimeseriesData data = SparkTimeseriesData.of(row);

        assertEquals(new BigDecimal(1.2d).floatValue(), data.getNumericValueAtVectorIndex(0).floatValue(),
                0.001d);
        assertFalse(data.isNullValue());
    }

    @Test
    public void shouldFailGetNumericValueAtIndexIfIncompatibleType() throws NoSuchMethodException {
        Row row = mockRow(1000000000, 1.2d, new DoubleType());

        TimeseriesData data = SparkTimeseriesData.of(row);

        assertThrows(NoSuchMethodException.class, () -> data.getNumericValueAtVectorIndex(0));
    }

    @Test
    public void shouldGetVectorDoubleValueAtIndexIfCompatibleType() throws NoSuchMethodException {
        Row row = mockRow(1000000000, new double[] { 1.2d }, new ArrayType());

        TimeseriesData data = SparkTimeseriesData.of(row);

        assertEquals(1.2d, data.getDoubleValueAtIndex(0), 0.0001d);
        assertFalse(data.isNullValue());
    }

    @Test
    public void shouldFailGetVectorDoubleValueAtIndexIfIncompatibleType() throws NoSuchMethodException {
        Row row = mockRow(1000000000, 1L, new LongType());

        TimeseriesData data = SparkTimeseriesData.of(row);

        assertThrows(NoSuchMethodException.class, () -> data.getDoubleValueAtIndex(0));
    }

    @Test
    public void shouldGetVectorLongValueAtIndexIfCompatibleType() throws NoSuchMethodException {
        Row row = mockRow(1000000000, new long[] { 1L }, new ArrayType());

        TimeseriesData data = SparkTimeseriesData.of(row);

        assertEquals(1L, data.getLongValueAtIndex(0));
        assertFalse(data.isNullValue());
    }

    @Test
    public void shouldFailGetVectorLongValueAtIndexIfIncompatibleType() throws NoSuchMethodException {
        Row row = mockRow(1000000000, 1.2d, new DoubleType());

        TimeseriesData data = SparkTimeseriesData.of(row);

        assertThrows(NoSuchMethodException.class, () -> data.getLongValueAtIndex(0));
    }

    @Test
    public void shouldGetVectorStringValueAtIndexIfCompatibleType() throws NoSuchMethodException {
        Row row = mockRow(1000000000, new String[] { "test" }, new ArrayType());

        TimeseriesData data = SparkTimeseriesData.of(row);

        assertEquals("test", data.getStringValueAtIndex(0));
        assertFalse(data.isNullValue());
    }

    @Test
    public void shouldFailGetVectorStringValueAtIndexIfIncompatibleType() throws NoSuchMethodException {
        Row row = mockRow(1000000000, 1.2d, new DoubleType());

        TimeseriesData data = SparkTimeseriesData.of(row);

        assertThrows(NoSuchMethodException.class, () -> data.getStringValueAtIndex(0));
    }

    @Test
    public void equalityForArraysShouldWork() throws NoSuchMethodException {
        Row row1 = mockRow(1000000000, new Long[] { 1L }, new ArrayType());
        Row row2 = mockRow(1000000000, new long[] { 1L }, new ArrayType());

        TimeseriesData data1 = SparkTimeseriesData.of(row1);
        TimeseriesData data2 = SparkTimeseriesData.of(row2);

        assertEquals(data1, data2);
    }

    @Test
    public void equalityForValuesShouldWork() throws NoSuchMethodException {
        Row row1 = mockRow(1000000000, new Long(1), new LongType());
        Row row2 = mockRow(1000000000, 1L, new LongType());

        TimeseriesData data1 = SparkTimeseriesData.of(row1);
        TimeseriesData data2 = SparkTimeseriesData.of(row2);

        assertEquals(data1, data2);
    }

    @Test
    public void shouldGetMatrixDoubleValuesIfDouble() throws NoSuchMethodException {
        StructType arrayType = new StructType();
        arrayType = arrayType.add(ARRAY_ELEMENTS_FIELD_NAME, new ArrayType());
        arrayType = arrayType.add(ARRAY_DIMENSIONS_FIELD_NAME, new ArrayType());
        Row array = new GenericRowWithSchema(
                new Object[] { new double[] { 1.2d, 1.3d, 1.4d, 1.5d, 1.6d, 1.7d }, new int[] { 2, 3 } }, arrayType);

        Row row = mockRow(1000000000, array, arrayType);

        TimeseriesData data = SparkTimeseriesData.of(row);
        assertFalse(data.isNullValue());

        double[][] matrix = data.getMatrixDoubleValues();
        assertArrayEquals(new double[] { 1.2d, 1.3d, 1.4d }, matrix[0], 0);
        assertArrayEquals(new double[] { 1.5d, 1.6d, 1.7d }, matrix[1], 0);
    }

    @Test
    public void shouldGetMatrixDoubleValuesIfCompatibleType() throws NoSuchMethodException {
        StructType arrayType = new StructType();
        arrayType = arrayType.add(ARRAY_ELEMENTS_FIELD_NAME, new ArrayType());
        arrayType = arrayType.add(ARRAY_DIMENSIONS_FIELD_NAME, new ArrayType());
        Row array = new GenericRowWithSchema(
                new Object[] { new double[] { 1.2f, 1.3f, 1.4f, 1.5f, 1.6f, 1.7f }, new int[] { 2, 3 } }, arrayType);

        Row row = mockRow(1000000000, array, arrayType);

        TimeseriesData data = SparkTimeseriesData.of(row);
        assertFalse(data.isNullValue());

        double[][] matrix = data.getMatrixDoubleValues();
        assertArrayEquals(new double[] { 1.2d, 1.3d, 1.4d }, matrix[0], 0.0001d);
        assertArrayEquals(new double[] { 1.5d, 1.6d, 1.7d }, matrix[1], 0.0001d);
    }

    @Test
    public void shouldGetMatrixDoubleValuesIfBoolean() throws NoSuchMethodException {
        StructType arrayType = new StructType();
        arrayType = arrayType.add(ARRAY_ELEMENTS_FIELD_NAME, new ArrayType());
        arrayType = arrayType.add(ARRAY_DIMENSIONS_FIELD_NAME, new ArrayType());
        Row array = new GenericRowWithSchema(
                new Object[] { new boolean[] { true, false, true, false, true, false }, new int[] { 2, 3 } }, arrayType);

        Row row = mockRow(1000000000, array, arrayType);

        TimeseriesData data = SparkTimeseriesData.of(row);
        assertFalse(data.isNullValue());

        double[][] matrix = data.getMatrixDoubleValues();
        assertArrayEquals(new double[] { 1, 0, 1 }, matrix[0], 0.0001d);
        assertArrayEquals(new double[] { 0, 1, 0 }, matrix[1], 0.0001d);
    }


    @Test
    public void shouldGetMatrixDoubleValuesIfIncompatibleType() throws NoSuchMethodException {
        StructType arrayType = new StructType();
        arrayType = arrayType.add(ARRAY_ELEMENTS_FIELD_NAME, new ArrayType());
        arrayType = arrayType.add(ARRAY_DIMENSIONS_FIELD_NAME, new ArrayType());
        Row array = new GenericRowWithSchema(new Object[] { new long[] { 1L, 2L, 3L, 4L, 5L, 6L }, new int[] { 3, 2 } },
                arrayType);

        Row row = mockRow(1000000000, array, arrayType);

        TimeseriesData data = SparkTimeseriesData.of(row);
        assertFalse(data.isNullValue());

        double[][] matrix = data.getMatrixDoubleValues();
        assertArrayEquals(new double[] { 1.0d, 2.0d }, matrix[0], 0.0001d);
        assertArrayEquals(new double[] { 3.0d, 4.0d }, matrix[1], 0.0001d);
        assertArrayEquals(new double[] { 5.0d, 6.0d }, matrix[2], 0.0001d);

    }

    @Test
    public void shouldGetMatrixLongValuesIfLong() throws NoSuchMethodException {
        StructType arrayType = new StructType();
        arrayType = arrayType.add(ARRAY_ELEMENTS_FIELD_NAME, new ArrayType());
        arrayType = arrayType.add(ARRAY_DIMENSIONS_FIELD_NAME, new ArrayType());
        Row array = new GenericRowWithSchema(new Object[] { new long[] { 1L, 2L, 3L, 4L, 5L, 6L }, new int[] { 2, 3 } },
                arrayType);

        Row row = mockRow(1000000000, array, arrayType);

        TimeseriesData data = SparkTimeseriesData.of(row);
        assertFalse(data.isNullValue());

        long[][] matrix = data.getMatrixLongValues();
        assertArrayEquals(new long[] { 1L, 2L, 3L }, matrix[0]);
        assertArrayEquals(new long[] { 4L, 5L, 6L }, matrix[1]);
    }

    @Test
    public void shouldGetMatrixLongValuesIfCompatibleType() throws NoSuchMethodException {
        StructType arrayType = new StructType();
        arrayType = arrayType.add(ARRAY_ELEMENTS_FIELD_NAME, new ArrayType());
        arrayType = arrayType.add(ARRAY_DIMENSIONS_FIELD_NAME, new ArrayType());
        Row array = new GenericRowWithSchema(new Object[] { new long[] { 1, 2, 3, 4, 5, 6 }, new int[] { 3, 2 } },
                arrayType);

        Row row = mockRow(1000000000, array, arrayType);

        TimeseriesData data = SparkTimeseriesData.of(row);
        assertFalse(data.isNullValue());

        long[][] matrix = data.getMatrixLongValues();
        assertArrayEquals(new long[] { 1L, 2L }, matrix[0]);
        assertArrayEquals(new long[] { 3L, 4L }, matrix[1]);
        assertArrayEquals(new long[] { 5L, 6L }, matrix[2]);
    }

    @Test
    public void shouldGetMatrixLongValuesIfBoolean() throws NoSuchMethodException {
        StructType arrayType = new StructType();
        arrayType = arrayType.add(ARRAY_ELEMENTS_FIELD_NAME, new ArrayType());
        arrayType = arrayType.add(ARRAY_DIMENSIONS_FIELD_NAME, new ArrayType());
        Row array = new GenericRowWithSchema(new Object[] { new boolean[] { true, false, true, false, true, false }, new int[] { 3, 2 } },
                arrayType);

        Row row = mockRow(1000000000, array, arrayType);

        TimeseriesData data = SparkTimeseriesData.of(row);
        assertFalse(data.isNullValue());

        long[][] matrix = data.getMatrixLongValues();
        assertArrayEquals(new long[] { 1L, 0L }, matrix[0]);
        assertArrayEquals(new long[] { 1L, 0L }, matrix[1]);
        assertArrayEquals(new long[] { 1L, 0L }, matrix[2]);
    }

    @Test
    public void shouldFailGetMatrixLongValuesIfIncompatibleType() throws NoSuchMethodException {
        StructType arrayType = new StructType();
        arrayType = arrayType.add(ARRAY_ELEMENTS_FIELD_NAME, new ArrayType());
        arrayType = arrayType.add(ARRAY_DIMENSIONS_FIELD_NAME, new ArrayType());
        Row array = new GenericRowWithSchema(
                new Object[] { new double[] { 1.2d, 2.3d, 3.4d, 4.5d, 5.6d, 6.7d }, new int[] { 2, 3 } }, arrayType);

        Row row = mockRow(1000000000, array, arrayType);

        TimeseriesData data = SparkTimeseriesData.of(row);
        assertFalse(data.isNullValue());

        long[][] matrix = data.getMatrixLongValues();
        assertArrayEquals(new long[] { 1L, 2L, 3L }, matrix[0]);
        assertArrayEquals(new long[] { 4L, 5L, 6L }, matrix[1]);
    }

    @Test
    public void shouldConvertToString() throws NoSuchMethodException {
        Row row = mockRow(1000000000, 1L, new LongType());

        TimeseriesData data = SparkTimeseriesData.of(row);

        assertThrows(NoSuchMethodException.class, () -> data.getVarcharValue());
    }

    @Test
    public void shouldFailOnConvertToStringIfNonArrayType() throws NoSuchMethodException {
        Row row = mockRow(1000000000, 1L, new LongType());

        TimeseriesData data = SparkTimeseriesData.of(row);

        assertThrows(NoSuchMethodException.class, () -> data.toStringValue(0));
    }

    @Test
    public void shouldConvertToStringArrayElement() throws NoSuchMethodException {
        Row row = mockRow(1000000000, new long[] { 1L }, new ArrayType());

        TimeseriesData data = SparkTimeseriesData.of(row);

        assertThrows(NoSuchMethodException.class, () -> data.toStringValue(0));
    }

    @Test
    public void shouldDetectNullValue() throws NoSuchMethodException {
        Row row = mockRow(1000000000, null, new DoubleType());

        TimeseriesData data = SparkTimeseriesData.of(row);

        assertTrue(data.isNullValue());
    }

    @Test
    public void shouldDetectNullValueInAnArray() throws NoSuchMethodException {
        Row row = mockRow(1000000000, new String[] { "test", null }, new ArrayType());

        TimeseriesData data = SparkTimeseriesData.of(row);

        assertFalse(data.isNullValue(0));
        assertTrue(data.isNullValue(1));
    }

    @Test
    public void alignToTimestamp() throws NoSuchMethodException {
        int timestamp1 = 1000000000;
        int timestamp2 = 2000000000;

        Row row = mockRow(timestamp1, new String[] { "test", null }, new ArrayType());

        TimeseriesData data1 = SparkTimeseriesData.of(row);
        TimeseriesData data2 = data1.alignToTimestamp(Timestamp.from(TimeUtils.getInstantFromNanos(timestamp2)));

        assertEquals(data1.getStamp(), Timestamp.from(TimeUtils.getInstantFromNanos(timestamp1)));
        assertEquals(data2.getStamp(), Timestamp.from(TimeUtils.getInstantFromNanos(timestamp2)));
    }

    @Test
    public void shouldCompareLongValues() throws NoSuchMethodException {
        TimeseriesData data1;
        TimeseriesData data2;
        data1 = SparkTimeseriesData.of(mockRow(1000000000, 1L, new LongType()));
        data2 = SparkTimeseriesData.of(mockRow(1000000000, 2L, new LongType()));

        assertTrue(data1.compareValueTo(data2) < 0);

        data1 = SparkTimeseriesData.of(mockRow(1000000000, 1L, new LongType()));
        data2 = SparkTimeseriesData.of(mockRow(1000000000, 1L, new LongType()));

        assertEquals(0, data1.compareValueTo(data2));

        data1 = SparkTimeseriesData.of(mockRow(1000000000, 2L, new LongType()));
        data2 = SparkTimeseriesData.of(mockRow(1000000000, 1L, new LongType()));

        assertTrue(data1.compareValueTo(data2) > 0);
    }

    @Test
    public void shouldCompareDoubleValues() throws NoSuchMethodException {
        TimeseriesData data1;
        TimeseriesData data2;

        data1 = SparkTimeseriesData.of(mockRow(1000000000, 1.2d, new DoubleType()));
        data2 = SparkTimeseriesData.of(mockRow(1000000000, 2.2d, new DoubleType()));

        assertTrue(data1.compareValueTo(data2) < 0);

        data1 = SparkTimeseriesData.of(mockRow(1000000000, 1.2d, new DoubleType()));
        data2 = SparkTimeseriesData.of(mockRow(1000000000, 1.2d, new DoubleType()));

        assertEquals(0, data1.compareValueTo(data2));

        data1 = SparkTimeseriesData.of(mockRow(1000000000, 2.2d, new DoubleType()));
        data2 = SparkTimeseriesData.of(mockRow(1000000000, 1.2d, new DoubleType()));

        assertTrue(data1.compareValueTo(data2) > 0);
    }

    @Test
    public void shouldDetectTypeOnNUlls() throws NoSuchMethodException {
        TimeseriesData convert;
        convert = SparkTimeseriesData.of(mockRow(1000000000, null, new DoubleType()));
        assertTrue(convert.isNullValue());
        assertNotNull(convert.type());
        assertEquals(convert.type(), EntryType.DOUBLE.getTypeClass());

        convert = SparkTimeseriesData.of(mockRow(1000000000, null, new StringType()));
        assertTrue(convert.isNullValue());
        assertNotNull(convert.type());
        assertEquals(convert.type(), EntryType.STRING.getTypeClass());
    }

    @Test
    public void shouldNotAcceptNullTimestamp() {
        Row row = mockRow(1000000000, 1.2d, new DoubleType());
        when(row.getAs(NXC_EXTR_TIMESTAMP.getValue())).thenReturn(null);
        assertThrows(NullPointerException.class, () -> SparkTimeseriesData.of(row));
    }

    @Test
    public void shouldGetScalarVariableDataType(){
        TimeseriesData data = SparkTimeseriesData.of(mockRow(0, 1.2d, new DoubleType()));
        assertSame(NUMERIC, data.getVariableDataType());
    }

    @Test
    public void shouldGetVectorVariableDataType(){
        String[] vector = new String[]{
                "A", "B", "C"
        };

        TimeseriesData data = SparkTimeseriesData.of(mockRow(0, vector, new StringType()));
        assertSame(VECTOR_STRING, data.getVariableDataType());
    }

    @Test
    public void shouldGetMatrixVariableDataType() {
        StructType arrayType = new StructType();
        arrayType = arrayType.add(ARRAY_ELEMENTS_FIELD_NAME, new ArrayType());
        arrayType = arrayType.add(ARRAY_DIMENSIONS_FIELD_NAME, new ArrayType());
        Row array = new GenericRowWithSchema(new Object[] { new long[] { 1L, 2L, 3L, 4L, 5L, 6L }, new int[] { 3, 2 } },
                arrayType);

        TimeseriesData data = SparkTimeseriesData.of(mockRow(0, array, arrayType));
        assertSame(MATRIX_NUMERIC, data.getVariableDataType());
    }

    @Test
    public void shouldGetUndeterminedVariableDataType() {
        TimeseriesData data = SparkTimeseriesData.of(mockRow(0, Boolean.TRUE, new BooleanType()));
        assertSame(UNDETERMINED, data.getVariableDataType());
    }

    private static Row mockRow(long timestamp, Object data, DataType type) {
        Row result = mockEmptyRow(timestamp);
        when(result.getAs(NXC_EXTR_VALUE.getValue())).thenReturn(data);

        StructField valueField = new StructField(NXC_EXTR_VALUE.getValue(), type, true, null);
        StructField timestampField = new StructField(NXC_EXTR_TIMESTAMP.getValue(), new LongType(), false, null);
        StructType structType = mock(StructType.class);
        when(structType.fields()).thenReturn(new StructField[] { timestampField, valueField });
        when(result.schema()).thenReturn(structType);
        return result;
    }

    private static Row mockEmptyRow(long timestamp) {
        Row result = mock(Row.class);
        when(result.getAs(NXC_EXTR_TIMESTAMP.getValue())).thenReturn(timestamp);

        StructField timestampField = new StructField(NXC_EXTR_TIMESTAMP.getValue(), new LongType(), false, null);
        StructType structType = mock(StructType.class);
        when(structType.fields()).thenReturn(new StructField[] { timestampField });
        when(result.schema()).thenReturn(structType);
        return result;
    }
}
