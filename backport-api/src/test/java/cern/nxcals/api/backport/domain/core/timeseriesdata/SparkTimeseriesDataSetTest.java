package cern.nxcals.api.backport.domain.core.timeseriesdata;

import cern.cmw.datax.ImmutableEntry;
import cern.nxcals.api.backport.domain.core.constants.VariableDataType;
import cern.nxcals.api.backport.domain.core.metadata.Variable;
import cern.nxcals.api.utils.TimeUtils;
import com.google.common.collect.ImmutableList;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SparkTimeseriesDataSetTest {

    @Test
    public void shouldSortUnorderedData() {
        List<TimeseriesData> list = ImmutableList.of(mockDataPoint(200), mockDataPoint(100), mockDataPoint(300));

        TimeseriesDataSet timeseriesData = SparkTimeseriesDataSet.of(mock(Variable.class), list);

        assertEquals(100, timeseriesData.getTimeseriesData(0).getStamp().getNanos());
        assertEquals(200, timeseriesData.getTimeseriesData(1).getStamp().getNanos());
        assertEquals(300, timeseriesData.getTimeseriesData(2).getStamp().getNanos());
    }

    @Test
    public void shouldPassVariableMetadata() {
        Variable mockVariable = mockVariable("name", "unit", VariableDataType.ALL);
        TimeseriesDataSet timeseriesData = SparkTimeseriesDataSet.of(mockVariable, Collections.emptyList());

        assertEquals("name", timeseriesData.getVariableName());
        assertEquals("unit", timeseriesData.getUnit());
        assertEquals(VariableDataType.ALL, timeseriesData.getVariableDataType());
        assertEquals(mockVariable, timeseriesData.getVariable());
    }

    @Test
    public void shouldGetMinAndMaxTimestamp() {
        TimeseriesDataSet timeseriesData = timeseriesDataSetFor(mock(Variable.class), 100, 200, 300);

        assertEquals(100, timeseriesData.getMinStamp().getNanos());
        assertEquals(300, timeseriesData.getMaxStamp().getNanos());
    }

    @Test
    public void shouldGetDataIfPresent() {
        List<TimeseriesData> list = ImmutableList.of(mockDataPoint(100), mockDataPoint(200), mockDataPoint(300));

        TimeseriesDataSet timeseriesData = SparkTimeseriesDataSet.of(mock(Variable.class), list);

        assertSame(list.get(0), timeseriesData.getTimeseriesData(timestampFromNanos(100)));
        assertSame(list.get(1), timeseriesData.getTimeseriesData(timestampFromNanos(200)));
        assertSame(list.get(2), timeseriesData.getTimeseriesData(timestampFromNanos(300)));
    }

    @Test
    public void shouldGetNullDataIfAbsent() {
        TimeseriesDataSet timeseriesData = timeseriesDataSetFor(mock(Variable.class), 100, 200, 300);

        assertNull(timeseriesData.getTimeseriesData(timestampFromNanos(120)));
        assertNull(timeseriesData.getTimeseriesData(timestampFromNanos(220)));
    }

    @Test
    public void shouldGetNullDataIfOutsideRange() {
        TimeseriesDataSet timeseriesData = timeseriesDataSetFor(mock(Variable.class), 100, 200, 300);

        assertNull(timeseriesData.getTimeseriesData(timestampFromNanos(20)));
        assertNull(timeseriesData.getTimeseriesData(timestampFromNanos(320)));
    }

    @Test
    public void shouldGetDataOrLastIfPresent() {
        List<TimeseriesData> list = ImmutableList.of(mockDataPoint(100), mockDataPoint(200), mockDataPoint(300));

        TimeseriesDataSet timeseriesData = SparkTimeseriesDataSet.of(mock(Variable.class), list);

        assertSame(list.get(0), timeseriesData.getTimeseriesDataOrLast(timestampFromNanos(100)));
        assertSame(list.get(1), timeseriesData.getTimeseriesDataOrLast(timestampFromNanos(200)));
        assertSame(list.get(2), timeseriesData.getTimeseriesDataOrLast(timestampFromNanos(300)));
    }

    @Test
    public void shouldGeNullDataOrLastIfAbsent() {
        List<TimeseriesData> list = ImmutableList.of(mockDataPoint(100), mockDataPoint(200), mockDataPoint(300));

        TimeseriesDataSet timeseriesData = SparkTimeseriesDataSet.of(mock(Variable.class), list);

        assertSame(list.get(0), timeseriesData.getTimeseriesDataOrLast(timestampFromNanos(120)));
        assertSame(list.get(1), timeseriesData.getTimeseriesDataOrLast(timestampFromNanos(220)));
        assertSame(list.get(2), timeseriesData.getTimeseriesDataOrLast(timestampFromNanos(320)));
    }

    @Test
    public void shouldGetNullDataOrLastIfOutsideRange() {
        TimeseriesDataSet timeseriesData = timeseriesDataSetFor(mock(Variable.class), 100, 200, 300);

        assertNull(timeseriesData.getTimeseriesDataOrLast(timestampFromNanos(20)));
    }

    @Test
    public void shouldAlignData() throws NoSuchMethodException {
        TimeseriesDataSet actual = timeseriesDataSetFor(mock(Variable.class), 100, 200, 300);
        TimeseriesDataSet desired = timeseriesDataSetFor(mock(Variable.class), 120, 220, 320);

        TimeseriesDataSet aligned = actual.alignToTimestamps(desired);

        assertEquals(120, aligned.getTimeseriesData(0).getStamp().getNanos());
        assertEquals(220, aligned.getTimeseriesData(1).getStamp().getNanos());
        assertEquals(320, aligned.getTimeseriesData(2).getStamp().getNanos());

        assertEquals(aligned.getTimeseriesData(0).getLongValue(), actual.getTimeseriesData(0).getLongValue());
        assertEquals(aligned.getTimeseriesData(1).getLongValue(), actual.getTimeseriesData(1).getLongValue());
        assertEquals(aligned.getTimeseriesData(2).getLongValue(), actual.getTimeseriesData(2).getLongValue());
    }

    @Test
    public void shouldAlignDataAtExactValues() throws NoSuchMethodException {
        TimeseriesDataSet actual = timeseriesDataSetFor(mock(Variable.class), 100, 200, 300);
        TimeseriesDataSet desired = timeseriesDataSetFor(mock(Variable.class), 100, 200, 300);

        TimeseriesDataSet aligned = actual.alignToTimestamps(desired);

        assertEquals(100, aligned.getTimeseriesData(0).getStamp().getNanos());
        assertEquals(200, aligned.getTimeseriesData(1).getStamp().getNanos());
        assertEquals(300, aligned.getTimeseriesData(2).getStamp().getNanos());

        assertEquals(aligned.getTimeseriesData(0).getLongValue(), actual.getTimeseriesData(0).getLongValue());
        assertEquals(aligned.getTimeseriesData(1).getLongValue(), actual.getTimeseriesData(1).getLongValue());
        assertEquals(aligned.getTimeseriesData(2).getLongValue(), actual.getTimeseriesData(2).getLongValue());
    }


    @Test
    public void shouldAlignManyToOne() throws NoSuchMethodException {
        TimeseriesDataSet actual = timeseriesDataSetFor(mock(Variable.class), 100, 200, 300);
        TimeseriesDataSet desired = timeseriesDataSetFor(mock(Variable.class), 220, 320);

        TimeseriesDataSet aligned = actual.alignToTimestamps(desired);

        assertEquals(220, aligned.getTimeseriesData(0).getStamp().getNanos());
        assertEquals(320, aligned.getTimeseriesData(1).getStamp().getNanos());

        assertEquals(aligned.getTimeseriesData(0).getLongValue(), actual.getTimeseriesData(1).getLongValue());
        assertEquals(aligned.getTimeseriesData(1).getLongValue(), actual.getTimeseriesData(2).getLongValue());
    }

    @Test
    public void shouldAlignOneToMany() throws NoSuchMethodException {
        TimeseriesDataSet actual = timeseriesDataSetFor(mock(Variable.class), 100, 200, 300);
        TimeseriesDataSet desired = timeseriesDataSetFor(mock(Variable.class), 210, 220, 320);

        TimeseriesDataSet aligned = actual.alignToTimestamps(desired);

        assertEquals(210, aligned.getTimeseriesData(0).getStamp().getNanos());
        assertEquals(220, aligned.getTimeseriesData(1).getStamp().getNanos());
        assertEquals(320, aligned.getTimeseriesData(2).getStamp().getNanos());

        assertEquals(aligned.getTimeseriesData(0).getLongValue(), actual.getTimeseriesData(1).getLongValue());
        assertEquals(aligned.getTimeseriesData(1).getLongValue(), actual.getTimeseriesData(1).getLongValue());
        assertEquals(aligned.getTimeseriesData(2).getLongValue(), actual.getTimeseriesData(2).getLongValue());
    }

    @Test
    public void shouldAlignWhenAllActualAfter() {
        TimeseriesDataSet actual = timeseriesDataSetFor(mock(Variable.class), 100, 200, 300);
        TimeseriesDataSet desired = timeseriesDataSetFor(mock(Variable.class), 60, 70, 80);

        TimeseriesDataSet aligned = actual.alignToTimestamps(desired);

        assertTrue(aligned.isEmpty());
    }

    @Test
    public void shouldAlignWhenAllActualBefore() throws NoSuchMethodException {
        TimeseriesDataSet actual = timeseriesDataSetFor(mock(Variable.class), 60, 70, 80);
        TimeseriesDataSet desired = timeseriesDataSetFor(mock(Variable.class), 100, 200, 300);

        TimeseriesDataSet aligned = actual.alignToTimestamps(desired);

        assertEquals(100, aligned.getTimeseriesData(0).getStamp().getNanos());
        assertEquals(200, aligned.getTimeseriesData(1).getStamp().getNanos());
        assertEquals(300, aligned.getTimeseriesData(2).getStamp().getNanos());

        assertEquals(aligned.getTimeseriesData(0).getLongValue(), actual.getTimeseriesData(2).getLongValue());
        assertEquals(aligned.getTimeseriesData(1).getLongValue(), actual.getTimeseriesData(2).getLongValue());
        assertEquals(aligned.getTimeseriesData(2).getLongValue(), actual.getTimeseriesData(2).getLongValue());
    }

    private TimeseriesDataSet timeseriesDataSetFor(Variable variable, long... timestamps) {
        return SparkTimeseriesDataSet.of(variable, LongStream.of(timestamps).mapToObj(this::mockDataPoint).collect(Collectors.toList()));
    }

    private Timestamp timestampFromNanos(int timeInNanos) {
        return Timestamp.from(TimeUtils.getInstantFromNanos(timeInNanos));
    }

    private TimeseriesData mockDataPoint(long nanos) {
        return SparkTimeseriesData
                .of(Timestamp.from(TimeUtils.getInstantFromNanos(nanos)), ImmutableEntry.of("TestData", nanos));
    }

    private Variable mockVariable(String name, String unit, VariableDataType type) {
        Variable result = mock(Variable.class);
        when(result.getVariableName()).thenReturn(name);
        when(result.getUnit()).thenReturn(unit);
        when(result.getVariableDataType()).thenReturn(type);
        return result;
    }
}