package cern.nxcals.api.backport.domain.core.filldata;

import cern.nxcals.api.domain.TimeWindow;
import org.junit.jupiter.api.Test;

import java.time.Instant;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

class BeamModeTest {


    @Test
    void shouldCreateBeamModeWithNullEndTime() {
        cern.nxcals.api.custom.domain.BeamMode beamMode = new cern.nxcals.api.custom.domain.BeamMode(TimeWindow.between(
                Instant.now(),null), "SETUP");
        BeamMode newBeamMode = BeamMode.from(beamMode);
        assertNull(newBeamMode.getEndTime());
        assertNotNull(newBeamMode.getStartTime());

    }
}