package cern.nxcals.api.backport.client.service;

import cern.nxcals.api.backport.domain.core.constants.VariableDataType;
import cern.nxcals.api.backport.domain.core.metadata.JapcParameterDefinition;
import cern.nxcals.api.backport.domain.core.metadata.SimpleJapcParameter;
import cern.nxcals.api.backport.domain.core.metadata.VariableList;
import cern.nxcals.api.backport.domain.core.metadata.VariableSet;
import cern.nxcals.api.custom.domain.GroupType;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.Group;
import cern.nxcals.api.domain.Partition;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.api.domain.VariableDeclaredType;
import cern.nxcals.api.domain.Visibility;
import cern.nxcals.api.extraction.metadata.EntityService;
import cern.nxcals.api.extraction.metadata.GroupService;
import cern.nxcals.api.extraction.metadata.HierarchyService;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.extraction.metadata.queries.Entities;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import cern.nxcals.common.utils.ReflectionUtils;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.avro.SchemaBuilder;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static cern.nxcals.api.custom.domain.CmwSystemConstants.DEVICE_KEY_NAME;
import static cern.nxcals.api.custom.domain.CmwSystemConstants.PROPERTY_KEY_NAME;
import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static com.google.common.collect.Sets.newHashSet;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MetaDataServiceImplTest {
    private static final String SCHEMA = SchemaBuilder.record("entity_type_1").fields()
            .name("device").type().stringType().noDefault()
            .name("property").type().stringType().noDefault()
            .endRecord().toString();
    private static final String VARIABLE_NAME1 = "CIB.CCR.LHC.B1:BEAM_PERMIT_A";
    private static final String VARIABLE_NAME2 = "VAR2";

    private static final SystemSpec SYSTEM_SPEC = SystemSpec.builder().name("TEST").entityKeyDefinitions("")
            .partitionKeyDefinitions("").timeKeyDefinitions("").build();

    public static final String CMW = "CMW";
    private static final SystemSpec CMW_SYSTEM_SPEC = SystemSpec.builder().name(CMW).entityKeyDefinitions(SCHEMA)
            .partitionKeyDefinitions("").timeKeyDefinitions("").build();

    public static final String FIELD_NAME1 = "field1";
    public static final String FIELD_NAME2 = "field2";
    public static final String FIELD_NAME3 = "field3";

    private static final VariableConfig VARIABLE_CONFIG1 = VariableConfig.builder().entityId(1L).fieldName(FIELD_NAME1)
            .variableName(VARIABLE_NAME1).validity(TimeWindow.infinite()).build();

    private static final VariableConfig VARIABLE_CONFIG2 = VariableConfig.builder().entityId(1L).fieldName(FIELD_NAME2)
            .validity(TimeWindow.between(0, 100)).build();

    private static final VariableConfig VARIABLE_CONFIG3 = VariableConfig.builder().entityId(1L).fieldName(FIELD_NAME3)
            .validity(TimeWindow.between(100, 200)).build();

    private static final Variable VARIABLE1 = Variable.builder().variableName(VARIABLE_NAME1)
            .configs(Sets.newTreeSet(newHashSet(VARIABLE_CONFIG1))).systemSpec(SYSTEM_SPEC)
            .declaredType(VariableDeclaredType.NUMERIC).build();

    private static final Variable VARIABLE2 = Variable.builder().variableName(VARIABLE_NAME2)
            .configs(Sets.newTreeSet(newHashSet(VARIABLE_CONFIG2, VARIABLE_CONFIG3))).systemSpec(SYSTEM_SPEC)
            .declaredType(VariableDeclaredType.NUMERIC).build();

    private static final String PATTERN = "VAR%";
    private static final String GROUP_OWNER = "group_owner";
    private static final String DESCRIPTION = "desc";

    public static final String PROPERTY_NAME = "PROP";
    private static final Entity entity = Entity.builder().entityKeyValues(
            ImmutableMap.of("device", "DEV", "property", PROPERTY_NAME)).systemSpec(CMW_SYSTEM_SPEC)
            .partition(mock(Partition.class)).build();
    public static final String DEVICE_NAME = "DEV";

    @Mock
    private VariableService variableService;
    @Mock
    private HierarchyService hierarchyService;
    @Mock
    private GroupService groupService;

    @Mock
    private SystemSpecService systemSpecService;

    @Mock
    private EntityService entityService;

    private MetaDataServiceImpl nxcalsMetaDataService;

    @Mock
    private SparkSession spark;

    @BeforeEach
    public void setUp() {
        nxcalsMetaDataService = new MetaDataServiceImpl(systemSpecService, entityService, variableService,
                hierarchyService, groupService, spark);
    }

    @Test
    public void shouldGetJapcParameterDefinitionForDeviceAndPropertyName() {
        //given
        when(systemSpecService.findByName(CMW)).thenReturn(Optional.of(CMW_SYSTEM_SPEC));
        when(entityService.findOne(argThat((Condition c) -> true))).thenReturn(Optional.of(entity));

        this.variableService
                .findAll(Variables.suchThat().configsEntityId().eq(entity.getId()));

        Condition<Variables> condition = Variables.suchThat().configsEntityId().eq(entity.getId());

        when(variableService.findAll(argThat((Condition<Variables> cond) -> toRSQL(cond).equals(toRSQL(condition)))))
                .thenReturn(newHashSet(VARIABLE1, VARIABLE2));
        //when
        JapcParameterDefinition japcParameterDefinition = nxcalsMetaDataService
                .getJapcParameterDefinitionForDeviceAndPropertyName(DEVICE_NAME, PROPERTY_NAME);

        //then
        assertEquals(DEVICE_NAME, japcParameterDefinition.getDeviceName());
        assertEquals(PROPERTY_NAME, japcParameterDefinition.getPropertyName());
        assertEquals(3, japcParameterDefinition.getFieldMap().size());

        Set<cern.nxcals.api.backport.domain.core.metadata.Variable> vars1 = japcParameterDefinition
                .getVariablesForField(FIELD_NAME1);
        assertEquals(1, vars1.size());
        assertTrue(vars1.stream().map(cern.nxcals.api.backport.domain.core.metadata.Variable::getVariableName).collect(
                Collectors.toSet()).contains(VARIABLE_NAME1));

        Set<cern.nxcals.api.backport.domain.core.metadata.Variable> vars2 = japcParameterDefinition
                .getVariablesForField(FIELD_NAME2);
        assertEquals(1, vars1.size());
        assertTrue(vars2.stream().map(cern.nxcals.api.backport.domain.core.metadata.Variable::getVariableName).collect(
                Collectors.toSet()).contains(VARIABLE_NAME2));

        Set<cern.nxcals.api.backport.domain.core.metadata.Variable> vars3 = japcParameterDefinition
                .getVariablesForField(FIELD_NAME3);
        assertEquals(1, vars1.size());
        assertTrue(vars3.stream().map(cern.nxcals.api.backport.domain.core.metadata.Variable::getVariableName).collect(
                Collectors.toSet()).contains(VARIABLE_NAME2));

        Set<cern.nxcals.api.backport.domain.core.metadata.Variable> vars4 = japcParameterDefinition
                .getVariablesForField(FIELD_NAME3 + "_NOT_THERE");
        assertEquals(0, vars4.size());
    }

    @Test
    public void shouldGetVariablesOfDataTypeWithNameLikePatternWithNUMERICType() {
        //given
        Condition<Variables> condition = Variables.suchThat().variableName().like(PATTERN).and().declaredType()
                .eq(VariableDeclaredType.NUMERIC);

        when(variableService.findAll(argThat((Condition<Variables> cond) -> toRSQL(cond).equals(toRSQL(condition)))))
                .thenReturn(newHashSet(VARIABLE1));

        //when
        VariableSet nxcalsVariables = nxcalsMetaDataService
                .getVariablesOfDataTypeWithNameLikePattern(PATTERN, VariableDataType.NUMERIC);

        //then
        assertNotNull(nxcalsVariables);
        assertThat(nxcalsVariables.getVariableNames()).containsExactlyInAnyOrder(VARIABLE_NAME1);
    }

    @Test
    public void shouldGetVariablesOfDataTypeWithNameLikePatternWithALLType() {
        //given
        Condition<Variables> condition = Variables.suchThat().variableName().like(PATTERN);

        when(variableService.findAll(argThat((Condition<Variables> cond) -> toRSQL(cond).equals(toRSQL(condition)))))
                .thenReturn(newHashSet(VARIABLE1));

        //when
        VariableSet nxcalsVariables = nxcalsMetaDataService
                .getVariablesOfDataTypeWithNameLikePattern(PATTERN, VariableDataType.ALL);

        //then
        assertNotNull(nxcalsVariables);
        assertThat(nxcalsVariables.getVariableNames()).containsExactlyInAnyOrder(VARIABLE_NAME1);
    }

    @Test
    public void shouldGetVariablesWithNameInListOfStrings() {
        //given
        List<String> listOfNames = Lists.newArrayList(VARIABLE_NAME1);
        when(variableService.findAll(argThat((Condition<Variables> cond) -> true))).thenReturn(newHashSet(VARIABLE1));

        //when
        VariableSet nxcalsVariables = nxcalsMetaDataService
                .getVariablesWithNameInListofStrings(listOfNames);

        //then
        assertNotNull(nxcalsVariables);
        assertThat(nxcalsVariables.getVariableNames()).containsExactlyInAnyOrder(VARIABLE_NAME1);
    }

    @Test
    public void getVariablesOfDataTypeInVariableList() {
        String name = "my-group";

        Group existing = group(name).build();

        when(groupService.findOne(any())).thenReturn(Optional.of(existing));

        Map<String, Set<Variable>> variables = new HashMap<>();
        variables.put(null, Sets.newHashSet(variable(1, "name-1", VariableDeclaredType.NUMERIC),
                variable(2, "name-2", VariableDeclaredType.TEXT),
                variable(3, "name-3", VariableDeclaredType.NUMERIC),
                variable(4, "name-4", VariableDeclaredType.FUNDAMENTAL)));

        when(groupService.getVariables(existing.getId())).thenReturn(variables);

        VariableSet found;
        found = nxcalsMetaDataService
                .getVariablesOfDataTypeInVariableList(mock(VariableList.class), VariableDataType.NUMERIC);

        assertTrue(found.containsNumericVariables());
        assertTrue(found.containsVariable("name-1"));
        assertFalse(found.containsVariable("name-2"));
        assertTrue(found.containsVariable("name-3"));
        assertFalse(found.containsVariable("name-4"));

        found = nxcalsMetaDataService.getVariablesOfDataTypeInVariableList(mock(VariableList.class), VariableDataType.ALL);

        assertTrue(found.containsNumericVariables());
        assertTrue(found.containsVariable("name-1"));
        assertTrue(found.containsVariable("name-2"));
        assertTrue(found.containsVariable("name-3"));
        assertTrue(found.containsVariable("name-4"));
    }

    @Test
    public void shouldNotAllowTypeToBeNull() {
        assertThrows(NullPointerException.class, () -> nxcalsMetaDataService.getVariablesOfDataTypeInVariableList(mock(VariableList.class), null));
    }

    @Test
    public void shouldNotAllowTypeToBeNullInPatternMethod() {
        assertThrows(NullPointerException.class, () -> nxcalsMetaDataService.getVariablesOfDataTypeWithNameLikePatternInVariableList(mock(VariableList.class), "pattern", null));
    }

    @Test
    public void getAllDevicesTest() {
        String query = String.format("%%\"%s\": %%", DEVICE_KEY_NAME);

        HashSet<Entity> result = newHashSet(
                entity("dev1", "prop1"),
                entity("dev2", "prop1"),
                entity("dev3", "prop2"),
                entity("dev4", "prop2")
        );

        when(entityService.findAll(argThat((Condition<Entities> cond) -> toRSQL(cond).contains(query)))).thenReturn(
                result);

        assertThat(nxcalsMetaDataService.getAllDeviceNames())
                .containsExactlyInAnyOrder("dev1", "dev2", "dev3", "dev4");
    }

    @Test
    public void getPropertiesForDeviceTest() {
        String query = String.format("%%\"%s\": \"%s\"%%\"%s\"%%", DEVICE_KEY_NAME, "dev1", PROPERTY_KEY_NAME);

        HashSet<Entity> result = newHashSet(
                entity("dev1", "prop1"),
                entity("dev1", "prop2")
        );

        when(entityService.findAll(argThat((Condition<Entities> cond) -> toRSQL(cond).contains(query)))).thenReturn(
                result);

        assertThat(nxcalsMetaDataService.getPropertyNamesForDeviceName("dev1"))
                .containsExactlyInAnyOrder("prop1", "prop2");

        assertThat(nxcalsMetaDataService.getPropertyNamesForDeviceName("dev2"))
                .isEmpty();
    }

    @Test
    public void getJapcDefinitionFor() {
        Group group = group("my-list")
                .build();

        when(groupService.findOne(argThat(cond -> toRSQL(cond).contains("500")))).thenReturn(Optional.of(group));

        HashSet<Entity> entities = newHashSet(
                entity(1, "dev1", "prop1"),
                entity(2, "dev1", "prop2"),
                entity(3, "dev2", "prop2")
        );
        when(entityService.findAll(
                argThat((Condition<Entities> cond) -> toRSQL(cond).contains("(\"1\",\"2\",\"3\",\"4\")"))))
                .thenReturn(entities);

        Map<String, Set<Variable>> variables = new HashMap<>();
        variables.put(null, Sets.newHashSet(
                variable(1, "name-A-1", VariableDeclaredType.NUMERIC),
                variable(2, "name-A-2", VariableDeclaredType.TEXT),
                variable(3, "name-B-3", VariableDeclaredType.NUMERIC),
                variable(4, "name-B-4", VariableDeclaredType.FUNDAMENTAL)
        ));
        when(groupService.getVariables(group.getId())).thenReturn(variables);

        Collection<SimpleJapcParameter> result = nxcalsMetaDataService.getJapcDefinitionFor(VariableList.from(group));

        assertThat(result).hasSize(3);
        assertThat(result).extracting(SimpleJapcParameter::getFieldName).containsOnly("field");
        assertThat(result).extracting(SimpleJapcParameter::getPropertyName).containsOnly("prop1", "prop2");
        assertThat(result).extracting(SimpleJapcParameter::getDeviceName).containsOnly("dev1", "dev2");
        assertThat(result).extracting(SimpleJapcParameter::getVariableName)
                .containsOnly("name-A-1", "name-A-2", "name-B-3");
    }

    @Test
    public void getJapcDefinitionForEmpty() {
        Group group = group("my-list").build();

        when(groupService.findOne(any())).thenReturn(Optional.empty());

        Collection<SimpleJapcParameter> result = nxcalsMetaDataService.getJapcDefinitionFor(VariableList.from(group));

        assertThat(result).isEmpty();
    }

    private Entity entity(long id, String device, String property) {
        Entity entity = entity(device, property);
        when(entity.getId()).thenReturn(id);
        return entity;
    }

    private Entity entity(String device, String property) {
        Entity entity = mock(Entity.class);
        when(entity.getEntityKeyValues()).thenReturn(ImmutableMap.of(DEVICE_KEY_NAME, device, PROPERTY_KEY_NAME, property));
        return entity;
    }

    @Test
    public void getVariablesOfDataTypeWithNameLikePatternInVariableList() {
        String name = "my-group";

        Group existing = group(name).build();

        when(groupService.findOne(any())).thenReturn(Optional.of(existing));

        Map<String, Set<Variable>> variables = new HashMap<>();
        variables.put(null, Sets.newHashSet(
                variable(1, "name-A-1", VariableDeclaredType.NUMERIC),
                variable(2, "name-A-2", VariableDeclaredType.TEXT),
                variable(3, "name-B-3", VariableDeclaredType.NUMERIC),
                variable(4, "name-B-4", VariableDeclaredType.FUNDAMENTAL)
        ));
        when(groupService.getVariables(existing.getId())).thenReturn(variables);

        VariableSet found;

        found = nxcalsMetaDataService
                .getVariablesOfDataTypeWithNameLikePatternInVariableList(mock(VariableList.class), "name-A-%",
                        VariableDataType.NUMERIC);

        assertTrue(found.containsVariable("name-A-1"));
        assertFalse(found.containsVariable("name-A-2"));
        assertFalse(found.containsVariable("name-B-3"));
        assertFalse(found.containsVariable("name-B-4"));

        found = nxcalsMetaDataService.getVariablesOfDataTypeWithNameLikePatternInVariableList(mock(VariableList.class), "name-B-%", VariableDataType.FUNDAMENTAL);

        assertFalse(found.containsVariable("name-A-1"));
        assertFalse(found.containsVariable("name-A-2"));
        assertFalse(found.containsVariable("name-B-3"));
        assertTrue(found.containsVariable("name-B-4"));

        found = nxcalsMetaDataService.getVariablesOfDataTypeWithNameLikePatternInVariableList(mock(VariableList.class), "name-%", VariableDataType.NUMERIC);

        assertTrue(found.containsVariable("name-A-1"));
        assertFalse(found.containsVariable("name-A-2"));
        assertTrue(found.containsVariable("name-B-3"));
        assertFalse(found.containsVariable("name-B-4"));

        found = nxcalsMetaDataService.getVariablesOfDataTypeWithNameLikePatternInVariableList(mock(VariableList.class), "name-%", VariableDataType.ALL);

        assertTrue(found.containsVariable("name-A-1"));
        assertTrue(found.containsVariable("name-A-2"));
        assertTrue(found.containsVariable("name-B-3"));
        assertTrue(found.containsVariable("name-B-4"));
    }

    private Group.Builder group(String name) {
        return ReflectionUtils.builderInstance(Group.InnerBuilder.class)
                .id(500)
                .owner(GROUP_OWNER)
                .label(GroupType.GROUP.toString())
                .name(name)
                .description(DESCRIPTION)
                .systemSpec(mock(SystemSpec.class))
                .visibility(Visibility.PROTECTED);
    }

    private Variable variable(long entityId, String name, VariableDeclaredType type) {
        VariableConfig config = VariableConfig.builder().entityId(entityId).fieldName("field").variableName(name).build();
        return Variable.builder().variableName(name).configs(ImmutableSortedSet.of(config)).systemSpec(mock(SystemSpec.class)).declaredType(type).build();
    }
}
