package cern.nxcals.api.backport.client.service;


import cern.nxcals.api.backport.domain.core.metadata.VariableList;
import cern.nxcals.api.backport.domain.core.metadata.VariableListSet;
import cern.nxcals.api.backport.domain.core.snapshot.Snapshot;
import cern.nxcals.api.backport.domain.core.snapshot.SnapshotCriteria;
import cern.nxcals.api.custom.domain.GroupType;
import cern.nxcals.api.domain.Group;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.Visibility;
import cern.nxcals.api.extraction.metadata.GroupService;
import cern.nxcals.common.utils.ReflectionUtils;
import com.google.common.collect.ImmutableSet;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class QuerySnapshotDataServiceImplTest {
    @Mock
    private GroupService groupService;

    private QuerySnapshotDataService service;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
        service = new QuerySnapshotDataServiceImpl(groupService);
    }

    @Test
    public void shouldFindVariableListByName() {
        String name = "my-group";

        Group existing = group(name);
        when(groupService.findOne(any())).thenReturn(Optional.of(existing));

        VariableList found = service.getVariableListWithName(name);

        assertEquals(found, existing);
    }

    @Test
    public void shouldFindVariableListByNameDescriptionAndType() {
        String name = "my-group";

        Group existing = group(name);
        when(groupService.findAll(any())).thenReturn(Collections.singleton(existing));

        VariableListSet found = service.getVariableListsOfUserWithNameLikeAndDescLike("owner", name, "desc");

        Assertions.assertEquals(1L, found.size());

        assertEquals(found.getVariableList(0), existing);
    }

    @Test
    public void shouldGetSnapshots() {
        Set<Group> existing = ImmutableSet.of(snapshot("my-snapshot-1"), snapshot("my-snapshot-2"));
        when(groupService.findAll(any())).thenReturn(existing);

        SnapshotCriteria criteria = spy(new SnapshotCriteria.SnapshotCriteriaBuilder("my-snapshot-%", null).build());

        List<Snapshot> found = service.getSnapshotsFor(criteria);

        assertThat(found).containsExactlyInAnyOrder(existing.stream().map(Snapshot::from).toArray(Snapshot[]::new));

        verify(criteria).getName();
        verify(criteria).getDescription();
        verify(criteria).getOwner();
        verify(criteria).isVisible();
    }

    @Test
    public void shouldGetSnapshotWithAttributes() {
        Group existing = snapshot("my-snapshot");
        when(groupService.findById(eq(existing.getId()))).thenReturn(Optional.of(existing));

        Snapshot found = service.getSnapshotWithAttributes(Snapshot.from(existing));

        assertThat(found).isEqualTo(Snapshot.from(existing));
    }

    @Test
    public void shouldGetSnapshotWithAttributesWhenNotFound() {
        Group existing = snapshot("my-snapshot");
        when(groupService.findById(eq(existing.getId()))).thenReturn(Optional.empty());

        Snapshot found = service.getSnapshotWithAttributes(Snapshot.from(existing));

        assertThat(found).isEqualTo(Snapshot.from(existing));
    }

    private Group group(String name) {
        return ReflectionUtils.builderInstance(Group.InnerBuilder.class)
                .id(500)
                .owner("group_owner")
                .label(GroupType.GROUP.toString())
                .name(name)
                .description("desc")
                .systemSpec(mock(SystemSpec.class))
                .visibility(Visibility.PROTECTED)
                .build();
    }

    private Group snapshot(String name) {
        return ReflectionUtils.builderInstance(Group.InnerBuilder.class)
                .id(500)
                .owner("group_owner")
                .label(GroupType.SNAPSHOT.toString())
                .name(name)
                .description("desc")
                .systemSpec(mock(SystemSpec.class))
                .visibility(Visibility.PROTECTED)
                .build();
    }

    private void assertEquals(VariableList found, Group existing) {
        Assertions.assertEquals(found.getUserName(), existing.getOwner());
        Assertions.assertEquals(found.getVariableListName(), existing.getName());
        Assertions.assertEquals(found.getDescription(), existing.getDescription());
        Assertions.assertEquals(found.isVisibleToPublic(), existing.isVisible());
        Assertions.assertEquals(found.getVariableListId(), existing.getId());
        Assertions.assertEquals(found.getSystem(), existing.getSystemSpec().getName());
    }
}