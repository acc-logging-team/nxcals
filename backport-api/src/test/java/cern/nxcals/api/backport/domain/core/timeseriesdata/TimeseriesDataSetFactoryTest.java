package cern.nxcals.api.backport.domain.core.timeseriesdata;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class TimeseriesDataSetFactoryTest {

    @Test
    public void newTimeseriesDataSet() {
        TimeseriesDataSet ds = TimeseriesDataSetFactory.newTimeseriesDataSet("name");
        assertEquals("name", ds.getVariableName());
        assertNull(ds.getUnit());
        assertNull(ds.getVariableDataType());
    }
}