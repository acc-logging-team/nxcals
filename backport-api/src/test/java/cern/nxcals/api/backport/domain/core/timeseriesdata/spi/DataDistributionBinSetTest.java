package cern.nxcals.api.backport.domain.core.timeseriesdata.spi;

import cern.nxcals.api.backport.domain.core.constants.VariableDataType;
import cern.nxcals.api.backport.domain.core.metadata.Variable;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.List;

import static cern.nxcals.common.SystemFields.NXC_EXTR_TIMESTAMP;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VALUE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;

@TestInstance(PER_CLASS)
public class DataDistributionBinSetTest {

    private static Variable VARIABLE = Variable.builder().variableName("TEST_VARIABLE").variableDataType(VariableDataType.NUMERIC).build();

    private SparkSession sparkSession = SparkSession.builder()
            .master("local")
            .appName("TimeseriesDataServiceImplTest")
            .getOrCreate();

    Dataset<Row> EMPTY_DATASET = sparkSession.emptyDataFrame();

    @Test
    public void shouldThrowWhenBinsNumberLessThanOne() {
        assertThrows(IllegalArgumentException.class, () -> new DataDistributionBinSet(VARIABLE, EMPTY_DATASET, 0));
    }

    @Test
    public void shouldCreateEmptyBinsForEmptyDataset() {
        DataDistributionBinSet dataDistributionBins = new DataDistributionBinSet(VARIABLE, EMPTY_DATASET, 10);
        assertEquals(10, dataDistributionBins.getBinsCount());
        assertEquals(VARIABLE, dataDistributionBins.getVariable());
        assertEquals(0, dataDistributionBins.getBin(0).getValuesCount());
    }

    @Test
    public void shouldCreateEmptyBinsForNotNumericVariableType(){
        Variable variable = Variable.builder().variableName("TEST_VARIABLE").variableDataType(VariableDataType.TEXT).build();
        Dataset <Row> dataset = ds(DataTypes.StringType, new Object[][] { { 1L, "text"} });
        DataDistributionBinSet dataDistributionBins = new DataDistributionBinSet(variable, dataset, 10);
        assertEquals(10, dataDistributionBins.getBinsCount());
        assertEquals(VARIABLE, dataDistributionBins.getVariable());
        assertEquals(0, dataDistributionBins.getBin(0).getValuesCount());
    }

    @ParameterizedTest
    @MethodSource("testData")
    public void shouldGetDataDistributionForBinsNumber(int binsNumber, Dataset<Row> dataset, double minValue,
            double maxValue, long valuesCount) {
        DataDistributionBinSet dataDistributionBins = new DataDistributionBinSet(VARIABLE, dataset, binsNumber);
        assertEquals(binsNumber, dataDistributionBins.getBinsCount());
        assertEquals(minValue, dataDistributionBins.getBin(0).getBottomLimit(), 0.01d);
        assertEquals(maxValue, dataDistributionBins.get(binsNumber - 1).getTopLimit(), 0.01d);
        for (int i = 0; i < binsNumber; i++) {
            assertEquals(valuesCount, dataDistributionBins.get(i).getValuesCount());
        }
    }

    public Object[][] testData() {
        //@formatter:off
        return new Object[][] {
                new Object[] {
                        2,
                        ds(DataTypes.IntegerType, new Object[][] { { 1L, 7 }, { 2L, 10 } }),
                        7d, 10d, 1L
                },
                new Object[] {
                        5,
                        ds(DataTypes.DoubleType,
                                new Object[][] { { 1L, 0d }, { 2L, 10d }, { 3L, 20d }, { 4L, 30d }, { 5L, 40d },
                                        { 6L, 50d }, { 7L, 60d }, { 8L, 70d }, { 9L, 80d }, { 10L, 90d } }),
                        0d, 90d, 2L
                },
                new Object[] {
                        3,
                        ds(DataTypes.LongType, new Object[][] { { 1L, 0L }, { 2L, 10L }, { 3L, 20L } }),
                        0d, 20d, 1L
                }
        };
        //@formatter:on
    }

    private Dataset<Row> ds(DataType valueDataType, Object[]... data) {
        List<Row> rows = new ArrayList<>(data.length);

        for (Object[] datum : data) {
            assertEquals(2, datum.length);
            Row row;
            if (valueDataType.typeName().equals("array")) {
                Row array = RowFactory.create(datum[1], new Integer[] { 1 });
                row = RowFactory.create(datum[0], array);
            } else {
                row = RowFactory.create(datum);
            }
            rows.add(row);
        }

        StructField aLong = new StructField(NXC_EXTR_TIMESTAMP.getValue(), DataTypes.LongType, true, Metadata.empty());
        StructField aType = new StructField(NXC_EXTR_VALUE.getValue(), valueDataType, true, Metadata.empty());
        StructType schema = new StructType(new StructField[] { aLong, aType });

        return sparkSession.createDataFrame(rows, schema);
    }

}
