package cern.nxcals.api.backport.domain.core.snapshot;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.DayOfWeek;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;

@TestInstance(PER_CLASS)
public class SimplePriorTimeTest
{
    @ParameterizedTest
    @MethodSource("previousWeekDayParams")
    public void shouldGetPreviousWeekDay(DateTime refDate, DayOfWeek previousWeekDay, DateTime expected) {
        assertThat(SimplePriorTime.previousWeekDay(refDate, previousWeekDay )).isEqualTo(expected);
    }

    public Object[][] previousWeekDayParams() {
        return new Object[][] {
                new Object[] { dateTimeFromString("10/10/2020 12:34:56"), DayOfWeek.THURSDAY, dateTimeFromString("08/10/2020 00:00:00")},
                new Object[] { dateTimeFromString("07/10/2020 12:34:56"), DayOfWeek.SATURDAY, dateTimeFromString("03/10/2020 00:00:00")},
                new Object[] { dateTimeFromString("07/10/2020 12:34:56"), DayOfWeek.WEDNESDAY, dateTimeFromString("07/10/2020 00:00:00")},
        };
    }

    private DateTime dateTimeFromString(String dateInString) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
        return DateTime.parse(dateInString, formatter);
    }
}
