package cern.nxcals.api.backport.domain.core.timeseriesdata;

import cern.nxcals.api.backport.domain.core.constants.LoggingTimeZone;
import cern.nxcals.api.utils.TimeUtils;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.sql.Timestamp;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class VariableStatisticsTest {
    private static final long COUNT = 100;
    private static final long MAX_VAL = 20;
    private static final long MIN_VAL = 10;
    private static final double AVG_VAL = 15.5d;
    private static final double STDDEV_VAL = 5.5d;
    private static final long MAX_STAMP = 2000;
    private static final long MIN_STAMP = 1000;
    private static final String NAME = "notEmpty";
    private static final String SYSTEM = "notEmpty";

    @Test
    public void shouldCreateStatistics() {
        VariableStatistics stats = stats(1);

        assertEquals(COUNT, stats.getValueCount());
        assertEquals(MAX_VAL, stats.getMaxValue().longValue());
        assertEquals(MIN_VAL, stats.getMinValue().longValue());
        assertEquals(AVG_VAL, stats.getAvgValue().doubleValue(), 0.01d);
        assertEquals(STDDEV_VAL, stats.getStandardDeviationValue().doubleValue(), 0.01d);
        assertEquals(MAX_STAMP, stats.getMaxTstamp().getNanos());
        assertEquals(MIN_STAMP, stats.getMinTstamp().getNanos());
        assertEquals((double) COUNT * 10E6, stats.getFrequencyHz(), 0.1d);
        assertEquals(NAME, stats.getVariableName());
    }

    @Test
    public void shouldSameStatistics() {
        VariableStatistics statsA = stats(1);
        VariableStatistics statsB = stats(1);

        VariableStatistics merged = statsA.mergeWith(statsB);

        assertEquals(COUNT + COUNT, merged.getValueCount());
        assertEquals(MAX_VAL, merged.getMaxValue().longValue());
        assertEquals(MIN_VAL, merged.getMinValue().longValue());
        assertEquals(AVG_VAL, merged.getAvgValue().doubleValue(), 0.01d);
        assertEquals(5.48d, merged.getStandardDeviationValue().doubleValue(), 0.01d);
        assertEquals(MAX_STAMP, merged.getMaxTstamp().getNanos());
        assertEquals(MIN_STAMP, merged.getMinTstamp().getNanos());
    }

    @Test
    public void shouldDifferentStatistics() {
        VariableStatistics statsA = stats(1);
        VariableStatistics statsB = stats(2);

        VariableStatistics merged = statsA.mergeWith(statsB);

        assertEquals(COUNT + COUNT * 2, merged.getValueCount());
        assertEquals(MAX_VAL * 2, merged.getMaxValue().longValue());
        assertEquals(MIN_VAL, merged.getMinValue().longValue());
        assertEquals(25.83d, merged.getAvgValue().doubleValue(), 0.01d);
        assertEquals(12.0d, merged.getStandardDeviationValue().doubleValue(), 0.01d);
        assertEquals(MAX_STAMP * 2, merged.getMaxTstamp().getNanos());
        assertEquals(MIN_STAMP, merged.getMinTstamp().getNanos());
    }

    @Test
    public void shouldCreateDateStrings() {
        VariableStatistics stats = stats(1000);
        assertEquals("1970-01-01 00:00:00.002", stats.getFormattedMaxTstamp(LoggingTimeZone.UTC_TIME));
        assertEquals("1970-01-01 00:00:00.001", stats.getFormattedMinTstamp(LoggingTimeZone.UTC_TIME));
    }

    private VariableStatistics stats(double scale) {
        return VariableStatistics.builder()
                .variableName(NAME)
                .system(SYSTEM)
                .valueCount((long) (COUNT * scale))
                .maxValue(BigDecimal.valueOf(MAX_VAL * scale))
                .minValue(BigDecimal.valueOf(MIN_VAL * scale))
                .avgValue(BigDecimal.valueOf(AVG_VAL * scale))
                .standardDeviationValue(BigDecimal.valueOf(STDDEV_VAL * scale))
                .maxTstamp(Timestamp.from(TimeUtils.getInstantFromNanos((long) (MAX_STAMP * scale))))
                .minTstamp(Timestamp.from(TimeUtils.getInstantFromNanos((long) (MIN_STAMP * scale))))
                .build();
    }
}