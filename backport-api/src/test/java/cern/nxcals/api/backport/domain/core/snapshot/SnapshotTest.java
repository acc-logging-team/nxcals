package cern.nxcals.api.backport.domain.core.snapshot;

import cern.nxcals.api.backport.domain.core.constants.DerivationSelection;
import cern.nxcals.api.backport.domain.core.constants.DynamicTimeDuration;
import cern.nxcals.api.backport.domain.core.constants.LoggingTimeInterval;
import cern.nxcals.api.backport.domain.core.constants.LoggingTimeZone;
import cern.nxcals.api.backport.domain.core.constants.ScaleAlgorithm;
import cern.nxcals.api.backport.domain.core.constants.TimescalingProperties;
import cern.nxcals.api.custom.domain.GroupType;
import cern.nxcals.api.domain.Group;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.api.domain.Visibility;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.common.utils.ReflectionUtils;
import com.google.common.collect.ImmutableSortedSet;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

public class SnapshotTest {

    @Test
    public void shouldDeserializeGroup() {
        TimescalingProperties propsA = TimescalingProperties.getTimeScaleProperties(ScaleAlgorithm.MIN, (short) 2, LoggingTimeInterval.SECOND);

        Group group = ReflectionUtils.builderInstance(Group.InnerBuilder.class)
                .id(500)
                .owner("group_owner")
                .label(GroupType.SNAPSHOT.toString())
                .name("snapshot_name")
                .description("desc")
                .systemSpec(mock(SystemSpec.class))
                .visibility(Visibility.PROTECTED)
                .property(SnapshotPropertyName.getPriorTime.toString(), SimplePriorTime.START_OF_HOUR.description())
                .property(SnapshotPropertyName.getEndTime.toString(), TimeUtils.getStringFromInstant(Instant.ofEpochSecond(10)))
                .property(SnapshotPropertyName.getStartTime.toString(), TimeUtils.getStringFromInstant(Instant.ofEpochSecond(20)))
                .property(SnapshotPropertyName.getDerivationTime.toString(), TimeUtils.getStringFromInstant(Instant.ofEpochSecond(30)))
                .property(SnapshotPropertyName.getSelectionOutput.toString(), "AA")
                .property(SnapshotPropertyName.isMetaDataIncluded.toString(), Boolean.toString(true))
                .property(SnapshotPropertyName.getFileFormat.toString(), "AB")
                .property(SnapshotPropertyName.getDeviceName.toString(), "AC")
                .property(SnapshotPropertyName.getTimeZone.toString(), LoggingTimeZone.UTC_TIME.toString())
                .property(SnapshotPropertyName.getDerivationSelection.toString(), DerivationSelection.BEST_NOW.toString())
                .property(SnapshotPropertyName.isGroupByTimestamp.toString(), Boolean.toString(true))
                .property(SnapshotPropertyName.isMultiFilesActivated.toString(), Boolean.toString(true))
                .property(SnapshotPropertyName.getChartType.toString(), "AD")
                .property(SnapshotPropertyName.isSelectableTimestampsIncluded.toString(), Boolean.toString(true))
                .property(SnapshotPropertyName.isDistributionChartIncluded.toString(), Boolean.toString(true))
                .property(SnapshotPropertyName.isLastValueAligned.toString(), Boolean.toString(true))
                .property(SnapshotPropertyName.isLastValueSearchedIfNoDataFound.toString(), Boolean.toString(true))
                .property(SnapshotPropertyName.isAlignToEnd.toString(), Boolean.toString(true))
                .property(SnapshotPropertyName.isAlignToWindow.toString(), Boolean.toString(true))
                .property(SnapshotPropertyName.isAlignToStart.toString(), Boolean.toString(true))
                .property(SnapshotPropertyName.isEndTimeDynamic.toString(), Boolean.toString(true))
                .property(SnapshotPropertyName.getDynamicDuration.toString(), String.valueOf(40))
                .property(SnapshotPropertyName.getNoOfIntervalsForMultiFiles.toString(), String.valueOf(50))
                .property(SnapshotPropertyName.getTimeIntervalTypeForMultiFiles.toString(),
                        LoggingTimeInterval.DAY.toString())
                .property(SnapshotPropertyName.getCorrelationSettingScale.toString(), String.valueOf(60))
                .property(SnapshotPropertyName.getTimescalingProperties.toString(), propsA.toDBString())
                .property(SnapshotPropertyName.getCorrelationSettingFreq.toString(),
                        LoggingTimeInterval.HOUR.toString())
                .property(SnapshotPropertyName.getPropertyName.toString(), "AE")
                .property(SnapshotPropertyName.getTime.toString(), DynamicTimeDuration.DAYS.toString())
                .build();

        Map<String, Set<Variable>> variables = new HashMap<>();
        variables.put(SnapshotPropertyName.getSelectedVariables.toString(), variableSet("A", "B", "C"));
        variables.put(SnapshotPropertyName.getFundamentalFilters.toString(), variableSet("D", "E", "F"));
        variables.put(SnapshotPropertyName.getDrivingVariable.toString(), Collections.singleton(variable("M")));
        variables.put(SnapshotPropertyName.getXAxisVariable.toString(), Collections.singleton(variable("N")));

        final Snapshot snapshot = Snapshot.from(group, variables);

        assertThat(snapshot.getId()).isEqualTo(500);
        assertThat(snapshot.isVisibleToPublic()).isEqualTo(false);
        assertThat(snapshot.getDescription()).isEqualTo("desc");
        assertThat(snapshot.getName()).isEqualTo("snapshot_name");
        assertThat(snapshot.getOwner()).isEqualTo("group_owner");

        assertThat((Map<String, ?>) snapshot.getProperties().getSelectedVariables()).containsKeys("A", "B", "C");
        assertThat((Map<String, ?>) snapshot.getProperties().getFundamentalFilters()).containsKeys("D", "E", "F");
        assertThat(snapshot.getProperties().getDrivingVariable().getVariableName()).isEqualTo("M");
        assertThat(snapshot.getProperties().getXAxisVariable().getVariableName()).isEqualTo("N");
        assertThat(snapshot.getProperties().getPriorTime()).isEqualTo(SimplePriorTime.START_OF_HOUR);
        assertThat(snapshot.getProperties().getEndTime()).isEqualTo(TimeUtils.getTimestampFromInstant(Instant.ofEpochSecond(10)));
        assertThat(snapshot.getProperties().getStartTime()).isEqualTo(TimeUtils.getTimestampFromInstant(Instant.ofEpochSecond(20)));
        assertThat(snapshot.getProperties().getDerivationTime()).isEqualTo(TimeUtils.getTimestampFromInstant(Instant.ofEpochSecond(30)));
        assertThat(snapshot.getProperties().getSelectionOutput()).isEqualTo("AA");
        assertThat(snapshot.getProperties().isMetaDataIncluded()).isEqualTo(true);
        assertThat(snapshot.getProperties().getFileFormat()).isEqualTo("AB");
        assertThat(snapshot.getProperties().getDeviceName()).isEqualTo("AC");
        assertThat(snapshot.getProperties().getTimeZone()).isEqualTo(LoggingTimeZone.UTC_TIME);
        assertThat(snapshot.getProperties().getDerivationSelection()).isEqualTo(DerivationSelection.BEST_NOW);
        assertThat(snapshot.getProperties().isGroupByTimestamp()).isEqualTo(true);
        assertThat(snapshot.getProperties().isMultiFilesActivated()).isEqualTo(true);
        assertThat(snapshot.getProperties().getChartType()).isEqualTo("AD");
        assertThat(snapshot.getProperties().isSelectableTimestampsIncluded()).isEqualTo(true);
        assertThat(snapshot.getProperties().isDistributionChartIncluded()).isEqualTo(true);
        assertThat(snapshot.getProperties().isLastValueAligned()).isEqualTo(true);
        assertThat(snapshot.getProperties().isLastValueSearchedIfNoDataFound()).isEqualTo(true);
        assertThat(snapshot.getProperties().isAlignToEnd()).isEqualTo(true);
        assertThat(snapshot.getProperties().isAlignToWindow()).isEqualTo(true);
        assertThat(snapshot.getProperties().isAlignToStart()).isEqualTo(true);
        assertThat(snapshot.getProperties().isEndTimeDynamic()).isEqualTo(true);
        assertThat(snapshot.getProperties().getDynamicDuration()).isEqualTo(40);
        assertThat(snapshot.getProperties().getNoOfIntervalsForMultiFiles()).isEqualTo(50);
        assertThat(snapshot.getProperties().getTimeIntervalTypeForMultiFiles()).isEqualTo(LoggingTimeInterval.DAY);
        assertThat(snapshot.getProperties().getCorrelationSettingScale()).isEqualTo(60);
        assertThat(snapshot.getProperties().getTimeScalingProperties()).isEqualTo(propsA);
        assertThat(snapshot.getProperties().getCorrelationSettingFreq()).isEqualTo(LoggingTimeInterval.HOUR);
        assertThat(snapshot.getProperties().getPropertyName()).isEqualTo("AE");
        assertThat(snapshot.getProperties().getTime()).isEqualTo(DynamicTimeDuration.DAYS);
    }

    private Set<Variable> variableSet(String... name) {
        return Arrays.stream(name).map(this::variable).collect(Collectors.toSet());
    }

    private cern.nxcals.api.domain.Variable variable(String name) {
        VariableConfig config = VariableConfig.builder().entityId(100).fieldName("field").build();
        return cern.nxcals.api.domain.Variable.builder()
                .variableName(name)
                .configs(ImmutableSortedSet.of(config))
                .systemSpec(mock(SystemSpec.class)).build();
    }
}
