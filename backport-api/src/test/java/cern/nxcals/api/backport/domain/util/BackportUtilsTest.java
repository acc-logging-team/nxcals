package cern.nxcals.api.backport.domain.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class BackportUtilsTest {

    // test nxcals to cals node path convertion

    @Test
    public void testConvertToCalsNodePathIfInputIsNullShouldDoNothing() {
        String nodePath = null;
        String calsNodePath = BackportUtils.toCalsHierarchyNodePath(nodePath);
        assertNull(calsNodePath);
    }

    @Test
    public void testConvertToCalsNodePathIfInputIsEmptyShouldDoNothing() {
        String nodePath = "";
        String calsNodePath = BackportUtils.toCalsHierarchyNodePath(nodePath);
        assertEquals("", calsNodePath);
    }

    @Test
    public void testConvertToCalsNodePathIfInputAlreadyConvertedRootShouldDoNothing() {
        String nodePath = "ROOT";
        String calsNodePath = BackportUtils.toCalsHierarchyNodePath(nodePath);
        assertEquals("ROOT", calsNodePath);
    }

    @Test
    public void testConvertToCalsNodePathIfInputAlreadyConvertedShouldDoNothing() {
        String nodePath = "ROOT->TEST->SUPER";
        String calsNodePath = BackportUtils.toCalsHierarchyNodePath(nodePath);
        assertEquals(nodePath, calsNodePath);
    }

    @Test
    public void testConvertToCalsNodePathIfNodeIsRootShouldApplyAction() {
        String nodePath = "/ROOT";
        String calsNodePath = BackportUtils.toCalsHierarchyNodePath(nodePath);
        assertEquals("ROOT", calsNodePath);
    }

    @Test
    public void testConvertToCalsNodePathShouldApplyAction() {
        String nodePath = "/ROOT/TEST/SUPER";
        String calsNodePath = BackportUtils.toCalsHierarchyNodePath(nodePath);
        assertEquals("ROOT->TEST->SUPER", calsNodePath);
    }

    // test cals to nxcals node path convertion

    @Test
    public void testConvertToNxcalsNodePathIfInputIsNullShouldDoNothing() {
        String nodePath = null;
        String nxCalsNodePath = BackportUtils.toNxcalsHierarchyNodePath(nodePath);
        assertNull(nxCalsNodePath);
    }

    @Test
    public void testConvertToNxcalsNodePathIfInputIsEmptyShouldDoNothing() {
        String nodePath = "";
        String nxCalsNodePath = BackportUtils.toNxcalsHierarchyNodePath(nodePath);
        assertEquals("", nxCalsNodePath);
    }

    @Test
    public void testConvertToNxcalsNodePathIfInputAlreadyConvertedRootShouldDoNothing() {
        String nodePath = "/ROOT";
        String nxcalsNodePath = BackportUtils.toNxcalsHierarchyNodePath(nodePath);
        assertEquals(nodePath, nxcalsNodePath);
    }

    @Test
    public void testConvertToNxcalsNodePathIfInputAlreadyConvertedShouldDoNothing() {
        String nodePath = "/ROOT/TEST/SUPER";
        String nxcalsNodePath = BackportUtils.toNxcalsHierarchyNodePath(nodePath);
        assertEquals(nodePath, nxcalsNodePath);
    }

    @Test
    public void testConvertToNxcalsNodePathIfNodeIsRootShouldApplyAction() {
        String nodePath = "ROOT";
        String nxCalsNodePath = BackportUtils.toNxcalsHierarchyNodePath(nodePath);
        assertEquals("/ROOT", nxCalsNodePath);
    }

    @Test
    public void testConvertToNxcalsNodePathShouldApplyAction() {
        String nodePath = "ROOT->TEST->SUPER";
        String nxCalsNodePath = BackportUtils.toNxcalsHierarchyNodePath(nodePath);
        assertEquals("/ROOT/TEST/SUPER", nxCalsNodePath);
    }

}
