package cern.nxcals.api.backport.domain.core.filldata;

import cern.nxcals.api.custom.domain.Fill;
import cern.nxcals.api.domain.TimeWindow;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

class LHCFillTest {

    @Test
    void shouldCreateFill() {
        Fill fill = new Fill(1, TimeWindow.between(Instant.now(), null), new ArrayList<>());
        LHCFill newFill = LHCFill.from(fill);

        assertNotNull(newFill.getStartTime());
        assertNull(newFill.getEndTime());

    }
}