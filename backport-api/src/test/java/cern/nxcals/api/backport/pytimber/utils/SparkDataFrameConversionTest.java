package cern.nxcals.api.backport.pytimber.utils;

import com.google.common.collect.Lists;
import org.apache.commons.lang.ClassUtils;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;

import static cern.nxcals.common.SystemFields.NXC_EXTR_TIMESTAMP;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VALUE;
import static cern.nxcals.common.avro.SchemaConstants.ARRAY_DIMENSIONS_FIELD_NAME;
import static cern.nxcals.common.avro.SchemaConstants.ARRAY_ELEMENTS_FIELD_NAME;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class SparkDataFrameConversionTest {


    private final SparkSession sparkSession = SparkSession.builder()
            .master("local")
            .appName("testExtractVariableData")
            .getOrCreate();

    @ParameterizedTest
    @MethodSource("parametersForTestExtractVariableData")
    public void testExtractVariableData(String typeName, Dataset<Row> dataset) {
        SortedMap<String, Object> val = SparkDataFrameConversions.extractAllColumns(dataset);

        Object colOfPrimitives = val.get("nxcals_value");
        assertTrue(colOfPrimitives.getClass().isArray());

        Object element = Array.get(colOfPrimitives, 0);
        if (element.getClass().isArray()) {
            assertEquals(typeName, element.getClass().getSimpleName());
        } else {
            if (element instanceof Number || element instanceof Boolean) {
                // element is a Wrapped Type even if the primitiveCol actually contains primitives
                assertEquals(typeName, ClassUtils.wrapperToPrimitive(element.getClass()).getSimpleName());
            } else {
                assertEquals(typeName, element.getClass().getSimpleName());
            }
        }
    }


    private Object[] parametersForTestExtractVariableData() {
        return new Object[]{
                new Object[]{"boolean",
                        ds(DataTypes.BooleanType, new Object[][]{{1L, true}, {2L, false},  {3L, false}})
                },
                new Object[]{"long",
                        ds(DataTypes.LongType, new Object[][]{{1L, 1L}, {2L, 2L}})
                },
                new Object[]{"long[]",
                        ds(arrayType(DataTypes.LongType), new Object[][]{{1L, new Object[]{-1L, 2L, -3L}}, {2L, new Object[]{4L, -5L, 6L}}})
                },
                new Object[]{"double",
                        ds(DataTypes.DoubleType, new Object[][]{{1L, 1d}, {2L, 2d}}),
                },
                new Object[]{"double[]",
                        ds(arrayType(DataTypes.DoubleType), new Object[][]{{1L, new Object[]{-1d, 2d, -3d}}, {2L, new Object[]{4d, -5d, 6d}}})
                },
                new Object[]{"String",
                        ds(DataTypes.StringType, new Object[][]{{1L, "a"}, {2L, "b"}}),
                }
        };
    }

    private StructType arrayType(DataType eltType) {
        StructField elts = new StructField(ARRAY_ELEMENTS_FIELD_NAME, DataTypes.createArrayType(eltType), true,
                Metadata.empty());
        StructField dims = new StructField(ARRAY_DIMENSIONS_FIELD_NAME,
                DataTypes.createArrayType(DataTypes.IntegerType), true, Metadata.empty());

        return DataTypes.createStructType(Lists.newArrayList(elts, dims));

    }

    private Dataset<Row> ds(DataType valueDataType, Object[]... data) {
        List<Row> rows = new ArrayList<>(data.length);

        for (Object[] datum : data) {
            assertEquals(2, datum.length);
            Row row;
            if (valueDataType.simpleString().contains("array")) {
                Row array = RowFactory.create(datum[1], new Integer[] { 1 });
                row = RowFactory.create(datum[0], array);
            } else {
                row = RowFactory.create(datum);
            }
            rows.add(row);
        }

        StructField aLong = new StructField(NXC_EXTR_TIMESTAMP.getValue(), DataTypes.LongType, true, Metadata.empty());
        StructField aType = new StructField(NXC_EXTR_VALUE.getValue(), valueDataType, true, Metadata.empty());
        StructType schema = new StructType(new StructField[] { aLong, aType });

        return sparkSession.createDataFrame(rows, schema);

    }
}
