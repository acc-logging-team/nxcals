package cern.nxcals.api.backport.client.service;

import cern.nxcals.api.backport.domain.core.constants.BeamModeValue;
import cern.nxcals.api.backport.domain.core.filldata.LHCFill;
import cern.nxcals.api.backport.domain.core.filldata.LHCFillSet;
import cern.nxcals.api.custom.domain.BeamMode;
import cern.nxcals.api.custom.domain.Fill;
import cern.nxcals.api.custom.service.FillService;
import cern.nxcals.api.domain.TimeWindow;
import com.google.common.collect.ImmutableList;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

public class LHCFillDataServiceImplTest {
    @Mock
    private FillService fillService;
    private List<Fill> list;
    private Instant t1 = Instant.ofEpochMilli(100);
    private Instant t2 = Instant.ofEpochMilli(200);
    private Instant t3 = Instant.ofEpochMilli(300);
    private LHCFillDataServiceImpl service;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);

        list = ImmutableList.of(
                new Fill(0, TimeWindow.between(t1, t2), ImmutableList.of(new BeamMode(TimeWindow.between(t1, t2), "BEAMDUMP"))),
                new Fill(1, TimeWindow.between(t2, t3), ImmutableList.of(new BeamMode(TimeWindow.between(t2, t3), "RAMPDOWN")))
        );

        when(fillService.findFills(eq(t1), eq(t2))).thenReturn(list);
        service = new LHCFillDataServiceImpl(fillService);
    }

    @Test
    public void shouldFindData() {
        LHCFillSet result = service.getLHCFillsInTimeWindow(new Timestamp(t1.toEpochMilli()), new Timestamp(t2.toEpochMilli()));

        assertEquals(result.size(), list.size());
        for (int i = 0; i < result.size(); i++ ) {
            assertEqual(result.getLHCFill(i), list.get(i));
        }
    }

    @Test
    public void shouldPairFillsAndBeamModes() {
        LHCFillSet result = service.getLHCFillsInTimeWindow(new Timestamp(t1.toEpochMilli()), new Timestamp(t2.toEpochMilli()));

        assertEquals(result.size(), list.size());
        for (int i = 0; i < result.size(); i++ ) {
            assertEqual(result.getLHCFill(i), list.get(i));
        }
    }

    @Test
    public void shouldReturnFillsFiltered() {
        LHCFillSet result = service.getLHCFillsInTimeWindowContainingBeamModes(new Timestamp(t1.toEpochMilli()), new Timestamp(t2.toEpochMilli()), new BeamModeValue[] {BeamModeValue.BEAMDUMP, BeamModeValue.CIRCDUMP});

        assertEquals(1, result.size());
        assertEqual(result.getLHCFill(0), list.get(0));
    }

    @Test
    public void shouldPairFillsAndBeamModesFiltered() {
        LHCFillSet result = service
                .getLHCFillsAndBeamModesInTimeWindowContainingBeamModes(new Timestamp(t1.toEpochMilli()),
                        new Timestamp(t2.toEpochMilli()),
                        new BeamModeValue[] { BeamModeValue.BEAMDUMP, BeamModeValue.CIRCDUMP });

        assertEquals(1, result.size());
        assertEqual(result.getLHCFill(0), list.get(0));
    }

    @Test
    public void shouldGetNullOnGetLastFillWhenNoData() {
        when(fillService.getLastCompleted()).thenReturn(Optional.empty());

        LHCFill result = service.getLastCompletedLHCFillAndBeamModes();
        assertNull(result);
    }

    @Test
    public void shouldGetLastFill() {
        when(fillService.getLastCompleted()).thenReturn(Optional.of(list.get(list.size() - 1)));

        LHCFill result = service.getLastCompletedLHCFillAndBeamModes();
        assertEqual(result, list.get(list.size() - 1));
    }

    @Test
    public void shouldNotLastFillIfNoneAvailable() {
        when(fillService.findFill(anyInt())).thenReturn(Optional.empty());
        assertNull(service.getLastCompletedLHCFillAndBeamModes());
    }

    @Test
    public void shouldGetFillByNumber() {
        when(fillService.findFill(eq(100))).thenReturn(Optional.of(list.get(0)));

        LHCFill result = service.getLHCFillAndBeamModesByFillNumber(100);
        assertEqual(result, list.get(0));
    }

    @Test
    public void shouldNotGetFillByNumberIfNoneAvailable() {
        when(fillService.findFill(anyInt())).thenReturn(Optional.empty());
        assertNull(service.getLHCFillAndBeamModesByFillNumber(100));
    }

    private void assertEqual(LHCFill lhcFill, Fill fill) {
        assertEquals(lhcFill.getFillNumber(), fill.getNumber());
        assertEquals(lhcFill.getStartTime().getTime(), fill.getValidity().getStartTime().toEpochMilli());
        assertEquals(lhcFill.getEndTime().getTime(), fill.getValidity().getEndTime().toEpochMilli());
        assertEquals(lhcFill.getBeamModes().size(), fill.getBeamModes().size());
        for (int j = 0; j < lhcFill.getBeamModes().size(); j++) {
            assertEqual(lhcFill.getBeamModes().get(j), fill.getBeamModes().get(j));
        }
    }

    private void assertEqual(cern.nxcals.api.backport.domain.core.filldata.BeamMode lhcMode, BeamMode mode) {
        assertEquals(lhcMode.getStartTime().getTime(), mode.getValidity().getStartTime().toEpochMilli());
        assertEquals(lhcMode.getEndTime().getTime(), mode.getValidity().getEndTime().toEpochMilli());
        assertEquals(lhcMode.getBeamModeValue().toString(), mode.getBeamModeValue());
    }
}
