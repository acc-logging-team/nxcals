package cern.nxcals.api.backport.domain.core.snapshot;

import cern.nxcals.api.backport.domain.core.TimeWindow;
import cern.nxcals.api.backport.domain.core.TimeWindowSet;
import cern.nxcals.api.backport.domain.core.constants.BeamModeValue;
import cern.nxcals.api.backport.domain.core.constants.DerivationSelection;
import cern.nxcals.api.backport.domain.core.constants.DynamicTimeDuration;
import cern.nxcals.api.backport.domain.core.constants.LoggingTimeInterval;
import cern.nxcals.api.backport.domain.core.constants.LoggingTimeZone;
import cern.nxcals.api.backport.domain.core.constants.ScaleAlgorithm;
import cern.nxcals.api.backport.domain.core.constants.TimescalingProperties;
import cern.nxcals.api.backport.domain.core.metadata.Variable;
import cern.nxcals.api.backport.domain.core.metadata.VariableSet;
import cern.nxcals.api.utils.TimeUtils;
import com.google.common.collect.Iterables;
import org.joda.time.DateTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;


public class SnapshotPropertiesTest {

    private SnapshotProperties properties;

    @BeforeEach
    public void init() {
        properties = SnapshotProperties.empty();
    }

    @Test
    public void getAttributes4Display() {
        TimescalingProperties propsA = TimescalingProperties.getTimeScaleProperties(ScaleAlgorithm.MIN, (short) 2, LoggingTimeInterval.SECOND);

        properties.setSelectedVariables(variableSet("A", "B", "C"));
        properties.setFundamentalFilters(variableSet("D", "E", "F"));
        properties.setDrivingVariable(variable("M"));
        properties.setXAxisVariable(variable("N"));
        properties.setPriorTime(SimplePriorTime.START_OF_HOUR);
        properties.setEndTime(Timestamp.from(Instant.ofEpochSecond(10)));
        properties.setStartTime(Timestamp.from(Instant.ofEpochSecond(20)));
        properties.setDerivationTime(Timestamp.from(Instant.ofEpochSecond(30)));
        properties.setSelectionOutput("AA");
        properties.setMetaDataIncluded(true);
        properties.setFileFormat("AB");
        properties.setDeviceName("AC");
        properties.setTimeZone(LoggingTimeZone.UTC_TIME);
        properties.setDerivationSelection(DerivationSelection.BEST_NOW);
        properties.setGroupByTimestamp(true);
        properties.setMultiFilesActivated(true);
        properties.setChartType("AD");
        properties.setSelectableTimestampsIncluded(true);
        properties.setDistributionChartIncluded(true);
        properties.setLastValueAligned(true);
        properties.setLastValueSearchedIfNoDataFound(true);
        properties.setAlignToEnd(true);
        properties.setAlignToWindow(true);
        properties.setAlignToStart(true);
        properties.setEndTimeDynamic(true);
        properties.setDynamicDuration(40);
        properties.setNoOfIntervalsForMultiFiles(50);
        properties.setTimeIntervalTypeForMultiFiles(LoggingTimeInterval.DAY);
        properties.setCorrelationSettingScale(60);
        properties.setTimeScalingProperties(propsA);
        properties.setCorrelationSettingFreq(LoggingTimeInterval.HOUR);
        properties.setPropertyName("AE");
        properties.setTime(DynamicTimeDuration.DAYS);

        Map<String, String> attributes = properties.getAttributes4Display();

        assertThat(attributes.get(SnapshotPropertyName.getSelectedVariables.getDisplayName())).isEqualTo("[A, B, C]");
        assertThat(attributes.get(SnapshotPropertyName.getFundamentalFilters.getDisplayName())).isEqualTo("[D, E, F]");
        assertThat(attributes.get(SnapshotPropertyName.getDrivingVariable.getDisplayName())).isEqualTo("M");
        assertThat(attributes.get(SnapshotPropertyName.getXAxisVariable.getDisplayName())).isEqualTo("N");
        assertThat(attributes.get(SnapshotPropertyName.getPriorTime.getDisplayName())).isEqualTo(SimplePriorTime.START_OF_HOUR.toString());
        assertThat(attributes.get(SnapshotPropertyName.getEndTime.getDisplayName())).isEqualTo(TimeUtils.getStringFromInstant(Instant.ofEpochSecond(10)));
        assertThat(attributes.get(SnapshotPropertyName.getStartTime.getDisplayName())).isEqualTo(TimeUtils.getStringFromInstant(Instant.ofEpochSecond(20)));
        assertThat(attributes.get(SnapshotPropertyName.getDerivationTime.getDisplayName())).isEqualTo(TimeUtils.getStringFromInstant(Instant.ofEpochSecond(30)));
        assertThat(attributes.get(SnapshotPropertyName.getSelectionOutput.getDisplayName())).isEqualTo("AA");
        assertThat(attributes.get(SnapshotPropertyName.isMetaDataIncluded.getDisplayName())).isEqualTo("true");
        assertThat(attributes.get(SnapshotPropertyName.getFileFormat.getDisplayName())).isEqualTo("AB");
        assertThat(attributes.get(SnapshotPropertyName.getDeviceName.getDisplayName())).isEqualTo("AC");
        assertThat(attributes.get(SnapshotPropertyName.getTimeZone.getDisplayName())).isEqualTo(LoggingTimeZone.UTC_TIME.toString());
        assertThat(attributes.get(SnapshotPropertyName.getDerivationSelection.getDisplayName())).isEqualTo(DerivationSelection.BEST_NOW.toString());
        assertThat(attributes.get(SnapshotPropertyName.isGroupByTimestamp.getDisplayName())).isEqualTo("true");
        assertThat(attributes.get(SnapshotPropertyName.isMultiFilesActivated.getDisplayName())).isEqualTo("true");
        assertThat(attributes.get(SnapshotPropertyName.getChartType.getDisplayName())).isEqualTo("AD");
        assertThat(attributes.get(SnapshotPropertyName.isSelectableTimestampsIncluded.getDisplayName())).isEqualTo("true");
        assertThat(attributes.get(SnapshotPropertyName.isDistributionChartIncluded.getDisplayName())).isEqualTo("true");
        assertThat(attributes.get(SnapshotPropertyName.isLastValueAligned.getDisplayName())).isEqualTo("true");
        assertThat(attributes.get(SnapshotPropertyName.isLastValueSearchedIfNoDataFound.getDisplayName())).isEqualTo("true");
        assertThat(attributes.get(SnapshotPropertyName.isAlignToEnd.getDisplayName())).isEqualTo("true");
        assertThat(attributes.get(SnapshotPropertyName.isAlignToWindow.getDisplayName())).isEqualTo("true");
        assertThat(attributes.get(SnapshotPropertyName.isAlignToStart.getDisplayName())).isEqualTo("true");
        assertThat(attributes.get(SnapshotPropertyName.isEndTimeDynamic.getDisplayName())).isEqualTo("true");
        assertThat(attributes.get(SnapshotPropertyName.getDynamicDuration.getDisplayName())).isEqualTo("40");
        assertThat(attributes.get(SnapshotPropertyName.getNoOfIntervalsForMultiFiles.getDisplayName())).isEqualTo("50");
        assertThat(attributes.get(SnapshotPropertyName.getTimeIntervalTypeForMultiFiles.getDisplayName())).isEqualTo(LoggingTimeInterval.DAY.toString());
        assertThat(attributes.get(SnapshotPropertyName.getCorrelationSettingScale.getDisplayName())).isEqualTo("60");
        assertThat(attributes.get(SnapshotPropertyName.getTimescalingProperties.getDisplayName())).isEqualTo(propsA.toDBString());
        assertThat(attributes.get(SnapshotPropertyName.getCorrelationSettingFreq.getDisplayName())).isEqualTo(LoggingTimeInterval.HOUR.toString());
        assertThat(attributes.get(SnapshotPropertyName.getPropertyName.getDisplayName())).isEqualTo("AE");
        assertThat(attributes.get(SnapshotPropertyName.getTime.getDisplayName())).isEqualTo(DynamicTimeDuration.DAYS.toString());
    }

    @Test
    public void getAttributesFalse() {
        TimescalingProperties propsA = TimescalingProperties.getTimeScaleProperties(ScaleAlgorithm.MIN, (short) 2, LoggingTimeInterval.SECOND);

        properties.setSelectedVariables(variableSet("A", "B", "C"));
        properties.setFundamentalFilters(variableSet("D", "E", "F"));
        properties.setDrivingVariable(variable("M"));
        properties.setXAxisVariable(variable("N"));
        properties.setPriorTime(SimplePriorTime.START_OF_HOUR);
        properties.setEndTime(Timestamp.from(Instant.ofEpochSecond(10)));
        properties.setStartTime(Timestamp.from(Instant.ofEpochSecond(20)));
        properties.setDerivationTime(Timestamp.from(Instant.ofEpochSecond(30)));
        properties.setSelectionOutput("AA");
        properties.setMetaDataIncluded(true);
        properties.setFileFormat("AB");
        properties.setDeviceName("AC");
        properties.setTimeZone(LoggingTimeZone.UTC_TIME);
        properties.setDerivationSelection(DerivationSelection.BEST_NOW);
        properties.setGroupByTimestamp(true);
        properties.setMultiFilesActivated(true);
        properties.setChartType("AD");
        properties.setSelectableTimestampsIncluded(true);
        properties.setDistributionChartIncluded(true);
        properties.setLastValueAligned(true);
        properties.setLastValueSearchedIfNoDataFound(true);
        properties.setAlignToEnd(true);
        properties.setAlignToWindow(true);
        properties.setAlignToStart(true);
        properties.setEndTimeDynamic(true);
        properties.setDynamicDuration(40);
        properties.setNoOfIntervalsForMultiFiles(50);
        properties.setTimeIntervalTypeForMultiFiles(LoggingTimeInterval.DAY);
        properties.setCorrelationSettingScale(60);
        properties.setTimeScalingProperties(propsA);
        properties.setCorrelationSettingFreq(LoggingTimeInterval.HOUR);
        properties.setPropertyName("AE");
        properties.setTime(DynamicTimeDuration.DAYS);

        Map<SnapshotPropertyName, String> attributes = properties.getAttributes(false);

        assertThat(attributes.get(SnapshotPropertyName.getSelectedVariables)).isEqualTo("3");
        assertThat(attributes.get(SnapshotPropertyName.getFundamentalFilters)).isEqualTo("3");
        assertThat(attributes.get(SnapshotPropertyName.getDrivingVariable)).isEqualTo("M");
        assertThat(attributes.get(SnapshotPropertyName.getXAxisVariable)).isEqualTo("N");
        assertThat(attributes.get(SnapshotPropertyName.getPriorTime)).isEqualTo(SimplePriorTime.START_OF_HOUR.toString());
        assertThat(attributes.get(SnapshotPropertyName.getEndTime)).isEqualTo(TimeUtils.getStringFromInstant(Instant.ofEpochSecond(10)));
        assertThat(attributes.get(SnapshotPropertyName.getStartTime)).isEqualTo(TimeUtils.getStringFromInstant(Instant.ofEpochSecond(20)));
        assertThat(attributes.get(SnapshotPropertyName.getDerivationTime)).isEqualTo(TimeUtils.getStringFromInstant(Instant.ofEpochSecond(30)));
        assertThat(attributes.get(SnapshotPropertyName.getSelectionOutput)).isEqualTo("AA");
        assertThat(attributes.get(SnapshotPropertyName.isMetaDataIncluded)).isEqualTo("true");
        assertThat(attributes.get(SnapshotPropertyName.getFileFormat)).isEqualTo("AB");
        assertThat(attributes.get(SnapshotPropertyName.getDeviceName)).isEqualTo("AC");
        assertThat(attributes.get(SnapshotPropertyName.getTimeZone)).isEqualTo(LoggingTimeZone.UTC_TIME.toString());
        assertThat(attributes.get(SnapshotPropertyName.getDerivationSelection)).isEqualTo(DerivationSelection.BEST_NOW.toString());
        assertThat(attributes.get(SnapshotPropertyName.isGroupByTimestamp)).isEqualTo("true");
        assertThat(attributes.get(SnapshotPropertyName.isMultiFilesActivated)).isEqualTo("true");
        assertThat(attributes.get(SnapshotPropertyName.getChartType)).isEqualTo("AD");
        assertThat(attributes.get(SnapshotPropertyName.isSelectableTimestampsIncluded)).isEqualTo("true");
        assertThat(attributes.get(SnapshotPropertyName.isDistributionChartIncluded)).isEqualTo("true");
        assertThat(attributes.get(SnapshotPropertyName.isLastValueAligned)).isEqualTo("true");
        assertThat(attributes.get(SnapshotPropertyName.isLastValueSearchedIfNoDataFound)).isEqualTo("true");
        assertThat(attributes.get(SnapshotPropertyName.isAlignToEnd)).isEqualTo("true");
        assertThat(attributes.get(SnapshotPropertyName.isAlignToWindow)).isEqualTo("true");
        assertThat(attributes.get(SnapshotPropertyName.isAlignToStart)).isEqualTo("true");
        assertThat(attributes.get(SnapshotPropertyName.isEndTimeDynamic)).isEqualTo("true");
        assertThat(attributes.get(SnapshotPropertyName.getDynamicDuration)).isEqualTo("40");
        assertThat(attributes.get(SnapshotPropertyName.getNoOfIntervalsForMultiFiles)).isEqualTo("50");
        assertThat(attributes.get(SnapshotPropertyName.getTimeIntervalTypeForMultiFiles)).isEqualTo(LoggingTimeInterval.DAY.toString());
        assertThat(attributes.get(SnapshotPropertyName.getCorrelationSettingScale)).isEqualTo("60");
        assertThat(attributes.get(SnapshotPropertyName.getTimescalingProperties)).isEqualTo(propsA.toDBString());
        assertThat(attributes.get(SnapshotPropertyName.getCorrelationSettingFreq)).isEqualTo(LoggingTimeInterval.HOUR.toString());
        assertThat(attributes.get(SnapshotPropertyName.getPropertyName)).isEqualTo("AE");
        assertThat(attributes.get(SnapshotPropertyName.getTime)).isEqualTo(DynamicTimeDuration.DAYS.toString());
    }

    @Test
    public void getAttributesTrue() {
        TimescalingProperties propsA = TimescalingProperties.getTimeScaleProperties(ScaleAlgorithm.MIN, (short) 2, LoggingTimeInterval.SECOND);

        properties.setSelectedVariables(variableSet("A", "B", "C"));
        properties.setFundamentalFilters(variableSet("D", "E", "F"));
        properties.setDrivingVariable(variable("M"));
        properties.setXAxisVariable(variable("N"));
        properties.setPriorTime(SimplePriorTime.START_OF_HOUR);
        properties.setEndTime(Timestamp.from(Instant.ofEpochSecond(10)));
        properties.setStartTime(Timestamp.from(Instant.ofEpochSecond(20)));
        properties.setDerivationTime(Timestamp.from(Instant.ofEpochSecond(30)));
        properties.setSelectionOutput("AA");
        properties.setMetaDataIncluded(true);
        properties.setFileFormat("AB");
        properties.setDeviceName("AC");
        properties.setTimeZone(LoggingTimeZone.UTC_TIME);
        properties.setDerivationSelection(DerivationSelection.BEST_NOW);
        properties.setGroupByTimestamp(true);
        properties.setMultiFilesActivated(true);
        properties.setChartType("AD");
        properties.setSelectableTimestampsIncluded(true);
        properties.setDistributionChartIncluded(true);
        properties.setLastValueAligned(true);
        properties.setLastValueSearchedIfNoDataFound(true);
        properties.setAlignToEnd(true);
        properties.setAlignToWindow(true);
        properties.setAlignToStart(true);
        properties.setEndTimeDynamic(true);
        properties.setDynamicDuration(40);
        properties.setNoOfIntervalsForMultiFiles(50);
        properties.setTimeIntervalTypeForMultiFiles(LoggingTimeInterval.DAY);
        properties.setCorrelationSettingScale(60);
        properties.setTimeScalingProperties(propsA);
        properties.setCorrelationSettingFreq(LoggingTimeInterval.HOUR);
        properties.setPropertyName("AE");
        properties.setTime(DynamicTimeDuration.DAYS);

        Map<SnapshotPropertyName, String> attributes = properties.getAttributes(true);

        assertThat(attributes.get(SnapshotPropertyName.getSelectedVariables)).isEqualTo("[A, B, C]");
        assertThat(attributes.get(SnapshotPropertyName.getFundamentalFilters)).isEqualTo("[D, E, F]");
        assertThat(attributes.get(SnapshotPropertyName.getDrivingVariable)).isEqualTo("M");
        assertThat(attributes.get(SnapshotPropertyName.getXAxisVariable)).isEqualTo("N");
        assertThat(attributes.get(SnapshotPropertyName.getPriorTime)).isEqualTo(SimplePriorTime.START_OF_HOUR.toString());
        assertThat(attributes.get(SnapshotPropertyName.getEndTime)).isEqualTo(TimeUtils.getStringFromInstant(Instant.ofEpochSecond(10)));
        assertThat(attributes.get(SnapshotPropertyName.getStartTime)).isEqualTo(TimeUtils.getStringFromInstant(Instant.ofEpochSecond(20)));
        assertThat(attributes.get(SnapshotPropertyName.getDerivationTime)).isEqualTo(TimeUtils.getStringFromInstant(Instant.ofEpochSecond(30)));
        assertThat(attributes.get(SnapshotPropertyName.getSelectionOutput)).isEqualTo("AA");
        assertThat(attributes.get(SnapshotPropertyName.isMetaDataIncluded)).isEqualTo("true");
        assertThat(attributes.get(SnapshotPropertyName.getFileFormat)).isEqualTo("AB");
        assertThat(attributes.get(SnapshotPropertyName.getDeviceName)).isEqualTo("AC");
        assertThat(attributes.get(SnapshotPropertyName.getTimeZone)).isEqualTo(LoggingTimeZone.UTC_TIME.toString());
        assertThat(attributes.get(SnapshotPropertyName.getDerivationSelection)).isEqualTo(DerivationSelection.BEST_NOW.toString());
        assertThat(attributes.get(SnapshotPropertyName.isGroupByTimestamp)).isEqualTo("true");
        assertThat(attributes.get(SnapshotPropertyName.isMultiFilesActivated)).isEqualTo("true");
        assertThat(attributes.get(SnapshotPropertyName.getChartType)).isEqualTo("AD");
        assertThat(attributes.get(SnapshotPropertyName.isSelectableTimestampsIncluded)).isEqualTo("true");
        assertThat(attributes.get(SnapshotPropertyName.isDistributionChartIncluded)).isEqualTo("true");
        assertThat(attributes.get(SnapshotPropertyName.isLastValueAligned)).isEqualTo("true");
        assertThat(attributes.get(SnapshotPropertyName.isLastValueSearchedIfNoDataFound)).isEqualTo("true");
        assertThat(attributes.get(SnapshotPropertyName.isAlignToEnd)).isEqualTo("true");
        assertThat(attributes.get(SnapshotPropertyName.isAlignToWindow)).isEqualTo("true");
        assertThat(attributes.get(SnapshotPropertyName.isAlignToStart)).isEqualTo("true");
        assertThat(attributes.get(SnapshotPropertyName.isEndTimeDynamic)).isEqualTo("true");
        assertThat(attributes.get(SnapshotPropertyName.getDynamicDuration)).isEqualTo("40");
        assertThat(attributes.get(SnapshotPropertyName.getNoOfIntervalsForMultiFiles)).isEqualTo("50");
        assertThat(attributes.get(SnapshotPropertyName.getTimeIntervalTypeForMultiFiles)).isEqualTo(LoggingTimeInterval.DAY.toString());
        assertThat(attributes.get(SnapshotPropertyName.getCorrelationSettingScale)).isEqualTo("60");
        assertThat(attributes.get(SnapshotPropertyName.getTimescalingProperties)).isEqualTo(propsA.toDBString());
        assertThat(attributes.get(SnapshotPropertyName.getCorrelationSettingFreq)).isEqualTo(LoggingTimeInterval.HOUR.toString());
        assertThat(attributes.get(SnapshotPropertyName.getPropertyName)).isEqualTo("AE");
        assertThat(attributes.get(SnapshotPropertyName.getTime)).isEqualTo(DynamicTimeDuration.DAYS.toString());
    }

    @Test
    public void shouldAllowSingleVariableInMultipleRoles() {
        properties.setSelectedVariables(variableSet("A", "B", "C"));
        properties.setXAxisVariable(variable("A"));

        assertThat((Map<String, Variable>) properties.getSelectedVariables()).containsKeys("A", "B", "C");
        assertThat(properties.getXAxisVariable()).isEqualTo(variable("A"));
    }

    @Test
    public void selectedVariables() {
        assertThat((Map<String, Variable>) properties.getSelectedVariables()).isEmpty();

        properties.setSelectedVariables(variableSet("A", "B", "C"));
        assertThat((Map<String, Variable>) properties.getSelectedVariables()).containsKeys("A", "B", "C");

        properties.setSelectedVariables(variableSet("B", "C", "D"));
        assertThat((Map<String, Variable>) properties.getSelectedVariables()).containsKeys("A", "B", "C", "D");


        properties.setSelectedVariables(null);
        assertThat((Map<String, Variable>) properties.getSelectedVariables()).isEmpty();
    }

    @Test
    public void fundamentalFilters() {
        assertThat((Map<String, Variable>) properties.getFundamentalFilters()).isEmpty();
        assertThat(properties.isFundamentalFiltered()).isFalse();

        properties.setFundamentalFilters(variableSet("A", "B", "C"));
        assertThat((Map<String, Variable>) properties.getFundamentalFilters()).containsKeys("A", "B", "C");
        assertThat(properties.isFundamentalFiltered()).isTrue();

        properties.setFundamentalFilters(variableSet("B", "C", "D"));
        assertThat((Map<String, Variable>) properties.getFundamentalFilters()).containsKeys("A", "B", "C", "D");
        assertThat(properties.isFundamentalFiltered()).isTrue();

        properties.setFundamentalFilters(null);
        assertThat((Map<String, Variable>) properties.getFundamentalFilters()).isEmpty();
        assertThat(properties.isFundamentalFiltered()).isFalse();
    }

    @Test
    public void getDrivingVariable() {
        assertThat(properties.getDrivingVariable()).isNull();

        properties.setDrivingVariable(variable("A"));
        assertThat(properties.getDrivingVariable()).isEqualTo(variable("A"));

        properties.setDrivingVariable(variable("B"));
        assertThat(properties.getDrivingVariable()).isEqualTo(variable("B"));

        properties.setDrivingVariable(null);
        assertThat(properties.getDrivingVariable()).isNull();
    }

    @Test
    public void getXAxisVariable() {
        assertThat(properties.getXAxisVariable()).isNull();

        properties.setXAxisVariable(variable("A"));
        assertThat(properties.getXAxisVariable()).isEqualTo(variable("A"));

        properties.setXAxisVariable(variable("B"));
        assertThat(properties.getXAxisVariable()).isEqualTo(variable("B"));

        properties.setXAxisVariable(null);
        assertThat(properties.getXAxisVariable()).isNull();
    }

    @Test
    public void getAllVariables() {
        properties.setXAxisVariable(variable("A"));
        properties.setFundamentalFilters(variableSet("B", "C"));
        properties.setSelectedVariables(variableSet("D", "E"));
        properties.setDrivingVariable(variable("F"));

        assertThat(properties.getAllVariables().stream().map(SimpleSnapshotVariableBean::toString))
                .containsExactlyInAnyOrder("A", "B", "C", "D", "E", "F");
    }

    @Test
    public void getPriorTime() {
        assertThat(properties.getPriorTime()).isNull();

        properties.setPriorTime(SimplePriorTime.START_OF_HOUR);
        assertThat(properties.getPriorTime()).isEqualTo(SimplePriorTime.START_OF_HOUR);

        properties.setPriorTime(SimplePriorTime.START_OF_MONTH);
        assertThat(properties.getPriorTime()).isEqualTo(SimplePriorTime.START_OF_MONTH);

        properties.setPriorTime(null);
        assertThat(properties.getPriorTime()).isNull();

        properties.setPriorTime(SimplePriorTime.PREVIOUS_MONDAY);
        assertThat(properties.getPriorTime()).isEqualTo(SimplePriorTime.PREVIOUS_MONDAY);
    }

    @Test
    public void getEndTime() {
        assertThat(properties.getEndTime()).isNull();

        properties.setEndTime(Timestamp.from(Instant.ofEpochSecond(10)));
        assertThat(properties.getEndTime()).isEqualTo(Timestamp.from(Instant.ofEpochSecond(10)));

        properties.setEndTime(Timestamp.from(Instant.ofEpochSecond(20)));
        assertThat(properties.getEndTime()).isEqualTo(Timestamp.from(Instant.ofEpochSecond(20)));

        properties.setEndTime(null);
        assertThat(properties.getEndTime()).isNull();
    }

    @Test
    public void getStartTime() {
        assertThat(properties.getStartTime()).isNull();

        properties.setStartTime(Timestamp.from(Instant.ofEpochSecond(10)));
        assertThat(properties.getStartTime()).isEqualTo(Timestamp.from(Instant.ofEpochSecond(10)));

        properties.setStartTime(Timestamp.from(Instant.ofEpochSecond(20)));
        assertThat(properties.getStartTime()).isEqualTo(Timestamp.from(Instant.ofEpochSecond(20)));

        properties.setStartTime(null);
        assertThat(properties.getStartTime()).isNull();
    }

    @Test
    public void getDerivationTime() {
        assertThat(properties.getDerivationTime()).isNull();

        properties.setDerivationTime(Timestamp.from(Instant.ofEpochSecond(10)));
        assertThat(properties.getDerivationTime()).isEqualTo(Timestamp.from(Instant.ofEpochSecond(10)));

        properties.setDerivationTime(Timestamp.from(Instant.ofEpochSecond(20)));
        assertThat(properties.getDerivationTime()).isEqualTo(Timestamp.from(Instant.ofEpochSecond(20)));

        properties.setDerivationTime(null);
        assertThat(properties.getDerivationTime()).isNull();
    }

    @Test
    public void getSelectionOutput() {
        assertThat(properties.getSelectionOutput()).isNull();

        properties.setSelectionOutput("A");
        assertThat(properties.getSelectionOutput()).isEqualTo("A");

        properties.setSelectionOutput("B");
        assertThat(properties.getSelectionOutput()).isEqualTo("B");

        properties.setSelectionOutput(null);
        assertThat(properties.getSelectionOutput()).isNull();
    }

    @Test
    public void isMetaDataIncluded() {
        assertThat(properties.isMetaDataIncluded()).isFalse();

        properties.setMetaDataIncluded(true);
        assertThat(properties.isMetaDataIncluded()).isTrue();

        properties.setMetaDataIncluded(false);
        assertThat(properties.isMetaDataIncluded()).isFalse();
    }

    @Test
    public void getFileFormat() {
        assertThat(properties.getFileFormat()).isNull();

        properties.setFileFormat("A");
        assertThat(properties.getFileFormat()).isEqualTo("A");

        properties.setFileFormat("B");
        assertThat(properties.getFileFormat()).isEqualTo("B");

        properties.setFileFormat(null);
        assertThat(properties.getFileFormat()).isNull();
    }

    @Test
    public void getDeviceName() {
        assertThat(properties.getDeviceName()).isNull();

        properties.setDeviceName("A");
        assertThat(properties.getDeviceName()).isEqualTo("A");

        properties.setDeviceName("B");
        assertThat(properties.getDeviceName()).isEqualTo("B");

        properties.setDeviceName(null);
        assertThat(properties.getDeviceName()).isNull();
    }

    @Test
    public void getTimeZone() {
        assertThat(properties.getTimeZone()).isNull();

        properties.setTimeZone(LoggingTimeZone.UTC_TIME);
        assertThat(properties.getTimeZone()).isEqualTo(LoggingTimeZone.UTC_TIME);

        properties.setTimeZone(LoggingTimeZone.LOCAL_TIME);
        assertThat(properties.getTimeZone()).isEqualTo(LoggingTimeZone.LOCAL_TIME);

        properties.setTimeZone(null);
        assertThat(properties.getTimeZone()).isNull();
    }

    @Test
    public void getDerivationSelection() {
        assertThat(properties.getDerivationSelection()).isNull();

        properties.setDerivationSelection(DerivationSelection.BEST_NOW);
        assertThat(properties.getDerivationSelection()).isEqualTo(DerivationSelection.BEST_NOW);

        properties.setDerivationSelection(DerivationSelection.BEST_AT_SPECIFIED_TIME);
        assertThat(properties.getDerivationSelection()).isEqualTo(DerivationSelection.BEST_AT_SPECIFIED_TIME);

        properties.setDerivationSelection(null);
        assertThat(properties.getDerivationSelection()).isNull();
    }

    @Test
    public void isGroupByTimestamp() {
        assertThat(properties.isGroupByTimestamp()).isFalse();

        properties.setGroupByTimestamp(true);
        assertThat(properties.isGroupByTimestamp()).isTrue();

        properties.setGroupByTimestamp(false);
        assertThat(properties.isGroupByTimestamp()).isFalse();
    }

    @Test
    public void isMultiFilesActivated() {
        assertThat(properties.isMultiFilesActivated()).isFalse();

        properties.setMultiFilesActivated(true);
        assertThat(properties.isMultiFilesActivated()).isTrue();

        properties.setMultiFilesActivated(false);
        assertThat(properties.isMultiFilesActivated()).isFalse();
    }

    @Test
    public void getChartType() {
        assertThat(properties.getChartType()).isEqualTo("Versus Time");

        properties.setChartType("A");
        assertThat(properties.getChartType()).isEqualTo("A");

        properties.setChartType("B");
        assertThat(properties.getChartType()).isEqualTo("B");

        properties.setChartType(null);
        assertThat(properties.getChartType()).isEqualTo("Versus Time");
    }

    @Test
    public void isSelectableTimestampsIncluded() {
        assertThat(properties.isSelectableTimestampsIncluded()).isFalse();

        properties.setSelectableTimestampsIncluded(true);
        assertThat(properties.isSelectableTimestampsIncluded()).isTrue();

        properties.setSelectableTimestampsIncluded(false);
        assertThat(properties.isSelectableTimestampsIncluded()).isFalse();
    }

    @Test
    public void isDistributionChartIncluded() {
        assertThat(properties.isDistributionChartIncluded()).isFalse();

        properties.setDistributionChartIncluded(true);
        assertThat(properties.isDistributionChartIncluded()).isTrue();

        properties.setDistributionChartIncluded(false);
        assertThat(properties.isDistributionChartIncluded()).isFalse();
    }

    @Test
    public void isLastValueAligned() {
        assertThat(properties.isLastValueAligned()).isFalse();

        properties.setLastValueAligned(true);
        assertThat(properties.isLastValueAligned()).isTrue();

        properties.setLastValueAligned(false);
        assertThat(properties.isLastValueAligned()).isFalse();
    }

    @Test
    public void isLastValueSearchedIfNoDataFound() {
        assertThat(properties.isLastValueSearchedIfNoDataFound()).isFalse();

        properties.setLastValueSearchedIfNoDataFound(true);
        assertThat(properties.isLastValueSearchedIfNoDataFound()).isTrue();

        properties.setLastValueSearchedIfNoDataFound(false);
        assertThat(properties.isLastValueSearchedIfNoDataFound()).isFalse();
    }

    @Test
    public void isAlignToEnd() {
        assertThat(properties.isAlignToEnd()).isFalse();

        properties.setAlignToEnd(true);
        assertThat(properties.isAlignToEnd()).isTrue();

        properties.setAlignToEnd(false);
        assertThat(properties.isAlignToEnd()).isFalse();
    }

    @Test
    public void isAlignToWindow() {
        assertThat(properties.isAlignToWindow()).isFalse();

        properties.setAlignToWindow(true);
        assertThat(properties.isAlignToWindow()).isTrue();

        properties.setAlignToWindow(false);
        assertThat(properties.isAlignToWindow()).isFalse();
    }

    @Test
    public void isAlignToStart() {
        assertThat(properties.isAlignToStart()).isFalse();

        properties.setAlignToStart(true);
        assertThat(properties.isAlignToStart()).isTrue();

        properties.setAlignToStart(false);
        assertThat(properties.isAlignToStart()).isFalse();
    }

    @Test
    public void isEndTimeDynamic() {
        assertThat(properties.isEndTimeDynamic()).isFalse();

        properties.setEndTimeDynamic(true);
        assertThat(properties.isEndTimeDynamic()).isTrue();

        properties.setEndTimeDynamic(false);
        assertThat(properties.isEndTimeDynamic()).isFalse();
    }

    @Test
    public void getDynamicDuration() {
        assertThat(properties.getDynamicDuration()).isEqualTo(0);

        properties.setDynamicDuration(10);
        assertThat(properties.getDynamicDuration()).isEqualTo(10);

        properties.setDynamicDuration(20);
        assertThat(properties.getDynamicDuration()).isEqualTo(20);
    }

    @Test
    public void getNoOfIntervalsForMultiFiles() {
        assertThat(properties.getNoOfIntervalsForMultiFiles()).isEqualTo(0);

        properties.setNoOfIntervalsForMultiFiles(10);
        assertThat(properties.getNoOfIntervalsForMultiFiles()).isEqualTo(10);

        properties.setNoOfIntervalsForMultiFiles(20);
        assertThat(properties.getNoOfIntervalsForMultiFiles()).isEqualTo(20);
    }

    @Test
    public void getTimeIntervalTypeForMultiFiles() {
        assertThat(properties.getTimeIntervalTypeForMultiFiles()).isNull();

        properties.setTimeIntervalTypeForMultiFiles(LoggingTimeInterval.DAY);
        assertThat(properties.getTimeIntervalTypeForMultiFiles()).isEqualTo(LoggingTimeInterval.DAY);

        properties.setTimeIntervalTypeForMultiFiles(LoggingTimeInterval.HOUR);
        assertThat(properties.getTimeIntervalTypeForMultiFiles()).isEqualTo(LoggingTimeInterval.HOUR);

        properties.setTimeIntervalTypeForMultiFiles(null);
        assertThat(properties.getTimeIntervalTypeForMultiFiles()).isNull();
    }

    @Test
    public void getCorrelationSettingScale() {
        assertThat(properties.getCorrelationSettingScale()).isEqualTo(0);

        properties.setCorrelationSettingScale(10);
        assertThat(properties.getCorrelationSettingScale()).isEqualTo(10);

        properties.setCorrelationSettingScale(20);
        assertThat(properties.getCorrelationSettingScale()).isEqualTo(20);
    }

    @Test
    public void setTimeScalingProperties() {
        assertThat(properties.getTimeScalingProperties()).isNull();
        assertThat(properties.isTimeScalingApplied()).isFalse();

        TimescalingProperties propsA = TimescalingProperties.getTimeScaleProperties(ScaleAlgorithm.MIN, (short) 2, LoggingTimeInterval.SECOND);
        properties.setTimeScalingProperties(propsA);
        assertThat(properties.getTimeScalingProperties().toDBString()).isEqualTo(propsA.toDBString());
        assertThat(properties.isTimeScalingApplied()).isTrue();

        TimescalingProperties propsB = TimescalingProperties.getTimeScaleProperties(ScaleAlgorithm.MAX, (short) 4, LoggingTimeInterval.MINUTE);
        properties.setTimeScalingProperties(propsB);
        assertThat(properties.getTimeScalingProperties().toDBString()).isEqualTo(propsB.toDBString());
        assertThat(properties.isTimeScalingApplied()).isTrue();

        properties.setTimeScalingProperties(null);
        assertThat(properties.getTimeScalingProperties()).isNull();
        assertThat(properties.isTimeScalingApplied()).isFalse();
    }

    @Test
    public void getCorrelationSettingFreq() {
        assertThat(properties.getCorrelationSettingFreq()).isNull();

        properties.setCorrelationSettingFreq(LoggingTimeInterval.DAY);
        assertThat(properties.getCorrelationSettingFreq()).isEqualTo(LoggingTimeInterval.DAY);

        properties.setCorrelationSettingFreq(LoggingTimeInterval.HOUR);
        assertThat(properties.getCorrelationSettingFreq()).isEqualTo(LoggingTimeInterval.HOUR);

        properties.setCorrelationSettingFreq(null);
        assertThat(properties.getCorrelationSettingFreq()).isNull();
    }

    @Test
    public void getPropertyName() {
        assertThat(properties.getPropertyName()).isNull();

        properties.setPropertyName("A");
        assertThat(properties.getPropertyName()).isEqualTo("A");

        properties.setPropertyName("B");
        assertThat(properties.getPropertyName()).isEqualTo("B");

        properties.setPropertyName(null);
        assertThat(properties.getPropertyName()).isNull();
    }

    @Test
    public void getTime() {
        assertThat(properties.getTime()).isNull();

        properties.setTime(DynamicTimeDuration.DAYS);
        assertThat(properties.getTime()).isEqualTo(DynamicTimeDuration.DAYS);

        properties.setTime(DynamicTimeDuration.HOURS);
        assertThat(properties.getTime()).isEqualTo(DynamicTimeDuration.HOURS);

        properties.setTime(null);
        assertThat(properties.getTime()).isNull();
    }

    @Test
    public void setFillsFiltersBeamModes() {
        assertThat(properties.getFillsFiltersBeamModes()).isEmpty();

        properties.setFillsFiltersBeamModes(new BeamModeValue[]{BeamModeValue.ABORT, BeamModeValue.ADJUST});
        assertThat(properties.getFillsFiltersBeamModes()).contains(BeamModeValue.ABORT, BeamModeValue.ADJUST);

        properties.setFillsFiltersBeamModes(new BeamModeValue[]{BeamModeValue.ADJUST, BeamModeValue.CIRCDUMP});
        assertThat(properties.getFillsFiltersBeamModes()).contains(BeamModeValue.ABORT, BeamModeValue.ADJUST, BeamModeValue.CIRCDUMP);

        properties.setFillsFiltersBeamModes(null);
        assertThat(properties.getFillsFiltersBeamModes()).isEmpty();
    }

    @Test
    public void setSelectedBeamModes() {
        assertThat(properties.getSelectedBeamModes()).isEmpty();

        properties.setSelectedBeamModes(new BeamModeValue[]{BeamModeValue.ABORT, BeamModeValue.ADJUST});
        assertThat(properties.getSelectedBeamModes()).contains(BeamModeValue.ABORT, BeamModeValue.ADJUST);

        properties.setSelectedBeamModes(new BeamModeValue[]{BeamModeValue.ADJUST, BeamModeValue.CIRCDUMP});
        assertThat(properties.getSelectedBeamModes()).contains(BeamModeValue.ABORT, BeamModeValue.ADJUST, BeamModeValue.CIRCDUMP);

        properties.setSelectedBeamModes(null);
        assertThat(properties.getSelectedBeamModes()).isEmpty();
    }

    @Test
    public void getValueFilters() {
        assertThat(properties.getValueFilters()).isEmpty();
    }

    @Test
    public void getFillInfo() {
        assertThat(properties.getFillInfo()).isNull();
    }

    @Test
    public void getAdditionalSelectionActionDescription() {
        assertThat(properties.getAdditionalSelectionActionDescription()).isNull();
    }

    @Test
    public void isAlignToSignal() {
        assertThat(properties.isAlignToSignal()).isFalse();
    }

    @Test
    public void isAlignedWithVariable() {
        assertThat(properties.isAlignedWithVariable()).isFalse();
    }

    @Test
    public void isGetLastDataWhenNone() {
        assertThat(properties.isGetLastDataWhenNone()).isFalse();
    }

    @Test
    public void isValueFiltered() {
        assertThat(properties.isValueFiltered()).isFalse();
    }

    @Test
    public void getTimeWindowSet() {
        properties.setStartTime(Timestamp.from(Instant.ofEpochSecond(20)));
        properties.setEndTime(Timestamp.from(Instant.ofEpochSecond(30)));

        TimeWindowSet timeWindowSet = properties.getTimeWindowSet();

        assertThat(timeWindowSet).hasSize(1);

        TimeWindow onlyElement = Iterables.getOnlyElement(timeWindowSet);

        assertThat(onlyElement.getStartTime()).isEqualTo(Timestamp.from(Instant.ofEpochSecond(20)));
        assertThat(onlyElement.getEndTime()).isEqualTo(Timestamp.from(Instant.ofEpochSecond(30)));

    }

    private class MyPriorTime implements PriorTime {

        @Override
        public DateTime time() {
            return new DateTime(0);
        }

        @Override
        public String description() {
            return null;
        }
    }

    //User can set his own class
    @Test
    public void shouldAcceptUserDefinedClassForPriorTime() {
        //given
        PriorTime priorTime = new MyPriorTime();
        properties.setPriorTime(priorTime);

        //when
        PriorTime time = properties.getPriorTime();

        //then
        assertThat(time).isEqualTo(priorTime);
    }

    private VariableSet variableSet(String... names) {
        VariableSet result = new VariableSet();
        for (String name : names) {
            result.addVariable(variable(name));
        }
        return result;
    }

    private Variable variable(String name) {
        return Variable.builder().variableName(name).build();
    }
}