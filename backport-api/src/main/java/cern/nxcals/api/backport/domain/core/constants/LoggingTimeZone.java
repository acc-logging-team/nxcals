package cern.nxcals.api.backport.domain.core.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.TimeZone;

@AllArgsConstructor
@Getter
public enum LoggingTimeZone {
    UTC_TIME(TimeZone.getTimeZone("GMT"), "UTC"),
    LOCAL_TIME(TimeZone.getTimeZone("CET"), "CET");

    private final TimeZone timeZone;
    private final String symbol;
}
