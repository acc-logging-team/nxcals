package cern.nxcals.api.backport.domain.core.metadata;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.VariableConfig;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.io.Serializable;

import static cern.nxcals.api.custom.domain.CmwSystemConstants.DEVICE_KEY_NAME;
import static cern.nxcals.api.custom.domain.CmwSystemConstants.PROPERTY_KEY_NAME;

@AllArgsConstructor
@EqualsAndHashCode
@ToString
@Getter
@Builder
public class SimpleJapcParameter implements Serializable {
    private static final long serialVersionUID = 156964889785216591L;
    private final String deviceName;
    private final String propertyName;
    private final String fieldName;
    private final String variableName;

    public static SimpleJapcParameter from(VariableConfig config, Entity entity) {
        return SimpleJapcParameter.builder()
                .deviceName(entity.getEntityKeyValues().getOrDefault(DEVICE_KEY_NAME, "absent").toString())
                .propertyName(entity.getEntityKeyValues().getOrDefault(PROPERTY_KEY_NAME, "absent").toString())
                .fieldName(config.getFieldName())
                .variableName(config.getVariableName())
                .build();
    }

    public SimpleJapcParameter(String deviceName, String propertyName) {
        this(deviceName, propertyName, null, null);
    }
}
