package cern.nxcals.api.backport.domain.core.timeseriesdata;

import cern.nxcals.api.backport.domain.core.metadata.Variable;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Singular;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class MultiTimeseriesDataSet implements MultiColumnTimeseriesDataSet {
    private final List<TimeseriesDataSet> series;
    private final Set<Timestamp> timestamps;

    @lombok.Builder(builderMethodName = "builder", builderClassName = "Builder")
    private static MultiColumnTimeseriesDataSet userBuilder(@NonNull @Singular("series") List<TimeseriesDataSet> series) {
        Set<Timestamp> timestamps = series.stream()
                .flatMap(Collection::stream)
                .map(TimeseriesData::getStamp)
                .collect(Collectors.toCollection(TreeSet::new));

        List<TimeseriesDataSet> internalSeries = new ArrayList<>(series);
        internalSeries.add(0, utcColumn());
        return new MultiTimeseriesDataSet(internalSeries, Collections.unmodifiableSet(timestamps));
    }

    private static TimeseriesDataSet utcColumn() {
        Variable utcStamp = Variable.builder().variableName("UTC_STAMP").build();
        return SparkTimeseriesDataSet.of(utcStamp, Collections.emptyList());
    }

    @Override
    public List<String> getColumnHeaders() {
        return series.stream().map(TimeseriesDataSet::getVariableName).collect(Collectors.toList());
    }

    @Override
    public int size() {
        return timestamps.size();
    }

    @Override
    public List<String> getRowOfData(Timestamp stamp) {
        return series.stream()
                .skip(1)
                .map(dataset -> dataset.getTimeseriesData(stamp))
                .map(data -> data == null ? null : toString(data))
                .collect(Collectors.toList());
    }

    private String toString(TimeseriesData data) {
        try {
            return data.toStringValue();
        } catch (NoSuchMethodException e) {
            return String.valueOf(Double.NaN);
        }
    }

    @Override
    public Set<Timestamp> getTimestamps() {
        return timestamps;
    }
}
