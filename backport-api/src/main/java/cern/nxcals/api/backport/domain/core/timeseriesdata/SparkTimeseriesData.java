package cern.nxcals.api.backport.domain.core.timeseriesdata;

import cern.cmw.datax.EntryType;
import cern.cmw.datax.ImmutableData;
import cern.cmw.datax.ImmutableEntry;
import cern.nxcals.api.backport.domain.core.constants.LoggingTimeZone;
import cern.nxcals.api.backport.domain.core.constants.VariableDataType;
import cern.nxcals.api.converters.SparkRowToImmutableDataConverter;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.common.SystemFields;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.spark.sql.Row;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Objects;

import static cern.cmw.datax.EntryType.BOOL_WRAPPER_ARRAY;
import static cern.cmw.datax.EntryType.DOUBLE;
import static cern.cmw.datax.EntryType.DOUBLE_ARRAY;
import static cern.cmw.datax.EntryType.DOUBLE_WRAPPER_ARRAY;
import static cern.cmw.datax.EntryType.FLOAT;
import static cern.cmw.datax.EntryType.FLOAT_ARRAY;
import static cern.cmw.datax.EntryType.FLOAT_WRAPPER_ARRAY;
import static cern.cmw.datax.EntryType.INT16;
import static cern.cmw.datax.EntryType.INT16_ARRAY;
import static cern.cmw.datax.EntryType.INT16_WRAPPER_ARRAY;
import static cern.cmw.datax.EntryType.INT32;
import static cern.cmw.datax.EntryType.INT32_ARRAY;
import static cern.cmw.datax.EntryType.INT32_WRAPPER_ARRAY;
import static cern.cmw.datax.EntryType.INT64;
import static cern.cmw.datax.EntryType.INT64_ARRAY;
import static cern.cmw.datax.EntryType.INT64_WRAPPER_ARRAY;
import static cern.cmw.datax.EntryType.INT8;
import static cern.cmw.datax.EntryType.INT8_ARRAY;
import static cern.cmw.datax.EntryType.INT8_WRAPPER_ARRAY;
import static cern.cmw.datax.EntryType.STRING;
import static cern.cmw.datax.EntryType.STRING_ARRAY;
import static cern.nxcals.api.backport.domain.core.constants.VariableDataType.MATRIX_NUMERIC;
import static cern.nxcals.api.backport.domain.core.constants.VariableDataType.NUMERIC;
import static cern.nxcals.api.backport.domain.core.constants.VariableDataType.TEXT;
import static cern.nxcals.api.backport.domain.core.constants.VariableDataType.UNDETERMINED;
import static cern.nxcals.api.backport.domain.core.constants.VariableDataType.VECTOR_NUMERIC;
import static cern.nxcals.api.backport.domain.core.constants.VariableDataType.VECTOR_STRING;
import static cern.nxcals.api.backport.domain.core.metadata.FundamentalData.DESTINATION_FIELD;
import static cern.nxcals.api.backport.domain.core.metadata.FundamentalData.FUNDAMENTAL_NAME_FIELD;
import static cern.nxcals.api.backport.domain.core.metadata.FundamentalData.NXCALS_FUNDAMENTAL_TIMESTAMP;
import static cern.nxcals.common.SystemFields.NXC_EXTR_TIMESTAMP;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VALUE;

@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class SparkTimeseriesData implements TimeseriesData {
    @Getter
    private final Timestamp stamp;
    private Object data;
    private int[] dims;
    private EntryType<?> type;
    @Getter
    private String name;
    private String destination;

    private static final ImmutableSet<EntryType> supportedEnumTypes = ImmutableSet.of(
            INT8,
            INT16,
            INT32,
            INT64,
            FLOAT,
            DOUBLE,
            STRING,
            INT8_ARRAY,
            INT16_ARRAY,
            INT32_ARRAY,
            INT64_ARRAY,
            FLOAT_ARRAY,
            DOUBLE_ARRAY,
            BOOL_WRAPPER_ARRAY,
            INT8_WRAPPER_ARRAY,
            INT16_WRAPPER_ARRAY,
            INT32_WRAPPER_ARRAY,
            INT64_WRAPPER_ARRAY,
            FLOAT_WRAPPER_ARRAY,
            DOUBLE_WRAPPER_ARRAY,
            STRING_ARRAY
    );

    public static TimeseriesData of(@NonNull Row row) {
        return new SparkTimeseriesData(row);
    }

    public static TimeseriesData of(@NonNull Timestamp timestamp, @NonNull ImmutableEntry data) {
        return new SparkTimeseriesData(timestamp, data.get(), data.getDims(), data.getType());
    }

    public static TimeseriesData ofDouble(@NonNull Timestamp timestamp, double value) {
        return new SparkTimeseriesData(timestamp, value);
    }

    public static TimeseriesData ofDataDistribution(@NonNull Row row) {
        ImmutableData immutableData = SparkRowToImmutableDataConverter.convert(row);
        ImmutableEntry data = immutableData.getEntry(NXC_EXTR_VALUE.getValue());
        Object value = getValueForDataDistribution(data.get());
        Timestamp timestamp = toTimestamp(row.getAs(NXC_EXTR_TIMESTAMP.getValue()));
        return new SparkTimeseriesData(timestamp, value, data.getDims(), data.getType());

    }

    public static TimeseriesData ofFundamental(@NonNull Row row) {
        ImmutableData immutableData = SparkRowToImmutableDataConverter.convert(row);
        ImmutableEntry name = immutableData.getEntry(FUNDAMENTAL_NAME_FIELD);
        ImmutableEntry timestamp = immutableData.getEntry(NXCALS_FUNDAMENTAL_TIMESTAMP);
        ImmutableEntry destination = immutableData.getEntry(DESTINATION_FIELD);

        return new SparkTimeseriesData(toTimestamp(timestamp.getAs(INT64)), name.getAs(STRING),
                destination.getAs(STRING));
    }

    private static Timestamp toTimestamp(@NonNull Long nanos) {
        return Timestamp.from(TimeUtils.getInstantFromNanos(nanos));
    }

    private static <T> T[] asArray(T... items) {
        return items;
    }

    private static Object getValueForDataDistribution(Object data) {
        if (data instanceof String) {
            return null;
        }
        if (data instanceof Double || data instanceof Float) {
            return 0d;
        }
        if (data instanceof Long || data instanceof Integer || data instanceof Short) {
            return 0;
        }
        if (data instanceof Double[] || data instanceof Float[]) {
            return asArray(0d);
        }
        if (data instanceof Long[] || data instanceof Integer[]|| data instanceof Short[]) {
            return asArray(0);
        }
        if (data instanceof String[]) {
            return asArray((String) null);
        }
        return null;
    }

    private SparkTimeseriesData(Timestamp timestamp, Object data, int[] dims, EntryType<?> type) {
        this.stamp = timestamp;
        this.data = data;
        this.dims = dims;
        this.type = type;
    }

    private SparkTimeseriesData(Timestamp timestamp, String fundamentalName, String destination) {
        this.stamp = timestamp;
        this.name = fundamentalName;
        this.destination = destination;
    }

    private SparkTimeseriesData(Timestamp timestamp, double value) {
        this.stamp = timestamp;
        this.data = value;
        this.type = EntryType.DOUBLE;
        this.dims = new int[0];
    }

    private SparkTimeseriesData(@NonNull Row row) {
        this(row, NXC_EXTR_TIMESTAMP, NXC_EXTR_VALUE);
    }

    private SparkTimeseriesData(@NonNull Row row, SystemFields stampField, SystemFields valueField) {
        ImmutableData immutableData = SparkRowToImmutableDataConverter.convert(row);
        ImmutableEntry value = immutableData.getEntry(valueField.getValue());
        ImmutableEntry timestamp = immutableData.getEntry(stampField.getValue());

        Preconditions.checkNotNull(value);
        Preconditions.checkNotNull(timestamp);

        stamp = toTimestamp(timestamp.getAs(INT64));
        dims = value.getDims();
        type = value.getType();
        data = value.get();
    }

    public SparkTimeseriesData.SparkTimeseriesDataBuilder toBuilder() {
        return SparkTimeseriesData.builder()
                .stamp(Timestamp.from(stamp.toInstant()))
                .data(data) //assumes immutable?
                .dims(Arrays.copyOf(dims, dims.length))
                .type(type).destination(destination).name(name);
    }

    @Override
    public Class<?> type() {
        return type.getTypeClass();
    }

    @Override
    public int[] dimensions() {
        return dims;
    }

    @Override
    public VariableDataType getVariableDataType() {

        if (!supportedEnumTypes.contains(this.type))
            return UNDETERMINED;

        if (dims.length == 0) {
            return this.type == STRING ? TEXT: NUMERIC;
        } else if (dims.length == 1) {
            return this.type == STRING_ARRAY ?  VECTOR_STRING : VECTOR_NUMERIC;
        } else if (dims.length == 2) {
            return MATRIX_NUMERIC;
        }

        return UNDETERMINED;
    }

    @Override
    public boolean after(TimeseriesData timeseriesData) {
        return getStamp().after(timeseriesData.getStamp());
    }

    @Override
    public boolean before(TimeseriesData timeseriesData) {
        return getStamp().before(timeseriesData.getStamp());
    }

    @Override
    public String getFormattedStamp(LoggingTimeZone timeZone) {
        // TODO:
        return null;
    }

    @Override
    public String getNumericStatus() throws NoSuchMethodException {
        // TODO:
        return null;
    }

    @Override
    public String getDestination() throws NoSuchMethodException {
        if (destination == null) {
            throw new NoSuchMethodException();
        }
        return this.destination;
    }

    /**
     * @deprecated (never use, major performance hit)
     */
    @Override
    @Deprecated
    public BigDecimal getNumericValue() throws NoSuchMethodException {
        verifyDimensions(0);
        return BigDecimal.valueOf(tryGetNumber().doubleValue());
    }

    @Override
    public double getDoubleValue() throws NoSuchMethodException {
        verifyDimensions(0);

        if (data instanceof Number) {
            return tryGetNumber().doubleValue();
        }
        return tryGetBooleanAsDouble();
    }

    @Override
    public long getLongValue() throws NoSuchMethodException {
        verifyDimensions(0);

        if (data instanceof Number) {
            return tryGetNumber().longValue();
        }
        return tryGetBooleanAsLong();
    }

    @Override
    public String getVarcharValue() throws NoSuchMethodException {
        verifyDimensions(0);
        return tryGet(String.class, data);
    }

    /**
     * @deprecated (never use, major performance hit)
     */
    @Override
    @Deprecated
    public BigDecimal[] getVectorNumericValues() throws NoSuchMethodException {
        verifyDimensions(1);
        BigDecimal[] result = new BigDecimal[dims[0]];
        for (int i = 0; i < result.length; i++) {
            result[i] = BigDecimal.valueOf(tryGetNumberAt(i).doubleValue());
        }
        return result;
    }

    @Override
    public double[] getDoubleValues() throws NoSuchMethodException {
        verifyDimensions(1);
        double[] result = new double[dims[0]];

        if (result.length > 0) {
            if (Array.get(data, 0) instanceof Number) {
                for (int i = 0; i < result.length; i++) {
                    result[i] = tryGetNumberAt(i).doubleValue();
                }
            } else {
                for (int i = 0; i < result.length; i++) {
                    result[i] = getBooleanAsDoubleAt(i);
                }
            }
        }
        return result;
    }

    @Override
    public long[] getLongValues() throws NoSuchMethodException {
        verifyDimensions(1);
        long[] result = new long[dims[0]];

        if (result.length > 0) {
            if (Array.get(data, 0) instanceof Number) {
                for (int i = 0; i < result.length; i++) {
                    result[i] = tryGetNumberAt(i).longValue();
                }
            } else {
                for (int i = 0; i < result.length; i++) {
                    result[i] = getBooleanAsLongAt(i);
                }
            }
        }
        return result;
    }

    @Override
    public String[] getStringValues() throws NoSuchMethodException {
        verifyDimensions(1);
        return tryGet(String[].class, data);
    }

    @Override
    public double[][] getMatrixDoubleValues() throws NoSuchMethodException {
        verifyDimensions(2);
        double[][] result = new double[dims[0]][dims[1]];

        if (Array.get(data, 0) instanceof Number) {

            for (int row = 0; row < dims[0]; row++) {
                for (int col = 0; col < dims[1]; col++) {
                    result[row][col] = tryGetNumberAt(row * dims[1] + col).doubleValue();
                }
            }
        } else {
            result = getBooleansAsDoubles();
        }
        return result;
    }

    @Override
    public long[][] getMatrixLongValues() throws NoSuchMethodException {
        verifyDimensions(2);
        long[][] result = new long[dims[0]][dims[1]];

        if (Array.get(data, 0) instanceof Number) {

            for (int row = 0; row < dims[0]; row++) {
                for (int col = 0; col < dims[1]; col++) {
                    result[row][col] = tryGetNumberAt(row * dims[1] + col).longValue();
                }
            }
        } else {
            result = getBooleansAsLongs();
        }
        return result;
    }

    /**
     * @deprecated (never use, antipattern)
     */
    @Override
    @Deprecated
    public void print(LoggingTimeZone timezone) {
        // TODO
    }

    /**
     * @deprecated (never use, performance hit)
     */
    @Override
    @Deprecated
    public BigDecimal getNumericValueAtVectorIndex(int index) throws NoSuchMethodException {
        verifyDimensions(1);
        return BigDecimal.valueOf(tryGetNumberAt(index).doubleValue());
    }

    @Override
    public double getDoubleValueAtIndex(int index) throws NoSuchMethodException {
        verifyDimensions(1);
        return tryGetNumberAt(index).doubleValue();
    }

    @Override
    public long getLongValueAtIndex(int index) throws NoSuchMethodException {
        verifyDimensions(1);
        return tryGetNumberAt(index).longValue();
    }

    @Override
    public String getStringValueAtIndex(int index) throws NoSuchMethodException {
        verifyDimensions(1);
        return tryGet(String.class, Array.get(data, index));
    }

    @Override
    public Boolean isSubSet() throws NoSuchMethodException {
        // TODO:
        return Boolean.FALSE;
    }

    @Override
    public int[] getVectorNumericIndexes() throws NoSuchMethodException {
        // TODO:
        return new int[0];
    }

    @Override
    public int[] getVectorIndexes() throws NoSuchMethodException {
        // TODO:
        return new int[0];
    }

    @Override
    public String toStringValue() throws NoSuchMethodException {
        if (isNullValue() || dims.length == 0) {
            return String.valueOf(data);
        } else if (dims.length == 1) {
            return "array with " + dims[0] + " values";
        } else if (dims.length == 2) {
            return "matrix array with " + dims[0] * dims[1] + " values";
        }
        throw new NoSuchMethodException("unable to convert to string");
    }

    @Override
    public String toStringValue(int index) throws NoSuchMethodException {
        return getStringValueAtIndex(index);
    }

    @Override
    public boolean isNullValue() throws NoSuchMethodException {
        return data == null;
    }

    @Override
    public boolean isNullValue(int index) throws NoSuchMethodException {
        verifyDimensions(1);
        return Array.get(data, index) == null;
    }

    @Override
    public TimeseriesData alignToTimestamp(Timestamp timestamp) throws NoSuchMethodException {
        return new SparkTimeseriesData(timestamp, data, dims, type);
    }

    @Override
    @SuppressWarnings("squid:CallToDeprecatedMethod")
    public int compareValueTo(TimeseriesData that) throws NoSuchMethodException {
        return this.getNumericValue().compareTo(that.getNumericValue());
    }

    @Override
    public int compareTo(TimeseriesData other) {
        return getStamp().compareTo(other.getStamp());
    }

    // Not using equals for preserving behaviour of old API
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        SparkTimeseriesData that = (SparkTimeseriesData) obj;

        if (!stamp.equals(that.stamp)) {
            return false;
        }

        // ArrayUtils.toPrimitive ignores non-array objects
        return Objects.deepEquals(ArrayUtils.toPrimitive(this.data), ArrayUtils.toPrimitive(that.data));
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31).append(stamp).append(data).build();
    }

    private Number tryGetNumber() throws NoSuchMethodException {
        return tryGet(Number.class, data);
    }

    private Number tryGetNumberAt(int i) throws NoSuchMethodException {
        return tryGet(Number.class, Array.get(data, i));
    }

    private double tryGetBooleanAsDouble() throws NoSuchMethodException {
        return tryGet(Boolean.class, data) ? 1 : 0;
    }

    private double getBooleanAsDoubleAt(int i) throws NoSuchMethodException {

        try {
            return (boolean) Array.get(data, i) ? 1D : 0D;
        } catch (ClassCastException e) {
            throw new NoSuchMethodException();
        }
    }

    private double[][] getBooleansAsDoubles() throws NoSuchMethodException {
        double[][] result = new double[dims[0]][dims[1]];

        for (int row = 0; row < dims[0]; row++) {
            for (int col = 0; col < dims[1]; col++) {
                    result[row][col] = getBooleanAsDoubleAt(row * dims[1] + col);
            }
        }
        return result;
    }

    private long tryGetBooleanAsLong() throws NoSuchMethodException {
        return tryGet(Boolean.class, data) ? 1 : 0;
    }

    private long getBooleanAsLongAt(int i) throws NoSuchMethodException {

        try {
            return (boolean) Array.get(data, i) ? 1L : 0L;
        } catch (ClassCastException e) {
            throw new NoSuchMethodException();
        }
    }

    private long[][] getBooleansAsLongs() throws NoSuchMethodException {
        long[][] result = new long[dims[0]][dims[1]];

        for (int row = 0; row < dims[0]; row++) {
            for (int col = 0; col < dims[1]; col++) {
                result[row][col] = getBooleanAsLongAt(row * dims[1] + col);
            }
        }
        return result;
    }

    private <T> T tryGet(Class<T> clazz, Object obj) throws NoSuchMethodException {
        try {
            return clazz.cast(obj);
        } catch (ClassCastException | NullPointerException ex) {
            throw new NoSuchMethodException();
        }
    }

    private void verifyDimensions(int expected) throws NoSuchMethodException {
        if (dims.length != expected) {
            throw new NoSuchMethodException();
        }
    }
}
