package cern.nxcals.api.backport.client.service;

import cern.nxcals.api.backport.domain.core.timeseriesdata.filter.value.VectorValueFilter;
import lombok.Data;

@Data
class VectorFilterVariable implements LegalColumnName {
    private final VectorValueFilter<?> vectorValueFilter;
    private final String legalColumnName;
}
