package cern.nxcals.api.backport.domain.core.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

/**
 * The Enum LoggingTimeInterval is used to represent a time interval
 */
@Getter
@AllArgsConstructor
public enum LoggingTimeInterval implements Serializable {

    /** The duration of 1 second */
    SECOND("SECOND", 1000L),

    /** The duration of 1 minute */
    MINUTE("MINUTE", 60 * SECOND.timeMs),

    /** The duration of an hour */
    HOUR("HOUR", 60 * MINUTE.timeMs),

    /** The duration of a day */
    DAY("DAY", 24 * HOUR.timeMs),

    /** The duration of a week (7 days) */
    WEEK("WEEK", 7 * DAY.timeMs),

    /** The duration of 30 days */
    MONTH("MONTH (30D)", 30 * DAY.timeMs),

    /** The duration of 365 days */
    YEAR("YEAR (365D)", 365 * DAY.timeMs);

    private final String label;
    private final long timeMs;

    @Override
    public String toString() {
        return label;
    }

    public static String getValues() {
        return StringUtils.join(values(), ',');
    }
}
