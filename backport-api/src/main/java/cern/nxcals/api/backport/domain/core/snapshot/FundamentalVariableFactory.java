package cern.nxcals.api.backport.domain.core.snapshot;

import cern.nxcals.api.backport.domain.core.metadata.FundamentalData;
import cern.nxcals.api.custom.domain.FundamentalFilter;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.VariableDeclaredType;
import lombok.NonNull;
import lombok.experimental.UtilityClass;
import org.apache.commons.lang.StringUtils;

import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@UtilityClass
class FundamentalVariableFactory {
    private static final String FUNDAMENTAL_NAME_PROPERTIES_SEPARATOR = FundamentalData.FUNDAMENTAL_SEPARATOR;

    static Set<cern.nxcals.api.domain.Variable> buildFrom(@NonNull Set<FundamentalFilter> filters,
            @NonNull SystemSpec systemSpec) {
        return filters.stream().map(f -> from(f, systemSpec)).collect(Collectors.toSet());
    }

    private static cern.nxcals.api.domain.Variable from(@NonNull FundamentalFilter filter,
            @NonNull SystemSpec systemSpec) {
        return cern.nxcals.api.domain.Variable.builder()
                .variableName(buildFundamentalVariableName(filter))
                .description("[Generated variable] Source Fundamental filter id: " + filter.getId())
                .declaredType(VariableDeclaredType.FUNDAMENTAL)
                .systemSpec(systemSpec)
                .configs(Collections.emptySortedSet())
                .build();
    }

    private static String buildFundamentalVariableName(FundamentalFilter filter) {
        return Stream.of(filter.getAccelerator(), filter.getLsaCycle(), filter.getTimingUser())
                .map(p -> StringUtils.defaultString(p, ""))
                .collect(Collectors.joining(FUNDAMENTAL_NAME_PROPERTIES_SEPARATOR));
    }

}
