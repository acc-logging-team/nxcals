package cern.nxcals.api.backport.domain.core.timeseriesdata.filter.value;

import java.io.Serializable;
import java.util.function.Consumer;

public interface Filter extends Consumer<FilterVisitor>, Serializable {
    String describe();
}
