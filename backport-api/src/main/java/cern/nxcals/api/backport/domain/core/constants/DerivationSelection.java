package cern.nxcals.api.backport.domain.core.constants;

import java.io.Serializable;

/**
 * Defines the correction algorithms to apply for correcting data.
 */
public enum DerivationSelection implements Serializable {

    /**
     * Takes the best available algorithm for the time of the data (should be the best one)
     */
    BEST_NOW,

    /**
     * Takes the correction algorithm available at the moment of the data (it can use more than one algorithm, if there
     * is more than one correction defined in the chosen time range)
     */
    AVAILABLE_AT_TIME_OF_DATA,

    /**
     * Take the algorithm available at a certain moment in time. An additional parameter (timestamp) must be provided,
     * when this method is chosen.
     */
    BEST_AT_SPECIFIED_TIME,

    /**
     * Uses the raw data
     */
    RAW
}
