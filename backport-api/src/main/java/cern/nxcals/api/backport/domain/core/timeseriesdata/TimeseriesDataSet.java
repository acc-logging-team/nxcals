package cern.nxcals.api.backport.domain.core.timeseriesdata;

import cern.nxcals.api.backport.domain.core.constants.LoggingTimeZone;
import cern.nxcals.api.backport.domain.core.constants.VariableDataType;
import cern.nxcals.api.backport.domain.core.metadata.TimeseriesDataSetMetaData;
import cern.nxcals.api.backport.domain.core.metadata.Variable;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.ListIterator;

public interface TimeseriesDataSet extends Collection<TimeseriesData>, Serializable {

    /**
     * @param index - the index in this TimeseriesDataSet for which TimeseriesData should be returned.
     * @return - TimeseriesData at the given index within this TimeseriesDataSet
     */
    TimeseriesData getTimeseriesData(int index);

    /**
     * @return - the name of the Variable to which this TimeseriesData belongs
     */
    String getVariableName();

    /**
     * @return - the type of the Variable to which this TimeseriesData belongs
     */
    VariableDataType getVariableDataType();

    /**
     * @param data - the TimeseriesData to be added to this TimeseriesDataSet
     * @deprecated (never use, major performance hit, can lead to unpredictable states)
     */
    @Deprecated
    void addTimeseriesData(TimeseriesData data);

    /**
     * @deprecated (never use, major performance hit, can lead to unpredictable states)
     * @return - a ListIterator for all TimeseriesData in this TimeseriesDataSet
     */
    @Deprecated
    ListIterator<TimeseriesData> listIterator();

    /**
     * @return - the highest timestamp in the timeseries data set.
     */
    Timestamp getMaxStamp();

    /**
     * @return - the smallest timestamp in the timeseries data set
     */
    Timestamp getMinStamp();

    /**
     * Returns the data at the given timestamp or repeat the last value if there is no value found at the given
     * timestamp If there is no value before that time, null is returned
     * 
     * @param stamp - The given timestamp
     * @return - The data at the given timestamp or just before
     */
    TimeseriesData getTimeseriesDataOrLast(Timestamp stamp);

    void print(LoggingTimeZone utcTime);

    /**
     * Appends a Timeseries Data Set to the end of the current data set
     *
     * @deprecated (never use, major performance hit, can lead to unpredictable states)
     * @param data The data set to be appended
     */
    @Deprecated
    void append(TimeseriesDataSet data);

    String getMetaDataInfo(LoggingTimeZone timeZone);

    /**
     * Returns the data at the given timestamp or null if the data is not found
     * 
     * @param timestamp
     * @return
     */
    TimeseriesData getTimeseriesData(Timestamp timestamp);

    TimeseriesDataSetMetaData getMetaData();

    TimeseriesDataSet alignToTimestamps(TimeseriesDataSet required);

    /**
     * Note: Not always available (depending on the extraction)
     * 
     * @return
     */
    Variable getVariable();

    String getUnit();
}
