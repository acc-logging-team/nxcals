package cern.nxcals.api.backport.client.service;

import cern.nxcals.api.backport.domain.core.constants.BeamModeValue;
import cern.nxcals.api.backport.domain.core.filldata.LHCFill;
import cern.nxcals.api.backport.domain.core.filldata.LHCFillSet;
import cern.nxcals.api.custom.service.FillService;
import com.google.common.collect.Iterators;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;

import java.sql.Timestamp;

import static java.util.Objects.isNull;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
class LHCFillDataServiceImpl implements LHCFillDataService {
    private final FillService fillService;

    @Override
    public LHCFillSet getLHCFillsInTimeWindow(Timestamp startTime, Timestamp endTime) {
        return getLHCFillsAndBeamModesInTimeWindow(startTime, endTime);
    }

    @Override
    public LHCFillSet getLHCFillsAndBeamModesInTimeWindow(Timestamp startTime, Timestamp endTime) {
        return LHCFillSet.from(fillService.findFills(startTime.toInstant(), endTime.toInstant()));
    }

    @Override
    public LHCFillSet getLHCFillsInTimeWindowContainingBeamModes(Timestamp startTime, Timestamp endTime, BeamModeValue[] beamModes) {
        LHCFillSet fills = getLHCFillsAndBeamModesInTimeWindow(startTime, endTime);
        Iterators.removeIf(fills.iterator(), t-> isNull(t) || !t.containsAnyBeamMode(beamModes));
        return fills;
    }

    @Override
    public LHCFillSet getLHCFillsAndBeamModesInTimeWindowContainingBeamModes(Timestamp startTime, Timestamp endTime, BeamModeValue[] beamModes) {
        return getLHCFillsInTimeWindowContainingBeamModes(startTime, endTime, beamModes);
    }

    @Override
    public LHCFill getLastCompletedLHCFillAndBeamModes() {
        return fillService.getLastCompleted().map(LHCFill::from).orElse(null);
    }

    @Override
    public LHCFill getLHCFillAndBeamModesByFillNumber(int fillNumber) {
        return fillService.findFill(fillNumber).map(LHCFill::from).orElse(null);
    }
}
