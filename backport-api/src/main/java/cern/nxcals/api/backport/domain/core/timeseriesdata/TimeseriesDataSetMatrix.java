package cern.nxcals.api.backport.domain.core.timeseriesdata;

import cern.nxcals.api.backport.domain.core.metadata.Variable;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

public interface TimeseriesDataSetMatrix extends Iterable<TimeseriesDataSet>, Serializable {

    /**
     * Gets the timeseries data set corresponding to the variable name
     * 
     * @param variableName
     * @return
     */
    TimeseriesDataSet getTimeseriesDataSet(String variableName);

    void addTimeseriesDataSet(TimeseriesDataSet dataSet);

    int getVariablesCount();

    String[] getVariablesName();

    /**
     * Updates the distinct timestamps, max and min in case the values have been added to the timeseries data set
     * instead of adding the data set.
     */
    void updateTimestamps();

    Timestamp getMinTimestamp();

    Timestamp getMaxTimestamp();

    List<Timestamp> getDistinctTimestamps();

    List<TimeseriesData> getTimeseriesDataOrderedByVariableName(Timestamp timestamp);

    List<TimeseriesData> getTimeseriesDataOrLastOrderedByVariableName(Timestamp timestamp);

    TimeseriesData getTimeseriesData(String variableName, Timestamp timestamp);

    TimeseriesData getTimeseriesDataOrLast(String variableName, Timestamp timestamp);

    TimeseriesDataSet getTimeseriesDataAligned(String variableNameToBeAligned, String variableNameToAlignedWith);

    TimeseriesData getTimeseriesDataAbsolute(int col, int row);

    TimeseriesData getTimeseriesDataRelative(int col, int row);

    Variable getVariable(String variableName);

    boolean containsVariableName(String variableName);
}
