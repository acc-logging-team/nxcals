package cern.nxcals.api.backport.domain.core.metadata;

import cern.nxcals.api.backport.domain.core.constants.LoggingTimeZone;
import cern.nxcals.api.backport.domain.core.constants.TimescalingProperties;
import cern.nxcals.api.backport.domain.core.constants.VariableDataType;
import cern.nxcals.api.backport.domain.core.timeseriesdata.filter.value.Filter;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;

@Getter
@Setter
public class TimeseriesDataSetMetaData implements Serializable {
    private static final long serialVersionUID = 5243848831871295334L;

    private TimescalingProperties timescaleProperties;
    private String drivingVariableName;
    private Filter filterValue;
    private Set<String> fundamentalNames;
    private boolean dataDistribution;
    private Timestamp startTimeRequest;
    private Timestamp endTimeRequest;

    // In case variable is not possible
    private String variableName;
    private VariableDataType dataType;
    private String unit;

    // If set, it will override variableName, variableDataType and unit
    private Variable variable;

    public boolean areTimestampsAligned() {
        return drivingVariableName != null;
    }

    public VariableDataType getDataType() {
        return variable == null ? dataType : variable.getVariableDataType();
    }

    public String getUnit() {
        return variable != null ? variable.getUnit() : unit;
    }

    public String getVariableName() {
        return variable != null ? variable.getVariableName() : variableName;
    }

    public boolean isFundamentalFiltered() {
        return fundamentalNames != null;
    }

    public boolean isTimescaledApplied() {
        return timescaleProperties != null;
    }

    @SuppressWarnings("squid:S1172") // unused parameters
    public String getMetaDataInfo(LoggingTimeZone timeZone) {
        StringBuilder content = new StringBuilder();

        // If variable is defined, take the variable
        if (variable != null) {
            content.append(variable.getMetaDataInfo());
        } else {
            content.append("Name:").append(variableName);
            content.append("Type:").append(dataType);
            content.append("Unit:").append(unit);
        }

        if (fundamentalNames != null) {
            content.append("!! Data was filtered by ");
            for (String fundamental : fundamentalNames) {
                content.append(fundamental).append(", \n");
            }
            content.append("\n");
        }

        if (timescaleProperties != null) {
            content.append(timescaleProperties);
        }

        if (filterValue != null) {
            content.append("Filtered  by :");
            content.append("\n");
        }

        if (drivingVariableName != null) {
            content.append("!! Data was aligned with ").append(drivingVariableName).append("!!\n");
        }

        if (dataDistribution) {
            content.append("!! Only timestamps are shown. The values is always a constant number and not representative.\n");
        }

        return content.toString();
    }
}
