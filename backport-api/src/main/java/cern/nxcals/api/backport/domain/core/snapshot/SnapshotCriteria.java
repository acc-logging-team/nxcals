package cern.nxcals.api.backport.domain.core.snapshot;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import java.io.Serializable;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class SnapshotCriteria implements Serializable {
    private static final long serialVersionUID = 3330018780756101078L;
    private final String name;
    private final String description;
    private final String owner;
    private final DateTime createdBefore;
    private final DateTime createdAfter;
    private final boolean visible;

    @RequiredArgsConstructor
    public static class SnapshotCriteriaBuilder {
        private static final String NOT_SUPPORTED = "currently date queries are not supported";
        private final String name;
        private final String description;
        private final String owner;
        private DateTime createdBefore = new DateTime();
        private DateTime createdAfter = new DateTime();
        private boolean visible = false; // synonym for public

        public SnapshotCriteriaBuilder(String name, String owner) {
            this(name, "%", owner);
        }

        public SnapshotCriteriaBuilder withCreatedBefore(DateTime createdBefore) {
            throw new IllegalStateException(NOT_SUPPORTED);
        }

        public SnapshotCriteriaBuilder withCreatedAfter(DateTime createdAfter) {
            throw new IllegalStateException(NOT_SUPPORTED);
        }

        public SnapshotCriteriaBuilder withPublic() {
            this.visible = true;
            return this;
        }

        public SnapshotCriteria build() {
            return new SnapshotCriteria(
                    sanitize(this.name),
                    sanitize(this.description),
                    sanitize(this.owner),
                    this.createdBefore,
                    this.createdAfter,
                    this.visible);
        }

        private String sanitize(String in) {
            return StringUtils.isEmpty(in) ? "%" : in;
        }
    }
}
