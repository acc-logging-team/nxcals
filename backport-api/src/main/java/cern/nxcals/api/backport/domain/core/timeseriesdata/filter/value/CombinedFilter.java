package cern.nxcals.api.backport.domain.core.timeseriesdata.filter.value;

import cern.nxcals.api.backport.domain.core.constants.VectorRestriction;
import lombok.Getter;

import java.io.Serializable;
import java.util.Objects;

@Getter
public class CombinedFilter implements Filter, Serializable {
    private static final long serialVersionUID = 2306833759762154123L;
    private final Filter f1;
    private final Filter f2;
    private final LogicalFilter operand;

    CombinedFilter(Filter f1, Filter f2, LogicalFilter operand) {
        this.f1 = Objects.requireNonNull(f1, "Filter must not be null");
        this.f2 = f2;
        this.operand = operand;
        validate();
    }

    private void validate() {
        validate(f1);
        validate(f2);
    }

    private void validate(Filter f) {
        if (f instanceof VectorValueFilter<?>) {
            VectorValueFilter<?> vvf = (VectorValueFilter<?>) f;
            if (vvf.getRestriction() == VectorRestriction.MATCHED_ELEMENTS) {
                throw new IllegalStateException("Cannot build a complex filter with MATCHED_ELEMENTS restriction");
            }
        }
    }

    @Override
    public void accept(FilterVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String describe() {
        return "(" + f1.describe() + " " + operand.describe() + " " + (f2 != null ? f2.describe() + " " : "") + ")";
    }

    @Override
    public String toString() {
        return describe();
    }
}
