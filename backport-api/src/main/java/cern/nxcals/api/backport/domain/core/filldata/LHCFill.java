package cern.nxcals.api.backport.domain.core.filldata;

import cern.nxcals.api.backport.domain.core.constants.BeamModeValue;
import cern.nxcals.api.custom.domain.Fill;
import com.google.common.base.Preconditions;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.Value;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Arrays;

import static cern.nxcals.api.utils.TimeUtils.getTimestampFromInstant;

@Value
@AllArgsConstructor
public class LHCFill implements Serializable {
    private static final long serialVersionUID = -1339024449356522100L;
    private final int fillNumber;
    private final Timestamp startTime;
    private final Timestamp endTime;
    private final BeamModeSet beamModes;


    public static LHCFill from(@NonNull Fill fill) {
        Preconditions.checkArgument(!fill.getValidity().isLeftInfinite());
        Timestamp from = getTimestampFromInstant(fill.getValidity().getStartTime());
        Instant endTime = fill.getValidity().getEndTime();
        Timestamp to = null;
        if(endTime != null) {
            to = getTimestampFromInstant(endTime);
        }

        return new LHCFill((int) fill.getNumber(), from, to, BeamModeSet.from(fill.getBeamModes()));
    }

    public boolean containsAnyBeamMode(BeamModeValue[] modes) {
        return beamModes != null && Arrays.stream(modes).anyMatch(beamModes::containsBeamModeValue);
    }

    public BeamModeSet getBeamModes(BeamModeValue beamMode) {
        return beamModes == null ? new BeamModeSet() : beamModes.getBeamModes(beamMode);
    }
}