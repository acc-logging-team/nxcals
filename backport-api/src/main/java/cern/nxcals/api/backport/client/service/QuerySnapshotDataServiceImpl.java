package cern.nxcals.api.backport.client.service;

import cern.nxcals.api.backport.domain.core.metadata.VariableList;
import cern.nxcals.api.backport.domain.core.metadata.VariableListSet;
import cern.nxcals.api.backport.domain.core.snapshot.Snapshot;
import cern.nxcals.api.backport.domain.core.snapshot.SnapshotCriteria;
import cern.nxcals.api.custom.domain.GroupType;
import cern.nxcals.api.domain.Visibility;
import cern.nxcals.api.extraction.metadata.GroupService;
import cern.nxcals.api.extraction.metadata.queries.Groups;
import cern.nxcals.api.extraction.metadata.queries.StringLikeProperty;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
public class QuerySnapshotDataServiceImpl implements QuerySnapshotDataService {
    @NonNull
    private final GroupService groupService;

    @Override
    public VariableList getVariableListWithName(String name) {
        Condition<Groups> condition = Groups.suchThat().label().in(GroupType.GROUP.toString(), GroupType.SNAPSHOT.toString()).and().name().eq(name);
        return groupService.findOne(condition).map(VariableList::from).orElse(null);
    }

    @Override
    public VariableListSet getVariableListsOfUserWithNameLikeAndDescLike(String userName, String listNamePattern, String listDescriptionPattern) {
        Condition<Groups> condition = Groups.suchThat().label().in(GroupType.GROUP.toString(), GroupType.SNAPSHOT.toString()).and().ownerName().eq(userName);
        condition = restrict(listNamePattern, condition, condition.and().name());
        condition = restrict(listDescriptionPattern, condition, condition.and().description());

        return groupService.findAll(condition).stream().map(VariableList::from).collect(VariableListSet.collector());
    }

    @Override
    public List<Snapshot> getSnapshotsFor(SnapshotCriteria criteria) {
        Condition<Groups> condition = Groups.suchThat().label().eq(GroupType.SNAPSHOT.toString()).and()
                .visibility().eq(criteria.isVisible() ? Visibility.PUBLIC : Visibility.PROTECTED);

        condition = restrict(criteria.getName(), condition, condition.and().name());
        condition = restrict(criteria.getDescription(), condition, condition.and().description());
        condition = restrict(criteria.getOwner(), condition, condition.and().ownerName());

        return groupService.findAll(condition).stream().map(g -> Snapshot.from(g,
                groupService.getVariables(g.getId()))).collect(Collectors.toList());
    }

    private Condition<Groups> restrict(String value, Condition<Groups> condition, StringLikeProperty<Groups> property) {
        if (value == null || "%".equals(value)) {
            return condition;
        } else {
            return property.like(value);
        }
    }

    /**
     * @deprecated this does nothing, please do not use
     */
    @Override
    @Deprecated
    public Snapshot getSnapshotWithAttributes(Snapshot snapshot) {
        return groupService.findById(snapshot.getId()).map(g ->
                Snapshot.from(g, groupService.getVariables(g.getId()))).orElse(snapshot);
    }
}
