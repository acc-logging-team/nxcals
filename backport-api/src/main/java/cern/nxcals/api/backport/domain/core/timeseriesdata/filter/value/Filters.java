package cern.nxcals.api.backport.domain.core.timeseriesdata.filter.value;

import cern.nxcals.api.backport.domain.core.constants.FilterOperand;

public final class Filters {
    private Filters() {
        throw new AssertionError("No instances allowed");
    }

    public static Filter and(Filter f) {
        return and(f, null);
    }

    public static Filter or(Filter f) {
        return or(f, null);
    }

    public static Filter and(Filter f1, Filter f2) {
        return getFilterFor(f1, f2, new LogicalFilter(FilterOperand.AND));
    }

    public static Filter or(Filter f1, Filter f2) {
        return getFilterFor(f1, f2, new LogicalFilter(FilterOperand.OR));
    }

    private static Filter getFilterFor(Filter f1, Filter f2, LogicalFilter op) {
        return new CombinedFilter(f1, f2, op);
    }

}
