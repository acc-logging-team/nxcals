package cern.nxcals.api.backport.domain.core.metadata;

import cern.nxcals.api.backport.domain.core.constants.VariableDataType;
import lombok.NoArgsConstructor;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@NoArgsConstructor
public class VariableSet extends TreeMap<String, Variable> implements Iterable<Variable> {
    private static final long serialVersionUID = 5563509914566768491L;

    /**
     * Creates a new variable set with a variable inside
     *
     * @param variable The Variable object to be added to the VariableSet.
     */
    public VariableSet(Variable variable) {
        addVariable(variable);
    }

    public static VariableSet from(Set<cern.nxcals.api.domain.Variable> vars) {
        VariableSet variableSet = new VariableSet();
        vars.stream().map(Variable::from).forEach(variableSet::addVariable);
        return variableSet;
    }

    public static VariableSet fromCalsVars(Set<Variable> vars) {
        VariableSet variableSet = new VariableSet();
        vars.forEach(variableSet::addVariable);
        return variableSet;
    }

    public static Collector<Variable, VariableSet, VariableSet> collector() {
        return Collector.of(
                VariableSet::new,
                VariableSet::addVariable,
                (set1, set2) -> {
                    set2.values().forEach(set1::addVariable);
                    return set1;
                }
        );
    }

    /**
     * Gets a Variable with the given name from the VariableSet if it exists.
     *
     * @param name - the name of the required Variable.
     * @return the Variable with given name if it exists within the VariableSet. Returns null if does not exist.
     */
    public Variable getVariable(String name) {
        return get(name);
    }

    /**
     * Gets a Variable from the given index in the VariableSet if it exists.
     *
     * @param idx - the index of the required Variable in the VariableSet.
     * @return the Variable with given index if it exists within the VariableSet. Returns null if does not exist.
     * @deprecated you should not try to obtain an n-th element of a set, even if it is an ordered set. Very inefficient.
     */
    @Deprecated
    public Variable getVariable(int idx) {
        Iterator<String> i = keySet().iterator();
        while (idx-- > 0) {
            i.next();
        }
        return get(i.next());
    }

    /**
     * Gets a count of the number of Variable objects within the VariableSet.
     *
     * @return a count of the number of Variable objects within the VariableSet.
     */
    public int getVariableCount() {
        return size();
    }

    /**
     * Get a set of the names of the Variable objects within the VariableSet.
     *
     * @return a set of the names of the Variable objects within the VariableSet.
     */
    public Set<String> getVariableNames() {
        return keySet();
    }

    /**
     * Get a set of the distinct data types of the Variable objects within the VariableSet.
     *
     * @return a set of the distinct data types of the Variable objects within the VariableSet.
     */
    public Set<VariableDataType> getVariableDataTypes() {
        return values().stream().map(Variable::getVariableDataType).collect(Collectors.toCollection(TreeSet::new));
    }

    /**
     * Gets a Collection of all the Variable objects within the VariableSet.
     *
     * @return - a Collection of all the Variable objects within the VariableSet.
     */
    public Collection<Variable> getVariables() {
        return values();
    }

    /**
     * Adds the given Variable object to the VariableSet. If the variable already exists with a different
     * datasource, the datasource is updated
     *
     * @param variable The Variable object to be added to the VariableSet.
     */
    public void addVariable(Variable variable) {
        putIfAbsent(variable.getVariableName(), variable);
    }

    /**
     * Indicates whether of not this object contains a Variable with the given name.
     *
     * @param name - the name of the Variable object to search for.
     * @return - true if this object is representing a Variable with the given name.
     */
    public boolean containsVariable(String name) {
        return containsKey(name);
    }

    @Override
    public Iterator<Variable> iterator() {
        return values().iterator();
    }

    /**
     * Removes the Variable with the given name from the VariableSet.
     *
     * @param name - the name of the Variable to be removed from the VariableSet.
     * @return - true if the Variable with the given name was removed from the VariableSet.
     */
    public boolean removeVariable(String name) {
        return remove(name) != null;
    }

    /**
     * Removes all Variable objects from the VariableSet.
     */
    public void removeAllVariables() {
        clear();
    }

    /**
     * Indicates whether of not this object contains one or more Variable objects of data type Numeric.
     *
     * @return - true if this object contains one or more Variable objects of data type Numeric.
     */
    public boolean containsNumericVariables() {
        return containsDataTypeVariables(VariableDataType.NUMERIC);
    }

    /**
     * Indicates whether of not this object contains one or more Variable objects of data type VectorNumeric.
     *
     * @return - true if this object contains one or more Variable objects of data type VectorNumeric.
     */
    public boolean containsVectorNumericVariables() {
        return containsDataTypeVariables(VariableDataType.VECTOR_NUMERIC);
    }

    /**
     * Indicates whether of not this object contains one or more Variable objects of data type NumericStatus.
     *
     * @return - true if this object contains one or more Variable objects of data type NumericStatus.
     */
    public boolean containsNumericStatusVariables() {
        return containsDataTypeVariables(VariableDataType.NUMERIC_STATUS);
    }

    /**
     * Indicates whether of not this object contains one or more Variable objects of data type String.
     *
     * @return - true if this object contains one or more Variable objects of data type String.
     */
    public boolean containsStringVariables() {
        return containsDataTypeVariables(VariableDataType.TEXT);
    }

    /**
     * Indicates whether of not this object contains one or more Variable objects of data type Fundamental.
     *
     * @return - true if this object contains one or more Variable objects of data type Fundamental.
     */

    public boolean containsFundamentalVariables() {
        return containsDataTypeVariables(VariableDataType.FUNDAMENTAL);
    }

    /**
     * Indicates whether of not this object contains one or more Variable objects of the given data type.
     *
     * @return - true if this object contains one or more Variable objects of the given data type.
     */
    private boolean containsDataTypeVariables(VariableDataType dataType) {
        return values().stream().map(Variable::getVariableDataType).anyMatch(dataType::equals);
    }

    /**
     * Gets a Collection of all the NUMERIC Variable objects within the VariableSet.
     *
     * @return - a Collection of all the NUMERIC Variable objects within the VariableSet.
     */
    public Collection<Variable> getNumericVariables() {
        return getDataTypeVariables(VariableDataType.NUMERIC);
    }

    /**
     * Gets a Collection of all the NUMERIC STATUS Variable objects within the VariableSet.
     *
     * @return - a Collection of all the NUMERIC STATUS Variable objects within the VariableSet.
     */
    public Collection<Variable> getNumericStatusVariables() {
        return getDataTypeVariables(VariableDataType.NUMERIC_STATUS);
    }

    /**
     * Gets a Collection of all the VECTORNUMERIC Variable objects within the VariableSet.
     *
     * @return - a Collection of all the VECTORNUMERIC Variable objects within the VariableSet.
     */
    public Collection<Variable> getVectorNumericVariables() {
        return getDataTypeVariables(VariableDataType.VECTOR_NUMERIC);
    }

    /**
     * Gets a Collection of all the TEXT Variable objects within the VariableSet.
     *
     * @return - a Collection of all the TEXT Variable objects within the VariableSet.
     */
    public Collection<Variable> getStringVariables() {
        return getDataTypeVariables(VariableDataType.TEXT);
    }

    /**
     * Gets a Collection of all the FUNDAMENTAL Variable objects within the VariableSet.
     *
     * @return - a Collection of all the FUNDAMENTAL Variable objects within the VariableSet.
     */
    public Collection<Variable> getFundamentalVariables() {
        return getDataTypeVariables(VariableDataType.FUNDAMENTAL);
    }

    /**
     * Gets a Collection of all the Variable objects within the VariableSet that are of the given dataType.
     *
     * @return - a Collection of all the Variable objects within the VariableSet that are of the given dataType..
     */
    private Collection<Variable> getDataTypeVariables(VariableDataType dataType) {
        return values().stream()
                .filter(t -> t.getVariableDataType().equals(dataType))
                .collect(Collectors.toCollection(TreeSet::new));
    }

    @Override
    public String toString() {
        return keySet().toString();
    }
}
