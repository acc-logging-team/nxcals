package cern.nxcals.api.backport.client.service;

import cern.nxcals.api.backport.domain.core.ClientSelectionDataCriteria;
import cern.nxcals.api.backport.domain.core.constants.LoggingTimeInterval;
import cern.nxcals.api.backport.domain.core.constants.ScaleAlgorithm;
import cern.nxcals.api.backport.domain.core.constants.TimescalingProperties;
import cern.nxcals.api.backport.domain.core.metadata.Variable;
import cern.nxcals.api.backport.domain.core.metadata.VariableSet;
import cern.nxcals.api.backport.domain.core.snapshot.SnapshotProperties;
import cern.nxcals.api.backport.domain.core.timeseriesdata.MultiColumnTimeseriesDataSet;
import cern.nxcals.api.backport.domain.core.timeseriesdata.TimeseriesData;
import cern.nxcals.api.backport.domain.core.timeseriesdata.TimeseriesDataSet;
import cern.nxcals.api.backport.domain.core.timeseriesdata.VariableStatisticsSet;
import cern.nxcals.api.backport.domain.core.timeseriesdata.filter.value.Filter;
import cern.nxcals.api.backport.domain.core.timeseriesdata.spi.DataDistributionBinSet;
import lombok.NonNull;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.sql.Timestamp;
import java.util.List;

@SuppressWarnings("squid:CommentedOutCodeLine")
public interface TimeseriesDataService {

    /**
     * Gets a TimeseriesDataSet containing the timeseries data for the given Variables between the given timestamps.
     * This data will be aggregated so the returned values and timestamps will be derived from the available data
     * according to the given algorithm and frequency.
     *
     * @param variables - the variables for which to get time series data. This can be extracted using the
     *            MetaDataServiceImpl
     * @param startTime - Start of the required time window represented by a Java Timestamp object which can be created
     *            by Timestamp startTime = Timestamp.valueOf("2012-01-01 12:00:00"); Note this should represent the
     *            local time.
     * @param endTime - End of the required time window represented by a Java Timestamp object. NOTE the format must be
     *            the JDBC escape format: yyyy-mm-dd hh:mm:ss
     * @param timescaleProperties - Object representing the scale algorithm and frequency to be applied, This object can
     *            be found at TimescalingProperties
     * @return the TimeseriesDataSet - REMEMBER in the case of Measurements and Logging - all data is time stamped
     *         according to UTC!
     */
    TimeseriesDataSet getDataForMultipleVariablesAggregatedInFixedIntervals(
            VariableSet variables,
            Timestamp startTime, Timestamp endTime, TimescalingProperties timescaleProperties);

    /**
     * Gets a TimeseriesDataSet containing for the given variable with timestamps which correspond to timestamps from
     * the drivingDataSet. If there is no value which corresponds to a timestamp from the drivingDataSet the previous
     * value for the variable in the database will be given the stamp and added to the TimeseriesDataSet.
     *
     * @param variable - the variable for which to get time series data. This can be extracted using the
     *            MetaDataServiceImpl
     * @param drivingDataSet - TimeseriesDataSet used to provide the timestamps which are used to determine the
     *            timestamps in the returned TimeseriesDataSet.
     * @return the TimeseriesDataSet - REMEMBER in the case of Measurements and Logging - all data is time stamped
     *         according to UTC!
     */
    TimeseriesDataSet getDataAlignedToTimestamps(
            Variable variable, TimeseriesDataSet drivingDataSet);

    /**
     * Gets Spark dataset filtered by fundamentals
     *
     * @param variable - the variable for which to get time series data. This can be extracted using the
     *            MetaDataServiceImpl
     * @param startTime - Start of the required time window represented by a Java Timestamp object which can be created
     *            by Timestamp startTime = Timestamp.valueOf("2012-01-01 12:00:00"); Note this should represent the
     *            local time.
     * @param endTime - End of the required time window represented by a Java Timestamp object. NOTE the format must be
     *            the JDBC escape format: yyyy-mm-dd hh:mm:ss
     * @param virtualFundamentals - Fundamental variableSet to filter results with. This can be extracted using the
     *            MetaDataServiceImpl
     * @return the Dataset
     */
    public Dataset<Row> getDatasetFilteredByFundamentals(
            @NonNull Variable variable,
            @NonNull Timestamp startTime, @NonNull Timestamp endTime,
            @NonNull VariableSet virtualFundamentals);

    /**
     * Gets a TimeseriesDataSet containing all the values for the given variable between the given timestamps which
     * correspond to the given fundamental variables.
     *
     * @param variable - the variable for which to get time series data. This can be extracted using the
     *            MetaDataServiceImpl
     * @param startTime - Start of the required time window represented by a Java Timestamp object which can be created
     *            by Timestamp startTime = Timestamp.valueOf("2012-01-01 12:00:00"); Note this should represent the
     *            local time.
     * @param endTime - End of the required time window represented by a Java Timestamp object. NOTE the format must be
     *            the JDBC escape format: yyyy-mm-dd hh:mm:ss
     * @param fundamentals - Fundamental variableSet to filter results with. This can be extracted using the
     *            MetaDataServiceImpl
     * @return the TimeseriesDataSet - REMEMBER in the case of Measurements and Logging - all data is time stamped
     *         according to UTC!
     */
    TimeseriesDataSet getDataInTimeWindowFilteredByFundamentals(
            Variable variable, Timestamp startTime,
            Timestamp endTime, VariableSet fundamentals);

    /**
     * Gets a TimeseriesDataSet containing all the values for the given variable between the given timestamps. If there
     * is any correction available to the data, the data will be corrected according to the correction preferences
     * given.
     *
     * @param variable - the variable for which to get time series data. This can be extracted using the
     *            MetaDataServiceImpl
     * @param startTime - Start of the required time window represented by a Java Timestamp object which can be created
     *            by Timestamp startTime = Timestamp.valueOf("2012-01-01 12:00:00"); Note this should represent the
     *            local time.
     * @param endTime - End of the required time window represented by a Java Timestamp object. NOTE the format must be
     *            the JDBC escape format: yyyy-mm-dd hh:mm:ss
     * @return the TimeseriesDataSet - REMEMBER in the case of Measurements and Logging - all data is time stamped
     *         according to UTC!
     */
    TimeseriesDataSet getDataInTimeWindow(
            Variable variable, Timestamp startTime, Timestamp endTime);

    /**
     * Gets a TimeseriesDataSet containing the timeseries data for the given Variable between the given Java Timestamp
     * objects and the last data point prior to the start timestamp given. If there is any correction available to the
     * data, the data will be corrected according to the correction preferences given.
     *
     * @param variable - the variable for which to get time series data. This can be extracted using the
     *            MetaDataServiceImpl
     * @param startTime - Start of the required time window represented by a Java Timestamp object which can be created
     *            by Timestamp startTime = Timestamp.valueOf("2012-01-01 12:00:00"); Note this should represent the
     *            local time.
     * @param endTime - End of the required time window represented by a Java Timestamp object. NOTE the format must be
     *            the JDBC escape format: yyyy-mm-dd hh:mm:ss
     * @return the TimeseriesDataSet - REMEMBER in the case of Measurements and Logging - all data is time stamped
     *         according to UTC!
     */
    TimeseriesDataSet getDataInTimeWindowAndLastDataPriorToTimeWindowWithinDefaultInterval(
            Variable variable,
            Timestamp startTime, Timestamp endTime);

    /**
     * Gets a TimeseriesDataSet containing the timeseries data for the given Variable between the given Java Timestamp
     * objects. If no data is found in the given range, the last available data (only the first value found) is
     * retrieved prior to the start time up to the given data interval. Otherwise it returns null.
     *
     * @param variable - the variable for which to get time series data. This can be extracted using the
     *            MetaDataServiceImpl
     * @param startTime - Start of the required time window represented by a Java Timestamp object which can be created
     *            by Timestamp startTime = Timestamp.valueOf("2012-01-01 12:00:00"); Note this should represent the
     *            local time.
     * @param endTime - End of the required time window represented by a Java Timestamp object. NOTE the format must be
     *            the JDBC escape format: yyyy-mm-dd hh:mm:ss
     * @param searchInterval - The interval constant within which to search for the last data (hour, day, week , month,
     *            year) this is represented by a LoggingTimeInterval enum which can be found at
     *            LoggingTimeInterval
     * @return the TimeseriesDataSet - REMEMBER in the case of Measurements and Logging - all data is time stamped
     *         according to UTC!
     */
    TimeseriesDataSet getDataInTimeWindowOrLastDataPriorToWindowWithinUserInterval(
            Variable variable,
            Timestamp startTime, Timestamp endTime, LoggingTimeInterval searchInterval);

    /**
     * Gets a timeseriesDataSet which has been filtered so that it only contains datapoints whose timestamp is in the
     * list of timestamps.
     *
     * @param variable - the variable for which to get time series data. This can be extracted using the
     *            MetaDataServiceImpl
     * @param ts - list of Java Timestamp objects to filter the data against. Timestamp objects can be created by
     *            Timestamp startTime = Timestamp.valueOf("2012-01-01 12:00:00"); Note this should represent the local
     *            time.
     * @return the TimeseriesDataSet - REMEMBER in the case of Measurements and Logging - all data is time stamped
     *         according to UTC!
     */
    TimeseriesDataSet getDataFilteredByTimestamps(
            Variable variable, List<Timestamp> ts);

    /**
     * Gets a TimeseriesDataSet containing the timeseries data for the given Variable between the given Java Timestamp
     * objects. If no data is found in the given range, the last available data (only the first value found) is
     * retrieved prior to the start time up to one year. Otherwise it returns null.
     *
     * @param variable - the variable for which to get time series data. This can be extracted using the
     *            MetaDataServiceImpl
     * @param startTime - Start of the required time window represented by a Java Timestamp object which can be created
     *            by Timestamp startTime = Timestamp.valueOf("2012-01-01 12:00:00"); Note this should represent the
     *            local time.
     * @param endTime - End of the required time window represented by a Java Timestamp object. NOTE the format must be
     *            the JDBC escape format: yyyy-mm-dd hh:mm:ss
     * @return the TimeseriesDataSet - REMEMBER in the case of Measurements and Logging - all data is time stamped
     *         according to UTC!
     */
    TimeseriesDataSet getDataInTimeWindowOrLastDataPriorToWindowWithinDefaultInterval(
            Variable variable,
            Timestamp startTime, Timestamp endTime);

    /**
     * Gets a TimeseriesDataSet containing the timeseries data for the given Variable between the given timestamps, and
     * value filtered according to the given variableFilters. If there is no value filter for the variable, or of the
     * varibale is of type NUMERIC_STATUS or FUNDAMENTAL, The an unfiltered data set will be returned.
     *
     * @param variable - the variable for which to get time series data. This can be extracted using the
     *            MetaDataServiceImpl
     * @param startTime - Start of the required time window represented by a Java Timestamp object which can be created
     *            by Timestamp startTime = Timestamp.valueOf("2012-01-01 12:00:00"); Note this should represent the
     *            local time.
     * @param endTime - End of the required time window represented by a Java Timestamp object. NOTE the format must be
     *            the JDBC escape format: yyyy-mm-dd hh:mm:ss
     * @param variableFilters - Object containing information required to allow value filtering this object can be found
     *            at cern.accsoft.backport.extr.domain.core.timeseriesdata.filter.value.VariableValueFilterProperties;
     * @return the TimeseriesDataSet - REMEMBER in the case of Measurements and Logging - all data is time stamped
     *         according to UTC!
     */
    // TODO: to be removed... maybe
//    @Deprecated
//    TimeseriesDataSet getDataInTimeWindowFilteredByValues(Variable variable, Timestamp startTime,
//            Timestamp endTime, AbstractVariableValueFilterProperties<?> variableFilters);

    /** Gets a TimeseriesDataSet containing the timeseries data for the given Variable between the given timestamps, and
     * value filtered according to the given variableFilters. If there is no value filter for the variable, or of the
     * variable is of type NUMERIC_STATUS or FUNDAMENTAL, The an unfiltered data set will be returned.
     *
     * @param variable - the variable for which to get time series data. This can be extracted using the
     *            MetaDataServiceImpl
     * @param startTime - Start of the required time window represented by a Java Timestamp object which can be created
     *            by Timestamp startTime = Timestamp.valueOf("2012-01-01 12:00:00"); Note this should represent the
     *            local time.
     * @param endTime - End of the required time window represented by a Java Timestamp object. NOTE the format must be
     *            the JDBC escape format: yyyy-mm-dd hh:mm:ss
     * @param variableFilters - Object containing information required to allow value filtering this object can be found
     *            at cern.accsoft.backport.extr.domain.core.timeseriesdata.filter.value.VariableValueFilterProperties;
     * @return the TimeseriesDataSet - REMEMBER in the case of Measurements and Logging - all data is time stamped
     *         according to UTC!
     */
    TimeseriesDataSet getDataInTimeWindowFilteredByValues(
            Variable variable, Timestamp startTime,
            Timestamp endTime, Filter variableFilters);

    /**
     * Gets a TimeseriesDataSet containing the timeseries data for the given Variable between the given timestamps. The
     * data will be scaled, so that returned values and timestamps will be derived from the available data, according to
     * the given algorithm and frequency. See cern.accsoft.backport.extr.domain.core.constants for the available scaling
     * ScaleFrequency and ScaleAlgorithm.
     *
     * @param variable - the variable for which to get time series data. This can be extracted using the
     *            MetaDataServiceImpl
     * @param startTime - Start of the required time window represented by a Java Timestamp object which can be created
     *            by Timestamp startTime = Timestamp.valueOf("2012-01-01 12:00:00"); Note this should represent the
     *            local time.
     * @param endTime - End of the required time window represented by a Java Timestamp object. NOTE the format must be
     *            the JDBC escape format: yyyy-mm-dd hh:mm:ss
     * @param timescaleProperties - Object representing the scale algorithm and frequency to be applied, This object can
     *            be found at TimescalingProperties
     * @return the TimeseriesDataSet - REMEMBER in the case of Measurements and Logging - all data is time stamped
     *         according to UTC!
     */
    TimeseriesDataSet getDataInFixedIntervals(
            Variable variable, Timestamp startTime, Timestamp endTime,
            TimescalingProperties timescaleProperties);

    /**
     * Gets a VariableStatisticsSet of VariableStatistics for the Variables in the given VariableSet between the given
     * timestamps. VariableStatistics will include the minimum value and timestamp, maximum value and timestamp, number
     * of values, average value and standard deviation of the values.
     *
     * @param variables - the variables for which to get statistics. This can be extracted using the MetaDataServiceImpl
     * @param startTime - Start of the required time window represented by a Java Timestamp object which can be created
     *            by Timestamp startTime = Timestamp.valueOf("2012-01-01 12:00:00"); Note this should represent the
     *            local time.
     * @param endTime - End of the required time window represented by a Java Timestamp object. NOTE the format must be
     *            the JDBC escape format: yyyy-mm-dd hh:mm:ss
     * @return the variables statistics set
     */
    VariableStatisticsSet getVariableStatisticsOverMultipleVariablesInTimeWindow(
            VariableSet variables,
            Timestamp startTime, Timestamp endTime);

    /**
     * Gets a TimeseriesData representing the maximum value of the variable in the range specified. The timestamp of the
     * TimeseriesData represents the UTC time of the maximum value
     *
     * @param variable - the variable for which to get time series data. This can be extracted using the
     *            MetaDataServiceImpl
     * @param startTime - Start of the required time window represented by a Java Timestamp object which can be created
     *            by Timestamp startTime = Timestamp.valueOf("2012-01-01 12:00:00"); Note this should represent the
     *            local time.
     * @param endTime - End of the required time window represented by a Java Timestamp object. NOTE the format must be
     *            the JDBC escape format: yyyy-mm-dd hh:mm:ss
     * @return The TimeseriesData or null if no data was found in range REMEMBER - in the case of Measurements and
     *         Logging - all data is timestamped according to UTC!
     */
    TimeseriesData getDataHavingMaxValueInTimeWindow(
            Variable variable, Timestamp startTime, Timestamp endTime);

    /**
     * Gets a TimeseriesDataSet containing the timeseries data for the given Vector Variable between the given Java
     * Timestamp objects filtered by the list of indexes given.
     *
     * @param variable - the variable for which to get time series data. This can be extracted using the
     *            MetaDataServiceImpl
     * @param startTime - Start of the required time window represented by a Java Timestamp object which can be created
     *            by Timestamp startTime = Timestamp.valueOf("2012-01-01 12:00:00"); Note this should represent the
     *            local time.
     * @param endTime - End of the required time window represented by a Java Timestamp object. NOTE the format must be
     *            the JDBC escape format: yyyy-mm-dd hh:mm:ss
     * @param indexes - the list of indexes to filter the vector variable
     * @return the TimeseriesDataSet - REMEMBER in the case of Measurements and Logging - all data is time stamped
     *         according to UTC!
     */
    TimeseriesDataSet getVectornumericDataInTimeWindowFilteredByVectorIndices(
            Variable variable,
            Timestamp startTime, Timestamp endTime, int[] indexes);

    /**
     * Gets TimeseriesData corresponding to the data available prior to the given timestamp for the given variable (if
     * found up to one year)
     *
     * @param variable - the variable for which to get time series data. This can be extracted using the
     *            MetaDataServiceImpl
     * @param timestamp - The time which we will get data prior to represented by a Java Timestamp object which can be
     *            created by Timestamp stamp = Timestamp.valueOf("2012-01-01 12:00:00"); Note this should represent the
     *            local time.
     * @return the TimeseriesData - REMEMBER in the case of Measurements and Logging - all data is time stamped
     *         according to UTC!
     */
    TimeseriesData getLastDataPriorToTimestampWithinDefaultInterval(
            Variable variable, Timestamp timestamp);

    /**
     * Gets a TimeseriesData containing the most recent data for the given Variable within one year prior to the
     * timestamp given
     *
     * @param variable - the variable for which to get time series data. This can be extracted using the
     *            MetaDataServiceImpl
     * @param timestamp - The time which we will get data prior to represented by a Java Timestamp object which can be
     *            created by Timestamp stamp = Timestamp.valueOf("2012-01-01 12:00:00"); Note this should represent the
     *            local time.
     * @param searchInterval - The interval constant within which to search for the last data (hour, day, week , month,
     *            year) this is represented by a LoggingTimeInterval enum which can be found at
     *            LoggingTimeInterval
     * @return the TimeseriesData - The last data found or null in case of no values available REMEMBER in the case of
     *         Measurements and Logging - all data is time stamped according to UTC!
     */
    TimeseriesData getLastDataPriorToTimestampWithinUserInterval(
            Variable variable, Timestamp timestamp,
            LoggingTimeInterval searchInterval);

    /**
     * Gets a TimeseriesData corresponding to the last data available for the given variable within one year prior to
     * the current time
     *
     * @param variable - the variable for which to get time series data. This can be extracted using the
     *            MetaDataServiceImpl
     * @return the TimeseriesData - The last data found or null in case of no values available REMEMBER in the case of
     *         Measurements and Logging - all data is time stamped according to UTC!
     */
    TimeseriesData getLastDataPriorToNowWithinDefaultInterval(
            Variable variable);

    /**
     * Gets a TimeseriesData corresponding to the last data available for the given variable within the user defined
     * interval prior to the current time
     *
     * @param variable - the variable for which to get time series data. This can be extracted using the
     *            MetaDataServiceImpl
     * @param searchInterval - The interval constant within which to search for the last data (hour, day, week , month,
     *            year) this is represented by a LoggingTimeInterval enum which can be found at
     *            LoggingTimeInterval
     * @return the TimeseriesData - The last data found or null in case of no values available REMEMBER in the case of
     *         Measurements and Logging - all data is time stamped according to UTC!
     */
    TimeseriesData getLastDataPriorToNowWithinUserInterval(
            Variable variable, LoggingTimeInterval searchInterval);

    /**
     * Gets a TimeseriesData containing the first data for the given Variable up to one year after the timestamp
     * defined.
     *
     * @param variable - the variable for which to get time series data. This can be extracted using the
     *            MetaDataServiceImpl
     * @param timestamp - The time which we will get data prior to represented by a Java Timestamp object which can be
     *            created by Timestamp stamp = Timestamp.valueOf("2012-01-01 12:00:00"); Note this should represent the
     *            local time.
     * @return the TimeseriesData - REMEMBER in the case of Measurements and Logging - all data is time stamped
     *         according to UTC!
     */
    TimeseriesData getNextDataAfterTimestampWithinDefaultInterval(
            Variable variable, Timestamp timestamp);

    /**
     * Gets a TimeseriesData containing the first data for the given Variable up to the user defined interval after the
     * timestamp defined.
     *
     * @param variable - the variable for which to get time series data. This can be extracted using the
     *            MetaDataServiceImpl
     * @param timestamp - The time which we will get data prior to represented by a Java Timestamp object which can be
     *            created by Timestamp stamp = Timestamp.valueOf("2012-01-01 12:00:00"); Note this should represent the
     *            local time.
     * @param searchInterval - The interval constant within which to search for the last data (hour, day, week , month,
     *            year) this is represented by a LoggingTimeInterval enum which can be found at
     *            LoggingTimeInterval
     * @return the TimeseriesData - REMEMBER in the case of Measurements and Logging - all data is time stamped
     *         according to UTC!
     */
    TimeseriesData getNextDataAfterTimestampWithinUserInterval(
            Variable variable, Timestamp timestamp,
            LoggingTimeInterval searchInterval);

    /**
     * Gets a TimeseriesDataSet representing the aggregated values of the variable in the range according to the
     * algorithm specified. The timestamp of the TimeseriesData represents the minimum timestamp found in the range. The
     * Java Timestamp objects should represent the LOCAL time. For example, to query data between 12:00 01/01/2009 UTC
     * and 14:00 01/01/2009 UTC the Java Timestamp objects to be passed to this method can be established as follows:
     * Timestamp startTime = Timestamp.valueOf("2009-01-01 12:00:00"); Timestamp endTime =
     * Timestamp.valueOf("2009-01-01 14:00:00"); NOTE the format must be the JDBC escape format: yyyy-mm-dd hh:mm:ss
     *
     * @param variable - the variables for which to get time series data. This can be extracted using the
     *            MetaDataServiceImpl
     * @param startTime - Start of the required time window represented by a Java Timestamp object which can be created
     *            by Timestamp startTime = Timestamp.valueOf("2012-01-01 12:00:00"); Note this should represent the
     *            local time.
     * @param endTime - End of the required time window represented by a Java Timestamp object. NOTE the format must be
     *            the JDBC escape format: yyyy-mm-dd hh:mm:ss
     * @param algorithm The algorithm to be applied (only ScaleAlgorithm.COUNT, ScaleAlgorithm.AVG or ScaleAlgorithm.SUM
     *            are supported)
     * @return The TimeseriesData or null if no data was found in range REMEMBER - in the case of Measurements and
     *         Logging - all data is timestamped according to UTC!
     */
    TimeseriesData getDataForVariableAggregated(Variable variable, Timestamp startTime, Timestamp endTime,
            ScaleAlgorithm algorithm);

    /**
     * Gets a MultiColumnTimeseriesDataSet containing for the given variable with timestamps which correspond to
     * timestamps from the drivingDataSet. If there is no value which corresponds to a timestamp from the drivingDataSet
     * the previous value for the variable in the database will be given the stamp and added to the TimeseriesDataSet.
     * The data for all Variable objects in the given VariableSet is grouped by timestamp.
     *
     * @param variables - the variable for which to get time series data. This can be extracted using the
     *            MetaDataServiceImpl
     * @param drivingDataSet - TimeseriesDataSet used to provide the timestamps which are used to determine the
     *            timestamps in the returned TimeseriesDataSet.
     * @return the MultiColumnTimeseriesDataSet - REMEMBER in the case of Measurements and Logging - all data is time
     *         stamped according to UTC!
     */
    MultiColumnTimeseriesDataSet getMultiColumnDataAlignedToTimestamps(
            VariableSet variables,
            TimeseriesDataSet drivingDataSet);

    /**
     * Gets a MultiColumnTimeseriesDataSet containing all the values for the given variable between the given
     * timestamps. If there is any correction available to the data, the data will be corrected according to the
     * correction preferences given. The data for all Variable objects in the given VariableSet is grouped by timestamp.
     *
     * @param variables - the variable for which to get time series data. This can be extracted using the
     *            MetaDataServiceImpl
     * @param startTime - Start of the required time window represented by a Java Timestamp object which can be created
     *            by Timestamp startTime = Timestamp.valueOf("2012-01-01 12:00:00"); Note this should represent the
     *            local time.
     * @param endTime - End of the required time window represented by a Java Timestamp object. NOTE the format must be
     *            the JDBC escape format: yyyy-mm-dd hh:mm:ss
     * @return the MultiColumnTimeseriesDataSet - REMEMBER in the case of Measurements and Logging - all data is time
     *         stamped according to UTC!
     */
    MultiColumnTimeseriesDataSet getMultiColumnDataInTimeWindow(
            VariableSet variables, Timestamp startTime,
            Timestamp endTime);

    /**
     * Gets a MultiColumnTimeseriesDataSet containing the timeseries data for the given Variable between the given
     * timestamps. The data will be scaled, so that returned values and timestamps will be derived from the available
     * data, according to the given algorithm and frequency. See cern.accsoft.backport.extr.domain.core.constants for the
     * available scaling ScaleFrequency and ScaleAlgorithm. The data for all Variable objects in the given VariableSet
     * is grouped by timestamp.
     *
     * @param variables - the variable for which to get time series data. This can be extracted using the
     *            MetaDataServiceImpl
     * @param startTime - Start of the required time window represented by a Java Timestamp object which can be created
     *            by Timestamp startTime = Timestamp.valueOf("2012-01-01 12:00:00"); Note this should represent the
     *            local time.
     * @param endTime - End of the required time window represented by a Java Timestamp object. NOTE the format must be
     *            the JDBC escape format: yyyy-mm-dd hh:mm:ss
     * @param timescaleProperties - Object representing the scale algorithm and frequency to be applied, This object can
     *            be found at TimescalingProperties
     * @return the MultiColumnTimeseriesDataSet - REMEMBER in the case of Measurements and Logging - all data is time
     *         stamped according to UTC!
     */
    MultiColumnTimeseriesDataSet getMultiColumnDataInFixedIntervals(
            VariableSet variables, Timestamp startTime,
            Timestamp endTime, TimescalingProperties timescaleProperties);

   
    /**
     * Estimates the data size of the request in MB. The limit is currently set in
     * TimeseriesAccessConstants.DATA_REQUEST_SIZE_MAX_MB to 250MB. If the request represents more than 250MB, it should
     * be split in smaller time windows
     *
     * @param variable - the variables for which to get time series data. This can be extracted using the
     *            MetaDataServiceImpl
     * @param startTime - Start of the required time window represented by a Java Timestamp object which can be created
     *            by Timestamp startTime = Timestamp.valueOf("2012-01-01 12:00:00"); Note this should represent the
     *            local time.
     * @param endTime - End of the required time window represented by a Java Timestamp object. NOTE the format must be
     *            the JDBC escape format: yyyy-mm-dd hh:mm:ss
     * @param fundamentals - The fundamentals filters if it is fundamental filtered, use null if there is no fundamental
     *            filters
     * @param timescaleProperties - Object representing the scale algorithm and frequency to be applied, This object can
     *            be found at TimescalingProperties use null if query is
     *            not time scaled.
     * @return The data size estimation in MB
     */
    // TODO this needs implementing
    double getJVMHeapSizeEstimationForDataInTimeWindow(Variable variable, Timestamp startTime,
            Timestamp endTime, VariableSet fundamentals, TimescalingProperties timescaleProperties);

    /**
     * Gets the size (number of points in the vector) of the given Vectornumeric Variable objects data between the given
     * timestamps (use ISO format of 'YYYY-MM-DD HH24:MI:SS.fff')
     *
     * @param variable - the variables for which to get time series data. This can be extracted using the
     *            MetaDataServiceImpl
     * @param startTime - Start of the required time window represented by a Java Timestamp object which can be created
     *            by Timestamp startTime = Timestamp.valueOf("2012-01-01 12:00:00"); Note this should represent the
     *            local time.
     * @param endTime - End of the required time window represented by a Java Timestamp object. NOTE the format must be
     *            the JDBC escape format: yyyy-mm-dd hh:mm:ss
     * @return the maximum size of the vectornumeric data in the time window
     */
    int getMaxVectornumericElementCountInTimeWindow(Variable variable, Timestamp startTime, Timestamp endTime);

    /**
     * Gets relevant data from the given snapshot. Only publicly visible snapshots are supported unless the caller
     * owns the given snapshot. In that case also private snapshots are supported.
     *
     * @param snapshotName - the snapshot name
     * @return collection of timeseries data; empty list if there's no data for the given snapshot criteria
     * @throws IllegalArgumentException when there's no snapshot with the given name
     */
    default List<TimeseriesDataSet> getDataForSnapshot(String snapshotName) {
        return getDataForSnapshot(snapshotName, null);
    }

    /**
     * Gets relevant data from the given snapshot. The snapshot name + owner pair must be unique. Only snapshots
     * are supported.
     * 
     * @param snapshotName - the snapshot name
     * @param owner - the snapshot owner
     * @return collection of timeseries data; empty list if there's no data for the given snapshot criteria
     * @throws IllegalArgumentException when there's no snapshot with the given name
     */
    default List<TimeseriesDataSet> getDataForSnapshot(String snapshotName, String owner) {
        return getDataForSnapshot(snapshotName, owner, SnapshotProperties.empty());
    }

    /**
     * Gets relevant data from the given snapshot. The snapshot name + owner pair must be unique. Only snapshots
     * are supported. The @param criteria argument allows to override some criteria selection settings. For the moment
     * however only the time window related ones are supported
     * 
     * @param snapshotName - the snapshot name
     * @param owner - the snapshot owner
     * @param criteria - overriden criteria; currently only the time window related ones are supported
     * @return collection of timeseries data; empty list if there's no data for the given snapshot criteria
     * @throws IllegalArgumentException when there's no snapshot with the given name
     */
    List<TimeseriesDataSet> getDataForSnapshot(String snapshotName, String owner,
            ClientSelectionDataCriteria criteria);

    /**
     * Gets a TimeseriesData Set representing the fundamental data within the given time window and with a name matching
     * the given pattern.  This implementation is limited to the pattern matching ACC:LSA_CYCLE:TIMING_USER only.
     *
     * @param filterFundamentalName - pattern used to match to the fundamental variable names against % = wildcard.
     * @param startTime - Start of the required time window represented by a Java Timestamp object which can be created
     *            by Timestamp startTime = Timestamp.valueOf("2012-01-01 12:00:00"); Note this should represent the
     *            local time.
     * @param endTime - End of the required time window represented by a Java Timestamp object. NOTE the format must be
     *            the JDBC escape format: yyyy-mm-dd hh:mm:ss
     * @return TimeseriesDataSet of fundamental data
     */
    TimeseriesDataSet getFundamentalDataInTimeWindowWithFundamentalNamesLikePattern(
            String filterFundamentalName, Timestamp startTime, Timestamp endTime);

    /**
     * Gets a TimeseriesDataSet containing only the timestamps for a given variable with the given value with the value
     * replaced with the value passed.
     *
     * @param variable - the variable for which to get time series data. This can be extracted using the
     *            MetaDataServiceImpl
     * @param startTime - Start of the required time window represented by a Java Timestamp object which can be created
     *            by Timestamp startTime = Timestamp.valueOf("2012-01-01 12:00:00"); Note this should represent the
     *            local time .
     * @param endTime - End of the required time window represented by a Java Timestamp object. NOTE the format must be
     *            the JDBC escape format: yyyy-mm-dd hh:mm:ss
     * @return the TimeseriesDataSet - REMEMBER in the case of Measurements and Logging - all data is time stamped
     *         according to UTC!
     */
    TimeseriesDataSet getDataDistribution(
            Variable variable, Timestamp startTime, Timestamp endTime);

    /**
     * Returs data distribution (quantitity of values in calculated data ranges). As a argument bin number is given-
     * this argument specifies how many intervals should be returned. Size of each bin is calculated dynamically, based
     * on range of given data and number of bins.
     * 
     * @param variable - the variables for which calculate data distribution. This can be extracted using the
     *            MetaDataServiceImpl
     * @param startTime - Start of the required time window represented by a Java Timestamp object which can be created
     *            by Timestamp startTime = Timestamp.valueOf("2012-01-01 12:00:00"); Note this should represent the
     *            local time.
     * @param endTime - End of the required time window represented by a Java Timestamp object. NOTE the format must be
     *            the JDBC escape format: yyyy-mm-dd hh:mm:ss
     * @param binsNumber number of value intervals.
     * @return Collections of objects containing bottom, top limit and number of values in given range.
     */
    DataDistributionBinSet getDataDistributionInTimewindowForBinsNumber(
            Variable variable, Timestamp startTime,
                                                                        Timestamp endTime, int binsNumber);
    
    /**
     * Gets a VariableStatisticsSet of VariableStatistics for the Variables in the given VariableSet between the given
     * timestamps filtered by fundamentals. VariableStatistics will include the minimum value and timestamp, maximum
     * value and timestamp, number of values, average value and standard deviation of the values.
     *
     * @param variables - the variables for which to get statistics. This can be extracted using the MetaDataServiceImpl
     * @param startTime - Start of the required time window represented by a Java Timestamp object which can be created
     *            by Timestamp startTime = Timestamp.valueOf("2012-01-01 12:00:00"); Note this should represent the
     *            local time.
     * @param endTime - End of the required time window represented by a Java Timestamp object. NOTE the format must be
     *            the JDBC escape format: yyyy-mm-dd hh:mm:ss
     * @param fundamentals - Fundamental variableSet to filter results with. This can be extracted using the
     *            MetaDataServiceImpl
     * @return the variable stats
     */
    VariableStatisticsSet getVariableStatisticsOverTimeWindowFilteredByFundamentals(
            VariableSet variables,
            Timestamp startTime, Timestamp endTime, VariableSet fundamentals);

    
    /**
     * INFO: moved from internal API 
     * Gets a TimeseriesDataSet containing the timeseries data for the given Variable between the given Java Timestamp
     * objects filtered by a filter. If no data exists then return the last data point prior to the start timestamp
     * given within the user defined interval. If there is any correction available to the data, the data will be
     * corrected according to the correction preferences given.
     *
     * @param variable - the variable for which to get time series data. This can be extracted using the
     *            MetaDataServiceImpl
     * @param startTime - Start of the required time window represented by a Java Timestamp object which can be created
     *            by Timestamp startTime = Timestamp.valueOf("2012-01-01 12:00:00"); Note this should represent the
     *            local time.
     * @param endTime - End of the required time window represented by a Java Timestamp object. NOTE the format must be
     *            the JDBC escape format: yyyy-mm-dd hh:mm:ss
     * @param interval - The interval constant within which to search for the last data (hour, day, week , month, year)
     *            this is represented by a LoggingTimeInterval enum which can be found at
     *            LoggingTimeInterval
     * @param filter - filter to filter out results with.
     * @return the TimeseriesDataSet - REMEMBER in the case of Measurements and Logging - all data is time stamped
     *         according to UTC!
     */
    TimeseriesDataSet getDataInTimeWindowOrLastDataPriorToWindowWithinUserIntervalFiltered(Variable variable,
            Timestamp startTime, Timestamp endTime, LoggingTimeInterval interval, Filter filter);
}
