package cern.nxcals.api.backport.domain.util;

import cern.nxcals.api.backport.domain.core.constants.BeamModeValue;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TimeseriesAccessConstants {
    /**
     * The logging client application name
     */
    public static final String LOGGING_CLIENT_APPLICATION_NAME_PROPERTY = "logging.client.app.name";
    /**
     * The logging client user name
     */
    public static final String LOGGING_CLIENT_USER_NAME_PROPERTY = "logging.client.user.name";

    public static final String LOGGING_CLIENT_ID_PROPERTY = "official.client.id";

    // /
    // / JDBC properties
    // /
    public static final int STANDARD_QUERY_FETCH_SIZE = 20;
    public static final int DATA_QUERY_FETCH_SIZE = 5000;
    public static final int DVN_QUERY_FETCH_SIZE = 1000;
    public static final String JDBC_LOCAL_PROPERTIES = "jdbc-local.properties";

    public static final double NUMERIC_ELEMENT_SIZE = 0.000050;
    public static final double NUMERIC_STATUS_ELEMENT_SIZE = 0.000080;
    public static final double TEXT_ELEMENT_SIZE = 0.000065;

    public static final double VECTOR_ELEMENT_SIZE_BASE = 0.000064;
    public static final double VECTOR_ELEMENT_SIZE_ELEMENT = 0.000008; // This should be correct (8 bytes for a double)
                                                                       // but apparently there is a kind of
                                                                       // compression....
    public static final long DEFAULT_VECTOR_SIZE = 300;

    /**
     * Defines the max limit in MB of the data set that can be extracted in one go
     */
    public static final double DATA_REQUEST_SIZE_MAX_MB = 400.0;

    /**
     * The maximum amount of time to look in hours
     */
    public static final int COUNT_SAMPLE_DATA_TIME = 1;

    public static final String DEV_LOGDB_DATASOURCE_NAME = "Logging Database (DEV)";
    public static final String TEST_LOGDB_DATASOURCE_NAME = "Logging Database (TEST)";
    public static final String PRO_LOGDB_DATASOURCE_NAME = "Logging Database (PRO)";
    public static final String DEV_MEASDB_DATASOURCE_NAME = "Measurement Database (DEV)";
    public static final String PRO_MEASDB_DATASOURCE_NAME = "Measurement Database (PRO)";

    public static final String TIMBER_DATASOURCE_NAME = "Timber Database";

    public static final int MAX_NUMBER_OF_DERIVATIONS = 3;

    // ///// COLORS //////////////////////////////

    public static final Color VIRTUAL_COLOR = new Color(255, 165, 0);
    public static final Color CORRECTIONS_COLOR = new Color(154, 235, 50);
    public static final Color SELECTION_COLOR = new Color(57, 105, 138);
    public static final Color EVEN_DISPLAY_COLOR = new Color(242, 242, 242);
    public static final ClassCastException VARIABLE_TYPE_EXCEPTION = new ClassCastException(
            "The variable data type is not handled correctly. Please contact BE/CO/DM.");

    private static final List<String> BEAM_MODES = orderBeamModes();
    private static final Color[] PALLET_BEAM_MODES = generateColors(BeamModeValue.values().length);

    private TimeseriesAccessConstants() {
        throw new AssertionError("This should never be used");
    }

    private static List<String> orderBeamModes() {
        ArrayList<String> beamModes = new ArrayList<>();
        for (BeamModeValue bmv : BeamModeValue.values()) {
            beamModes.add(bmv.name());
        }
        Collections.sort(beamModes);
        return beamModes;
    }

    private static Color[] generateColors(int n) {
        Color[] cols = new Color[n];
        for (int i = 0; i < n; i++) {
            cols[i] = Color.getHSBColor((float) i / (float) n, 0.8f, i + 1f);
        }
        return cols;
    }

    public static Color getBeamModeColor(BeamModeValue bmv) {
        return PALLET_BEAM_MODES[BEAM_MODES.indexOf(bmv.name())];
    }

    public static Color getBeamModeColor(String beamModeName) {
        return getBeamModeColor(BeamModeValue.valueOf(beamModeName));
    }
}