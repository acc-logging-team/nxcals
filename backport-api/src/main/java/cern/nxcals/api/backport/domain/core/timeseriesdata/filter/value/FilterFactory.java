/**
 * Copyright (c) 2015 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.backport.domain.core.timeseriesdata.filter.value;



import cern.nxcals.api.backport.domain.core.constants.FilterOperand;
import cern.nxcals.api.backport.domain.core.constants.VariableDataType;
import cern.nxcals.api.backport.domain.core.constants.VectorRestriction;
import cern.nxcals.api.backport.domain.core.metadata.Variable;
import lombok.experimental.UtilityClass;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static cern.nxcals.api.backport.domain.core.constants.FilterOperand.NONE;
import static cern.nxcals.api.backport.domain.core.constants.VariableDataType.FUNDAMENTAL;
import static cern.nxcals.api.backport.domain.core.constants.VariableDataType.MATRIX_NUMERIC;
import static cern.nxcals.api.backport.domain.core.constants.VariableDataType.NUMERIC;
import static cern.nxcals.api.backport.domain.core.constants.VariableDataType.TEXT;
import static cern.nxcals.api.backport.domain.core.constants.VariableDataType.VECTOR_NUMERIC;
import static cern.nxcals.api.backport.domain.core.constants.VariableDataType.VECTOR_STRING;

/**
 * Allows to create filters.
 *
 */
@UtilityClass
public class FilterFactory {
    private static final Map<Class<?>, Factory<?>> factories = new HashMap<>();


    public static Factory<Double> getInstanceForNumerics() {
        return getInstanceFor(Double.class);
    }

    public static Factory<String> getInstanceForTextuals() {
        return getInstanceFor(String.class);
    }

    @SuppressWarnings("unchecked")
    private static synchronized <T> Factory<T> getInstanceFor(Class<T> clazz) {
        Factory<?> factory = factories.get(clazz);
        if (factory == null) {
            factory = createFactoryFor(clazz);
            factories.put(clazz, factory);
        }
        return (Factory<T>) factory;
    }

    @SuppressWarnings("unchecked")
    private static <T> Factory<T> createFactoryFor(Class<T> clazz) {
        if (clazz == Double.class) {
            return (Factory<T>) new NumericFilterFactory();
        }
        if (clazz == String.class) {
            return (Factory<T>) new StringFilterFactory();
        }
        throw new IllegalArgumentException(
                "Only NUMERIC and STRING factories are supported. Please use Double.class and String.class respectively as the method argument");
    }

    public interface Factory<T> {

         Filter withTimePredicate(Variable v);

         Filter withSingleValuePredicate(Variable v, T value, FilterOperand operand);

         Filter withSingleValuePredicate(Variable v, T value, FilterOperand operand,
                VectorRestriction restriction);

         Filter withMultipleValuesPredicate(Variable v, T[] values, FilterOperand operand);

         Filter withMultipleValuesPredicate(Variable v, T[] values, FilterOperand operand,
                VectorRestriction restriction);
    }

    private abstract static class AbstractFactory<T> implements Factory<T> {
        @Override
        public Filter withTimePredicate(Variable v) {
            return this.withSingleValuePredicate(v, null, NONE);
        }

        @Override
        public Filter withSingleValuePredicate(Variable v, T value, FilterOperand operand) {
            return this.withSingleValuePredicate(v, value, operand, null);
        }

        @Override
        public Filter withMultipleValuesPredicate(Variable v, T[] values, FilterOperand operand) {
            return this.withMultipleValuesPredicate(v, values, operand, null);
        }

        protected void validate(Variable v, T[] values, FilterOperand operand, VectorRestriction restriction) {
            this.verifyType(v);
            this.verifyOperand(values, operand);
            if (operand != null && operand != NONE) {
                this.verifyRestriction(v, restriction);
            }
        }

        private void verifyType(Variable v) {
            if (v == null) {
                throw new IllegalArgumentException("Variable must not be null");
            }
            Set<VariableDataType> types = this.getSupportedTypes();
            if (types.contains(v.getVariableDataType())) {
                return;
            }
            throw new IllegalArgumentException(String.format("Variable data type must belong to %s", types));
        }

        private void verifyOperand(T[] values, FilterOperand operand) {
            if (operand == null || operand == NONE) {
                if (values != null && values.length != 0) {
                    throw new IllegalArgumentException(
                            String.format("You must specify operand for the values = %s", Arrays.toString(values)));
                }
                return;
            }
            if (values == null || values.length == 0) {
                throw new IllegalArgumentException(
                        String.format("Operand %s requires value(s) to be used with", operand));
            }
            switch (operand) {
            case BETWEEN:
                if (values.length == 2) {
                    return;
                }
                throw new IllegalArgumentException(
                        String.format("Operand %s requires exactly 2 values to be used with", operand));
            case EQUALS:
            case GREATER:
            case GREATER_OR_EQUALS:
            case LESS:
            case LESS_OR_EQUALS:
                if (values.length == 1) {
                    return;
                }
                throw new IllegalArgumentException(
                        String.format("Operand %s requires exactly 1 value to be used with", operand));
            case IN:
            case NOT_IN:
                if (values.length > 0) {
                    return;
                }
                throw new IllegalArgumentException(
                        String.format("Operand %s requires at least 1 value to be used with", operand));
            default:
                throw new IllegalArgumentException(String.format("Operand %s is not supported", operand));
            }
        }

        private void verifyRestriction(Variable v, VectorRestriction restriction) {
            VariableDataType type = v.getVariableDataType();
            if (type != VECTOR_NUMERIC && type != VECTOR_STRING) {
                return;
            }
            if (restriction == null) {
                throw new IllegalArgumentException("You have to specify VectorRestriction for vector based types");
            }
        }

        protected abstract Set<VariableDataType> getSupportedTypes();
    }

    private static class NumericFilterFactory extends AbstractFactory<Double> {
        private static final Set<VariableDataType> SUPPORTED_TYPES = EnumSet
                .of(NUMERIC, VECTOR_NUMERIC, MATRIX_NUMERIC);

        @Override
        protected Set<VariableDataType> getSupportedTypes() {
            return SUPPORTED_TYPES;
        }

        @Override
        public Filter withSingleValuePredicate(Variable v, Double value, FilterOperand operand,
                VectorRestriction restriction) {
            return this.withMultipleValuesPredicate(v, value != null ? new Double[] { value } : null, operand,
                    restriction);
        }

        @Override
        public Filter withMultipleValuesPredicate(Variable v, Double[] values, FilterOperand operand,
                VectorRestriction restriction) {
            this.validate(v, values, operand, restriction);
            if (restriction == null) {
                return new NumericFilter(v, operand, values);
            }
            return new VectorValueFilter<Double>(v, operand, values, restriction);
        }

    }

    private static class StringFilterFactory extends AbstractFactory<String> {
        private static final Set<VariableDataType> SUPPORTED_TYPES = EnumSet.of(TEXT, FUNDAMENTAL, VECTOR_STRING);

        @Override
        protected Set<VariableDataType> getSupportedTypes() {
            return SUPPORTED_TYPES;
        }

        @Override
        public Filter withSingleValuePredicate(Variable v, String value, FilterOperand operand,
                VectorRestriction restriction) {
            return this.withMultipleValuesPredicate(v, value != null ? new String[] { value } : null, operand,
                    restriction);
        }

        @Override
        public Filter withMultipleValuesPredicate(Variable v, String[] values, FilterOperand operand,
                VectorRestriction restriction) {
            this.validate(v, values, operand, restriction);
            if (restriction == null) {
                return new StringFilter(v, operand, values);
            }
            return new VectorValueFilter<String>(v, operand, values, restriction);
        }
    }

}
