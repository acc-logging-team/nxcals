package cern.nxcals.api.backport.client.service;

import cern.nxcals.api.backport.domain.core.metadata.Variable;
import lombok.Data;

@Data
class ScalarFilterVariable implements LegalColumnName {
    private final Variable variable;
    private final String legalColumnName;
}
