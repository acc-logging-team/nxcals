package cern.nxcals.api.backport.domain.core.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * The VectorNumericFilterSelection defines the possible selection applied for filtering vector numeric value
 */
@AllArgsConstructor
@Getter
public enum VectorRestriction {
    /**
     * Filters the vector numeric data, if at least one of the element respects the filtering criteria.
     */
    SINGLE_ELEMENT(false),

    /**
     * Filters the vector numeric data, if all the elements respects the filtering criteria.
     */
    ALL_ELEMENTS(false),

    /**
     * Returns only elements matching filter
     */
    MATCHED_ELEMENTS(true);

    private final boolean valueFilteringOnly;
}
