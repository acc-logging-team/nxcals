package cern.nxcals.api.backport.domain.core.snapshot;

import org.joda.time.DateTime;

import java.io.Serializable;

public interface PriorTime extends Serializable {
    DateTime time();
    String description();
}
