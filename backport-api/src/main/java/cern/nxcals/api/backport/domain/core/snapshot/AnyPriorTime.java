package cern.nxcals.api.backport.domain.core.snapshot;


import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.joda.time.DateTime;

/**
 * An implementation that allows to set any time with any description.
 */
@AllArgsConstructor
public class AnyPriorTime implements PriorTime {
    @NonNull
    private final DateTime time;
    @NonNull
    private final String description;

    @Override
    public DateTime time() {
        return time;
    }

    @Override
    public String description() {
        return description;
    }
}
