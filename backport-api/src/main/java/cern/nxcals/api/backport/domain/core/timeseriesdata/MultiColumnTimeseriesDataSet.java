package cern.nxcals.api.backport.domain.core.timeseriesdata;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

public interface MultiColumnTimeseriesDataSet extends Serializable, Cloneable {

    /**
     * @return - the columnHeaders, typically the names of the Variable having data in this TimeseriesDataSet
     */
    List<String> getColumnHeaders();

    /**
     * @return - the no of rows (timestamps) in this MultiColumnTimeseriesDataSet
     */
    int size();

    /**
     * @param stamp - the timstamp for which a row of data should be retrieved
     * @return - a row of data corresponding to the given timestamp
     */
    List<String> getRowOfData(Timestamp stamp);

    /**
     * @return - the timestamps for which this MultiColumnTimeseriesDataSet is holding data
     */
    Set<Timestamp> getTimestamps();
}