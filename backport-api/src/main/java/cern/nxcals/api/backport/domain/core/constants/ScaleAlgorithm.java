package cern.nxcals.api.backport.domain.core.constants;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

/**
 * The Enum ScaleAlgorithm is used to retrieve scale algorithms for time scaling operations
 */
public enum ScaleAlgorithm implements Serializable {
    AVG, MIN, MAX, REPEAT, INTERPOLATE, SUM, COUNT;

    public static String getValues() {
        return StringUtils.join(values(), ',');
    }
}
