package cern.nxcals.api.backport.domain.core.constants;

import cern.nxcals.api.backport.domain.util.TimeseriesAccessConstants;
import cern.nxcals.api.domain.VariableDeclaredType;
import lombok.AllArgsConstructor;

import java.io.Serializable;

/**
 * The VariableDataType defines the possible types of a variable
 */
@AllArgsConstructor
public enum VariableDataType implements Serializable {
    ALL("%"),
    NUMERIC("NUMERIC"),
    NUMERIC_STATUS("NUMSTATUS"),
    TEXT("TEXTUAL"),
    VECTOR_NUMERIC("VECTORNUMERIC"),
    FUNDAMENTAL("FUNDAMENTAL"),
    VECTOR_STRING("VECTORSTRING"),
    MATRIX_NUMERIC("MATRIXNUMERIC"),
    UNDETERMINED("UNDETERMINED");

    String description;

    @Override
    public String toString() {
        return this.description;
    }

    public int getPreferredFetchSize() {
        switch (this) {
        case NUMERIC:
            case TEXT:
            case NUMERIC_STATUS:
            case FUNDAMENTAL:
                return TimeseriesAccessConstants.DATA_QUERY_FETCH_SIZE;
            case VECTOR_NUMERIC:
            case VECTOR_STRING:
            case MATRIX_NUMERIC:
                return TimeseriesAccessConstants.DVN_QUERY_FETCH_SIZE;
            default:
                throw new IllegalArgumentException("No fetch size specified for " + this);
        }
    }

    public static VariableDataType getDataType(String s) {
        for (VariableDataType dt : VariableDataType.values()) {
            if (dt.toString().equalsIgnoreCase(s)) {
                return dt;
            }
        }
        return null;
    }

    public static VariableDataType from(VariableDeclaredType declaredType) {
        return declaredType == null ? UNDETERMINED : VariableDataType.valueOf(declaredType.name());
    }
}
