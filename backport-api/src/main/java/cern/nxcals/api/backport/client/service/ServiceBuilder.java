package cern.nxcals.api.backport.client.service;

import cern.nxcals.api.backport.domain.core.constants.DerivationSelection;
import cern.nxcals.api.config.SparkProperties;
import cern.nxcals.api.custom.service.Services;
import cern.nxcals.api.utils.SparkUtils;
import cern.nxcals.common.utils.Lazy;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.apache.spark.sql.SparkSession;

import java.sql.Timestamp;

import static cern.nxcals.api.extraction.metadata.ServiceClientFactory.createEntityService;
import static cern.nxcals.api.extraction.metadata.ServiceClientFactory.createGroupService;
import static cern.nxcals.api.extraction.metadata.ServiceClientFactory.createHierarchyService;
import static cern.nxcals.api.extraction.metadata.ServiceClientFactory.createSystemSpecService;
import static cern.nxcals.api.extraction.metadata.ServiceClientFactory.createVariableService;

/**
 * A builder that allows the users to create services to access the logging data.
 */
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ServiceBuilder {
    private static final String SERVICE_NOT_IMPLEMENTED_YET = "This service is unimplemented";
    private static final String DEFAULT_APP_NAME = "nxcals-backport-app";
    private final Lazy<SparkSession> session;

    public static ServiceBuilder getInstance() {
        return getInstance(SparkProperties.defaults(DEFAULT_APP_NAME));
    }

    public static ServiceBuilder getInstance(@NonNull SparkProperties props) {
        return new ServiceBuilder(new Lazy<>(() -> SparkUtils.createSparkSession(SparkUtils.createSparkConf(props))));
    }

    public static ServiceBuilder getInstance(@NonNull SparkSession session) {
        return new ServiceBuilder(new Lazy<>(() -> session));
    }

    public MetaDataService createMetaService() {
        return new MetaDataServiceImpl(createSystemSpecService(), createEntityService(), createVariableService(),
                createHierarchyService(), createGroupService(), session.get());
    }

    public TimeseriesDataService createTimeseriesService() {
        return new TimeseriesDataServiceImpl(session.get(), createGroupService(),
                createLHCFillService(), createMetaService(), createVariableService(),
                Services.newInstance(session.get()).aggregationService());
    }

    public TimeseriesDataService createTimeseriesService(DerivationSelection correction, Timestamp correctionTime) {
        throw new UnsupportedOperationException(SERVICE_NOT_IMPLEMENTED_YET);
    }

    public LHCFillDataService createLHCFillService() {
        return new LHCFillDataServiceImpl(Services.newInstance(session.get()).fillService());
    }

    public AcquiredParameterValuesService createAcquiredParameterService() {
        return new AcquiredParameterValueServiceImpl(session.get());
    }

    public QuerySnapshotDataService createQuerySnapshotDataService() {
        return new QuerySnapshotDataServiceImpl(createGroupService());
    }
}
