package cern.nxcals.api.backport.domain.core.snapshot;

import cern.nxcals.api.backport.domain.core.ClientSelectionDataCriteria;
import cern.nxcals.api.backport.domain.core.TimeWindow;
import cern.nxcals.api.backport.domain.core.TimeWindowSet;
import cern.nxcals.api.backport.domain.core.constants.BeamModeValue;
import cern.nxcals.api.backport.domain.core.constants.DerivationSelection;
import cern.nxcals.api.backport.domain.core.constants.DynamicTimeDuration;
import cern.nxcals.api.backport.domain.core.constants.LoggingTimeInterval;
import cern.nxcals.api.backport.domain.core.constants.LoggingTimeZone;
import cern.nxcals.api.backport.domain.core.constants.TimescalingProperties;
import cern.nxcals.api.backport.domain.core.metadata.Variable;
import cern.nxcals.api.backport.domain.core.metadata.VariableSet;
import cern.nxcals.api.backport.domain.core.timeseriesdata.filter.value.Filter;
import cern.nxcals.api.custom.domain.FundamentalFilter;
import cern.nxcals.api.custom.domain.util.FundamentalMapper;
import cern.nxcals.api.domain.Group;
import cern.nxcals.api.utils.TimeUtils;
import com.google.common.collect.Iterables;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

@NoArgsConstructor
public class SnapshotProperties implements Serializable, ClientSelectionDataCriteria {
    private static final long serialVersionUID = 8051375954072799970L;
    private final Map<String, String> values = new HashMap<>();
    private final Map<String, VariableSet> variables = new HashMap<>();
    //Needed for the NXCALS-3786 (set from the client-implemented PriorTime
    private PriorTime priorTime;

    /**
     * @deprecated use {@link SnapshotProperties#from(Group, Map)} instead
     */
    @Deprecated
    static SnapshotProperties from(@NonNull Group group) {
        return from(group, new HashMap<>());
    }

    static SnapshotProperties from(@NonNull Group group,
            @NonNull Map<String, Set<cern.nxcals.api.domain.Variable>> variables) {
        SnapshotProperties result = new SnapshotProperties();
        result.values.putAll(group.getProperties());
        enrichWithFundamentalVariables(group, variables)
                .forEach((key, value) -> result.variables.put(key, VariableSet.from(value)));
        return result;
    }

    private static Map<String, Set<cern.nxcals.api.domain.Variable>> enrichWithFundamentalVariables(Group group,
            Map<String, Set<cern.nxcals.api.domain.Variable>> variables) {
        Set<FundamentalFilter> fundamentalFilters = FundamentalMapper.toFundamentals(group.getProperties());
        if (CollectionUtils.isEmpty(fundamentalFilters)) {
            return variables;
        }
        Map<String, Set<cern.nxcals.api.domain.Variable>> result = new HashMap<>(variables);
        result.put(SnapshotPropertyName.getFundamentalFilters.name(), FundamentalVariableFactory.buildFrom(
                fundamentalFilters, group.getSystemSpec()));
        return result;
    }

    public SnapshotProperties(@NonNull Map<SnapshotPropertyName, String> values) {
        values.forEach((key, value) -> this.values.put(key.toString(), value));
    }

    public static SnapshotProperties empty() {
        return new SnapshotProperties(Collections.emptyMap());
    }

    public Map<String, String> getAttributes4Display() {
        Map<String, String> ret = new TreeMap<>();
        for (Map.Entry<SnapshotPropertyName, String> entry : getAttributes(true).entrySet()) {
            ret.put(entry.getKey().getDisplayName(), entry.getValue());
        }
        return ret;
    }

    public Map<SnapshotPropertyName, String> getAttributes(boolean display) {
        Map<SnapshotPropertyName, String> result = new EnumMap<>(SnapshotPropertyName.class);

        for (SnapshotPropertyName type : SnapshotPropertyName.values()) {
            if (!isPropertyAbsent(type)) {
                result.put(type, getProperty(type));
            }
        }

        result.remove(SnapshotPropertyName.getDrivingVariable);
        Variable drivingVariable = getDrivingVariable();
        if (drivingVariable != null) {
            result.put(SnapshotPropertyName.getDrivingVariable, drivingVariable.getVariableName());
        }

        result.remove(SnapshotPropertyName.getXAxisVariable);
        Variable xAxisVariable  = getXAxisVariable();
        if (xAxisVariable != null) {
            result.put(SnapshotPropertyName.getXAxisVariable, xAxisVariable.getVariableName());
        }

        result.remove(SnapshotPropertyName.getSelectedVariables);
        VariableSet selectedVariables = getSelectedVariables();
        if (selectedVariables!= null && !Iterables.isEmpty(selectedVariables)) {
            result.put(SnapshotPropertyName.getSelectedVariables, display ? String.valueOf(selectedVariables) : String.valueOf(selectedVariables.size()));
        }

        result.remove(SnapshotPropertyName.getFundamentalFilters);
        VariableSet fundamentalFilters = getFundamentalFilters();
        if (fundamentalFilters != null && !Iterables.isEmpty(fundamentalFilters)) {
            result.put(SnapshotPropertyName.getFundamentalFilters, display ? String.valueOf(fundamentalFilters) : String.valueOf(fundamentalFilters.size()));
        }

        return Collections.unmodifiableMap(result);
    }

    @Override
    public VariableSet getSelectedVariables() {
        return getVariablesFor(SnapshotPropertyName.getSelectedVariables);
    }

    public void setSelectedVariables(VariableSet set) {
        if (set == null) {
            clearVariablesFor(SnapshotPropertyName.getSelectedVariables);
        } else {
            for (Variable var: set) {
                addVariableFor(SnapshotPropertyName.getSelectedVariables, var);
            }
        }
    }

    @Override
    public VariableSet getFundamentalFilters() {
        return getVariablesFor(SnapshotPropertyName.getFundamentalFilters);
    }

    public void setFundamentalFilters(VariableSet set) {
        if (set == null) {
            clearVariablesFor(SnapshotPropertyName.getFundamentalFilters);
        } else {
            for (Variable var: set) {
                addVariableFor(SnapshotPropertyName.getFundamentalFilters, var);
            }
        }
    }

    @Override
    public boolean isFundamentalFiltered() {
        return !getVariablesFor(SnapshotPropertyName.getFundamentalFilters).isEmpty();
    }

    @Override
    public Variable getDrivingVariable() {
        VariableSet result = getVariablesFor(SnapshotPropertyName.getDrivingVariable);
        if (result.isEmpty()) {
            return null;
        } else {
            return Iterables.getOnlyElement(result);
        }
    }

    public void setDrivingVariable(Variable var) {
        clearVariablesFor(SnapshotPropertyName.getDrivingVariable);
        addVariableFor(SnapshotPropertyName.getDrivingVariable, var);
    }

    public Variable getXAxisVariable() {
        VariableSet result = getVariablesFor(SnapshotPropertyName.getXAxisVariable);
        if (result.isEmpty()) {
            return null;
        } else {
            return Iterables.getOnlyElement(result);
        }
    }

    public void setXAxisVariable(Variable var) {
        clearVariablesFor(SnapshotPropertyName.getXAxisVariable);
        addVariableFor(SnapshotPropertyName.getXAxisVariable, var);
    }

    public List<SimpleSnapshotVariableBean> getAllVariables() {
        List<SimpleSnapshotVariableBean> result = new ArrayList<>();
        for (Map.Entry<String, VariableSet> entry: variables.entrySet()) {
            for (Variable variable : entry.getValue()) {
                result.add(new SimpleSnapshotVariableBean(entry.getKey(), variable));
            }
        }
        return result;
    }

    @Override
    public PriorTime getPriorTime() {
        if (this.priorTime != null) {
            return this.priorTime;
        }
        if (isPropertyAbsent(SnapshotPropertyName.getPriorTime)) {
            return null;
        } else {
            return SimplePriorTime.from(getProperty(SnapshotPropertyName.getPriorTime));
        }
    }

    public void setPriorTime(PriorTime priorTime) {
        this.priorTime = priorTime;

        if (priorTime == null) {
            setProperty(SnapshotPropertyName.getPriorTime, null);
        } else if (priorTime instanceof SimplePriorTime) {
            //We set in properties only our enum, protection against user defined class that cannot be parsed back.
            setProperty(SnapshotPropertyName.getPriorTime, priorTime.description());
        }
    }

    @Override
    public Timestamp getEndTime() {
        if (isPropertyAbsent(SnapshotPropertyName.getEndTime)) {
            return null;
        } else {
            return TimeUtils.getTimestampFromString(getProperty(SnapshotPropertyName.getEndTime));
        }
    }

    @SuppressWarnings("squid:CallToDeprecatedMethod")
    public void setEndTime(Timestamp endTime) {
        if (endTime == null) {
            setProperty(SnapshotPropertyName.getEndTime, null);
        } else {
            setProperty(SnapshotPropertyName.getEndTime, TimeUtils.getStringFromInstant(endTime.toInstant()));
        }
    }

    @Override
    public Timestamp getStartTime() {
        if (isPropertyAbsent(SnapshotPropertyName.getStartTime)) {
            return null;
        } else {
            return TimeUtils.getTimestampFromString(getProperty(SnapshotPropertyName.getStartTime));
        }
    }

    @SuppressWarnings("squid:CallToDeprecatedMethod")
    public void setStartTime(Timestamp getStartTime) {
        if (getStartTime == null) {
            setProperty(SnapshotPropertyName.getStartTime, null);
        } else {
            setProperty(SnapshotPropertyName.getStartTime, TimeUtils.getStringFromInstant(getStartTime.toInstant()));
        }
    }

    public Timestamp getDerivationTime() {
        if (isPropertyAbsent(SnapshotPropertyName.getDerivationTime)) {
            return null;
        } else {
            return TimeUtils.getTimestampFromString(getProperty(SnapshotPropertyName.getDerivationTime));
        }
    }

    @SuppressWarnings("squid:CallToDeprecatedMethod")
    public void setDerivationTime(Timestamp derivationTime) {
        if (derivationTime == null) {
            setProperty(SnapshotPropertyName.getDerivationTime, null);
        } else {
            setProperty(SnapshotPropertyName.getDerivationTime, TimeUtils.getStringFromInstant(derivationTime.toInstant()));
        }
    }

    public String getSelectionOutput() {
        return getProperty(SnapshotPropertyName.getSelectionOutput);
    }

    public void setSelectionOutput(String output) {
        setProperty(SnapshotPropertyName.getSelectionOutput, output);
    }

    public boolean isMetaDataIncluded() {
        return Boolean.valueOf(getProperty(SnapshotPropertyName.isMetaDataIncluded));
    }

    public void setMetaDataIncluded(boolean included) {
        setProperty(SnapshotPropertyName.isMetaDataIncluded, Boolean.toString(included));
    }

    public String getFileFormat() {
        return getProperty(SnapshotPropertyName.getFileFormat);
    }

    public void setFileFormat(String fileFormat) {
        setProperty(SnapshotPropertyName.getFileFormat, fileFormat);
    }

    public String getDeviceName() {
        return getProperty(SnapshotPropertyName.getDeviceName);
    }

    public void setDeviceName(String deviceName) {
        setProperty(SnapshotPropertyName.getDeviceName, deviceName);
    }

    @Override
    public LoggingTimeZone getTimeZone() {
        if (isPropertyAbsent(SnapshotPropertyName.getTimeZone)) {
            return null;
        } else {
            return LoggingTimeZone.valueOf(getProperty(SnapshotPropertyName.getTimeZone));
        }
    }

    public void setTimeZone(LoggingTimeZone timeZone) {
        if (timeZone == null) {
            setProperty(SnapshotPropertyName.getTimeZone, null);
        } else {
            setProperty(SnapshotPropertyName.getTimeZone, timeZone.toString());
        }
    }

    public DerivationSelection getDerivationSelection() {
        if (isPropertyAbsent(SnapshotPropertyName.getDerivationSelection)) {
            return null;
        } else {
            return DerivationSelection.valueOf(getProperty(SnapshotPropertyName.getDerivationSelection));
        }
    }

    public void setDerivationSelection(DerivationSelection selection) {
        if (selection == null) {
            setProperty(SnapshotPropertyName.getDerivationSelection, null);
        } else {
            setProperty(SnapshotPropertyName.getDerivationSelection, selection.toString());
        }
    }

    public boolean isGroupByTimestamp() {
        return Boolean.valueOf(getProperty(SnapshotPropertyName.isGroupByTimestamp));
    }

    public void setGroupByTimestamp(boolean group) {
        setProperty(SnapshotPropertyName.isGroupByTimestamp, Boolean.toString(group));
    }

    public boolean isMultiFilesActivated() {
        return Boolean.valueOf(getProperty(SnapshotPropertyName.isMultiFilesActivated));
    }

    public void setMultiFilesActivated(boolean multi) {
        setProperty(SnapshotPropertyName.isMultiFilesActivated, Boolean.toString(multi));
    }

    public String getChartType() {
        if (isPropertyAbsent(SnapshotPropertyName.getChartType)) {
            return "Versus Time";
        } else {
            return getProperty(SnapshotPropertyName.getChartType);
        }
    }

    public void setChartType(String chartType) {
        setProperty(SnapshotPropertyName.getChartType, chartType);
    }

    public boolean isSelectableTimestampsIncluded() {
        return Boolean.valueOf(getProperty(SnapshotPropertyName.isSelectableTimestampsIncluded));
    }

    public void setSelectableTimestampsIncluded(boolean include) {
        setProperty(SnapshotPropertyName.isSelectableTimestampsIncluded, Boolean.toString(include));
    }

    public boolean isDistributionChartIncluded() {
        return Boolean.valueOf(getProperty(SnapshotPropertyName.isDistributionChartIncluded));
    }

    public void setDistributionChartIncluded(boolean included) {
        setProperty(SnapshotPropertyName.isDistributionChartIncluded, Boolean.toString(included));
    }

    @Override
    public boolean isLastValueAligned() {
        return Boolean.valueOf(getProperty(SnapshotPropertyName.isLastValueAligned));
    }

    public void setLastValueAligned(boolean aligned) {
        setProperty(SnapshotPropertyName.isLastValueAligned, Boolean.toString(aligned));
    }

    public boolean isLastValueSearchedIfNoDataFound() {
        return Boolean.valueOf(getProperty(SnapshotPropertyName.isLastValueSearchedIfNoDataFound));
    }

    public void setLastValueSearchedIfNoDataFound(boolean flag) {
        setProperty(SnapshotPropertyName.isLastValueSearchedIfNoDataFound, Boolean.toString(flag));
    }

    @Override
    public boolean isAlignToEnd() {
        return Boolean.valueOf(getProperty(SnapshotPropertyName.isAlignToEnd));
    }

    public void setAlignToEnd(boolean alignToEnd) {
        setProperty(SnapshotPropertyName.isAlignToEnd, Boolean.toString(alignToEnd));
    }

    public boolean isAlignToWindow() {
        return Boolean.valueOf(getProperty(SnapshotPropertyName.isAlignToWindow));
    }

    public void setAlignToWindow(boolean alignToWindow) {
        setProperty(SnapshotPropertyName.isAlignToWindow, Boolean.toString(alignToWindow));
    }

    @Override
    public boolean isAlignToStart() {
        return Boolean.valueOf(getProperty(SnapshotPropertyName.isAlignToStart));
    }

    public void setAlignToStart(boolean alignToStart) {
        setProperty(SnapshotPropertyName.isAlignToStart, Boolean.toString(alignToStart));
    }

    @Override
    public boolean isEndTimeDynamic() {
        return Boolean.valueOf(getProperty(SnapshotPropertyName.isEndTimeDynamic));
    }

    public void setEndTimeDynamic(boolean end) {
        setProperty(SnapshotPropertyName.isEndTimeDynamic, Boolean.toString(end));
    }

    @Override
    public int getDynamicDuration() {
        if (isPropertyAbsent(SnapshotPropertyName.getDynamicDuration)) {
            return 0;
        } else {
            return Integer.valueOf(getProperty(SnapshotPropertyName.getDynamicDuration));
        }
    }

    public void setDynamicDuration(int duration) {
        setProperty(SnapshotPropertyName.getDynamicDuration, String.valueOf(duration));
    }

    public int getNoOfIntervalsForMultiFiles() {
        if (isPropertyAbsent(SnapshotPropertyName.getNoOfIntervalsForMultiFiles)) {
            return 0;
        } else {
            return Integer.valueOf(getProperty(SnapshotPropertyName.getNoOfIntervalsForMultiFiles));
        }
    }

    public void setNoOfIntervalsForMultiFiles(int number) {
        setProperty(SnapshotPropertyName.getNoOfIntervalsForMultiFiles, String.valueOf(number));
    }

    public LoggingTimeInterval getTimeIntervalTypeForMultiFiles() {
        if (isPropertyAbsent(SnapshotPropertyName.getTimeIntervalTypeForMultiFiles)) {
            return null;
        } else {
            return LoggingTimeInterval.valueOf(getProperty(SnapshotPropertyName.getTimeIntervalTypeForMultiFiles));
        }
    }

    public void setTimeIntervalTypeForMultiFiles(LoggingTimeInterval interval) {
        if (interval == null) {
            setProperty(SnapshotPropertyName.getTimeIntervalTypeForMultiFiles, null);
        } else {
            setProperty(SnapshotPropertyName.getTimeIntervalTypeForMultiFiles, interval.toString());
        }
    }

    public int getCorrelationSettingScale() {
        if (isPropertyAbsent(SnapshotPropertyName.getCorrelationSettingScale)) {
            return 0;
        } else {
            return Integer.valueOf(getProperty(SnapshotPropertyName.getCorrelationSettingScale));
        }
    }

    public void setCorrelationSettingScale(int scale) {
        setProperty(SnapshotPropertyName.getCorrelationSettingScale, String.valueOf(scale));
    }

    @Override
    public boolean isTimeScalingApplied() {
        return getTimeScalingProperties() != null;
    }

    public void setTimeScalingProperties(TimescalingProperties props) {
        if (props == null) {
            setProperty(SnapshotPropertyName.getTimescalingProperties, null);
        } else {
            setProperty(SnapshotPropertyName.getTimescalingProperties, props.toDBString());
        }
    }

    @Override
    public TimescalingProperties getTimeScalingProperties() {
        if (isPropertyAbsent(SnapshotPropertyName.getTimescalingProperties)) {
            return null;
        } else {
            return TimescalingProperties.valueOf(getProperty(SnapshotPropertyName.getTimescalingProperties));
        }
    }

    public LoggingTimeInterval getCorrelationSettingFreq() {
        if (isPropertyAbsent(SnapshotPropertyName.getCorrelationSettingFreq)) {
            return null;
        } else {
            return LoggingTimeInterval.valueOf(getProperty(SnapshotPropertyName.getCorrelationSettingFreq));
        }
    }

    public void setCorrelationSettingFreq(LoggingTimeInterval freq) {
        if (freq == null) {
            setProperty(SnapshotPropertyName.getCorrelationSettingFreq, null);
        } else {
            setProperty(SnapshotPropertyName.getCorrelationSettingFreq, freq.toString());
        }
    }

    public String getPropertyName() {
        return getProperty(SnapshotPropertyName.getPropertyName);
    }

    public void setPropertyName(String name) {
        setProperty(SnapshotPropertyName.getPropertyName, name);
    }

    @Override
    public DynamicTimeDuration getTime() {
        if (isPropertyAbsent(SnapshotPropertyName.getTime)) {
            return null;
        } else {
            return DynamicTimeDuration.valueOf(getProperty(SnapshotPropertyName.getTime));
        }
    }

    public void setTime(DynamicTimeDuration time) {
        if (time == null) {
            setProperty(SnapshotPropertyName.getTime, null);
        } else {
            setProperty(SnapshotPropertyName.getTime, time.toString());
        }
    }

    public void setFillsFiltersBeamModes(BeamModeValue[] fillFiltersBeamModes) {
        if (ArrayUtils.isEmpty(fillFiltersBeamModes)) {
            clearBeamModesFor(SnapshotPropertyName.getFillsFiltersBeamModes);
        } else {
            for (BeamModeValue beamMode: fillFiltersBeamModes) {
                addBeamModeFor(SnapshotPropertyName.getFillsFiltersBeamModes, beamMode);
            }
        }
    }

    @Override
    public BeamModeValue[] getFillsFiltersBeamModes() {
        return getBeamModesFor(SnapshotPropertyName.getFillsFiltersBeamModes);
    }

    public void setSelectedBeamModes(BeamModeValue[] beamModes) {
        if (ArrayUtils.isEmpty(beamModes)) {
            clearBeamModesFor(SnapshotPropertyName.getSelectedBeamModes);
        } else {
            for (BeamModeValue beamMode: beamModes) {
                addBeamModeFor(SnapshotPropertyName.getSelectedBeamModes, beamMode);
            }
        }
    }

    @Override
    public BeamModeValue[] getSelectedBeamModes() {
        return getBeamModesFor(SnapshotPropertyName.getSelectedBeamModes);
    }

    @Override
    public Map<Variable, Filter> getValueFilters() {
        return Collections.emptyMap();
    }

    @Override
    public String getFillInfo() {
        return null;
    }

    @Override
    public String getAdditionalSelectionActionDescription() {
        return null;
    }

    @Override
    public boolean isAlignToSignal() {
        return false;
    }

    @Override
    public boolean isAlignedWithVariable() {
        return false;
    }

    @Override
    public boolean isGetLastDataWhenNone() {
        return false;
    }

    @Override
    public boolean isValueFiltered() {
        return false;
    }

    @Override
    public TimeWindowSet getTimeWindowSet() {
        TimeWindowSet result = new TimeWindowSet(getTimeZone());
        result.addTimeWindow(TimeWindow.newTimeWindow(getStartTime(), getEndTime()));
        return result;
    }

    private void addBeamModeFor(SnapshotPropertyName type, BeamModeValue beamModeValue) {
        values.put(beamModeValue.toString(), type.toString());
    }

    private void clearBeamModesFor(SnapshotPropertyName type) {
        values.entrySet().removeIf(next -> next.getValue().equals(type.toString()));
    }

    private BeamModeValue[] getBeamModesFor(SnapshotPropertyName type) {
        List<BeamModeValue> result = new ArrayList<>();
        for (Map.Entry<String, String> entry : values.entrySet()) {
            if (type.toString().equals(entry.getValue())) {
                result.add(BeamModeValue.valueOf(entry.getKey()));
            }
        }
        return result.toArray(new BeamModeValue[0]);
    }

    private void addVariableFor(SnapshotPropertyName type, Variable variable) {
        if (variable != null) {
            variables.computeIfAbsent(type.toString(), s -> new VariableSet()).put(variable.getVariableName(), variable);
        }
    }

    private void clearVariablesFor(SnapshotPropertyName type) {
        variables.remove(type.toString());
    }

    private VariableSet getVariablesFor(SnapshotPropertyName type) {
        return variables.computeIfAbsent(type.toString(), s -> new VariableSet());
    }

    private boolean isPropertyAbsent(SnapshotPropertyName type) {
        return values.get(type.toString()) == null;
    }

    private void setProperty(SnapshotPropertyName type, String value) {
        values.put(type.toString(), value);
    }

    private String getProperty(SnapshotPropertyName type) {
        return values.get(type.toString());
    }

}
