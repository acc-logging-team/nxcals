package cern.nxcals.api.backport.domain.core.metadata;

import java.io.Serializable;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class HierarchySet implements Serializable {
    private static final long serialVersionUID = 209771934267224093L;
    private final Map<Integer, SortedSet<Hierarchy>> hierarchies = new TreeMap<>();

    public static Collector<Hierarchy, HierarchySet, HierarchySet> collector() {
        return Collector.of(
                HierarchySet::new,
                HierarchySet::addHierarchy,
                (set1, set2) -> {
                    set1.getAllHierachies().forEach(set2::addHierarchy);
                    return set2;
                }
        );
    }

    /**
     * Gets the parent Hierarchy of the given Hierarchy.
     * 
     * @param hierarchy - the Hierarchy of which the parent Hierarchy should be returned.
     * @return the parent Hierarchy of the given Hierarchy.
     */
    public Hierarchy getParentHierarchy(Hierarchy hierarchy) {
        int parentNodeLevel = hierarchy.getNodeLevel() - 1;
        return hierarchies.getOrDefault(parentNodeLevel, Collections.emptySortedSet())
                .stream()
                .filter(h -> h.getHierarchyID() == hierarchy.getParentID())
                .findFirst().orElse(null);
    }

    /**
     * Gets a List of Hierarchy objects which are child nodes of the given Hierarchy.
     * 
     * @param hierarchy - the Hierarchy of which child nodes should be returned.
     * @return a List of Hierarchy objects which are child nodes of the given Hierarchy.
     */
    public SortedSet<Hierarchy> getChildHierarchies(Hierarchy hierarchy) {
        int childNodeLevel = hierarchy.getNodeLevel() + 1;
        return hierarchies.getOrDefault(childNodeLevel, Collections.emptySortedSet())
                .stream()
                .filter(h -> h.getParentID() == hierarchy.getHierarchyID())
                .collect(Collectors.toCollection(TreeSet::new));
    }

    /**
     * Gets a count of the number of Hierarchy objects within the HierarchySet.
     * 
     * @return a count of the number of Hierarchy objects within the HierarchySet.
     */
    public int getHierarchyCount() {
        return hierarchies.values().stream().mapToInt(Set::size).sum();
    }

    /**
     * Gets a List of all the Hierarchy objects within the HierarchySet.
     * 
     * @return - a List of all the Hierarchy objects within the HierarchySet.
     */
    public SortedSet<Hierarchy> getAllHierachies() {
        return hierarchies.values().stream()
                .flatMap(SortedSet::stream)
                .collect(Collectors.toCollection(TreeSet::new));
    }

    /**
     * Gets a Set of Integers which represent the distinct node levels of all Hierarchy objects within the HierarchySet.
     * 
     * @return - a Set of Integers which represent the distinct node levels of all Hierarchy objects within the
     *         HierarchySet.
     */
    public Set<Integer> getHierachyLevels() {
        return hierarchies.keySet();
    }

    /**
     * Gets a count of the Hierarchy objects which are children of the given Hierarchy.
     *
     * @param hierarchy - the Hierarchy for which child Hierarchy objects should be counted.
     * @return a count of the Hierarchy objects which are children of the given Hierarchy.
     */
    public int getChildHierarchyCount(Hierarchy hierarchy) {
        int childNodeLevel = hierarchy.getNodeLevel() + 1;
        return (int) hierarchies.getOrDefault(childNodeLevel, Collections.emptySortedSet())
                .stream()
                .filter(h -> h.getParentID() == hierarchy.getHierarchyID())
                .count();
    }

    /**
     * Gets the child Hierarchy of the given Hierarchy at the given index.
     *
     * @param hierarchy - the parent Hierarchy of which the child Hierarchy at the given index should be returned.
     * @return the child Hierarchy of the given Hierarchy at the given index.
     */
    public Hierarchy getChildHierarchy(Hierarchy hierarchy, int index) {
        SortedSet<Hierarchy> children = getChildHierarchies(hierarchy);

        if (children.size() <= index) {
            return null;
        }

        Iterator<Hierarchy> itr = children.iterator();
        while (index-- > 0) {
            itr.next();
        }
        return itr.next();
    }

    /**
     * Gets the index of the child Hierarchy belonging to the parent Hierarchy.
     * 
     * @param hierarchy - the parent Hierarchy
     * @param child - the child Hierarchy whose index is required
     * @return - the index of the child Hierarchy belonging to the parent Hierarchy.
     */
    public int getIndexOfChildHierarchy(Hierarchy hierarchy, Hierarchy child) {
        Iterator<Hierarchy> itr = getChildHierarchies(hierarchy).iterator();
        int i = 0;
        while (itr.hasNext()) {
            if (itr.next().getHierarchyID() == child.getHierarchyID()) {
                return i;
            }
            i++;
        }
        return -1;
    }

    /**
     * Gets a List of all the Hierarchy objects within the HierarchySet at the given node level.
     * 
     * @return - a List of all the Hierarchy objects within the HierarchySet at the given node level. Returns null if
     *         there are no hierarchies at the requested level.
     */
    public SortedSet<Hierarchy> getHierachies(int nodeLevel) {
        return hierarchies.get(nodeLevel);
    }

    /**
     * Adds the given Hierarchy object to the HierarchySet.
     * 
     * @param hierarchy - the Hierarchy object to be added to the HierarchySet.
     */
    public void addHierarchy(Hierarchy hierarchy) {
        hierarchies.computeIfAbsent(hierarchy.getNodeLevel(), s -> new TreeSet<>()).add(hierarchy);
    }
}