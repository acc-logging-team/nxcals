package cern.nxcals.api.backport.domain.core.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * The ValueFiltering defines the possible types of filters to apply
 */
@Getter
@AllArgsConstructor
@SuppressWarnings("squid:S00116")
public enum FilterOperand {
    /**
     * Applicable for NUMERIC, STRING
     */
    IN("IN"), NOT_IN("NOT IN"),

    /**
     * Applicable for NUMERIC
     */
    GREATER(">"), LESS("<"), GREATER_OR_EQUALS(">="), LESS_OR_EQUALS("<="),
    BETWEEN("BETWEEN"), EQUALS("="), AND("AND"), OR("OR"), NONE("IS NOT NULL");

    private String DBValue;

    public static FilterOperand getFilterOperandFor(String value) {
        for (FilterOperand op : FilterOperand.values()) {
            if (op.getDBValue().equals(value)) {
                return op;
            }
        }
        return null;
    }
}
