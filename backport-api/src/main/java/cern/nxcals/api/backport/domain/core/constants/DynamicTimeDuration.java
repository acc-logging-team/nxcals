package cern.nxcals.api.backport.domain.core.constants;

public enum DynamicTimeDuration {
    SECONDS, MINUTES, HOURS, DAYS, WEEKS, MONTHS, YEARS, FILLS, BEAM_MODES
}
