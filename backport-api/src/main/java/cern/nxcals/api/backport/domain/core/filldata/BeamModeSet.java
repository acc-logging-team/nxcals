package cern.nxcals.api.backport.domain.core.filldata;

import cern.nxcals.api.backport.domain.core.constants.BeamModeValue;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class BeamModeSet extends ArrayList<BeamMode> {
    private static final long serialVersionUID = -1949126889036948444L;

    public BeamModeSet() {
        super();
    }

    public static BeamModeSet from(@NonNull List<cern.nxcals.api.custom.domain.BeamMode> beamModes) {
        BeamModeSet result = new BeamModeSet();
        beamModes.stream().map(BeamMode::from).forEach(result::addBeamMode);
        return result;
    }

    public void addBeamMode(BeamMode beamMode) {
        add(beamMode);
    }

    public BeamMode getBeamMode(int beamModeOccurrence) {
        return get(beamModeOccurrence);
    }

    public BeamModeValue[] getBeamModeValues() {
        return stream().map(BeamMode::getBeamModeValue).toArray(BeamModeValue[]::new);
    }

    public Boolean containsBeamModeValue(BeamModeValue beamModeValue) {
        return stream().map(BeamMode::getBeamModeValue).anyMatch(t -> t.equals(beamModeValue));
    }

    public BeamModeSet getBeamModes(BeamModeValue beamMode) {
        return stream().filter(t -> t.getBeamModeValue().equals(beamMode)).collect(Collectors.toCollection(BeamModeSet::new));
    }
}