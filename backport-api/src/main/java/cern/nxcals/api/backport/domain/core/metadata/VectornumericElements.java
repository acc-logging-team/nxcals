package cern.nxcals.api.backport.domain.core.metadata;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class VectornumericElements implements Serializable, Iterable<Entry<Integer, String>> {
    private static final long serialVersionUID = 5206784144453236850L;

    private Map<Integer, String> elements = new TreeMap<>();

    public void addElement(Integer elementIndex, String elementName) {
        elements.put(elementIndex, elementName);
    }

    public int size() {
        return elements.size();
    }

    @Override
    public Iterator<Entry<Integer, String>> iterator() {
        return elements.entrySet().iterator();
    }

    public int getElementIndex(String elementName) {
        if (elements.containsValue(elementName)) {
            for (Entry<Integer, String> elementEntry : elements.entrySet()) {
                if (elementEntry.getValue().equals(elementName)) {
                    return elementEntry.getKey();
                }
            }
        }
        return -1;
    }

    public String getElementName(int elementIndex) {
        return elements.get(elementIndex);
    }

}
