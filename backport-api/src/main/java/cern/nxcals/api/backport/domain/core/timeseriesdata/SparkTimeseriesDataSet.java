package cern.nxcals.api.backport.domain.core.timeseriesdata;

import cern.nxcals.api.backport.domain.core.constants.LoggingTimeZone;
import cern.nxcals.api.backport.domain.core.constants.VariableDataType;
import cern.nxcals.api.backport.domain.core.metadata.TimeseriesDataSetMetaData;
import cern.nxcals.api.backport.domain.core.metadata.Variable;
import com.google.common.collect.Comparators;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static cern.nxcals.api.backport.domain.core.metadata.FundamentalData.NXCALS_FUNDAMENTAL_TIMESTAMP;
import static cern.nxcals.api.custom.service.aggregation.AggregationConstants.AGGREGATION_TIMESTAMP_FIELD;
import static cern.nxcals.api.custom.service.aggregation.AggregationConstants.AGGREGATION_VALUE_FIELD;
import static cern.nxcals.common.SystemFields.NXC_EXTR_TIMESTAMP;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VALUE;
import static org.apache.spark.sql.functions.col;

/*
 TODO: this class implements publicly available methods from TimeseriesDataSet. However, there is a number
 of public methods in the original interface's implementation (TimeseriesDataSetImpl) that may, or may not have to be
 backported.
 */

@Getter
@EqualsAndHashCode(callSuper = true)
@SuppressWarnings("java:S1168") // returning null instead of empty collection, due to CALS API semantics.
public class SparkTimeseriesDataSet extends ArrayList<TimeseriesData> implements
        TimeseriesDataSet {
    private final Variable variable;
    private final Timestamp minStamp;
    private final Timestamp maxStamp;

    private SparkTimeseriesDataSet(Variable variable, List<TimeseriesData> data) {
        super(data);
        this.variable = variable;

        if (data.isEmpty()) {
            minStamp = new Timestamp(-1);
            maxStamp = new Timestamp(-1);
        } else {
            minStamp = data.get(0).getStamp();
            maxStamp = data.get(data.size() - 1).getStamp();
        }
    }

    public static TimeseriesDataSet of(Variable variable, List<TimeseriesData> data) {
        if (!Comparators.isInOrder(data, Comparator.naturalOrder())) {
            data = data.stream().sorted().collect(Collectors.toList());
        }

        return new SparkTimeseriesDataSet(variable, Objects.requireNonNull(data));

    }

    public static TimeseriesDataSet ofSorted(Variable variable, List<TimeseriesData> data) {
        return new SparkTimeseriesDataSet(Objects.requireNonNull(variable), Objects.requireNonNull(data));
    }

    private static List<TimeseriesData> getTimeSeriesDataList(Dataset<Row> dataset, boolean isDataDistribution) {
        Stream<Row> result = dataset.sort(NXC_EXTR_TIMESTAMP.getValue())
                .collectAsList().stream();
        if (isDataDistribution) {
            return result.map(SparkTimeseriesData::ofDataDistribution).collect(Collectors.toList());
        } else {
            return result.map(SparkTimeseriesData::of).collect(Collectors.toList());
        }
    }

    public static TimeseriesDataSet of(Variable variable, Dataset<Row> dataset) {
        return new SparkTimeseriesDataSet(variable, getTimeSeriesDataList(dataset, false));
    }

    public static TimeseriesDataSet ofFundamentals(Dataset<Row> dataset) {
        return new SparkTimeseriesDataSet(null, dataset
                .sort(NXCALS_FUNDAMENTAL_TIMESTAMP)
                .collectAsList()
                .stream()
                .map(SparkTimeseriesData::ofFundamental)
                .collect(Collectors.toList()));
    }

    public static TimeseriesDataSet ofDataDistribution(Variable variable, Dataset<Row> dataset) {
        return new SparkTimeseriesDataSet(variable, getTimeSeriesDataList(dataset, true));
    }

    public static TimeseriesDataSet ofSorted(Variable variable, Dataset<Row> dataset) {
        return new SparkTimeseriesDataSet(variable, dataset
                .collectAsList()
                .stream()
                .map(SparkTimeseriesData::of)
                .collect(Collectors.toList()));
    }

    public static TimeseriesDataSet ofAggregated(Variable variable, Dataset<Row> dataset) {
        Dataset<Row> datasetWithTimestamp = dataset
                .withColumn(NXC_EXTR_TIMESTAMP.getValue(), col(AGGREGATION_TIMESTAMP_FIELD))
                .withColumn(NXC_EXTR_VALUE.getValue(), col(AGGREGATION_VALUE_FIELD))
                .drop(AGGREGATION_TIMESTAMP_FIELD, AGGREGATION_VALUE_FIELD);
        return of(variable, datasetWithTimestamp);
    }

    @Override
    public TimeseriesData getTimeseriesData(int index) {
        return index < size() && index >= 0 ? get(index) : null;
    }

    @Override
    public String getVariableName() {
        return variable.getVariableName();
    }

    @Override
    public String getUnit() {
        return variable.getUnit();
    }

    @Override
    public VariableDataType getVariableDataType() {
        return variable.getVariableDataType();
    }

    @Override
    public TimeseriesData getTimeseriesDataOrLast(Timestamp timestamp) {
        if (timestamp == null || minStamp.after(timestamp)) {
            return null;
        }

        if (!maxStamp.after(timestamp)) {
            return get(size() - 1);
        }

        int tmp = 1;
        while (!get(tmp).getStamp().after(timestamp)) {
            tmp++;
        }
        return get(tmp - 1);
    }

    @Override
    public TimeseriesData getTimeseriesData(Timestamp timestamp) {
        if (timestamp == null || minStamp.after(timestamp) || maxStamp.before(timestamp)) {
            return null;
        }

        int tmp = 0;
        while (get(tmp).getStamp().before(timestamp)) {
            tmp++;
        }
        return get(tmp).getStamp().equals(timestamp) ? get(tmp) : null;
    }

    @Override
    public String getMetaDataInfo(LoggingTimeZone timeZone) {
        // TODO
        return null;
    }

    @Override
    public TimeseriesDataSetMetaData getMetaData() {
        // TODO
        return null;
    }

    @Override
    public TimeseriesDataSet alignToTimestamps(
            TimeseriesDataSet required) {
        try {
            List<TimeseriesData> result = new ArrayList<>();
            int d = required.size() - 1;
            int a = size() - 1;
            TimeseriesData desired = required.getTimeseriesData(d);
            TimeseriesData actual = getTimeseriesData(a);

            while (d >= 0) {
                while (a >= 0 && actual.after(desired)) {
                    actual = getTimeseriesData(--a);
                }

                if (a < 0) {
                    break;
                }

                while (d >= 0 && !actual.after(desired)) {
                    result.add(actual.alignToTimestamp(desired.getStamp()));
                    desired = required.getTimeseriesData(--d);
                }
            }

            Collections.reverse(result);
            return new SparkTimeseriesDataSet(variable, result);
        } catch (NoSuchMethodException e) {
            return null;
        }
    }

    // antipattern method
    // maintained for compatibility

    /**
     * @deprecated (never use, antipattern)
     */
    @Override
    @Deprecated
    public void print(LoggingTimeZone utcTime) {
        // TODO
    }

    // mutative methods. Should never be used. By anyone. Ever.
    // maintained for interface compatibility

    /**
     * @deprecated (never use, major performance hit, could have led to unpredictable states)
     */
    @Override
    @Deprecated
    public void addTimeseriesData(TimeseriesData data) {
        throw unsupportedOperation();
    }

    /**
     * @deprecated (never use, major performance hit, could have led to unpredictable states)
     */
    @Override
    @Deprecated
    public void append(TimeseriesDataSet data) {
        throw unsupportedOperation();
    }

    /**
     * @deprecated (never use, major performance hit, could have led to unpredictable states)
     */
    @Override
    @Deprecated
    public boolean add(TimeseriesData data) {
        throw unsupportedOperation();
    }

    /**
     * @deprecated (never use, major performance hit, could have led to unpredictable states)
     */
    @Override
    @Deprecated
    public boolean remove(Object o) {
        throw unsupportedOperation();
    }

    /**
     * @deprecated (never use, major performance hit, could have led to unpredictable states)
     */
    @Override
    @Deprecated
    public boolean addAll(Collection<? extends TimeseriesData> c) {
        throw unsupportedOperation();
    }

    /**
     * @deprecated (never use, major performance hit, could have led to unpredictable states)
     */
    @Override
    @Deprecated
    public boolean removeAll(Collection<?> c) {
        throw unsupportedOperation();
    }

    /**
     * @deprecated (never use, major performance hit, could have led to unpredictable states)
     */
    @Override
    @Deprecated
    public boolean retainAll(Collection<?> c) {
        throw unsupportedOperation();
    }

    /**
     * @deprecated (never use, major performance hit, could have led to unpredictable states)
     */
    @Override
    @Deprecated
    public void clear() {
        throw unsupportedOperation();
    }

    private RuntimeException unsupportedOperation() {
        return new UnsupportedOperationException("Do not use");
    }

}
