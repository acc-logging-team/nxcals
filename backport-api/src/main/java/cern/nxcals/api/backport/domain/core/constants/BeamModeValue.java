package cern.nxcals.api.backport.domain.core.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

/**
 * The Beam Mode Value
 */
@AllArgsConstructor
@Getter
@SuppressWarnings("squid:CommentedOutCodeLine")
public enum BeamModeValue implements Serializable {
    /*
     * Generates enum constants
     * select substr(variable_name,10) || '("' || description || '"),' from da_meta_variables where variable_name like
     * 'HX:BMODE%' and variable_name != 'HX:BMODE';
     */
    //@formatter:off
    SETUP("Setup"),
    INJPHYS("Nominal injection"),
    PRERAMP("Before ramp"),
    RAMP("Ramp"),
    FLATTOP("Flat top"),
    SQUEEZE("Squeeze"),
    ADJUST("Adjust beam on flat top"),
    STABLE("Stable beam for physics"),
    BEAMDUMP("Beam dump"),
    RAMPDOWN("Ramp down"),
    NOBEAM("No beam or preparation for beam"),
    CYCLING("Pre-cycle before injection, no beam"),
    INJPROB("Pilot injection"),
    INJSTUP("Intermediate injection"),
    INJDUMP("Inject and dump"),
    CIRCDUMP("Circulate and dump"),
    UNSTABLE("Unstable beam"),
    RECOVERY("Recovering"),
    WBDUMP("Warning beam dump"),
    ABORT("Recovery after a beam permit flag drop"),
    NOMODE("No mode, data is not available, not set"),
    TEST_BEAM_MODE1("TEST_BEAM_MODE1 for system tests only"),
    TEST_BEAM_MODE2("TEST_BEAM_MODE2 for system tests only"),
    TEST_BEAM_MODE3("TEST_BEAM_MODE2 for system tests only"),
    UNKNOWN("unknown");
    //@formatter:on

    private final String description;

    public static String getValues() {
        return StringUtils.join(values(), ',');
    }

    public static boolean isBeamModeValue(String variableName) {
        try {
            BeamModeValue.valueOf(variableName);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    public static BeamModeValue[] parseBeamModes(String string) {
        final String[] beamModeNames = string.replaceAll("\\[|\\]| ", "").split(",");
        final BeamModeValue[] beamModes = new BeamModeValue[beamModeNames.length];
        for (int i = 0; i < beamModeNames.length; i++) {
            if (BeamModeValue.isBeamModeValue(beamModeNames[i]))
                beamModes[i] = BeamModeValue.valueOf(beamModeNames[i]);
        }
        return beamModes;
    }
}
