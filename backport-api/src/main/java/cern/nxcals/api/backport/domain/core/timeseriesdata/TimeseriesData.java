package cern.nxcals.api.backport.domain.core.timeseriesdata;

import cern.nxcals.api.backport.domain.core.constants.LoggingTimeZone;
import cern.nxcals.api.backport.domain.core.constants.VariableDataType;

import java.math.BigDecimal;
import java.sql.Timestamp;

@SuppressWarnings("squid:S1133") // deprecated code
public interface TimeseriesData extends Cloneable, Comparable<TimeseriesData> {

    /**
     * Gets the data type of time series data.
     *
     * @return the data type of time series data.
     */
    Class<?> type();

    /**
     * Gets the data dimensions of time series data.
     *
     * @return the data dimensions of time series data.
     */
    int[] dimensions();

    /**
     * @see TimeseriesDataSet#getVariableDataType()
     */
    VariableDataType getVariableDataType();

    /**
     * Gets the Status associated with the time series data.
     * 
     * @return the Status associated with the time series data.
     * @throws NoSuchMethodException - if an attempt is made to retrieve the Status of time series data that does not
     *             have a status associated with it i.e. it is not of the relevant datatype.
     */
    String getNumericStatus() throws NoSuchMethodException;

    /**
     * Gets the Numeric value associated with the time series data.
     * 
     * @return the Numeric value associated with the time series data.
     * @throws NoSuchMethodException - if an attempt is made to retrieve the Numeric value of time series data that does
     *             not have a Numeric Value associated with it i.e. it is not of the relevant datatype.
     * @deprecated Use getDoubleValue instead
     */
    @Deprecated
    BigDecimal getNumericValue() throws NoSuchMethodException;

    /**
     * Gets the Numeric value associated with the time series data.
     * 
     * @return the Numeric value associated with the time series data.
     * @throws NoSuchMethodException - if an attempt is made to retrieve the Numeric value of time series data that does
     *             not have a Numeric Value associated with it i.e. it is not of the relevant datatype.
     * @throws ArithmeticException - In case the implementation is in a long and can't be converted to double, because
     *             of precision loss
     */
    double getDoubleValue() throws NoSuchMethodException;

    /**
     * Gets the Numeric value associated with the time series data.
     * 
     * @return the Numeric value associated with the time series data.
     * @throws NoSuchMethodException - if an attempt is made to retrieve the Numeric value of time series data that does
     *             not have a Numeric Value associated with it i.e. it is not of the relevant datatype.
     * @throws ArithmeticException - In case the implementation is in a double and can't be converted to long, because
     *             of precision loss
     */
    long getLongValue() throws NoSuchMethodException;

    /**
     * Gets the timestamp of the timeseries data.
     * 
     * @return - the timestamp of the timeseries data.
     */
    Timestamp getStamp();

    boolean after(TimeseriesData timeseriesData);
    boolean before(TimeseriesData timeseriesData);

    /**
     * Gets the Varchar value associated with the time series data.
     * 
     * @return the Varchar value associated with the time series data.
     * @throws NoSuchMethodException - if an attempt is made to retrieve the Varchar value of time series data that does
     *             not have a Varchar value associated with it i.e. it is not of the relevant datatype.
     */
    String getVarcharValue() throws NoSuchMethodException;

    /**
     * Gets the Varchar value associated with the time series data.
     * 
     * @return the Varchar value associated with the time series data.
     * @throws NoSuchMethodException - if an attempt is made to retrieve the destination value of time series data that
     *             does not have a destination value associated with it i.e. it is not of the fundamental datatype.
     */
    String getDestination() throws NoSuchMethodException;

    /**
     * Gets the Vector of Numeric values associated with the time series data.
     * 
     * @return the Vector of Numeric values associated with the time series data.
     * @throws NoSuchMethodException - if an attempt is made to retrieve the Vector of Numeric values of the time series
     *             data that does not have a Vector of Numeric values associated with it i.e. it is not of the relevant
     *             datatype.
     * @deprecated use getDoubleValues() instead
     */
    @Deprecated
    BigDecimal[] getVectorNumericValues() throws NoSuchMethodException;

    /**
     * Gets the Vector of Numeric values associated with the time series data.
     * 
     * @return the Vector of Numeric values associated with the time series data.
     * @throws NoSuchMethodException - if an attempt is made to retrieve the Vector of Numeric values of the time series
     *             data that does not have a Vector of Numeric values associated with it i.e. it is not of the relevant
     *             datatype.
     * @throws ArithmeticException - In case the implementation is in long and can't be converted to double, because of
     *             precision loss
     */
    double[] getDoubleValues() throws NoSuchMethodException;

    /**
     * Gets the Vector of Numeric values associated with the time series data.
     * 
     * @return the Vector of Numeric values associated with the time series data.
     * @throws NoSuchMethodException - if an attempt is made to retrieve the Vector of Numeric values of the time series
     *             data that does not have a Vector of Numeric values associated with it i.e. it is not of the relevant
     *             datatype.
     * @throws ArithmeticException - In case the implementation is in a double and can't be converted to long, because
     *             of precision loss
     */
    long[] getLongValues() throws NoSuchMethodException;

    double[][] getMatrixDoubleValues() throws NoSuchMethodException;

    long[][] getMatrixLongValues() throws NoSuchMethodException;

    /**
     * @param timeZone - the timezone which should be used to format the timestamp of this data
     * @return - a String representing the timestamp of this data, formatted according to the given timezone
     */
    String getFormattedStamp(LoggingTimeZone timeZone);

    void print(LoggingTimeZone timezone);

    /**
     * Gets numeric value from the vectornumeric value at the given index.
     * 
     * @return the numeric value from the vectornumeric value at the given index or null if there is no value at the
     *         given index.
     * @deprecated use getDoubleValueAtIndex instead
     */
    @Deprecated
    BigDecimal getNumericValueAtVectorIndex(int index) throws NoSuchMethodException;

    /**
     * Gets numeric value from the vectornumeric value at the given index.
     * 
     * @return the numeric value from the vectornumeric value at the given index or null if there is no value at the
     *         given index.
     * @throws ArithmeticException - In case the implementation is in long and can't be converted to double, because of
     *             precision loss
     * @throws IndexOutOfBoundsException - In case the index is not valid
     */
    double getDoubleValueAtIndex(int index) throws NoSuchMethodException;

    /**
     * Gets numeric value from the vectornumeric value at the given index.
     * 
     * @return the numeric value from the vectornumeric value at the given index or null if there is no value at the
     *         given index.
     * @throws ArithmeticException - In case the implementation is in a double and can't be converted to long, because
     *             of precision loss
     * @throws IndexOutOfBoundsException - In case the index is not valid
     */
    long getLongValueAtIndex(int index) throws NoSuchMethodException;

    /**
     * Indicates if this Timeseries Data is a subset of the actual logged vectornumerioc value as a result of filtering
     * by index or value.
     * 
     * @return true if this Timeseries Data is a subset of the actual logged vectornumerioc value.
     */
    Boolean isSubSet() throws NoSuchMethodException;

    /**
     * Gets the array of vectornumeric element indexes. This is useful in the case that this vectornumeric data is a
     * subset of the actual logged vectornumerioc value (as a result of filtering by index or value).
     * 
     * @return the array of vectornumeric element indexes.
     * @deprecated Use getVectorIndexes instead
     */
    @Deprecated
    int[] getVectorNumericIndexes() throws NoSuchMethodException;

    int[] getVectorIndexes() throws NoSuchMethodException;

    String[] getStringValues() throws NoSuchMethodException;

    String getStringValueAtIndex(int index) throws NoSuchMethodException;

    /**
     * Represents the value in a string
     * 
     * @return The value in a string
     */
    String toStringValue() throws NoSuchMethodException;

    /**
     * Checks whether the value is null
     * 
     * @return
     * @throws NoSuchMethodException
     */
    boolean isNullValue() throws NoSuchMethodException;

    /**
     * Gets the string of a value for a given index for a vector numeric type
     * 
     * @param index
     * @return
     * @throws NoSuchMethodException
     */
    String toStringValue(int index) throws NoSuchMethodException;

    /**
     * Checks whether the value is null for a given index (vector numeric types)
     * 
     * @return
     * @throws NoSuchMethodException
     */
    boolean isNullValue(int index) throws NoSuchMethodException;

    TimeseriesData alignToTimestamp(Timestamp timestamp) throws NoSuchMethodException;

    int compareValueTo(TimeseriesData data) throws NoSuchMethodException;

    String getName();
}
