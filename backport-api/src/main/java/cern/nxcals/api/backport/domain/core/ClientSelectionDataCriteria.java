package cern.nxcals.api.backport.domain.core;

import cern.nxcals.api.backport.domain.core.constants.BeamModeValue;
import cern.nxcals.api.backport.domain.core.constants.DynamicTimeDuration;
import cern.nxcals.api.backport.domain.core.constants.LoggingTimeZone;
import cern.nxcals.api.backport.domain.core.constants.TimescalingProperties;
import cern.nxcals.api.backport.domain.core.metadata.Variable;
import cern.nxcals.api.backport.domain.core.metadata.VariableSet;
import cern.nxcals.api.backport.domain.core.snapshot.PriorTime;
import cern.nxcals.api.backport.domain.core.timeseriesdata.filter.value.Filter;

import java.sql.Timestamp;
import java.util.Map;

public interface ClientSelectionDataCriteria {

    VariableSet getSelectedVariables();

    boolean isAlignToSignal();

    boolean isAlignToEnd();

    boolean isAlignToStart();

    Map<Variable, Filter> getValueFilters();

    boolean isTimeScalingApplied();

    TimescalingProperties getTimeScalingProperties();

    boolean isAlignedWithVariable();

    boolean isGetLastDataWhenNone();

    boolean isLastValueAligned();

    boolean isValueFiltered();

    TimeWindowSet getTimeWindowSet();

    Timestamp getStartTime();

    Timestamp getEndTime();

    Variable getDrivingVariable();

    LoggingTimeZone getTimeZone();

    String getFillInfo();

    String getAdditionalSelectionActionDescription();

    boolean isFundamentalFiltered();

    VariableSet getFundamentalFilters();

    DynamicTimeDuration getTime();

    int getDynamicDuration();

    PriorTime getPriorTime();

    BeamModeValue[] getFillsFiltersBeamModes();

    BeamModeValue[] getSelectedBeamModes();

    boolean isEndTimeDynamic();

}