package cern.nxcals.api.backport.domain.core.filldata;

import cern.nxcals.api.backport.domain.core.constants.BeamModeValue;
import com.google.common.base.Preconditions;
import lombok.NonNull;
import lombok.Value;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;

import static cern.nxcals.api.utils.TimeUtils.getTimestampFromInstant;


@Value
public class BeamMode implements Serializable {
    private static final long serialVersionUID = -8576006414339421103L;

    private final Timestamp startTime;
    private final Timestamp endTime;
    private final BeamModeValue beamModeValue;

    public static BeamMode from(@NonNull cern.nxcals.api.custom.domain.BeamMode beamMode) {
        Preconditions.checkArgument(!beamMode.getValidity().isLeftInfinite());
        Timestamp from = getTimestampFromInstant(beamMode.getValidity().getStartTime());
        Instant endTime = beamMode.getValidity().getEndTime();
        Timestamp to = null;
        if(endTime != null) {
            to = getTimestampFromInstant(endTime);
        }
        String beamModeValue = beamMode.getBeamModeValue();
        return new BeamMode(from, to, BeamModeValue.isBeamModeValue(beamModeValue) ? BeamModeValue.valueOf(beamModeValue) : BeamModeValue.UNKNOWN);
    }
}