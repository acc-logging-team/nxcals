package cern.nxcals.api.backport.domain.core.timeseriesdata.spi;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.io.Serializable;

@AllArgsConstructor
@Getter
@ToString
public class DataDistributionBin implements Serializable {
    private static final long serialVersionUID = 7112605466333597792L;

    private final long valuesCount;
    private final double bottomLimit;
    private final double topLimit;
}
