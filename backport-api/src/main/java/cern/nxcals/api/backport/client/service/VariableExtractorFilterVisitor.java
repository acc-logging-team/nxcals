package cern.nxcals.api.backport.client.service;

import cern.nxcals.api.backport.domain.core.metadata.Variable;
import cern.nxcals.api.backport.domain.core.timeseriesdata.filter.value.AbstractValueFilter;
import cern.nxcals.api.backport.domain.core.timeseriesdata.filter.value.CombinedFilter;
import cern.nxcals.api.backport.domain.core.timeseriesdata.filter.value.Filter;
import cern.nxcals.api.backport.domain.core.timeseriesdata.filter.value.FilterVisitor;
import cern.nxcals.api.backport.domain.core.timeseriesdata.filter.value.LogicalFilter;
import cern.nxcals.api.backport.domain.core.timeseriesdata.filter.value.NumericFilter;
import cern.nxcals.api.backport.domain.core.timeseriesdata.filter.value.StringFilter;
import cern.nxcals.api.backport.domain.core.timeseriesdata.filter.value.VectorValueFilter;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import static cern.nxcals.common.SystemFields.NXC_EXTR_VALUE;

/**
 * Allows to traverse the Filter tree to get the scalar and vector filters with its variables.
 * It also constructs the SQL query predicate.
 * It converts the variable names into legal SQL column names.
 */
@Getter
@RequiredArgsConstructor
class VariableExtractorFilterVisitor implements FilterVisitor {

    private final Set<ScalarFilterVariable> scalarFilterVariables = new HashSet<>();
    private final Set<VectorFilterVariable> vectorFilterVariables = new HashSet<>();
    private String predicate = "";
    //must identify each filter separately for vectors...
    @Getter(value = AccessLevel.PRIVATE)
    private AtomicInteger vectorCounter = new AtomicInteger(0);

    private final Variable drivingVariable;

    private String createLegalColumnName(Variable var) {
        return var.getVariableName().replaceAll("[.:\\-/]", "_");
    }

    private String withSuffix(String prefix, String suffix) {
        return prefix + "_" + suffix;
    }

    private void processScalarFilter(AbstractValueFilter<?> visitable) {
        Variable variable = visitable.getVariable();

        if (!variable.equals(drivingVariable)) {

            ScalarFilterVariable scalarFilterVariable = new ScalarFilterVariable(variable,
                    createLegalColumnName(variable));
            scalarFilterVariables.add(scalarFilterVariable);

            predicate += visitable.describe()
                    .replace(variable.getVariableName(), scalarFilterVariable.getLegalColumnName());
        } else {
            //in the case of the filter on the driving variable we just modify the predicate without adding it to the set (we don't need an additional join).
            predicate += visitable.describe()
                    .replace(variable.getVariableName(), NXC_EXTR_VALUE.getValue());
        }
    }

    @Override
    public void visit(NumericFilter visitable) {
        processScalarFilter(visitable);
    }

    @Override
    public void visit(StringFilter visitable) {
        processScalarFilter(visitable);
    }

    @Override
    public void visit(VectorValueFilter<?> visitable) {
        Variable variable = visitable.getVariable();
        VectorFilterVariable vectorFilterVariable = new VectorFilterVariable(visitable,
                withSuffix(createLegalColumnName(variable), Integer.toString(vectorCounter.getAndIncrement())));
        vectorFilterVariables.add(vectorFilterVariable);
        predicate += " " + vectorFilterVariable.getLegalColumnName() + " = true ";
    }

    @Override
    public void visit(LogicalFilter visitable) {
        predicate += " " + visitable.getOperand().getDBValue() + " ";
    }

    @Override
    public void visit(CombinedFilter combinedFilter) {
        predicate += "( ";
        combinedFilter.getF1().accept(this);
        Filter f2 = combinedFilter.getF2();
        if (f2 != null) {
            combinedFilter.getOperand().accept(this);
            f2.accept(this);
        }
        predicate += ") ";
    }

}
