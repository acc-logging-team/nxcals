package cern.nxcals.api.backport.domain.core.timeseriesdata;

import cern.nxcals.api.backport.domain.core.constants.LoggingTimeZone;
import cern.nxcals.api.backport.domain.util.DateUtils;
import cern.nxcals.api.utils.TimeUtils;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import org.apache.commons.lang3.ObjectUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;

@Getter
@Builder
public class VariableStatistics implements Serializable {
    private static final long serialVersionUID = 1874270701960649777L;

    private long valueCount;
    @NonNull
    private Timestamp maxTstamp;
    @NonNull
    private Timestamp minTstamp;
    @NonNull
    private String variableName;
    @NonNull
    private String system;
    @NonNull
    private BigDecimal maxValue;
    @NonNull
    private BigDecimal minValue;
    @NonNull
    private BigDecimal avgValue;
    @NonNull
    private BigDecimal standardDeviationValue;

    /**
     * @param timeZone - timezone used to format the maximum timestamp of this VariableStatistics
     * @return - a String representing the maximum timestamp of this VariableStatistics object, formatted according to
     * the given timezone
     */
    public String getFormattedMaxTstamp(LoggingTimeZone timeZone) {
        return DateUtils.getFormattedStamp(maxTstamp, timeZone);
    }

    /**
     * @param timeZone - timezone used to format the minimum timestamp of this VariableStatistics
     * @return - a String representing the minimum timestamp of this VariableStatistics object, formatted according to
     * the given timezone
     */
    public String getFormattedMinTstamp(LoggingTimeZone timeZone) {
        return DateUtils.getFormattedStamp(minTstamp, timeZone);
    }

    /**
     * @return - the frequency of the data in Hz
     */
    public double getFrequencyHz() {
        long from = TimeUtils.getNanosFromInstant(minTstamp.toInstant());
        long to = TimeUtils.getNanosFromInstant(maxTstamp.toInstant());
        return 10E9 * valueCount / (to - from);
    }

    VariableStatistics mergeWith(VariableStatistics other) {
        return builder()
                .variableName(variableName)
                .system(system)
                .minTstamp(ObjectUtils.min(minTstamp, other.minTstamp))
                .maxTstamp(ObjectUtils.max(maxTstamp, other.maxTstamp))
                .minValue(ObjectUtils.min(minValue, other.minValue))
                .maxValue(ObjectUtils.max(maxValue, other.maxValue))
                .valueCount(valueCount + other.valueCount)
                .avgValue(combineMean(other))
                .standardDeviationValue(combineStandardDeviation(other))
                .build();
    }

    private BigDecimal itemCount() {
        return BigDecimal.valueOf(valueCount);
    }

    private BigDecimal sum() {
        return avgValue.multiply(itemCount());
    }

    private BigDecimal sumOfSquares() {
        return standardDeviationValue.pow(2).multiply(itemCount().subtract(BigDecimal.ONE))
                .add(avgValue.pow(2).multiply(itemCount()));
    }

    private BigDecimal combineMean(VariableStatistics other) {
        BigDecimal tn = itemCount().add(other.itemCount());
        BigDecimal tx = sum().add(other.sum());

        return tx.divide(tn, 10, RoundingMode.HALF_EVEN);
    }

    private BigDecimal combineStandardDeviation(VariableStatistics other) {
        BigDecimal tn = itemCount().add(other.itemCount());
        BigDecimal tx = sum().add(other.sum());
        BigDecimal txx = sumOfSquares().add(other.sumOfSquares());

        return BigDecimal.valueOf(Math.sqrt(txx.subtract(tx.pow(2).divide(tn, 10, RoundingMode.HALF_EVEN))
                .divide(tn.subtract(BigDecimal.ONE), 10, RoundingMode.HALF_EVEN)
                .doubleValue()));
    }
}