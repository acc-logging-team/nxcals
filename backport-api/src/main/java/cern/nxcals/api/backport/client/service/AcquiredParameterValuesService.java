package cern.nxcals.api.backport.client.service;

import cern.japc.core.AcquiredParameterValue;
import cern.nxcals.api.backport.domain.core.metadata.JapcParameterDefinition;
import cern.nxcals.api.backport.domain.core.metadata.VariableSet;

import java.sql.Timestamp;
import java.util.List;

public interface AcquiredParameterValuesService {

    /**
     * Gets a list of all the timestamps of the data of the given JAPC parameter logged within the given time window.
     * 
     * @param param - Acquired parameter object to get data
     * @param startTime - Start of the query time window
     * @param endTime - End of the query time window
     * @return List&lt;Timestamp&gt; - a list of java.sql Timestamp objects representing the times at which the given JAPC
     *         parameter was logged within the given time window.
     */
    List<Timestamp> getDataDistribution(JapcParameterDefinition param, Timestamp startTime, Timestamp endTime);

    /**
     * Gets a list of AcquiredParameterValues for the given JAPC parameter logged within the given time window.
     * 
     * @param param - Acquired parameter object to get data
     * @param startTime - Start of the query time window
     * @param endTime - End of the query time window
     * @return List&lt;AcquiredParameterValue&gt; - list of acquired parameter values
     */
    List<AcquiredParameterValue> getParameterValuesInTimeWindow(
            JapcParameterDefinition param,
            Timestamp startTime, Timestamp endTime);

    /**
     * Gets an AcquiredParameterValue for the given AcquiredParameter having a timestamp corresponding exactly to the
     * given timestamp.
     * 
     * @param param - Acquired parameter object to get data
     * @param stamp - timestamp at which to extract data
     * @return - AcquiredParameterValue
     */
    AcquiredParameterValue getParameterValueAtTimestamp(
            JapcParameterDefinition param, Timestamp stamp);

    /**
     * Gets a list of AcquiredParameterValues for the given JAPC parameter logged within the given time window having
     * timestamps matching those of the given Fundamental Variables.
     * 
     * @param param - Acquired parameter object to get data
     * @param start - Start of the query time window
     * @param end - End of the query time window
     * @param fundamentals - Fundamental variableSet to filter results with.
     * @return List&lt;AcquiredParameterValue&gt; - list of acquired parameter values
     */
    List<AcquiredParameterValue> getParameterValuesInTimeWindowFilteredByFundamentals(JapcParameterDefinition param, Timestamp start, Timestamp end, VariableSet fundamentals);

}