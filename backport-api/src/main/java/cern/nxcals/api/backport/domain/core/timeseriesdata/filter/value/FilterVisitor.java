package cern.nxcals.api.backport.domain.core.timeseriesdata.filter.value;

public interface FilterVisitor {
    void visit(NumericFilter visitable);
    void visit(StringFilter visitable);
    void visit(VectorValueFilter<?> visitable);
    void visit(LogicalFilter visitable);
    void visit(CombinedFilter combinedFilter);
}
