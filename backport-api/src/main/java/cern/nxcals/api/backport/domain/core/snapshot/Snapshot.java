package cern.nxcals.api.backport.domain.core.snapshot;

import cern.nxcals.api.domain.Group;
import cern.nxcals.api.domain.Variable;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Snapshot implements Serializable {
    private static final long serialVersionUID = 5565694814558309395L;
    @EqualsAndHashCode.Include
    private String name;
    private String description;
    private String owner;
    private DateTime createdOn;
    private DateTime expirationDate;
    private boolean visibleToPublic;
    private String timberVersion;
    private long id = -1L;
    private SnapshotProperties properties;

    public static Snapshot from(@NonNull Group group) {
        return from(group, new HashMap<>());
    }

    public static Snapshot from(@NonNull Group group, @NonNull Map<String, Set<Variable>> variables) {
        Snapshot result = new Snapshot(group.getName(), group.getDescription(), group.getOwner(), null, null,
                group.isVisible(), null);
        result.id = group.getId();
        result.properties = SnapshotProperties.from(group, variables);
        return result;
    }

    public Snapshot(String name, String description, String owner, DateTime createdOn, DateTime expiresIn,
            boolean visibleToPublic, String timberVersion) {
        this.name = name;
        this.description = description;
        this.owner = owner;
        this.createdOn = createdOn;
        this.expirationDate = expiresIn;
        this.visibleToPublic = visibleToPublic;
        this.timberVersion = timberVersion;
    }
}
