package cern.nxcals.api.backport.domain.exceptions;

import org.apache.commons.lang3.tuple.Pair;

public final class DataAccessException extends RuntimeException {
    private static final long serialVersionUID = -6167248108370166989L;

    public static final DataAccessException UNREACHABLE_CODE_EXCEPTION = new DataAccessException(
            DataAccessException.UNREACHABLE_CODE, "Stupid coder... Sorry for that, please notify BE/CO/DM");

    /** Indicates a problem with some SQL execution at the datasource */
    public static final Pair<Integer, String> SQL_EXCEPTION = Pair.of(4, "Login Error");

    /** Indicates a problem with some PL/SQL execution at the datasource */
    public static final Pair<Integer, String> PLSQL_EXCEPTION = Pair.of(5, "Problem Connecting to the Database");

    /** Indicates a problem with the metadata */
    public static final Pair<Integer, String> META_EXCEPTION = Pair.of(3, "Metadata Error");

    /** Indicates a problem connecting to the measurement datasource */
    public static final Pair<Integer, String> CONNECTION_EXCEPTION = Pair.of(2, "SQL Error");

    /** Indicates a problem with the supplied login credentials */
    public static final Pair<Integer, String> LOGIN_EXCEPTION = Pair.of(1, "PL/SQL Error");

    /** Indicates a problem with the data */
    public static final Pair<Integer, String> DATA_EXCEPTION = Pair.of(6, "Unsuitable Data or Data for Unregistered VariableImpl");

    /** Indicates an internal system or API error */
    public static final Pair<Integer, String> INTERNAL_ERROR = Pair.of(7, "Internal Error");

    /**Indicates that an attempt has been made to load at least one duplicate time series data record */
    public static final Pair<Integer, String> DUPLICATE_DATA_EXCEPTION = Pair.of(8, "Duplicate Attempt to Store Time Series Data");

    /** Indicates that a naming exception has occured at the level of JNDI */
    public static final Pair<Integer, String> JNDI_EXCEPTION = Pair.of(9, "JNDI error occurred");

    /** Related to java reflexion when querying the user profile */
    public static final Pair<Integer, String> ILLEGAL_ARGUMENT = Pair.of(10, "Illegal argument occurred");
    public static final Pair<Integer, String> ILLEGAL_ACCESS = Pair.of(11, "Illegal access");
    public static final Pair<Integer, String> INVOCATION_TRAGET = Pair.of(12, "Invocation target occurred");

    /** When there is too much data to extract */
    public static final Pair<Integer, String> TOO_MUCH_DATA = Pair.of(13, "Too much data requested");

    /** When there is too much data to extract */
    public static final Pair<Integer, String> UNAUTHORIZED_APPLICATION = Pair.of(14, "Unauthorized application");

    /** When there is something wrong with a derivation algorithm */
    public static final Pair<Integer, String> DERIVATION_ERROR = Pair.of(15, "Derivation Error application");

    /** All servers are down */
    public static final Pair<Integer, String> SERVICE_EXCEPTION = Pair.of(16, "All servers are down");

    /** An exception related to code that should never be reached, usually due to programming bugs */
    public static final Pair<Integer, String> UNREACHABLE_CODE = Pair.of(17, "Exception that should never be thrown");

    private final Pair<Integer, String> errorCode;
    private final StringBuilder message;

    public DataAccessException(Pair<Integer, String> errorCode, String message) {
        this.errorCode = errorCode;
        this.message = new StringBuilder(message);
    }

    public int getErrorCode() {
        return errorCode.getKey();
    }

    @Override
    public String getMessage() {
        return errorCode.getKey() + " - " + errorCode.getValue() + ",\t" + message;
    }

    public void appendMessage(String extraInfo) {
        message.append(extraInfo);
    }

    public static void throwUnreachableCodeException() {
        throw UNREACHABLE_CODE_EXCEPTION;
    }
}