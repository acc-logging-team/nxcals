package cern.nxcals.api.backport.client.service;

import cern.cmw.datax.EntryType;
import cern.cmw.datax.ImmutableEntry;
import cern.nxcals.api.backport.domain.core.ClientSelectionDataCriteria;
import cern.nxcals.api.backport.domain.core.TimeWindow;
import cern.nxcals.api.backport.domain.core.TimeWindowSet;
import cern.nxcals.api.backport.domain.core.constants.BeamModeValue;
import cern.nxcals.api.backport.domain.core.constants.DynamicTimeDuration;
import cern.nxcals.api.backport.domain.core.constants.LoggingTimeInterval;
import cern.nxcals.api.backport.domain.core.constants.ScaleAlgorithm;
import cern.nxcals.api.backport.domain.core.constants.TimescalingProperties;
import cern.nxcals.api.backport.domain.core.constants.VariableDataType;
import cern.nxcals.api.backport.domain.core.filldata.BeamMode;
import cern.nxcals.api.backport.domain.core.filldata.BeamModeSet;
import cern.nxcals.api.backport.domain.core.filldata.LHCFill;
import cern.nxcals.api.backport.domain.core.filldata.LHCFillSet;
import cern.nxcals.api.backport.domain.core.metadata.FundamentalData;
import cern.nxcals.api.backport.domain.core.metadata.Variable;
import cern.nxcals.api.backport.domain.core.metadata.VariableSet;
import cern.nxcals.api.backport.domain.core.snapshot.PriorTime;
import cern.nxcals.api.backport.domain.core.snapshot.Snapshot;
import cern.nxcals.api.backport.domain.core.snapshot.SnapshotProperties;
import cern.nxcals.api.backport.domain.core.timeseriesdata.MultiColumnTimeseriesDataSet;
import cern.nxcals.api.backport.domain.core.timeseriesdata.MultiTimeseriesDataSet;
import cern.nxcals.api.backport.domain.core.timeseriesdata.SparkTimeseriesData;
import cern.nxcals.api.backport.domain.core.timeseriesdata.SparkTimeseriesDataSet;
import cern.nxcals.api.backport.domain.core.timeseriesdata.TimeseriesData;
import cern.nxcals.api.backport.domain.core.timeseriesdata.TimeseriesDataSet;
import cern.nxcals.api.backport.domain.core.timeseriesdata.VariableStatistics;
import cern.nxcals.api.backport.domain.core.timeseriesdata.VariableStatisticsSet;
import cern.nxcals.api.backport.domain.core.timeseriesdata.filter.value.Filter;
import cern.nxcals.api.backport.domain.core.timeseriesdata.filter.value.VectorValueFilter;
import cern.nxcals.api.backport.domain.core.timeseriesdata.spi.DataDistributionBinSet;
import cern.nxcals.api.converters.SparkRowToImmutableDataConverter;
import cern.nxcals.api.custom.domain.CernSystemConstants;
import cern.nxcals.api.custom.domain.GroupType;
import cern.nxcals.api.custom.service.AggregationService;
import cern.nxcals.api.custom.service.aggregation.AggregationFunction;
import cern.nxcals.api.custom.service.aggregation.AggregationFunctions;
import cern.nxcals.api.custom.service.aggregation.DatasetAggregationProperties;
import cern.nxcals.api.custom.service.aggregation.WindowAggregationProperties;
import cern.nxcals.api.domain.Group;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.extraction.metadata.GroupService;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.extraction.metadata.queries.Groups;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.common.spark.UDFFilterOperand;
import cern.nxcals.common.spark.UDFVectorRestriction;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.util.SizeEstimator;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static cern.nxcals.api.backport.domain.core.constants.DynamicTimeDuration.BEAM_MODES;
import static cern.nxcals.api.backport.domain.core.constants.DynamicTimeDuration.DAYS;
import static cern.nxcals.api.backport.domain.core.constants.DynamicTimeDuration.FILLS;
import static cern.nxcals.api.backport.domain.core.constants.DynamicTimeDuration.HOURS;
import static cern.nxcals.api.backport.domain.core.constants.DynamicTimeDuration.MINUTES;
import static cern.nxcals.api.backport.domain.core.constants.DynamicTimeDuration.MONTHS;
import static cern.nxcals.api.backport.domain.core.constants.DynamicTimeDuration.SECONDS;
import static cern.nxcals.api.backport.domain.core.constants.DynamicTimeDuration.WEEKS;
import static cern.nxcals.api.backport.domain.core.constants.DynamicTimeDuration.YEARS;
import static cern.nxcals.api.backport.domain.core.constants.LoggingTimeZone.LOCAL_TIME;
import static cern.nxcals.api.backport.domain.core.constants.ScaleAlgorithm.AVG;
import static cern.nxcals.api.backport.domain.core.constants.ScaleAlgorithm.COUNT;
import static cern.nxcals.api.backport.domain.core.constants.ScaleAlgorithm.MAX;
import static cern.nxcals.api.backport.domain.core.constants.ScaleAlgorithm.MIN;
import static cern.nxcals.api.backport.domain.core.constants.ScaleAlgorithm.SUM;
import static cern.nxcals.api.backport.domain.core.constants.VectorRestriction.ALL_ELEMENTS;
import static cern.nxcals.api.backport.domain.core.constants.VectorRestriction.MATCHED_ELEMENTS;
import static cern.nxcals.api.backport.domain.util.FundamentalUtils.createDatasetFromFundamentals;
import static cern.nxcals.common.SystemFields.NXC_EXTR_TIMESTAMP;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VALUE;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VARIABLE_NAME;
import static cern.nxcals.common.avro.SchemaConstants.ARRAY_ELEMENTS_FIELD_NAME;
import static cern.nxcals.common.spark.MoreFunctions.vectorFilter;
import static cern.nxcals.common.spark.MoreFunctions.vectorFilterMatchedElements;
import static cern.nxcals.common.spark.UDFVectorRestriction.ALL;
import static cern.nxcals.common.spark.UDFVectorRestriction.ANY;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static org.apache.spark.sql.functions.avg;
import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.count;
import static org.apache.spark.sql.functions.max;
import static org.apache.spark.sql.functions.min;
import static org.apache.spark.sql.functions.size;
import static org.apache.spark.sql.functions.stddev;
import static org.apache.spark.sql.functions.sum;
import static org.apache.spark.sql.types.DataTypes.LongType;
import static org.apache.spark.sql.types.DataTypes.createStructField;
import static org.apache.spark.sql.types.DataTypes.createStructType;

@Slf4j
@AllArgsConstructor(access = AccessLevel.PACKAGE)
class TimeseriesDataServiceImpl implements TimeseriesDataService {
    static final String NXCALS_OTHER_TIMESTAMP = "nxcals_other_timestamp";
    private static final String UNSUPPORTED_VARIABLE_DATA_TYPE = "Unsupported variable data type";
    private static final String UNSUPPORTED_DATA_TYPE_FOR_FILTERING = "Unsupported data type for filtering";

    //copied from CALS
    private static final Comparator<LHCFill> COMPARATOR = (o1, o2) ->
            o1.getStartTime().after(o2.getStartTime()) ? 0 : 1;
    //copied from CALS - don't ask why this number...
    private static final int FIRST_LHC_FILL_NUMBER = 887;

    private static final Map<ScaleAlgorithm, AggregationFunction> ALGORITHM_TO_FUNCTION_MAP = constructFunctionMap();
    private static final StructType TIMESTAMP_AS_LONG_SCHEMA = createStructType(new StructField[] {
            createStructField(NXC_EXTR_TIMESTAMP.getValue(), LongType, false)
    });
    @NonNull
    private final SparkSession session;
    @NonNull
    private final Supplier<DataQuery> dataBuilderSupplier;
    @NonNull
    private final GroupService groupService;
    @NonNull
    private final LHCFillDataService fillService;
    @NonNull
    private final MetaDataService metaDataService;
    @NonNull
    private final VariableService variableService;
    @NonNull
    private final AggregationService aggregationService;

    TimeseriesDataServiceImpl(SparkSession spark, GroupService groupService, LHCFillDataService fillService,
            MetaDataService metaDataService, VariableService variableService, AggregationService aggregationService) {
        this(spark, () -> DataQuery.builder(spark), groupService, fillService, metaDataService, variableService,
                aggregationService);
    }

    private static Map<ScaleAlgorithm, AggregationFunction> constructFunctionMap() {
        Map<ScaleAlgorithm, AggregationFunction> result = new EnumMap<>(ScaleAlgorithm.class);
        result.put(ScaleAlgorithm.MIN, AggregationFunctions.MIN);
        result.put(ScaleAlgorithm.MAX, AggregationFunctions.MAX);
        result.put(ScaleAlgorithm.AVG, AggregationFunctions.AVG);

        result.put(ScaleAlgorithm.COUNT, AggregationFunctions.COUNT);
        result.put(ScaleAlgorithm.SUM, AggregationFunctions.SUM);

        result.put(ScaleAlgorithm.REPEAT, AggregationFunctions.REPEAT);
        result.put(ScaleAlgorithm.INTERPOLATE, AggregationFunctions.INTERPOLATE);
        return Collections.unmodifiableMap(result);
    }

    private static UDFFilterOperand getUDFFilterOperand(VectorValueFilter<?> filter) {
        return UDFFilterOperand.getFilterOperandFor(filter.getOperand().getDBValue());
    }

    private static UDFVectorRestriction getUdfVectorRestriction(VectorValueFilter<?> filter) {
        return ALL_ELEMENTS.equals(filter.getRestriction()) ? ALL : ANY;
    }

    private static Row createRowWithTimeInNanos(Timestamp timestamp) {
        return RowFactory
                .create(TimeUtils.getNanosFromInstant(timestamp.toInstant()));
    }

    private static double bytesToMBytes(long bytes, double scalingFactor) {
        return BigDecimal.valueOf(bytes).divide(BigDecimal.valueOf(1024).pow(2))
                .divide(BigDecimal.valueOf(scalingFactor)).doubleValue();
    }


    @SuppressWarnings("java:S1168") //empty collection instead of null but we cannot change old CALS code semantics.
    @Override
    public TimeseriesDataSet getDataForMultipleVariablesAggregatedInFixedIntervals(@NonNull VariableSet variables,
                                                                                   @NonNull Timestamp startTime, @NonNull Timestamp endTime,
                                                                                   @NonNull TimescalingProperties timescaleProperties) {
        if (variables.isEmpty()) {
            return null;
        }
        ScaleAlgorithm algorithm = timescaleProperties.getTimescaleAlgorithm();
        verifyInput(variables, algorithm);
        if (variables.size() == 1) {
            return this.getDataInFixedIntervals(variables.firstEntry().getValue(), startTime, endTime,
                    timescaleProperties);
        }
        if (ScaleAlgorithm.AVG.equals(algorithm)) {
            return getAverageForMultipleVariables(variables, startTime, endTime, timescaleProperties);
        } else {
            List<TimeseriesDataSet> timeseriesDataSets = getDatasetsForEachVariable(variables, startTime, endTime,
                    timescaleProperties);

            List<TimeseriesData> result = aggregateData(algorithm, timeseriesDataSets);

            return SparkTimeseriesDataSet.of(null, result);
        }
    }

    private List<TimeseriesData> aggregateData(ScaleAlgorithm algorithm, List<TimeseriesDataSet> timeseriesDataSets) {
        List<TimeseriesData> result = new ArrayList<>();
        final int datasetSize = timeseriesDataSets.get(0).size();

        try {
            for (int i = 0; i < datasetSize; i++) {
                switch (algorithm) {
                case MIN:
                    evaluateMin(timeseriesDataSets, result, i);
                    break;
                case MAX:
                    evaluateMax(timeseriesDataSets, result, i);
                    break;
                case SUM:
                case COUNT:
                    evaluateCountOrSum(timeseriesDataSets, result, i);
                    break;
                default:
                    throw new IllegalArgumentException("The algorithm " + algorithm
                            + " is not appropriate for method");
                }
            }
        } catch (NoSuchMethodException e) {
            log.error(e.getMessage());
        }
        return result;
    }

    private void evaluateCountOrSum(List<TimeseriesDataSet> timeseriesDataSets, List<TimeseriesData> result, int i)
            throws NoSuchMethodException {
        boolean allNullValues = true;
        double value = 0.0;
        for (TimeseriesDataSet dataSet : timeseriesDataSets) {
            if (!dataSet.getTimeseriesData(i).isNullValue()) {
                value += dataSet.getTimeseriesData(i).getDoubleValue();
                allNullValues = false;
            }
        }
        addValueToDataset(result, value,
                timeseriesDataSets.get(0).getTimeseriesData(i).getStamp(),
                allNullValues);
    }

    private void evaluateMin(List<TimeseriesDataSet> timeseriesDataSets, List<TimeseriesData> result, int i)
            throws NoSuchMethodException {
        boolean allNullValues = true;
        double value = Double.MAX_VALUE;
        for (TimeseriesDataSet dataSet : timeseriesDataSets) {
            if (!dataSet.getTimeseriesData(i).isNullValue()) {
                value = Math.min(value, dataSet.getTimeseriesData(i).getDoubleValue());
                allNullValues = false;
            }
        }
        addValueToDataset(result, value,
                timeseriesDataSets.get(0).getTimeseriesData(i).getStamp(), allNullValues);
    }

    private void evaluateMax(List<TimeseriesDataSet> timeseriesDataSets, List<TimeseriesData> result, int i)
            throws NoSuchMethodException {
        boolean allNullValues = true;
        double value = Double.MIN_VALUE;
        for (TimeseriesDataSet dataSet : timeseriesDataSets) {
            if (!dataSet.getTimeseriesData(i).isNullValue()) {
                value = Math.max(value, dataSet.getTimeseriesData(i).getDoubleValue());
                allNullValues = false;
            }
        }
        addValueToDataset(result, value,
                timeseriesDataSets.get(0).getTimeseriesData(i).getStamp(), allNullValues);
    }

    private TimeseriesDataSet getAverageForMultipleVariables(VariableSet variables, Timestamp startTime,
            Timestamp endTime, TimescalingProperties timescaleProperties) {
        List<TimeseriesDataSet> sumDatasets = new ArrayList<>();
        List<TimeseriesDataSet> countDatasets = new ArrayList<>();

        for (Variable variable : variables) {
            sumDatasets.add(getDataInFixedIntervals(
                    variable,
                    startTime,
                    endTime,
                    TimescalingProperties.getTimeScaleProperties(ScaleAlgorithm.SUM, timescaleProperties.getScaleSize(),
                            timescaleProperties.getTimescaleInterval())));
            countDatasets.add(getDataInFixedIntervals(
                    variable,
                    startTime,
                    endTime,
                    TimescalingProperties
                            .getTimeScaleProperties(ScaleAlgorithm.COUNT, timescaleProperties.getScaleSize(),
                                    timescaleProperties.getTimescaleInterval())));
        }

        List<TimeseriesData> result = aggregateAvgData(sumDatasets, countDatasets);

        return SparkTimeseriesDataSet.of(null, result);
    }

    @SuppressWarnings("squid:S3518")
    private List<TimeseriesData> aggregateAvgData(List<TimeseriesDataSet> sumDatasets,
            List<TimeseriesDataSet> countDatasets) {
        List<TimeseriesData> result = new ArrayList<>();
        final int size = sumDatasets.get(0).size();
        for (int i = 0; i < size; i++) {

            boolean allNullValues = true;
            double valueSum = 0.0;
            double valueCount = 0.0;
            try {
                for (int j = 0; j < sumDatasets.size(); j++) {
                    if (!sumDatasets.get(j).getTimeseriesData(i).isNullValue()) {
                        valueSum += sumDatasets.get(j).getTimeseriesData(i).getDoubleValue();
                        valueCount += countDatasets.get(j).getTimeseriesData(i).getDoubleValue();
                        allNullValues = false;
                    }
                }
                addValueToDataset(result,
                        valueSum / valueCount, sumDatasets.get(0).getTimeseriesData(i).getStamp(), allNullValues);
            } catch (NoSuchMethodException e) {
                log.error(e.getMessage());
            }
        }
        return result;
    }

    private void addValueToDataset(List<TimeseriesData> result, double value, Timestamp timestamp,
            boolean areAllValuesNull) {
        if (areAllValuesNull) {
            value = Double.NaN;
        }
        result.add(SparkTimeseriesData
                .ofDouble(timestamp, value));
    }

    private List<TimeseriesDataSet> getDatasetsForEachVariable(VariableSet variables,
            Timestamp startTime, Timestamp endTime, TimescalingProperties timescaleProperties) {
        List<TimeseriesDataSet> result = new ArrayList<>();

        int size = -1;
        for (Variable v : variables) {
            TimeseriesDataSet data = getDataInFixedIntervals(v, startTime, endTime, timescaleProperties);
            if (size == -1) {
                size = data.size();
            } else {
                if (data.size() != size) {
                    throw new IllegalArgumentException(
                            "It was not possible to aggregate these variables since they do not have the same number of values in the range");
                }
            }
            result.add(data);
        }
        return result;
    }

    private void verifyInput(VariableSet variables, ScaleAlgorithm algorithm) {
        List<ScaleAlgorithm> validAlgorithms = asList(AVG, MIN, MAX, SUM, COUNT);
        if (!validAlgorithms.contains(algorithm)) {
            throw new IllegalArgumentException(
                    "Only SUM, AVG, MIN, MAX, COUNT are the algorithms allowed for aggregating time scaled data");
        }
        variables.forEach(v -> {
            if (v.getVariableDataType() != VariableDataType.NUMERIC) {
                throw new IllegalArgumentException("Only Numeric Data Type is allowed for the variables");
            }
        });
    }

    @SuppressWarnings("java:S1168") //empty collection instead of null but we cannot change old CALS code semantics.
    @Override
    public TimeseriesDataSet getDataAlignedToTimestamps(@NonNull Variable variable, TimeseriesDataSet drivingDataSet) {
        if (CollectionUtils.isEmpty(drivingDataSet)) {
            return null;
        }
        DatasetAggregationProperties properties = DatasetAggregationProperties.builder()
                .drivingDataset(toTimestampsDataset(drivingDataSet)).build();
        Dataset<Row> alignedDataset = aggregationService.getData(findFrom(variable), properties);
        return SparkTimeseriesDataSet.ofAggregated(variable, alignedDataset);
    }

    private Dataset<Row> toTimestampsDataset(TimeseriesDataSet drivingDataSet) {
        List<Row> dsRows = drivingDataSet.stream()
                .map(ds -> RowFactory.create(TimeUtils.getNanosFromInstant(ds.getStamp().toInstant())))
                .collect(Collectors.toList());
        return session.createDataFrame(dsRows, TIMESTAMP_AS_LONG_SCHEMA);
    }

    /**
     * Fundamentals here should be of the form of ACC:LSA_CYCLE:TIMING_USER. They do not exist really in NXCALS but are
     * only transformed into the query for the real fundamentals.
     *
     * @param variable            - the variable for which to get time series data. This can be extracted using the
     *                            MetaDataServiceImpl
     * @param startTime           - Start of the required time window represented by a Java Timestamp object which can be created
     *                            by Timestamp startTime = Timestamp.valueOf("2012-01-01 12:00:00"); Note this should represent the
     *                            local time.
     * @param endTime             - End of the required time window represented by a Java Timestamp object. NOTE the format must be
     *                            the JDBC escape format: yyyy-mm-dd hh:mm:ss
     * @param virtualFundamentals
     * @return
     */
    @Override
    public TimeseriesDataSet getDataInTimeWindowFilteredByFundamentals(@NonNull Variable variable,
            @NonNull Timestamp startTime, @NonNull Timestamp endTime, @NonNull VariableSet virtualFundamentals) {

        Dataset<Row> dsFilteredByFundamentals =
                getDatasetFilteredByFundamentals(variable, startTime, endTime, virtualFundamentals);

        return SparkTimeseriesDataSet.of(variable, dsFilteredByFundamentals);
    }

    @Override
    public TimeseriesDataSet getDataInTimeWindow(@NonNull Variable variable, @NonNull Timestamp startTime,
            @NonNull Timestamp endTime) {
        return SparkTimeseriesDataSet
                .of(variable, variableQuery(variable, startTime.toInstant(), endTime.toInstant()));
    }

    @Override
    public TimeseriesDataSet getDataInTimeWindowAndLastDataPriorToTimeWindowWithinDefaultInterval(Variable variable,
            Timestamp startTime, Timestamp endTime) {

        TimeseriesDataSet data = SparkTimeseriesDataSet
                .of(variable, variableQuery(variable, startTime.toInstant(), endTime.toInstant()));

        TimeseriesData last = getLastDataPriorToTimestampWithinUserIntervalFiltred(variable, startTime,
                LoggingTimeInterval.YEAR,
                null);

        if (last == null) {
            return data;
        } else {
            ArrayList<TimeseriesData> output = Lists.newArrayList(last);
            output.addAll(data);
            return SparkTimeseriesDataSet.ofSorted(variable, output);
        }
    }

    @Override
    public TimeseriesDataSet getDataInTimeWindowOrLastDataPriorToWindowWithinDefaultInterval(Variable variable,
            Timestamp startTime, Timestamp endTime) {
        return getDataInTimeWindowOrLastDataPriorToWindowWithinUserInterval(variable, startTime, endTime,
                LoggingTimeInterval.YEAR);
    }

    @Override
    public TimeseriesDataSet getDataInTimeWindowOrLastDataPriorToWindowWithinUserInterval(Variable variable,
            Timestamp startTime, Timestamp endTime, LoggingTimeInterval interval) {
        return getDataInTimeWindowOrLastDataPriorToWindowWithinUserIntervalFilteredInternal(variable, startTime,
                endTime, interval, null);
    }

    @Override
    public TimeseriesDataSet getDataInTimeWindowOrLastDataPriorToWindowWithinUserIntervalFiltered(Variable variable,
            Timestamp startTime, Timestamp endTime, LoggingTimeInterval interval, Filter filter) {
        return getDataInTimeWindowOrLastDataPriorToWindowWithinUserIntervalFilteredInternal(variable, startTime,
                endTime, interval, filter);
    }

    private <T> TimeseriesDataSet getDataInTimeWindowOrLastDataPriorToWindowWithinUserIntervalFilteredInternal(
            Variable variable,
            Timestamp startTime, Timestamp endTime, LoggingTimeInterval interval, T filter) {

        Dataset<Row> dataset = variableQuery(variable, startTime.toInstant(), endTime.toInstant());

        if (filter != null) {
            dataset = filter(variable, dataset, startTime, endTime, filter);
        }

        if (!dataset.isEmpty()) {
            return SparkTimeseriesDataSet
                    .of(variable, dataset);
        } else {
            TimeseriesData last = getLastDataPriorToTimestampWithinUserIntervalFiltred(variable, startTime, interval,
                    filter);
            return last == null ? null : SparkTimeseriesDataSet.ofSorted(variable, Lists.newArrayList(last));
        }
    }

    //Needed by Snapshots
    private TimeseriesDataSet getDataInTimeWindowOrLastDataPriorToWindowWithinUserIntervalFilteredByFundamentals(
            Variable variable,
            Timestamp startTime, Timestamp endTime, LoggingTimeInterval interval,
            VariableSet virtualFundamentalFilter) {
        return getDataInTimeWindowOrLastDataPriorToWindowWithinUserIntervalFilteredInternal(variable, startTime,
                endTime, interval, virtualFundamentalFilter);
    }

    @Override
    public TimeseriesDataSet getDataFilteredByTimestamps(@NonNull Variable variable, @NonNull List<Timestamp> ts) {
        if (ts.isEmpty() || ts.stream().anyMatch(Objects::isNull)) {
            throw new IllegalArgumentException("Timestamps are not valid");
        }
        Collections.sort(ts);
        Dataset<Row> dataset = variableQuery(variable, ts.get(0).toInstant(), ts.get(ts.size() - 1).toInstant());
        return getTimeseriesDataForTimestamps(variable, dataset, ts);
    }

    private TimeseriesDataSet getTimeseriesDataForTimestamps(Variable variable, Dataset<Row> variableDataset,
            List<Timestamp> ts) {
        return SparkTimeseriesDataSet
                .of(variable, filterDatasetByTimestamps(variableDataset, ts));
    }

    @Override
    public TimeseriesDataSet getDataInTimeWindowFilteredByValues(Variable drivingVariable, Timestamp startTime,
            Timestamp endTime, Filter variableFilters) {
        Dataset<Row> baseDataSet = variableQuery(drivingVariable, startTime.toInstant(), endTime.toInstant());
        return getTimeseriesData(drivingVariable, baseDataSet, startTime, endTime, variableFilters);
    }

    private TimeseriesDataSet getTimeseriesData(Variable drivingVariable, Dataset<Row> variableDataset,
            Timestamp startTime, Timestamp endTime, Filter variableFilters) {
        return SparkTimeseriesDataSet
                .of(drivingVariable, filter(drivingVariable, variableDataset, startTime, endTime, variableFilters));
    }

    private Dataset<Row> filter(Variable drivingVariable, Dataset<Row> variableDataset, Timestamp startTime,
            Timestamp endTime, Filter variableFilters) {
        VariableExtractorFilterVisitor variableVisitor = getVariableVisitor(variableFilters, drivingVariable);

        //handle scalar filter
        Set<ScalarFilterVariable> scalarFilterVariables = variableVisitor.getScalarFilterVariables();
        variableDataset = processScalarFilters(startTime, endTime, variableDataset, scalarFilterVariables);

        //handle vector filters
        Set<VectorFilterVariable> vectorFilterVariables = variableVisitor.getVectorFilterVariables();
        variableDataset = processVectorFilters(startTime, endTime, variableDataset, vectorFilterVariables);

        String whereCondition = variableVisitor.getPredicate();

        variableDataset = variableDataset.where(whereCondition)
                .drop(getColNames(scalarFilterVariables))
                .drop(getColNames(vectorFilterVariables));

        //handle matched elements filter if exists
        variableDataset = processMatchedElementsFilter(variableDataset, vectorFilterVariables);
        return variableDataset;
    }

    private Dataset<Row> processMatchedElementsFilter(Dataset<Row> baseDataSet,
            Set<VectorFilterVariable> vectorFilterVariables) {
        //matched elements filter can be only one and it cannot be composed.
        if (vectorFilterVariables.size() == 1) {
            VectorValueFilter<?> vectorValueFilter = Iterables.getOnlyElement(vectorFilterVariables)
                    .getVectorValueFilter();
            if (vectorValueFilter.getRestriction().equals(MATCHED_ELEMENTS)) {
                DataType dataType = getDataType(baseDataSet);
                baseDataSet = baseDataSet
                        .select(functions.udf(vectorFilterMatchedElements(vectorValueFilter.getValues(),
                                                getUDFFilterOperand(vectorValueFilter)), dataType)
                                        .apply(col(NXC_EXTR_VALUE.getValue())).alias(NXC_EXTR_VALUE.getValue()),
                                col(NXC_EXTR_TIMESTAMP.getValue()));
            }
        }

        return baseDataSet;
    }

    private DataType getDataType(Dataset<Row> baseDataSet) {
        StructType schema = baseDataSet.schema();
        int fieldIndex = schema.fieldIndex(NXC_EXTR_VALUE.getValue());
        return schema.fields()[fieldIndex].dataType();
    }

    private Dataset<Row> processVectorFilters(Timestamp startTime, Timestamp endTime, Dataset<Row> baseDataSet,
            Set<VectorFilterVariable> vectorFilterVariables) {
        for (VectorFilterVariable vectorValueFilter : vectorFilterVariables) {
            VectorValueFilter<?> filter = vectorValueFilter.getVectorValueFilter();

            Dataset<Row> variableDataSet = variableQuery(filter.getVariable(),
                    startTime.toInstant(), endTime.toInstant())
                    .select(functions.udf(vectorFilter(filter.getValues(),
                                            getUDFFilterOperand(filter),
                                            getUdfVectorRestriction(filter)), DataTypes.BooleanType)
                                    .apply(col(NXC_EXTR_VALUE.getValue())).alias(vectorValueFilter.getLegalColumnName()),
                            col(NXC_EXTR_TIMESTAMP.getValue()).alias(NXCALS_OTHER_TIMESTAMP));

            baseDataSet = baseDataSet.join(variableDataSet,
                            baseDataSet.col(NXC_EXTR_TIMESTAMP.getValue()).equalTo(variableDataSet.col(NXCALS_OTHER_TIMESTAMP)),
                            "left_outer")
                    .drop(NXCALS_OTHER_TIMESTAMP);
        }
        return baseDataSet;
    }

    private Dataset<Row> processScalarFilters(Timestamp startTime, Timestamp endTime, Dataset<Row> baseDataSet,
            Set<ScalarFilterVariable> scalarFilterVariables) {
        for (ScalarFilterVariable scalarFilterVariable : scalarFilterVariables) {
            Dataset<Row> variableDataSet = variableQuery(scalarFilterVariable.getVariable(), startTime.toInstant(),
                    endTime.toInstant()).select(
                    col(NXC_EXTR_VALUE.getValue()).alias(scalarFilterVariable.getLegalColumnName()),
                    col(NXC_EXTR_TIMESTAMP.getValue()).alias(NXCALS_OTHER_TIMESTAMP));

            baseDataSet = baseDataSet.join(variableDataSet,
                            baseDataSet.col(NXC_EXTR_TIMESTAMP.getValue()).equalTo(variableDataSet.col(NXCALS_OTHER_TIMESTAMP)),
                            "left_outer")
                    .drop(NXCALS_OTHER_TIMESTAMP);
        }
        return baseDataSet;
    }

    private String[] getColNames(Set<? extends LegalColumnName> scalarFilterVariables) {
        return scalarFilterVariables.stream().map(LegalColumnName::getLegalColumnName)
                .collect(toList()).toArray(new String[] {});
    }

    private VariableExtractorFilterVisitor getVariableVisitor(Filter variableFilters, Variable drivingVariable) {
        VariableExtractorFilterVisitor visitor = new VariableExtractorFilterVisitor(drivingVariable);
        variableFilters.accept(visitor);
        return visitor;
    }

    private Dataset<Row> filterDatasetByTimestamps(Dataset<Row> variableDataset, List<Timestamp> timestamps) {
        String fieldName = "timestamp";
        Dataset<Row> timestampsDataset = createTimestampsDataset(timestamps, fieldName);
        return variableDataset.join(timestampsDataset,
                        variableDataset.col(NXC_EXTR_TIMESTAMP.getValue()).equalTo(timestampsDataset.col(fieldName)))
                .select(col(NXC_EXTR_TIMESTAMP.getValue()), col(NXC_EXTR_VALUE.getValue()));
    }

    private Dataset<Row> createTimestampsDataset(List<Timestamp> timestamps, String fieldName) {
        StructField[] structFields = new StructField[] {
                new StructField(fieldName, DataTypes.LongType, false, Metadata.empty())
        };
        StructType structType = new StructType(structFields);
        List<Row> rows = timestamps.stream().map(TimeseriesDataServiceImpl::createRowWithTimeInNanos)
                .collect(Collectors.toList());
        return session.createDataFrame(rows, structType);
    }

    @Override
    public TimeseriesDataSet getDataInFixedIntervals(@NonNull Variable variable, @NonNull Timestamp startTime,
            @NonNull Timestamp endTime, @NonNull TimescalingProperties timescaleProperties) {
        return SparkTimeseriesDataSet
                .ofAggregated(variable,
                        getDataInFixedIntervalsInternal(variable, startTime, endTime, timescaleProperties));
    }

    private Dataset<Row> getDataInFixedIntervalsInternal(@NonNull Variable variable,
            @NonNull Timestamp startTime, @NonNull Timestamp endTime,
            @NonNull TimescalingProperties timescaleProperties) {
        WindowAggregationProperties aggregationProperties = WindowAggregationProperties.builder()
                .function(toFunction(timescaleProperties.getTimescaleAlgorithm()))
                .timeWindow(startTime.toInstant(), endTime.toInstant())
                .interval(timescaleProperties.getNumberOfSecondsInInterval(), ChronoUnit.SECONDS).build();
        return aggregationService.getData(findFrom(variable), aggregationProperties);
    }

    private AggregationFunction toFunction(@NonNull ScaleAlgorithm scaleAlgorithm) {
        AggregationFunction aggregationFunction = ALGORITHM_TO_FUNCTION_MAP.get(scaleAlgorithm);
        if (aggregationFunction == null) {
            throw new UnsupportedOperationException(
                    String.format("Scale algorithm [%s] has no corresponding aggregation function!",
                            scaleAlgorithm));
        }
        return aggregationFunction;
    }

    private cern.nxcals.api.domain.Variable findFrom(@NonNull Variable variable) {
        return variableService.findById(variable.getVariableInternalID())
                .orElseThrow(() -> new IllegalArgumentException(
                        String.format("Could not find variable [%s] by nxcals id [%s]!",
                                variable.getVariableName(), variable.getVariableInternalID())));
    }

    //Needed by Snapshots
    private TimeseriesDataSet getDataInFixedIntervalsFilteredByFundamentals(@NonNull Variable variable,
            @NonNull Timestamp startTime,
            @NonNull Timestamp endTime, @NonNull TimescalingProperties timescaleProperties,
            VariableSet virtualFundamentals) {

        Dataset<Row> variableDataset = getDataInFixedIntervalsInternal(variable, startTime, endTime,
                timescaleProperties);
        return SparkTimeseriesDataSet
                .of(variable, filter(variable, variableDataset, startTime, endTime, virtualFundamentals));
    }

    private Dataset<Row> filter(@NonNull Dataset<Row> variableDataset,
            @NonNull Timestamp startTime,
            @NonNull Timestamp endTime, @NonNull VariableSet virtualFundamentals) {
        Dataset<Row> filteredData = variableDataset;

        if (!virtualFundamentals.isEmpty()) {
            Dataset<Row> fundamentalsDataset = createDatasetFromFundamentals(startTime, endTime,
                    virtualFundamentals, this.session);
            filteredData = variableDataset.join(fundamentalsDataset,
                    variableDataset.col(NXC_EXTR_TIMESTAMP.getValue())
                            .equalTo(fundamentalsDataset.col(FundamentalData.NXCALS_FUNDAMENTAL_TIMESTAMP)));
        }
        return filteredData;
    }

    @Override
    public VariableStatisticsSet getVariableStatisticsOverMultipleVariablesInTimeWindow(@NonNull VariableSet variables,
            @NonNull Timestamp startTime, @NonNull Timestamp endTime) {

        VariableStatisticsSet result = new VariableStatisticsSet();

        for (Variable v : variables) {
            Dataset<Row> data = variableQuery(v, startTime.toInstant(), endTime.toInstant());
            result.addVariableStatistics(statisticsOf(v, data));
        }

        return result;
    }

    @Override
    public TimeseriesData getDataHavingMaxValueInTimeWindow(Variable variable, Timestamp startTime, Timestamp endTime) {
        Dataset<Row> dataset = variableQuery(variable, startTime.toInstant(), endTime.toInstant());
        if (dataset.isEmpty()) {
            return null;
        }
        return SparkTimeseriesData.of(dataset.select(max(NXC_EXTR_VALUE.getValue()).as(NXC_EXTR_VALUE.getValue()))
                .join(dataset, NXC_EXTR_VALUE.getValue()).head());
    }

    @Override
    public TimeseriesDataSet getVectornumericDataInTimeWindowFilteredByVectorIndices(@NonNull Variable variable,
            @NonNull Timestamp startTime, @NonNull Timestamp endTime, @NonNull int[] indexes) {

        int indexesLength = indexes.length;
        Dataset<Row> dataset = variableQuery(variable, startTime.toInstant(), endTime.toInstant());

        if (dataset.isEmpty() || indexesLength == 0) {
            return SparkTimeseriesDataSet.of(variable, dataset);
        }

        StructType schema = dataset.schema();
        int fieldIndex = schema.fieldIndex(NXC_EXTR_VALUE.getValue());

        if (!schema.fields()[fieldIndex].dataType().simpleString().contains(ARRAY_ELEMENTS_FIELD_NAME)) {
            throw new UnsupportedOperationException(UNSUPPORTED_VARIABLE_DATA_TYPE);
        }

        List<TimeseriesData> filteredList;
        filteredList = dataset.select(col(NXC_EXTR_TIMESTAMP.getValue()), col(NXC_EXTR_VALUE.getValue())
                        .getField(ARRAY_ELEMENTS_FIELD_NAME))
                .collectAsList()
                .stream()
                .map(r -> {
                    Timestamp timestamp = getTimestampFromRow(r);
                    ImmutableEntry elements = getEntryFromRow(r,
                            NXC_EXTR_VALUE.getValue() + "." + ARRAY_ELEMENTS_FIELD_NAME);
                    int elementsLength = elements.getDims()[0];
                    EntryType<?> elementsType = elements.getType();

                    if (elementsType.equals(EntryType.INT32_WRAPPER_ARRAY)) {
                        return filterIntegerElements(indexes, elementsLength, elements, timestamp);
                    }

                    if (elementsType.equals(EntryType.INT64_WRAPPER_ARRAY)) {
                        return filterLongElements(indexes, elementsLength, elements, timestamp);
                    }

                    if (elementsType.equals(EntryType.FLOAT_WRAPPER_ARRAY)) {
                        return filterFloatElements(indexes, elementsLength, elements, timestamp);
                    }

                    if (elementsType.equals(EntryType.DOUBLE_WRAPPER_ARRAY)) {
                        return filterDoubleElements(indexes, elementsLength, elements, timestamp);
                    }

                    if (elementsType.equals(EntryType.STRING_ARRAY)) {
                        return filterStringElements(indexes, elementsLength, elements, timestamp);
                    }

                    throw new UnsupportedOperationException(UNSUPPORTED_DATA_TYPE_FOR_FILTERING);
                }).collect(Collectors.toList());

        return SparkTimeseriesDataSet.of(variable, filteredList);
    }

    private Timestamp getTimestampFromRow(Row r) {
        return TimeUtils.getTimestampFromNanos(r.getAs(NXC_EXTR_TIMESTAMP.getValue()));
    }

    private ImmutableEntry getEntryFromRow(Row r, String entryName) {
        return SparkRowToImmutableDataConverter.convert(r).getEntry(entryName);
    }

    private TimeseriesData filterIntegerElements(int[] indexes, int elementsLength, ImmutableEntry elements,
            Timestamp timestamp) {
        List<Integer> filteredElementslist = new ArrayList<>();

        for (int index : indexes) {
            if (elementsLength >= index) {
                filteredElementslist.add(elements.getAs(EntryType.INT32_WRAPPER_ARRAY)[index - 1]);
            }
        }
        return SparkTimeseriesData.of(timestamp,
                ImmutableEntry.of(NXC_EXTR_VALUE.getValue(), filteredElementslist.toArray(new Integer[0])));
    }

    private TimeseriesData filterLongElements(int[] indexes, int elementsLength, ImmutableEntry elements,
            Timestamp timestamp) {
        List<Long> filteredElementslist = new ArrayList<>();

        for (int index : indexes) {
            if (elementsLength >= index) {
                filteredElementslist.add(elements.getAs(EntryType.INT64_WRAPPER_ARRAY)[index - 1]);
            }
        }
        return SparkTimeseriesData.of(timestamp,
                ImmutableEntry.of(NXC_EXTR_VALUE.getValue(), filteredElementslist.toArray(new Long[0])));
    }

    private TimeseriesData filterFloatElements(int[] indexes, int elementsLength, ImmutableEntry elements,
            Timestamp timestamp) {
        List<Float> filteredElementslist = new ArrayList<>();

        for (int index : indexes) {
            if (elementsLength >= index) {
                filteredElementslist.add(elements.getAs(EntryType.FLOAT_WRAPPER_ARRAY)[index - 1]);
            }
        }
        return SparkTimeseriesData.of(timestamp,
                ImmutableEntry.of(NXC_EXTR_VALUE.getValue(), filteredElementslist.toArray(new Float[0])));
    }

    private TimeseriesData filterDoubleElements(int[] indexes, int elementsLength, ImmutableEntry elements,
            Timestamp timestamp) {
        List<Double> filteredElementslist = new ArrayList<>();

        for (int index : indexes) {
            if (elementsLength >= index) {
                filteredElementslist.add(elements.getAs(EntryType.DOUBLE_WRAPPER_ARRAY)[index - 1]);
            }
        }
        return SparkTimeseriesData.of(timestamp,
                ImmutableEntry.of(NXC_EXTR_VALUE.getValue(), filteredElementslist.toArray(new Double[0])));
    }

    private TimeseriesData filterStringElements(int[] indexes, int elementsLength, ImmutableEntry elements,
            Timestamp timestamp) {
        List<String> filteredElementslist = new ArrayList<>();

        for (int index : indexes) {
            if (elementsLength >= index) {
                filteredElementslist.add(elements.getAs(EntryType.STRING_ARRAY)[index - 1]);
            }
        }
        return SparkTimeseriesData.of(timestamp,
                ImmutableEntry.of(NXC_EXTR_VALUE.getValue(), filteredElementslist.toArray(new String[0])));
    }

    @Override
    public TimeseriesData getLastDataPriorToTimestampWithinDefaultInterval(Variable variable, Timestamp timestamp) {
        return getLastDataPriorToTimestampWithinUserInterval(variable, timestamp, LoggingTimeInterval.YEAR);
    }

    @Override
    public TimeseriesData getLastDataPriorToNowWithinDefaultInterval(Variable variable) {
        return getLastDataPriorToTimestampWithinUserInterval(variable, Timestamp.from(Instant.now()),
                LoggingTimeInterval.YEAR);
    }

    @Override
    public TimeseriesData getLastDataPriorToNowWithinUserInterval(Variable variable,
            LoggingTimeInterval searchInterval) {
        return getLastDataPriorToTimestampWithinUserInterval(variable, Timestamp.from(Instant.now()), searchInterval);
    }

    @Override
    public TimeseriesData getLastDataPriorToTimestampWithinUserInterval(@NonNull Variable variable,
            @NonNull Timestamp timestamp,
            @NonNull LoggingTimeInterval searchInterval) {
        return getLastDataPriorToTimestampWithinUserIntervalFiltred(variable, timestamp, searchInterval, null);
    }

    private <T> TimeseriesData getLastDataPriorToTimestampWithinUserIntervalFiltred(@NonNull Variable variable,
            @NonNull Timestamp timestamp,
            @NonNull LoggingTimeInterval searchInterval, T filter) {

        Optional<Dataset<Row>> optionalDataset = getLastData(variable, timestamp, searchInterval);

        return optionalDataset
                .map(dataset -> {
                    //filter using the timestamp from the data only
                    if (filter != null) {
                        TimeseriesData data = SparkTimeseriesData.of(dataset.first());
                        return filter(variable, dataset, data.getStamp(), data.getStamp(), filter);
                    } else {
                        return dataset;
                    }
                }).map(dataset -> {
                    if (dataset.isEmpty()) {
                        return null;
                    } else {
                        return SparkTimeseriesData.of(dataset.first());
                    }
                }).orElse(null);
    }

    /**
     * This is a bad as it gets but helps not having to duplicate some other methods.
     * CALS filters had no common root class, they can be anything... (jwozniak)
     */
    private <T> Dataset<Row> filter(Variable variable, Dataset<Row> dataset, Timestamp start, Timestamp stop,
            T filter) {
        if (filter instanceof VariableSet) {
            return filter(dataset, start, stop, (VariableSet) filter);
        } else if (filter instanceof Filter) {
            return filter(variable, dataset, start, stop, (Filter) filter);
        } else {
            throw new IllegalArgumentException("Unknown filter type: " + filter.getClass());
        }
    }

    private Optional<Dataset<Row>> getLastData(@NonNull Variable variable,
            @NonNull Timestamp timestamp,
            @NonNull LoggingTimeInterval searchInterval) {
        Instant stamp = getLastTimestampBefore(variable, timestamp.getTime(), searchInterval);
        if (stamp != null) {
            return Optional.of(variableQuery(variable, stamp, stamp));
        }
        return Optional.empty();
    }

    private Instant getLastTimestampBefore(Variable variable, long beforeMillis,
            LoggingTimeInterval maxSearchInterval) {
        Duration delta = Duration.ofDays(1);

        Instant upper = Instant.ofEpochMilli(beforeMillis).minusNanos(1);
        Instant lower = upper.truncatedTo(ChronoUnit.DAYS);

        Row timestampMax;
        do {
            if (isOutsideInterval(maxSearchInterval, beforeMillis, lower)) {
                return null;
            }

            timestampMax = variableQuery(variable, lower, upper)
                    .agg(max(NXC_EXTR_TIMESTAMP.getValue()))
                    .first();

            upper = lower;
            lower = upper.minus(delta);
            delta = delta.plus(delta);
        } while (timestampMax.get(0) == null);

        return TimeUtils.getInstantFromNanos(timestampMax.getLong(0));
    }

    @Override
    public TimeseriesData getNextDataAfterTimestampWithinDefaultInterval(Variable variable, Timestamp timestamp) {
        return getNextDataAfterTimestampWithinUserInterval(variable, timestamp, LoggingTimeInterval.YEAR);
    }

    @Override
    public TimeseriesData getNextDataAfterTimestampWithinUserInterval(@NonNull Variable variable,
            @NonNull Timestamp timestamp,
            @NonNull LoggingTimeInterval searchInterval) {

        Instant stamp = getLastTimestampAfter(variable, timestamp.getTime(), searchInterval);
        return stamp == null ? null : SparkTimeseriesData.of(variableQuery(variable, stamp, stamp).first());
    }

    private Instant getLastTimestampAfter(Variable variableName, long afterMillis,
            LoggingTimeInterval maxSearchInterval) {
        Duration delta = Duration.ofDays(1);

        Instant lower = Instant.ofEpochMilli(afterMillis).plusNanos(1);
        Instant upper = lower.truncatedTo(ChronoUnit.DAYS).plus(delta);

        Row timestampMin;
        do {
            if (isOutsideInterval(maxSearchInterval, afterMillis, upper)) {
                return null;
            }

            timestampMin = variableQuery(variableName, lower, upper)
                    .agg(min(NXC_EXTR_TIMESTAMP.getValue()))
                    .first();

            lower = upper;
            upper = upper.plus(delta);
            delta = delta.plus(delta);
        } while (timestampMin.get(0) == null);

        return TimeUtils.getInstantFromNanos(timestampMin.getLong(0));
    }

    private boolean isOutsideInterval(LoggingTimeInterval interval, long current, Instant to) {
        return Math.abs(to.toEpochMilli() - current) > interval.getTimeMs();
    }

    private Dataset<Row> variableQuery(Variable variable, Instant from, Instant to) {
        return dataBuilderSupplier.get().byVariables()
                .system(variable.getSystem())
                .startTime(from)
                .endTime(to)
                .variable(variable.getVariableName())
                .build();
    }

    private Dataset<Row> multipleVariableQuery(List<Variable> variables, String system, Instant from, Instant to) {
        List<String> variableNames = variables.stream().map(Variable::getVariableName).collect(toList());
        return dataBuilderSupplier.get().byVariables()
                .system(system)
                .startTime(from)
                .endTime(to)
                .variables(variableNames)
                .build();
    }

    @Override
    public TimeseriesData getDataForVariableAggregated(Variable variable, Timestamp startTime, Timestamp endTime,
            ScaleAlgorithm algorithm) {
        Dataset<Row> dataset = variableQuery(variable, startTime.toInstant(), endTime.toInstant());
        if (dataset.isEmpty()) {
            return null;
        }
        Row result = getRowForAlgorithm(algorithm, dataset);
        return SparkTimeseriesData.of(result);
    }

    private Row getRowForAlgorithm(ScaleAlgorithm scaleAlgorithm, Dataset<Row> dataset) {
        String timestampField = NXC_EXTR_TIMESTAMP.getValue();
        String valueField = NXC_EXTR_VALUE.getValue();
        switch (scaleAlgorithm) {
        case AVG:
        case SUM:
        case COUNT:
            return dataset.agg(getColumnForAlgorithm(scaleAlgorithm).name(valueField),
                    min(timestampField).name(timestampField)).head();
        case MIN:
        case MAX:
            return dataset.agg(getColumnForAlgorithm(scaleAlgorithm).name(valueField))
                    .join(dataset, valueField).head();
        default:
            throw new IllegalArgumentException(
                    "The scale algorithm " + scaleAlgorithm.name()
                            + " isn't supported for individual timeseriesData points");
        }
    }

    private Column getColumnForAlgorithm(ScaleAlgorithm algorithm) {
        String valueField = NXC_EXTR_VALUE.getValue();
        switch (algorithm) {
        case MAX:
            return max(valueField);
        case SUM:
            return sum(valueField);
        case MIN:
            return min(valueField);
        case COUNT:
            return count(valueField);
        case AVG:
            return avg(valueField);
        default:
            return col(valueField);
        }
    }

    @Override
    public MultiColumnTimeseriesDataSet getMultiColumnDataAlignedToTimestamps(VariableSet variables,
            TimeseriesDataSet drivingDataSet) {
        MultiTimeseriesDataSet.Builder builder = MultiTimeseriesDataSet.builder();
        for (Variable variable : variables) {
            builder = builder.series(getDataAlignedToTimestamps(variable, drivingDataSet));
        }
        return builder.build();
    }

    @Override
    public MultiColumnTimeseriesDataSet getMultiColumnDataInTimeWindow(VariableSet variables, Timestamp startTime,
            Timestamp endTime) {
        MultiTimeseriesDataSet.Builder builder = MultiTimeseriesDataSet.builder();
        for (Variable variable : variables) {
            builder = builder.series(getDataInTimeWindow(variable, startTime, endTime));
        }
        return builder.build();
    }

    @Override
    public MultiColumnTimeseriesDataSet getMultiColumnDataInFixedIntervals(VariableSet variables, Timestamp startTime,
            Timestamp endTime, TimescalingProperties timescaleProperties) {
        MultiTimeseriesDataSet.Builder builder = MultiTimeseriesDataSet.builder();
        for (Variable variable : variables) {
            builder = builder.series(getDataInFixedIntervals(variable, startTime, endTime, timescaleProperties));
        }
        return builder.build();
    }

    @Override
    public double getJVMHeapSizeEstimationForDataInTimeWindow(@NonNull Variable variable, @NonNull Timestamp startTime,
            @NonNull Timestamp endTime,
            VariableSet fundamentals, TimescalingProperties timescaleProperties) {
        if (fundamentals != null && timescaleProperties != null) {
            throw new IllegalArgumentException("Either timescaleProperites or fundamentals should be null");
        }
        Dataset<Row> data;
        double scalingFactor = 0.01;
        if (fundamentals != null) {
            data = getDatasetFilteredByFundamentals(variable, startTime, endTime, fundamentals);
        } else if (timescaleProperties != null) {
            data = getDataInFixedIntervalsInternal(variable, startTime, endTime, timescaleProperties);
        } else {
            data = variableQuery(variable, startTime.toInstant(), endTime.toInstant());
        }
        Object[] sampledResultSet = data.sample(scalingFactor).collectAsList().toArray();
        long sizeEstimation = SizeEstimator.estimate(sampledResultSet);
        return bytesToMBytes(sizeEstimation, scalingFactor);
    }

    @Override
    public Dataset<Row> getDatasetFilteredByFundamentals(Variable variable, Timestamp startTime, Timestamp endTime,
            VariableSet fundamentals) {
        Dataset<Row> baseDataset = variableQuery(variable, startTime.toInstant(), endTime.toInstant());
        return filter(baseDataset, startTime, endTime, fundamentals);
    }

    @Override
    public int getMaxVectornumericElementCountInTimeWindow(@NonNull Variable variable, @NonNull Timestamp startTime,
            @NonNull Timestamp endTime) {
        String nrElements = "element_count";
        String maxNrElements = "max_element_count";

        Dataset<Row> dataset = variableQuery(variable, startTime.toInstant(), endTime.toInstant());
        StructType schema = dataset.schema();
        int fieldIndex = schema.fieldIndex(NXC_EXTR_VALUE.getValue());

        if (!schema.fields()[fieldIndex].dataType().simpleString().contains(ARRAY_ELEMENTS_FIELD_NAME)) {
            throw new UnsupportedOperationException(UNSUPPORTED_VARIABLE_DATA_TYPE);
        }

        return Optional.ofNullable(SparkRowToImmutableDataConverter
                .convert(dataset
                        .select(size(col(NXC_EXTR_VALUE.getValue()).getField(ARRAY_ELEMENTS_FIELD_NAME)).as(nrElements))
                        .agg(max(nrElements).as(maxNrElements)).first())
                .getEntry(maxNrElements).getAs(EntryType.INT32)).orElse(0);
    }

    //copied from CALS
    private DateTime getStartTimeFrom(DynamicTimeDuration time, int duration, DateTime endTime) {
        if (SECONDS == time) {
            return endTime.minusSeconds(duration);
        } else if (MINUTES == time) {
            return endTime.minusMinutes(duration);
        } else if (HOURS == time) {
            return endTime.minusHours(duration);
        } else if (DAYS == time) {
            return endTime.minusDays(duration);
        } else if (WEEKS == time) {
            return endTime.minusWeeks(duration);
        } else if (MONTHS == time) {
            return endTime.minusMonths(duration);
        } else if (YEARS == time) {
            return endTime.minusYears(duration);
        }
        return endTime;
    }

    //copied from CALS
    private DateTime getEndTime(PriorTime prior, ClientSelectionDataCriteria criteria) {
        return prior.time()
                .withZone(criteria.getTimeZone() == LOCAL_TIME ? DateTimeZone.forID("CET") : DateTimeZone.UTC);
    }

    //copied from CALS
    private TimeWindowSet getTimeWindowSetFor(ClientSelectionDataCriteria criteria) {
        DynamicTimeDuration time = criteria.getTime();
        PriorTime prior = criteria.getPriorTime();
        DateTime endTime = getEndTime(prior, criteria);

        if (BEAM_MODES == time || FILLS == time) {
            BeamModeValue[] fillFilterBeamModes = criteria.getFillsFiltersBeamModes();
            return this.getTimeWindowSetFor(new Timestamp(endTime.getMillis()), fillFilterBeamModes, criteria);
        }

        DateTime startTime = getStartTimeFrom(time, criteria.getDynamicDuration(), endTime);
        TimeWindowSet windowSet = new TimeWindowSet(criteria.getTimeZone());
        windowSet.addTimeWindow(
                TimeWindow.newTimeWindow(new Timestamp(startTime.getMillis()), new Timestamp(endTime.getMillis())));
        return windowSet;
    }

    //copied from CALS
    private TimeWindowSet getTimeWindowSetFor(Timestamp endTime, BeamModeValue[] beamModes,
            ClientSelectionDataCriteria criteria) {
        LHCFillSet fillSet = null;
        int duration = criteria.getDynamicDuration();
        if (!ArrayUtils.isEmpty(beamModes)) {
            fillSet = this.getLastLHCFillsAndBeamModesFilteredByBeamModesUpTo(duration, beamModes, endTime);
        } else {
            fillSet = this.getLastLHCFillsAndBeamModesUpTo(duration, endTime);
        }

        return this.getTimeWindowsBasedOnFillOrBeamModesSelection(fillSet, criteria);
    }

    //copied from CALS
    private LHCFillSet getLastLHCFillsAndBeamModesFilteredByBeamModesUpTo(int duration, BeamModeValue[] beamModes,
            Timestamp endTime) {
        final LHCFill lastFill = this.fillService.getLastCompletedLHCFillAndBeamModes();
        final LHCFill fill = this.fillService.getLHCFillAndBeamModesByFillNumber(lastFill.getFillNumber() - duration);
        final Timestamp baseStartTimestamp = fill.getStartTime();
        LHCFillSet fillSet = this.fillService.getLHCFillsAndBeamModesInTimeWindowContainingBeamModes(baseStartTimestamp,
                endTime, beamModes);
        if (fillSet.size() >= duration)
            return getLastFills(duration, fillSet);

        final long interval = endTime.getTime() - baseStartTimestamp.getTime();
        int k = 2;
        Timestamp shiftedBaseTimestamp;
        Timestamp firstFillStart = this.fillService.getLHCFillAndBeamModesByFillNumber(FIRST_LHC_FILL_NUMBER)
                .getStartTime();
        do {
            shiftedBaseTimestamp = new Timestamp(baseStartTimestamp.getTime() - interval * k);
            fillSet = this.fillService.getLHCFillsAndBeamModesInTimeWindowContainingBeamModes(shiftedBaseTimestamp,
                    endTime, beamModes);
            k *= 2;
        } while (fillSet.size() < duration && shiftedBaseTimestamp.after(firstFillStart));

        return getLastFills(duration, fillSet);
    }

    //copied from CALS
    private LHCFillSet getLastFills(int duration, LHCFillSet fillSet) {
        if (fillSet.size() == duration) {
            return fillSet;
        }
        final List<LHCFill> fillList = new ArrayList<>();
        for (LHCFill fill : fillSet) {
            fillList.add(fill);
        }

        fillList.sort(COMPARATOR);

        LHCFillSet filteredFillSet = new LHCFillSet();
        for (LHCFill fill : fillList.subList(0, duration)) {
            filteredFillSet.addLHCFill(fill);
        }
        return filteredFillSet;
    }

    //copied from CALS
    private LHCFillSet getLastLHCFillsAndBeamModesUpTo(long duration, Timestamp endTime) {
        final LHCFill lastFill = this.fillService.getLastCompletedLHCFillAndBeamModes();
        final int firstFillNumber = lastFill.getFillNumber() - (int) (duration - 1);
        final LHCFill firstLhcFill = this.fillService.getLHCFillAndBeamModesByFillNumber(firstFillNumber);
        if (firstLhcFill == null) {
            return new LHCFillSet();
        }
        return this.fillService.getLHCFillsAndBeamModesInTimeWindow(firstLhcFill.getStartTime(), endTime);
    }

    //copied from CALS
    private TimeWindowSet getTimeWindowsBasedOnFillOrBeamModesSelection(LHCFillSet fillSet,
            ClientSelectionDataCriteria criteria) {
        TimeWindowSet tws = new TimeWindowSet(criteria.getTimeZone());
        if (DynamicTimeDuration.FILLS.equals(criteria.getTime())) {
            for (LHCFill fill : fillSet) {
                tws.addTimeWindow(TimeWindow.newTimeWindow(fill.getStartTime(), fill.getEndTime(),
                        "Fill no: " + fill.getFillNumber()));
            }
            return tws;
        }

        if (DynamicTimeDuration.BEAM_MODES.equals(criteria.getTime())) {
            BeamModeValue[] selectedBeamModes = criteria.getSelectedBeamModes();
            if (ArrayUtils.isEmpty(selectedBeamModes)) {
                selectedBeamModes = BeamModeValue.values();
            }
            for (LHCFill fill : fillSet) {
                for (BeamModeValue bmv : selectedBeamModes) {
                    BeamModeSet beamModesInFill = fill.getBeamModes(bmv);
                    for (BeamMode bm : beamModesInFill) {
                        tws.addTimeWindow(TimeWindow.newTimeWindow(bm.getStartTime(), bm.getEndTime(),
                                "Beam mode: " + bm.getBeamModeValue() + " in fill no: " + fill.getFillNumber()));
                    }
                }
            }
        }

        return tws;
    }

    /**
     * Currently only PriorTime is supported, copied as is from CALS.
     */
    private ClientSelectionDataCriteria combineCriteriaFrom(SnapshotProperties properties,
            ClientSelectionDataCriteria criteria) {
        if (criteria == null) {
            return properties;
        }
        PriorTime priorTime = criteria.getPriorTime();
        if (priorTime == null) {
            return properties;
        }
        properties.setPriorTime(priorTime);
        return properties;
    }

    /**
     * Copied from CALS Internal API
     * If alignStartTime is set and dataSet retrieved doesn't have any datapoints before this time then the value of the
     * last data point is used to create a data point with the same timestamp as the alignStartTime. If alignEndTime is
     * set and the dataSet retireved doesn't have any datapoints after this time then the value of the last data point
     * of the dataSet is used to create a new datapoint with the same timestamp as the alignEndTime.
     *
     * @param variable       - the variable for which to get time series data. This can be extracted using the
     *                       MetaDataServiceImpl
     * @param startTime      - Start of the required time window represented by a Java Timestamp object which can be created
     *                       by Timestamp startTime = Timestamp.valueOf("2012-01-01 12:00:00"); Note this should represent the
     *                       local time.
     * @param endTime        - End of the required time window represented by a Java Timestamp object. NOTE the format must be
     *                       the JDBC escape format: yyyy-mm-dd hh:mm:ss
     * @param alignStartTime - timestamp to align start of data to, set to null if data isn't to be aligned to start.
     * @param alignEndTime   - timestamp to align end of data to set to, set to null if data isn't to be aligned to end.
     * @return the TimeseriesDataSet - REMEMBER in the case of Measurements and Logging - all data is time stamped
     * according to UTC!
     */
    private TimeseriesDataSet getDataAlignedToTimeWindow(Variable variable, Timestamp startTime, Timestamp endTime,
            Timestamp alignStartTime, Timestamp alignEndTime) {
        TimeseriesDataSet data = getDataInTimeWindow(variable, startTime, endTime);

        List<TimeseriesData> output = new ArrayList<>();

        if (alignStartTime != null && !data.isEmpty() && data.getMinStamp().after(alignStartTime)) {
            //add a point to the beginning
            output.add(((SparkTimeseriesData) data.getTimeseriesData(0)).toBuilder().stamp(alignStartTime).build());
        }

        //middle
        output.addAll(data);

        if (alignEndTime != null && !data.isEmpty() && data.getMaxStamp().before(alignEndTime)) {
            //add a point to the end
            output.add(((SparkTimeseriesData) data.getTimeseriesData(data.size() - 1)).toBuilder().stamp(alignEndTime)
                    .build());
        }

        return SparkTimeseriesDataSet.ofSorted(data.getVariable(), output);
    }

    @Override
    public List<TimeseriesDataSet> getDataForSnapshot(String snapshotName, String owner,
            ClientSelectionDataCriteria clientCriteria) {
        Condition<Groups> queryCondition = Groups.suchThat().name().eq(snapshotName).and().systemName()
                .eq(CernSystemConstants.CERN_SYSTEM).and().label().eq(GroupType.SNAPSHOT.toString());

        if (owner != null) {
            queryCondition = queryCondition.and().ownerName().eq(owner);
        }

        Group group = groupService.findOne(queryCondition)
                .orElseThrow(
                        () -> new IllegalArgumentException(
                                "No such snapshot " + snapshotName + " for owner " + owner + " found."));

        Snapshot snapshot = Snapshot.from(group, groupService.getVariables(group.getId()));
        ClientSelectionDataCriteria combinedProperties = combineCriteriaFrom(snapshot.getProperties(), clientCriteria);

        TimeWindowSet timeWindows = combinedProperties.isEndTimeDynamic() ? getTimeWindowSetFor(combinedProperties)
                : combinedProperties.getTimeWindowSet();

        List<TimeseriesDataSet> output = new ArrayList<>();
        for (Variable variable : snapshot.getProperties().getSelectedVariables()) {
            List<TimeseriesData> outputForVariable = new ArrayList<>();
            for (TimeWindow timeWindow : timeWindows) {
                outputForVariable.addAll(getTimeseriesDataFor(variable, timeWindow, combinedProperties));
            }
            TimeseriesDataSet dataSet = SparkTimeseriesDataSet.of(variable, outputForVariable);
            output.add(dataSet);
        }

        return output;
    }

    /**
     * This method is used to extract data using the criteria from Snapshot for a given Variable and a given time window.
     * It is a sensible compilation of what was done in CALS in multiple classes.
     * Please be aware that some of the CALS ifs were in fact dead-code that could not be reached.
     * Some of others could never worked as they must have thrown a NPE.
     * Also the fundamental filter was only applied in CALS to certain combinations, not to all.
     * The VariableFilter was always empty, thus excluded here.
     * I hope this is the best possible approximation (jwozniak)
     *
     * @param variable
     * @param timeWindow
     * @param criteria
     * @return
     */
    private TimeseriesDataSet getTimeseriesDataFor(Variable variable,
            TimeWindow timeWindow, ClientSelectionDataCriteria criteria) {

        VariableSet fundamentalFilters = criteria.getFundamentalFilters();

        if (criteria.isAlignToEnd()) {
            if (criteria.isAlignToStart()) {
                return getDataAlignedToTimeWindow(variable, timeWindow.getStartTime(),
                        timeWindow.getEndTime(), timeWindow.getStartTime(), timeWindow.getEndTime());
            } else {
                return getDataAlignedToTimeWindow(variable, timeWindow.getStartTime(),
                        timeWindow.getEndTime(), null, timeWindow.getEndTime());
            }
        }

        if (criteria.isAlignToStart()) {
            return getDataAlignedToTimeWindow(variable, timeWindow.getStartTime(),
                    timeWindow.getEndTime(), timeWindow.getStartTime(), null);
        }

        if (criteria.isTimeScalingApplied()) {
            return getDataInFixedIntervalsFilteredByFundamentals(variable, timeWindow.getStartTime(),
                    timeWindow.getEndTime(),
                    criteria.getTimeScalingProperties(), fundamentalFilters);
        }

        if (criteria.isAlignedWithVariable()) {
            TimeseriesDataSet dataSet = getDataInTimeWindowFilteredByFundamentals(
                    criteria.getDrivingVariable(), timeWindow.getStartTime(), timeWindow.getEndTime(),
                    fundamentalFilters);

            if (criteria.getDrivingVariable().getVariableName().equals(variable.getVariableName())) {
                return dataSet;
            } else {
                return getDataAlignedToTimestamps(variable, dataSet);
            }
        }
        //This if is a dead code, we don't suppport this property in Criteria. Leaving just in case...
        if (criteria.isGetLastDataWhenNone()) {
            TimeseriesDataSet data = getDataInTimeWindowOrLastDataPriorToWindowWithinUserIntervalFilteredByFundamentals(
                    variable,
                    timeWindow.getStartTime(),
                    timeWindow.getEndTime(),
                    null,
                    fundamentalFilters);

            alignTo(timeWindow, criteria, data);

            return data;
        }

        return this.getDataInTimeWindowFilteredByFundamentals(variable, timeWindow.getStartTime(),
                timeWindow.getEndTime(),
                fundamentalFilters);
    }

    // If the option to align the data is set (wtf?)
    private void alignTo(TimeWindow timeWindow, ClientSelectionDataCriteria criteria, TimeseriesDataSet data) {

        if (data.size() == 1 && criteria.isLastValueAligned()) {
            // The timestamp of the first value
            Timestamp stamp = data.getTimeseriesData(0).getStamp();
            // And the stamp is before the start time
            if (stamp.before(timeWindow.getStartTime())) {
                // Set the time to be the start time
                stamp.setTime(timeWindow.getStartTime().getTime());
            }
        }
    }

    @Override
    public TimeseriesDataSet getFundamentalDataInTimeWindowWithFundamentalNamesLikePattern(String filterFundamentalName,
            Timestamp startTime, Timestamp endTime) {
        VariableSet fundamentals = metaDataService.getFundamentalsInTimeWindowWithNameLikePattern(startTime, endTime,
                filterFundamentalName);
        Dataset<Row> fundamentalsDataset = createDatasetFromFundamentals(startTime, endTime, fundamentals,
                this.session);
        return SparkTimeseriesDataSet.ofFundamentals(fundamentalsDataset);
    }

    @Override
    public TimeseriesDataSet getDataDistribution(@NonNull Variable variable, @NonNull Timestamp startTime,
            @NonNull Timestamp endTime) {
        return SparkTimeseriesDataSet
                .ofDataDistribution(variable, variableQuery(variable, startTime.toInstant(), endTime.toInstant()));
    }

    @Override
    public DataDistributionBinSet getDataDistributionInTimewindowForBinsNumber(Variable variable, Timestamp startTime,
            Timestamp endTime, int binsNumber) {
        Dataset<Row> dataset = variableQuery(variable, startTime.toInstant(), endTime.toInstant());
        return new DataDistributionBinSet(variable, dataset, binsNumber);
    }

    @Override
    public VariableStatisticsSet getVariableStatisticsOverTimeWindowFilteredByFundamentals(
            @NonNull VariableSet variables, @NonNull Timestamp startTime, @NonNull Timestamp endTime,
            @NonNull VariableSet fundamentals) {

        VariableStatisticsSet result = new VariableStatisticsSet();
        if (variables.getVariables().isEmpty()) {
            return result;
        }

        Map<String, List<Variable>> variablesGroupedBySystem = variables.getVariables().stream()
                .collect(Collectors.groupingBy(Variable::getSystem));

        variablesGroupedBySystem.forEach((system, variableList) -> {
            Dataset<Row> data = multipleVariableQuery(variableList, system, startTime.toInstant(),
                    endTime.toInstant());
            Dataset<Row> filteredData = filter(data, startTime, endTime, fundamentals);
            for (Variable v : variableList) {
                if (!filteredData.isEmpty()) {
                    result.addVariableStatistics(statisticsOf(v, filteredData));
                }
            }
        });

        return result;
    }

    private VariableStatistics statisticsOf(Variable variable, @NonNull Dataset<Row> dataset) {

        final String count_val = "count_val";
        final String max_val = "max_val";
        final String min_val = "min_val";
        final String avg_val = "avg_val";
        final String stddev_val = "stddev_val";
        final String max_stamp = "max_stamp";
        final String min_stamp = "min_stamp";

        Row agg = dataset.filter(col(NXC_EXTR_VARIABLE_NAME.getValue()).equalTo(variable.getVariableName()))
                .agg(count(NXC_EXTR_VALUE.getValue()).alias(count_val),
                        max(NXC_EXTR_VALUE.getValue()).alias(max_val),
                        min(NXC_EXTR_VALUE.getValue()).alias(min_val),
                        avg(NXC_EXTR_VALUE.getValue()).alias(avg_val),
                        stddev(NXC_EXTR_VALUE.getValue()).alias(stddev_val),
                        max(NXC_EXTR_TIMESTAMP.getValue()).alias(max_stamp),
                        min(NXC_EXTR_TIMESTAMP.getValue()).alias(min_stamp)
                ).first();

        return VariableStatistics.builder()
                .variableName(variable.getVariableName())
                .system(variable.getSystem())
                .valueCount(agg.getAs(count_val))
                .maxValue(safeConvert(agg.getAs(max_val)))
                .minValue(safeConvert(agg.getAs(min_val)))
                .avgValue(safeConvert(agg.getAs(avg_val)))
                .standardDeviationValue(safeConvert(agg.getAs(stddev_val)))
                .maxTstamp(Timestamp.from(TimeUtils.getInstantFromNanos(agg.<Long>getAs(max_stamp))))
                .minTstamp(Timestamp.from(TimeUtils.getInstantFromNanos(agg.<Long>getAs(min_stamp))))
                .build();
    }

    private BigDecimal safeConvert(Object value) {
        if (value instanceof Double && !((Double) value).isNaN() && !((Double) value).isInfinite()) {
            return BigDecimal.valueOf((Double) value);
        } else if (value instanceof Long) {
            return BigDecimal.valueOf((Long) value);
        } else if (value instanceof Integer)
            return BigDecimal.valueOf((Integer) value);
        return BigDecimal.ZERO;
    }
}
