package cern.nxcals.api.backport.domain.core.filldata;

import cern.nxcals.api.custom.domain.Fill;
import lombok.NonNull;

import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

public class LHCFillSet extends TreeMap<Integer, LHCFill> implements Iterable<LHCFill> {
    private static final long serialVersionUID = -1949126889036948444L;

    public static LHCFillSet from(@NonNull List<Fill> fills) {
        LHCFillSet result = new LHCFillSet();
        fills.stream().map(LHCFill::from).forEach(result::addLHCFill);
        return result;
    }

    public void addLHCFill(LHCFill lhcFill) {
        put(lhcFill.getFillNumber(), lhcFill);
    }

    public boolean containsLHCFill(int fillNumber) {
        return containsKey(fillNumber);
    }

    public LHCFill getLHCFill(int fillNumber) {
        return get(fillNumber);
    }

    @Override
    public Iterator<LHCFill> iterator() {
        return values().iterator();
    }

    public int[] getFillNumbers() {
        return values().stream().mapToInt(LHCFill::getFillNumber).toArray();
    }
}