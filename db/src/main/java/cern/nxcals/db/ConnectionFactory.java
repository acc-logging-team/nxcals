package cern.nxcals.db;

import java.sql.Connection;

/**
 * Created by msobiesz on 04/08/17.
 */
public interface ConnectionFactory {

    Connection getConnectionFor(String url, String user, String pass);

    static ConnectionFactory forDriver(String name) {
        return new DriverBasedConnectionFactory(name);
    }
}
