package cern.nxcals.db;

import java.sql.Connection;
import java.sql.DriverManager;

import static java.util.Objects.requireNonNull;

/**
 * Created by msobiesz on 04/08/17.
 */
public class DriverBasedConnectionFactory implements ConnectionFactory {
    private final String driverName;

    public DriverBasedConnectionFactory(String name) {
        this.driverName = requireNonNull(name);
    }

    @Override
    @SuppressWarnings("squid:S00112")
    public Connection getConnectionFor(String url, String user, String pass) {
        try {
            Class.forName(this.driverName);
            return DriverManager.getConnection(url, user, pass);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}
