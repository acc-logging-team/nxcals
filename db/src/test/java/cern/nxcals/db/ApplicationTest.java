package cern.nxcals.db;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Created by msobiesz on 08/08/17.
 */
public class ApplicationTest {
    private Application instance;

    @BeforeEach
    public void setUp() throws Exception {
        this.instance = new Application();
    }

    @Test
    public void shouldPerformUpdate() {
        Config config = ConfigFactory.load("application.conf");
        this.instance.run(config);
    }

    @Test
    public void shouldPerformUpdateWithTestContext() {
        Config config = ConfigFactory.load("applicationTestContext.conf");
        this.instance.run(config);
    }

    @Test
    public void shouldNotPerformUpdateDueToNonExistingChangeLogPath() {
        Config config = ConfigFactory.parseString("db.install.changelog.path=ala_ma_kota").
                withFallback(ConfigFactory.load());
        assertThrows(RuntimeException.class, () -> this.instance.run(config));
    }
}