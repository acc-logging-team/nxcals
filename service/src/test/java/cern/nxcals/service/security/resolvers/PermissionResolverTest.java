package cern.nxcals.service.security.resolvers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PermissionResolverTest {
    private static final String PREFIX = "NXCALS-";
    private static String PERMISSION_AREA = "AREA";
    private static String PERMISSION_TYPE = "TYPE";
    private static String ALL_PERMISSION_STRING = "ALL";

    private ConcatenatingPermissionResolver concatenatingPermissionResolver;

    private DevPermissionResolver devPermissionResolver;

    @BeforeEach
    public void setUp() {
        concatenatingPermissionResolver = new ConcatenatingPermissionResolver(PREFIX);
        devPermissionResolver = new DevPermissionResolver();
    }

    @Test
    void shouldConcatenateStringWithColon() {
        assertEquals(PREFIX + PERMISSION_AREA + "-" + PERMISSION_TYPE,
                concatenatingPermissionResolver.getPermissionString(PERMISSION_AREA, PERMISSION_TYPE));
    }

    @Test
    void shouldReturnAllPermissionString() {
        assertEquals(ALL_PERMISSION_STRING,
                devPermissionResolver.getPermissionString(PERMISSION_AREA, PERMISSION_TYPE));
    }
}
