package cern.nxcals.service.security.rbac;

import cern.rbac.common.RbaToken;
import cern.rbac.common.test.TestTokenBuilder;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;
import java.util.Base64;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DefaultRbacAuthenticationServiceTest {
    @Mock
    private RbacToAuthenticationConverter converter;
    @Mock
    private HttpServletRequest httpRequest;
    @Mock
    private Authentication auth;

    @BeforeEach
    public void setUp() throws Exception {

    }

    @Test
    public void shouldExtractRbacToken() {
        //given
        RbaToken token = TestTokenBuilder.newInstance().username("acclog").build();
        String encoded = Base64.getEncoder().encodeToString(token.getEncoded());
        when(httpRequest.getHeader(HttpHeaders.AUTHORIZATION)).thenReturn(encoded);
        DefaultRbacAuthenticationService service = new DefaultRbacAuthenticationService(converter);

        //when
        RbaToken rbaToken = service.extractRbacToken(httpRequest);

        //then
        assertNotNull(rbaToken);
    }

    @Test
    public void shouldConvertRbacTokenToAuthentication() {
        //given
        RbaToken token = TestTokenBuilder.newInstance().username("acclog").build();
        DefaultRbacAuthenticationService service = new DefaultRbacAuthenticationService(converter);
        when(converter.convert(any())).thenReturn(auth);

        //when
        Optional<Authentication> authentication = service.convertRbacTokenToAuthentication(token);

        //then
        assertNotNull(authentication.orElseThrow(() -> new IllegalStateException("Test failed")));
    }
}