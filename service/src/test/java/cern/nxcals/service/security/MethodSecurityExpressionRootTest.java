/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.security;

import cern.nxcals.api.domain.CreateEntityRequest;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.Partition;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.common.utils.ReflectionUtils;
import cern.nxcals.service.BaseTest;
import cern.nxcals.service.domain.EntityData;
import cern.nxcals.service.domain.PartitionData;
import cern.nxcals.service.domain.SystemSpecData;
import cern.nxcals.service.repository.EntityRepository;
import cern.nxcals.service.repository.SystemSpecRepository;
import cern.nxcals.service.security.resolvers.PermissionResolver;
import com.google.common.collect.Sets;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;

import static cern.nxcals.service.security.MethodSecurityExpressionRoot.ACCESS_WRITE;
import static cern.nxcals.service.security.resolvers.ConcatenatingPermissionResolver.ROLE_SEPARATOR;
import static java.util.Collections.emptyMap;
import static java.util.Collections.emptySortedSet;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@Transactional(transactionManager = "jpaTransactionManager")
public class MethodSecurityExpressionRootTest extends BaseTest {
    private final static long SYSTEM_ID = 1L;
    private final static long SYSTEM_ID2 = 2L;
    private final static long ENTITY_ID = 2L;
    private static final Map<String, Object> ENTITY_KEY_VALUE_MAP = Collections.singletonMap("key", "value1");
    private static final Map<String, Object> ENTITY_KEY_VALUE_MAP2 = Collections.singletonMap("key", "value2");
    private static final Map<String, Object> ENTITY_KEY_VALUE_MAP3 = Collections.singletonMap("key", "value3");

    @Mock
    private SystemSpecRepository systemRepository;

    @Mock
    private EntityRepository entityRepository;

    @Mock
    private PermissionResolver permissionResolver1;

    @Mock
    private PermissionResolver permissionResolver2;

    @Mock
    private PermissionResolver permissionResolver3;

    private MethodSecurityExpressionRoot securityExpressionRoot;

    @BeforeEach
    public void setup() {
        this.securityExpressionRoot = new MethodSecurityExpressionRoot(
                () -> SecurityContextHolder.getContext().getAuthentication(), systemRepository, entityRepository,
                Sets.newHashSet(permissionResolver1, permissionResolver2, permissionResolver3), ROLE_PREFIX);
    }

    @Test
    @WithMockUser(authorities = BaseTest.AUTHORITY)
    public void shouldAcceptAfterSystemChecking() {
        when(permissionResolver1.getPermissionString(eq(SYSTEM_NAME), eq(ACCESS_WRITE))).thenReturn(
                ROLE_PREFIX + SYSTEM_NAME + ROLE_SEPARATOR + ACCESS_WRITE);
        SystemSpecData system = createSystemSpecData(SYSTEM_NAME);

        assertTrue(securityExpressionRoot.hasSystemPermission(system, ACCESS_WRITE));
    }

    @Test
    @WithMockUser(authorities = BaseTest.AUTHORITY)
    public void shouldDenyAfterSystemWithWrongNameChecking() {
        String failName = "fail";
        when(permissionResolver1.getPermissionString(eq(failName), eq(ACCESS_WRITE))).thenReturn(
                failName + ":" + ACCESS_WRITE);
        SystemSpecData system = createSystemSpecData(failName);

        assertFalse(securityExpressionRoot.hasSystemPermission(system, ACCESS_WRITE));
    }

    @Test
    @WithMockUser(authorities = BaseTest.AUTHORITY)
    public void shouldDenyAfterSystemCheckingWithWrongPermission() {
        when(permissionResolver1.getPermissionString(eq(SYSTEM_NAME), eq(ACCESS_WRITE))).thenReturn(
                SYSTEM_NAME + ":" + ACCESS_WRITE);
        SystemSpecData system = createSystemSpecData(SYSTEM_NAME);

        assertFalse(securityExpressionRoot.hasSystemPermission(system, "fail"));
    }

    @Test
    @WithMockUser(authorities = BaseTest.ADMIN_AUTHORITY)
    public void shouldAllowAdminWithoutSpecificPermission() {
        when(permissionResolver1.getPermissionString(eq(SYSTEM_NAME), eq(ACCESS_WRITE))).thenReturn(AUTHORITY);
        assertTrue(securityExpressionRoot.hasPermission(SYSTEM_NAME, ACCESS_WRITE));
    }

    @Test
    @WithMockUser(authorities = BaseTest.ADMIN_AUTHORITY)
    public void shouldAllowAdminAfterSystemCheckingWithWrongPermission() {
        when(permissionResolver1.getPermissionString(eq(SYSTEM_NAME), eq(ACCESS_WRITE))).thenReturn(
                SYSTEM_NAME + ":" + ACCESS_WRITE);
        SystemSpecData system = createSystemSpecData(SYSTEM_NAME);

        assertTrue(securityExpressionRoot.hasSystemPermission(system, "ALL"));
    }

    @Test
    @WithMockUser(authorities = BaseTest.AUTHORITY)
    public void shouldAcceptAfterSystemIdChecking() {
        when(permissionResolver1.getPermissionString(eq(SYSTEM_NAME), eq(ACCESS_WRITE))).thenReturn(
                ROLE_PREFIX + SYSTEM_NAME + ROLE_SEPARATOR + ACCESS_WRITE);
        SystemSpecData system = createSystemSpecData(SYSTEM_NAME);
        when(systemRepository.findById(SYSTEM_ID)).thenReturn(Optional.of(system));

        assertTrue(securityExpressionRoot.hasSystemPermission(SYSTEM_ID, ACCESS_WRITE));
    }

    @Test
    @WithMockUser(authorities = BaseTest.AUTHORITY)
    public void shouldDenyAfterSysteIdmWithWrongNameChecking() {
        String failName = "fail";
        when(permissionResolver1.getPermissionString(eq(failName), eq(ACCESS_WRITE))).thenReturn(
                failName + ":" + ACCESS_WRITE);
        SystemSpecData system = createSystemSpecData(failName);
        when(systemRepository.findById(SYSTEM_ID)).thenReturn(Optional.of(system));

        assertFalse(securityExpressionRoot.hasSystemPermission(SYSTEM_ID, ACCESS_WRITE));
    }

    @Test
    @WithMockUser(authorities = BaseTest.AUTHORITY)
    public void shouldDenyAfterSystemIdCheckingWithWrongPermission() {
        when(permissionResolver1.getPermissionString(eq(SYSTEM_NAME), eq(ACCESS_WRITE))).thenReturn(
                SYSTEM_NAME + ":" + ACCESS_WRITE);
        SystemSpecData system = createSystemSpecData(SYSTEM_NAME);
        when(systemRepository.findById(SYSTEM_ID)).thenReturn(Optional.of(system));

        assertFalse(securityExpressionRoot.hasSystemPermission(SYSTEM_ID, "fail"));
    }

    @Test
    @WithMockUser(authorities = BaseTest.AUTHORITY)
    public void shouldAuthorizeIfAnyResolverReturnsMatchingString() {
        when(permissionResolver1.getPermissionString(eq(SYSTEM_NAME), eq(ACCESS_WRITE))).thenReturn("STH ELSE");
        when(permissionResolver2.getPermissionString(eq(SYSTEM_NAME), eq(ACCESS_WRITE))).thenReturn(
                ROLE_PREFIX + SYSTEM_NAME + ROLE_SEPARATOR + ACCESS_WRITE);
        when(permissionResolver3.getPermissionString(eq(SYSTEM_NAME), eq(ACCESS_WRITE))).thenReturn("WRONG");
        SystemSpecData system = createSystemSpecData(SYSTEM_NAME);

        assertTrue(securityExpressionRoot.hasSystemPermission(system, ACCESS_WRITE));
    }

    @Test
    @WithMockUser(authorities = BaseTest.AUTHORITY)
    public void shouldFailIfAllResolverReturnsNotMatchingString() {
        when(permissionResolver1.getPermissionString(eq(SYSTEM_NAME), eq(ACCESS_WRITE))).thenReturn("STH ELSE");
        when(permissionResolver2.getPermissionString(eq(SYSTEM_NAME), eq(ACCESS_WRITE))).thenReturn("WRONG");
        when(permissionResolver3.getPermissionString(eq(SYSTEM_NAME), eq(ACCESS_WRITE))).thenReturn("ALSO WRONG");
        SystemSpecData system = createSystemSpecData(SYSTEM_NAME);

        assertFalse(securityExpressionRoot.hasSystemPermission(system, ACCESS_WRITE));
    }

    @Test
    @WithMockUser(authorities = { BaseTest.AUTHORITY, BaseTest.AUTHORITY2 })
    public void shouldAuthorizeIfAllSystemsOfEntitiesArePermitted() {
        when(permissionResolver1.getPermissionString(eq(SYSTEM_NAME), eq(ACCESS_WRITE))).thenReturn(AUTHORITY);
        when(permissionResolver2.getPermissionString(eq(SYSTEM_NAME2), eq(ACCESS_WRITE))).thenReturn(AUTHORITY2);

        assertTrue(securityExpressionRoot.hasEntityPermission(
                Lists.newArrayList(mockEntity(SYSTEM_NAME), mockEntity(SYSTEM_NAME2), mockEntity(SYSTEM_NAME)),
                ACCESS_WRITE));
    }

    @Test
    @WithMockUser(authorities = { BaseTest.AUTHORITY, BaseTest.AUTHORITY2 })
    public void shouldAuthorizeIfAllSystemsOfEntitiesArePermitted2() {
        when(permissionResolver1.getPermissionString(eq(SYSTEM_NAME), eq(ACCESS_WRITE))).thenReturn(AUTHORITY);
        when(permissionResolver2.getPermissionString(eq(SYSTEM_NAME2), eq(ACCESS_WRITE))).thenReturn(AUTHORITY2);

        assertTrue(securityExpressionRoot.hasEntityPermission(
                Lists.newArrayList(mockEntity(SYSTEM_NAME), mockEntity(SYSTEM_NAME), mockEntity(SYSTEM_NAME)),
                ACCESS_WRITE));
    }

    @Test
    @WithMockUser(authorities = { BaseTest.AUTHORITY, BaseTest.AUTHORITY2 })
    public void shouldNotAuthorizeIfAnySystemOfEntitiesIsNotPermitted() {
        when(permissionResolver1.getPermissionString(eq(SYSTEM_NAME), eq(ACCESS_WRITE))).thenReturn(AUTHORITY);

        assertFalse(securityExpressionRoot.hasEntityPermission(
                Lists.newArrayList(mockEntity(SYSTEM_NAME), mockEntity(SYSTEM_NAME2), mockEntity(SYSTEM_NAME)),
                ACCESS_WRITE));
    }

    @Test
    @WithMockUser(authorities = { BaseTest.AUTHORITY, BaseTest.AUTHORITY2 })
    public void shouldAuthorizeIfAllSystemsOfCreateEntityRequestsArePermitted() {
        when(permissionResolver1.getPermissionString(eq(SYSTEM_NAME), eq(ACCESS_WRITE))).thenReturn(AUTHORITY);
        when(permissionResolver2.getPermissionString(eq(SYSTEM_NAME2), eq(ACCESS_WRITE))).thenReturn(AUTHORITY2);

        SystemSpecData system = createSystemSpecData(SYSTEM_NAME);
        when(systemRepository.findById(SYSTEM_ID)).thenReturn(Optional.of(system));

        SystemSpecData system2 = createSystemSpecData(SYSTEM_NAME2);
        when(systemRepository.findById(SYSTEM_ID2)).thenReturn(Optional.of(system2));

        assertTrue(securityExpressionRoot.hasSystemPermission(
                Sets.newHashSet(mockCreateEntityRequest(SYSTEM_ID, ENTITY_KEY_VALUE_MAP),
                        mockCreateEntityRequest(SYSTEM_ID2, ENTITY_KEY_VALUE_MAP2),
                        mockCreateEntityRequest(SYSTEM_ID, ENTITY_KEY_VALUE_MAP3)), ACCESS_WRITE));
    }

    @Test
    @WithMockUser(authorities = { BaseTest.AUTHORITY, BaseTest.AUTHORITY2 })
    public void shouldAuthorizeIfAllSystemsOfCreateEntityRequestsArePermitted2() {
        when(permissionResolver1.getPermissionString(eq(SYSTEM_NAME), eq(ACCESS_WRITE))).thenReturn(AUTHORITY);
        when(permissionResolver2.getPermissionString(eq(SYSTEM_NAME2), eq(ACCESS_WRITE))).thenReturn(AUTHORITY2);

        SystemSpecData system = createSystemSpecData(SYSTEM_NAME);
        when(systemRepository.findById(SYSTEM_ID)).thenReturn(Optional.of(system));

        assertTrue(securityExpressionRoot.hasSystemPermission(
                Sets.newHashSet(mockCreateEntityRequest(SYSTEM_ID, ENTITY_KEY_VALUE_MAP),
                        mockCreateEntityRequest(SYSTEM_ID, ENTITY_KEY_VALUE_MAP2),
                        mockCreateEntityRequest(SYSTEM_ID, ENTITY_KEY_VALUE_MAP3)), ACCESS_WRITE));
    }

    @Test
    @WithMockUser(authorities = { BaseTest.AUTHORITY, BaseTest.AUTHORITY2 })
    public void shouldNotAuthorizeIfAnySystemOfCreateEntityRequestsIsNotPermitted() {
        when(permissionResolver1.getPermissionString(eq(SYSTEM_NAME), eq(ACCESS_WRITE))).thenReturn(AUTHORITY);

        SystemSpecData system = createSystemSpecData(SYSTEM_NAME);
        when(systemRepository.findById(SYSTEM_ID)).thenReturn(Optional.of(system));

        SystemSpecData system2 = createSystemSpecData(SYSTEM_NAME2);
        when(systemRepository.findById(SYSTEM_ID2)).thenReturn(Optional.of(system2));

        assertFalse(securityExpressionRoot.hasSystemPermission(
                Sets.newHashSet(mockCreateEntityRequest(SYSTEM_ID, ENTITY_KEY_VALUE_MAP),
                        mockCreateEntityRequest(SYSTEM_ID2, ENTITY_KEY_VALUE_MAP2),
                        mockCreateEntityRequest(SYSTEM_ID, ENTITY_KEY_VALUE_MAP3)), ACCESS_WRITE));
    }

    @Test
    @WithMockUser(authorities = BaseTest.AUTHORITY)
    public void shouldAcceptAfterEntityIdChecking() {
        when(permissionResolver1.getPermissionString(eq(SYSTEM_NAME), eq(ACCESS_WRITE))).thenReturn(
                ROLE_PREFIX + SYSTEM_NAME + ROLE_SEPARATOR + ACCESS_WRITE);
        SystemSpecData system = createSystemSpecData(SYSTEM_NAME);
        EntityData entity = new EntityData();
        PartitionData partition = new PartitionData();
        partition.setSystem(system);
        entity.setPartition(partition);
        when(systemRepository.findById(SYSTEM_ID)).thenReturn(Optional.of(system));
        when(entityRepository.findById(ENTITY_ID)).thenReturn(Optional.of(entity));

        assertTrue(securityExpressionRoot.hasEntityPermission(ENTITY_ID, ACCESS_WRITE));
    }

    @Test
    @WithMockUser(authorities = BaseTest.AUTHORITY)
    public void shouldDenyAfterEntityIdWithWrongNameChecking() {
        String failName = "fail";
        when(permissionResolver1.getPermissionString(eq(failName), eq(ACCESS_WRITE))).thenReturn(
                failName + ":" + ACCESS_WRITE);
        SystemSpecData system = createSystemSpecData(failName);
        EntityData entity = new EntityData();
        PartitionData partition = new PartitionData();
        partition.setSystem(system);
        entity.setPartition(partition);
        when(systemRepository.findById(SYSTEM_ID)).thenReturn(Optional.of(system));
        when(entityRepository.findById(ENTITY_ID)).thenReturn(Optional.of(entity));

        assertFalse(securityExpressionRoot.hasSystemPermission(SYSTEM_ID, ACCESS_WRITE));
    }

    @Test
    @WithMockUser(authorities = BaseTest.AUTHORITY)
    public void shouldDenyAfterEntityIdCheckingWithWrongPermission() {
        when(permissionResolver1.getPermissionString(eq(SYSTEM_NAME), eq(ACCESS_WRITE))).thenReturn(
                SYSTEM_NAME + ":" + ACCESS_WRITE);
        SystemSpecData system = createSystemSpecData(SYSTEM_NAME);
        EntityData entity = new EntityData();
        PartitionData partition = new PartitionData();
        partition.setSystem(system);
        entity.setPartition(partition);
        when(systemRepository.findById(SYSTEM_ID)).thenReturn(Optional.of(system));
        when(entityRepository.findById(ENTITY_ID)).thenReturn(Optional.of(entity));

        assertFalse(securityExpressionRoot.hasSystemPermission(SYSTEM_ID, "fail"));
    }

    private Entity mockEntity(String systemName) {
        SystemSpec system = createSystem(systemName);
        return ReflectionUtils.builderInstance(Entity.InnerBuilder.class).id(-1).entityKeyValues(emptyMap())
                .systemSpec(system).partition(createPartition(system)).entityHistory(emptySortedSet())
                .lockedUntilStamp(null).recVersion(0).build();
    }

    private CreateEntityRequest mockCreateEntityRequest(long systemId, Map<String, Object> entityKeyValues) {
        return CreateEntityRequest.builder().systemId(systemId)
                .entityKeyValues(Collections.singletonMap("key", entityKeyValues)).partitionKeyValues(emptyMap())
                .build();
    }

    private Partition createPartition(SystemSpec system) {
        return ReflectionUtils.builderInstance(Partition.InnerBuilder.class).id(-1).systemSpec(system)
                .keyValues(emptyMap()).build();
    }

    private SystemSpec createSystem(String systemName) {
        return ReflectionUtils.builderInstance(SystemSpec.InnerBuilder.class).id(-1).name(systemName)
                .entityKeyDefinitions("").partitionKeyDefinitions("").timeKeyDefinitions("").build();
    }

    private SystemSpecData createSystemSpecData(String systemName) {
        SystemSpecData system = new SystemSpecData();
        system.setName(systemName);
        return system;
    }
}
