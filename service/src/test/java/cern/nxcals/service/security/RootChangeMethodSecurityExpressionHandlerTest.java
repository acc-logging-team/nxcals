/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.security;

import cern.nxcals.service.BaseTest;
import org.junit.jupiter.api.Test;
import org.springframework.expression.EvaluationContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.util.SimpleMethodInvocation;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Transactional(transactionManager = "jpaTransactionManager")
public class RootChangeMethodSecurityExpressionHandlerTest extends BaseTest {

    @Test
    @WithMockUser
    public void shouldSetMethodSecurityExpressionHandler() {
        // given
        RootChangeMethodSecurityExpressionHandler expressionHandler = new RootChangeMethodSecurityExpressionHandler();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        // when
        EvaluationContext returnedContext = expressionHandler.createEvaluationContext(
                () -> authentication, new SimpleMethodInvocation(this, this.getClass().getMethods()[0]));

        // then
        assertNotNull(returnedContext);
        assertNotNull(returnedContext.getRootObject().getValue());
        assertInstanceOf(MethodSecurityExpressionRoot.class, returnedContext.getRootObject().getValue());
    }

}