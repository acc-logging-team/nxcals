package cern.nxcals.service.security.rbac;

import cern.rbac.common.RbaToken;
import cern.rbac.common.test.TestTokenBuilder;
import lombok.Getter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class RbacAuthenticationFilterTest {

    @Mock
    private RbacAuthenticationService service;
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;

    private TestChain chain;
    @Mock
    private Authentication auth;

    @BeforeEach
    public void setUp() throws Exception {
        chain = new TestChain();
    }

    @Getter
    private static class TestChain implements FilterChain {
        private Authentication authentication;

        @Override
        public void doFilter(ServletRequest request, ServletResponse response) {
            authentication = SecurityContextHolder.getContext().getAuthentication();
        }
    }

    @Test
    public void shouldDoFilterInternalAndSetAuthAndRbacToken() throws ServletException, IOException {
        //given
        RbaToken token = TestTokenBuilder.newInstance().username("acclog").build();
        when(service.extractRbacToken(any())).thenReturn(token);
        when(service.convertRbacTokenToAuthentication(any())).thenReturn(Optional.of(auth));
        RbacAuthenticationFilter filter = new RbacAuthenticationFilter(service);
        //when
        filter.doFilterInternal(request, response, chain);
        //then
        assertEquals(auth, chain.getAuthentication());
    }
}
