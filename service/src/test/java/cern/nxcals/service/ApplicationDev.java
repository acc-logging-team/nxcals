/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service;

import cern.nxcals.service.security.resolvers.DevPermissionResolver;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

/**
 * This class starts local instance of nxcals-service as well as local KDC server.
 * As service's endpoints are protected by Kerberos, client has to set following properties in order to use it locally.
 *
 * try {
 * System.setProperty("service.url", "https://" + InetAddress.getLocalHost().getHostName() + ":<service-port>"); //Do not use localhost here.
 * } catch (UnknownHostException e) {
 * throw new RuntimeException("Cannot acquire hostname programmatically, provide the name full name of localhost");
 * }
 *
 * Name of the user. To change it go to {@link ApplicationDev#TEST_PRINCIPAL} variable.
 * Also the same name must be used in application.properties in kerberos.principal property.
 * Do not set this variable to your username if you have Kerberos ticket cached,
 * this will cause conflicts during authentication with local KDC instance.
 *
 * System.setProperty("kerberos.principal", "mock-system-user");
 *
 * This keytab is created automatically. It contains local service principle
 * and user principle (with username as above)
 *
 * System.setProperty("kerberos.keytab", ".service.keytab");
 *
 * This conf is created automatically.
 * It contains configuration necessary to run local instance of KDC.
 *
 * System.setProperty("java.security.krb5.conf", "build/LocalKdc/krb5.conf");
 *
 * Also in application.properties unit test database credentials are set. You probably want to change it to use your personal DB instance.
 */
@Slf4j
@SpringBootApplication
public class ApplicationDev {
//    static {
//        try {
//            SSLUtils.turnOffSslChecking();
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        } catch (KeyManagementException e) {
//            e.printStackTrace();
//        }
//    }
public static void main(String[] args) {
    SpringApplication app = new SpringApplication(ApplicationDev.class);
    app.setAdditionalProfiles("dev");
    app.addListeners(new ApplicationPidFileWriter());
    ConfigurableApplicationContext context = app.run(args);
    IdGeneratorFactory.setContext(context);
}

    //    @Bean
    //    @Profile("dev")
    //    public DevPermissionResolver devPermissionResolver() {
    //        return new DevPermissionResolver();
    //    }

}
