package cern.nxcals.service.config;

import cern.nxcals.api.domain.EntityHistory;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static cern.nxcals.common.Constants.HBASE_TABLE_NAME_DELIMITER;
import static cern.nxcals.common.Constants.PERMANENT_TABLE_PREFIX;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.when;

class DataAccessConfigTest {

    private static DataAccessConfig instance;

    @BeforeAll
    static void setUp() {
        instance = new DataAccessConfig(new DataLocationProperties());
    }

    @Test
    void shouldProvideTableNameForNotUpdatablePartition() {
        //given
        EntityHistory entityHistory = Mockito.mock(EntityHistory.class, RETURNS_DEEP_STUBS);
        when(entityHistory.getPartition().getProperties().isUpdatable()).thenReturn(false);
        when(entityHistory.getPartition().getSystemSpec().getId()).thenReturn(1L);
        when(entityHistory.getPartition().getId()).thenReturn(2L);
        when(entityHistory.getEntitySchema().getId()).thenReturn(3L);
        //when
        String providedTableName = instance.multiTable().apply(entityHistory);
        //then
        String expectedTableName = 1L + HBASE_TABLE_NAME_DELIMITER + 2L + HBASE_TABLE_NAME_DELIMITER + 3L;
        assertEquals(expectedTableName, providedTableName);
    }

    @Test
    void shouldProvideTableNameForUpdatablePartition() {
        //given
        EntityHistory entityHistory = Mockito.mock(EntityHistory.class, RETURNS_DEEP_STUBS);
        when(entityHistory.getPartition().getProperties().isUpdatable()).thenReturn(true);
        when(entityHistory.getPartition().getSystemSpec().getId()).thenReturn(1L);
        when(entityHistory.getPartition().getId()).thenReturn(2L);
        when(entityHistory.getEntitySchema().getId()).thenReturn(3L);
        //when
        String providedTableName = instance.multiTable().apply(entityHistory);
        //then
        String expectedTableName =
                PERMANENT_TABLE_PREFIX + HBASE_TABLE_NAME_DELIMITER + 1L + HBASE_TABLE_NAME_DELIMITER + 2L
                        + HBASE_TABLE_NAME_DELIMITER + 3L;
        assertEquals(expectedTableName, providedTableName);
    }
}