package cern.nxcals.service.tracing;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TracingRequestFilterTest {

    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private FilterChain chain;

    @Test
    public void shouldPrintStatsWithScheduler() throws InterruptedException {
        //given
        CountDownLatch latch = new CountDownLatch(1);
        TracingRequestFilter filter = new TracingRequestFilter(latch::countDown);
        filter.setDisplayStatsFreq(Duration.of(1, ChronoUnit.MILLIS));

        //when
        filter.init();

        //then
        assertTrue(latch.await(10, TimeUnit.SECONDS));

    }

    @Nested
    class TracingRequestFilterTestNested {
        @BeforeEach
        public void setup() {
            when(request.getRequestURI()).thenReturn("/test1").thenReturn("/test2");
            when(request.getRemoteUser()).thenReturn("user@CERN.CH").thenReturn("user2@CERN.CH");

        }

        @Test
        public void shouldAddToCacheAndRemoveRealm() throws ServletException, IOException {
            //given
            TracingRequestFilter filter = new TracingRequestFilter();

            //when
            filter.doFilterInternal(request, response, chain);
            filter.doFilterInternal(request, response, chain);

            //then
            assertEquals(2, filter.getAccessCache().asMap().size());
            assertTrue(filter.getAccessCache().asMap().keySet().stream().noneMatch(s -> s.contains("@CERN.CH")));

        }

        @Test
        public void shouldNotRemoveRealm() throws ServletException, IOException {
            //given
            TracingRequestFilter filter = new TracingRequestFilter();
            filter.setRemoveUserRealm(false);

            //when
            filter.doFilterInternal(request, response, chain);
            filter.doFilterInternal(request, response, chain);

            //then
            assertTrue(filter.getAccessCache().asMap().keySet().stream().allMatch(s -> s.contains("@CERN.CH")));

        }
    }

}