package cern.nxcals.service.internal.compaction;

import cern.nxcals.common.domain.MergeCompactionJob;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MergeCompactionJobCreatorTest extends CompactionTestUtils {
    private static final int MAX_PARTITION_SIZE = 1024 * 1024 * 2;

    @Mock
    private FileSystem fs;

    private MergeCompactionJobCreator creator;

    @BeforeEach
    void init() {
        creator = new MergeCompactionJobCreator(SORT_ABOVE, MAX_PARTITION_SIZE, AVRO_PARQUET_RATIO, "x", "y", fs,
                x -> true);
    }

    @Nested
    class CreateJobTests {
        @Test
        void shouldPartitionLargeFiles() {
            int fileSize = MAX_PARTITION_SIZE + 1;
            Set<FileStatus> files = generateUnbucketedFiles(1, fileSize);

            long jobSize = DataProcessingJobCreator.sizeOf(files);
            MergeCompactionJob job = creator.createJob(STAGING_PATH, files, jobSize, jobSize, "prefix");

            assertEquals(files.size(), job.getFiles().size());
            assertEquals(files.size() * fileSize, job.getJobSize());
            assertEquals("prefix", job.getFilePrefix());
            assertEquals(2, job.getPartitionCount());
        }

        @Test
        void shouldPartitionLargeFilesWithRatio() {
            creator = new MergeCompactionJobCreator(SORT_ABOVE, MAX_PARTITION_SIZE, 5, "x", "y", fs, x -> true);

            int fileSize = MAX_PARTITION_SIZE - 1;
            Set<FileStatus> files = generateUnbucketedFiles(10, fileSize);

            long jobSize = DataProcessingJobCreator.sizeOf(files);
            MergeCompactionJob job = creator.createJob(STAGING_PATH, files, jobSize, jobSize, "prefix");

            assertEquals(files.size(), job.getFiles().size());
            assertEquals(files.size() * fileSize, job.getJobSize());
            assertEquals("prefix", job.getFilePrefix());
            assertEquals(2, job.getPartitionCount());
        }

        @Test
        void shouldNotPartitionSmallFiles() {
            int fileSize = MAX_PARTITION_SIZE / 1024;
            Set<FileStatus> files = generateUnbucketedFiles(1, fileSize);

            long jobSize = DataProcessingJobCreator.sizeOf(files);
            MergeCompactionJob job = creator.createJob(STAGING_PATH, files, jobSize, jobSize, "prefix");

            assertEquals(files.size(), job.getFiles().size());
            assertEquals(jobSize, job.getJobSize());
            assertEquals("prefix", job.getFilePrefix());
            assertEquals(1, job.getPartitionCount());
        }

        @Test
        void shouldNotSortSmallJobs() {
            int fileSize = SORT_ABOVE / 1024;
            Set<FileStatus> files = generateMixedFiles(fileSize, 100, 100);

            long jobSize = DataProcessingJobCreator.sizeOf(files);
            MergeCompactionJob job = creator.createJob(STAGING_PATH, files, jobSize, jobSize, "prefix");

            assertFalse(job.isSortEnabled());
        }

        @Test
        void shouldSortLargeJobs() {
            int fileSize = SORT_ABOVE + 1;
            Set<FileStatus> files = generateMixedFiles(fileSize, 100, 100);

            long jobSize = DataProcessingJobCreator.sizeOf(files);
            MergeCompactionJob job = creator.createJob(STAGING_PATH, files, jobSize, jobSize, "prefix");

            assertTrue(job.isSortEnabled());
        }

        @Test
        void shouldSortLargeJobsWithRatio() {
            int ratio = 5;
            creator = new MergeCompactionJobCreator(SORT_ABOVE, MAX_PARTITION_SIZE, ratio, "x", "y", fs, x -> true);

            int fileSize = SORT_ABOVE * ratio + 1;
            Set<FileStatus> files = generateUnbucketedFiles(10, fileSize);

            long jobSize = DataProcessingJobCreator.sizeOf(files);
            MergeCompactionJob job = creator.createJob(STAGING_PATH, files, jobSize, jobSize, "prefix");

            assertTrue(job.isSortEnabled());
        }
    }

    @Nested
    class PathCheckerTests {
        private void initMock(boolean reportFileExist) throws Exception {
            Path reportFile = new Path(creator.getHdfsPathCreator().toRestageReportFileUri(STAGING_PATH));
            when(fs.exists(reportFile)).thenReturn(reportFileExist);
        }

        @Test
        void shouldReturnFalseWhenReportDoesNotExists() throws Exception {
            initMock(false);
            assertFalse(creator.getPathChecker().test(STAGING_PATH));
        }

        @Test
        void shouldReturnTrueWhenReportExists() throws Exception {
            initMock(true);
            assertTrue(creator.getPathChecker().test(STAGING_PATH));
        }
    }

}
