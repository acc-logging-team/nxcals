package cern.nxcals.service.internal.compaction;

import cern.nxcals.api.domain.TimeEntityPartitionType;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.common.domain.AdaptiveCompactionJob;
import cern.nxcals.common.paths.StagingPath;
import cern.nxcals.service.domain.PartitionData;
import cern.nxcals.service.domain.PartitionPropertiesData;
import cern.nxcals.service.domain.PartitionResourceData;
import cern.nxcals.service.domain.PartitionResourceHistoryData;
import cern.nxcals.service.internal.PartitionResourceHistoryService;
import cern.nxcals.service.internal.PartitionResourceService;
import cern.nxcals.service.internal.PartitionService;
import com.google.common.collect.Lists;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.time.Instant;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Stream;

import static cern.nxcals.api.utils.TimeUtils.TimeSplits.FIFTEEN_MINUTE_SPLIT;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AdaptiveCompactionJobCreatorTest {


    @Mock
    private PartitionResourceService partitionResourceService;
    @Mock
    private PartitionResourceHistoryService partitionResourceHistoryService;
    private final long sortThreshold = 2 * 1024 * 2014;
    private final long maxPartitionSize = AdaptiveCompactionJobCreator.ONE_GB_FILE_SIZE;
    private final int avroParquetSizeRation = 4;
    private final String dataPrefix = "/project/nxcals/nxcals_pro/data";
    private final String stagingPrefix = "/project/nxcals/nxcals_pro/staging";
    @Mock
    private FileSystem fileSystem;
    private AdaptiveCompactionJobCreator creator;
    @Mock
    private RemoteIterator<LocatedFileStatus> fileIterator;
    @Mock
    private LocatedFileStatus file;

    @Mock
    private FileStatus file1, file2, file3, file4, file5, file6, file7, file8;

    @Mock
    private PartitionService partitionService;
    @Mock
    private PartitionResourceData partitionResourceData;

    @Mock
    private PartitionResourceHistoryData partitionResourceHistoryData;

    @BeforeEach
    void setUp() {
        creator = new AdaptiveCompactionJobCreator(partitionService, partitionResourceService,
                partitionResourceHistoryService, 4L,
                sortThreshold, maxPartitionSize, avroParquetSizeRation, dataPrefix, stagingPrefix, fileSystem);
    }

    public static Stream<Arguments> adaptiveSplits() {
        return Stream.of(
                Arguments.of("2024-01-02 00:00:00",
                        new PartitionPropertiesData(false, FIFTEEN_MINUTE_SPLIT.name(), null, null),
                        FIFTEEN_MINUTE_SPLIT.getSize()),
                Arguments.of("2024-01-02 00:00:00",
                        new PartitionPropertiesData(false, FIFTEEN_MINUTE_SPLIT.name(), null,
                                TimeUtils.getInstantFromString("2024-01-01 00:00:00")), 10L)
        );

    }

    @ParameterizedTest
    @MethodSource("adaptiveSplits")
    void shouldHandleAdaptiveNumberOfStagingTimePartitions(String stagingDirTime, PartitionPropertiesData properties,
            long output) {
        //given
        PartitionData partitionData = mock(PartitionData.class);
        Instant time = TimeUtils.getInstantFromString(stagingDirTime);
        when(partitionData.getProperties()).thenReturn(properties);

        //when
        long stagingPartitions = AdaptiveCompactionJobCreator.findMaxNumberOfStagingPartitions(partitionData,
                time, 10L);

        assertEquals(output, stagingPartitions);

    }
    @Test
    void shouldCreateJob() {
        //given
        String stagingBasePath = "/project/nxcals/nxcals_pro/staging/ETL1/1/2/3/2022-07-05";
        String dataBasePath = "/project/nxcals/nxcals_pro/data/1/2/3/2022/7/5";

        StagingPath stagingPath = new StagingPath(new Path(stagingBasePath), true);
        when(file1.getPath()).thenReturn(new Path(stagingBasePath + "/0/file1.avro"));
        when(file2.getPath()).thenReturn(new Path(stagingBasePath + "/0/file2.avro"));
        when(file3.getPath()).thenReturn(new Path(stagingBasePath + "/1/file3.avro"));
        when(file4.getPath()).thenReturn(new Path(stagingBasePath + "/1/file4.avro"));
        when(file5.getPath()).thenReturn(new Path(stagingBasePath + "/2/file5.avro"));
        when(file6.getPath()).thenReturn(new Path(stagingBasePath + "/3/file6.avro"));
        when(file7.getPath()).thenReturn(new Path(stagingBasePath + "/__sys_nxcals_time_partition__=0-file7.parquet"));
        when(file8.getPath()).thenReturn(new Path(stagingBasePath + "/__sys_nxcals_time_partition__=1-file8.parquet"));

        when(file1.getLen()).thenReturn(100L);
        when(file2.getLen()).thenReturn(100L);
        when(file3.getLen()).thenReturn(100L);
        when(file4.getLen()).thenReturn(100L);
        when(file5.getLen()).thenReturn(100L);
        when(file6.getLen()).thenReturn(100L);
        when(file7.getLen()).thenReturn(100L);
        when(file8.getLen()).thenReturn(100L);

        when(file7.isFile()).thenReturn(true);
        when(file8.isFile()).thenReturn(true);

        Collection<FileStatus> files = Lists.newArrayList(file1, file2, file3, file4, file5, file6, file7, file8);

        when(partitionResourceService.findOrCreatePartitionResourceData(1, 2, 3)).thenReturn(partitionResourceData);
        when(partitionResourceHistoryService.findOrCreatePartitionResourceInfoFor(argThat(v -> true), argThat(v -> true))).thenReturn(partitionResourceHistoryData);
        when(partitionResourceHistoryData.getInformation()).thenReturn(TimeEntityPartitionType.of(4, 2).toString());

        PartitionData partition = mock(PartitionData.class);
        PartitionPropertiesData partitionPropertiesData = mock(PartitionPropertiesData.class);
        when(partition.getProperties()).thenReturn(partitionPropertiesData);

        when(partitionService.findById(2)).thenReturn(Optional.of(partition));

        //when
        Optional<AdaptiveCompactionJob> jobOptional = creator.create(stagingPath, files);


        //then
        assertTrue(jobOptional.isPresent());
        AdaptiveCompactionJob job = jobOptional.get();
        assertEquals(dataBasePath, job.getDestinationDir().getPath());
        assertEquals("", job.getFilePrefix()); //no prefix should be used
        System.out.println(job.getFiles());
        assertEquals(3, job.getFiles().size());
        assertTrue(job.getFiles().contains(new Path(stagingBasePath + "/0/file1.avro").toUri()));
        assertTrue(job.getFiles().contains(new Path(stagingBasePath + "/0/file2.avro").toUri()));
        assertTrue(job.getFiles()
                .contains(new Path(stagingBasePath + "/__sys_nxcals_time_partition__=0-file7.parquet").toUri()));

        assertEquals(1, job.getSystemId());
        assertEquals(2, job.getPartitionId());
        assertEquals(3, job.getSchemaId());
        assertEquals(TimeEntityPartitionType.of(4, 2), job.getPartitionType());

    }


    static Stream<Arguments> partitionValues() {
        return Stream.of(
                Arguments.of(0L, 10L, 2, 0L),
                Arguments.of(5L, 10L, 2, 1L),
                Arguments.of(3L, 10L, 2, 0L),
                Arguments.of(9L, 10L, 2, 1L),
                Arguments.of(9L, 100L, 10, 0L),
                Arguments.of(19L, 100L, 10, 1L),
                Arguments.of(29L, 100L, 10, 2L),
                Arguments.of(99L, 100L, 10, 9L),
                //if the output partition is more than the input ones
                //the output is equal to the input.
                Arguments.of(0L, 24L, 48, 0L),
                Arguments.of(1L, 24L, 48, 1L),
                Arguments.of(23L, 24L, 48, 23L)

        );
    }

    @ParameterizedTest
    @MethodSource("partitionValues")
    public void shouldCalculateCorrectOutputTimePartition(long inputPartition, long maxInputPartitions,
            int maxOutputPartitions, long correctOutputPartition) {
        assertEquals(correctOutputPartition,
                AdaptiveCompactionJobCreator.calculateOutputPartition(inputPartition, maxInputPartitions,
                        maxOutputPartitions),
                () -> "Wrong value for input partition " + inputPartition + " max input " + maxInputPartitions + " max output " + maxOutputPartitions);
    }

    static Stream<Arguments> fileNames() {
        return Stream.of(
                Arguments.of(
                        "__sys_nxcals_time_partition__=43-__sys_nxcals_entity_bucket__=7-part-00004-3c4ae51c-3035-490a-bc39-17f302a45ee4.c000.snappy.parquet",
                        43L),
                Arguments.of(
                        "__sys_nxcals_time_partition__=1-__sys_nxcals_entity_bucket__=7-part-00004-3c4ae51c-3035-490a-bc39-17f302a45ee4.c000.snappy.parquet",
                        1L),
                Arguments.of(
                        "__sys_nxcals_time_partition__=0-__sys_nxcals_entity_bucket__=7-part-00004-3c4ae51c-3035-490a-bc39-17f302a45ee4.c000.snappy.parquet",
                        0L),
                Arguments.of(
                        "__sys_nxcals_time_partition__=243-__sys_nxcals_entity_bucket__=7-part-00004-3c4ae51c-3035-490a-bc39-17f302a45ee4.c000.snappy.parquet",
                        243L),
                Arguments.of(
                        "__sys_nxcals_time_partition__=57-__sys_nxcals_entity_bucket__=7-part-00004-3c4ae51c-3035-490a-bc39-17f302a45ee4.c000.snappy.parquet",
                        57L),
                Arguments.of(
                        "__sys_nxcals_time_partition__=99-__sys_nxcals_entity_bucket__=7-part-00004-3c4ae51c-3035-490a-bc39-17f302a45ee4.c000.snappy.parquet",
                        99L),
                Arguments.of(
                        "__sys_nxcals_time_partition__=101-__sys_nxcals_entity_bucket__=7-part-00004-3c4ae51c-3035-490a-bc39-17f302a45ee4.c000.snappy.parquet",
                        101L),
                Arguments.of(
                        "__sys_nxcals_time_partition__=25-__sys_nxcals_entity_bucket__=7-part-00004-3c4ae51c-3035-490a-bc39-17f302a45ee4.c000.snappy.parquet",
                        25L),
                Arguments.of(
                        "__sys_nxcals_time_partition__=11-__sys_nxcals_entity_bucket__=7-part-00004-3c4ae51c-3035-490a-bc39-17f302a45ee4.c000.snappy.parquet",
                        11L)
        );
    }

    @ParameterizedTest
    @MethodSource("fileNames")
    public void shouldFindTimePartitionInFileNames(String name, long partition) {
        assertEquals(partition, AdaptiveCompactionJobCreator.parseFilenameForTimePartition(name).orElseThrow(),
                () -> "Wrong partition for " + name);
    }

    @Test
    public void shouldReturnEmptyFindTimePartitionInFileNames() {
        assertTrue(AdaptiveCompactionJobCreator.parseFilenameForTimePartition(
                "part-00004-3c4ae51c-3035-490a-bc39-17f302a45ee4.c000.snappy.parquet").isEmpty());
    }

    @Nested
    class SelectPartitioningTypeTests {
        @Test
        void shouldSelectTypeSmallerThan11GB() {
            //given
            long size = 11 * AdaptiveCompactionJobCreator.ONE_GB_FILE_SIZE;
            //when
            TimeEntityPartitionType type = AdaptiveCompactionJobCreator.selectPartitioningType(size);

            //then
            assertEquals(TimeEntityPartitionType.of(4, 3), type);
        }

        @Test
        void shouldSelectTypeSmallest() {
            //given
            long size = (long) (0.5 * AdaptiveCompactionJobCreator.ONE_GB_FILE_SIZE);
            //when
            TimeEntityPartitionType type = AdaptiveCompactionJobCreator.selectPartitioningType(size);

            //then
            assertEquals(TimeEntityPartitionType.of(1, 1), type);
        }

        @Test
        void shouldSelectType2To4Gb() {
            //given
            long size = (long) (1.52 * AdaptiveCompactionJobCreator.ONE_GB_FILE_SIZE);
            //when
            TimeEntityPartitionType type = AdaptiveCompactionJobCreator.selectPartitioningType(size);

            //then
            assertEquals(TimeEntityPartitionType.of(2, 2), type);
        }

        @Test
        void shouldSelectType1To1Gb() {
            //given - it will be rounded to 1
            long size = (long) (1.4 * AdaptiveCompactionJobCreator.ONE_GB_FILE_SIZE);
            //when
            TimeEntityPartitionType type = AdaptiveCompactionJobCreator.selectPartitioningType(size);

            //then
            assertEquals(TimeEntityPartitionType.of(1, 1), type);
        }

        @Test
        void shouldSelectType4Gb() {
            //given
            long size = (long) (3.99999 * AdaptiveCompactionJobCreator.ONE_GB_FILE_SIZE);
            //when
            TimeEntityPartitionType type = AdaptiveCompactionJobCreator.selectPartitioningType(size);

            //then
            assertEquals(TimeEntityPartitionType.of(2, 2), type);
        }

        @Test
        void shouldSelectTypeOver4Gb() {
            //given
            long size = (long) (4.501 * AdaptiveCompactionJobCreator.ONE_GB_FILE_SIZE);
            //when
            TimeEntityPartitionType type = AdaptiveCompactionJobCreator.selectPartitioningType(size);

            //then
            //first smaller than 10 GB
            assertEquals(TimeEntityPartitionType.of(3, 2), type);
        }

        @Test
        void shouldSelectTypeOver6Gb() {
            //given
            long size = (long) (6.501 * AdaptiveCompactionJobCreator.ONE_GB_FILE_SIZE);
            //when
            TimeEntityPartitionType type = AdaptiveCompactionJobCreator.selectPartitioningType(size);

            //then
            assertEquals(TimeEntityPartitionType.of(4, 2), type);
        }

        @Test
        void shouldSelectTypeOverTheLimit() {
            //given
            long size = 1500 * AdaptiveCompactionJobCreator.ONE_GB_FILE_SIZE;
            //when
            TimeEntityPartitionType type = AdaptiveCompactionJobCreator.selectPartitioningType(size);

            //then
            assertEquals(TimeEntityPartitionType.of(96, 14), type);
        }
    }

    @Nested
    class PathCheckerTests {
        @Test
        void shouldNotVerifyJobPathAsDataExistsWithNoResourceInfoAssigned() throws IOException {
            //given
            StagingPath stagingPath = new StagingPath(
                    new Path("/project/nxcals/nxcals_pro/staging/ETL1/1/2/3/2022-07-05"), true);
            when(fileSystem.exists(
                    argThat(p -> p.toString().equals("/project/nxcals/nxcals_pro/data/1/2/3/2022/7/5")))).thenReturn(
                    true);
            when(fileSystem.listFiles(argThat(
                            p -> p.toString().equals("/project/nxcals/nxcals_pro/data/1/2/3/2022/7/5")),
                    anyBoolean()))
                    .thenReturn(fileIterator);

            when(fileIterator.hasNext()).thenReturn(true).thenReturn(false);
            when(fileIterator.next()).thenReturn(file);
            when(file.isFile()).thenReturn(true);
            when(file.getPath()).thenReturn(new Path("/project/nxcals/nxcals_pro/data/1/2/3/2022/7/5/file.parquet"));
            //when
            boolean output = creator.getPathChecker().test(stagingPath);
            //then
            assertFalse(output);
        }

        @Test
        void shouldVerifyJobPathAsDataExistsAndResourceInfoAssigned() throws IOException {
            //given
            StagingPath stagingPath = new StagingPath(
                    new Path("/project/nxcals/nxcals_pro/staging/ETL1/1/2/3/2022-07-05"), true);
            when(fileSystem.exists(
                    argThat(p -> p.toString().equals("/project/nxcals/nxcals_pro/data/1/2/3/2022/7/5")))).thenReturn(
                    true);
            when(fileSystem.listFiles(argThat(
                            p -> p.toString().equals("/project/nxcals/nxcals_pro/data/1/2/3/2022/7/5")),
                    anyBoolean()))
                    .thenReturn(fileIterator);

            when(fileIterator.hasNext()).thenReturn(true).thenReturn(false);
            when(fileIterator.next()).thenReturn(file);
            when(file.isFile()).thenReturn(true);
            when(file.getPath()).thenReturn(new Path("/project/nxcals/nxcals_pro/data/1/2/3/2022/7/5/file.parquet"));

            when(partitionResourceService
                    .findBySystemIdAndPartitionIdAndSchemaId(1, 2, 3))
                    .thenReturn(Optional.of(partitionResourceData));
            when(partitionResourceHistoryService
                    .findByPartitionResourceAndTimestamp(partitionResourceData, stagingPath.getDate().toInstant()))
                    .thenReturn(Optional.of(partitionResourceHistoryData));
            //when
            boolean output = creator.getPathChecker().test(stagingPath);
            //then
            assertTrue(output);
        }

        @Test
        void shouldVerifyJobPathWithNoData() throws IOException {
            //given
            StagingPath stagingPath = new StagingPath(
                    new Path("/project/nxcals/nxcals_pro/staging/ETL1/1/2/3/2022-07-05"), true);
            when(fileSystem.exists(
                    argThat(p -> p.toString().equals("/project/nxcals/nxcals_pro/data/1/2/3/2022/7/5")))).thenReturn(
                    true);
            when(fileSystem.listFiles(argThat(
                            p -> p.toString().equals("/project/nxcals/nxcals_pro/data/1/2/3/2022/7/5")),
                    anyBoolean()))
                    .thenReturn(fileIterator);

            when(fileIterator.hasNext()).thenReturn(false);
            //when
            boolean output = creator.getPathChecker().test(stagingPath);
            //then
            assertTrue(output);
        }
    }
}
