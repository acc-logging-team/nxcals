package cern.nxcals.service.internal.compaction;

import cern.nxcals.common.domain.CompactionJob;
import cern.nxcals.common.paths.StagingPath;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Set;
import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
class CompactionJobCreatorTest extends CompactionTestUtils {
    private static final int MAX_PARTITION_SIZE = 1024 * 1024 * 2;
    private static final int SORT_ABOVE = 1024 * 1024;
    private static final int AVRO_PARQUET_RATIO = 1;
    @Mock
    private FileSystem fs;
    private CompactionJobCreator creator;

    private Predicate<StagingPath> pathChecker = (path) -> true;

    @BeforeEach
    public void init() {
        creator = new CompactionJobCreator(pathChecker, SORT_ABOVE, MAX_PARTITION_SIZE, AVRO_PARQUET_RATIO, "x", "y", fs);
    }

    @Nested
    class CreateJobTests {
        @Test
        void shouldPartitionLargeFilesBucketed() {
            int fileSize = MAX_PARTITION_SIZE + 1;
            Set<FileStatus> files = generateBucketedFiles(1, fileSize, "04");
            CompactionJob job = creator.createJob(STAGING_PATH, files, fileSize, DataProcessingJobCreator.sizeOf(files),
                    "prefix");

            assertEquals(files.size(), job.getFiles().size());
            assertEquals(files.size() * fileSize, job.getJobSize());
            assertEquals("prefix", job.getFilePrefix());
            assertEquals(2, job.getPartitionCount());
        }

        @Test
        void shouldNotPartitionSmallFilesBucketed() {
            int fileSize = MAX_PARTITION_SIZE / 1024;
            Set<FileStatus> files = generateBucketedFiles(1, fileSize, "06");

            CompactionJob job = creator.createJob(STAGING_PATH, files, fileSize, DataProcessingJobCreator.sizeOf(files),
                    "prefix");

            assertEquals(files.size(), job.getFiles().size());
            assertEquals(files.size() * fileSize, job.getJobSize());
            assertEquals("prefix", job.getFilePrefix());
            assertEquals(1, job.getPartitionCount());
        }

        @Test
        void shouldPartitionLargeFilesWithRatio() {
            creator = new CompactionJobCreator(pathChecker, SORT_ABOVE, MAX_PARTITION_SIZE, 5, "x", "y", fs);

            int fileSize = MAX_PARTITION_SIZE - 1;
            Set<FileStatus> files = generateUnbucketedFiles(10, fileSize);

            CompactionJob job = creator.createJob(STAGING_PATH, files, DataProcessingJobCreator.sizeOf(files),
                    DataProcessingJobCreator.sizeOf(files), "prefix");

            assertEquals(files.size(), job.getFiles().size());
            assertEquals(files.size() * fileSize, job.getJobSize());
            assertEquals("prefix", job.getFilePrefix());
            assertEquals(2, job.getPartitionCount());
        }

        @Test
        void shouldNotSortSmallJobs() {
            int fileSize = SORT_ABOVE / 1024;
            Set<FileStatus> files = generateMixedFiles(fileSize, 100, 100);

            CompactionJob job = creator.createJob(STAGING_PATH, files, DataProcessingJobCreator.sizeOf(files),
                    DataProcessingJobCreator.sizeOf(files), "prefix");

            assertFalse(job.isSortEnabled());
        }

        @Test
        void shouldSortLargeJobs() {
            int fileSize = SORT_ABOVE + 1;
            Set<FileStatus> files = generateMixedFiles(fileSize, 100, 100);

            CompactionJob job = creator.createJob(STAGING_PATH, files, DataProcessingJobCreator.sizeOf(files),
                    DataProcessingJobCreator.sizeOf(files), "prefix");

            assertTrue(job.isSortEnabled());
        }
    }
}
