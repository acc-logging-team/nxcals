package cern.nxcals.service.internal;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.Group;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.api.domain.Visibility;
import cern.nxcals.api.extraction.metadata.queries.Groups;
import cern.nxcals.common.utils.ReflectionUtils;
import cern.nxcals.service.BaseTest;
import cern.nxcals.service.domain.EntityData;
import cern.nxcals.service.domain.GroupData;
import cern.nxcals.service.domain.StandardPersistentEntity;
import cern.nxcals.service.domain.SystemSpecData;
import cern.nxcals.service.domain.VariableData;
import cern.nxcals.service.domain.VisibilityData;
import cern.nxcals.service.rest.exceptions.AccessDeniedRuntimeException;
import cern.nxcals.service.rest.exceptions.ConfigDataConflictException;
import cern.nxcals.service.rest.exceptions.NotFoundRuntimeException;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Sets;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_KEY_VALUES_1;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_1;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_DOUBLE_SCHEMA_KEY_1;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_STRING_SCHEMA_KEY_1;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Transactional(transactionManager = "jpaTransactionManager")
@Rollback
public class GroupServiceTest extends BaseTest {
    public static final String MAIN_USER = "MAIN_USER";
    public static final String OTHER_USER = "OTHER_USER";
    public static final String THIRD_USER = "THIRD_USER";

    @Autowired
    private GroupService groupService;

    @Autowired
    private VariableService variableService;

    @Autowired
    private EntityService entityService;

    private SystemSpec system;

    @BeforeEach
    public void init() {
        setAuthentication();
        system = createAndPersistSystem("my-system");
    }

    @Test
    public void shouldCreateEmptyGroup() {
        authenticate(MAIN_USER);

        Group groupOld = publicGroup("my-group", system);
        Group groupNew = groupService.create(groupOld).toGroup();

        AssertDeepEquals(groupNew, groupOld);
    }

    @Test
    @Rollback
    public void shouldNotAllowConcurrentChange() {
        authenticate(MAIN_USER);

        groupService.create(publicGroup("my-group", system));

        Condition<Groups> queryCondition = Groups.suchThat().name().eq("my-group");
        Group found = groupService.findAll(toRSQL(queryCondition)).iterator().next().toGroup();

        Group newGroup1 = found.toBuilder().name("my-group-2").build();
        Group newGroup2 = found.toBuilder().name("my-group-3").build();

        groupService.update(newGroup1);
        assertThrows(ObjectOptimisticLockingFailureException.class, () -> groupService.update(newGroup2));
    }

    @Test
    public void shouldCreateEmptyGroupWithAuthenticatedUserAsOwner() {
        authenticate(MAIN_USER);

        Group groupOld = publicGroup("my-group", system);
        Group groupNew = groupService.create(groupOld).toGroup();

        AssertDeepEquals(groupNew, groupOld);
        assertEquals(MAIN_USER, groupNew.getOwner());
    }

    @Test
    public void shouldCreateEmptyGroupWithSpecifiedOwner() {
        authenticate(MAIN_USER);

        Group groupToCreate = publicGroup("my-group", system).toBuilder().owner(OTHER_USER).build();
        Group createdGroup = groupService.create(groupToCreate).toGroup();

        AssertDeepEquals(createdGroup, groupToCreate);
        assertEquals(OTHER_USER, createdGroup.getOwner());
    }

    @Test
    public void shouldThrowOnCreateEmptyGroupWithoutAuthentication() {
        unauthenticate();

        Group groupOld = publicGroup("my-group", system);
        assertThrows(AccessDeniedRuntimeException.class, () -> groupService.create(groupOld).toGroup());
    }

    @Test
    public void shouldUpdateExistingOwnerWhenAnotherOneSpecifiedForPublicGroup() {
        authenticate(MAIN_USER);

        Group groupToCreate = publicGroup("my-group", system);
        Group createdGroup = groupService.create(groupToCreate).toGroup();

        assertEquals(MAIN_USER, createdGroup.getOwner());

        authenticate(OTHER_USER);

        //This is possible in the server's GroupService but forbidden in SecureGroupService (which handles client's GroupService calls).
        //I.e. in the API user cannot update the group that has a different owner.
        Group updatedGroup = groupService.update(createdGroup.toBuilder().owner(THIRD_USER).build()).toGroup();
        assertEquals(THIRD_USER, updatedGroup.getOwner());
    }

    @Test
    public void shouldNotUpdateExistingOwnerWhenNoOwnerSpecifiedForPublicGroup() {
        authenticate(MAIN_USER);

        Group groupToCreate = publicGroup("my-group", system);
        Group createdGroup = groupService.create(groupToCreate).toGroup();
        assertEquals(MAIN_USER, createdGroup.getOwner());

        authenticate(OTHER_USER);

        Group updatedGroup = groupService.update(createdGroup.toBuilder().owner(null).description("new_desc").build())
                .toGroup();
        assertEquals(MAIN_USER, updatedGroup.getOwner());
    }

    @Test
    public void shouldUpdateExistingOwnerWhenAnotherOneSpecifiedForProtectedGroup() {
        authenticate(MAIN_USER);

        Group groupToCreate = protectedGroup("my-group", system);
        Group createdGroup = groupService.create(groupToCreate).toGroup();

        assertEquals(MAIN_USER, createdGroup.getOwner());

        authenticate(OTHER_USER);

        Group updatedGroup = groupService.update(createdGroup.toBuilder().owner(THIRD_USER).build()).toGroup();
        assertEquals(THIRD_USER, updatedGroup.getOwner());
    }

    @Test
    public void shouldNotUpdateExistingOwnerWhenNoOwnerSpecifiedForProtectedGroup() {
        authenticate(MAIN_USER);

        Group groupToCreate = protectedGroup("my-group", system);
        Group createdGroup = groupService.create(groupToCreate).toGroup();

        assertEquals(MAIN_USER, createdGroup.getOwner());

        authenticate(OTHER_USER);

        Group updatedGroup = groupService.update(createdGroup.toBuilder().owner(null).description("new_desc").build())
                .toGroup();
        assertEquals(MAIN_USER, updatedGroup.getOwner());
    }

    @Test
    public void shouldNotAllowAbsentSystem() {
        authenticate(MAIN_USER);

        ReflectionUtils.enhanceWithId(system, Long.MAX_VALUE - 1);
        Group groupOld = publicGroup("my-group", system).toBuilder().build();
        assertThrows(NotFoundRuntimeException.class, () -> groupService.create(groupOld));
    }

    @Test
    public void shouldNotDeleteGroupIfItDoesNotExist() {
        long nonExistingGroupId = 1234L;
        assertThrows(NotFoundRuntimeException.class, () -> groupService.delete(nonExistingGroupId));
    }

    @Test
    public void shouldDeleteGroup() {
        authenticate(MAIN_USER);

        Group group = groupService.create(protectedGroup("my-group", system)).toGroup();

        groupService.delete(group.getId());
    }

    @Test
    public void shouldCreateGroupWithEntities() {
        authenticate(MAIN_USER);

        Entity entity = createEntity(system);

        GroupData groupNew = groupService.create(publicGroup("my-group", system));
        assertNotNull(groupNew);
        assertNotNull(groupNew.getId());

        Map<String, Set<Long>> associatedEntities = new HashMap<>();
        associatedEntities.put(null, Collections.singleton(entity.getId()));
        groupService.addEntities(groupNew.getId(), associatedEntities);
        Map<String, Set<EntityData>> fetchedAssociatedEntities = groupService.getEntities(groupNew.getId());

        assertEquals(entity, fetchedAssociatedEntities.get(null).iterator().next().toEntity());
        assertEquals(MAIN_USER, groupNew.getOwner());
    }

    @Test
    public void shouldNotAllowAbsentEntities() {
        authenticate(MAIN_USER);

        GroupData group = groupService.create(publicGroup("my-group", system));
        assertNotNull(group.getId());

        Map<String, Set<Long>> entities = new HashMap<>();
        entities.put("entities", Collections.singleton(Long.MAX_VALUE - 1));

        assertThrows(NotFoundRuntimeException.class, () -> groupService.setEntities(group.getId(), entities));
    }

    @Test
    public void shouldCreateGroupWithVariables() {
        authenticate(MAIN_USER);

        Group groupRequest = publicGroup("my-group", system);
        Group group = groupService.create(groupRequest).toGroup();
        AssertDeepEquals(groupRequest, group);

        Entity entity = createEntity(system);
        Variable variable = createVariable(system, entity, "my-variable", "my-field");
        Map<String, Set<Long>> variables = new HashMap<>();
        variables.put(null, Collections.singleton(variable.getId()));
        groupService.addVariables(group.getId(), variables);

        Map<String, Set<VariableData>> fetchedGroupVariables = groupService.getVariables(group.getId());

        assertEquals(variables.size(), fetchedGroupVariables.size());
        assertEquals(variable, fetchedGroupVariables.get(null).iterator().next().toVariable());
        assertEquals(MAIN_USER, group.getOwner());
    }

    @Test
    public void shouldAllowEditingVariables() {
        authenticate(MAIN_USER);

        Entity entity = createEntity(system);
        Variable variable1 = createVariable(system, entity, "my-variable-1", "my-field-A");
        Variable variable2 = createVariable(system, entity, "my-variable-2", "my-field-B");

        Group obtained = groupService.create(publicGroup("my-group", system)).toGroup();

        String associationKey = "variables";
        Map<String, Set<Long>> variables = new HashMap<>();

        variables.put(associationKey, new HashSet<>(Arrays.asList(variable1.getId(), variable2.getId())));
        groupService.setVariables(obtained.getId(), variables);

        Map<String, Set<Variable>> obtainedVariables = groupService.getVariables(obtained.getId()).entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().stream()
                        .map(VariableData::toVariable).collect(Collectors.toSet())));

        assertThat(obtainedVariables.get(associationKey)).containsExactly(variable1, variable2);

        variables.put(associationKey, new HashSet<>(Collections.singleton(variable2.getId())));
        groupService.setVariables(obtained.getId(), variables);
        obtainedVariables = groupService.getVariables(obtained.getId()).entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().stream()
                        .map(VariableData::toVariable).collect(Collectors.toSet())));

        assertThat(obtainedVariables.get(associationKey)).containsExactly(variable2);

        variables.put(associationKey, new HashSet<>(Collections.singleton(variable1.getId())));
        groupService.setVariables(obtained.getId(), variables);
        obtainedVariables = groupService.getVariables(obtained.getId()).entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().stream()
                        .map(VariableData::toVariable).collect(Collectors.toSet())));

        assertThat(obtainedVariables.get(associationKey)).containsExactly(variable1);
    }

    @Test
    public void shouldFindByLAbel() {
        authenticate(MAIN_USER);

        GroupData groupDataA = groupService.create(publicGroup("my-group-A", system, "my-label-A"));
        GroupData groupDataB = groupService.create(publicGroup("my-group-B", system, "my-label-A"));
        GroupData groupDataC = groupService.create(publicGroup("my-group-C", system, "my-label-B"));
        GroupData groupDataD = groupService.create(publicGroup("my-group-D", system, "my-label-B"));

        List<GroupData> labelA = groupService.findAll(toRSQL(Groups.suchThat().label().eq("my-label-A")));
        List<GroupData> labelB = groupService.findAll(toRSQL(Groups.suchThat().label().eq("my-label-B")));

        assertThat(labelA).containsExactlyInAnyOrder(groupDataA, groupDataB);
        assertThat(labelB).containsExactlyInAnyOrder(groupDataC, groupDataD);
    }

    @Test
    public void shouldNotAllowChangingType() {
        authenticate(MAIN_USER);

        Group groupOld = groupService.create(publicGroup("my-group", system)).toGroup();

        Group groupNew = ReflectionUtils.builderInstance(Group.InnerBuilder.class)
                .id(groupOld.getId())
                .recVersion(groupOld.getRecVersion())
                .name(groupOld.getName())
                .systemSpec(groupOld.getSystemSpec())
                .owner(groupOld.getOwner())
                .visibility(Visibility.PUBLIC)
                .label("my-snapshot-label")
                .build();

        assertThrows(IllegalStateException.class, () -> groupService.update(groupNew));
    }

    @Test
    public void shouldAllowManagingProperties() {
        authenticate(MAIN_USER);

        Group groupOld = Group.builder().label("my-snapshot-label").name("name").description("desc").systemSpec(system)
                .visibility(Visibility.PUBLIC)
                .property("key_1", "value_1")
                .property("key_2", "value_2")
                .property("key_3", null)
                .build();

        Group groupNew;
        Map<String, String> properties;

        groupNew = groupService.create(groupOld).toGroup();

        properties = new HashMap<>(groupNew.getProperties());
        assertTrue(properties.containsKey("key_1"));
        assertTrue(properties.containsKey("key_2"));
        assertTrue(properties.containsKey("key_3"));
        assertFalse(properties.containsKey("key_4"));

        properties.remove("key_2");
        properties.put("key_4", "value_4");

        groupNew = groupService.update(groupNew.toBuilder().clearProperties().properties(properties).build()).toGroup();

        properties = new HashMap<>(groupNew.getProperties());
        assertTrue(properties.containsKey("key_1"));
        assertFalse(properties.containsKey("key_2"));
        assertTrue(properties.containsKey("key_3"));
        assertTrue(properties.containsKey("key_4"));
    }

    @Test
    public void shouldAllowUpdate() {
        authenticate(MAIN_USER);

        Group obtained = groupService.create(publicGroup("my-group-1", system)).toGroup();

        assertTrue(exists("my-group-1"));
        assertFalse(exists("my-group-2"));

        groupService.update(obtained.toBuilder().name("my-group-2").build());

        assertFalse(exists("my-group-1"));
        assertTrue(exists("my-group-2"));
    }

    @Test
    public void shouldAllowUpdateMultipleTimes() {
        authenticate(MAIN_USER);

        Group created = groupService.create(publicGroup("my-group-1", system)).toGroup();

        assertTrue(exists("my-group-1"));
        assertFalse(exists("my-group-2"));

        Group updated = groupService.update(created.toBuilder().name("my-group-2").build()).toGroup();

        assertFalse(exists("my-group-1"));
        assertTrue(exists("my-group-2"));

        groupService.update(updated.toBuilder().name("my-group-3").build()).toGroup();

        assertFalse(exists("my-group-2"));
        assertTrue(exists("my-group-3"));
    }

    @Test
    public void shouldSeePrivateGroups() {
        authenticate(MAIN_USER);

        SystemSpecData systemSpecData = systemRepository.findById(system.getId()).orElse(null);

        GroupData groupA = groupDataWithUser("my-public-group", systemSpecData, MAIN_USER);
        groupA.setVisibility(VisibilityData.PUBLIC);
        GroupData groupB = groupDataWithUser("my-protected-group", systemSpecData, MAIN_USER);
        groupB.setVisibility(VisibilityData.PROTECTED);
        GroupData groupC = groupDataWithUser("my-private-group", systemSpecData, MAIN_USER);
        groupC.setVisibility(VisibilityData.PRIVATE);

        persistGroup(groupA);
        persistGroup(groupB);
        persistGroup(groupC);

        authenticate(MAIN_USER);
        assertTrue(exists("my-public-group"));
        assertTrue(exists("my-protected-group"));
        assertTrue(exists("my-private-group"));

        authenticate(OTHER_USER);
        assertTrue(exists("my-public-group"));
        assertTrue(exists("my-protected-group"));
        assertTrue(exists("my-private-group"));

        unauthenticate();
        assertTrue(exists("my-public-group"));
        assertTrue(exists("my-protected-group"));
        assertTrue(exists("my-private-group"));
    }

    @Test
    public void shouldNotUpdateNewGroup() {
        authenticate(MAIN_USER);

        Group groupOld = publicGroup("my-group", system);
        assertThrows(NotFoundRuntimeException.class, () -> groupService.update(groupOld));
    }

    @Test
    public void shouldNotCreateExistingGroup() {
        authenticate(MAIN_USER);

        Group groupOld = publicGroup("my-group", system);
        Group groupNew = groupService.create(groupOld).toGroup();
        assertThrows(ConfigDataConflictException.class, () -> groupService.create(groupNew));
    }

    @Test
    public void shouldNotCreateGroupWithTheSameName() {
        authenticate(MAIN_USER);

        Group groupOld = publicGroup("my-group", system);
        groupService.create(groupOld);
        assertThrows(ConfigDataConflictException.class, () -> groupService.create(groupOld));
    }

    @Test
    public void shouldNotAllowCreateOnUpdate() {
        authenticate(MAIN_USER);

        Group group = publicGroup("my-group", system);
        Group saved = groupService.create(group).toGroup();
        assertTrue(exists("my-group"));

        groupService.delete(saved.getId());
        assertFalse(exists("my-group"));

        assertThrows(NotFoundRuntimeException.class, () -> groupService.update(saved));
    }

    // test: getVariables

    @Test
    public void shouldNotAllowGetVariablesOnNonExistingGroup() {
        authenticate(MAIN_USER);

        assertThrows(NotFoundRuntimeException.class, () -> groupService.getVariables(Long.MIN_VALUE + 1));
    }

    @Test
    public void shouldGetEmptyVariablesIfNoAssociationsExist() {
        authenticate(MAIN_USER);

        Group groupRequest = publicGroup("my-group", system);
        Group group = groupService.create(groupRequest).toGroup();
        AssertDeepEquals(groupRequest, group);

        Map<String, Set<VariableData>> fetchedVariables = groupService.getVariables(group.getId());
        assertNotNull(fetchedVariables);
        assertTrue(fetchedVariables.isEmpty());
    }

    @Test
    public void shouldGetVariables() {
        authenticate(MAIN_USER);

        String association = "vars";
        Group groupRequest = publicGroup("my-group", system);
        Group group = groupService.create(groupRequest).toGroup();
        AssertDeepEquals(groupRequest, group);

        Variable variable = createVariable(system, createEntity(system), "my-variable-1", "my-field-A");
        Map<String, Set<Long>> variables = new HashMap<>();
        variables.put(association, Collections.singleton(variable.getId()));

        groupService.setVariables(group.getId(), variables);

        Map<String, Set<VariableData>> fetchedVariables = groupService.getVariables(group.getId());
        assertEquals(1, fetchedVariables.size());
        assertEquals(1, fetchedVariables.get(association).size());
        assertEquals(variable, fetchedVariables.get(association).iterator().next().toVariable());
    }

    // test: setVariables

    @Test
    public void shouldNotAllowSetVariablesOnNonExistingGroup() {
        authenticate(MAIN_USER);

        Variable variable = createVariable(system, createEntity(system), "my-variable-1", "my-field-A");
        Map<String, Set<Long>> variables = new HashMap<>();
        variables.put("vars", Collections.singleton(variable.getId()));

        assertThrows(NotFoundRuntimeException.class, () -> groupService.setVariables(Long.MIN_VALUE + 1, variables));
    }

    @Test
    public void shouldNotAllowSetVariablesWithAbsentVariables() {
        authenticate(MAIN_USER);

        Group groupRequest = publicGroup("my-group", system);
        Group group = groupService.create(groupRequest).toGroup();
        AssertDeepEquals(groupRequest, group);

        Map<String, Set<Long>> variables = new HashMap<>();
        variables.put("variables", Collections.singleton(Long.MAX_VALUE - 1));

        assertThrows(NotFoundRuntimeException.class, () -> groupService.setVariables(group.getId(), variables));
    }

    @Test
    public void shouldDoNothingWhenSetVariablesAssociationWithEmptySet() {
        authenticate(MAIN_USER);

        Group group = groupService.create(publicGroup("my-group", system)).toGroup();
        assertTrue(exists("my-group"));

        Map<String, Set<Long>> variables = new HashMap<>();
        variables.put("vars", Collections.emptySet());

        groupService.setVariables(group.getId(), variables);

        Map<String, Set<VariableData>> fetchedVariables = groupService.getVariables(group.getId());
        assertTrue(fetchedVariables.isEmpty());
    }

    @Test
    public void shouldAllowSetVariables() {
        authenticate(MAIN_USER);

        Group group = groupService.create(publicGroup("my-group", system)).toGroup();
        assertTrue(exists("my-group"));

        Variable variable = createVariable(system, createEntity(system), "my-variable-1", "my-field-A");
        Map<String, Set<Long>> variables = new HashMap<>();
        variables.put("vars", Collections.singleton(variable.getId()));

        groupService.setVariables(group.getId(), variables);

        Map<String, Set<VariableData>> fetchedVariables = groupService.getVariables(group.getId());
        assertEquals(1, fetchedVariables.get("vars").size());
        assertEquals(variable, fetchedVariables.get("vars").iterator().next().toVariable());
    }

    @Test
    public void shouldOverwriteExistingVariableAssociationsWithSetVariables() {
        authenticate(MAIN_USER);

        Group group = groupService.create(publicGroup("my-group", system)).toGroup();
        assertTrue(exists("my-group"));
        Entity entity = createEntity(system);

        Variable variable1 = createVariable(system, entity, "my-variable-1", "my-field-A");
        Variable variable2 = createVariable(system, entity, "my-variable-2", "my-field-B");
        Variable variable3 = createVariable(system, entity, "my-variable-3", "my-field-C");

        Map<String, Set<Long>> variables = new HashMap<>();
        variables.put("vars-should-overwrite-1", Collections.singleton(variable1.getId()));
        variables.put("vars-should-overwrite-2", Sets.newHashSet(variable2.getId(), variable3.getId()));

        groupService.setVariables(group.getId(), variables);

        Map<String, Set<Long>> fetchedVariables = toIdMap(groupService.getVariables(group.getId()));
        assertEquals(2, fetchedVariables.size());
        assertEquals(1, fetchedVariables.get("vars-should-overwrite-1").size());
        assertEquals(2, fetchedVariables.get("vars-should-overwrite-2").size());
        assertEquals(Collections.singleton(variable1.getId()), fetchedVariables.get("vars-should-overwrite-1"));
        assertEquals(Sets.newHashSet(variable2.getId(), variable3.getId()),
                fetchedVariables.get("vars-should-overwrite-2"));

        Variable newVariable = createVariable(system, entity, "my-new-variable", "my-field-A");
        Map<String, Set<Long>> variablesUpdate = new HashMap<>();
        variablesUpdate.put("vars-should-overwrite-2", Collections.singleton(newVariable.getId()));

        groupService.setVariables(group.getId(), variablesUpdate);

        Map<String, Set<VariableData>> fetchedUpdatedVariables = groupService.getVariables(group.getId());
        assertEquals(1, fetchedUpdatedVariables.size());
        assertNull(fetchedUpdatedVariables.get("vars-should-overwrite-1"));
        assertEquals(1, fetchedUpdatedVariables.get("vars-should-overwrite-2").size());
        assertEquals(newVariable,
                fetchedUpdatedVariables.get("vars-should-overwrite-2").iterator().next().toVariable());
    }

    // test: addVariables

    @Test
    public void shouldNotAllowAddVariablesOnNonExistingGroup() {
        authenticate(MAIN_USER);

        Variable variable = createVariable(system, createEntity(system), "my-variable-1", "my-field-A");
        Map<String, Set<Long>> variables = new HashMap<>();
        variables.put("vars", Collections.singleton(variable.getId()));

        assertThrows(NotFoundRuntimeException.class, () -> groupService.addVariables(Long.MIN_VALUE + 1, variables));
    }

    @Test
    public void shouldNotAllowAddVariablesWithAbsentVariables() {
        authenticate(MAIN_USER);

        Group groupRequest = publicGroup("my-group", system);
        Group group = groupService.create(groupRequest).toGroup();
        AssertDeepEquals(groupRequest, group);

        Map<String, Set<Long>> variables = new HashMap<>();
        variables.put("variables", Collections.singleton(Long.MAX_VALUE - 1));

        assertThrows(NotFoundRuntimeException.class, () -> groupService.addVariables(group.getId(), variables));
    }

    @Test
    public void shouldAllowAddVariablesCreateNewAssociation() {
        authenticate(MAIN_USER);
        String variablesAssociation = "vars";
        Group group = groupService.create(publicGroup("my-group", system)).toGroup();
        assertTrue(exists("my-group"));

        Variable variable = createVariable(system, createEntity(system), "my-variable-1", "my-field-A");
        Map<String, Set<Long>> variables = new HashMap<>();
        variables.put(variablesAssociation, Collections.singleton(variable.getId()));

        groupService.addVariables(group.getId(), variables);

        Map<String, Set<VariableData>> fetchedVariables = groupService.getVariables(group.getId());
        assertEquals(1, fetchedVariables.get(variablesAssociation).size());
        assertEquals(variable, fetchedVariables.get(variablesAssociation).iterator().next().toVariable());
    }

    @Test
    public void shouldAllowAddVariablesOnExistingAssociation() {
        authenticate(MAIN_USER);
        String variablesAssociation = "vars";
        Group group = groupService.create(publicGroup("my-group", system)).toGroup();
        assertTrue(exists("my-group"));

        Entity entity = createEntity(system);
        Variable variable = createVariable(system, entity, "my-variable-1", "my-field-A");
        Map<String, Set<Long>> variables = new HashMap<>();
        variables.put(variablesAssociation, Collections.singleton(variable.getId()));

        groupService.setVariables(group.getId(), variables);

        Map<String, Set<VariableData>> fetchedVariables = groupService.getVariables(group.getId());
        assertEquals(1, fetchedVariables.get(variablesAssociation).size());
        assertEquals(variable, fetchedVariables.get(variablesAssociation).iterator().next().toVariable());

        Variable extraVariable1 = createVariable(system, entity, "my-variable-extra-1", "my-field-B");
        Variable extraVariable2 = createVariable(system, entity, "my-variable-extra-2", "my-field-B");
        Map<String, Set<Long>> moreVariables = new HashMap<>();
        moreVariables.put(variablesAssociation, Sets.newHashSet(extraVariable1.getId(), extraVariable2.getId()));

        groupService.addVariables(group.getId(), moreVariables);

        Map<String, Set<Long>> fetchedUpdatedVariables = toIdMap(groupService.getVariables(group.getId()));
        assertEquals(3, fetchedUpdatedVariables.get(variablesAssociation).size());
        assertEquals(Sets.newHashSet(variable.getId(), extraVariable1.getId(), extraVariable2.getId()),
                fetchedUpdatedVariables.get(variablesAssociation));
    }

    @Test
    public void shouldAddVariablesOnExistingVariableAssociationWithoutAffectingOther() {
        authenticate(MAIN_USER);

        Group group = groupService.create(publicGroup("my-group", system)).toGroup();
        assertTrue(exists("my-group"));
        Entity entity = createEntity(system);
        Variable variable1 = createVariable(system, entity, "my-variable-1", "my-field-A");
        Variable variable2 = createVariable(system, entity, "my-variable-2", "my-field-B");
        Variable variable3 = createVariable(system, entity, "my-variable-3", "my-field-C");

        Map<String, Set<Long>> variables = new HashMap<>();
        variables.put("vars-should-not-be-affected", Collections.singleton(variable1.getId()));
        variables.put("vars-should-add-extra", Sets.newHashSet(variable2.getId(), variable3.getId()));

        groupService.setVariables(group.getId(), variables);

        Map<String, Set<Long>> fetchedVariables = toIdMap(groupService.getVariables(group.getId()));
        assertEquals(2, fetchedVariables.size());
        assertEquals(1, fetchedVariables.get("vars-should-not-be-affected").size());
        assertEquals(2, fetchedVariables.get("vars-should-add-extra").size());
        assertEquals(variables.get("vars-should-not-be-affected"), fetchedVariables.get("vars-should-not-be-affected"));
        assertEquals(variables.get("vars-should-add-extra"), fetchedVariables.get("vars-should-add-extra"));

        Variable newVariable = createVariable(system, entity, "my-new-variable", "my-field-A");
        Map<String, Set<Long>> variablesUpdate = new HashMap<>();
        variablesUpdate.put("vars-should-add-extra", Collections.singleton(newVariable.getId()));

        groupService.addVariables(group.getId(), variablesUpdate);

        Map<String, Set<Long>> fetchedUpdatedVariables = toIdMap(groupService.getVariables(group.getId()));
        assertEquals(2, fetchedUpdatedVariables.size());
        assertEquals(1, fetchedUpdatedVariables.get("vars-should-not-be-affected").size());
        assertEquals(3, fetchedUpdatedVariables.get("vars-should-add-extra").size());
        assertEquals(variable1.getId(),
                fetchedUpdatedVariables.get("vars-should-not-be-affected").iterator().next().longValue());
        assertEquals(Sets.newHashSet(variable2.getId(), variable3.getId(), newVariable.getId()),
                fetchedUpdatedVariables.get("vars-should-add-extra"));
    }

    // test: removeVariables

    @Test
    public void shouldNotAllowRemoveVariablesOnNonExistingGroup() {
        authenticate(MAIN_USER);

        Variable variable = createVariable(system, createEntity(system), "my-variable-1", "my-field-A");
        Map<String, Set<Long>> variables = new HashMap<>();
        variables.put("vars", Collections.singleton(variable.getId()));

        assertThrows(NotFoundRuntimeException.class, () -> groupService.removeVariables(Long.MIN_VALUE + 1, variables));
    }

    @Test
    public void shouldNotAllowRemoveVariablesWithAbsentVariables() {
        authenticate(MAIN_USER);

        Group groupRequest = publicGroup("my-group", system);
        Group group = groupService.create(groupRequest).toGroup();
        AssertDeepEquals(groupRequest, group);

        Map<String, Set<Long>> variables = new HashMap<>();
        variables.put("variables", Collections.singleton(Long.MAX_VALUE - 1));

        assertThrows(NotFoundRuntimeException.class, () -> groupService.removeVariables(group.getId(), variables));
    }

    @Test
    public void shouldDoNothingOnRemoveVariablesWhenAssociationDoesNotExist() {
        authenticate(MAIN_USER);
        String variablesAssociation = "vars";
        Group group = groupService.create(publicGroup("my-group", system)).toGroup();
        assertTrue(exists("my-group"));

        Variable variable = createVariable(system, createEntity(system), "my-variable-1", "my-field-A");
        Map<String, Set<Long>> variables = new HashMap<>();
        variables.put(variablesAssociation, Collections.singleton(variable.getId()));

        groupService.setVariables(group.getId(), variables);

        Map<String, Set<Long>> fetchedVariables = toIdMap(groupService.getVariables(group.getId()));
        assertEquals(1, fetchedVariables.get(variablesAssociation).size());
        assertEquals(variable.getId(), fetchedVariables.get(variablesAssociation).iterator().next().longValue());

        Map<String, Set<Long>> removeVariables = new HashMap<>();
        removeVariables.put("non-existing-association", Collections.singleton(variable.getId()));

        groupService.removeVariables(group.getId(), removeVariables);

        Map<String, Set<Long>> fetchedVariablesResult = toIdMap(groupService.getVariables(group.getId()));
        assertEquals(1, fetchedVariablesResult.get(variablesAssociation).size());
        assertEquals(variable.getId(), fetchedVariablesResult.get(variablesAssociation).iterator().next().longValue());
        assertNull(fetchedVariablesResult.get("non-existing-association"));

    }

    @Test
    public void shouldThrowExceptionOnRemoveVariablesWhenVariableDoesNotExist() {
        authenticate(MAIN_USER);
        String variablesAssociation = "vars";
        Group group = groupService.create(publicGroup("my-group", system)).toGroup();
        assertTrue(exists("my-group"));

        Variable variable = createVariable(system, createEntity(system), "my-variable-1", "my-field-A");
        Map<String, Set<Long>> variables = new HashMap<>();
        variables.put(variablesAssociation, Collections.singleton(variable.getId()));

        groupService.setVariables(group.getId(), variables);

        Map<String, Set<Long>> fetchedVariables = toIdMap(groupService.getVariables(group.getId()));
        assertEquals(1, fetchedVariables.get(variablesAssociation).size());
        assertEquals(variable.getId(), fetchedVariables.get(variablesAssociation).iterator().next().longValue());

        Map<String, Set<Long>> removeVariables = new HashMap<>();
        removeVariables.put(variablesAssociation, Collections.singleton(Long.MIN_VALUE + 1));

        assertThrows(NotFoundRuntimeException.class,
                () -> groupService.removeVariables(group.getId(), removeVariables));
    }

    @Test
    public void shouldAllowRemoveVariablesOnExistingAssociation() {
        authenticate(MAIN_USER);
        String variablesAssociation = "vars";

        Group group = groupService.create(publicGroup("my-group", system)).toGroup();
        assertTrue(exists("my-group"));

        Variable variable = createVariable(system, createEntity(system), "my-variable-1", "my-field-A");
        Map<String, Set<Long>> variables = new HashMap<>();
        variables.put(variablesAssociation, Collections.singleton(variable.getId()));

        groupService.setVariables(group.getId(), variables);

        Map<String, Set<VariableData>> fetchedVariables = groupService.getVariables(group.getId());
        assertEquals(1, fetchedVariables.get(variablesAssociation).size());
        assertEquals(variable, fetchedVariables.get(variablesAssociation).iterator().next().toVariable());

        Map<String, Set<Long>> removeVariables = new HashMap<>();
        removeVariables.put(variablesAssociation, Collections.singleton(variable.getId()));

        groupService.removeVariables(group.getId(), removeVariables);

        Map<String, Set<Long>> fetchedVariablesResult = toIdMap(groupService.getVariables(group.getId()));
        assertTrue(fetchedVariablesResult.isEmpty());
    }

    @Test
    public void shouldRemoveVariablesOnExistingVariableAssociationWithoutAffectingOther() {
        authenticate(MAIN_USER);

        Group group = groupService.create(publicGroup("my-group", system)).toGroup();
        assertTrue(exists("my-group"));
        Entity entity = createEntity(system);
        Variable variable1 = createVariable(system, entity, "my-variable-1", "my-field-A");
        Variable variable2 = createVariable(system, entity, "my-variable-2", "my-field-B");
        Variable variable3 = createVariable(system, entity, "my-variable-3", "my-field-C");

        Map<String, Set<Long>> variables = new HashMap<>();
        variables.put("vars-should-not-be-affected", Collections.singleton(variable1.getId()));
        variables.put("vars-should-remove", Sets.newHashSet(variable2.getId(), variable3.getId()));

        groupService.setVariables(group.getId(), variables);

        Map<String, Set<Long>> fetchedVariables = toIdMap(groupService.getVariables(group.getId()));
        assertEquals(2, fetchedVariables.size());
        assertEquals(1, fetchedVariables.get("vars-should-not-be-affected").size());
        assertEquals(2, fetchedVariables.get("vars-should-remove").size());
        assertEquals(variables.get("vars-should-not-be-affected"), fetchedVariables.get("vars-should-not-be-affected"));
        assertEquals(variables.get("vars-should-remove"), fetchedVariables.get("vars-should-remove"));

        Map<String, Set<Long>> removeVariables = new HashMap<>();
        removeVariables.put("vars-should-remove", Collections.singleton(variable2.getId()));

        groupService.removeVariables(group.getId(), removeVariables);

        Map<String, Set<Long>> fetchedVariablesResult = toIdMap(groupService.getVariables(group.getId()));
        assertEquals(2, fetchedVariablesResult.size());
        assertEquals(1, fetchedVariablesResult.get("vars-should-not-be-affected").size());
        assertEquals(1, fetchedVariablesResult.get("vars-should-remove").size());
        assertEquals(variable1.getId(),
                fetchedVariablesResult.get("vars-should-not-be-affected").iterator().next().longValue());
        assertEquals(variable3.getId(), fetchedVariablesResult.get("vars-should-remove").iterator().next().longValue());
    }

    // test: getEntities

    @Test
    public void shouldNotAllowGetEntitiesOnNonExistingGroup() {
        authenticate(MAIN_USER);

        assertThrows(NotFoundRuntimeException.class, () -> groupService.getEntities(Long.MIN_VALUE + 1));
    }

    @Test
    public void shouldGetEmptyEntitiesIfNoAssociationsExist() {
        authenticate(MAIN_USER);

        Group groupRequest = publicGroup("my-group", system);
        Group group = groupService.create(groupRequest).toGroup();
        AssertDeepEquals(groupRequest, group);

        Map<String, Set<EntityData>> fetchedEntities = groupService.getEntities(group.getId());
        assertNotNull(fetchedEntities);
        assertTrue(fetchedEntities.isEmpty());
    }

    @Test
    public void shouldGetEntities() {
        authenticate(MAIN_USER);

        Group groupRequest = publicGroup("my-group", system);
        Group group = groupService.create(groupRequest).toGroup();
        AssertDeepEquals(groupRequest, group);

        String association = "entities";

        Entity entity = createEntity(system);
        Map<String, Set<Long>> entities = new HashMap<>();
        entities.put(association, Collections.singleton(entity.getId()));

        groupService.setEntities(group.getId(), entities);

        Map<String, Set<EntityData>> fetchedEntities = groupService.getEntities(group.getId());
        assertEquals(1, fetchedEntities.size());
        assertEquals(1, fetchedEntities.get(association).size());
        assertEquals(entity, fetchedEntities.get(association).iterator().next().toEntity());
    }

    // test: setEntities

    @Test
    public void shouldNotAllowSetEntitiesOnNonExistingGroup() {
        authenticate(MAIN_USER);

        Entity entity = createEntity(system);
        Map<String, Set<Long>> entities = new HashMap<>();
        entities.put("entities", Collections.singleton(entity.getId()));

        assertThrows(NotFoundRuntimeException.class, () -> groupService.setEntities(Long.MIN_VALUE + 1, entities));
    }

    @Test
    public void shouldNotAllowSetEntitiesWithAbsentVariables() {
        authenticate(MAIN_USER);

        Group groupRequest = publicGroup("my-group", system);
        Group group = groupService.create(groupRequest).toGroup();
        AssertDeepEquals(groupRequest, group);

        Map<String, Set<Long>> entities = new HashMap<>();
        entities.put("entities", Collections.singleton(Long.MAX_VALUE - 1));

        assertThrows(NotFoundRuntimeException.class, () -> groupService.setEntities(group.getId(), entities));
    }

    @Test
    public void shouldDoNothingWhenSetEntitiesAssociationWithEmptySet() {
        authenticate(MAIN_USER);

        Group group = groupService.create(publicGroup("my-group", system)).toGroup();
        assertTrue(exists("my-group"));

        Map<String, Set<Long>> entities = new HashMap<>();
        entities.put("entities", Collections.emptySet());

        groupService.setEntities(group.getId(), entities);

        Map<String, Set<EntityData>> fetchedEntities = groupService.getEntities(group.getId());
        assertTrue(fetchedEntities.isEmpty());
    }

    @Test
    public void shouldAllowSetEntities() {
        authenticate(MAIN_USER);

        String association = "entities";
        Group group = groupService.create(publicGroup("my-group", system)).toGroup();
        assertTrue(exists("my-group"));

        Entity entity = createEntity(system);
        Map<String, Set<Long>> entities = new HashMap<>();
        entities.put(association, Collections.singleton(entity.getId()));

        groupService.setEntities(group.getId(), entities);

        Map<String, Set<EntityData>> fetchedEntities = groupService.getEntities(group.getId());
        assertEquals(1, fetchedEntities.get(association).size());
        assertEquals(entity, fetchedEntities.get(association).iterator().next().toEntity());
    }

    @Test
    public void shouldOverwriteExistingEntityAssociationsWithSetVariables() {
        authenticate(MAIN_USER);

        Group group = groupService.create(publicGroup("my-group", system)).toGroup();
        assertTrue(exists("my-group"));

        Entity entity1 = createEntity(system, "entity1");
        Entity entity2 = createEntity(system, "entity2");
        Entity entity3 = createEntity(system, "entity3");

        Map<String, Set<Long>> entities = new HashMap<>();
        entities.put("should-overwrite-1", Collections.singleton(entity1.getId()));
        entities.put("should-overwrite-2", Sets.newHashSet(entity2.getId(), entity3.getId()));

        groupService.setEntities(group.getId(), entities);

        Map<String, Set<Long>> fetchedEntities = toIdMap(groupService.getEntities(group.getId()));
        assertEquals(2, fetchedEntities.size());
        assertEquals(1, fetchedEntities.get("should-overwrite-1").size());
        assertEquals(2, fetchedEntities.get("should-overwrite-2").size());
        assertEquals(Collections.singleton(entity1.getId()), fetchedEntities.get("should-overwrite-1"));
        assertEquals(Sets.newHashSet(entity2.getId(), entity3.getId()),
                fetchedEntities.get("should-overwrite-2"));

        Entity newEntity = createEntity(system, "newEntity");
        Map<String, Set<Long>> entitiesUpdate = new HashMap<>();
        entitiesUpdate.put("should-overwrite-2", Collections.singleton(newEntity.getId()));

        groupService.setEntities(group.getId(), entitiesUpdate);

        Map<String, Set<EntityData>> fetchedUpdatedEntities = groupService.getEntities(group.getId());
        assertEquals(1, fetchedUpdatedEntities.size());
        assertNull(fetchedUpdatedEntities.get("should-overwrite-1"));
        assertEquals(1, fetchedUpdatedEntities.get("should-overwrite-2").size());
        assertEquals(newEntity, fetchedUpdatedEntities.get("should-overwrite-2").iterator().next().toEntity());
    }

    // test: addEntities

    @Test
    public void shouldNotAllowAddEntitiesOnNonExistingGroup() {
        authenticate(MAIN_USER);

        Entity entity = createEntity(system);
        Map<String, Set<Long>> entities = new HashMap<>();
        entities.put("vars", Collections.singleton(entity.getId()));

        assertThrows(NotFoundRuntimeException.class, () -> groupService.addEntities(Long.MIN_VALUE + 1, entities));
    }

    @Test
    public void shouldNotAllowAddEntitiesWithAbsentEntities() {
        authenticate(MAIN_USER);

        Group groupRequest = publicGroup("my-group", system);
        Group group = groupService.create(groupRequest).toGroup();
        AssertDeepEquals(groupRequest, group);

        Map<String, Set<Long>> entities = new HashMap<>();
        entities.put("entities", Collections.singleton(Long.MAX_VALUE - 1));

        assertThrows(NotFoundRuntimeException.class, () -> groupService.addEntities(group.getId(), entities));
    }

    @Test
    public void shouldAllowAddEntitiesCreateNewAssociation() {
        authenticate(MAIN_USER);
        String variablesAssociation = "vars";
        Group group = groupService.create(publicGroup("my-group", system)).toGroup();
        assertTrue(exists("my-group"));

        Entity entity = createEntity(system);
        Map<String, Set<Long>> entities = new HashMap<>();
        entities.put(variablesAssociation, Collections.singleton(entity.getId()));

        groupService.addEntities(group.getId(), entities);

        Map<String, Set<EntityData>> fetchedEntities = groupService.getEntities(group.getId());
        assertEquals(1, fetchedEntities.get(variablesAssociation).size());
        assertEquals(entity, fetchedEntities.get(variablesAssociation).iterator().next().toEntity());
    }

    @Test
    public void shouldAllowAddEntitiesOnExistingAssociation() {
        authenticate(MAIN_USER);
        String entitiesAssociation = "entities";
        Group group = groupService.create(publicGroup("my-group", system)).toGroup();
        assertTrue(exists("my-group"));

        Entity entity = createEntity(system);
        Map<String, Set<Long>> entities = new HashMap<>();
        entities.put(entitiesAssociation, Collections.singleton(entity.getId()));

        groupService.setEntities(group.getId(), entities);

        Map<String, Set<EntityData>> fetchedEntities = groupService.getEntities(group.getId());
        assertEquals(1, fetchedEntities.get(entitiesAssociation).size());
        assertEquals(entity, fetchedEntities.get(entitiesAssociation).iterator().next().toEntity());

        Entity extraEntity1 = createEntity(system, "entity-extra-1");
        Entity extraEntity2 = createEntity(system, "entity-extra-2");
        Map<String, Set<Long>> moreEntities = new HashMap<>();
        moreEntities.put(entitiesAssociation, Sets.newHashSet(extraEntity1.getId(), extraEntity2.getId()));

        groupService.addEntities(group.getId(), moreEntities);

        Map<String, Set<Long>> fetchedUpdatedEntities = toIdMap(groupService.getEntities(group.getId()));
        assertEquals(3, fetchedUpdatedEntities.get(entitiesAssociation).size());
        assertEquals(Sets.newHashSet(entity.getId(), extraEntity1.getId(), extraEntity2.getId()),
                fetchedUpdatedEntities.get(entitiesAssociation));
    }

    @Test
    public void shouldAddEntitiesOnExistingVariableAssociationWithoutAffectingOther() {
        authenticate(MAIN_USER);

        Group group = groupService.create(publicGroup("my-group", system)).toGroup();
        assertTrue(exists("my-group"));
        Entity entity1 = createEntity(system, "entity-1");
        Entity entity2 = createEntity(system, "entity-2");
        Entity entity3 = createEntity(system, "entity-3");

        Map<String, Set<Long>> entities = new HashMap<>();
        entities.put("should-not-be-affected", Collections.singleton(entity1.getId()));
        entities.put("should-add-extra", Sets.newHashSet(entity2.getId(), entity3.getId()));

        groupService.setEntities(group.getId(), entities);

        Map<String, Set<Long>> fetchedEntities = toIdMap(groupService.getEntities(group.getId()));
        assertEquals(2, fetchedEntities.size());
        assertEquals(1, fetchedEntities.get("should-not-be-affected").size());
        assertEquals(2, fetchedEntities.get("should-add-extra").size());
        assertEquals(entities.get("should-not-be-affected"), fetchedEntities.get("should-not-be-affected"));
        assertEquals(entities.get("should-add-extra"), fetchedEntities.get("should-add-extra"));

        Entity newEntity = createEntity(system, "entity-new");
        Map<String, Set<Long>> entitiesUpdate = new HashMap<>();
        entitiesUpdate.put("should-add-extra", Collections.singleton(newEntity.getId()));

        groupService.addEntities(group.getId(), entitiesUpdate);

        Map<String, Set<Long>> fetchedUpdatedEntities = toIdMap(groupService.getEntities(group.getId()));
        assertEquals(2, fetchedUpdatedEntities.size());
        assertEquals(1, fetchedUpdatedEntities.get("should-not-be-affected").size());
        assertEquals(3, fetchedUpdatedEntities.get("should-add-extra").size());
        assertEquals(entity1.getId(),
                fetchedUpdatedEntities.get("should-not-be-affected").iterator().next().longValue());
        assertEquals(Sets.newHashSet(entity2.getId(), entity3.getId(), newEntity.getId()),
                fetchedUpdatedEntities.get("should-add-extra"));
    }

    // test: removeEntities

    @Test
    public void shouldNotAllowRemoveEntitiesOnNonExistingGroup() {
        authenticate(MAIN_USER);

        Entity entity = createEntity(system);
        Map<String, Set<Long>> entities = new HashMap<>();
        entities.put("entities", Collections.singleton(entity.getId()));

        assertThrows(NotFoundRuntimeException.class, () -> groupService.removeEntities(Long.MIN_VALUE + 1, entities));
    }

    @Test
    public void shouldNotAllowRemoveEntitiesWithAbsentEntities() {
        authenticate(MAIN_USER);

        Group groupRequest = publicGroup("my-group", system);
        Group group = groupService.create(groupRequest).toGroup();
        AssertDeepEquals(groupRequest, group);

        Map<String, Set<Long>> entities = new HashMap<>();
        entities.put("entities", Collections.singleton(Long.MAX_VALUE - 1));

        assertThrows(NotFoundRuntimeException.class, () -> groupService.removeEntities(group.getId(), entities));
    }

    @Test
    public void shouldDoNothingOnRemoveEntitiesWhenAssociationDoesNotExist() {
        authenticate(MAIN_USER);
        String association = "entities";
        Group group = groupService.create(publicGroup("my-group", system)).toGroup();
        assertTrue(exists("my-group"));

        Entity entity = createEntity(system);
        Map<String, Set<Long>> entities = new HashMap<>();
        entities.put(association, Collections.singleton(entity.getId()));

        groupService.setEntities(group.getId(), entities);

        Map<String, Set<Long>> fetchedEntities = toIdMap(groupService.getEntities(group.getId()));
        assertEquals(1, fetchedEntities.get(association).size());
        assertEquals(entity.getId(), fetchedEntities.get(association).iterator().next().longValue());

        Map<String, Set<Long>> removeEntities = new HashMap<>();
        removeEntities.put("non-existing-association", Collections.singleton(entity.getId()));

        groupService.removeEntities(group.getId(), removeEntities);

        Map<String, Set<Long>> fetchedEntitiesResult = toIdMap(groupService.getEntities(group.getId()));
        assertEquals(1, fetchedEntitiesResult.get(association).size());
        assertEquals(entity.getId(), fetchedEntitiesResult.get(association).iterator().next().longValue());
        assertNull(fetchedEntitiesResult.get("non-existing-association"));

    }

    @Test
    public void shouldThrowExceptionOnRemoveEntitiesWhenVariableDoesNotExist() {
        authenticate(MAIN_USER);
        String association = "entities";
        Group group = groupService.create(publicGroup("my-group", system)).toGroup();
        assertTrue(exists("my-group"));

        Entity entity = createEntity(system);
        Map<String, Set<Long>> entities = new HashMap<>();
        entities.put(association, Collections.singleton(entity.getId()));

        groupService.setEntities(group.getId(), entities);

        Map<String, Set<Long>> fetchedEntities = toIdMap(groupService.getEntities(group.getId()));
        assertEquals(1, fetchedEntities.get(association).size());
        assertEquals(entity.getId(), fetchedEntities.get(association).iterator().next().longValue());

        Map<String, Set<Long>> removeEntities = new HashMap<>();
        removeEntities.put(association, Collections.singleton(Long.MIN_VALUE + 1));

        assertThrows(NotFoundRuntimeException.class, () -> groupService.removeEntities(group.getId(), removeEntities));
    }

    @Test
    public void shouldAllowRemoveEntitiesOnExistingAssociation() {
        authenticate(MAIN_USER);
        String association = "entities";

        Group group = groupService.create(publicGroup("my-group", system)).toGroup();
        assertTrue(exists("my-group"));

        Entity entity = createEntity(system);
        Map<String, Set<Long>> entities = new HashMap<>();
        entities.put(association, Collections.singleton(entity.getId()));

        groupService.setEntities(group.getId(), entities);

        Map<String, Set<EntityData>> fetchedEntities = groupService.getEntities(group.getId());
        assertEquals(1, fetchedEntities.get(association).size());
        assertEquals(entity, fetchedEntities.get(association).iterator().next().toEntity());

        Map<String, Set<Long>> removeEntities = new HashMap<>();
        removeEntities.put(association, Collections.singleton(entity.getId()));

        groupService.removeEntities(group.getId(), removeEntities);

        Map<String, Set<Long>> fetchedEntitiesResult = toIdMap(groupService.getEntities(group.getId()));
        assertTrue(fetchedEntitiesResult.isEmpty());
    }

    @Test
    public void shouldRemoveEntitiesOnExistingVariableAssociationWithoutAffectingOther() {
        authenticate(MAIN_USER);

        Group group = groupService.create(publicGroup("my-group", system)).toGroup();
        assertTrue(exists("my-group"));

        Entity entity1 = createEntity(system, "entity-1");
        Entity entity2 = createEntity(system, "entity-2");
        Entity entity3 = createEntity(system, "entity-3");
        Map<String, Set<Long>> entities = new HashMap<>();
        entities.put("should-not-be-affected", Collections.singleton(entity1.getId()));
        entities.put("should-remove", Sets.newHashSet(entity2.getId(), entity3.getId()));

        groupService.setEntities(group.getId(), entities);

        Map<String, Set<Long>> fetchedEntities = toIdMap(groupService.getEntities(group.getId()));
        assertEquals(2, fetchedEntities.size());
        assertEquals(1, fetchedEntities.get("should-not-be-affected").size());
        assertEquals(2, fetchedEntities.get("should-remove").size());
        assertEquals(entities.get("should-not-be-affected"), fetchedEntities.get("should-not-be-affected"));
        assertEquals(entities.get("should-remove"), fetchedEntities.get("should-remove"));

        Map<String, Set<Long>> removeEntities = new HashMap<>();
        removeEntities.put("should-remove", Collections.singleton(entity2.getId()));

        groupService.removeEntities(group.getId(), removeEntities);

        Map<String, Set<Long>> fetchedEntitiesResult = toIdMap(groupService.getEntities(group.getId()));
        assertEquals(2, fetchedEntitiesResult.size());
        assertEquals(1, fetchedEntitiesResult.get("should-not-be-affected").size());
        assertEquals(1, fetchedEntitiesResult.get("should-remove").size());
        assertEquals(entity1.getId(),
                fetchedEntitiesResult.get("should-not-be-affected").iterator().next().longValue());
        assertEquals(entity3.getId(), fetchedEntitiesResult.get("should-remove").iterator().next().longValue());
    }

    // Helper methods

    private boolean exists(String s) {
        return !groupService.findAll(toRSQL(Groups.suchThat().name().eq(s))).isEmpty();
    }

    private Entity createEntity(SystemSpec system) {
        return entityService.createEntity(system.getId(), ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1).toEntity();
    }

    private Entity createEntity(SystemSpec system, String name) {
        Map<String, Object> keyValues = ImmutableMap
                .of(ENTITY_STRING_SCHEMA_KEY_1, name, ENTITY_DOUBLE_SCHEMA_KEY_1, 1d);
        return entityService.createEntity(system.getId(), keyValues, PARTITION_KEY_VALUES_1).toEntity();
    }

    private Variable createVariable(SystemSpec system, Entity entity, String name, String field) {
        VariableConfig config = VariableConfig.builder().entityId(entity.getId()).fieldName(field).build();
        Variable variable = Variable.builder().variableName(name).configs(ImmutableSortedSet.of(config))
                .systemSpec(system).build();
        final VariableData variableData = variableService.create(variable);
        return variableData.toVariable();
    }

    private void AssertDeepEquals(Group groupNew, Group groupOld) {
        assertEquals(groupNew, groupOld);
        assertEquals(groupNew.getName(), groupOld.getName());
        assertEquals(groupNew.getDescription(), groupOld.getDescription());
        assertEquals(groupNew.isVisible(), groupOld.isVisible());
        assertEquals(groupNew.getSystemSpec(), groupOld.getSystemSpec());
        assertEquals(groupNew.getProperties(), groupOld.getProperties());
        assertEquals(groupNew.getLabel(), groupOld.getLabel());
    }

    private <T extends StandardPersistentEntity> Map<String, Set<Long>> toIdMap(
            Map<String, Set<T>> entityAssociations) {
        return entityAssociations.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey,
                e -> e.getValue().stream().map(StandardPersistentEntity::getId).collect(Collectors.toSet())));
    }
}
