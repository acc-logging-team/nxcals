package cern.nxcals.service.internal.extraction;

import cern.nxcals.common.domain.HBaseExtractionTask;
import cern.nxcals.common.domain.HdfsExtractionTask;
import cern.nxcals.service.internal.extraction.ExtractionTaskMaker.TaskInput;
import org.apache.avro.Schema;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.function.Function;

import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ExtractionTaskMakerTest {
    private PredicateProvider predicateProvider;
    private ExtractionTaskMaker instance;

    @BeforeEach
    void setUp() {
        this.predicateProvider = mock(PredicateProvider.class);
        this.instance = new ExtractionTaskMaker(predicateProvider);
    }

    @Test
    void shouldCreateEmptyTasksFromEmptyResources() {
        TaskInput input = mock(TaskInput.class);
        when(input.getResources()).thenReturn(emptyList());
        Optional<HBaseExtractionTask> hbase = this.instance.hbaseTask(input);
        Optional<HdfsExtractionTask> hdfs = this.instance.hdfsTask(input);
        assertFalse(hbase.isPresent());
        assertFalse(hdfs.isPresent());
    }

    @Test
    void shouldCreateHbaseTask() {

    }

    @Test
    void shouldCreateHdfsTask() {

    }


}
