package cern.nxcals.service.internal.metadata;

import cern.nxcals.api.metadata.queries.VariableQueryWithOptions;
import cern.nxcals.service.domain.VariableData;
import org.junit.jupiter.api.Test;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.UnaryOperator;

import static cern.nxcals.service.internal.metadata.QueryOptionsProcessors.createQueryProcessor;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class QueryOptionsProcessorsTest {

    @Test
    void createdQueryProcessorShouldApplySortingOptions() {
        // given
        VariableQueryWithOptions options = VariableQueryWithOptions.getDefaultInstance().orderBy().systemId().desc();
        BiFunction<CriteriaBuilder, CriteriaQuery<VariableData>, CriteriaQuery<VariableData>> processor = createQueryProcessor(
                options, VariableData.class);
        CriteriaBuilder builder = mock(CriteriaBuilder.class);
        CriteriaQuery criteriaQuery = mock(CriteriaQuery.class);
        Root root = mock(Root.class, RETURNS_DEEP_STUBS);
        Path field = mock(Path.class);
        Order order = mock(Order.class);
        when(root.getModel().getJavaType()).thenReturn(VariableData.class);
        when(root.get(anyString())).thenReturn(field);
        when(criteriaQuery.getRoots()).thenReturn(Set.of(root));
        when(criteriaQuery.orderBy(order)).thenReturn(criteriaQuery);
        when(builder.desc(field)).thenReturn(order);
        // when
        CriteriaQuery result = processor.apply(builder, criteriaQuery);
        // then
        assertAll(
                () -> assertEquals(criteriaQuery, result),
                () -> verify(criteriaQuery, times(1)).orderBy(order)
        );
    }

    @Test
    void createResultProcessor() {
        // given
        int offset = 20;
        int limit = 10;
        VariableQueryWithOptions options = VariableQueryWithOptions.getDefaultInstance().limit(limit).offset(offset);
        UnaryOperator<TypedQuery<VariableData>> resultProcessor = QueryOptionsProcessors.createResultProcessor(options);
        TypedQuery<VariableData> typedQuery = mock(TypedQuery.class);
        when(typedQuery.setFirstResult(anyInt())).thenReturn(typedQuery);
        when(typedQuery.setMaxResults(anyInt())).thenReturn(typedQuery);
        // when
        TypedQuery<VariableData> result = resultProcessor.apply(typedQuery);
        assertAll(
                () -> assertEquals(typedQuery, result),
                () -> verify(typedQuery, times(1)).setMaxResults(limit),
                () -> verify(typedQuery, times(1)).setFirstResult(offset)
        );
    }
}
