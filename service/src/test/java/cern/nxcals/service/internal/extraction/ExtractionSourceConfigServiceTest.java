package cern.nxcals.service.internal.extraction;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.common.domain.EntityKeyValue;
import cern.nxcals.common.domain.EntityKeyValues;
import cern.nxcals.common.domain.ExtractionCriteria;
import cern.nxcals.service.domain.EntityData;
import cern.nxcals.service.domain.SystemSpecData;
import cern.nxcals.service.domain.VariableConfigData;
import cern.nxcals.service.domain.VariableData;
import cern.nxcals.service.internal.EntityService;
import cern.nxcals.service.internal.VariableService;
import cern.nxcals.service.repository.SystemSpecRepository;
import cern.nxcals.service.rest.exceptions.NotFoundRuntimeException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anySet;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.contains;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ExtractionSourceConfigServiceTest {
    private static final String SYSTEM = "MONITORING";
    private static final String DEVICE = "device";
    private static final String DEVICE_NAME = "MONITORING_DEVICE";
    private static final EntityKeyValue keyValue = EntityKeyValue.builder()
            .key(DEVICE).value(DEVICE_NAME)
            .build();

    private static final String KEY_SCHEMA =
            "{\"type\":\"record\",\"name\":\"CmwKey\",\"namespace\":\"cern.nxcals\",\"fields\":" + "[{\"name\":\""
                    + DEVICE + "\",\"type\":\"string\"}]}";
    private static final long QUERY_START = 0;
    private static final long QUERY_END = 100;
    private static final TimeWindow timeWindow = TimeWindow.between(QUERY_START, QUERY_END);
    @Mock
    private SystemSpecRepository systemRepository;
    @Mock
    private EntityService entityService;
    @Mock
    private VariableService variableService;

    private ExtractionSourceConfigService sourceConfigProvider;

    @BeforeEach
    void createProvider() {
        sourceConfigProvider = new ExtractionSourceConfigService(entityService, variableService, systemRepository);
    }

    @Test
    void shouldThrowExceptionWhenSystemWasNotFound() {
        // given
        ExtractionCriteria criteria = ExtractionCriteria.builder()
                .systemName(SYSTEM)
                .entities(List.of(EntityKeyValues.builder().keyValue(keyValue).build()))
                .variables(Map.of())
                .timeWindow(timeWindow)
                .build();

        // when
        assertThrows(IllegalStateException.class, () -> sourceConfigProvider.getEntitiesFor(criteria));
    }

    @Test
    void shouldThrowExceptionWhenEntityWasNotFound() {
        // given
        ExtractionCriteria criteria = ExtractionCriteria.builder()
                .systemName(SYSTEM)
                .entities(List.of(EntityKeyValues.builder().keyValue(keyValue).build()))
                .variables(Map.of())
                .timeWindow(timeWindow)
                .build();

        initSystemRepository();

        // when
        assertThrows(IllegalArgumentException.class, () -> sourceConfigProvider.getEntitiesFor(criteria));
    }

    @Test
    void shouldReturnConfigForEntityQueryByKeyValues() {
        // given
        ExtractionCriteria criteria = ExtractionCriteria.builder()
                .systemName(SYSTEM)
                .entities(List.of(EntityKeyValues.builder().keyValue(keyValue).build()))
                .variables(Map.of())
                .timeWindow(timeWindow)
                .build();

        initSystemRepository();
        initEntityServiceForKeyValueQuery();

        // when
        Collection<Entity> configs = sourceConfigProvider.getEntitiesFor(criteria);

        // then
        assertEquals(1, configs.size());
    }

    private void initSystemRepository() {
        SystemSpecData systemSpecData = mock(SystemSpecData.class);
        SystemSpec systemSpec = mock(SystemSpec.class);
        lenient().when(systemSpecData.toSystemSpec()).thenReturn(systemSpec);
        lenient().when(systemSpecData.getName()).thenReturn(SYSTEM);
        lenient().when(systemRepository.findByName(SYSTEM)).thenReturn(Optional.of(systemSpecData));
        lenient().when(systemSpec.getEntityKeyDefinitions()).thenReturn(KEY_SCHEMA);
    }

    private void initEntityServiceForKeyValueQuery() {
        EntityData entityData = mock(EntityData.class);
        Entity entity = mock(Entity.class);
        when(entityData.toEntity()).thenReturn(entity);
        when(entityService.findAll(anyString(), eq(QUERY_START), eq(QUERY_END))).thenReturn(
                List.of(entityData));
        when(entity.getEntityKeyValues()).thenReturn(Map.of(DEVICE, DEVICE_NAME));
    }

    @Test
    void shouldReturnConfigForEntityQueryByKeyValuesPattern() {
        // given
        ExtractionCriteria criteria = ExtractionCriteria.builder()
                .systemName(SYSTEM)
                .entities(List.of(EntityKeyValues.builder().keyValue(keyValue.toBuilder().wildcard(true).build())
                        .build()))
                .variables(Map.of())
                .timeWindow(timeWindow)
                .build();

        initSystemRepository();
        initEntityServiceForKeyValueQueryLike();

        // when
        Collection<Entity> configs = sourceConfigProvider.getEntitiesFor(criteria);

        // then
        assertEquals(1, configs.size());
    }

    private void initEntityServiceForKeyValueQueryLike() {
        EntityData entityData = mock(EntityData.class);
        Entity entity = mock(Entity.class);
        when(entityData.toEntity()).thenReturn(entity);
        when(entityService.findAll(contains("like"), eq(QUERY_START), eq(QUERY_END))).thenReturn(
                List.of(entityData));
        when(entity.getEntityKeyValues()).thenReturn(Map.of(DEVICE, DEVICE_NAME));
    }

    @Test
    void shouldReturnConfigForEntityQueryById() {
        // given
        long entityId = 100;
        ExtractionCriteria criteria = ExtractionCriteria.builder()
                .systemName(SYSTEM)
                .entities(List.of())
                .entityId(entityId)
                .variables(Map.of())
                .timeWindow(timeWindow)
                .build();

        initSystemRepository();
        initEntityServiceForIdQuery(entityId);

        // when
        Collection<Entity> configs = sourceConfigProvider.getEntitiesFor(criteria);

        // then
        assertEquals(1, configs.size());
    }

    private void initEntityServiceForIdQuery(long entityId) {
        EntityData entityData = mock(EntityData.class);
        Entity entity = mock(Entity.class);
        when(entityData.toEntity()).thenReturn(entity);
        when(entityService.findAll(anyString(), eq(QUERY_START), eq(QUERY_END))).thenReturn(List.of(entityData));
        lenient().when(entity.getId()).thenReturn(entityId);
        lenient().when(entityData.getId()).thenReturn(entityId);
    }

    @Test
    void shouldThrowWhenVariableWasNotFoundById() {
        // given
        long variableId = 100;
        ExtractionCriteria criteria = ExtractionCriteria.builder()
                .systemName(SYSTEM)
                .entities(List.of())
                .variableId(variableId)
                .variables(Map.of())
                .timeWindow(timeWindow)
                .build();

        initSystemRepository();
        when(variableService.findExactlyByIdIn(anySet())).thenThrow(new NotFoundRuntimeException("Not found"));

        // when
        assertThrows(IllegalArgumentException.class, () -> sourceConfigProvider.getVariableConfigsFor(criteria));
    }

    @Test
    void shouldReturnConfigForVariableQueryById() {
        // given
        long variableId = 100;
        ExtractionCriteria criteria = ExtractionCriteria.builder()
                .systemName(SYSTEM)
                .entities(List.of())
                .variableId(variableId)
                .variables(Map.of())
                .timeWindow(timeWindow)
                .build();

        initSystemRepository();
        initVariableServiceForIdQuery();

        // when
        Collection<VariableSourceConfig> configs = sourceConfigProvider.getVariableConfigsFor(criteria);

        // then
        assertEquals(1, configs.size());
    }

    private void initVariableServiceForIdQuery() {
        long entityId = 10;
        VariableData variableData = mock(VariableData.class);
        VariableConfigData configData = mock(VariableConfigData.class, RETURNS_DEEP_STUBS);
        when(configData.validity()).thenReturn(timeWindow);
        when(configData.getEntity().getId()).thenReturn(entityId);
        initEntityServiceForIdQuery(entityId);
        HashSet<VariableConfigData> configs = new HashSet<>();
        configs.add(configData);
        when(variableData.getVariableConfigs()).thenReturn(configs);

        when(variableService.findExactlyByIdIn(anySet())).thenReturn(Set.of(variableData));
    }

    @Test
    void shouldThrowWhenVariableWasNotFoundByName() {
        // given
        ExtractionCriteria criteria = ExtractionCriteria.builder()
                .systemName(SYSTEM)
                .entities(List.of())
                .variables(Map.of("NAME", false))
                .timeWindow(timeWindow)
                .build();

        initSystemRepository();

        // when
        assertThrows(IllegalArgumentException.class, () -> sourceConfigProvider.getVariableConfigsFor(criteria));
    }

    @Test
    void shouldReturnConfigForVariableQueryByName() {
        // given
        ExtractionCriteria criteria = ExtractionCriteria.builder()
                .systemName(SYSTEM)
                .entities(List.of())
                .variables(Map.of("NAME", false))
                .timeWindow(timeWindow)
                .build();

        initSystemRepository();
        initVariableServiceForNameQuery();

        // when
        Collection<VariableSourceConfig> configs = sourceConfigProvider.getVariableConfigsFor(criteria);

        // then
        assertEquals(1, configs.size());
    }

    private void initVariableServiceForNameQuery() {
        long entityId = 10;
        VariableData variableData = mock(VariableData.class);
        VariableConfigData configData = mock(VariableConfigData.class, RETURNS_DEEP_STUBS);
        when(configData.validity()).thenReturn(timeWindow);
        when(configData.getEntity().getId()).thenReturn(entityId);
        initEntityServiceForIdQuery(entityId);
        HashSet<VariableConfigData> configs = new HashSet<>();
        configs.add(configData);
        when(variableData.getVariableConfigs()).thenReturn(configs);

        when(variableService.findBySystemIdAndVariableNameIn(anyLong(), anySet())).thenReturn(Set.of(variableData));
    }

    @Test
    void shouldReturnConfigForVariableQueryByNamePattern() {
        // given
        ExtractionCriteria criteria = ExtractionCriteria.builder()
                .systemName(SYSTEM)
                .entities(List.of())
                .variables(Map.of("NAME", true))
                .timeWindow(timeWindow)
                .build();

        initSystemRepository();
        initVariableServiceForNamePatternQuery();

        // when
        Collection<VariableSourceConfig> configs = sourceConfigProvider.getVariableConfigsFor(criteria);

        // then
        assertEquals(1, configs.size());
    }

    private void initVariableServiceForNamePatternQuery() {
        long entityId = 10;
        VariableData variableData = mock(VariableData.class);
        VariableConfigData configData = mock(VariableConfigData.class, RETURNS_DEEP_STUBS);
        when(configData.validity()).thenReturn(timeWindow);
        when(configData.getEntity().getId()).thenReturn(entityId);
        initEntityServiceForIdQuery(entityId);
        HashSet<VariableConfigData> configs = new HashSet<>();
        configs.add(configData);
        when(variableData.getVariableConfigs()).thenReturn(configs);

        when(variableService.findBySystemIdAndVariableNameLike(anyLong(), anyString())).thenReturn(
                Set.of(variableData));
    }
}
