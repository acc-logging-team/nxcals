package cern.nxcals.service.internal.security;

import cern.accsoft.ccs.ccda.client.rbac.User;
import cern.accsoft.ccs.ccda.client.rbac.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class RbaUserServiceTest {
    private UserService mockService;
    private RbaUserService instance;

    @BeforeEach
    public void setUp() {
        this.mockService = mock(UserService.class);
        this.instance = new RbaUserService(mockService, Duration.ofHours(1L).toString());
    }

    @Test
    void shouldGetFromCache() {
        String username = UUID.randomUUID().toString();
        User user = User.builder().name(username).build();
        when(mockService.findByUserName(username)).thenReturn(user);

        getAndVerify(user);
        getAndVerify(user);
        getAndVerify(user);

        verify(mockService, times(1)).findByUserName(username);
    }

    private void getAndVerify(User user) {
        Optional<User> found = this.instance.findBy(user.getName());
        assertNotNull(found);
        assertTrue(found.isPresent());
        assertEquals(user, found.get());
    }
}
