package cern.nxcals.service.internal.compaction;

import cern.nxcals.common.domain.RestageJob;
import cern.nxcals.common.paths.StagingPath;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.Set;

import static cern.nxcals.common.utils.HdfsFileUtils.SearchType.NONRECURSIVE;
import static cern.nxcals.service.internal.compaction.DataProcessingJobCreator.sizeOf;
import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class RestageJobCreatorTest extends CompactionTestUtils {
    private static final long TOTAL_FILE_SIZE = 1024 * 1024 * 40;
    private static final int SORT_ABOVE = 1024 * 1024;
    private static final int AVRO_PARQUET_RATIO = 1;

    private static final StagingPath STAGING_PATH = new StagingPath(new Path("y/1/2/3/2008-01-10"));
    private static final Path DATA_PATH = new Path("x/1/2/3/2008/1/10");

    @Mock
    private FileSystem fs;

    private RestageJobCreator creator;

    @BeforeEach
    public void init() {
        creator = new RestageJobCreator(SORT_ABOVE, AVRO_PARQUET_RATIO, "x", "y", fs);
    }

    @Nested
    class CreateJobTests {
        @BeforeEach
        void init() throws Exception {
            Path outputPath = new Path(creator.getHdfsPathCreator().toOutputUri(STAGING_PATH));
            when(fs.exists(outputPath)).thenReturn(true);
            Collection<LocatedFileStatus> parquetFiles = generateParquetFiles(4);
            when(fs.listFiles(any(), eq(NONRECURSIVE.getValue()))).thenReturn(
                    new RemoteIteratorAdapter<>(parquetFiles));
        }

        @Test
        void shouldCreateJobWithCorrectNumberOfDataFiles() {
            // given
            long fileSize = TOTAL_FILE_SIZE / 1024;
            Set<FileStatus> files = generateMixedFiles(fileSize, 0, 100);
            String prefix = "prefix";

            // when
            long jobSize = sizeOf(files);
            RestageJob job = creator.createJob(STAGING_PATH, files, jobSize, jobSize, prefix);

            // then
            assertAll(
                    () -> assertEquals(4, job.getDataFiles().size()),
                    () -> assertEquals(prefix, job.getFilePrefix()),
                    () -> assertEquals(1, job.getSystemId())
            );
        }

        @Test
        void shouldCreateOneJobForLargeFilesBucketed() throws IOException {
            long fileSize = TOTAL_FILE_SIZE + 1;
            Set<FileStatus> files = generateMixedFiles(fileSize, 1000, 0);
            initFsContainsReportFile(false);

            Optional<RestageJob> data = creator.create(STAGING_PATH, files);
            assertTrue( data.isPresent());
            assertEquals(4, data.get().getDataFiles().size());

        }
    }

    void initFsContainsReportFile(boolean contains) throws IOException {
        Path reportFile = new Path(creator.getHdfsPathCreator().toRestageReportFileUri(STAGING_PATH));
        when(fs.exists(reportFile)).thenReturn(contains);
    }

    @Nested
    class PathCheckerTests {
        @Test
        void shouldCheckIfReportFileExistAndAcceptIfNot() throws Exception {
            initFsContainsReportFile(false);

            assertTrue(creator.getPathChecker().test(STAGING_PATH));
        }

        @Test
        void shouldCheckIfReportFileExistAndRejectIfYes() throws Exception {
            initFsContainsReportFile(true);

            assertFalse(creator.getPathChecker().test(STAGING_PATH));
        }
    }

    @Test
    public void shouldCreateJobWithEmptyDataFilesWhenOutputDirDoesNotExists() throws Exception {
        Set<FileStatus> files = generateMixedFiles(100, 50, 50);

        // when
        Path reportFile = new Path(creator.getHdfsPathCreator().toRestageReportFileUri(STAGING_PATH));
        when(fs.exists(reportFile)).thenReturn(false);
        Path outputPath = new Path(creator.getHdfsPathCreator().toOutputUri(STAGING_PATH));
        when(fs.exists(outputPath)).thenReturn(false);

        Optional<RestageJob> data = creator.create(STAGING_PATH, files);
        assertTrue(data.isPresent());
        assertTrue(data.get().getDataFiles().isEmpty());
    }

    private Collection<LocatedFileStatus> generateParquetFiles(int size) {
        Collection<LocatedFileStatus> result = new HashSet<>();
        Random r = new Random(System.currentTimeMillis());
        for (int i = 0; i < size; i++) {
            LocatedFileStatus file = mock(LocatedFileStatus.class);
            when(file.isFile()).thenReturn(true);
            when(file.getPath()).thenReturn(new Path(DATA_PATH + format("/file%d.parquet", r.nextInt(24), i)));
            result.add(file);
        }
        return result;
    }

    private class RemoteIteratorAdapter<E> implements RemoteIterator<E> {
        private final Collection<E> collection;

        private Iterator<E> internal;

        private RemoteIteratorAdapter(Collection<E> collection) {
            this.collection = Objects.requireNonNull(collection);
            this.internal = collection.iterator();
        }

        @Override
        public boolean hasNext() {
            boolean hasNext = internal.hasNext();
            if (!hasNext) {
                this.reset();
            }
            return hasNext;
        }

        @Override
        public E next() {
            return internal.next();
        }

        private void reset() {
            this.internal = collection.iterator();
        }
    }

}
