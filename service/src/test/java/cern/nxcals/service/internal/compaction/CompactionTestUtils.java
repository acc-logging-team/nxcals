package cern.nxcals.service.internal.compaction;

import cern.nxcals.common.paths.StagingPath;
import com.google.common.collect.Sets;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.Path;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CompactionTestUtils {
    protected static final long MAX_PARTITION_SIZE = AdaptiveCompactionJobCreator.ONE_GB_FILE_SIZE;
    protected static final int SORT_ABOVE = 1024 * 1024;
    protected static final int AVRO_PARQUET_RATIO = 1;

    protected static final StagingPath STAGING_PATH = new StagingPath(new Path("/1/2/3/2008-01-10"));

    Sets.SetView<FileStatus> generateMixedFiles(long fileSize, int bucketedCount, int unbucketedCount) {
        return Sets.union(generateBucketedFiles(bucketedCount, fileSize),
                generateUnbucketedFiles(unbucketedCount, fileSize));
    }

    Set<FileStatus> generateBucketedFiles(int numberOfFiles, long fileSize, String bucket) {
        Set<FileStatus> result = new HashSet<>();

        for (int i = 0; i < numberOfFiles; i++) {
            FileStatus file = mock(FileStatus.class);
            when(file.getPath()).thenReturn(new Path(String.format("ANYTHING/%s/file%d.avro", bucket, i)));
            when(file.getLen()).thenReturn(fileSize);
            result.add(file);
        }

        return result;
    }

    Set<FileStatus> generateBucketedFiles(int size, long fileSize) {
        Set<FileStatus> result = new HashSet<>();

        Random r = new Random(System.currentTimeMillis());
        for (int i = 0; i < size; i++) {
            FileStatus file = mock(FileStatus.class);
            when(file.getPath()).thenReturn(new Path(String.format("ANYTHING/%02d/file%d.avro", r.nextInt(24), i)));
            when(file.getLen()).thenReturn(fileSize);
            result.add(file);
        }

        return result;
    }

    Set<FileStatus> generateUnbucketedFiles(int size, long fileSize) {
        Set<FileStatus> result = new HashSet<>();

        for (int i = 0; i < size; i++) {
            FileStatus file = mock(FileStatus.class);
            when(file.getPath()).thenReturn(new Path(String.format("ANYTHING/file%d.avro", i)));
            when(file.getLen()).thenReturn(fileSize);
            result.add(file);
        }

        return result;
    }
}
