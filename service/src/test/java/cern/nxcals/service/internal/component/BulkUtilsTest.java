package cern.nxcals.service.internal.component;

import com.google.common.collect.Sets;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class BulkUtilsTest {
    private final Function<Collection<String>, Collection<String>> mockOperation = mock(Function.class);

    @BeforeEach
    public void setUp() throws Exception {
        reset(
                mockOperation
        );
    }

    @AfterEach
    public void tearDown() throws Exception {
        verifyNoMoreInteractions(
                mockOperation
        );
    }

    @Test
    public void shouldThrowExceptionWhenReceivesNullInput() {
        Set<String> inputElements = null;

        assertThrows(NullPointerException.class, () -> BulkUtils.batchAndApply(inputElements, mockOperation));
    }

    @Test
    public void shouldThrowExceptionWhenReceivesNullOperation() {
        Set<String> inputElements = Sets.newLinkedHashSet(Arrays.asList("Test", "Testy", "Testify"));

        assertThrows(NullPointerException.class, () -> BulkUtils.batchAndApply(inputElements, null));
    }

    @Test
    public void shouldPropagateExceptionWhenOperationThrowsOne() {
        Set<String> inputElements = Sets.newLinkedHashSet(Arrays.asList("Test", "Testy", "Testify"));
        when(mockOperation.apply(inputElements)).thenThrow(new IllegalStateException("Testing is for the weak!!"));

        assertThrows(IllegalStateException.class, () -> BulkUtils.batchAndApply(inputElements, mockOperation));
        verify(mockOperation).apply(inputElements);
    }

    @Test
    public void shouldThrowExceptionWhenOperationResultIsNull() {
        Set<String> inputElements = Sets.newLinkedHashSet(Arrays.asList("Test", "Testy", "Testify"));
        when(mockOperation.apply(inputElements)).thenReturn(null);

        assertThrows(IllegalStateException.class, () -> BulkUtils.batchAndApply(inputElements, mockOperation));
        verify(mockOperation).apply(inputElements);
    }

    @Test
    public void shouldContinueWhenBatchOperationResultIsEmpty() {
        Set<String> inputElements = Sets.newLinkedHashSet(Arrays.asList("Test", "Testy", "Testify"));

        when(mockOperation.apply(inputElements)).thenReturn(Collections.emptySet());

        Collection<String> result = BulkUtils.batchAndApply(inputElements, mockOperation);

        assertNotNull(result);

        verify(mockOperation).apply(inputElements);
    }

    @Test
    public void shouldSplitIn2ChunksAndApplyBulkAction() {
        Set<String> elements = Sets.newLinkedHashSet(Arrays.asList("Test", "Testy", "Testify"));

        when(mockOperation.apply(any())).thenAnswer(invocation -> {
            Set<String> invocationElemets = (Set<String>) invocation.getArguments()[0];
            String result = null;
            if (invocationElemets.size() == 2) {
                result = "Duper";
            } else {
                result = "Super";
            }
            return Collections.singleton(result);
        });


        Collection<String> actionResult = BulkUtils.batchAndApply(elements, mockOperation, 2);

        assertNotNull(actionResult);
        assertEquals(2, actionResult.size());
        assertTrue(actionResult.contains("Super"));
        assertTrue(actionResult.contains("Duper"));

        verify(mockOperation, times(2)).apply(any());
    }

}
