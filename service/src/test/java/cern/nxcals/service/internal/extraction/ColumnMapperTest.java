package cern.nxcals.service.internal.extraction;

import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.common.Schemas;
import cern.nxcals.common.domain.ColumnMapping;
import cern.nxcals.common.domain.EntityExtractionResource;
import cern.nxcals.common.domain.ExtractionCriteria;
import cern.nxcals.common.domain.VariableExtractionResource;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.apache.avro.Schema;
import org.apache.avro.Schema.Field;
import org.apache.avro.SchemaBuilder;
import org.apache.avro.SchemaBuilder.FieldAssembler;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;
import java.util.function.Function;

import static cern.nxcals.common.Schemas.PARTITION_ID;
import static cern.nxcals.common.Schemas.SCHEMA_ID;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VALUE;
import static cern.nxcals.service.internal.extraction.ColumnMapper.ENTITY_ID;
import static cern.nxcals.service.internal.extraction.ColumnMapper.TIMESTAMP;
import static cern.nxcals.service.internal.extraction.ColumnMapper.VALUE;
import static cern.nxcals.service.internal.extraction.ColumnMapper.VARIABLE_NAME;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ColumnMapperTest {

    private Function<String, Field> timestampFieldProvider;
    private Function<String, List<Field>> systemFieldsProvider;
    private ColumnMapper instance;

    @BeforeEach
    void setUp() {
        this.timestampFieldProvider = mock(TimestampProvider.class);
        this.systemFieldsProvider = mock(SystemFieldsProvider.class);
        this.instance = new ColumnMapper(timestampFieldProvider, systemFieldsProvider);
    }

    @Test
    void shouldGetEntityGlobalMappings() {
        EntityExtractionResource r1 = mock(EntityExtractionResource.class, RETURNS_DEEP_STUBS);
        EntityExtractionResource r2 = mock(EntityExtractionResource.class, RETURNS_DEEP_STUBS);
        List<Pair<String, String>> nameAndAlias = ImmutableList.of(Pair.of("a1", "a"), Pair.of("b2", "b"));

        when(r1.getEntityHistory().getEntitySchema().getSchema()).thenReturn(
                getRecordSchemaFor(nameAndAlias.get(0).getKey(), Schema.Type.STRING));
        when(r2.getEntityHistory().getEntitySchema().getSchema()).thenReturn(
                getRecordSchemaFor(nameAndAlias.get(1).getKey(), Schema.Type.STRING));

        ExtractionCriteria criteria = mock(ExtractionCriteria.class);
        when(criteria.isVariableSearch()).thenReturn(false);
        // @formatter:off
        when(criteria.getAliasFields()).thenReturn(
                ImmutableMap.of(nameAndAlias.get(0).getValue(), singletonList(nameAndAlias.get(0).getKey()),
                                nameAndAlias.get(1).getValue(), singletonList(nameAndAlias.get(1).getKey())));
        // @formatter:on

        List<ColumnMapping> mappings = this.instance.getEntityMappings(ImmutableList.of(r1, r2), criteria);
        assertEquals(2, mappings.size());
        for (Pair<String, String> pair : nameAndAlias) {
            assertTrue(mappings.stream()
                    .anyMatch(cm -> pair.getKey().equals(cm.getFieldName()) && pair.getValue().equals(cm.getAlias())));
        }
        for (ColumnMapping mapping : mappings) {
            assertEquals(Schema.Type.STRING, mapping.getSchema().get().getType());
        }
    }

    @Test
    void shouldGetVariableGlobalMappings() {
        String fieldName1 = "a";
        String fieldName2 = "b";
        ImmutableList<VariableExtractionResource> resources = getVariableResources(fieldName1, fieldName2);

        ExtractionCriteria criteria = mock(ExtractionCriteria.class);
        when(criteria.isVariableSearch()).thenReturn(true);

        List<ColumnMapping> mappings = this.instance.getVariableMappings(resources, criteria);
        assertEquals(2, mappings.size());

        for (String fieldName : asList(fieldName1, fieldName2)) {
            assertTrue(mappings.stream().anyMatch(
                    cm -> fieldName.equals(cm.getFieldName()) && NXC_EXTR_VALUE.getValue().equals(cm.getAlias())));
        }
    }

    @Test
    void shouldThrowWhenQueryingForFieldBoundAndEntityBoundVariablesTogether() {
        ImmutableList<VariableExtractionResource> resources = getVariableResources("a", null);

        ExtractionCriteria criteria = mock(ExtractionCriteria.class);
        when(criteria.isVariableSearch()).thenReturn(true);

        assertThrows(IllegalArgumentException.class, () -> {
            this.instance.getVariableMappings(resources, criteria);
        });

    }

    @Test
    void shouldGetEmptyDatasetMappingsForVariables() {
        String systemName = "CMW";
        ExtractionCriteria criteria = mock(ExtractionCriteria.class);
        when(criteria.isVariableSearch()).thenReturn(true);
        when(criteria.getSystemName()).thenReturn(systemName);
        when(this.timestampFieldProvider.apply(systemName)).thenReturn(
                new Field(Schemas.TIMESTAMP.getFieldName(), Schemas.TIMESTAMP.getSchema(), null, (Object) null));

        List<ColumnMapping> mappings = this.instance.enhanceEmptyVariableDatasetMappings(List.of(), Set.of(systemName),
                true);
        assertEquals(4, mappings.size());

        for (String aliasName : asList(VARIABLE_NAME, VALUE, ENTITY_ID, TIMESTAMP)) {
            assertTrue(mappings.stream().anyMatch(cm -> aliasName.equals(cm.getAlias())));
        }
    }

    @Test
    void shouldGetEmptyDatasetMappingsForEntities() {
        String systemName = "CMW";
        ExtractionCriteria criteria = mock(ExtractionCriteria.class);
        when(criteria.isVariableSearch()).thenReturn(false);
        when(criteria.getSystemName()).thenReturn(systemName);

        when(this.timestampFieldProvider.apply(systemName)).thenReturn(
                new Field(Schemas.TIMESTAMP.getFieldName(), Schemas.TIMESTAMP.getSchema(), null, (Object) null));
        when(this.systemFieldsProvider.apply(systemName)).thenReturn(
                singletonList(new Field(PARTITION_ID.getFieldName(), SCHEMA_ID.getSchema(), null, (Object) null)));

        List<ColumnMapping> mappings = this.instance.enhanceWithCommonEntityMappings(List.of(), Set.of(systemName));
        assertEquals(3, mappings.size());

        for (String aliasName : asList(Schemas.TIMESTAMP.getFieldName(), PARTITION_ID.getFieldName(), ENTITY_ID)) {
            assertTrue(mappings.stream().anyMatch(cm -> aliasName.equals(cm.getAlias())));
        }

    }

    private ImmutableList<VariableExtractionResource> getVariableResources(String fieldName1, String fieldName2) {
        return ImmutableList.of(mockVariableResource(fieldName1), mockVariableResource(fieldName2));
    }

    private VariableExtractionResource mockVariableResource(String fieldName) {
        TimeWindow timeWindow = TimeWindow.infinite();
        VariableExtractionResource r = mock(VariableExtractionResource.class, RETURNS_DEEP_STUBS);
        VariableConfig vc1 = mock(VariableConfig.class);
        when(vc1.getFieldName()).thenReturn(fieldName);
        when(r.getVariableConfig()).thenReturn(vc1);
        when(r.getTimeWindow()).thenReturn(timeWindow);
        when(r.isFieldBound()).thenReturn(fieldName != null);
        Schema schemaMock = mock(Schema.class);
        when(r.getEntityHistory().getEntitySchema().getSchema()).thenReturn(schemaMock);
        Field field = mock(Field.class);
        when(field.name()).thenReturn(fieldName);
        when(field.schema()).thenReturn(mock(Schema.class));
        when(schemaMock.getField(fieldName)).thenReturn(field);
        return r;
    }

    private Schema getRecordSchemaFor(String fieldName, Schema.Type type) {
        FieldAssembler<Schema> fa = SchemaBuilder.builder().record("record").namespace("namespace").fields();
        fa.name(fieldName).type(Schema.create(type)).noDefault();
        return fa.endRecord();
    }

    private interface TimestampProvider extends Function<String, Field> {
    }

    private interface SystemFieldsProvider extends Function<String, List<Field>> {
    }
}
