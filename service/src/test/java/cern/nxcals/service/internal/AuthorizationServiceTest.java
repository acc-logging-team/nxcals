/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.internal;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

import org.apache.hadoop.security.Credentials;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.google.common.collect.ImmutableMap;

import cern.nxcals.api.custom.domain.DelegationToken;

public class AuthorizationServiceTest {

    private AuthorizationService service;

    @BeforeEach
    public void before() {
        service = new AuthorizationService(ImmutableMap.of(//
                "YARN", new AuthorizationService.YarnTokenProvider(), //
                "HDFS", new AuthorizationService.HdfsTokenProvider()
        // Not testing HBaseTokenProvider, it requires an running HBase service
        ));
    }

    @Test
    public void shouldCreateDelegationToken() throws IOException {
        DelegationToken token = service.createDelegationToken("myusername");
        byte[] tokenStorage = token.getStorage();

        assertThat(tokenStorage.length).isGreaterThan(0);

        Credentials credentials = new Credentials();
        credentials.readTokenStorageStream(new DataInputStream(new ByteArrayInputStream(tokenStorage)));
    }

}
