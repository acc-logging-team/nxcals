package cern.nxcals.service.internal;

import cern.nxcals.api.domain.EntitySchema;
import cern.nxcals.api.domain.Partition;
import cern.nxcals.api.domain.PartitionResource;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.service.BaseTest;
import cern.nxcals.service.domain.EntityData;
import cern.nxcals.service.domain.EntityHistoryData;
import cern.nxcals.service.domain.PartitionResourceData;
import cern.nxcals.service.domain.SystemSpecData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static cern.nxcals.service.rest.TestSchemas.ENTITY_SCHEMA_2;
import static cern.nxcals.service.rest.TestSchemas.PARTITION_SCHEMA_2;
import static cern.nxcals.service.rest.TestSchemas.TIME_SCHEMA;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Transactional(transactionManager = "jpaTransactionManager")
@Rollback
class PartitionResourceServiceTest extends BaseTest {

    @Autowired
    private PartitionResourceService service;

    @BeforeEach
    void setUp() {
        setAuthentication();
    }

    @Test
    void shouldCreateNewPartitionResource() {
        //given
        EntityHistoryData entityHistory = createAndPersistDefaultTestEntityKeyHist();
        EntityData entity = entityRepository.findById(entityHistory.getEntity().getId()).get();
        SystemSpec systemSpec = entity.getPartition().getSystem().toSystemSpec();
        EntitySchema schema = entity.getEntityHistories().first().getSchema().toEntitySchema();
        Partition partition = entity.getPartition().toPartition();
        PartitionResource partitionResource = createPartitionResource(systemSpec, partition, schema);
        //when
        PartitionResourceData newPartitionResource = service.create(partitionResource);

        //then
        assertNotNull(newPartitionResource);
        assertEquals(newPartitionResource.getSystem().getId(), partitionResource.getSystemSpec().getId());
        assertEquals(newPartitionResource.getPartition().getId(), partitionResource.getPartition().getId());
        assertEquals(newPartitionResource.getSchema().getId(), partitionResource.getSchema().getId());
    }

    @Test
    void shouldUpdatePartitionResource() {
        //given
        EntityHistoryData entityHistory = createAndPersistDefaultTestEntityKeyHist();
        EntityData entity = entityRepository.findById(entityHistory.getEntity().getId()).get();
        SystemSpec systemSpec = entity.getPartition().getSystem().toSystemSpec();
        EntitySchema schema = entity.getEntityHistories().first().getSchema().toEntitySchema();
        Partition partition = entity.getPartition().toPartition();
        PartitionResource partitionResource = createPartitionResource(systemSpec, partition, schema);
        //when
        PartitionResourceData newPartitionResource = service.create(partitionResource);

        SystemSpecData newSystem = createAndPersistSystemData("my-new-system", ENTITY_SCHEMA_2, PARTITION_SCHEMA_2, TIME_SCHEMA);
        entityManager.flush();
        newPartitionResource.setSystem(newSystem);

        PartitionResourceData updatedPartitionResource = service.update(newPartitionResource.toPartitionResource());

        //then
        assertNotNull(updatedPartitionResource);
        assertEquals(updatedPartitionResource.getSystem().getId(), newSystem.getId());
        assertEquals(updatedPartitionResource.getPartition().getId(), partitionResource.getPartition().getId());
        assertEquals(updatedPartitionResource.getSchema().getId(), partitionResource.getSchema().getId());
    }

    @Test
    void shouldUpdateMultipleTimesPartitionResource() {
        //given
        EntityHistoryData entityHistory = createAndPersistDefaultTestEntityKeyHist();
        EntityData entity = entityRepository.findById(entityHistory.getEntity().getId()).get();
        SystemSpec systemSpec = entity.getPartition().getSystem().toSystemSpec();
        EntitySchema schema = entity.getEntityHistories().first().getSchema().toEntitySchema();
        Partition partition = entity.getPartition().toPartition();
        PartitionResource partitionResource = createPartitionResource(systemSpec, partition, schema);

        PartitionResourceData newPartitionResource = service.create(partitionResource);
        SystemSpecData oldSystem = newPartitionResource.getSystem();
        SystemSpecData newSystem = createAndPersistSystemData("my-new-system", ENTITY_SCHEMA_2, PARTITION_SCHEMA_2, TIME_SCHEMA);
        entityManager.flush();
        newPartitionResource.setSystem(newSystem);

        PartitionResourceData updatedPartitionResource = service.update(newPartitionResource.toPartitionResource());

        assertNotNull(updatedPartitionResource);
        assertEquals(updatedPartitionResource.getSystem().getId(), newSystem.getId());
        assertEquals(updatedPartitionResource.getPartition().getId(), partitionResource.getPartition().getId());
        assertEquals(updatedPartitionResource.getSchema().getId(), partitionResource.getSchema().getId());

        updatedPartitionResource.setSystem(oldSystem);
        PartitionResourceData newlyUpdatedPartitionResource = service.update(updatedPartitionResource.toPartitionResource());

        assertNotNull(newlyUpdatedPartitionResource);
        assertEquals(newlyUpdatedPartitionResource.getSystem().getId(), oldSystem.getId());
        assertEquals(newlyUpdatedPartitionResource.getPartition().getId(), partitionResource.getPartition().getId());
        assertEquals(newlyUpdatedPartitionResource.getSchema().getId(), partitionResource.getSchema().getId());
    }

    @Test
    void shouldFindOnlyExistentPartitionResource() {
        //given
        EntityHistoryData entityHistory = createAndPersistDefaultTestEntityKeyHist();
        EntityData entity = entityRepository.findById(entityHistory.getEntity().getId()).get();
        SystemSpec systemSpec = entity.getPartition().getSystem().toSystemSpec();
        EntitySchema schema = entity.getEntityHistories().first().getSchema().toEntitySchema();
        Partition partition = entity.getPartition().toPartition();
        PartitionResource partitionResource = createPartitionResource(systemSpec, partition, schema);
        //when
        PartitionResourceData createdPartitionResource = service.create(partitionResource);

        Optional<PartitionResourceData> newPartitionResource = service.findById(createdPartitionResource.getId());
        Optional<PartitionResourceData> inexistentPartitionResource = service.findById(createdPartitionResource.getId() + 1);

        //then
        assertFalse(inexistentPartitionResource.isPresent());
        assertNotNull(newPartitionResource.get());
        assertEquals(newPartitionResource.get().getSystem().getId(), partitionResource.getSystemSpec().getId());
        assertEquals(newPartitionResource.get().getPartition().getId(), partitionResource.getPartition().getId());
        assertEquals(newPartitionResource.get().getSchema().getId(), partitionResource.getSchema().getId());
    }

    @Test
    void shouldFindBySystemIdAndPartitionIdAndSchemaId() {
        //given
        EntityHistoryData entityHistory = createAndPersistDefaultTestEntityKeyHist();
        EntityData entity = entityRepository.findById(entityHistory.getEntity().getId()).get();
        SystemSpec systemSpec = entity.getPartition().getSystem().toSystemSpec();
        EntitySchema schema = entity.getEntityHistories().first().getSchema().toEntitySchema();
        Partition partition = entity.getPartition().toPartition();
        PartitionResource partitionResource = createPartitionResource(systemSpec, partition, schema);
        PartitionResourceData createdPartitionResource = service.create(partitionResource);

        //when
        PartitionResourceData foundPartitionResource = service
                .findBySystemIdAndPartitionIdAndSchemaId(createdPartitionResource.getSystem().getId(),
                        createdPartitionResource.getPartition().getId(),
                        createdPartitionResource.getSchema().getId()).get();

        //then
        assertNotNull(foundPartitionResource);
        assertEquals(foundPartitionResource.getSystem().getId(), partitionResource.getSystemSpec().getId());
        assertEquals(foundPartitionResource.getPartition().getId(), partitionResource.getPartition().getId());
        assertEquals(foundPartitionResource.getSchema().getId(), partitionResource.getSchema().getId());
    }

    @Test
    void shouldFailIfSystemIsNotSet() {
        EntityHistoryData entityHistory = createAndPersistDefaultTestEntityKeyHist();
        EntityData entity = entityRepository.findById(entityHistory.getEntity().getId()).get();
        EntitySchema schema = entity.getEntityHistories().first().getSchema().toEntitySchema();
        Partition partition = entity.getPartition().toPartition();
        assertThrows(NullPointerException.class, () -> createPartitionResource(null, partition, schema));
    }

    @Test
    void shouldFailIfPartitionIsNotSet() {
        EntityHistoryData entityHistory = createAndPersistDefaultTestEntityKeyHist();
        EntityData entity = entityRepository.findById(entityHistory.getEntity().getId()).get();
        SystemSpec systemSpec = entity.getPartition().getSystem().toSystemSpec();
        EntitySchema schema = entity.getEntityHistories().first().getSchema().toEntitySchema();
        assertThrows(NullPointerException.class, () -> createPartitionResource(systemSpec, null, schema));
    }

    @Test
    void shouldFailIfSchemaIsNotSet() {
        EntityHistoryData entityHistory = createAndPersistDefaultTestEntityKeyHist();
        EntityData entity = entityRepository.findById(entityHistory.getEntity().getId()).get();
        SystemSpec systemSpec = entity.getPartition().getSystem().toSystemSpec();
        Partition partition = entity.getPartition().toPartition();
        assertThrows(NullPointerException.class, () -> createPartitionResource(systemSpec, partition, null));
    }

    @Test
    void shouldDeletePartitionResource() {
        //given
        EntityHistoryData entityHistory = createAndPersistDefaultTestEntityKeyHist();
        EntityData entity = entityRepository.findById(entityHistory.getEntity().getId()).get();
        SystemSpec systemSpec = entity.getPartition().getSystem().toSystemSpec();
        EntitySchema schema = entity.getEntityHistories().first().getSchema().toEntitySchema();
        Partition partition = entity.getPartition().toPartition();
        PartitionResource partitionResource = createPartitionResource(systemSpec, partition, schema);

        PartitionResourceData newPartitionResource = service.create(partitionResource);
        long partitionResourceId = newPartitionResource.getId();

        Optional<PartitionResourceData> justCreatedPartitionResource = service.findById(partitionResourceId);
        assertTrue(justCreatedPartitionResource.isPresent());

        //when
        service.delete(partitionResourceId);
        Optional<PartitionResourceData> deletedPartitionResource = service.findById(partitionResourceId);

        //then
        assertFalse(deletedPartitionResource.isPresent());
    }

    @Test
    void shouldFindOrCreatePartitionResourceDataNew() {
        //given
        EntityHistoryData entityHistory = createAndPersistDefaultTestEntityKeyHist();
        EntityData entity = entityRepository.findById(entityHistory.getEntity().getId()).get();
        SystemSpec systemSpec = entity.getPartition().getSystem().toSystemSpec();
        EntitySchema schema = entity.getEntityHistories().first().getSchema().toEntitySchema();
        Partition partition = entity.getPartition().toPartition();

        //when
        PartitionResourceData data = service.findOrCreatePartitionResourceData(systemSpec.getId(), partition.getId(), schema.getId());
        //then
        assertEquals(partition, data.getPartition().toPartition());
        assertEquals(systemSpec, data.getSystem().toSystemSpec());
        assertEquals(schema, data.getSchema().toEntitySchema());
    }

    @Test
    void shouldFindOrCreatePartitionResourceDataExisting() {
        //given
        EntityHistoryData entityHistory = createAndPersistDefaultTestEntityKeyHist();
        EntityData entity = entityRepository.findById(entityHistory.getEntity().getId()).get();
        SystemSpec systemSpec = entity.getPartition().getSystem().toSystemSpec();
        EntitySchema schema = entity.getEntityHistories().first().getSchema().toEntitySchema();
        Partition partition = entity.getPartition().toPartition();
        PartitionResource partitionResource = createPartitionResource(systemSpec, partition, schema);

        //when
        PartitionResourceData data = service
                .findOrCreatePartitionResourceData(systemSpec.getId(), partition.getId(), schema.getId());
        //then
        assertEquals(partitionResource.getPartition(), data.getPartition().toPartition());
        assertEquals(partitionResource.getSystemSpec(), data.getSystem().toSystemSpec());
        assertEquals(partitionResource.getSchema(), data.getSchema().toEntitySchema());
    }

    @Test
    void shouldFindOrCreatePartitionResourceDataTwice() {
        //given
        EntityHistoryData entityHistory = createAndPersistDefaultTestEntityKeyHist();
        EntityData entity = entityRepository.findById(entityHistory.getEntity().getId()).get();
        SystemSpec systemSpec = entity.getPartition().getSystem().toSystemSpec();
        EntitySchema schema = entity.getEntityHistories().first().getSchema().toEntitySchema();
        Partition partition = entity.getPartition().toPartition();

        //when
        PartitionResourceData data = service
                .findOrCreatePartitionResourceData(systemSpec.getId(), partition.getId(), schema.getId());
        PartitionResourceData data2 = service
                .findOrCreatePartitionResourceData(systemSpec.getId(), partition.getId(), schema.getId());
        //then
        assertEquals(data2.getId(), data.getId());

    }
}
