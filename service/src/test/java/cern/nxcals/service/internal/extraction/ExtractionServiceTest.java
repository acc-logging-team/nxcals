package cern.nxcals.service.internal.extraction;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.EntityHistory;
import cern.nxcals.api.domain.EntitySchema;
import cern.nxcals.api.domain.Partition;
import cern.nxcals.api.domain.PartitionProperties;
import cern.nxcals.api.domain.Resource;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.common.domain.ColumnMapping;
import cern.nxcals.common.domain.EntityExtractionResource;
import cern.nxcals.common.domain.EntityKeyValue;
import cern.nxcals.common.domain.EntityKeyValues;
import cern.nxcals.common.domain.ExtractionCriteria;
import cern.nxcals.common.domain.ExtractionUnit;
import cern.nxcals.common.domain.VariableExtractionResource;
import cern.nxcals.service.internal.EntityService;
import cern.nxcals.service.internal.ExtractionResourceService;
import cern.nxcals.service.internal.VariableService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatcher;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anySet;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class ExtractionServiceTest {
    private static final String SCHEMA = "{\"type\":\"record\",\"name\":\"device\",\"fields\":[{\"name\":\"device\",\"type\":\"string\"}, {\"name\":\"property\",\"type\":\"string\"}, {\"name\":\"value\",\"type\":\"string\"}]}";
    private static final Map<String, Object> keyValues = Map.of("device", "test_device");
    static final String SYSTEM = "SYSTEM";
    private static final SystemSpec systemSpec = mock(SystemSpec.class);
    private static final EntityService entityService = mock(EntityService.class);
    private static final VariableService variableService = mock(VariableService.class);

    @BeforeAll
    static void initSystemMock() {
        when(systemSpec.getName()).thenReturn(SYSTEM);
    }

    @Test
    void shouldReturnEmptyExtractionUnitForEmptyExtractionCriteria() {
        // given
        ExtractionResourceService resourceService = mock(ExtractionResourceService.class);
        ExtractionTaskMaker taskMaker = mock(ExtractionTaskMaker.class);
        ColumnMapper columnMapper = mock(ColumnMapper.class);

        ExtractionService service = new ExtractionService(resourceService, entityService, variableService, columnMapper,
                taskMaker);

        ExtractionCriteria criteria = extractionCriteria(TimeWindow.after(0), Map.of(), List.of());

        // when
        ExtractionUnit extractionUnit = service.findExtractionUnitForEntities(criteria);

        // then
        assertFalse(extractionUnit.isVariableSearch());
        assertEquals(List.of(), extractionUnit.getTasks());
        assertEquals(criteria, extractionUnit.getCriteria());
    }

    @Test
    void shouldReturnExtractionUnitForExtractionCriteriaWithVariable() {
        // given
        ExtractionResourceService resourceService = mock(ExtractionResourceService.class);
        TimeWindow timeWindow = TimeWindow.infinite();
        VariableConfig variableConfig = VariableConfig.builder()
                .variableName("VAR")
                .entityId(0L)
                .fieldName("FIELD")
                .validity(timeWindow)
                .build();
        Entity entity = createEntityMock(0L);
        EntityHistory entityHistory = createEntityHistory(timeWindow, entity, false);
        VariableExtractionResource resource = createVariableResource(timeWindow, entityHistory, variableConfig);
        when(resourceService.findVariableResourcesBy(any())).thenReturn(List.of(resource));

        ExtractionTaskMaker taskMaker = mock(ExtractionTaskMaker.class);
        ColumnMapper columnMapper = mock(ColumnMapper.class);
        ColumnMapping columnMapping = ColumnMapping.builder().schemaJson("[]").fieldName("device").alias("value")
                .build();
        when(columnMapper.getVariableMappings(any(), any())).thenReturn(List.of(columnMapping));

        ExtractionService service = new ExtractionService(resourceService, entityService, variableService, columnMapper,
                taskMaker);

        ExtractionCriteria criteria = extractionCriteria(timeWindow, Map.of("VAR", false), List.of());
        // when
        ExtractionUnit extractionUnit = service.findExtractionUnitForVariables(criteria);

        // then
        assertAll(
                () -> verify(taskMaker).hdfsTask(any()),
                () -> verify(taskMaker).hbaseTask(any()),
                () -> verify(columnMapper).getVariableMappings(any(), any()),
                () -> assertTrue(extractionUnit.isVariableSearch()),
                () -> assertEquals(List.of(), extractionUnit.getTasks()),
                () -> assertEquals(criteria, extractionUnit.getCriteria())
        );
    }

    @Nested
    @DisplayName("ExtractionService entity extraction tests")
    class EntityExtractionTests {
        String EMPTY_SCHEMA = "[]";
        ColumnMapping columnMappingDevice = ColumnMapping.builder().schemaJson(EMPTY_SCHEMA).fieldName("device")
                .build();
        ColumnMapping columnMappingProperty = ColumnMapping.builder().schemaJson(EMPTY_SCHEMA).fieldName("property")
                .build();
        ColumnMapping columnMappingValue = ColumnMapping.builder().schemaJson(EMPTY_SCHEMA).fieldName("value").build();
        List<ColumnMapping> globalColumnMappings = List.of(columnMappingDevice, columnMappingProperty,
                columnMappingValue);

        Entity dummyEntity = createEntityMock(0L);
        TimeWindow infiniteTimeWindow = TimeWindow.infinite();

        @Test
        void shouldReturnExtractionUnitForExtractionCriteriaWithEntity() {
            // given
            ExtractionResourceService resourceService = mock(ExtractionResourceService.class);
            EntityHistory entityHistory = createEntityHistory(infiniteTimeWindow, dummyEntity, false);
            EntityExtractionResource resource = createEntityResource(infiniteTimeWindow, entityHistory);
            when(resourceService.findEntityResourcesBy(any())).thenReturn(List.of(resource));

            ExtractionTaskMaker taskMaker = mock(ExtractionTaskMaker.class);
            AtomicBoolean enhanceWithCommonMappingsWasInvoked = new AtomicBoolean(false);
            ColumnMapper columnMapper = mock(ColumnMapper.class,
                    invocation -> { // done in this way, because normal verify(columnMapper).enhanceWithCommonMappings(argThat(checkColumnMappings), any(), any()) doesn't work
                        String methodName = invocation.getMethod().getName();
                        if (methodName.equals("enhanceWithCommonEntityMappings")) {
                            enhanceWithCommonMappingsWasInvoked.set(true);
                            return invocation.getArgument(0);
                        }
                        return null;
                    });
            when(columnMapper.getEntityMappings(any(), any())).thenReturn(globalColumnMappings);
            when(columnMapper.enhanceWithCommonEntityMappings(any(), anySet())).thenReturn(globalColumnMappings);

            ExtractionService service = new ExtractionService(resourceService, entityService, variableService,
                    columnMapper, taskMaker);

            ExtractionCriteria criteria = entityExtractionCriteria(infiniteTimeWindow, "Dev1", "Prop1");
            // when
            ExtractionUnit extractionUnit = service.findExtractionUnitForEntities(criteria);

            ArgumentMatcher<List<ColumnMapping>> checkColumnMappings = input -> {
                assertEquals(3, input.size());
                return true;
            };

            // then
            assertAll(
                    () -> verify(taskMaker).hdfsTask(any()),
                    () -> verify(taskMaker).hbaseTask(any()),
                    () -> verify(columnMapper).getEntityMappings(any(), any()),
                    () -> assertTrue(enhanceWithCommonMappingsWasInvoked.get()),
                    () -> assertFalse(extractionUnit.isVariableSearch()),
                    () -> assertEquals(List.of(), extractionUnit.getTasks()),
                    () -> assertEquals(criteria, extractionUnit.getCriteria())
            );
        }

        @Test
        void shouldReturnExtractionUnitForExtractionCriteriaWithEntityWithAlias() {
            // given
            String alias1Name = "VAL1";
            String alias2Name = "VAL2";

            ExtractionResourceService resourceService = mock(ExtractionResourceService.class);
            EntityHistory entityHistory = createEntityHistory(infiniteTimeWindow, dummyEntity, false);
            EntityExtractionResource resource = createEntityResource(infiniteTimeWindow, entityHistory);
            when(resourceService.findEntityResourcesBy(any())).thenReturn(List.of(resource));

            ExtractionTaskMaker taskMaker = mock(ExtractionTaskMaker.class);
            AtomicBoolean enhanceWithCommonMappingsWasInvoked = new AtomicBoolean(false);
            ColumnMapper columnMapper = mock(ColumnMapper.class, invocation -> {
                String methodName = invocation.getMethod().getName();
                if (methodName.equals("enhanceWithCommonEntityMappings")) {
                    enhanceWithCommonMappingsWasInvoked.set(true);
                    return invocation.getArgument(0);
                }
                return null;
            });

            ColumnMapping columnMappingValueToAlias = columnMappingValue.toBuilder().alias("VAL1").build();
            when(columnMapper.getEntityMappings(any(), any())).thenReturn(
                    List.of(columnMappingValueToAlias, columnMappingDevice, columnMappingProperty));
            when(columnMapper.enhanceWithCommonEntityMappings(any(), anySet())).thenReturn(globalColumnMappings);

            ExtractionService service = new ExtractionService(resourceService, entityService, variableService,
                    columnMapper, taskMaker);

            ExtractionCriteria entityExtractionCriteria = entityExtractionCriteria(infiniteTimeWindow, "Dev1", "Prop1");
            ExtractionCriteria criteriaWithAliases = entityExtractionCriteria.toBuilder()
                    .aliasField(alias1Name, List.of("value"))
                    .aliasField(alias2Name, List.of("NonExisting"))
                    .build();
            // when
            ExtractionUnit extractionUnit = service.findExtractionUnitForEntities(criteriaWithAliases);

            // then
            ArgumentMatcher<ExtractionTaskMaker.TaskInput> checkTaskInput = input -> {
                ExtractionService.SelectionCriteria selectionCriteria = input.getCriteria();
                assertEquals(Set.of(0L), selectionCriteria.getEntityIds());
                return true;
            };

            ArgumentMatcher<List<ColumnMapping>> checkColumnMappings = input -> {
                assertEquals(3, input.size());
                return true;
            };

            assertAll(
                    () -> verify(taskMaker).hdfsTask(argThat(checkTaskInput)),
                    () -> verify(taskMaker).hbaseTask(argThat(checkTaskInput)),
                    () -> verify(columnMapper).getEntityMappings(any(), any()),
                    () -> assertTrue(enhanceWithCommonMappingsWasInvoked.get()),
                    () -> assertFalse(extractionUnit.isVariableSearch()),
                    () -> assertEquals(List.of(), extractionUnit.getTasks()),
                    () -> assertEquals(criteriaWithAliases, extractionUnit.getCriteria())
            );
        }
    }

    @Test
    void shouldReturnExtractionUnitForTwoEntityResourcesPointingToOneEntityWithAndWithoutPredicate() {
        // given
        TimeWindow timeWindow = TimeWindow.infinite();

        VariableExtractionResource resourceWithVariableConfig = createVariableResource(timeWindow, 10L, "");

        VariableExtractionResource resourceWithPredicate = createVariableResource(timeWindow, 10L, "x > 0");

        ExtractionResourceService resourceService = mock(ExtractionResourceService.class);
        when(resourceService.findVariableResourcesBy(any())).thenReturn(
                List.of(resourceWithVariableConfig, resourceWithPredicate));

        ExtractionTaskMaker taskMaker = mock(ExtractionTaskMaker.class);
        ColumnMapper columnMapper = mock(ColumnMapper.class);

        ExtractionService service = new ExtractionService(resourceService, entityService, variableService, columnMapper,
                taskMaker);

        ExtractionCriteria criteria = entityExtractionCriteria(timeWindow, "device", "test_device");

        //when
        ExtractionUnit extractionUnit = service.findExtractionUnitForVariables(criteria);

        // then
        ArgumentMatcher<ExtractionTaskMaker.TaskInput> checkTaskInput = input -> {
            ExtractionService.SelectionCriteria selectionCriteria = input.getCriteria();
            assertAll(
                    () -> assertEquals(Map.of(10L, Optional.empty()), selectionCriteria.getEntitiesWithPredicates()),
                    () -> assertEquals(Set.of(10L), selectionCriteria.getEntityIds())
            );
            return true;
        };

        assertAll(
                () -> verify(taskMaker).hdfsTask(argThat(checkTaskInput)),
                () -> verify(taskMaker).hbaseTask(argThat(checkTaskInput)),
                () -> verify(columnMapper).getVariableMappings(any(), any()),
                () -> assertFalse(extractionUnit.isVariableSearch()),
                () -> assertEquals(List.of(), extractionUnit.getTasks()),
                () -> assertEquals(criteria, extractionUnit.getCriteria())
        );
    }

    @Test
    void shouldReturnExtractionUnitForTwoVariableExtractionResourcesPointingToTwoEntitiesWithAndWithoutPredicate() {
        // given
        TimeWindow timeWindow = TimeWindow.infinite();

        VariableExtractionResource resourceWithoutPredicate = createVariableResource(timeWindow, 10L, "");

        String predicate = "x > 0";
        VariableExtractionResource resourceWithPredicate = createVariableResource(timeWindow, 11L, predicate);

        ExtractionResourceService resourceService = mock(ExtractionResourceService.class);
        when(resourceService.findVariableResourcesBy(any())).thenReturn(
                List.of(resourceWithoutPredicate, resourceWithPredicate));

        ExtractionTaskMaker taskMaker = mock(ExtractionTaskMaker.class);
        ColumnMapper columnMapper = mock(ColumnMapper.class);

        ExtractionService service = new ExtractionService(resourceService, entityService, variableService, columnMapper,
                taskMaker);

        ExtractionCriteria criteria = entityExtractionCriteria(timeWindow, "device", "test_device");

        // when
        ExtractionUnit extractionUnit = service.findExtractionUnitForVariables(criteria);

        // then
        ArgumentMatcher<ExtractionTaskMaker.TaskInput> checkTaskInput = input -> {
            ExtractionService.SelectionCriteria selectionCriteria = input.getCriteria();
            assertAll(
                    () -> assertEquals(Map.of(10L, Optional.empty(), 11L, Optional.of(predicate)),
                            selectionCriteria.getEntitiesWithPredicates()),
                    () -> assertEquals(Set.of(10L, 11L), selectionCriteria.getEntityIds())
            );
            return true;
        };

        assertAll(
                () -> verify(taskMaker).hdfsTask(argThat(checkTaskInput)),
                () -> verify(taskMaker).hbaseTask(argThat(checkTaskInput)),
                () -> verify(columnMapper).getVariableMappings(any(), any()),
                () -> assertFalse(extractionUnit.isVariableSearch()),
                () -> assertEquals(List.of(), extractionUnit.getTasks()),
                () -> assertEquals(criteria, extractionUnit.getCriteria())
        );
    }

    @Test
    void shouldReturnExtractionUnitForTwoEntityResourcesPointingToOneEntityWithPredicates() {
        // given
        TimeWindow timeWindow = TimeWindow.infinite();

        String predicate1 = "x < 10";
        VariableExtractionResource resourceWithVariableConfig = createVariableResource(timeWindow, 10L, predicate1);

        String predicate2 = "x > 0";
        VariableExtractionResource resourceWithPredicate = createVariableResource(timeWindow, 10L, predicate2);

        ExtractionResourceService resourceService = mock(ExtractionResourceService.class);
        when(resourceService.findVariableResourcesBy(any())).thenReturn(
                List.of(resourceWithVariableConfig, resourceWithPredicate));

        ExtractionTaskMaker taskMaker = mock(ExtractionTaskMaker.class);
        ColumnMapper columnMapper = mock(ColumnMapper.class);

        ExtractionService service = new ExtractionService(resourceService, entityService, variableService, columnMapper,
                taskMaker);

        ExtractionCriteria criteria = entityExtractionCriteria(timeWindow, "device", "test_device");

        // when
        ExtractionUnit extractionUnit = service.findExtractionUnitForVariables(criteria);

        // then
        ArgumentMatcher<ExtractionTaskMaker.TaskInput> checkTaskInput = input -> {
            ExtractionService.SelectionCriteria selectionCriteria = input.getCriteria();
            assertAll(
                    () -> assertEquals(Map.of(10L, Optional.of(predicate1 + " or " + predicate2)),
                            selectionCriteria.getEntitiesWithPredicates()),
                    () -> assertEquals(Set.of(10L), selectionCriteria.getEntityIds())
            );
            return true;
        };

        assertAll(
                () -> verify(taskMaker).hdfsTask(argThat(checkTaskInput)),
                () -> verify(taskMaker).hbaseTask(argThat(checkTaskInput)),
                () -> verify(columnMapper).getVariableMappings(any(), any()),
                () -> assertFalse(extractionUnit.isVariableSearch()),
                () -> assertEquals(List.of(), extractionUnit.getTasks()),
                () -> assertEquals(criteria, extractionUnit.getCriteria())
        );
    }

    private Entity createEntityMock(long id) {
        Entity entity = mock(Entity.class);
        when(entity.getId()).thenReturn(id);
        return entity;
    }

    private VariableConfig createVariableConfigMock(String predicate) {
        VariableConfig config = mock(VariableConfig.class);
        when(config.getPredicate()).thenReturn(predicate);
        return config;
    }

    private EntityHistory createEntityHistory(TimeWindow timeWindow, Entity entity, Boolean updatable) {
        return EntityHistory.builder()
                .validity(timeWindow)
                .entity(entity)
                .entitySchema(EntitySchema.builder()
                        .schemaJson(SCHEMA)
                        .build())
                .partition(Partition.builder()
                        .keyValues(keyValues)
                        .systemSpec(systemSpec)
                        .properties(PartitionProperties.builder()
                                .updatable(updatable)
                                .build()
                        ).build())
                .build();
    }

    private EntityExtractionResource createEntityResource(TimeWindow timeWindow, EntityHistory entityHistory) {
        return EntityExtractionResource.builder()
                .resource(mock(Resource.class))
                .entityHistory(entityHistory)
                .timeWindow(timeWindow)
                .build();
    }

    private VariableExtractionResource createVariableResource(TimeWindow timeWindow, EntityHistory entityHistory,
            VariableConfig config) {
        return VariableExtractionResource.builder()
                .resource(mock(Resource.class))
                .entityHistory(entityHistory)
                .variableConfig(config)
                .timeWindow(timeWindow)
                .build();
    }

    private VariableExtractionResource createVariableResource(TimeWindow timeWindow, long entityId,
            String predicate) {
        return createVariableResource(timeWindow,
                createEntityHistory(timeWindow, createEntityMock(entityId), false),
                createVariableConfigMock(predicate));
    }

    private ExtractionCriteria extractionCriteria(TimeWindow timeWindow, Map<String, Boolean> variablePatterns,
            List<EntityKeyValues> keyValues) {
        return ExtractionCriteria
                .builder()
                .systemName("SYSTEM")
                .timeWindow(timeWindow)
                .variables(variablePatterns)
                .entities(keyValues)
                .build();
    }

    private ExtractionCriteria entityExtractionCriteria(TimeWindow timeWindow, String key, String value) {
        EntityKeyValue keyValue = EntityKeyValue.builder()
                .key(key)
                .value(value)
                .wildcard(false)
                .build();
        EntityKeyValues keyValues = EntityKeyValues.builder()
                .keyValue(keyValue)
                .build();
        return extractionCriteria(timeWindow, Map.of(), List.of(keyValues));
    }
}
