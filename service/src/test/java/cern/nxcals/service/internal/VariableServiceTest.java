/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.internal;

import avro.shaded.com.google.common.collect.Sets;
import cern.nxcals.api.domain.Identifiable;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.api.domain.VariableDeclaredType;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.service.BaseTest;
import cern.nxcals.service.domain.EntityData;
import cern.nxcals.service.domain.SystemSpecData;
import cern.nxcals.service.domain.VariableConfigData;
import cern.nxcals.service.domain.VariableData;
import cern.nxcals.service.rest.exceptions.ConfigDataConflictException;
import cern.nxcals.service.rest.exceptions.NotFoundRuntimeException;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Iterables;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_KEY_VALUES_1;
import static java.time.format.DateTimeFormatter.ISO_DATE_TIME;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Transactional(transactionManager = "jpaTransactionManager")
@Rollback
public class VariableServiceTest extends BaseTest {
    private static final DateTimeFormatter UTC = ISO_DATE_TIME.withZone(ZoneOffset.UTC);
    private final Instant t1 = ZonedDateTime.parse("2017-03-11T20:30:30", UTC).toInstant();
    private final Instant t2 = ZonedDateTime.parse("2017-03-12T21:00:45", UTC).toInstant();
    private final Instant t3 = ZonedDateTime.parse("2017-03-13T12:00:45", UTC).toInstant();

    private final String variableName = "test:Variable";
    private final String fieldName = "field";
    private final String description = "Description";
    private final String unit = "var unit";

    @Autowired
    private VariableService variableService;

    @BeforeEach
    public void setUp() {
        setAuthentication();
    }

    @Test
    public void shouldCreateNewVariable() {
        //given
        EntityData entity = createAndSaveDefaultTestEntityKey();
        SystemSpec systemSpec = entity.getPartition().getSystem().toSystemSpec();

        VariableConfig config = createVariableConfig(entity.getId(), fieldName, null, null);
        Variable variableData = createVariable(variableName, systemSpec, description, unit,
                ImmutableSortedSet.of(config));

        //when
        VariableData newVariable = variableService.create(variableData);

        //then
        assertNotNull(newVariable);
        assertEquals(variableName, newVariable.getVariableName());
        assertEquals(newVariable.getSystem().getName(), systemSpec.getName());
        assertEquals(description, newVariable.getDescription());
        assertEquals(unit, newVariable.getUnit());
        assertEquals(1, newVariable.getVariableConfigs().size());
        VariableConfigData createdConfig = Iterables.getOnlyElement(newVariable.getVariableConfigs());
        assertEquals(Long.valueOf(config.getEntityId()), createdConfig.getEntity().getId());
        assertEquals(config.getFieldName(), createdConfig.getFieldName());
        assertNull(createdConfig.getValidFromStamp());
        assertNull(createdConfig.getValidToStamp());
    }

    @Test
    public void shouldUpdateVariable() {
        //given
        EntityData entity = createAndSaveDefaultTestEntityKey();
        SystemSpec systemSpec = entity.getPartition().getSystem().toSystemSpec();
        VariableConfig config = createVariableConfig(entity.getId(), fieldName, null, null);
        Variable variableData = createVariable(variableName, systemSpec, description, unit,
                ImmutableSortedSet.of(config));

        VariableData newVariable = variableService.create(variableData);
        String suffix = "_1";
        Variable updatedData = newVariable.toVariable().toBuilder().variableName(variableName + suffix)
                .description(description + suffix).unit(unit + suffix).build();
        // when
        newVariable = variableService.update(updatedData);

        // then
        assertNotNull(newVariable);
        assertEquals(variableName + suffix, newVariable.getVariableName());
        assertEquals(newVariable.getSystem().getName(), systemSpec.getName());
        assertEquals(description + suffix, newVariable.getDescription());
        assertEquals(unit + suffix, newVariable.getUnit());

        VariableConfigData createdConfig = Iterables.getOnlyElement(newVariable.getVariableConfigs());
        assertEquals(Long.valueOf(config.getEntityId()), createdConfig.getEntity().getId());
        assertEquals(config.getFieldName(), createdConfig.getFieldName());
        assertNull(createdConfig.getValidFromStamp());
        assertNull(createdConfig.getValidToStamp());
    }

    @Test
    public void shouldUpdateVariableMultipleTimes() {
        //given
        EntityData entity = createAndSaveDefaultTestEntityKey();
        SystemSpec systemSpec = entity.getPartition().getSystem().toSystemSpec();
        VariableConfig config = createVariableConfig(entity.getId(), fieldName, null, null);
        Variable variableData = createVariable(variableName, systemSpec, description, unit,
                ImmutableSortedSet.of(config));

        // register
        VariableData newVariable = variableService.create(variableData);
        entityManager.flush();
        assertNotNull(newVariable);
        assertNotNull(newVariable.getRecVersion());
        assertEquals(0L, newVariable.getRecVersion().longValue());

        // update 1
        String updatedFieldName = fieldName + "_updated";
        VariableConfig updatedConfig = config.toBuilder().fieldName(updatedFieldName).build();
        String suffix = "_1";
        Variable updatedData = newVariable.toVariable().toBuilder().variableName(variableName + suffix)
                .description(description + suffix).unit(unit + suffix)
                .configs(new TreeSet<>(Collections.singleton(updatedConfig))).build();
        // when
        newVariable = variableService.update(updatedData);
        entityManager.flush();

        // then
        assertNotNull(newVariable);
        assertNotNull(newVariable.getRecVersion());
        assertEquals(1L, newVariable.getRecVersion().longValue());
        assertEquals(variableName + suffix, newVariable.getVariableName());
        assertEquals(newVariable.getSystem().getName(), systemSpec.getName());
        assertEquals(description + suffix, newVariable.getDescription());
        assertEquals(unit + suffix, newVariable.getUnit());
        VariableConfigData createdConfigA = Iterables.getOnlyElement(newVariable.getVariableConfigs());
        assertEquals(Long.valueOf(config.getEntityId()), createdConfigA.getEntity().getId());
        assertEquals(updatedFieldName, createdConfigA.getFieldName());
        assertNull(createdConfigA.getValidFromStamp());
        assertNull(createdConfigA.getValidToStamp());

        // update 2
        String newDescription = description + " some extra details that we forgot to put before";
        updatedData = newVariable.toVariable().toBuilder().configs(new TreeSet<>(Collections.singleton(config)))
                .description(newDescription).build();
        // when
        newVariable = variableService.update(updatedData);
        entityManager.flush();

        // then
        assertNotNull(newVariable);
        assertNotNull(newVariable.getRecVersion());
        assertEquals(2L, newVariable.getRecVersion().longValue());
        assertEquals(variableName + suffix, newVariable.getVariableName());
        assertEquals(newVariable.getSystem().getName(), systemSpec.getName());
        assertEquals(newDescription, newVariable.getDescription());
        assertEquals(unit + suffix, newVariable.getUnit());
        VariableConfigData createdConfigB = Iterables.getOnlyElement(newVariable.getVariableConfigs());
        assertEquals(Long.valueOf(config.getEntityId()), createdConfigB.getEntity().getId());
        assertEquals(config.getFieldName(), createdConfigB.getFieldName());
        assertNull(createdConfigB.getValidFromStamp());
        assertNull(createdConfigB.getValidToStamp());
    }

    @Test
    public void shouldThrowVariableNotFoundWhenUpdateVariableWithNonExistingId() {
        //given
        long nonExistingId = -32450235L;
        EntityData entity = createAndSaveDefaultTestEntityKey();
        SystemSpec systemSpec = entity.getPartition().getSystem().toSystemSpec();
        VariableConfig config = createVariableConfig(entity.getId(), fieldName, null, null);
        Variable nonExistingVariable = createVariableDataWithFixedId(nonExistingId, variableName, systemSpec,
                description, unit, ImmutableSortedSet.of(config));

        assertThrows(NotFoundRuntimeException.class,
                () -> variableService.update(nonExistingVariable).toVariable(),
                "Update should not accept non-existing input variables! Should throw an exception instead."
        );
    }

    @Test
    public void shouldNotUpdateVariableWhenInputVariableHasOldRecordVersion() {
        //given
        EntityData entity = createAndSaveDefaultTestEntityKey();
        SystemSpec systemSpec = entity.getPartition().getSystem().toSystemSpec();
        VariableConfig config = createVariableConfig(entity.getId(), fieldName, null, null);
        Variable variable = createVariable(variableName, systemSpec, description, unit,
                ImmutableSortedSet.of(config));

        VariableData variableData = variableService.create(variable);
        assertNotNull(variableData);
        assertNotNull(variableData.getId());
        assertEquals(0L, variableData.getRecVersion().longValue());

        String updatedVariableName = variableName + "_1";
        VariableData updatedVariableData = variableService.update(variableData.toVariable()
                .toBuilder().variableName(updatedVariableName).build());
        assertNotNull(updatedVariableData);
        assertNotNull(updatedVariableData.getId());
        assertEquals(updatedVariableName, updatedVariableData.getVariableName());
        assertEquals(1L, updatedVariableData.getRecVersion().longValue());

        // use the original variableData as update input to simulate call with older/outdated data
        assertThrows(ObjectOptimisticLockingFailureException.class,
                () -> variableService.update(variableData.toVariable()),
        "Update should not accept variables with older record version! Should throw an exception instead."
        );
    }

    @Test
    public void shouldAddRemoveAndModifyExistingConfig() {
        //given
        EntityData entity = createAndSaveDefaultTestEntityKey();
        SystemSpec systemSpec = entity.getPartition().getSystem().toSystemSpec();

        VariableConfig config1 = createVariableConfig(entity.getId(), "field1", null, t1);
        VariableConfig config2 = createVariableConfig(entity.getId(), "field2", t1, t2);
        VariableConfig config3 = createVariableConfig(entity.getId(), "field3", t2, null);

        Variable variableData = createVariable(variableName, systemSpec, description, unit,
                ImmutableSortedSet.of(config1, config2, config3));

        Variable newData = variableService.create(variableData).toVariable();

        SortedSet<VariableConfig> configData = newData.getConfigs();
        VariableConfig config3Modified = configData.first().toBuilder()
                .validity(TimeWindow.between(t2.plusSeconds(1), configData.first().getValidity().getEndTime())).build();
        VariableConfig config1Modified = configData.last().toBuilder()
                .validity(TimeWindow.between(configData.last().getValidity().getStartTime(), t1.plusSeconds(1)))
                .build();
        VariableConfig config2Modified = createVariableConfig(entity.getId(), "field2", t1.plusSeconds(1),
                t2.plusSeconds(1));

        Variable updatedData = newData.toBuilder()
                .configs(ImmutableSortedSet.of(config1Modified, config2Modified, config3Modified)).build();

        // when
        newData = variableService.update(updatedData).toVariable();
        configData = newData.getConfigs();
        assertEquals(configData.first().getValidity().getStartTime(), config3Modified.getValidity().getStartTime());
        assertEquals(configData.last().getValidity().getEndTime(), config1Modified.getValidity().getEndTime());

        VariableConfig config2Updated = configData.stream()
                .filter(c -> !c.getValidity().isLeftInfinite() && !c.getValidity().isRightInfinite()).findFirst()
                .orElseThrow(IllegalStateException::new);

        assertEquals(config2Modified.getValidity().getStartTime(), config2Updated.getValidity().getStartTime());
        assertEquals(config2Modified.getValidity().getEndTime(), config2Updated.getValidity().getEndTime());
    }

    @Test
    public void shouldCheckOrderedConfigData() {
        //given
        EntityData entity = createAndSaveDefaultTestEntityKey();
        SystemSpec systemSpec = entity.getPartition().getSystem().toSystemSpec();

        VariableConfig varConfData1 = createVariableConfig(entity.getId(), "field1", null, t1);
        VariableConfig varConfData2 = createVariableConfig(entity.getId(), "field2", t1, t2);
        VariableConfig varConfData3 = createVariableConfig(entity.getId(), "field3", t2, null);

        SortedSet<VariableConfig> varConfSet = ImmutableSortedSet.of(varConfData1, varConfData2, varConfData3);

        Variable variableData = createVariable(variableName, systemSpec, description, unit, varConfSet);

        //when
        VariableData newVariable = variableService.create(variableData);

        //then
        assertNotNull(newVariable);
        assertEquals(variableName, newVariable.getVariableName());
        assertEquals(3, newVariable.getVariableConfigs().size());

        assertThat(newVariable.getVariableConfigs().stream().map(VariableConfigData::getFieldName))
                .containsExactlyInAnyOrder("field1", "field2", "field3");
        assertThat(newVariable.getVariableConfigs().stream().map(VariableConfigData::getValidFromStamp))
                .containsExactlyInAnyOrder(null, t1, t2);
        assertThat(newVariable.getVariableConfigs().stream().map(VariableConfigData::getValidToStamp))
                .containsExactlyInAnyOrder(null, t1, t2);

        assertEquals(newVariable.getSystem().getName(), systemSpec.getName());
    }

    @Test
    public void shouldReplaceOldConfigs() {
        //given
        EntityData entity = createAndSaveDefaultTestEntityKey();
        SystemSpec systemSpec = entity.getPartition().getSystem().toSystemSpec();

        VariableConfig config1 = createVariableConfig(entity.getId(), "field1", null, t1);
        VariableConfig config2 = createVariableConfig(entity.getId(), "field2", t1, null);

        Variable variableData = createVariable(variableName, systemSpec, description, unit,
                ImmutableSortedSet.of(config1, config2));
        Variable initialVariable = variableService.create(variableData).toVariable();

        VariableConfig newConfig = createVariableConfig(entity.getId(), "field1", null, null);
        Variable updatedVariable = initialVariable.toBuilder().configs(ImmutableSortedSet.of(newConfig)).build();

        //when
        Variable resultVariable = variableService.update(updatedVariable).toVariable();

        //then
        assertNotNull(initialVariable);
        assertNotNull(resultVariable);
        assertEquals(initialVariable.getVariableName(), resultVariable.getVariableName());
        assertEquals(2, initialVariable.getConfigs().size());
        assertEquals(1, resultVariable.getConfigs().size());
        assertTrue(resultVariable.getConfigs().first().getValidity().isLeftInfinite());
        assertTrue(resultVariable.getConfigs().first().getValidity().isRightInfinite());
        assertTrue(initialVariable.getConfigs().first().getValidity().isRightInfinite());
        assertEquals(t1, initialVariable.getConfigs().first().getValidity().getStartTime());
        assertEquals(initialVariable.getSystemSpec().getName(), systemSpec.getName());
        assertEquals(resultVariable.getSystemSpec().getName(), systemSpec.getName());
    }

    @Test
    public void shouldReturnMatchedVariablesByName() {
        //given
        SystemSpecData system = createAndSaveDefaultTestEntityKey().getPartition().getSystem();
        String nameExpression = "R%VARIABLE_THAT_WILL_NEVER%";
        VariableData variable = createAndPersistVariable("REGEX_VARIABLE_THAT_WILL_NEVER_BE_IN_DATABASE", system,
                description);

        //when
        List<VariableData> returnedVariables = variableService.findAll(
                toRSQL(Variables.suchThat().systemName().eq(system.getName()).and().variableName()
                        .like(nameExpression)));

        //then
        assertThat(returnedVariables).containsExactly(variable);
        assertThat(returnedVariables.stream().allMatch(var -> var.getSystem().getName().equals(system.getName())))
                .isTrue();
    }

    @Test
    public void shouldReturnMatchedVariablesByDesc() {
        //given
        SystemSpecData system = createAndSaveDefaultTestEntityKey().getPartition().getSystem();
        String descriptionExpression = "V%L_n%n";
        VariableData variable = createAndPersistVariable("REGEX", system, "Very Long Distinctive Expression");

        //when
        List<VariableData> returnedVariables = variableService.findAll(
                toRSQL(Variables.suchThat().systemName().eq(system.getName()).and().description()
                        .like(descriptionExpression)));

        //then
        assertThat(returnedVariables).containsExactly(variable);
        assertThat(returnedVariables.stream().allMatch(var -> var.getSystem().getName().equals(system.getName())))
                .isTrue();
    }

    private VariableConfigData createVariableConfigData(VariableConfig config, EntityData entityData) {
        VariableConfigData configData = new VariableConfigData();

        configData.setEntity(entityData);
        configData.setFieldName(config.getFieldName());
        configData.setValidFromStamp(config.getValidity().getStartTime());
        configData.setValidToStamp(config.getValidity().getEndTime());

        return configData;
    }

    @Test
    public void shouldReturnMatchedVariablesByConfigEntityId() {
        //given
        EntityData entityData = createAndSaveDefaultTestEntityKey();

        VariableConfig vConf1 = createVariableConfig(entityData.getId(), "f1", TimeUtils.getInstantFromNanos(100),
                TimeUtils.getInstantFromNanos(200));

        VariableData variable = createAndPersistVariable("VAR1", entityData.getPartition().getSystem(),
                description, VariableDeclaredType.NUMERIC,
                Sets.newHashSet(createVariableConfigData(vConf1, entityData)));

        //when
        List<VariableData> returnedVariables = variableService.findAll(
                toRSQL(Variables.suchThat().configsEntityId().eq(entityData.getId())));

        //then
        assertThat(returnedVariables).containsExactly(variable);

    }

    @Test
    public void shouldReturnMatchedVariablesByConfigEntityKeyValuesString() {
        //given
        EntityData entityData = createAndSaveDefaultTestEntityKey();

        VariableConfig vConf1 = createVariableConfig(entityData.getId(), "f1", TimeUtils.getInstantFromNanos(100),
                TimeUtils.getInstantFromNanos(200));

        VariableData variable = createAndPersistVariable("VAR1", entityData.getPartition().getSystem(),
                description, VariableDeclaredType.NUMERIC,
                Sets.newHashSet(createVariableConfigData(vConf1, entityData)));

        //when
        List<VariableData> returnedVariables = variableService.findAll(
                toRSQL(Variables.suchThat().configsEntityKeyValues().eq(entityData.getKeyValues())));

        //then
        assertThat(returnedVariables).containsExactly(variable);

    }

    @Test
    public void shouldReturnMatchedVariablesByConfigEntityKeyValues() {
        //given
        EntityData entityData = createAndSaveDefaultTestEntityKey();

        VariableConfig vConf1 = createVariableConfig(entityData.getId(), "f1", TimeUtils.getInstantFromNanos(100),
                TimeUtils.getInstantFromNanos(200));

        VariableData variable = createAndPersistVariable("VAR1", entityData.getPartition().getSystem(),
                description, VariableDeclaredType.NUMERIC,
                Sets.newHashSet(createVariableConfigData(vConf1, entityData)));

        //when
        List<VariableData> returnedVariables = variableService.findAll(
                toRSQL(Variables.suchThat().configsEntityKeyValues()
                        .eq(entityData.getPartition().getSystem().toSystemSpec(), ENTITY_KEY_VALUES_1)));

        //then
        assertThat(returnedVariables).containsExactly(variable);

    }

    @Test
    public void shouldReturnMatchedVariablesByConfigFieldName() {
        //given
        EntityData entityData = createAndSaveDefaultTestEntityKey();

        VariableConfig vConf1 = createVariableConfig(entityData.getId(), "f1,f2", TimeUtils.getInstantFromNanos(100),
                TimeUtils.getInstantFromNanos(200));

        VariableData variable = createAndPersistVariable("VAR1", entityData.getPartition().getSystem(),
                description, VariableDeclaredType.NUMERIC,
                Sets.newHashSet(createVariableConfigData(vConf1, entityData)));

        //when
        List<VariableData> returnedVariables = variableService.findAll(
                toRSQL(Variables.suchThat().configsFieldName().eq("f1,f2")));

        //then
        assertThat(returnedVariables).containsExactly(variable);

    }

    @Test
    public void shouldReturnMatchedVariablesByConfigsValidFromStamp() {
        //given
        EntityData entityData = createAndSaveDefaultTestEntityKey();

        VariableConfig vConf1 = createVariableConfig(entityData.getId(), "f1,f2", TimeUtils.getInstantFromNanos(100),
                TimeUtils.getInstantFromNanos(200));

        VariableData variable = createAndPersistVariable("VAR1", entityData.getPartition().getSystem(),
                description, VariableDeclaredType.NUMERIC,
                Sets.newHashSet(createVariableConfigData(vConf1, entityData)));

        //when
        List<VariableData> returnedVariables = variableService.findAll(
                toRSQL(Variables.suchThat().configsValidFromStamp().eq(TimeUtils.getInstantFromNanos(100))));

        //then
        assertThat(returnedVariables).containsExactly(variable);

    }

    @Test
    public void shouldReturnMatchedVariablesByConfigsValidToStamp() {
        //given
        EntityData entityData = createAndSaveDefaultTestEntityKey();

        VariableConfig vConf1 = createVariableConfig(entityData.getId(), "f1,f2", TimeUtils.getInstantFromNanos(100),
                TimeUtils.getInstantFromNanos(200));

        VariableData variable = createAndPersistVariable("VAR1", entityData.getPartition().getSystem(),
                description, VariableDeclaredType.NUMERIC,
                Sets.newHashSet(createVariableConfigData(vConf1, entityData)));

        //when
        List<VariableData> returnedVariables = variableService.findAll(
                toRSQL(Variables.suchThat().configsValidToStamp().eq(TimeUtils.getInstantFromNanos(200))));

        //then
        assertThat(returnedVariables).containsExactly(variable);

    }

    @Test
    public void shouldReturnMatchedVariablesByTypeAndName() {
        //given
        SystemSpecData system = createAndSaveDefaultTestEntityKey().getPartition().getSystem();

        VariableData variable = createAndPersistVariable("REGEX_VARIABLE_THAT_WILL_NEVER_BE_IN_DATABASE", system,
                description, VariableDeclaredType.FUNDAMENTAL, null);

        //when
        List<VariableData> returnedVariables = variableService.findAll(
                toRSQL(Variables.suchThat().declaredType().eq(VariableDeclaredType.FUNDAMENTAL).and().variableName()
                        .eq("REGEX_VARIABLE_THAT_WILL_NEVER_BE_IN_DATABASE")));

        //then
        assertThat(returnedVariables).containsExactly(variable);
        assertThat(returnedVariables.stream().allMatch(var -> var.getSystem().getName().equals(system.getName())))
                .isTrue();
    }

    @Test
    public void shouldReturnMatchedVariablesByTypeAndNameLike() {
        //given
        SystemSpecData system = createAndSaveDefaultTestEntityKey().getPartition().getSystem();
        String nameExpression = "R%VARIABLE_THAT_WILL_NEVER%";
        VariableData variable = createAndPersistVariable("REGEX_VARIABLE_THAT_WILL_NEVER_BE_IN_DATABASE", system,
                description, VariableDeclaredType.FUNDAMENTAL, null);

        //when
        List<VariableData> returnedVariables = variableService.findAll(
                toRSQL(Variables.suchThat().declaredType().eq(VariableDeclaredType.FUNDAMENTAL).and().variableName()
                        .like(nameExpression)));

        //then

        assertThat(returnedVariables).containsExactly(variable);
        assertThat(returnedVariables.stream().allMatch(var -> var.getSystem().getName().equals(system.getName())))
                .isTrue();
    }

    @Test
    public void shouldFailIfSystemIsNotSet() {
        //given
        EntityData entity = createAndSaveDefaultTestEntityKey();
        VariableConfig varConfData1 = createVariableConfig(entity.getId(), fieldName, null, null);
        SortedSet<VariableConfig> varConfSet = new TreeSet<>(Collections.singletonList(varConfData1));

        //when
        assertThrows(NullPointerException.class, () ->
                createVariable(variableName, null, description, unit, varConfSet));
    }

    @Test
    public void shouldPassIfConfigDataWithTimeHoles() {
        //given
        EntityData entity = createAndSaveDefaultTestEntityKey();
        SystemSpec systemSpec = entity.getPartition().getSystem().toSystemSpec();

        SortedSet<VariableConfig> varConfSet = new TreeSet<>();
        varConfSet.add(createVariableConfig(entity.getId(), "test", null, t1));
        varConfSet.add(createVariableConfig(entity.getId(), "test", t2, null));

        //when
        createVariable(variableName, systemSpec, description, unit, varConfSet);
    }

    @Test
    public void shouldPassIfConfigDataWithClosedEndInterval() {
        //given
        EntityData entity = createAndSaveDefaultTestEntityKey();
        SystemSpec systemSpec = entity.getPartition().getSystem().toSystemSpec();

        SortedSet<VariableConfig> varConfSet = new TreeSet<>();
        varConfSet.add(createVariableConfig(entity.getId(), "test1", null, t1));
        varConfSet.add(createVariableConfig(entity.getId(), "test2", t1, t2));

        //when
        createVariable(variableName, systemSpec, description, unit, varConfSet);
    }

    @Test
    public void shouldPassIfConfigDataWithClosedBothInterval() {
        //when
        EntityData entity = createAndSaveDefaultTestEntityKey();
        SystemSpec systemSpec = entity.getPartition().getSystem().toSystemSpec();

        SortedSet<VariableConfig> varConfSet = new TreeSet<>();
        varConfSet.add(createVariableConfig(entity.getId(), "test1", t1, t2));
        varConfSet.add(createVariableConfig(1, "test1", t2, t3));

        //when
        createVariable(variableName, systemSpec, description, unit, varConfSet);
    }

    @Test
    public void shouldPassIfConfigDataWithClosedStartInterval() {
        //given
        EntityData entity = createAndSaveDefaultTestEntityKey();
        SystemSpec systemSpec = entity.getPartition().getSystem().toSystemSpec();

        SortedSet<VariableConfig> varConfSet = new TreeSet<>();
        varConfSet.add(createVariableConfig(entity.getId(), "test1", t1, t2));
        varConfSet.add(createVariableConfig(entity.getId(), "test1", t2, t3));
        varConfSet.add(createVariableConfig(entity.getId(), "test1", t3, null));

        //when
        createVariable(variableName, systemSpec, description, unit, varConfSet);

    }

    @Test
    public void shouldFailToVerifyConfigDataForOverlappingPeriods() {
        //given
        EntityData entity = createAndSaveDefaultTestEntityKey();
        SystemSpec systemSpec = entity.getPartition().getSystem().toSystemSpec();

        SortedSet<VariableConfig> varConfSet = new TreeSet<>();
        varConfSet.add(createVariableConfig(entity.getId(), "test1", null, t2));
        varConfSet.add(createVariableConfig(1, "test1", t1, null));

        //when
        assertThrows(IllegalStateException.class, () ->
                createVariable(variableName, systemSpec, description, unit, varConfSet));
    }

    @Test
    public void shouldFailToVerifyConfigDataForWrongTimeWindow() {
        //given
        EntityData entity = createAndSaveDefaultTestEntityKey();
        SystemSpec systemSpec = entity.getPartition().getSystem().toSystemSpec();

        SortedSet<VariableConfig> varConfSet = new TreeSet<>();
        varConfSet.add(createVariableConfig(entity.getId(), "test1", null, t2));
        varConfSet.add(createVariableConfig(1, "test1", t2, t1));
        varConfSet.add(createVariableConfig(1, "test1", t1, null));

        //when
        assertThrows(IllegalStateException.class, () ->
                createVariable(variableName, systemSpec, description, unit, varConfSet));
    }

    @Test
    public void shouldThrowExceptionEntityDoesNotExist() {
        //given
        SystemSpec systemSpec = createAndSaveDefaultTestEntityKey().getPartition().getSystem().toSystemSpec();
        VariableConfig config = createVariableConfig(0, fieldName, null, null);

        Variable variableData = createVariable(variableName, systemSpec, description, unit,
                ImmutableSortedSet.of(config));
        //when
        assertThrows(NotFoundRuntimeException.class, () -> variableService.create(variableData));
    }

    @Test
    public void shouldCreateVariableWithNullConfig() {
        //given
        SystemSpec systemSpec = createAndSaveDefaultTestEntityKey().getPartition().getSystem().toSystemSpec();
        Variable variable = createVariable(variableName, systemSpec, description, unit, null);
        assertNotNull(variable);
        assertEquals(Identifiable.NOT_SET, variable.getId());
        assertEquals(variableName, variable.getVariableName());
    }

    @Test
    public void shouldNotFailCreatingVariableWithEmptyConfig() {
        //given
        SystemSpec systemSpec = createAndSaveDefaultTestEntityKey().getPartition().getSystem().toSystemSpec();
        Variable variable = createVariable(variableName, systemSpec, description, unit, Collections.emptySortedSet());
        assertNotNull(variable);
        assertEquals(Identifiable.NOT_SET, variable.getId());
        assertEquals(variableName, variable.getVariableName());
    }

    @Test
    public void shouldNotCreateSeveralVariablesWithTheSameName() {
        //given
        EntityData entity = createAndSaveDefaultTestEntityKey();
        SystemSpec systemSpec = entity.getPartition().getSystem().toSystemSpec();
        VariableConfig config = createVariableConfig(entity.getId(), fieldName, null, null);

        Variable variableData = createVariable(variableName, systemSpec, description, unit,
                ImmutableSortedSet.of(config));
        //when
        variableService.create(variableData);
        assertThrows(ConfigDataConflictException.class, () -> variableService.create(variableData));
    }

    @Test
    public void shouldNotUpdateVariableToExistingName() {
        //given
        EntityData entity = createAndSaveDefaultTestEntityKey();
        SystemSpec systemSpec = entity.getPartition().getSystem().toSystemSpec();
        VariableConfig config = createVariableConfig(entity.getId(), fieldName, null, null);

        String variableOneName = variableName;
        String variableTwoName = variableOneName + "_2";
        Variable variableOne = createVariable(variableOneName, systemSpec, description, unit,
                ImmutableSortedSet.of(config));
        Variable variableTwo = createVariable(variableTwoName, systemSpec, description, unit,
                ImmutableSortedSet.of(config));

        //when
        variableService.create(variableOne);
        VariableData variableTwoData = variableService.create(variableTwo);
        Variable variableTwoWithChangedName = variableTwoData.toVariable().toBuilder().variableName(variableOneName)
                .build();
        assertThrows(ConfigDataConflictException.class, () -> variableService.update(variableTwoWithChangedName));
    }

    @Test
    public void shouldReturnEmtpySetOnFindBySystemIdAndVariableNameInWithWrongSystemId() {
        long numOfElements = 10;
        long numberOfActualVariables = numOfElements / 2;
        SystemSpecData system = createAndPersistSystemDataFor("test-cmw");
        Set<String> variableNames = new HashSet<>();
        for (int i = 0; i < numOfElements; i++) {
            String actualVariableName = variableName + i;
            if (i < numberOfActualVariables) {
                createAndPersistVariable(actualVariableName, system, description);
            }
            variableNames.add(actualVariableName);
        }

        Set<VariableData> fetchedVariables = variableService
                .findBySystemIdAndVariableNameIn(100, variableNames);
        assertNotNull(fetchedVariables);
        assertTrue(fetchedVariables.isEmpty());
    }

    @Test
    public void shouldReturnEmtpySetOnFindBySystemIdAndVariableNameInWithNonExistingVariableNames() {
        long numOfElements = 10;
        long numberOfActualVariables = numOfElements / 2;
        SystemSpecData system = createAndPersistSystemDataFor("test-cmw");
        Set<String> variableNames = new HashSet<>();
        for (int i = 0; i < numOfElements; i++) {
            String actualVariableName = variableName + i;
            if (i < numberOfActualVariables) {
                createAndPersistVariable(actualVariableName, system, description);
            }
            variableNames.add(actualVariableName + ":non:available");
        }

        Set<VariableData> fetchedVariables = variableService
                .findBySystemIdAndVariableNameIn(100, variableNames);
        assertNotNull(fetchedVariables);
        assertTrue(fetchedVariables.isEmpty());
    }

    @Test
    public void shouldFindBySystemIdAndVariableNameInWithMoreThan1kEntries() {
        long numOfElements = 1400;
        long numberOfActualVariables = numOfElements / 2;
        SystemSpecData system = createAndPersistSystemDataFor("test-cmw");
        Set<String> variableNames = new HashSet<>();
        for (int i = 0; i < numOfElements; i++) {
            String actualVariableName = variableName + i;
            if (i < numberOfActualVariables) {
                createAndPersistVariable(actualVariableName, system, description);
            }
            variableNames.add(actualVariableName);
        }

        Set<VariableData> fetchedVariables = variableService
                .findBySystemIdAndVariableNameIn(system.getId(), variableNames);
        assertNotNull(fetchedVariables);
        assertEquals(numberOfActualVariables, fetchedVariables.size());
    }

    @Test
    public void shouldReturnEmptySetOnFindBySystemIdAndVariableNameLikeWithWrongSystem() {
        long numOfElements = 5;
        SystemSpecData system = createAndPersistSystemDataFor("test-cmw");
        for (int i = 0; i < numOfElements; i++) {
            createAndPersistVariable(variableName + i, system, description);
        }

        Set<VariableData> fetchedVariables = variableService
                .findBySystemIdAndVariableNameLike(10, variableName + "%");
        assertNotNull(fetchedVariables);
        assertTrue(fetchedVariables.isEmpty());
    }

    @Test
    public void shouldReturnEmptySetOnFindBySystemIdAndVariableNameLikeWithWrongVariableNamePattern() {
        long numOfElements = 5;
        SystemSpecData system = createAndPersistSystemDataFor("test-cmw");
        for (int i = 0; i < numOfElements; i++) {
            createAndPersistVariable(variableName + i, system, description);
        }

        Set<VariableData> fetchedVariables = variableService
                .findBySystemIdAndVariableNameLike(10, "non:existing:var%");
        assertNotNull(fetchedVariables);
        assertTrue(fetchedVariables.isEmpty());
    }

    @Test
    public void shouldFindBySystemIdAndVariableNameLike() {
        long numOfElements = 50;
        SystemSpecData system = createAndPersistSystemDataFor("test-cmw");
        for (int i = 0; i < numOfElements; i++) {
            createAndPersistVariable(variableName + i, system, description);
        }

        Set<VariableData> fetchedVariables = variableService
                .findBySystemIdAndVariableNameLike(system.getId(), variableName + "%");
        assertNotNull(fetchedVariables);
        assertEquals(numOfElements, fetchedVariables.size());
    }

    @Test
    public void shouldCreateMultipleVariables() {
        EntityData entity = createAndSaveDefaultTestEntityKey();
        SystemSpec systemSpec = entity.getPartition().getSystem().toSystemSpec();

        VariableConfig config = createVariableConfig(entity.getId(), fieldName, null, null);
        Variable variableData1 = createVariable(variableName + "1", systemSpec, description, unit,
                ImmutableSortedSet.of(config));

        Variable variableData2 = createVariable(variableName + "2", systemSpec, description, unit,
                ImmutableSortedSet.of(config));

        //when
        Set<VariableData> newVariables = variableService.createAll(Sets.newHashSet(variableData1, variableData2));

        //then
        assertNotNull(newVariables);
        assertEquals(2, newVariables.size());
        for(VariableData foundVariable: newVariables){
            assertEquals(foundVariable.getSystem().getName(), systemSpec.getName());
            assertEquals(description, foundVariable.getDescription());
            assertEquals(unit, foundVariable.getUnit());
            assertEquals(1, foundVariable.getVariableConfigs().size());
            VariableConfigData createdConfig = Iterables.getOnlyElement(foundVariable.getVariableConfigs());
            assertEquals(Long.valueOf(config.getEntityId()), createdConfig.getEntity().getId());
            assertEquals(config.getFieldName(), createdConfig.getFieldName());
            assertNull(createdConfig.getValidFromStamp());
            assertNull(createdConfig.getValidToStamp());
        }
    }

    @Test
    public void shouldUpdateMultipleVariables() {
        EntityData entity = createAndSaveDefaultTestEntityKey();
        SystemSpec systemSpec = entity.getPartition().getSystem().toSystemSpec();
        VariableConfig config = createVariableConfig(entity.getId(), fieldName, null, null);
        Variable variableData1 = createVariable(variableName + "1", systemSpec, description, unit,
                ImmutableSortedSet.of(config));
        Variable variableData2 = createVariable(variableName + "2", systemSpec, description, unit,
                ImmutableSortedSet.of(config));

        Set<VariableData> createdVariables = variableService.createAll(Sets.newHashSet(variableData1, variableData2));
        String suffix = "_update";
        // when
        Set<Variable> updatedData = createdVariables.stream().map(v -> v.toVariable().toBuilder().variableName(variableName + suffix)
                .description(description + suffix).unit(unit + suffix).build()).collect(Collectors.toSet());
        createdVariables = variableService.updateAll(updatedData);

        for(VariableData foundVariable: createdVariables){
            assertEquals(foundVariable.getSystem().getName(), systemSpec.getName());
            assertEquals(description + suffix, foundVariable.getDescription());
            assertEquals(unit + suffix, foundVariable.getUnit());
            assertEquals(1, foundVariable.getVariableConfigs().size());
            VariableConfigData createdConfig = Iterables.getOnlyElement(foundVariable.getVariableConfigs());
            assertEquals(Long.valueOf(config.getEntityId()), createdConfig.getEntity().getId());
            assertEquals(config.getFieldName(), createdConfig.getFieldName());
            assertNull(createdConfig.getValidFromStamp());
            assertNull(createdConfig.getValidToStamp());
        }
    }

    @Test
    public void shouldNotDeleteVariableIfItDoesNotExist() {
        long nonExistingVariableId = 1234L;
        assertThrows(EmptyResultDataAccessException.class, () -> variableService.delete(nonExistingVariableId));
    }

    @Test
    public void shouldDeleteVariable() {
        EntityData entity = createAndSaveDefaultTestEntityKey();
        SystemSpec systemSpec = entity.getPartition().getSystem().toSystemSpec();
        VariableConfig config = createVariableConfig(entity.getId(), fieldName, null, null);
        Variable variableData = createVariable(variableName + "1", systemSpec, description, unit,
                ImmutableSortedSet.of(config));
        VariableData createdVariable = variableService.create(variableData);
        variableService.delete(createdVariable.getId());
    }

    @Test
    public void shouldDeleteMultipleVariables() {
        EntityData entity = createAndSaveDefaultTestEntityKey();
        SystemSpec systemSpec = entity.getPartition().getSystem().toSystemSpec();
        VariableConfig config = createVariableConfig(entity.getId(), fieldName, null, null);
        Variable variableData1 = createVariable(variableName + "1", systemSpec, description, unit,
                ImmutableSortedSet.of(config));
        Variable variableData2 = createVariable(variableName + "2", systemSpec, description, unit,
                ImmutableSortedSet.of(config));
       Set<VariableData> createdVariables =  variableService.createAll(Sets.newHashSet(variableData1, variableData2));
        variableService.deleteAll(createdVariables.stream().map(VariableData::getId).collect(Collectors.toSet()));
    }
}
