package cern.nxcals.service.internal.hdfs;

import cern.nxcals.api.utils.TimeUtils;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.time.temporal.ChronoUnit.DAYS;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class DateGroupingTest {
    @Test
    public void shouldNotGroupForDays() {
        final Instant start = TimeUtils.getInstantFromString("2017-12-03 10:15:30.0");
        final Instant end = TimeUtils.getInstantFromString("2017-12-05 10:15:30.0");
        Set<String> result = new DateGrouping(DAYS, DateGroupingType.YEARS_MONTHS_AND_DAYS).group(start, end);
        assertEquals(3, result.size());
        Set<String> expected = days(2017, 12, 3, 5);
        assertEquals(expected, result);
    }

    @Test
    public void shouldNotGroupForDaysSpanningMonth() {
        final Instant start = TimeUtils.getInstantFromString("2017-11-30 10:15:30.0");
        final Instant end = TimeUtils.getInstantFromString("2017-12-03 10:15:30.0");
        Set<String> result = new DateGrouping(DAYS, DateGroupingType.YEARS_MONTHS_AND_DAYS).group(start, end);
        assertEquals(4, result.size());
        Set<String> expected = days(2017, 12, 1, 3);
        expected.add(day(2017, 11, 30));
        assertEquals(expected, result);
    }

    @Test
    public void shouldNotGroupForMinutes() {
        final Instant start = TimeUtils.getInstantFromString("2017-11-30 10:15:30.0");
        final Instant end = TimeUtils.getInstantFromString("2017-11-30 10:17:30.0");
        Set<String> result = new DateGrouping(DAYS, DateGroupingType.YEARS_MONTHS_AND_DAYS).group(start, end);
        assertEquals(1, result.size());
        Set<String> expected = days(2017, 11, 30, 30);
        assertEquals(expected, result);
    }

    @Test
    public void shouldGroupMonthsAtEdges() {
        final Instant start = TimeUtils.getInstantFromString("2017-11-01 00:00:00.0");
        final Instant end = TimeUtils.getInstantFromString("2017-12-31 23:59:59.9");
        Set<String> result = new DateGrouping(DAYS, DateGroupingType.YEARS_MONTHS_AND_DAYS).group(start, end);
        assertEquals(2, result.size());
        Set<String> expected = months(2017, 11, 12);
        assertEquals(expected, result);
    }

    @Test
    public void shouldGroupMonthsAtYearEdges() {
        final Instant start = TimeUtils.getInstantFromString("2017-11-01 00:00:00.0");
        final Instant end = TimeUtils.getInstantFromString("2018-01-01 00:00:00.0");
        Set<String> result = new DateGrouping(DAYS, DateGroupingType.YEARS_MONTHS_AND_DAYS).group(start, end);
        assertEquals(3, result.size());
        Set<String> expected = months(2017, 11, 12);
        expected.add(day(2018, 1, 1));
        assertEquals(expected, result);
    }

    @Test
    public void shouldGroupMonthsAtDaysEdges() {
        final Instant start = TimeUtils.getInstantFromString("2017-11-01 00:00:00.0");
        final Instant end = TimeUtils.getInstantFromString("2018-01-01 00:00:00.1");
        Set<String> result = new DateGrouping(DAYS, DateGroupingType.YEARS_MONTHS_AND_DAYS).group(start, end);
        assertEquals(3, result.size());
        Set<String> expected = months(2017, 11, 12);
        expected.add(day(2018, 1, 1));
        assertEquals(expected, result);
    }

    @Test
    public void shouldIgnoreEmptyTimeWindows() {
        final Instant start = TimeUtils.getInstantFromString("2017-11-30 11:15:30.0");
        final Instant end = TimeUtils.getInstantFromString("2017-11-30 10:17:30.0");
        Set<String> result = new DateGrouping(DAYS, DateGroupingType.YEARS_MONTHS_AND_DAYS).group(start, end);
        assertEquals(0, result.size());
    }

    @Test
    public void shouldIgnoreEmptyTimeWindowsAtUnitEdges() {
        final Instant start = TimeUtils.getInstantFromString("2017-11-30 00:00:01.0");
        final Instant end = TimeUtils.getInstantFromString("2017-11-30 00:00:00.0");
        Set<String> result = new DateGrouping(DAYS, DateGroupingType.YEARS_MONTHS_AND_DAYS).group(start, end);
        assertEquals(0, result.size());
    }

    @Test
    public void shouldIgnoreZeroTimeWindowsAtUnitEdges() {
        final Instant start = TimeUtils.getInstantFromString("2017-01-30 00:00:00.0");
        final Instant end = TimeUtils.getInstantFromString("2017-01-30 00:00:00.0");
        Set<String> result = new DateGrouping(DAYS, DateGroupingType.YEARS_MONTHS_AND_DAYS).group(start, end);
        assertEquals(1, result.size());
        assertEquals(days(2017, 1, 30, 30), result);
    }

    @Test
    public void shouldGroupMonths() {
        final Instant start = TimeUtils.getInstantFromString("2016-12-30 10:15:30.0");
        final Instant end = TimeUtils.getInstantFromString("2018-02-02 10:15:30.0");
        Set<String> result = new DateGrouping(DAYS, DateGroupingType.YEARS_MONTHS_AND_DAYS).group(start, end);
        assertEquals(6, result.size());
        Set<String> expected = new HashSet<>();
        expected.add(year(2017));
        expected.add(month(2018, 1));
        expected.addAll(days(2016, 12, 30, 31));
        expected.addAll(days(2018, 2, 1, 2));
        assertEquals(expected, result);
    }

    @Test
    public void shouldGroupYears() {
        final Instant start = TimeUtils.getInstantFromString("2016-12-30 10:15:30.0");
        final Instant end = TimeUtils.getInstantFromString("2018-01-02 10:15:30.0");
        Set<String> result = new DateGrouping(DAYS, DateGroupingType.YEARS_MONTHS_AND_DAYS).group(start, end);
        assertEquals(5, result.size());
        Set<String> expected = new HashSet<>();
        expected.add(year(2017));
        expected.addAll(days(2016, 12, 30, 31));
        expected.addAll(days(2018, 1, 1, 2));
        assertEquals(expected, result);
    }

    @Test
    public void shouldGroupYearsAndMonths() {
        final Instant start = TimeUtils.getInstantFromString("2017-10-30 10:15:30.0");
        final Instant end = TimeUtils.getInstantFromString("2017-12-03 10:15:30.0");
        Set<String> result = new DateGrouping(DAYS, DateGroupingType.YEARS_MONTHS_AND_DAYS).group(start, end);
        assertEquals(6, result.size());
        Set<String> expected = new HashSet<>();
        expected.add(month(2017, 11));
        expected.addAll(days(2017, 10, 30, 31));
        expected.addAll(days(2017, 12, 1, 3));
        assertEquals(expected, result);
    }

    @Test
    public void shouldGroupYearsAtEdges() {
        final Instant start = TimeUtils.getInstantFromString("2016-12-01 00:00:00.0");
        final Instant end = TimeUtils.getInstantFromString("2017-01-01 00:00:00.0");
        Set<String> result = new DateGrouping(DAYS, DateGroupingType.YEARS_MONTHS_AND_DAYS).group(start, end);
        Set<String> expected = new HashSet<>();
        expected.add(month(2016, 12));
        expected.add(day(2017, 1, 1));
        assertEquals(2, result.size());
        assertEquals(expected, result);
    }

    @Test
    public void shouldGroupYearsAtYearEdges() {
        final Instant start = TimeUtils.getInstantFromString("2016-01-01 00:00:00.0");
        final Instant end = TimeUtils.getInstantFromString("2017-01-01 00:00:01.0");
        Set<String> result = new DateGrouping(DAYS, DateGroupingType.YEARS_MONTHS_AND_DAYS).group(start, end);
        assertEquals(2, result.size());
        Set<String> expected = new HashSet<>();
        expected.add(year(2016));
        expected.add(day(2017, 1, 1));
        assertEquals(expected, result);
    }

    @Test
    public void shouldNotGroupYearsWithMonthGrouper() {
        final Instant start = TimeUtils.getInstantFromString("2016-12-01 00:00:00.0");
        final Instant end = TimeUtils.getInstantFromString("2018-01-01 00:00:00.0");
        Set<String> result = new DateGrouping(DAYS, DateGroupingType.MONTHS_AND_DAYS).group(start, end);
        assertEquals(14, result.size());
        Set<String> expected = months(2017, 1, 12);
        expected.add(month(2016, 12));
        expected.add(day(2018, 1, 1));
        assertEquals(expected, result);
    }

    @Test
    public void shouldNotGroupYearsWithMonthGrouperRightClosed() {
        final Instant start = TimeUtils.getInstantFromString("2016-12-01 00:00:00.0");
        final Instant end = TimeUtils.getInstantFromString("2017-12-31 23:59:59.99999");
        Set<String> result = new DateGrouping(DAYS, DateGroupingType.MONTHS_AND_DAYS).group(start, end);
        assertEquals(13, result.size());
        Set<String> expected = months(2017, 1, 12);
        expected.add(month(2016, 12));
        assertEquals(expected, result);
    }

    @Test
    public void shouldNotGroupYearsWithDayGrouper() {
        final Instant start = TimeUtils.getInstantFromString("2016-12-01 00:00:00.0");
        final Instant end = TimeUtils.getInstantFromString("2018-01-01 00:00:00.0");
        Set<String> result = new DateGrouping(DAYS, DateGroupingType.DAYS).group(start, end);
        assertEquals(397, result.size());
        Set<String> expected = days(2017, 1, 365);
        expected.addAll(days(2016, 12, 1, 31));
        expected.add(day(2018, 1, 1));
        assertEquals(expected, result);
    }

    @Test
    public void shouldGroupCurrentDay() {
        final Instant day = Instant.now().truncatedTo(DAYS);

        Set<String> result = new DateGrouping(DAYS, DateGroupingType.DAYS).group(day, day);
        assertEquals(1, result.size());

    }

    private Set<String> years(int yearStart, int yearEnd) {
        return IntStream.rangeClosed(yearStart, yearEnd).mapToObj(this::year).collect(Collectors.toSet());
    }

    private String year(int year) {
        return DateGroupingType.YEARS_MONTHS_AND_DAYS.getFormatter().format(LocalDate.of(year, 1, 1));
    }

    private Set<String> months(int year, int monthStart, int monthEnd) {
        return IntStream.rangeClosed(monthStart, monthEnd).mapToObj(month -> month(year, month)).collect(Collectors.toSet());
    }

    private String month(int year, int month) {
        return DateGroupingType.MONTHS_AND_DAYS.getFormatter().format( LocalDate.of(year, month, 1));
    }

    private Set<String> days(int year, int dayStart, int dayEnd) {
        return IntStream.rangeClosed(dayStart, dayEnd).mapToObj(day -> day(year, day)).collect(Collectors.toSet());
    }

    private Set<String> days(int year, int month, int dayStart, int dayEnd) {
        return IntStream.rangeClosed(dayStart, dayEnd).mapToObj(day -> day(year, month, day)).collect(Collectors.toSet());
    }

    private String day(int year, int day) {
        return DateGroupingType.DAYS.getFormatter().format( LocalDate.ofYearDay(year, day));
    }

    private String day(int year, int month, int day) {
        return DateGroupingType.DAYS.getFormatter().format( LocalDate.of(year, month, day));
    }

}