/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.internal;

import cern.nxcals.api.domain.Partition;
import cern.nxcals.api.domain.PartitionProperties;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.extraction.metadata.queries.Partitions;
import cern.nxcals.service.BaseTest;
import cern.nxcals.service.domain.PartitionData;
import cern.nxcals.service.domain.SystemSpecData;
import com.google.common.collect.ImmutableMap;
import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

import static cern.nxcals.common.utils.KeyValuesUtils.convertMapIntoAvroSchemaString;
import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_SCHEMA_1;
import static cern.nxcals.service.rest.TestSchemas.TIME_SCHEMA;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@Transactional(transactionManager = "jpaTransactionManager")
@Rollback
class PartitionServiceTest extends BaseTest {
    static final String PARTITION_STRING_SCHEMA_KEY = "partition_string_1";
    static final Schema PARTITION_SCHEMA = SchemaBuilder.record("partition_type_1").fields()
            .name(PARTITION_STRING_SCHEMA_KEY).type().stringType().noDefault()
            .endRecord();
    static final Map<String, Object> PARTITION_KEY_VALUES = ImmutableMap
            .of(PARTITION_STRING_SCHEMA_KEY, "string_1");
    @Autowired
    private PartitionService partitionService;

    @BeforeEach
    void setUp() {
        setAuthentication();
    }

    @Test
    void shouldUpdatePartition() {
        //given
        SystemSpecData system = createAndPersistSystemData(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA,
                TIME_SCHEMA);
        PartitionData savedPartition = partitionRepository
                .save(createPartition(system, PARTITION_KEY_VALUES, PARTITION_SCHEMA));
        assertEquals(false, savedPartition.getProperties().getUpdatable());
        Partition newPartition = savedPartition.toPartition().toBuilder()
                .properties(new PartitionProperties(true, null, TimeWindow.between(null, null)))
                .build();
        // when
        PartitionData updatedPartition = partitionService.update(newPartition);
        //then
        assertEquals(true, updatedPartition.getProperties().getUpdatable());
    }

    @Test
    void shouldCreatePartition() {
        //given
        SystemSpecData system = createAndPersistSystemData(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA,
                TIME_SCHEMA);
        Partition partition = Partition.builder().keyValues(PARTITION_KEY_VALUES).systemSpec(system.toSystemSpec())
                .build();
        // when
        PartitionData savedPartition = partitionService.create(partition);
        //then
        assertNotNull(savedPartition);
        assertEquals(partition.getSystemSpec(), savedPartition.getSystem().toSystemSpec());
        assertEquals(convertMapIntoAvroSchemaString(partition.getKeyValues(), system.getPartitionKeyDefs()),
                savedPartition.getKeyValues());
    }

    @Test
    void shouldFindPartitionByKeyValues() {
        SystemSpecData system = createAndPersistSystemData(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA,
                TIME_SCHEMA);
        PartitionData savedPartition = partitionRepository
                .save(createPartition(system, PARTITION_KEY_VALUES, PARTITION_SCHEMA));
        List<PartitionData> foundPartitions = partitionService
                .findAll(toRSQL(Partitions.suchThat().keyValues().eq(system.toSystemSpec(), PARTITION_KEY_VALUES)));
        assertEquals(1, foundPartitions.size());
        assertEquals(savedPartition, foundPartitions.get(0));
    }

    @Test
    void shouldFindById() {
        //given
        SystemSpecData system = createAndPersistSystemData(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA,
                TIME_SCHEMA);
        PartitionData savedPartition = partitionRepository
                .save(createPartition(system, PARTITION_KEY_VALUES, PARTITION_SCHEMA));

        //when
        PartitionData partitionData = partitionService.findById(savedPartition.getId()).get();


        //then
        assertEquals(savedPartition, partitionData);


    }
}
