/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service;

import cern.nxcals.api.domain.EntitySchema;
import cern.nxcals.api.domain.Group;
import cern.nxcals.api.domain.Partition;
import cern.nxcals.api.domain.PartitionResource;
import cern.nxcals.api.domain.PartitionResourceHistory;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.api.domain.VariableDeclaredType;
import cern.nxcals.api.domain.Visibility;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.common.utils.KeyValuesUtils;
import cern.nxcals.common.utils.ReflectionUtils;
import cern.nxcals.service.domain.EntityData;
import cern.nxcals.service.domain.EntityHistoryData;
import cern.nxcals.service.domain.EntitySchemaData;
import cern.nxcals.service.domain.GroupData;
import cern.nxcals.service.domain.PartitionData;
import cern.nxcals.service.domain.PartitionPropertiesData;
import cern.nxcals.service.domain.PartitionResourceData;
import cern.nxcals.service.domain.PartitionResourceHistoryData;
import cern.nxcals.service.domain.SystemSpecData;
import cern.nxcals.service.domain.VariableConfigData;
import cern.nxcals.service.domain.VariableData;
import cern.nxcals.service.domain.VisibilityData;
import cern.nxcals.service.internal.EntityService;
import cern.nxcals.service.repository.EntityHistoryRepository;
import cern.nxcals.service.repository.EntityRepository;
import cern.nxcals.service.repository.GroupRepository;
import cern.nxcals.service.repository.PartitionRepository;
import cern.nxcals.service.repository.PartitionResourceHistoryRepository;
import cern.nxcals.service.repository.PartitionResourceRepository;
import cern.nxcals.service.repository.SchemaRepository;
import cern.nxcals.service.repository.SystemSpecRepository;
import cern.nxcals.service.repository.VariableRepository;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.avro.Schema;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.hadoop.hbase.client.Connection;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.Instant;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_KEY_VALUES_1;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_KEY_VALUES_JSON;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_1;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_JSON_1;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_SCHEMA_1;
import static cern.nxcals.service.rest.TestSchemas.PARTITION_SCHEMA_1;
import static cern.nxcals.service.rest.TestSchemas.TIME_SCHEMA;
import static cern.nxcals.service.security.MethodSecurityExpressionRoot.ACCESS_ADMIN;
import static cern.nxcals.service.security.MethodSecurityExpressionRoot.ACCESS_WRITE;
import static cern.nxcals.service.security.UserPrincipalService.EMPTY_PASSWORD;
import static cern.nxcals.service.security.resolvers.ConcatenatingPermissionResolver.ROLE_SEPARATOR;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

//FIXME tests based on this class are integration tests and not really unit tests
// This makes it hard to understand, change and maintain. Also slows down significantly the test execution.
// It should be changed so that we actually mock the next layers instead of recreating the whole context every single time
@Slf4j
@ExtendWith(SpringExtension.class)
@Transactional(transactionManager = "jpaTransactionManager")
@SpringBootTest(classes = { Application.class, DummyKerberosBean.class })
public abstract class BaseTest implements ApplicationContextAware {

    public static final String TEST_NAME = "Test Name";
    public static final String ROLE_PREFIX = "NXCALS-";
    public static final long TEST_RECORD_TIME = 1000000000L;
    public static final String SYSTEM_NAME = "SYSTEM_NAME";
    public static final String SYSTEM_NAME2 = "SYSTEM_NAME2";
    public static final String VARIABLE = "VARIABLE";
    protected static final String TEST_USER_NAME = "UNIT_TEST_USER";
    public static final String AUTHORITY = ROLE_PREFIX + SYSTEM_NAME + ROLE_SEPARATOR + ACCESS_WRITE;
    public static final String AUTHORITY2 = ROLE_PREFIX + SYSTEM_NAME2 + ROLE_SEPARATOR + ACCESS_WRITE;
    public static final String VARIABLE_AUTHORITY = ROLE_PREFIX + VARIABLE + ROLE_SEPARATOR + ACCESS_WRITE;
    public static final String ADMIN_AUTHORITY = ROLE_PREFIX + ACCESS_ADMIN;

    @PersistenceContext
    protected EntityManager entityManager;
    @Autowired
    protected SystemSpecRepository systemRepository;
    @Autowired
    protected EntityRepository entityRepository;
    @Autowired
    protected SchemaRepository schemaRepository;
    @Autowired
    protected PartitionRepository partitionRepository;
    @Autowired
    protected EntityHistoryRepository entityHistRepository;
    @Autowired
    protected VariableRepository variableRepository;
    @Autowired
    protected GroupRepository groupRepository;
    @Autowired
    protected PartitionResourceRepository partitionResourceRepository;
    @Autowired
    protected PartitionResourceHistoryRepository partitionResourceHistoryRepository;
    @Autowired
    protected EntityService service;

    // avoids real HBase Connection bean creation that repeatedly fails to retrieve cluster id until all retries expired.
    // speeds up tests
    @MockBean
    public Connection hbaseConnection;

    @Override
    public void setApplicationContext(ApplicationContext context) throws BeansException {
        IdGeneratorFactory.setContext(context);
    }

    protected SystemSpecData findOrCreateAndPersistSystemData(@NonNull String systemName, Schema entitySchema,
            Schema partitionSchema, Schema timeSchema) {
        return systemRepository.findByName(systemName)
                .orElseGet(() -> createAndPersistSystemData(systemName, entitySchema, partitionSchema, timeSchema));
    }

    protected SystemSpecData createAndPersistSystemData(String systemName, Schema entitySchema, Schema partitionSchema,
            Schema timeSchema) {
        SystemSpecData system = new SystemSpecData();
        if (entitySchema != null) {
            system.setEntityKeyDefs(entitySchema.toString());
        }
        if (partitionSchema != null) {
            system.setPartitionKeyDefs(partitionSchema.toString());
        }
        if (timeSchema != null) {
            system.setTimeKeyDefs(timeSchema.toString());
        }
        if (systemName != null) {
            system.setName(systemName);
        }
        return systemRepository.save(system);
    }

    protected SystemSpec createAndPersistSystem(String systemName) {
        return createAndPersistSystemData(systemName, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA).toSystemSpec();
    }

    protected SystemSpecData createAndPersistSystemDataFor(String systemName) {
        return createAndPersistSystemData(systemName, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA);
    }

    protected PartitionData createPartition(SystemSpecData system, Map<String, Object> partitionKeyValues,
            Schema schema) {
        return createPartitionWithProps(system, partitionKeyValues, schema, PartitionPropertiesData.empty());
    }

    protected PartitionData createPartitionWithProps(SystemSpecData system, Map<String, Object> partitionKeyValues,
            Schema schema, PartitionPropertiesData props) {
        PartitionData partition = new PartitionData();
        partition.setSystem(system);
        String keyValuesContent = partitionKeyValues != null ?
                KeyValuesUtils.convertMapIntoAvroSchemaString(partitionKeyValues, schema.toString()) :
                null;
        partition.setKeyValues(keyValuesContent);
        partition.setProperties(props);
        partitionRepository.save(partition);
        return partition;
    }

    protected PartitionData createPartition(String systemName, Map<String, Object> partitionKeyValues, Schema schema) {
        SystemSpecData system = findOrCreateAndPersistSystemData(systemName, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1,
                TIME_SCHEMA);
        return createPartition(system, partitionKeyValues, schema);
    }

    protected PartitionData createPartitionWithProps(String systemName, Map<String, Object> partitionKeyValues,
            Schema schema,
            PartitionPropertiesData props) {
        SystemSpecData system = findOrCreateAndPersistSystemData(systemName, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1,
                TIME_SCHEMA);
        return createPartitionWithProps(system, partitionKeyValues, schema, props);
    }

    protected EntitySchemaData createSchema(Schema schema) {
        EntitySchemaData newSchema = new EntitySchemaData();
        String schemaContent = schema != null ? schema.toString() : null;
        newSchema.setContent(schemaContent);
        if (schemaContent != null) {
            newSchema.setContentHash(DigestUtils.md5Hex(schemaContent));
        }
        schemaRepository.save(newSchema);
        return newSchema;
    }

    protected EntityData createEntity(String systemName, Map<String, Object> entityKeyValues, Schema entitySchema,
            Map<String, Object> partitionKeyValues, Schema partitionSchema) {
        return createEntity(systemName, entityKeyValues, entitySchema, partitionKeyValues, partitionSchema, null);
    }

    protected EntityData createEntity(String systemName, Map<String, Object> entityKeyValues, Schema entitySchema,
            Map<String, Object> partitionKeyValues, Schema partitionSchema, Instant lockedTimestamp) {
        EntityData entity = new EntityData();
        String entityKeyValuesString = entityKeyValues != null ?
                KeyValuesUtils.convertMapIntoAvroSchemaString(entityKeyValues, entitySchema.toString()) :
                null;
        entity.setKeyValues(entityKeyValuesString);
        entity.setPartition(createPartition(systemName, partitionKeyValues, partitionSchema));
        if (lockedTimestamp != null) {
            entity.setLockedUntilStamp(lockedTimestamp);
        }
        return entity;
    }

    protected EntityData createAndPersistEntity(String systemName, Map<String, Object> entityKeyValues,
            Schema entitySchema, Map<String, Object> partitionKeyValues, Schema partitionSchema) {
        return createAndPersistEntity(systemName, entityKeyValues, entitySchema, partitionKeyValues, partitionSchema,
                null);
    }

    protected EntityData createAndPersistEntity(String systemName, Map<String, Object> entityKeyValues,
            Schema entitySchema, Map<String, Object> partitionKeyValues, Schema partitionSchema,
            Instant lockedTimestamp) {
        return persistEntity(
                createEntity(systemName, entityKeyValues, entitySchema, partitionKeyValues, partitionSchema,
                        lockedTimestamp));
    }

    protected EntityData persistEntity(EntityData entity) {
        if (entity.getPartition() != null) {
            partitionRepository.save(entity.getPartition());
        }
        return entityRepository.save(entity);
    }

    protected VariableData createAndPersistVariable(String variableName, SystemSpecData system,
            String variableDescription) {

        return createAndPersistVariable(variableName, system, variableDescription, null, null);
    }

    protected VariableData createAndPersistVariable(String variableName, SystemSpecData system,
            String variableDescription, VariableDeclaredType declaredType, Set<VariableConfigData> configs) {
        VariableData variable = new VariableData();
        variable.setVariableName(variableName);
        variable.setSystem(system);
        variable.setDescription(variableDescription);
        variable.setDeclaredType(declaredType);

        if (configs != null) {
            configs.forEach(variable::addConfig);
        }
        return persistVariable(variable);
    }

    protected VariableData persistVariable(VariableData variableData) {
        return variableRepository.save(variableData);
    }

    protected EntityData createAndSaveDefaultTestEntityKey() {
        EntityData entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);
        createAndPersistEntityHistory(entity, entity.getPartition(), createSchema(ENTITY_SCHEMA_1), TEST_RECORD_TIME);
        return entity;
    }

    // FIXME entity already has partition and schema, do we use this to force different partitions ands schemas somewhere? If not receive only entity.
    protected EntityHistoryData createAndPersistEntityHistory(EntityData key, PartitionData part,
            EntitySchemaData schema, Long recordTimestamp) {
        EntityHistoryData history = new EntityHistoryData();
        history.setEntity(key);
        history.setPartition(part);
        history.setSchema(schema);
        if (recordTimestamp != null) {
            history.setValidFromStamp(TimeUtils.getInstantFromNanos(recordTimestamp));
        }
        if (key != null) {
            key.addEntityHist(history);
        }
        return this.entityHistRepository.save(history);
    }

    protected EntityHistoryData createAndPersistDefaultTestEntityKeyHist() {
        EntityData entity = createAndPersistEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1,
                PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);
        EntityHistoryData entityHistory = createAndPersistEntityHistory(entity, entity.getPartition(),
                createSchema(ENTITY_SCHEMA_1), TEST_RECORD_TIME);
        this.entityRepository.save(entity);
        return entityHistory;
    }

    protected VariableConfig createVariableConfig(long entityId, String fieldName, Instant from, Instant to) {
        return VariableConfig.builder().entityId(entityId).fieldName(fieldName).validity(TimeWindow.between(from, to))
                .build();
    }

    protected VariableConfigData createVariableConfigData(EntityData entityData, String fieldName, Instant from,
            Instant to) {
        VariableConfigData result = new VariableConfigData();
        result.setEntity(entityData);
        result.setFieldName(fieldName);
        result.setValidFromStamp(from);
        result.setValidToStamp(to);
        return result;
    }

    protected Variable createVariable(String variableName, SystemSpec systemSpec, String description, String unit,
            SortedSet<VariableConfig> configs) {
        return Variable.builder().variableName(variableName).systemSpec(systemSpec).description(description).unit(unit)
                .configs(configs).build();
    }

    protected PartitionResource createPartitionResource(SystemSpec system, Partition partition, EntitySchema schema) {
        return PartitionResource.builder().systemSpec(system).partition(partition).schema(schema).build();
    }

    protected PartitionResourceHistory createPartitionResourceHistory(PartitionResource pr, String information,
            String compactionType, String storageType,
            boolean isFixedSettings, Instant startTime,
            Instant endTime) {
        return PartitionResourceHistory.builder().partitionResource(pr).partitionInformation(information)
                .compactionType(compactionType).storageType(storageType).isFixedSettings(isFixedSettings)
                .validity(TimeWindow.between(startTime, endTime)).build();
    }

    protected Variable createVariableDataWithFixedId(long id, String variableName, SystemSpec systemSpec,
            String description, String unit, SortedSet<VariableConfig> configs) {
        return ReflectionUtils.builderInstance(Variable.InnerBuilder.class).id(id).variableName(variableName)
                .systemSpec(systemSpec).description(description).unit(unit).configs(configs).build();
    }

    protected Group protectedGroup(String name, SystemSpec system) {
        return Group.builder().label("my-group-label").name(name).description("desc").systemSpec(system)
                .visibility(Visibility.PROTECTED).build();
    }

    protected Group publicGroup(String name, SystemSpec system) {
        return publicGroup(name, system, "my-group-label");
    }

    protected Group publicGroup(String name, SystemSpec system, String label) {
        return Group.builder().label(label).name(name).description("desc").systemSpec(system)
                .visibility(Visibility.PUBLIC).build();
    }

    protected GroupData persistGroup(GroupData groupData) {
        return groupRepository.save(groupData);
    }

    protected GroupData groupData(String name, SystemSpecData system, String label) {
        GroupData group = groupData(name, system);
        group.setLabel(label);
        return group;
    }

    protected GroupData groupDataWithUser(String name, SystemSpecData system, String user) {
        GroupData group = groupData(name, system);
        group.setOwner(user);
        return group;
    }

    protected GroupData groupData(String name, SystemSpecData system) {
        GroupData group = new GroupData();
        group.setLabel("my-group-label");
        group.setName(name);
        group.setSystem(system);
        group.setVisibility(VisibilityData.PUBLIC);
        return group;
    }

    protected PartitionResourceData partitionResourceData(SystemSpecData system, EntitySchemaData entitySchema,
            Map<String, Object> partitionKeyValues, Schema schema) {
        PartitionResourceData partitionResource = new PartitionResourceData();
        partitionResource.setSystem(system);
        partitionResource.setPartition(partitionData(system, partitionKeyValues, schema));
        partitionResource.setSchema(entitySchema);
        partitionResource.setRecVersion(2L);
        return partitionResourceRepository.save(partitionResource);
    }

    protected PartitionResourceData createDefaultPartitionResourceData() {
        EntityData entityData = createAndSaveDefaultTestEntityKey();
        SystemSpecData system = entityData.getPartition().getSystem();
        EntitySchemaData schema = entityData.getEntityHistories().first().getSchema();
        return partitionResourceData(system, schema, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1);
    }

    protected PartitionResourceHistoryData partitionResourceInformationData(PartitionResourceData partitionResource,
                                                                            String partitionInformation, String compactionType, String storageType,
                                                                            boolean isFixedSettings, Instant startTime, Instant endTime, long recVersion) {
        PartitionResourceHistoryData prid = new PartitionResourceHistoryData();
        prid.setPartitionResource(partitionResource);
        prid.setInformation(partitionInformation);
        prid.setCompactionType(compactionType);
        prid.setStorageType(storageType);
        prid.setFixedSettings(isFixedSettings);
        prid.setValidFromStamp(startTime);
        prid.setValidToStamp(endTime);
        prid.setRecVersion(recVersion);
        return partitionResourceHistoryRepository.save(prid);
    }

    protected PartitionData partitionData(SystemSpecData system, Map<String, Object> partitionKeyValues,
            Schema schema) {
        PartitionData partition = createPartition(system, partitionKeyValues, schema);
        return partitionRepository.save(partition);
    }

    protected VariableData variableData(String name, SystemSpecData system, EntityData entity) {
        VariableData variable = new VariableData();
        variable.setVariableName(name);
        variable.setSystem(system);
        variable.setDescription("desc");
        variable.setDeclaredType(VariableDeclaredType.MATRIX_NUMERIC);

        VariableConfigData configData = new VariableConfigData();
        configData.setVariable(variable);
        configData.setFieldName("field");
        configData.setEntity(entity);
        variable.addConfig(configData);
        return variableRepository.save(variable);
    }

    protected EntityData entityData(SystemSpecData system) {
        final EntityData entityData = new EntityData();
        entityData.setKeyValues(ENTITY_KEY_VALUES_JSON);
        PartitionData partitionData = new PartitionData();
        partitionData.setSystem(system);
        partitionData.setKeyValues(PARTITION_KEY_VALUES_JSON_1);
        entityData.setPartition(partitionRepository.save(partitionData));
        return entityRepository.save(entityData);
    }

    public static String kerberosNameFrom(String username, String realm) {
        return String.format("%s@%s", username, realm);
    }

    protected void authenticate(String user) {
        WebAuthenticationDetails details = mock(WebAuthenticationDetails.class);
        when(details.getRemoteAddress()).thenReturn("remoteAddress");

        Authentication auth = mock(Authentication.class);
        when(auth.getPrincipal()).thenReturn(
                org.springframework.security.core.userdetails.User.builder().username(user).password(EMPTY_PASSWORD)
                        .authorities(Collections.emptySet()).build());
        when(auth.getDetails()).thenReturn(details);

        SecurityContextHolder.getContext().setAuthentication(auth);
    }

    // set Authentication with a non-persisted user (persisting requires authentication to be in place already)
    public void setAuthentication() {
        authenticate(TEST_USER_NAME);
    }

    protected void unauthenticate() {
        SecurityContextHolder.getContext().setAuthentication(null);
    }
}
