/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.rest;

import cern.nxcals.service.domain.EntitySchemaData;
import cern.nxcals.service.repository.SchemaRepository;
import com.google.common.collect.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static cern.nxcals.common.web.Endpoints.SCHEMAS_FIND_ALL;
import static cern.nxcals.service.rest.DomainTestConstants.SCHEMA;
import static cern.nxcals.service.rest.DomainTestConstants.SCHEMA_ID;
import static cern.nxcals.service.rest.DomainTestConstants.SCHEMA_VALUE;
import static cern.nxcals.service.rest.DomainTestConstants.TEST_MESSAGE;
import static cern.nxcals.service.rest.GlobalControllerExceptionHandler.INTERNAL_ERROR_FORMAT;
import static java.lang.String.format;
import static java.util.Collections.emptyList;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
public class EntitySchemaControllerTest {

    @Mock
    private SchemaRepository schemaRepository;

    @InjectMocks
    private EntitySchemaController schemaController;

    private MockMvc mockMvc;

    @Mock
    private EntitySchemaData schema;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(schemaController)
                .setControllerAdvice(new GlobalControllerExceptionHandler())
                .build();
    }

    @Test
    public void shouldReturnSetForQuery() throws Exception {
        when(schema.toEntitySchema()).thenReturn(SCHEMA);
        when(schemaRepository.queryAll("query", EntitySchemaData.class)).thenReturn(Lists.newArrayList(schema));

        mockMvc.perform(post(SCHEMAS_FIND_ALL)
                .contentType(MediaType.TEXT_PLAIN_VALUE)
                .content("query"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(SCHEMA_ID))
                .andExpect(jsonPath("$[0].schemaJson").value(SCHEMA_VALUE));
    }

    @Test
    public void shouldReturnEmptySetWhenQueryFindsNothing() throws Exception {
        when(schemaRepository.queryAll("query", EntitySchemaData.class)).thenReturn(emptyList());

        mockMvc.perform(post(SCHEMAS_FIND_ALL)
                .contentType(MediaType.TEXT_PLAIN_VALUE)
                .content("query"))
                .andExpect(status().isOk())
                .andExpect(content().string("[]"));
    }

    @Test
    public void shouldGet500WhenFindByIdFailsUnexpectedly() throws Exception {
        RuntimeException runtimeException = new RuntimeException(TEST_MESSAGE);
        when(schemaRepository.queryAll("query", EntitySchemaData.class)).thenThrow(runtimeException);
        mockMvc.perform(post(SCHEMAS_FIND_ALL)
                .contentType(MediaType.TEXT_PLAIN_VALUE)
                .content("query"))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("message").value(format(INTERNAL_ERROR_FORMAT, TEST_MESSAGE)));
    }
}
