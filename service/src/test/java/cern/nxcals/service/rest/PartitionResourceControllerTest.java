package cern.nxcals.service.rest;

import cern.nxcals.api.domain.PartitionResource;
import cern.nxcals.common.utils.GlobalObjectMapperProvider;
import cern.nxcals.common.utils.ReflectionUtils;
import cern.nxcals.service.domain.EntitySchemaData;
import cern.nxcals.service.domain.PartitionData;
import cern.nxcals.service.domain.PartitionResourceData;
import cern.nxcals.service.domain.SystemSpecData;
import cern.nxcals.service.internal.PartitionResourceService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static cern.nxcals.common.web.Endpoints.PARTITION_RESOURCES;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
public class PartitionResourceControllerTest {
    private ObjectMapper mapper = GlobalObjectMapperProvider.get();

    private MockMvc mockMvc;

    private final SystemSpecData system = system();
    @Mock
    private PartitionData partition;
    private final EntitySchemaData schema = schema();

    @Mock
    private PartitionResourceService service;

    @InjectMocks
    private PartitionResourceController controller;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(controller)
                .setControllerAdvice(new GlobalControllerExceptionHandler())
                .build();
    }

    // create
    @Test
    public void shouldCreatePartitionResource() throws Exception {
        when(partition.toPartition()).thenReturn(PARTITION);

        PartitionResource partitionResourceToCreate = partitionResource();
        PartitionResourceData partitionResourceCreated = partitionResourceData();

        when(service.create(partitionResourceToCreate)).thenReturn(partitionResourceCreated);

        mockMvc.perform(post(PARTITION_RESOURCES)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(partitionResourceToCreate)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").value(42L))
                .andExpect(jsonPath("systemSpec.id").value(system.getId()))
                .andExpect(jsonPath("systemSpec.name").value(system.getName()));
    }

    // update
    @Test
    public void shouldUpdatePartitionResource() throws Exception {
        when(partition.toPartition()).thenReturn(PARTITION);

        PartitionResource partitionResourceToCreate = partitionResource();
        PartitionResourceData partitionResourceCreated = partitionResourceData();

        when(service.update(partitionResourceToCreate)).thenReturn(partitionResourceCreated);

        mockMvc.perform(put(PARTITION_RESOURCES)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(partitionResourceToCreate)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").value(42L))
                .andExpect(jsonPath("systemSpec.id").value(system.getId()))
                .andExpect(jsonPath("systemSpec.name").value(system.getName()));
    }

    // delete
    @Test
    public void shouldDeletePartitionResource() throws Exception {
        long partitionResourceId = 1234L;
        doNothing().when(service).delete(partitionResourceId);
        mockMvc.perform(delete(PARTITION_RESOURCES + "/" + partitionResourceId))
                .andExpect(status().isOk());
        //                .andDo(print());
    }

    private PartitionResourceData partitionResourceData() {
        PartitionResourceData prd = new PartitionResourceData();
        prd.setId(42L);
        prd.setSystem(system);
        prd.setPartition(partition);
        prd.setSchema(schema);
        prd.setRecVersion(2L);
        return prd;
    }

    private PartitionResource partitionResource() {
        return ReflectionUtils.builderInstance(PartitionResource.InnerBuilder.class)
                .id(42L)
                .systemSpec(system.toSystemSpec())
                .partition(partition.toPartition())
                .schema(schema.toEntitySchema())
                .recVersion(2L)
                .build();
    }

    private EntitySchemaData schema() {
        EntitySchemaData schema = new EntitySchemaData();
        schema.setId(24L);
        schema.setContent("content");
        schema.setContentHash("hash content");
        schema.setRecVersion(63L);
        return schema;
    }

    private SystemSpecData system() {
        SystemSpecData systemSpecData = new SystemSpecData();
        systemSpecData.setId(10L);
        systemSpecData.setName("SYSTEM_NAME");
        systemSpecData.setEntityKeyDefs("EKD");
        systemSpecData.setPartitionKeyDefs("PKD");
        systemSpecData.setTimeKeyDefs("TKD");
        return systemSpecData;
    }
}
