package cern.nxcals.service.rest;

import cern.nxcals.api.domain.HierarchyVariablesChangelog;
import cern.nxcals.api.extraction.metadata.queries.HierarchyVariablesChangelogs;
import cern.nxcals.service.domain.HierarchyVariablesChangelogData;
import cern.nxcals.service.internal.HierarchyVariablesChangelogService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Collections;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static cern.nxcals.common.web.Endpoints.HIERARCHY_VARIABLES_CHANGELOGS_FIND_ALL;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
public class HierarchyVariablesChangelogControllerTest {

    private static final long HIERARCHY_ID_1 = 1;
    private static final HierarchyVariablesChangelog CHANGELOG_1 = HierarchyVariablesChangelog.builder().newHierarchyId(
            HIERARCHY_ID_1).build();
    @Mock
    private HierarchyVariablesChangelogService hierarchyVariablesChangelogService;

    @InjectMocks
    private HierarchyVariablesChangelogController hierarchyVariablesChangelogController;

    private MockMvc mockMvc;

    @Mock
    private HierarchyVariablesChangelogData hierarchyVariablesChangelogData1;

    @BeforeEach
    public void setUp() {
        when(hierarchyVariablesChangelogData1.toHierarchyVariablesChangelog()).thenReturn(CHANGELOG_1);
        mockMvc = MockMvcBuilders.standaloneSetup(hierarchyVariablesChangelogController)
                .setControllerAdvice(new GlobalControllerExceptionHandler()).build();
    }

    @Test
    public void shouldReturnCollectionOfChangelogs() throws Exception {
        String rsql = toRSQL(HierarchyVariablesChangelogs.suchThat().newHierarchyId().eq(HIERARCHY_ID_1));
        when(hierarchyVariablesChangelogService.findAll(argThat(arg -> arg.equals(rsql))))
                .thenReturn(Collections.singletonList(hierarchyVariablesChangelogData1));

        mockMvc.perform(post(HIERARCHY_VARIABLES_CHANGELOGS_FIND_ALL).contentType(MediaType.TEXT_PLAIN_VALUE)
                .content(rsql))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].newHierarchyId").value(HIERARCHY_ID_1));
    }

}
