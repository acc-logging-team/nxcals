package cern.nxcals.service.rest;

import cern.nxcals.api.domain.HierarchyChangelog;
import cern.nxcals.api.extraction.metadata.queries.HierarchyChangelogs;
import cern.nxcals.service.domain.HierarchyChangelogData;
import cern.nxcals.service.internal.HierarchyChangelogService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Collections;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static cern.nxcals.common.web.Endpoints.HIERARCHIES_CHANGELOGS_FIND_ALL;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
public class HierarchyChangelogControllerTest {

    private static final long HIERARCHY_ID_1 = 1;
    private static final long HIERARCHY_ID_2 = 2;
    private static final HierarchyChangelog HIERARCHY_CHANGELOG_1 = HierarchyChangelog.builder().hierarchyId(
            HIERARCHY_ID_1).build();
    private static final HierarchyChangelog HIERARCHY_CHANGELOG_2 = HierarchyChangelog.builder().hierarchyId(
            HIERARCHY_ID_2).build();
    @Mock
    private HierarchyChangelogService hierarchyChangelogService;

    @InjectMocks
    private HierarchyChangelogController hierarchyChangelogController;

    private MockMvc mockMvc;

    @Mock
    private HierarchyChangelogData hierarchyChangelogData1, hierarchyChangelogData2;

    @BeforeEach
    public void setUp() {
        when(hierarchyChangelogData1.toHierarchyChangelog()).thenReturn(HIERARCHY_CHANGELOG_1);
        when(hierarchyChangelogData2.toHierarchyChangelog()).thenReturn(HIERARCHY_CHANGELOG_2);
        mockMvc = MockMvcBuilders.standaloneSetup(hierarchyChangelogController)
                .setControllerAdvice(new GlobalControllerExceptionHandler()).build();
    }

    @Test
    public void shouldReturnCollectionOfVariableData() throws Exception {
        String rsql = toRSQL(HierarchyChangelogs.suchThat().hierarchyId().eq(HIERARCHY_ID_1));
        when(hierarchyChangelogService.findAll(argThat(arg -> arg.equals(rsql))))
                .thenReturn(Collections.singletonList(hierarchyChangelogData1));

        mockMvc.perform(post(HIERARCHIES_CHANGELOGS_FIND_ALL).contentType(MediaType.TEXT_PLAIN_VALUE)
                .content(rsql))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].hierarchyId").value(HIERARCHY_ID_1));
    }

}
