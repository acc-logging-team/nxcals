/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.rest;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.common.domain.FindOrCreateEntityRequest;
import cern.nxcals.common.utils.ReflectionUtils;
import cern.nxcals.service.BaseTest;
import cern.nxcals.service.domain.EntityData;
import cern.nxcals.service.domain.PartitionData;
import cern.nxcals.service.domain.SystemSpecData;
import cern.nxcals.service.domain.VariableData;
import cern.nxcals.service.internal.EntityService;
import cern.nxcals.service.internal.VariableService;
import cern.nxcals.service.repository.EntityRepository;
import cern.nxcals.service.repository.SystemSpecRepository;
import org.assertj.core.util.Lists;
import org.hamcrest.CustomMatcher;
import org.hamcrest.Matcher;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static cern.nxcals.common.web.Endpoints.ENTITIES;
import static cern.nxcals.common.web.Endpoints.ENTITY_EXTEND_FIRST_HISTORY;
import static cern.nxcals.common.web.Endpoints.ENTITY_UPDATE;
import static cern.nxcals.common.web.Endpoints.VARIABLES;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_ID;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_KEY_VALUES_1;
import static cern.nxcals.service.rest.DomainTestConstants.OBJECT_MAPPER;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_1;
import static cern.nxcals.service.rest.DomainTestConstants.SYSTEM_ID;
import static cern.nxcals.service.rest.DomainTestConstants.TIMESTAMP;
import static cern.nxcals.service.rest.DomainTestConstants.VARIABLE_DATA;
import static cern.nxcals.service.rest.EntityControllerTest.ENTITY;
import static cern.nxcals.service.rest.GlobalControllerExceptionHandler.FORBIDDEN_ERROR_FORMAT;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_SCHEMA_1;
import static java.lang.String.format;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Test suite that checks if the endpoints defined in {@link EntityController} follow the desired contract.
 */
@WebAppConfiguration
@Transactional(transactionManager = "jpaTransactionManager")
public class SecurityTest extends BaseTest {

    private static final SystemSpec SYSTEM2 = createSystem();

    private static SystemSpec createSystem() {
        return ReflectionUtils.builderInstance(SystemSpec.InnerBuilder.class).id(20)
                .name(SYSTEM_NAME2)
                .entityKeyDefinitions(ENTITY_SCHEMA_1.toString())
                .partitionKeyDefinitions(ENTITY_SCHEMA_1.toString())
                .timeKeyDefinitions(ENTITY_SCHEMA_1.toString())
                .recordVersionKeyDefinitions(ENTITY_SCHEMA_1.toString())
                .build();
    }

    static final Entity ENTITY2 = createEntity();

    private static Entity createEntity() {
        return ReflectionUtils.builderInstance(Entity.InnerBuilder.class).id(20)
                .entityKeyValues(ENTITY_KEY_VALUES_1)
                .systemSpec(SYSTEM2)
                .partition(PARTITION)
                .entityHistory(Collections.emptySortedSet())
                .unlock()
                .recVersion(1)
                .build();
    }

    @MockBean
    private SystemSpecRepository systemRepository;

    @MockBean
    private EntityRepository entityRepository;

    @MockBean
    private EntityService internalEntityService;

    @MockBean
    private VariableService internalVariableService;

    @Mock
    private EntityData entity;

    @Mock
    private PartitionData partition;

    @Mock
    private SystemSpecData system;

    @Mock
    private VariableData variable;

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private final static Matcher<String> ERROR_MESSAGE_CONTENT = new CustomMatcher<>("message format matcher") {
        @Override
        public boolean matches(Object item) {
            return ((String) item).contains(format(FORBIDDEN_ERROR_FORMAT, "Access Denied"));
        }
    };

    @BeforeEach
    public void setup() {
        when(entity.getId()).thenReturn(ENTITY_ID);

        when(internalEntityService
                .findOrCreateEntityFor(isA(Long.class), isA(Map.class), isA(Map.class), isA(String.class),
                        isA(Long.class))).thenReturn(entity);

        when(internalEntityService.extendEntityFirstHistoryDataFor(isA(Long.class), isA(String.class), isA(Long.class)))
                .thenReturn(entity);

        when(internalVariableService.create(any(Variable.class))).thenReturn(variable);
        when(internalVariableService.update(any(Variable.class))).thenReturn(variable);

        when(system.getName()).thenReturn(SYSTEM_NAME);

        when(partition.getSystem()).thenReturn(system);

        when(entity.getPartition()).thenReturn(partition);

        when(systemRepository.findById(eq(SYSTEM_ID))).thenReturn(Optional.of(system));

        when(entityRepository.findById(eq(ENTITY_ID))).thenReturn(Optional.of(entity));

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .apply(SecurityMockMvcConfigurers.springSecurity()).build();
    }

    @Test
    @WithMockUser(authorities = BaseTest.AUTHORITY)
    public void shouldFindOrCreateEntity() throws Exception {
        var request = put(ENTITIES)
                .param("systemId", String.valueOf(SYSTEM_ID))
                .param("recordTimestamp", String.valueOf(TIMESTAMP))
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsString(
                                FindOrCreateEntityRequest.builder()
                                        .entityKeyValues(ENTITY_KEY_VALUES_1)
                                        .partitionKeyValues(PARTITION_KEY_VALUES_1)
                                        .schema(ENTITY_SCHEMA_1.toString())
                                        .build()
                        )
                );

        mockMvc.perform(request)
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(authorities = "SOME_AUTHORITY")
    public void shouldNotAuthorizeToFindOrCreateEntityFor() throws Exception {
        // given
        var request = put(ENTITIES)
                .param("systemId", String.valueOf(SYSTEM_ID))
                .param("recordTimestamp", String.valueOf(TIMESTAMP))
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsString(
                                FindOrCreateEntityRequest.builder()
                                        .entityKeyValues(ENTITY_KEY_VALUES_1)
                                        .partitionKeyValues(PARTITION_KEY_VALUES_1)
                                        .schema(ENTITY_SCHEMA_1.toString())
                                        .build()
                        )
                );

        // when
        mockMvc.perform(request)
                .andExpect(status().isForbidden())
                .andExpect(content().string(ERROR_MESSAGE_CONTENT));
    }

    @Test
    @WithMockUser(authorities = BaseTest.VARIABLE_AUTHORITY)
    public void shouldCreateVariable() throws Exception {
        // given
        var request = post(VARIABLES)
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsString(VARIABLE_DATA));

        // when
        mockMvc.perform(request)
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(authorities = BaseTest.VARIABLE_AUTHORITY)
    public void shouldUpdateVariable() throws Exception {
        // given
        var request = put(VARIABLES)
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsString(VARIABLE_DATA));

        // when
        mockMvc.perform(request)
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(authorities = "SOME_AUTHORITY")
    public void shouldNotAuthorizeToCreateVariable() throws Exception {
        // given
        var request = post(VARIABLES)
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsString(VARIABLE_DATA));

        // when
        mockMvc.perform(request)
                //                .andDo(print())
                .andExpect(status().isForbidden())
                .andExpect(content().string(ERROR_MESSAGE_CONTENT));
    }

    @Test
    @WithMockUser(authorities = "SOME_AUTHORITY")
    public void shouldNotAuthorizeToUpdateVariable() throws Exception {
        // given
        var request = put(VARIABLES)
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsString(VARIABLE_DATA));

        // when
        mockMvc.perform(request)
                //                .andDo(print())
                .andExpect(status().isForbidden())
                .andExpect(content().string(ERROR_MESSAGE_CONTENT));
    }

    @Test
    @WithMockUser(authorities = { BaseTest.AUTHORITY, BaseTest.AUTHORITY2 })
    public void shouldAuthorizeToUpdateEntities() throws Exception {
        // given
        List<EntityData> entityList = Collections.singletonList(entity);
        List<Entity> entityDataList = Lists.newArrayList();
        entityDataList.add(ENTITY);
        entityDataList.add(ENTITY2);

        when(internalEntityService.updateEntities(eq(entityDataList))).thenReturn(entityList);

        var request = put(ENTITY_UPDATE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsString(entityDataList));

        // when
        mockMvc.perform(request)
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(authorities = { BaseTest.AUTHORITY, BaseTest.AUTHORITY2 })
    public void shouldAuthorizeToUpdateEntitiesWhenMoreAuthoritiesThanNecessary() throws Exception {
        // given
        List<EntityData> entityList = Collections.singletonList(entity);
        List<Entity> entityDataList = Lists.newArrayList();
        entityDataList.add(ENTITY);

        when(internalEntityService.updateEntities(eq(entityDataList))).thenReturn(entityList);

        var request = put(ENTITY_UPDATE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsString(entityDataList));
        // when
        mockMvc.perform(request)
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(authorities = { BaseTest.AUTHORITY })
    public void shouldNotAuthorizeToUpdateEntities() throws Exception {
        // given
        List<EntityData> entityList = Collections.singletonList(entity);
        List<Entity> entityDataList = Lists.newArrayList();
        entityDataList.add(ENTITY);
        entityDataList.add(ENTITY2);

        when(internalEntityService.updateEntities(eq(entityDataList))).thenReturn(entityList);

        var request = put(ENTITY_UPDATE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(OBJECT_MAPPER.writeValueAsString(entityDataList));
        // when
        mockMvc.perform(request)
                .andExpect(content().string(ERROR_MESSAGE_CONTENT))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(authorities = BaseTest.AUTHORITY)
    public void shouldExtendEntityFirstHistoryDataFor() throws Exception {
        // given
        var request = put(ENTITY_EXTEND_FIRST_HISTORY)
                .param("entityId", String.valueOf(ENTITY_ID))
                .param("from", String.valueOf(TIMESTAMP))
                .contentType(MediaType.APPLICATION_JSON)
                .content(ENTITY_SCHEMA_1.toString());

        // when
        mockMvc.perform(request)
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(authorities = "SOME_AUTHORITY")
    public void shouldNotAuthorizeToExtendEntityFirstHistoryDataFor() throws Exception {
        // given
        var request = put(ENTITY_EXTEND_FIRST_HISTORY)
                .param("entityId", String.valueOf(ENTITY_ID))
                .param("from", String.valueOf(TIMESTAMP))
                .contentType(MediaType.APPLICATION_JSON)
                .content(ENTITY_SCHEMA_1.toString());

        // when
        this.mockMvc.perform(request)
                .andExpect(status().isForbidden())
                .andExpect(content().string(ERROR_MESSAGE_CONTENT));
    }
}