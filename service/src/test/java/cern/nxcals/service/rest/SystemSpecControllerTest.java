/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.rest;

import cern.nxcals.service.domain.SystemSpecData;
import cern.nxcals.service.repository.SystemSpecRepository;
import com.google.common.collect.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.HashSet;
import java.util.Set;

import static cern.nxcals.common.web.Endpoints.SYSTEMS_FIND_ALL;
import static cern.nxcals.service.rest.DomainTestConstants.SYSTEM_ID;
import static cern.nxcals.service.rest.DomainTestConstants.SYSTEM_NAME;
import static cern.nxcals.service.rest.DomainTestConstants.SYSTEM_SPEC;
import static cern.nxcals.service.rest.DomainTestConstants.SYSTEM_SPEC_2;
import static cern.nxcals.service.rest.DomainTestConstants.TEST_MESSAGE;
import static cern.nxcals.service.rest.GlobalControllerExceptionHandler.INTERNAL_ERROR_FORMAT;
import static java.lang.String.format;
import static java.util.Collections.emptyList;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
public class SystemSpecControllerTest {

    @Mock
    private SystemSpecRepository systemRepository;

    @InjectMocks
    private SystemSpecController systemSpecController;

    private MockMvc mockMvc;

    @Mock
    private SystemSpecData system, system2;


    private Set<SystemSpecData> systems;

    @BeforeEach
    public void setUp() {
        when(system.toSystemSpec()).thenReturn(SYSTEM_SPEC);
        when(system2.toSystemSpec()).thenReturn(SYSTEM_SPEC_2);
        mockMvc = MockMvcBuilders.standaloneSetup(systemSpecController)
                .setControllerAdvice(new GlobalControllerExceptionHandler())
                .build();
        systems = new HashSet<>();
        systems.add(system);
        systems.add(system2);

    }

    @Test
    public void shouldReturnSetForQuery() throws Exception {
        when(systemRepository.queryAll("query", SystemSpecData.class)).thenReturn(Lists.newArrayList(system));

        mockMvc.perform(post(SYSTEMS_FIND_ALL)
                .contentType(MediaType.TEXT_PLAIN_VALUE)
                .content("query"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(SYSTEM_ID))
                .andExpect(jsonPath("$[0].name").value(SYSTEM_NAME));
    }


    @Test
    public void shouldReturnEmptySetWhenQueryFindsNothing() throws Exception {
        when(systemRepository.queryAll("query", SystemSpecData.class)).thenReturn(emptyList());

        mockMvc.perform(post(SYSTEMS_FIND_ALL)
                .contentType(MediaType.TEXT_PLAIN_VALUE)
                .content("query"))
                .andExpect(status().isOk())
                .andExpect(content().string("[]"));
    }

    @Test
    public void shouldGet500WhenFindsByNameFailsUnexpectedly() throws Exception {
        RuntimeException runtimeException = new RuntimeException(TEST_MESSAGE);
        when(systemRepository.queryAll("query", SystemSpecData.class)).thenThrow(runtimeException);

        mockMvc.perform(post(SYSTEMS_FIND_ALL)
                .contentType(MediaType.TEXT_PLAIN_VALUE)
                .content("query"))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("message").value(format(INTERNAL_ERROR_FORMAT, TEST_MESSAGE)));
    }


}
