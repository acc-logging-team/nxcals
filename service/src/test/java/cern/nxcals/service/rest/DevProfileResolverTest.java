/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.rest;

import cern.nxcals.service.domain.EntityData;
import cern.nxcals.service.domain.SystemSpecData;
import cern.nxcals.service.internal.EntityService;
import cern.nxcals.service.repository.SystemSpecRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Optional;

import static cern.nxcals.common.web.Endpoints.ENTITIES;
import static cern.nxcals.service.BaseTest.TEST_NAME;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_ID;
import static cern.nxcals.service.rest.DomainTestConstants.FIND_OR_CREATE_ENTITY_REQUEST_JSON;
import static cern.nxcals.service.rest.DomainTestConstants.SYSTEM_ID;
import static cern.nxcals.service.rest.DomainTestConstants.TIMESTAMP;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Disabled("This test should be run explicitly")
@ExtendWith(MockitoExtension.class)
@WebAppConfiguration
@ActiveProfiles(value = "dev")
public class DevProfileResolverTest {

    @MockBean
    private SystemSpecRepository systemRepository;

    @MockBean
    private EntityService internalEntityService;

    @Mock
    private EntityData entity;

    @Mock
    private SystemSpecData system;

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @BeforeEach
    public void setup() {
        when(entity.getId()).thenReturn(ENTITY_ID);

        when(internalEntityService.findOrCreateEntityFor(isA(Long.class), anyMap(),
                anyMap(), isA(String.class), isA(Long.class))).thenReturn(entity);

        when(system.getName()).thenReturn(TEST_NAME);

        when(systemRepository.findById(eq(SYSTEM_ID))).thenReturn(Optional.of(system));

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
    }

    @Test
    @WithMockUser(authorities = "ALL")
    public void shouldAuthorizeWithDevProfileAndAllAuthority() throws Exception {
        mockMvc.perform(put(ENTITIES)
                .param("systemId", String.valueOf(SYSTEM_ID))
                .param("recordTimestamp", String.valueOf(TIMESTAMP))
                .contentType(MediaType.APPLICATION_JSON)
                .content(FIND_OR_CREATE_ENTITY_REQUEST_JSON))
                .andExpect(status().isOk());
    }
}

