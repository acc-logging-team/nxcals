/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.rest;

import cern.nxcals.api.extraction.metadata.queries.Variables;
import cern.nxcals.service.domain.VariableData;
import cern.nxcals.service.internal.VariableService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Collections;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static cern.nxcals.common.web.Endpoints.VARIABLES_FIND_ALL;
import static cern.nxcals.service.rest.DomainTestConstants.SYSTEM_NAME;
import static cern.nxcals.service.rest.DomainTestConstants.VARIABLE_DATA;
import static cern.nxcals.service.rest.DomainTestConstants.VARIABLE_DATA2;
import static cern.nxcals.service.rest.DomainTestConstants.VARIABLE_DESCRIPTION;
import static cern.nxcals.service.rest.DomainTestConstants.VARIABLE_NAME;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
public class VariableControllerTest {

    @Mock
    private VariableService variableService;

    @InjectMocks
    private VariableController variableController;

    private MockMvc mockMvc;

    @Mock
    private VariableData variable, variable2;

    @BeforeEach
    public void setUp() {
        when(variable.toVariable()).thenReturn(VARIABLE_DATA);
        when(variable2.toVariable()).thenReturn(VARIABLE_DATA2);
        mockMvc = MockMvcBuilders.standaloneSetup(variableController)
                .setControllerAdvice(new GlobalControllerExceptionHandler()).build();
    }


    @Test
    public void shouldReturnCollectionOfVariableData() throws Exception {
        String rsql = toRSQL(Variables.suchThat().systemName().eq(SYSTEM_NAME).and().variableName().eq(VARIABLE_NAME));
        when(variableService.findAll(argThat(arg -> arg.equals(rsql))))
                .thenReturn(Collections.singletonList(variable));

        mockMvc.perform(post(VARIABLES_FIND_ALL).contentType(MediaType.TEXT_PLAIN_VALUE)
                .content(rsql))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].variableName").value(VARIABLE_NAME))
                .andExpect(jsonPath("$[0].description").value(VARIABLE_DESCRIPTION));
    }

}
