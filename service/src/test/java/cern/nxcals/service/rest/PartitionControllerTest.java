/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.rest;

import cern.nxcals.service.domain.PartitionData;
import cern.nxcals.service.internal.PartitionService;
import com.google.common.collect.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Collections;

import static cern.nxcals.common.web.Endpoints.PARTITIONS;
import static cern.nxcals.common.web.Endpoints.PARTITIONS_FIND_ALL;
import static cern.nxcals.service.rest.DomainTestConstants.OBJECT_MAPPER;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_ID;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_1;
import static cern.nxcals.service.rest.DomainTestConstants.TEST_MESSAGE;
import static cern.nxcals.service.rest.DomainTestConstants.UPDATED_PARTITION;
import static cern.nxcals.service.rest.GlobalControllerExceptionHandler.INTERNAL_ERROR_FORMAT;
import static java.lang.String.format;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
public class PartitionControllerTest {

    private static final String QUERY = "query";

    @Mock
    private PartitionService partitionService;

    @InjectMocks
    private PartitionController partitionController;

    private MockMvc mockMvc;

    @Mock
    private PartitionData partition;

    @Mock
    private PartitionData updatedPartition;

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(partitionController)
                .setControllerAdvice(new GlobalControllerExceptionHandler())
                .build();
    }

    @Test
    public void shouldGetPartitionBySystemIdAndKeyValues() throws Exception {
        when(partition.toPartition()).thenReturn(PARTITION);
        when(partitionService.findAll(argThat(arg -> arg.equals(QUERY))))
                .thenReturn(Lists.newArrayList(partition));

        mockMvc.perform(post(PARTITIONS_FIND_ALL)
                        .contentType(MediaType.TEXT_PLAIN_VALUE)
                        .content(QUERY))
                .andExpect(status().isOk())
                //                .andDo(print())
                .andExpect(jsonPath("$[0].id").value(PARTITION_ID))
                .andExpect(jsonPath("$[0].keyValues").value(is(PARTITION_KEY_VALUES_1)));
    }

    @Test
    public void shouldUpdatePartition() throws Exception {
        when(updatedPartition.toPartition()).thenReturn(UPDATED_PARTITION);
        when(partitionService.update(UPDATED_PARTITION))
                .thenReturn(updatedPartition);

        mockMvc.perform(put(PARTITIONS)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(OBJECT_MAPPER.writeValueAsString(UPDATED_PARTITION)))
                .andExpect(status().isOk())
                //                .andDo(print())
                .andExpect(jsonPath("id").value(PARTITION_ID))
                .andExpect(jsonPath("keyValues").value(is(PARTITION_KEY_VALUES_1)))
                .andExpect(jsonPath("properties").exists());
    }

    @Test
    public void shouldCreatePartition() throws Exception {
        when(partition.toPartition()).thenReturn(PARTITION);
        when(partitionService.create(PARTITION))
                .thenReturn(partition);

        mockMvc.perform(post(PARTITIONS)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(OBJECT_MAPPER.writeValueAsString(PARTITION)))
                .andExpect(status().isOk())
                //                .andDo(print())
                .andExpect(jsonPath("id").value(PARTITION_ID))
                .andExpect(jsonPath("keyValues").value(is(PARTITION_KEY_VALUES_1)))
                .andExpect(jsonPath("properties").exists());
    }

    @Test
    public void shouldReturnEmptyListWhenFindsNothing() throws Exception {
        when(partitionService.findAll(argThat(arg -> arg.equals(QUERY))))
                .thenReturn(Collections.emptyList());

        mockMvc.perform(post(PARTITIONS_FIND_ALL)
                        .contentType(MediaType.TEXT_PLAIN_VALUE)
                        .content(QUERY))
                //                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("[]")));
    }

    @Test
    public void shouldGet500WhenFindBySystemIdFailsUnexpectedly() throws Exception {
        RuntimeException runtimeException = new RuntimeException(TEST_MESSAGE);

        when(partitionService.findAll(argThat(arg -> arg.equals(QUERY))))
                .thenThrow(runtimeException);

        mockMvc.perform(post(PARTITIONS_FIND_ALL)
                        .contentType(MediaType.TEXT_PLAIN_VALUE)
                        .content(QUERY))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("message").value(format(INTERNAL_ERROR_FORMAT, TEST_MESSAGE)));
    }
}
