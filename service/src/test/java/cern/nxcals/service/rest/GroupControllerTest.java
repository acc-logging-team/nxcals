package cern.nxcals.service.rest;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.Group;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.api.domain.VariableDeclaredType;
import cern.nxcals.api.domain.Visibility;
import cern.nxcals.common.utils.GlobalObjectMapperProvider;
import cern.nxcals.common.utils.ReflectionUtils;
import cern.nxcals.common.web.Endpoints;
import cern.nxcals.service.domain.EntityData;
import cern.nxcals.service.domain.GroupData;
import cern.nxcals.service.domain.PartitionData;
import cern.nxcals.service.domain.PartitionPropertiesData;
import cern.nxcals.service.domain.SystemSpecData;
import cern.nxcals.service.domain.VariableConfigData;
import cern.nxcals.service.domain.VariableData;
import cern.nxcals.service.domain.VisibilityData;
import cern.nxcals.service.internal.SecureGroupService;
import cern.nxcals.service.rest.exceptions.AccessDeniedRuntimeException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Sets;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.Instant;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static cern.nxcals.common.web.Endpoints.ENTITIES;
import static cern.nxcals.common.web.Endpoints.GROUPS;
import static cern.nxcals.common.web.Endpoints.IDS_SUFFIX;
import static cern.nxcals.common.web.Endpoints.VARIABLES;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_KEY_VALUES_1;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_KEY_VALUES_JSON;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_JSON_1;
import static cern.nxcals.service.rest.DomainTestConstants.SYSTEM_SPEC;
import static java.util.Collections.emptySortedSet;
import static org.hamcrest.Matchers.aMapWithSize;
import static org.hamcrest.Matchers.anEmptyMap;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasKey;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
public class GroupControllerTest {
    private final ObjectMapper objectMapper = GlobalObjectMapperProvider.get();
    private MockMvc mockMvc;

    @Mock
    private SecureGroupService service;

    @InjectMocks
    private GroupController controller;

    private final SystemSpecData system = system();

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(controller)
                .setMessageConverters(new MappingJackson2HttpMessageConverter(objectMapper))
                .setControllerAdvice(new GlobalControllerExceptionHandler()).build();
    }

    @Test
    public void shouldCreateGroup() throws Exception {
        Group group = group("A");
        GroupData groupData = groupData("A");

        when(service.create(group)).thenReturn(groupData);

        mockMvc.perform(post(Endpoints.GROUPS).contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(group))).andExpect(status().isOk())
                .andExpect(jsonPath("id").value(groupData.getId()))
                .andExpect(jsonPath("name").value(groupData.getName()))
                .andExpect(jsonPath("description").value(groupData.getDescription()))
                .andExpect(jsonPath("visibility").value(groupData.getVisibility().toVisibility().toString()))
                .andExpect(jsonPath("label").value(groupData.getLabel()))
                .andExpect(jsonPath("owner").value(groupData.getOwner()))
                .andExpect(jsonPath("systemSpec.id").value(groupData.getSystem().getId()))
                .andExpect(jsonPath("systemSpec.name").value(groupData.getSystem().getName()))
                .andExpect(jsonPath("properties").isEmpty())
                .andExpect(jsonPath("recVersion").value(groupData.getRecVersion()));
        //                .andDo(print());
    }

    @Test
    public void shouldUpdateGroup() throws Exception {
        Group group = group("A");
        GroupData groupData = groupData("B");

        when(service.update(group)).thenReturn(groupData);

        mockMvc.perform(put(Endpoints.GROUPS).contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(group))).andExpect(status().isOk())
                .andExpect(jsonPath("id").value(groupData.getId()))
                .andExpect(jsonPath("name").value(groupData.getName()))
                .andExpect(jsonPath("description").value(groupData.getDescription()))
                .andExpect(jsonPath("visibility").value(groupData.getVisibility().toVisibility().toString()))
                .andExpect(jsonPath("label").value(groupData.getLabel()))
                .andExpect(jsonPath("owner").value(groupData.getOwner()))
                .andExpect(jsonPath("systemSpec.id").value(groupData.getSystem().getId()))
                .andExpect(jsonPath("systemSpec.name").value(groupData.getSystem().getName()))
                .andExpect(jsonPath("properties").isEmpty())
                .andExpect(jsonPath("recVersion").value(groupData.getRecVersion()));
        //                .andDo(print());
    }

    // test: delete

    @Test
    public void shouldPropagateExceptionOnDeleteGroupToClient() throws Exception {
        long groupId = 1234L;

        doThrow(AccessDeniedRuntimeException.notGroupOwner(groupId)).when(service).delete(groupId);

        mockMvc.perform(delete(Endpoints.GROUPS + "/" + groupId)).andExpect(status().isForbidden());
        //                .andDo(print());
    }

    @Test
    public void shouldDeleteGroup() throws Exception {
        long groupId = 1234L;

        doNothing().when(service).delete(groupId);

        mockMvc.perform(delete(Endpoints.GROUPS + "/" + groupId)).andExpect(status().isOk());
        //                .andDo(print());
    }

    // test: addVariables

    @Test
    public void shouldAddVariablesToGroup() throws Exception {
        Group group = group("A").toBuilder().property("key_1", "value_1").build();

        Map<String, Set<Long>> variables = new HashMap<>();
        variables.put(null, Sets.newHashSet(variable(1, "var-1").getId(), variable(1, "var-2").getId()));
        variables.put("A", Collections.singleton(variable(1, "var-1").getId()));

        doNothing().when(service).addVariables(group.getId(), variables);

        mockMvc.perform(
                post(GROUPS + "/" + group.getId() + VARIABLES + IDS_SUFFIX).contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(variables))).andExpect(status().isOk());
        //                .andDo(print());

        Map<String, Set<VariableData>> variableAssociations = new HashMap<>();
        variableAssociations.put(null, Sets.newHashSet(variableData(10, "var-1"), variableData(20, "var-2")));
        variableAssociations.put("A", Collections.singleton(variableData(20, "var-2")));

        when(service.getVariables(group.getId())).thenReturn(variableAssociations);

        mockMvc.perform(MockMvcRequestBuilders.get(GROUPS + "/" + group.getId() + VARIABLES)
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(jsonPath("$", aMapWithSize(2))).andExpect(jsonPath("$", hasKey("A")))
                .andExpect(jsonPath("$", hasKey("NULL_KEY")))
                .andExpect(jsonPath("$.*[*].variableName", hasItem("var-1")))
                .andExpect(jsonPath("$.*[*].variableName", hasItem("var-2")))
                .andExpect(jsonPath("$.*[*].id", hasItem(10))).andExpect(jsonPath("$.*[*].id", hasItem(20)));
        //                .andDo(print());

        verify(service, times(1)).addVariables(group.getId(), variables);
        verify(service, times(1)).getVariables(group.getId());
        verifyNoMoreInteractions(service);
    }

    // test: setVariables

    @Test
    public void shouldSetVariablesToGroup() throws Exception {
        Group group = group("A").toBuilder().property("key_1", "value_1").build();

        Map<String, Set<Long>> variables = new HashMap<>();
        variables.put(null, Sets.newHashSet(variable(1, "var-1").getId(), variable(1, "var-2").getId()));
        variables.put("A", Collections.singleton(variable(1, "var-1").getId()));

        doNothing().when(service).setVariables(group.getId(), variables);

        mockMvc.perform(
                put(GROUPS + "/" + group.getId() + VARIABLES + IDS_SUFFIX).contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(variables))).andExpect(status().isOk());
        //                .andDo(print());

        Map<String, Set<VariableData>> variableAssociations = new HashMap<>();
        variableAssociations.put(null, Sets.newHashSet(variableData(10, "var-1"), variableData(20, "var-2")));
        variableAssociations.put("A", Collections.singleton(variableData(20, "var-2")));

        when(service.getVariables(group.getId())).thenReturn(variableAssociations);

        mockMvc.perform(MockMvcRequestBuilders.get(GROUPS + "/" + group.getId() + VARIABLES)
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(jsonPath("$", aMapWithSize(2))).andExpect(jsonPath("$", hasKey("A")))
                .andExpect(jsonPath("$", hasKey("NULL_KEY")))
                .andExpect(jsonPath("$.*[*].variableName", hasItem("var-1")))
                .andExpect(jsonPath("$.*[*].variableName", hasItem("var-2")))
                .andExpect(jsonPath("$.*[*].id", hasItem(10))).andExpect(jsonPath("$.*[*].id", hasItem(20)));
        //                .andDo(print());

        verify(service, times(1)).setVariables(group.getId(), variables);
        verify(service, times(1)).getVariables(group.getId());
        verifyNoMoreInteractions(service);
    }

    // test: removeVariables

    @Test
    public void shouldRemoveVariablesFromGroup() throws Exception {
        Group group = group("A").toBuilder().property("key_1", "value_1").build();

        Map<String, Set<Long>> variables = new HashMap<>();
        variables.put(null, Sets.newHashSet(variable(1, "var-1").getId(), variable(1, "var-2").getId()));
        variables.put("A", Collections.singleton(variable(1, "var-1").getId()));

        doNothing().when(service).removeVariables(group.getId(), variables);

        mockMvc.perform(
                delete(GROUPS + "/" + group.getId() + VARIABLES + IDS_SUFFIX).contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(variables))).andExpect(status().isOk());
        //                .andDo(print());

        when(service.getVariables(group.getId())).thenReturn(Collections.emptyMap());

        mockMvc.perform(MockMvcRequestBuilders.get(GROUPS + "/" + group.getId() + VARIABLES)
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(jsonPath("$", anEmptyMap()));
        //                .andDo(print());

        verify(service, times(1)).removeVariables(group.getId(), variables);
        verify(service, times(1)).getVariables(group.getId());
        verifyNoMoreInteractions(service);
    }

    // test: addEntities

    @Test
    public void shouldAddEntitiesToGroup() throws Exception {
        Group group = group("A").toBuilder().property("key_1", "value_1").build();

        Map<String, Set<Long>> entities = new HashMap<>();
        entities.put(null, Collections.singleton(entity(10L).getId()));
        entities.put("B", Collections.singleton(entity(10L).getId()));

        doNothing().when(service).addEntities(group.getId(), entities);

        mockMvc.perform(
                post(GROUPS + "/" + group.getId() + ENTITIES + IDS_SUFFIX).contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(entities))).andExpect(status().isOk());
        //                .andDo(print());

        Map<String, Set<EntityData>> entityAssociations = new HashMap<>();
        entityAssociations.put(null, Collections.singleton(entityData()));
        entityAssociations.put("B", Collections.singleton(entityData()));

        when(service.getEntities(group.getId())).thenReturn(entityAssociations);

        mockMvc.perform(MockMvcRequestBuilders.get(GROUPS + "/" + group.getId() + ENTITIES)
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(jsonPath("$", aMapWithSize(2))).andExpect(jsonPath("$", hasKey("B")))
                .andExpect(jsonPath("$", hasKey("NULL_KEY"))).andExpect(jsonPath("$.*[*].id", hasItem(10)));
        //                .andDo(print());

        verify(service, times(1)).addEntities(group.getId(), entities);
        verify(service, times(1)).getEntities(group.getId());
        verifyNoMoreInteractions(service);
    }

    // test: setEntities

    @Test
    public void shouldSetEntitiesToGroup() throws Exception {
        Group group = group("A").toBuilder().property("key_1", "value_1").build();

        Map<String, Set<Long>> entities = new HashMap<>();
        entities.put(null, Collections.singleton(entity(10L).getId()));
        entities.put("B", Collections.singleton(entity(10L).getId()));

        doNothing().when(service).setEntities(group.getId(), entities);

        mockMvc.perform(
                put(GROUPS + "/" + group.getId() + ENTITIES + IDS_SUFFIX).contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(entities))).andExpect(status().isOk());
        //                .andDo(print());

        Map<String, Set<EntityData>> entityAssociations = new HashMap<>();
        entityAssociations.put(null, Collections.singleton(entityData()));
        entityAssociations.put("B", Collections.singleton(entityData()));

        when(service.getEntities(group.getId())).thenReturn(entityAssociations);

        mockMvc.perform(MockMvcRequestBuilders.get(GROUPS + "/" + group.getId() + ENTITIES)
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(jsonPath("$", aMapWithSize(2))).andExpect(jsonPath("$", hasKey("B")))
                .andExpect(jsonPath("$", hasKey("NULL_KEY"))).andExpect(jsonPath("$.*[*].id", hasItem(10)));
        //                .andDo(print());

        verify(service, times(1)).setEntities(group.getId(), entities);
        verify(service, times(1)).getEntities(group.getId());
        verifyNoMoreInteractions(service);
    }

    // test: removeVariables

    @Test
    public void shouldRemoveEntitiesFromGroup() throws Exception {
        Group group = group("A").toBuilder().property("key_1", "value_1").build();

        Map<String, Set<Long>> entities = new HashMap<>();
        entities.put(null, Collections.singleton(entity(10L).getId()));
        entities.put("B", Collections.singleton(entity(10L).getId()));

        doNothing().when(service).removeEntities(group.getId(), entities);

        mockMvc.perform(
                delete(GROUPS + "/" + group.getId() + ENTITIES + IDS_SUFFIX).contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(entities))).andExpect(status().isOk());
        //                .andDo(print());

        when(service.getEntities(group.getId())).thenReturn(Collections.emptyMap());

        mockMvc.perform(MockMvcRequestBuilders.get(GROUPS + "/" + group.getId() + ENTITIES)
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
                .andExpect(jsonPath("$", anEmptyMap()));
        //                .andDo(print());

        verify(service, times(1)).removeEntities(group.getId(), entities);
        verify(service, times(1)).getEntities(group.getId());
        verifyNoMoreInteractions(service);
    }

    // helper methods

    private Group group(String name) {
        return ReflectionUtils.builderInstance(Group.InnerBuilder.class).id(10).recVersion(20)
                .label("my-snapshot-label").name(name).systemSpec(system.toSystemSpec()).description("DESC")
                .visibility(Visibility.PUBLIC).build();
    }

    private GroupData groupData(String name) {
        GroupData group = new GroupData();
        group.setId(10L);
        group.setRecVersion(20L);
        group.setName(name);
        group.setSystem(system);
        group.setDescription("DESC");
        group.setVisibility(VisibilityData.PROTECTED);
        group.setOwner("owner");
        group.setLabel("my-snapshot-label");
        return group;
    }

    private Variable variable(long entityId, String name) {
        VariableConfig config = VariableConfig.builder().entityId(entityId).fieldName("field").build();
        return Variable.builder().variableName(name).configs(ImmutableSortedSet.of(config))
                .systemSpec(system.toSystemSpec()).build();
    }

    private VariableData variableData(long id, String name) {
        VariableData variable = new VariableData();
        variable.setVariableName(name);
        variable.setSystem(system);
        variable.setDescription("desc");
        variable.setDeclaredType(VariableDeclaredType.MATRIX_NUMERIC);
        variable.setId(id);
        variable.setRecVersion(20L);

        VariableConfigData configData = new VariableConfigData();
        configData.setVariable(variable);
        configData.setFieldName("field");
        configData.setEntity(entityData());
        configData.setId(id);
        configData.setRecVersion(20L);
        variable.addConfig(configData);
        return variable;
    }

    private EntityData entityData() {
        final EntityData entityData = new EntityData();
        entityData.setId(10L);
        entityData.setRecVersion(20L);
        entityData.setKeyValues(ENTITY_KEY_VALUES_JSON);
        PartitionData partitionData = new PartitionData();
        partitionData.setId(10L);
        partitionData.setRecVersion(20L);
        partitionData.setSystem(system);
        partitionData.setKeyValues(PARTITION_KEY_VALUES_JSON_1);
        partitionData.setProperties(PartitionPropertiesData.empty());
        entityData.setPartition(partitionData);
        return entityData;
    }

    private Entity entity(long id) {
        return ReflectionUtils.builderInstance(Entity.InnerBuilder.class).id(id).entityKeyValues(ENTITY_KEY_VALUES_1)
                .systemSpec(SYSTEM_SPEC).partition(PARTITION).entityHistory(emptySortedSet())
                .lockedUntilStamp(Instant.MIN).recVersion(20).build();
    }

    private SystemSpecData system() {
        SystemSpecData systemSpecData = new SystemSpecData();
        systemSpecData.setId(10L);
        systemSpecData.setName("SYSTEM_NAME");
        systemSpecData.setEntityKeyDefs(ENTITY_KEY_VALUES_JSON);
        systemSpecData.setPartitionKeyDefs(PARTITION_KEY_VALUES_JSON_1);
        systemSpecData.setTimeKeyDefs("TKD");
        return systemSpecData;
    }
}
