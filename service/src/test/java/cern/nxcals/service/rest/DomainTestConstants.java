/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.rest;

import cern.nxcals.api.domain.CreateEntityRequest;
import cern.nxcals.api.domain.EntitySchema;
import cern.nxcals.api.domain.Partition;
import cern.nxcals.api.domain.PartitionProperties;
import cern.nxcals.api.domain.PartitionResource;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.common.domain.FindOrCreateEntityRequest;
import cern.nxcals.common.utils.GlobalObjectMapperProvider;
import cern.nxcals.common.utils.ReflectionUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Sets;

import java.util.Map;
import java.util.Set;

import static cern.nxcals.common.utils.KeyValuesUtils.convertMapIntoAvroSchemaString;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_DOUBLE_SCHEMA_KEY_1;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_DOUBLE_SCHEMA_KEY_2;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_SCHEMA_1;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_STRING_SCHEMA_KEY_1;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_STRING_SCHEMA_KEY_2;
import static cern.nxcals.service.rest.TestSchemas.PARTITION_DOUBLE_SCHEMA_KEY_1;
import static cern.nxcals.service.rest.TestSchemas.PARTITION_DOUBLE_SCHEMA_KEY_2;
import static cern.nxcals.service.rest.TestSchemas.PARTITION_SCHEMA_1;
import static cern.nxcals.service.rest.TestSchemas.PARTITION_SCHEMA_2;
import static cern.nxcals.service.rest.TestSchemas.PARTITION_STRING_SCHEMA_KEY_1;
import static cern.nxcals.service.rest.TestSchemas.PARTITION_STRING_SCHEMA_KEY_2;
import static cern.nxcals.service.rest.TestSchemas.RECORD_VERSION_SCHEMA;
import static cern.nxcals.service.rest.TestSchemas.TIME_SCHEMA;

public class DomainTestConstants {

    public static final ObjectMapper OBJECT_MAPPER = GlobalObjectMapperProvider.get();
    public static final long SYSTEM_ID = 10;
    public static final long SYSTEM_ID2 = 20;
    public static final String SYSTEM_NAME = "SYSTEM_NAME";
    public static final String SYSTEM_NAME2 = "SYSTEM_NAME2";
    public static final String TEST_MESSAGE = "TEST_MESSAGE";
    public static final long TIMESTAMP = 1;

    public static final long PARTITION_ID = 20;
    public static final long PARTITION_RESOURCE_ID = 21;

    public static final Map<String, Object> PARTITION_KEY_VALUES_1 = ImmutableMap
            .of(PARTITION_STRING_SCHEMA_KEY_1, "string_1", PARTITION_DOUBLE_SCHEMA_KEY_1, 1d);
    public static final Map<String, Object> PARTITION_KEY_VALUES_1_2 = ImmutableMap
            .of(PARTITION_STRING_SCHEMA_KEY_1, "string_2", PARTITION_DOUBLE_SCHEMA_KEY_1, 2d);

    public static final Map<String, Object> UPDATED_PARTITION_PROPERTIES = ImmutableMap
            .of("updatable", true, "stagingPartitionType", TimeUtils.TimeSplits.ONE_HOUR_SPLIT.toString(),
                    "stagingPartitionTypeFromStamp", "2024-01-01 00:00:00.000",
                    "stagingPartitionTypeToStamp", "2024-01-02 00:00:00.000");

    public static final Map<String, Object> PARTITION_PROPERTIES = ImmutableMap
            .of("updatable", false, "stagingPartitionType", TimeUtils.TimeSplits.ONE_HOUR_SPLIT.toString(),
                    "stagingPartitionTypeFromStamp", "2024-01-01 00:00:00.000",
                    "stagingPartitionTypeToStamp", "2024-01-02 00:00:00.000");

    public static final String PARTITION_KEY_VALUES_STRING_1 = convertMapIntoAvroSchemaString(PARTITION_KEY_VALUES_1,
            PARTITION_SCHEMA_1.toString());

    public static final Map<String, Object> PARTITION_KEY_VALUES_2 = ImmutableMap
            .of(PARTITION_STRING_SCHEMA_KEY_2, "string_2", PARTITION_DOUBLE_SCHEMA_KEY_2, 2d);

    public static final String PARTITION_KEY_VALUES_STRING_2 = convertMapIntoAvroSchemaString(PARTITION_KEY_VALUES_2,
            PARTITION_SCHEMA_2.toString());

    public static final Partition PARTITION = createPartition(false);
    public static final Partition UPDATED_PARTITION = createPartition(true);
    public static final PartitionResource PARTITION_RESOURCE = createPartitionResource();
    public static final Map<String, Object> ENTITY_KEY_VALUES_1 = ImmutableMap
            .of(ENTITY_STRING_SCHEMA_KEY_1, "string", ENTITY_DOUBLE_SCHEMA_KEY_1, 1d);
    public static final Map<String, Object> ENTITY_KEY_VALUES_1_2 = ImmutableMap
            .of(ENTITY_STRING_SCHEMA_KEY_1, "string2", ENTITY_DOUBLE_SCHEMA_KEY_1, 2d);
    public static final Map<String, Object> ENTITY_KEY_VALUES_1_WITH_WILDCARD = ImmutableMap
            .of(ENTITY_STRING_SCHEMA_KEY_1, "%tr%", ENTITY_DOUBLE_SCHEMA_KEY_1, 1d);
    public static final Map<String, Object> ENTITY_KEY_VALUES_1_WITH_WILDCARD_WRONG = ImmutableMap
            .of(ENTITY_STRING_SCHEMA_KEY_1, "%1%", ENTITY_DOUBLE_SCHEMA_KEY_1, 1d);
    public static final Map<String, Object> ENTITY_KEY_VALUES_2 = ImmutableMap
            .of(ENTITY_STRING_SCHEMA_KEY_2, "string", ENTITY_DOUBLE_SCHEMA_KEY_2, 1d);
    public static final String ENTITY_KEY_VALUES_JSON = convertMapIntoAvroSchemaString(ENTITY_KEY_VALUES_1,
            ENTITY_SCHEMA_1.toString());
    public static final String ENTITY_KEY_VALUES_JSON_WITH_WILDCARD = convertMapIntoAvroSchemaString(
            ENTITY_KEY_VALUES_1_WITH_WILDCARD, ENTITY_SCHEMA_1.toString());
    public static final String ENTITY_KEY_VALUES_JSON_WITH_WILDCARD_WRONG = convertMapIntoAvroSchemaString(
            ENTITY_KEY_VALUES_1_WITH_WILDCARD_WRONG, ENTITY_SCHEMA_1.toString());
    public static final String PARTITION_KEY_VALUES_JSON_1 = convertMapIntoAvroSchemaString(PARTITION_KEY_VALUES_1,
            PARTITION_SCHEMA_1.toString());
    public static final String SCHEMA_VALUE = "SCHEMA_VALUE";
    public static final long ENTITY_ID = 40;
    public static final long ENTITY_ID2 = 41;
    public static final long SCHEMA_ID = 30;
    public static final EntitySchema SCHEMA = createSchema();
    public static final String VARIABLE_NAME = "VARIABLE_NAME";
    public static final String VARIABLE_NAME2 = "VARIABLE_NAME2";
    public static final String VARIABLE_DESCRIPTION = "VARIABLE_DESCRIPTION";
    public static final String VARIABLE_CONFIG_FIELD_NAME = "VARIABLE_CONFIG_FIELD_NAME";
    public static final VariableConfig VARIABLE_CONFIGS = VariableConfig.builder().entityId(ENTITY_ID)
            .fieldName(VARIABLE_CONFIG_FIELD_NAME).build();
    public static final SystemSpec SYSTEM_SPEC = createSystemSpec(SYSTEM_ID, SYSTEM_NAME);
    public static final SystemSpec SYSTEM_SPEC_2 = createSystemSpec(SYSTEM_ID2, SYSTEM_NAME2);
    public static final Variable VARIABLE_DATA = Variable.builder().variableName(VARIABLE_NAME)
            .description(VARIABLE_DESCRIPTION)
            .configs(ImmutableSortedSet.of(VARIABLE_CONFIGS)).systemSpec(SYSTEM_SPEC)
            .build();
    public static final Variable VARIABLE_DATA2 = Variable.builder().variableName(VARIABLE_NAME2)
            .description(VARIABLE_DESCRIPTION)
            .configs(ImmutableSortedSet.of(VARIABLE_CONFIGS)).systemSpec(SYSTEM_SPEC)
            .build();
    public static final CreateEntityRequest CREATE_ENTITY_REQUEST = CreateEntityRequest.builder().systemId(SYSTEM_ID)
            .entityKeyValues(ENTITY_KEY_VALUES_1).partitionKeyValues(PARTITION_KEY_VALUES_1).build();
    public static final CreateEntityRequest CREATE_ENTITY_REQUEST2 = CreateEntityRequest.builder().systemId(SYSTEM_ID2)
            .entityKeyValues(ENTITY_KEY_VALUES_2).partitionKeyValues(PARTITION_KEY_VALUES_2).build();
    public static final Set<CreateEntityRequest> CREATE_ENTITY_REQUESTS = Sets
            .newHashSet(CREATE_ENTITY_REQUEST, CREATE_ENTITY_REQUEST2);
    static String FIND_OR_CREATE_ENTITY_REQUEST_JSON;
    static String CREATE_ENTITY_REQUESTS_JSON;
    static String VARIABLE_DATA_JSON;

    static {
        try {
            FIND_OR_CREATE_ENTITY_REQUEST_JSON = OBJECT_MAPPER.writeValueAsString(
                    FindOrCreateEntityRequest.builder().entityKeyValues(ENTITY_KEY_VALUES_1)
                            .partitionKeyValues(PARTITION_KEY_VALUES_1).schema(ENTITY_SCHEMA_1.toString()).build());

            VARIABLE_DATA_JSON = OBJECT_MAPPER.writeValueAsString(VARIABLE_DATA);
            CREATE_ENTITY_REQUESTS_JSON = OBJECT_MAPPER.writeValueAsString(CREATE_ENTITY_REQUESTS);
        } catch (Exception exception) {
            throw new RuntimeException("Error initializing necessary variables for testing.", exception);
        }
    }

    private static Partition createPartition(boolean isUpdatable) {
        return ReflectionUtils.builderInstance(Partition.InnerBuilder.class).id(PARTITION_ID).systemSpec(createSystemSpec(SYSTEM_ID, SYSTEM_NAME))
                .keyValues(PARTITION_KEY_VALUES_1).properties(new PartitionProperties(isUpdatable,
                        TimeUtils.TimeSplits.ONE_HOUR_SPLIT,
                        TimeWindow.between(TimeUtils.getInstantFromString("2024-01-01 00:00:00.000"),
                                TimeUtils.getInstantFromString("2024-01-02 00:00:00.000")))).build();
    }

    private static EntitySchema createSchema() {
        return ReflectionUtils.builderInstance(EntitySchema.InnerBuilder.class).id(SCHEMA_ID).schemaJson(SCHEMA_VALUE).build();
    }

    private static SystemSpec createSystemSpec(long systemId, String systemName) {
        return ReflectionUtils.builderInstance(SystemSpec.InnerBuilder.class).id(systemId)
                .name(systemName)
                .entityKeyDefinitions(ENTITY_SCHEMA_1.toString()).partitionKeyDefinitions(PARTITION_SCHEMA_1.toString())
                .timeKeyDefinitions(TIME_SCHEMA.toString())
                .recordVersionKeyDefinitions(RECORD_VERSION_SCHEMA.toString()).build();
    }

    private static PartitionResource createPartitionResource() {
        return ReflectionUtils.builderInstance(PartitionResource.InnerBuilder.class)
                .id(PARTITION_RESOURCE_ID)
                .systemSpec(createSystemSpec(SYSTEM_ID, SYSTEM_NAME))
                .partition(createPartition(false))
                .schema(createSchema())
                .recVersion(1L)
                .build();
    }
}
