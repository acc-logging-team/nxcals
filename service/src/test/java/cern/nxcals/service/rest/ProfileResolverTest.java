/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.rest;

import cern.nxcals.service.BaseTest;
import cern.nxcals.service.domain.EntityData;
import cern.nxcals.service.domain.SystemSpecData;
import cern.nxcals.service.internal.EntityService;
import cern.nxcals.service.repository.SystemSpecRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Optional;

import static cern.nxcals.common.web.Endpoints.ENTITIES;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_ID;
import static cern.nxcals.service.rest.DomainTestConstants.FIND_OR_CREATE_ENTITY_REQUEST_JSON;
import static cern.nxcals.service.rest.DomainTestConstants.SYSTEM_ID;
import static cern.nxcals.service.rest.DomainTestConstants.TIMESTAMP;
import static cern.nxcals.service.rest.GlobalControllerExceptionHandler.FORBIDDEN_ERROR_FORMAT;
import static java.lang.String.format;
import static org.hamcrest.Matchers.containsString;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@ExtendWith(SpringExtension.class)
public class ProfileResolverTest extends BaseTest {
    @MockBean
    private SystemSpecRepository systemRepository;

    @MockBean
    private EntityService internalEntityService;

    @Mock
    private EntityData entity;

    @Mock
    private SystemSpecData system;

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @BeforeEach
    public void setup() {
        when(entity.getId()).thenReturn(ENTITY_ID);

        when(internalEntityService.findOrCreateEntityFor(isA(Long.class), anyMap(),
                anyMap(), isA(String.class), isA(Long.class))).thenReturn(entity);

        when(system.getName()).thenReturn(SYSTEM_NAME);

        when(systemRepository.findById(eq(SYSTEM_ID))).thenReturn(Optional.of(system));

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
    }

    @Test
    @WithMockUser(authorities = "ALL")
    public void shouldNotAuthorizeToFindOrCreateEntityFor() throws Exception {
        // given
        String expectedString = format(FORBIDDEN_ERROR_FORMAT, "Access Denied");
        var request = put(ENTITIES)
                .param("systemId", String.valueOf(SYSTEM_ID))
                .param("recordTimestamp", String.valueOf(TIMESTAMP))
                .contentType(MediaType.APPLICATION_JSON)
                .content(FIND_OR_CREATE_ENTITY_REQUEST_JSON);

        // when
        mockMvc.perform(request)
                .andExpect(status().isForbidden())
                .andExpect(content().string(containsString(expectedString)));
    }

    @Test
    @WithMockUser(authorities = BaseTest.AUTHORITY)
    public void shouldAuthorizeWithDefaultProfile() throws Exception {
        // given
        var request = put(ENTITIES)
                .param("systemId", String.valueOf(SYSTEM_ID))
                .param("recordTimestamp", String.valueOf(TIMESTAMP))
                .contentType(MediaType.APPLICATION_JSON)
                .content(FIND_OR_CREATE_ENTITY_REQUEST_JSON);

        // when
        var result = mockMvc.perform(request);

        result.andExpect(status().isOk());
    }
}

