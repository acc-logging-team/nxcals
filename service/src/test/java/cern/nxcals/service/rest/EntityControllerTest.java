/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.rest;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.common.utils.ReflectionUtils;
import cern.nxcals.service.BaseTest;
import cern.nxcals.service.domain.EntityData;
import cern.nxcals.service.internal.EntityService;
import cern.nxcals.service.rest.exceptions.ConfigDataConflictException;
import cern.nxcals.service.rest.exceptions.VersionMismatchException;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import org.assertj.core.util.Lists;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.persistence.OptimisticLockException;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static cern.nxcals.common.web.Endpoints.BATCH;
import static cern.nxcals.common.web.Endpoints.ENTITIES;
import static cern.nxcals.common.web.Endpoints.ENTITIES_FIND_ALL;
import static cern.nxcals.common.web.Endpoints.ENTITIES_FIND_ALL_WITH_HISTORY;
import static cern.nxcals.common.web.Endpoints.ENTITY_UPDATE;
import static cern.nxcals.service.rest.DomainTestConstants.CREATE_ENTITY_REQUESTS;
import static cern.nxcals.service.rest.DomainTestConstants.CREATE_ENTITY_REQUESTS_JSON;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_ID;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_ID2;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_KEY_VALUES_1;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_KEY_VALUES_2;
import static cern.nxcals.service.rest.DomainTestConstants.FIND_OR_CREATE_ENTITY_REQUEST_JSON;
import static cern.nxcals.service.rest.DomainTestConstants.OBJECT_MAPPER;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_ID;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_1;
import static cern.nxcals.service.rest.DomainTestConstants.SCHEMA_VALUE;
import static cern.nxcals.service.rest.DomainTestConstants.SYSTEM_ID;
import static cern.nxcals.service.rest.DomainTestConstants.SYSTEM_ID2;
import static cern.nxcals.service.rest.DomainTestConstants.SYSTEM_SPEC;
import static cern.nxcals.service.rest.DomainTestConstants.SYSTEM_SPEC_2;
import static cern.nxcals.service.rest.DomainTestConstants.TEST_MESSAGE;
import static cern.nxcals.service.rest.DomainTestConstants.TIMESTAMP;
import static cern.nxcals.service.rest.GlobalControllerExceptionHandler.CONSTRAINT_VIOLATION_DEFAULT_ERROR_FORMAT;
import static cern.nxcals.service.rest.GlobalControllerExceptionHandler.DATA_CONFLICT_ERROR_FORMAT;
import static cern.nxcals.service.rest.GlobalControllerExceptionHandler.ILLEGAL_ARGUMENT_ERROR_FORMAT;
import static cern.nxcals.service.rest.GlobalControllerExceptionHandler.INTERNAL_ERROR_FORMAT;
import static cern.nxcals.service.rest.GlobalControllerExceptionHandler.VERSION_MISMATCH_ERROR_FORMAT;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_DOUBLE_SCHEMA_KEY_1;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_SCHEMA_1;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_STRING_SCHEMA_KEY_1;
import static java.lang.String.format;
import static java.util.Collections.emptyList;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Test suite that checks if the endpoints defined in {@link EntityController} follow the desired contract.
 */
@ExtendWith(SpringExtension.class)
public class EntityControllerTest {
    private static final Instant LOCK_UNTIL_EPOCH_NANOS = Instant.ofEpochSecond(1);
    private static final long REC_VERSION = 0L;
    private static final long NEW_REC_VERSION = 1L;
    private static final Map<String, Object> NEW_KEY_VALUES = ImmutableMap
            .of(ENTITY_STRING_SCHEMA_KEY_1, "new_string", ENTITY_DOUBLE_SCHEMA_KEY_1, "new_double");

    static final Entity ENTITY = createEntity1();
    private static final String QUERY = "query";

    private static Entity createEntity1() {
        return ReflectionUtils.builderInstance(Entity.InnerBuilder.class)
                .id(ENTITY_ID)
                .entityKeyValues(ENTITY_KEY_VALUES_1)
                .systemSpec(SYSTEM_SPEC)
                .partition(PARTITION)
                .entityHistory(Collections.emptySortedSet())
                .lockedUntilStamp(LOCK_UNTIL_EPOCH_NANOS)
                .recVersion(REC_VERSION)
                .build();
    }

    static final Entity NEW_ENTITY = createEntity2();

    private static Entity createEntity2() {
        return ReflectionUtils.builderInstance(Entity.InnerBuilder.class)
                .id(ENTITY_ID + 1)
                .entityKeyValues(NEW_KEY_VALUES)
                .systemSpec(SYSTEM_SPEC)
                .partition(PARTITION)
                .entityHistory(Collections.emptySortedSet())
                .lockedUntilStamp(LOCK_UNTIL_EPOCH_NANOS)
                .recVersion(NEW_REC_VERSION)
                .build();
    }

    static final Entity ENTITY3 = createEntity3();

    private static Entity createEntity3() {
        return ReflectionUtils.builderInstance(Entity.InnerBuilder.class)
                .id(ENTITY_ID2)
                .entityKeyValues(ENTITY_KEY_VALUES_2)
                .systemSpec(SYSTEM_SPEC_2)
                .partition(PARTITION)
                .entityHistory(Collections.emptySortedSet())
                .lockedUntilStamp(LOCK_UNTIL_EPOCH_NANOS)
                .recVersion(REC_VERSION)
                .build();
    }

    private static final String EMPTY_JSON_ARRAY = "[]";

    @InjectMocks
    private EntityController entityController;

    @Mock
    private EntityService internalEntityService;

    @Mock
    private EntityData entity;

    @Mock
    private EntityData entity3;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        when(entity.toEntity()).thenReturn(ENTITY);
        when(entity3.toEntity()).thenReturn(ENTITY3);

        mockMvc = MockMvcBuilders.standaloneSetup(entityController)
                .setControllerAdvice(new GlobalControllerExceptionHandler())
                .build();
    }

    @Test
    public void shouldFindBySystemIdAndKeyValues() throws Exception {
        when(internalEntityService.findAll(QUERY)).thenReturn(Lists.newArrayList(entity));

        mockMvc.perform(post(ENTITIES_FIND_ALL)
                        .contentType(MediaType.TEXT_PLAIN_VALUE)
                        .content(QUERY))
                .andExpect(status().isOk()).andExpect(jsonPath("$[0].id").value(ENTITY_ID));
    }

    @Test
    public void shouldReturnEmptyListWhenFindsNothing() throws Exception {
        when(internalEntityService.findAll(QUERY)).thenReturn(emptyList());

        mockMvc.perform(post(ENTITIES_FIND_ALL)
                        .contentType(MediaType.TEXT_PLAIN_VALUE)
                        .content(QUERY))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("[]")));
    }

    @Test
    public void shouldReturn500WhenFindBySystemIdAndKeyValuesFailsUnexpectedly() throws Exception {
        RuntimeException runtimeException = new RuntimeException(TEST_MESSAGE);
        when(internalEntityService.findAll(QUERY))
                .thenThrow(runtimeException);

        mockMvc.perform(post(ENTITIES_FIND_ALL)
                        .contentType(MediaType.TEXT_PLAIN_VALUE)
                        .content(QUERY))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("message").value(format(INTERNAL_ERROR_FORMAT, TEST_MESSAGE)));
    }

    @Test
    public void shouldFindOrCreateEntity() throws Exception {
        when(internalEntityService.findOrCreateEntityFor(eq(SYSTEM_ID), eq(ENTITY_KEY_VALUES_1), eq(
                        PARTITION_KEY_VALUES_1),
                eq(ENTITY_SCHEMA_1.toString()), eq(TIMESTAMP))).thenReturn(entity);

        mockMvc.perform(put(ENTITIES)
                        .param("systemId", String.valueOf(SYSTEM_ID))
                        .param("recordTimestamp", String.valueOf(TIMESTAMP))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(FIND_OR_CREATE_ENTITY_REQUEST_JSON))
                //                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").value(ENTITY_ID))
                .andExpect(jsonPath("systemSpec.id").value(SYSTEM_ID));
    }

    @Test
    public void shouldCreateEntities() throws Exception {
        when(internalEntityService.createEntities(eq(CREATE_ENTITY_REQUESTS))).thenReturn(
                Sets.newHashSet(entity, entity3));
        mockMvc.perform(post(ENTITIES + BATCH)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(CREATE_ENTITY_REQUESTS_JSON))
                //                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*].id", containsInAnyOrder((int) ENTITY_ID, (int) ENTITY_ID2)))
                .andExpect(jsonPath("$[*].systemSpec.id", containsInAnyOrder((int) SYSTEM_ID, (int) SYSTEM_ID2)));
    }

    @Test
    @WithMockUser(authorities = BaseTest.AUTHORITY)
    public void shouldReturn500WhenFindOrCreateEntityFailsUnexpectedly() throws Exception {
        RuntimeException runtimeException = new RuntimeException(TEST_MESSAGE);
        when(internalEntityService.findOrCreateEntityFor(isA(Long.class), anyMap(),
                anyMap(),
                isA(String.class), isA(Long.class))).thenThrow(runtimeException);

        mockMvc.perform(put(ENTITIES)
                        .param("systemId", String.valueOf(SYSTEM_ID))
                        .param("recordTimestamp", String.valueOf(TIMESTAMP))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(FIND_OR_CREATE_ENTITY_REQUEST_JSON))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("message").value(format(INTERNAL_ERROR_FORMAT, TEST_MESSAGE)));
    }

    @Test
    public void shouldReturn409WhenFindOrCreateEntityFailsWithConfigDataConflictException() throws Exception {
        ConfigDataConflictException runtimeException = new ConfigDataConflictException(TEST_MESSAGE);
        when(internalEntityService.findOrCreateEntityFor(isA(Long.class), isA(Long.class), isA(Long.class),
                isA(String.class), isA(Long.class))).thenThrow(runtimeException);

        mockMvc.perform(put(ENTITIES)
                        .param("systemId", String.valueOf(SYSTEM_ID))
                        .param("entityId", String.valueOf(ENTITY_ID))
                        .param("partitionId", String.valueOf(PARTITION_ID))
                        .param("recordTimestamp", String.valueOf(TIMESTAMP))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(SCHEMA_VALUE))
                .andExpect(status().isConflict())
                .andExpect(jsonPath("message").value(format(DATA_CONFLICT_ERROR_FORMAT, TEST_MESSAGE)));
    }

    @Test
    public void shouldReturn400WhenFindOrCreateEntityFailsWithIllegalArgumentException() throws Exception {
        IllegalArgumentException runtimeException = new IllegalArgumentException(TEST_MESSAGE);
        when(internalEntityService.findOrCreateEntityFor(isA(Long.class), isA(Long.class), isA(Long.class),
                isA(String.class), isA(Long.class))).thenThrow(runtimeException);

        mockMvc.perform(put(ENTITIES)
                        .param("systemId", String.valueOf(SYSTEM_ID))
                        .param("entityId", String.valueOf(ENTITY_ID))
                        .param("partitionId", String.valueOf(PARTITION_ID))
                        .param("recordTimestamp", String.valueOf(TIMESTAMP))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(SCHEMA_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("message").value(format(ILLEGAL_ARGUMENT_ERROR_FORMAT, TEST_MESSAGE)));
    }

    @Test
    public void shouldFindBySystemIdKeyValuesAndTimeWindow() throws Exception {
        when(internalEntityService
                .findAll(QUERY, TIMESTAMP, TIMESTAMP))
                .thenReturn(Lists.newArrayList(entity));

        mockMvc.perform(post(ENTITIES_FIND_ALL_WITH_HISTORY)
                        .param("startTime", String.valueOf(TIMESTAMP))
                        .param("endTime", String.valueOf(TIMESTAMP))
                        .contentType(MediaType.TEXT_PLAIN_VALUE)
                        .content(QUERY))
                //                .andDo(print())
                .andExpect(status().isOk()).andExpect(jsonPath("$[0].id").value(ENTITY_ID))
                .andExpect(jsonPath("$[0].systemSpec.id").value(SYSTEM_ID));
    }

    @Test
    public void shouldReturnEmptyListWhenQueryFindsNothing() throws Exception {
        when(internalEntityService
                .findAll(QUERY, TIMESTAMP, TIMESTAMP))
                .thenReturn(emptyList());

        mockMvc.perform(post(ENTITIES_FIND_ALL_WITH_HISTORY)
                        .param("startTime", String.valueOf(TIMESTAMP))
                        .param("endTime", String.valueOf(TIMESTAMP))
                        .contentType(MediaType.TEXT_PLAIN_VALUE)
                        .content(QUERY))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("[]")));
    }

    @Test
    public void shouldReturn500WhenFindBySystemIdKeyvaluesAndTimeWindowFailsUnexpectedly() throws Exception {
        RuntimeException runtimeException = new RuntimeException(TEST_MESSAGE);
        when(internalEntityService
                .findAll(QUERY, TIMESTAMP, TIMESTAMP))
                .thenThrow(runtimeException);

        mockMvc.perform(post(ENTITIES_FIND_ALL_WITH_HISTORY)
                        .param("startTime", String.valueOf(TIMESTAMP))
                        .param("endTime", String.valueOf(TIMESTAMP))
                        .contentType(MediaType.TEXT_PLAIN_VALUE)
                        .content(QUERY))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("message").value(format(INTERNAL_ERROR_FORMAT, TEST_MESSAGE)));
    }

    @Test
    public void shouldRenameEntityUsingEntityRenameRequest() throws Exception {
        when(entity.toEntity()).thenReturn(NEW_ENTITY);
        List<EntityData> entityList = Collections.singletonList(entity);

        List<Entity> entityDataList = Lists.newArrayList();
        entityDataList.add(ENTITY);

        when(internalEntityService.updateEntities(eq(entityDataList))).thenReturn(entityList);

        mockMvc.perform(put(ENTITY_UPDATE)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(OBJECT_MAPPER.writeValueAsString(entityDataList)))
                //                .andDo(print())
                .andExpect(jsonPath("$[0].entityKeyValues").value(is(NEW_KEY_VALUES)));
    }

    @Test
    public void shouldUpdateEntityLockUntilUsingEntityUpdateRequest() throws Exception {
        when(entity.toEntity()).thenReturn(NEW_ENTITY);

        List<Entity> entityDataList = Collections.singletonList(ENTITY);
        when(internalEntityService.updateEntities(eq(entityDataList)))
                .thenReturn(Collections.singletonList(entity));

        mockMvc.perform(put(ENTITY_UPDATE)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(OBJECT_MAPPER.writeValueAsString(entityDataList)))
                //                .andDo(print())
                .andExpect(jsonPath("$[0].lockedUntilStamp").value(LOCK_UNTIL_EPOCH_NANOS.getEpochSecond()));
    }

    @Test
    public void shouldReturnUnlockedEntityWhenUnlockEntityUpdateRequest() throws Exception {
        Entity nonLockedEntityData = NEW_ENTITY.toBuilder().unlock().build();

        when(entity.toEntity()).thenReturn(nonLockedEntityData);

        List<Entity> entityDataList = Collections.singletonList(nonLockedEntityData);
        when(internalEntityService.updateEntities(eq(entityDataList))).thenReturn(Collections.singletonList(entity));

        mockMvc.perform(put(ENTITY_UPDATE)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(OBJECT_MAPPER.writeValueAsString(entityDataList)))
                //                .andDo(print())
                .andExpect(jsonPath("$[0].lockedUntilStamp").isEmpty());
    }

    @Test
    public void shouldReturn428CodeWhenThereIsRecordVersionMismatch() throws Exception {
        RuntimeException runtimeException = new VersionMismatchException(TEST_MESSAGE);
        when(internalEntityService.updateEntities(anyList()))
                .thenThrow(runtimeException);

        mockMvc.perform(put(ENTITY_UPDATE)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(EMPTY_JSON_ARRAY))
                .andExpect(status().isPreconditionRequired())
                .andExpect(jsonPath("message").value(format(VERSION_MISMATCH_ERROR_FORMAT, TEST_MESSAGE)));
    }

    @Test
    public void shouldReturn428CodeWhenThereIsJPAOptimisticLockException() throws Exception {
        RuntimeException runtimeException = new OptimisticLockException(TEST_MESSAGE);
        when(internalEntityService.updateEntities(anyList()))
                .thenThrow(runtimeException);

        mockMvc.perform(put(ENTITY_UPDATE)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(EMPTY_JSON_ARRAY))
                .andExpect(status().isPreconditionRequired())
                .andExpect(jsonPath("message").value(format(VERSION_MISMATCH_ERROR_FORMAT, TEST_MESSAGE)));
    }

    @Test
    public void shouldReturn428CodeWhenThereIsConstraintViolationException() throws Exception {
        RuntimeException runtimeException = new ConstraintViolationException(TEST_MESSAGE, null, "");
        when(internalEntityService.updateEntities(anyList()))
                .thenThrow(runtimeException);

        mockMvc.perform(put(ENTITY_UPDATE)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(EMPTY_JSON_ARRAY))
                .andExpect(status().isPreconditionRequired())
                .andExpect(jsonPath("message").value(format(CONSTRAINT_VIOLATION_DEFAULT_ERROR_FORMAT, TEST_MESSAGE)));
    }
}
