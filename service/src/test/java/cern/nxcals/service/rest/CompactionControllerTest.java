/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.rest;

import cern.nxcals.common.domain.CompactionJob;
import cern.nxcals.common.domain.DataProcessingJob;
import cern.nxcals.common.domain.DataProcessingJob.JobType;
import cern.nxcals.common.domain.MergeCompactionJob;
import cern.nxcals.common.domain.RestageJob;
import cern.nxcals.service.internal.CompactionService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static cern.nxcals.common.web.Endpoints.COMPACTION_JOBS;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
public class CompactionControllerTest {
    private MockMvc mockMvc;

    @Mock
    private CompactionService internalCompactionService;
    @InjectMocks
    private CompactionController controller;

    @BeforeEach
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.standaloneSetup(controller)
                .setControllerAdvice(new GlobalControllerExceptionHandler())
                .build();
    }

    @AfterEach
    public void tearDown() throws Exception {
    }

    @Test
    public void shouldGetCompactionJobs() throws Exception {
        int maxJobs = 2;
        JobType jobType = JobType.COMPACT;
        List<DataProcessingJob> jobs = new ArrayList<>();
        jobs.add(createCompactionJob(URI.create("destDir1"), Collections.singletonList(URI.create("file1.avro"))));
        jobs.add(createCompactionJob(URI.create("destDir2"), Collections.singletonList(URI.create("file2.avro"))));
        when(internalCompactionService.getDataProcessJobs(maxJobs, jobType)).thenReturn(jobs);

        mockMvc.perform(get(COMPACTION_JOBS)
                .param("maxJobs", Integer.toString(maxJobs))
                .param("jobType", jobType.name()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].destinationDir").value("destDir1"))
                .andExpect(jsonPath("$[0].files[0]").value("file1.avro"))
                .andExpect(jsonPath("$[0].filePrefix").value("test"))
                .andExpect(jsonPath("$[0].partitionCount").value(3))
                .andExpect(jsonPath("$[1].destinationDir").value("destDir2"))
                .andExpect(jsonPath("$[1].files[0]").value("file2.avro"))
                .andExpect(jsonPath("$[1].filePrefix").value("test"))
                .andExpect(jsonPath("$[1].partitionCount").value(3));
    }

    @Test
    public void shouldGetRestageJobs() throws Exception {
        int maxJobs = 1;
        JobType jobType = JobType.RESTAGE;
        String restageReportFile = "restage_report.info";
        List<DataProcessingJob> jobs = new ArrayList<>();
        jobs.add(createRestageJob(URI.create("sourceDir1"), URI.create("destDir1"),
                Collections.singletonList(URI.create("file1.avro")),
                Arrays.asList(URI.create("file1.parquet"), URI.create("file2.parquet")),
                URI.create(restageReportFile)));
        when(internalCompactionService.getDataProcessJobs(maxJobs, jobType)).thenReturn(jobs);

        mockMvc.perform(get(COMPACTION_JOBS)
                .param("maxJobs", Integer.toString(maxJobs))
                .param("jobType", jobType.name()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].destinationDir").value("destDir1"))
                .andExpect(jsonPath("$[0].sourceDir").value("sourceDir1"))
                .andExpect(jsonPath("$[0].files[0]").value("file1.avro"))
                .andExpect(jsonPath("$[0].dataFiles[0]").value("file1.parquet"))
                .andExpect(jsonPath("$[0].dataFiles[1]").value("file2.parquet"))
                .andExpect(jsonPath("$[0].restagingReportFile").value(restageReportFile));
    }

    @Test
    public void shouldGetMergeCompactionJobs() throws Exception {
        int maxJobs = 2;
        JobType jobType = JobType.MERGE_COMPACT;
        String restageReportFile = "restage_report.info";
        List<DataProcessingJob> jobs = new ArrayList<>();
        jobs.add(createMergeCompactJob(URI.create("destDir1"), Collections.singletonList(URI.create("file1.avro")),
                URI.create(restageReportFile)));
        jobs.add(createMergeCompactJob(URI.create("destDir2"), Collections.singletonList(URI.create("file2.avro")),
                URI.create(restageReportFile)));
        when(internalCompactionService.getDataProcessJobs(maxJobs, jobType)).thenReturn(jobs);

        mockMvc.perform(get(COMPACTION_JOBS)
                .param("maxJobs", Integer.toString(maxJobs))
                .param("jobType", jobType.name()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].destinationDir").value("destDir1"))
                .andExpect(jsonPath("$[0].files[0]").value("file1.avro"))
                .andExpect(jsonPath("$[0].filePrefix").value("test"))
                .andExpect(jsonPath("$[0].partitionCount").value(3))
                .andExpect(jsonPath("$[0].restagingReportFile").value(restageReportFile))
                .andExpect(jsonPath("$[1].destinationDir").value("destDir2"))
                .andExpect(jsonPath("$[1].files[0]").value("file2.avro"))
                .andExpect(jsonPath("$[1].filePrefix").value("test"))
                .andExpect(jsonPath("$[1].partitionCount").value(3))
                .andExpect(jsonPath("$[0].restagingReportFile").value(restageReportFile));
    }

    private DataProcessingJob createCompactionJob(URI destinationDir, List<URI> files) {
        return CompactionJob.builder()
                .destinationDir(destinationDir)
                .files(files)
                .filePrefix("test")
                .partitionCount(3)
                .build();
    }

    private DataProcessingJob createRestageJob(URI sourceDir, URI destinationDir, List<URI> files, List<URI> dataFiles,
            URI restageReportFile) {
        return RestageJob.builder()
                .destinationDir(destinationDir)
                .files(files)
                .sourceDir(sourceDir)
                .dataFiles(dataFiles)
                .restagingReportFile(restageReportFile)
                .build();
    }

    private DataProcessingJob createMergeCompactJob(URI destinationDir, List<URI> files, URI restageReportFile) {
        return MergeCompactionJob.builder()
                .destinationDir(destinationDir)
                .files(files)
                .filePrefix("test")
                .partitionCount(3)
                .restagingReportFile(restageReportFile)
                .build();
    }
}
