/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.repository;

import cern.nxcals.service.BaseTest;
import cern.nxcals.service.domain.EntitySchemaData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolationException;
import java.util.Optional;

import static cern.nxcals.service.rest.TestSchemas.ENTITY_SCHEMA_1;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Transactional(transactionManager = "jpaTransactionManager")
public class EntitySchemaRepositoryTest extends BaseTest {

    @BeforeEach
    public void setUp() {
        setAuthentication();
    }

    @Test
    @Rollback
    public void shouldNotFindSchemaWrongId() {
        Optional<EntitySchemaData> optionalSchema = schemaRepository.findById(-1L);
        assertThat(optionalSchema).isNotPresent();
    }

    @Rollback
    @Test
    public void shouldNotCreateSchemaWithoutContent() {
        EntitySchemaData schema = createSchema(null);
        schemaRepository.save(schema);
        assertThrows(ConstraintViolationException.class, () -> entityManager.flush());
    }

    @Test
    @Rollback
    public void shouldCreateSchema() {
        EntitySchemaData newSchema = createSchema(ENTITY_SCHEMA_1);
        schemaRepository.save(newSchema);
        Optional<EntitySchemaData> optionalSchema = schemaRepository.findById(newSchema.getId());
        assertThat(optionalSchema).isPresent();
        EntitySchemaData foundSchema = optionalSchema.get();
        assertThat(foundSchema.getContent()).isEqualTo(ENTITY_SCHEMA_1.toString());
    }
}
