/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.repository;

import cern.nxcals.api.extraction.metadata.queries.SystemSpecs;
import cern.nxcals.service.BaseTest;
import cern.nxcals.service.domain.SystemSpecData;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolationException;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.LongStream;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_KEY_VALUES_1;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_1;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_SCHEMA_1;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_SCHEMA_2;
import static cern.nxcals.service.rest.TestSchemas.PARTITION_SCHEMA_1;
import static cern.nxcals.service.rest.TestSchemas.PARTITION_SCHEMA_2;
import static cern.nxcals.service.rest.TestSchemas.TIME_SCHEMA;
import static cern.nxcals.service.rest.TestSchemas.getFullSchema;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Transactional(transactionManager = "jpaTransactionManager")
public class SystemSpecRepositoryTest extends BaseTest {

    @BeforeEach
    public void setUp() {
        setAuthentication();
    }

    @Test
    @Rollback
    public void shouldNotFindSystemWIthWrongName() {
        createAndPersistSystemData(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA);

        Optional<SystemSpecData> foundSystem = systemRepository.findByName("");
        assertThat(foundSystem).isNotPresent();
    }

    @Test
    @Rollback
    public void shouldFindSystemWithCorrectName() {
        Long id = createAndPersistSystemData(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA).getId();

        Optional<SystemSpecData> optionalSystem = systemRepository.findByName(TEST_NAME);

        assertThat(optionalSystem).isPresent();

        SystemSpecData foundSystem = optionalSystem.get();

        assertThat(foundSystem.getId()).isEqualTo(id);
        assertThat(foundSystem.getEntityKeyDefs()).isEqualTo(ENTITY_SCHEMA_1.toString());
    }

    @Test
    @Rollback
    public void shouldWorkWithLongLists() {
        Condition<SystemSpecs> queryCondition = SystemSpecs.suchThat().name().exists().and()
                .id().in(LongStream.range(1, 1002).boxed().toArray(Long[]::new));
        systemRepository.queryAll(toRSQL(queryCondition), SystemSpecData.class);
    }

    @Test
    @Rollback
    public void shouldFindAllSystems() {
        Long id1 = createAndPersistSystemData(SYSTEM_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA).getId();
        Long id2 = createAndPersistSystemData(SYSTEM_NAME2, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA).getId();

        Set<SystemSpecData> systems = systemRepository.findAll();

        assertThat(systems).isNotNull();

        Set<Long> systemIds = new HashSet<>();
        systems.forEach(system -> systemIds.add(system.getId()));

        assertThat(systemIds.contains(id1)).isTrue();
        assertThat(systemIds.contains(id2)).isTrue();
    }


    @Test
    @Rollback
    public void shouldNotCreateSystemWithoutName() {
        createAndPersistSystemData(null, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA);
        assertThrows(ConstraintViolationException.class, () -> entityManager.flush());
    }

    @Test
    @Rollback
    public void shouldNotCreateSystemWithoutKeyDefinitions() {
        createAndPersistSystemData(TEST_NAME, null, PARTITION_SCHEMA_1, TIME_SCHEMA);
        assertThrows(ConstraintViolationException.class, () -> entityManager.flush());
    }

    @Test
    @Rollback
    public void shouldNotCreateSystemWithoutPartitionDefinitions() {
        createAndPersistSystemData(TEST_NAME, ENTITY_SCHEMA_1, null, TIME_SCHEMA);
        assertThrows(ConstraintViolationException.class, () -> entityManager.flush());
    }

    @Test
    @Rollback
    public void shouldNotCreateSystemWithoutTimeDefinitions() {
        createAndPersistSystemData(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, null);
        assertThrows(ConstraintViolationException.class, () -> entityManager.flush());
    }

    @Test
    @Rollback
    public void shouldCreateClientSystem() {
        Long id = createAndPersistSystemData(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA).getId();

        Optional<SystemSpecData> optionalSystem = systemRepository.findById(id);

        assertThat(optionalSystem).isPresent();

        SystemSpecData foundSystem = optionalSystem.get();

        assertThat(foundSystem.getName()).isEqualTo(TEST_NAME);
        assertThat(foundSystem.getEntityKeyDefs()).isEqualTo(ENTITY_SCHEMA_1.toString());
        assertThat(foundSystem.getPartitionKeyDefs()).isEqualTo(PARTITION_SCHEMA_1.toString());
        assertThat(foundSystem.getTimeKeyDefs()).isEqualTo(TIME_SCHEMA.toString());
    }

    @Test
    @Rollback
    public void shouldThrowExceptionWhenCreatingSystemsWithTwoIdenticalNames() {
        Long systemId1 = createAndPersistSystemData(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA)
                .getId();

        createAndPersistSystemData(TEST_NAME, ENTITY_SCHEMA_2, PARTITION_SCHEMA_2, TIME_SCHEMA)
                .getId();
        assertThrows(DataIntegrityViolationException.class, () ->
                service.findOrCreateEntityFor(systemId1, ENTITY_KEY_VALUES_1, PARTITION_KEY_VALUES_1,
                getFullSchema(ENTITY_SCHEMA_1, PARTITION_SCHEMA_1).toString(), TEST_RECORD_TIME + 10)
                .getEntityHistories().first().getSchema().getId());
    }
}
