package cern.nxcals.service.repository;

import cern.nxcals.api.extraction.metadata.queries.PartitionResourceHistories;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.service.BaseTest;
import cern.nxcals.service.domain.PartitionResourceData;
import cern.nxcals.service.domain.PartitionResourceHistoryData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_KEY_VALUES_2;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_SCHEMA_2;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Transactional(transactionManager = "jpaTransactionManager")
@Rollback
class PartitionResourceHistoryRepositoryTest extends BaseTest {
    @Autowired
    private PartitionResourceHistoryRepository repository;
    private PartitionResourceHistoryData firstPartitionResourceInfo;
    private PartitionResourceHistoryData secondPartitionResourceInfo;
    private PartitionResourceHistoryData thirdPartitionResourceInfo;
    private PartitionResourceData firstPartitionResource;
    private PartitionResourceData secondPartitionResource;

    long startTime = 100L;

    @BeforeEach
    void init() {
        setAuthentication();
        firstPartitionResource = createDefaultPartitionResourceData();
        firstPartitionResourceInfo = partitionResourceInformationData(firstPartitionResource, "Information",
                "Compaction", "Storage 1", false, TimeUtils.getInstantFromNanos(startTime), TimeUtils.getInstantFromNanos(startTime + 100L), 2L);
        secondPartitionResourceInfo = partitionResourceInformationData(firstPartitionResource, "Information",
                "Compaction", "Storage 2", false, TimeUtils.getInstantFromNanos(startTime + 100L), TimeUtils.getInstantFromNanos(startTime + 200L), 2L);
        secondPartitionResource = partitionResourceData(firstPartitionResource.getSystem(),
                firstPartitionResource.getSchema(),
                ENTITY_KEY_VALUES_2,
                ENTITY_SCHEMA_2);
        thirdPartitionResourceInfo = partitionResourceInformationData(secondPartitionResource, "Information",
                "Compaction", "Storage 1", false, TimeUtils.getInstantFromNanos(startTime), TimeUtils.getInstantFromNanos(startTime + 100L), 2L);
    }

    @Test
    void shouldFindPartitionResourceWithId() {
        List<PartitionResourceHistoryData> firstResult = repository.queryAll(
                toRSQL(PartitionResourceHistories.suchThat().id().eq(firstPartitionResourceInfo.getId())),
                PartitionResourceHistoryData.class);
        assertThat(firstResult).hasSize(1);
        assertThat(firstResult).contains(firstPartitionResourceInfo);

        List<PartitionResourceHistoryData> secondResult = repository.queryAll(
                toRSQL(PartitionResourceHistories.suchThat().id().eq(secondPartitionResourceInfo.getId())),
                PartitionResourceHistoryData.class);
        assertThat(secondResult).hasSize(1);
        assertThat(secondResult).contains(secondPartitionResourceInfo);
    }

    @Test
    void shouldNotFindAnyResults() {
        List<PartitionResourceHistoryData> firstResult = repository.queryAll(
                toRSQL(PartitionResourceHistories.suchThat().id().eq(-1L)),
                PartitionResourceHistoryData.class);
        assertThat(firstResult).isEmpty();

        List<PartitionResourceHistoryData> secondResult = repository.queryAll(
                toRSQL(PartitionResourceHistories.suchThat().partitionResourceId().eq(-1L)),
                PartitionResourceHistoryData.class);
        assertThat(secondResult).isEmpty();

        List<PartitionResourceHistoryData> thirdResult = repository.queryAll(
                toRSQL(PartitionResourceHistories.suchThat().storageType().eq("Inexistent Storage Type")),
                PartitionResourceHistoryData.class);
        assertThat(thirdResult).isEmpty();
    }

    @Test
    void shouldFindAllPartitionResourcesWithSamePartitionResourceId() {
        List<PartitionResourceHistoryData> results = repository.queryAll(
                toRSQL(PartitionResourceHistories.suchThat().partitionResourceId().eq(firstPartitionResource.getId())),
                PartitionResourceHistoryData.class);
        assertThat(results).containsExactlyInAnyOrder(firstPartitionResourceInfo, secondPartitionResourceInfo);
    }

    @Test
    void shouldFindAllPartitionResourcesWithSameStorageType() {
        List<PartitionResourceHistoryData> results = repository.queryAll(
                toRSQL(PartitionResourceHistories.suchThat().storageType().eq("Storage 1")),
                PartitionResourceHistoryData.class);
        assertThat(results).containsExactlyInAnyOrder(firstPartitionResourceInfo, thirdPartitionResourceInfo);
    }

    @Test
    void shouldFindByPartitionResourceAndTimestampFirst() {
        PartitionResourceHistoryData pri = repository.findByPartitionResourceAndTimestamp(firstPartitionResource, TimeUtils.getInstantFromNanos(startTime + 10)).get();
        assertEquals(firstPartitionResourceInfo, pri);
    }

    @Test
    void shouldFindByPartitionResourceAndTimestampSecond() {
        PartitionResourceHistoryData pri = repository.findByPartitionResourceAndTimestamp(firstPartitionResource, TimeUtils.getInstantFromNanos(startTime + 100)).get();
        assertEquals(secondPartitionResourceInfo, pri);
    }

    @Test
    void shouldFindByPartitionResourceIdAndValidToStamp() {
        PartitionResourceHistoryData pri = repository.findByPartitionResourceIdAndValidToStamp(firstPartitionResource.getId(), TimeUtils.getInstantFromNanos(startTime + 200L)).get();
        assertEquals(secondPartitionResourceInfo, pri);
    }

    @Test
    void shouldFindByPartitionResourceIdAndValidFromStamp() {
        PartitionResourceHistoryData pri = repository.findByPartitionResourceIdAndValidFromStamp(firstPartitionResource.getId(), TimeUtils.getInstantFromNanos(startTime + 100L)).get();
        assertEquals(secondPartitionResourceInfo, pri);
    }

    @Test
    void shouldFailOnWrongRQSL() {
        String query = "This is not a good format";
        assertThrows(cz.jirutka.rsql.parser.RSQLParserException.class, () -> repository.queryAll(query, PartitionResourceHistoryData.class));
    }




}
