/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.repository;

import cern.nxcals.api.extraction.metadata.queries.Entities;
import cern.nxcals.service.BaseTest;
import cern.nxcals.service.domain.EntityData;
import cern.nxcals.service.domain.PartitionData;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolationException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.stream.LongStream;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_KEY_VALUES_1;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_KEY_VALUES_JSON;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_KEY_VALUES_JSON_WITH_WILDCARD;
import static cern.nxcals.service.rest.DomainTestConstants.ENTITY_KEY_VALUES_JSON_WITH_WILDCARD_WRONG;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_1;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_2;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_JSON_1;
import static cern.nxcals.service.rest.DomainTestConstants.PARTITION_KEY_VALUES_STRING_2;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_SCHEMA_1;
import static cern.nxcals.service.rest.TestSchemas.PARTITION_SCHEMA_1;
import static cern.nxcals.service.rest.TestSchemas.PARTITION_SCHEMA_2;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Transactional(transactionManager = "jpaTransactionManager")
public class EntityRepositoryTest extends BaseTest {

    private EntityData newEntity;

    @BeforeEach
    public void setUp() {
        setAuthentication();

        newEntity = createEntity(TEST_NAME, ENTITY_KEY_VALUES_1, ENTITY_SCHEMA_1, PARTITION_KEY_VALUES_1,
                PARTITION_SCHEMA_1);
    }

    @Test
    @Rollback
    public void shouldNotCreateEntityWithoutKeyValues() {
        newEntity.setKeyValues(null);
        persistEntity(newEntity);
        assertThrows(ConstraintViolationException.class, () -> entityManager.flush());
    }

    @Test
    @Rollback
    public void shouldNotCreateEntityWithoutPartition() {
        newEntity.setPartition(null);
        entityRepository.save(newEntity);
        assertThrows(ConstraintViolationException.class, () -> entityManager.flush());
    }

    @Test
    @Rollback
    public void shouldCreateEntity() {
        persistEntity(newEntity);
        EntityData savedEntity = entityRepository.save(newEntity);
        assertThat(savedEntity).isNotNull();
        assertThat(savedEntity.getKeyValues()).isEqualTo(ENTITY_KEY_VALUES_JSON);
        assertThat(savedEntity.getPartition().getKeyValues()).isEqualTo(PARTITION_KEY_VALUES_JSON_1);
    }

    @Test
    @Rollback
    public void shouldChangeEntityPartition() {
        persistEntity(newEntity);
        PartitionData newPartition = createPartition(TEST_NAME, PARTITION_KEY_VALUES_2, PARTITION_SCHEMA_2);
        partitionRepository.save(newPartition);
        newEntity.setPartition(newPartition);
        entityRepository.save(newEntity);
        entityManager.flush();

        Optional<EntityData> optionalEntity = entityRepository.findById(newEntity.getId());

        assertThat(optionalEntity).isPresent();
        assertThat(optionalEntity.get().getPartition().getKeyValues()).isEqualTo(PARTITION_KEY_VALUES_STRING_2);
    }

    //TODO Check why this test fails
    /*@Test
    @Rollback
    public void shouldChangeEntityKeyValues() {
        persistEntity(newEntity);
        newEntity.setKeyValues(getKeyValuesString("Test"));
        entityRepository.save(newEntity);

        Optional<Entity> optionalEntity = entityRepository.findById(newEntity.getId());

        assertThat(optionalEntity).isPresent();
        Entity foundEntity = optionalEntity.get();

        assertThat(foundEntity.getKeyValues()).isEqualTo(getKeyValuesString("Test"));
        assertThat(foundEntity.getEntitySchema().getContent()).isEqualTo(getEntitySchema());
        assertThat(foundEntity.getPartition().getKeyValues()).isEqualTo(getKeyValues());
        assertThat(foundEntity.getPartition().getSystemSpec().getName()).isEqualTo(TEST_NAME);
    }*/

    @Test
    @Rollback
    public void shouldChangeEntityLockedUntilStamp() {
        persistEntity(newEntity);

        Instant lockUntilStamp = Instant.now().plus(2, ChronoUnit.HOURS);
        newEntity.setLockedUntilStamp(lockUntilStamp);
        entityRepository.save(newEntity);

        Optional<EntityData> optionalEntity = entityRepository.findById(newEntity.getId());

        assertThat(optionalEntity).isPresent();

        EntityData foundEntity = optionalEntity.get();

        assertThat(foundEntity.getLockedUntilStamp()).isEqualTo(lockUntilStamp);
        assertThat(foundEntity.getPartition().getKeyValues()).isEqualTo(PARTITION_KEY_VALUES_JSON_1);
        assertThat(foundEntity.getPartition().getSystem().getName()).isEqualTo(TEST_NAME);
    }

    @Test
    @Rollback
    public void shouldWorkWithLongLists() {
        Condition<Entities> queryCondition = Entities.suchThat().keyValues().exists().and()
                .id().in(LongStream.range(1, 1002).boxed().toArray(Long[]::new));
        entityRepository.queryAll(toRSQL(queryCondition), EntityData.class);
    }

    @Test
    @Rollback
    public void shouldNotFindResult() {
        persistEntity(newEntity);

        Condition<Entities> condition = Entities.suchThat().systemId().eq(-1L).and().keyValues()
                .eq(ENTITY_KEY_VALUES_JSON);

        Optional<EntityData> entity = entityRepository.queryOne(toRSQL(condition), EntityData.class);

        assertThat(entity).isEmpty();
    }

    @Test
    @Rollback
    public void shouldNotFindResultWhileQueryingWithRegex() {
        persistEntity(newEntity);

        Condition<Entities> condition = Entities.suchThat().keyValues().like("%rsdfin%");

        List<EntityData> foundEntities = entityRepository.queryAll(toRSQL(condition), EntityData.class);

        assertThat(foundEntities).isEmpty();
    }

    @Test
    @Rollback
    public void shouldFindEntitiesWhileQueryingWithRegex() {
        persistEntity(newEntity);
        Condition<Entities> condition = Entities.suchThat().keyValues().like("%trin%");

        List<EntityData> foundEntities = entityRepository.queryAll(toRSQL(condition), EntityData.class);

        assertThat(foundEntities).containsExactly(newEntity);
    }

    @Test
    @Rollback
    public void shouldGetEntityForSystemAndKeyValues() {
        persistEntity(newEntity);
        createAndPersistEntityHistory(newEntity, newEntity.getPartition(), createSchema(ENTITY_SCHEMA_1),
                TEST_RECORD_TIME);

        Long systemId = newEntity.getPartition().getSystem().getId();

        Condition<Entities> condition = Entities.suchThat().systemId().eq(systemId).and().keyValues()
                .eq(ENTITY_KEY_VALUES_JSON);

        EntityData foundEntity = entityRepository.queryOne(toRSQL(condition), EntityData.class)
                .orElseThrow(() -> new IllegalArgumentException("No such entity"));

        assertThat(foundEntity.getKeyValues()).isEqualTo(ENTITY_KEY_VALUES_JSON);
        assertThat(foundEntity.getPartition().getKeyValues()).isEqualTo(PARTITION_KEY_VALUES_JSON_1);
        assertThat(foundEntity.getPartition().getSystem().getName()).isEqualTo(TEST_NAME);

        assertThat(foundEntity.getEntityHistories()).hasSize(1);
    }

    @Test
    @Rollback
    public void shouldGetEntityForSystemAndKeyValuesLike() {
        persistEntity(newEntity);
        createAndPersistEntityHistory(newEntity, newEntity.getPartition(), createSchema(ENTITY_SCHEMA_1),
                TEST_RECORD_TIME);

        Long systemId = newEntity.getPartition().getSystem().getId();

        Condition<Entities> condition = Entities.suchThat().systemId().eq(systemId).and().keyValues()
                .like(ENTITY_KEY_VALUES_JSON_WITH_WILDCARD);

        List<EntityData> entities = entityRepository.queryAll(toRSQL(condition), EntityData.class);

        assertThat(entities).isNotEmpty();
        assert (entities.size() == 1);
        EntityData foundEntity = entities.stream().findFirst().get();

        assertThat(foundEntity.getKeyValues()).isEqualTo(ENTITY_KEY_VALUES_JSON);
        assertThat(foundEntity.getPartition().getKeyValues()).isEqualTo(PARTITION_KEY_VALUES_JSON_1);
        assertThat(foundEntity.getPartition().getSystem().getName()).isEqualTo(TEST_NAME);

        assertThat(foundEntity.getEntityHistories()).hasSize(1);
    }

    @Test
    @Rollback
    public void shouldNotGetEntityForSystemAndKeyValuesLike() {
        persistEntity(newEntity);
        createAndPersistEntityHistory(newEntity, newEntity.getPartition(), createSchema(ENTITY_SCHEMA_1),
                TEST_RECORD_TIME);

        Long systemId = newEntity.getPartition().getSystem().getId();

        Condition<Entities> condition = Entities.suchThat().systemId().eq(systemId).and().keyValues()
                .like(ENTITY_KEY_VALUES_JSON_WITH_WILDCARD_WRONG);
        List<EntityData> entities = entityRepository.queryAll(toRSQL(condition), EntityData.class);

        assertThat(entities).isEmpty();
    }

    @Test
    @Rollback
    public void shouldWriteEntityLockedUntilStampAsNull() {
        persistEntity(newEntity);
        newEntity.setLockedUntilStamp(null);
        entityRepository.save(newEntity);

        Optional<EntityData> optionalEntity = entityRepository.findById(newEntity.getId());

        assertThat(optionalEntity).isPresent();

        EntityData foundEntity = optionalEntity.get();

        assertThat(foundEntity.getLockedUntilStamp()).isNull();
        assertThat(foundEntity.getPartition().getKeyValues()).isEqualTo(PARTITION_KEY_VALUES_JSON_1);
        assertThat(foundEntity.getPartition().getSystem().getName()).isEqualTo(TEST_NAME);
    }
}
