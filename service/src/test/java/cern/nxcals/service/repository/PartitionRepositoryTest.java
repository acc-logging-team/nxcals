/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.repository;

import cern.nxcals.api.extraction.metadata.queries.Partitions;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.common.utils.KeyValuesUtils;
import cern.nxcals.service.BaseTest;
import cern.nxcals.service.domain.PartitionData;
import cern.nxcals.service.domain.PartitionPropertiesData;
import cern.nxcals.service.domain.SystemSpecData;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.google.common.collect.ImmutableMap;
import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.LongStream;

import static cern.nxcals.common.utils.KeyValuesUtils.convertMapIntoAvroSchemaString;
import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static cern.nxcals.service.rest.TestSchemas.ENTITY_SCHEMA_1;
import static cern.nxcals.service.rest.TestSchemas.TIME_SCHEMA;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Transactional(transactionManager = "jpaTransactionManager")
public class PartitionRepositoryTest extends BaseTest {


    public static final String PARTITION_STRING_SCHEMA_KEY_1 = "partition_string_1";
    public static final String PARTITION_DOUBLE_SCHEMA_KEY_1 = "partition_double_1";
    public static final String PARTITION_STRING_SCHEMA_KEY_2 = "partition_string_2";
    public static final String PARTITION_DOUBLE_SCHEMA_KEY_2 = "partition_double_2";


    public static final Schema PARTITION_SCHEMA_1 = SchemaBuilder.record("partition_type_1").fields()
            .name(PARTITION_STRING_SCHEMA_KEY_1).type().stringType().noDefault()
            .name(PARTITION_DOUBLE_SCHEMA_KEY_1).type().doubleType().noDefault()
            .endRecord();

    public static final Map<String, Object> PARTITION_KEY_VALUES_1 = ImmutableMap
            .of(PARTITION_STRING_SCHEMA_KEY_1, "string_1", PARTITION_DOUBLE_SCHEMA_KEY_1, 1d);

    public static final String PARTITION_KEY_VALUES_STRING_1 = convertMapIntoAvroSchemaString(PARTITION_KEY_VALUES_1,
            PARTITION_SCHEMA_1.toString());

    public static final Map<String, Object> PARTITION_KEY_VALUES_2 = ImmutableMap
            .of(PARTITION_STRING_SCHEMA_KEY_1, "string11", PARTITION_DOUBLE_SCHEMA_KEY_1, 1d);


    public static final String PARTITION_KEY_VALUES_JSON_1 = KeyValuesUtils
            .convertMapIntoAvroSchemaString(PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1.toString());




    private PartitionData newPartition;
    private PartitionData newPartition2;



    @BeforeEach
    public void setUp() {
        setAuthentication();

        SystemSpecData system = createAndPersistSystemData(TEST_NAME, ENTITY_SCHEMA_1, PARTITION_SCHEMA_1, TIME_SCHEMA);
        newPartition = createPartition(system, PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);
        newPartition2 = createPartition(system, PARTITION_KEY_VALUES_2, PARTITION_SCHEMA_1);
    }

    @Test
    @Rollback
    public void shouldNotFindPartition() {
        Condition<Partitions> queryCondition = Partitions.suchThat().systemId().eq(-1L).and().keyValues()
                .eq(PARTITION_KEY_VALUES_STRING_1);

        Optional<PartitionData> foundPartition = partitionRepository.queryOne(toRSQL(queryCondition), PartitionData.class);
        assertThat(foundPartition).isNotPresent();
    }

    @Test
    @Rollback
    public void shouldWorkWithLongLists() {
        Condition<Partitions> queryCondition = Partitions.suchThat().keyValues().exists().and()
                .id().in(LongStream.range(1, 1002).boxed().toArray(Long[]::new));
        partitionRepository.queryAll(toRSQL(queryCondition), PartitionData.class);
    }

    @Test
    @Rollback
    public void shouldGetOnePartitionForSystemAndKeyValues() {
        //given
        //Saving two similar partitions to check if eq works correctly
        partitionRepository.save(newPartition);
        partitionRepository.save(newPartition2);
        Long systemId = newPartition.getSystem().getId();

        Condition<Partitions> queryCondition = Partitions.suchThat().systemId().eq(systemId)
                .and().keyValues().eq(PARTITION_KEY_VALUES_STRING_1);
        //when
        PartitionData foundPartition = partitionRepository.queryOne(toRSQL(queryCondition), PartitionData.class).orElseThrow(() -> new IllegalArgumentException("No such partition"));

        //then
        assertThat(foundPartition.getSystem().getName()).isEqualTo(TEST_NAME);
        assertThat(foundPartition.getKeyValues()).isEqualTo(PARTITION_KEY_VALUES_STRING_1);
    }

    @Test
    @Rollback
    public void shouldGetTwoPartitionForSystemAndKeyValues() {
        //given
        //Saving two similar partitions to check if eq works correctly
        partitionRepository.save(newPartition);
        partitionRepository.save(newPartition2);
        Long systemId = newPartition.getSystem().getId();

        Condition<Partitions> queryCondition = Partitions.suchThat().systemId().eq(systemId)
                .and().keyValues().like("%string_1%");
        //when
        List<PartitionData> foundPartition = partitionRepository.queryAll(toRSQL(queryCondition), PartitionData.class);

        //then
        assertThat(foundPartition.size()).isEqualTo(2);
    }


    @Test
    @Rollback
    public void shouldNotCreatePartitionWithoutSystem() {
        partitionRepository.save(createPartition((SystemSpecData) null, PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1));
        assertThrows(ConstraintViolationException.class, () -> entityManager.flush());
    }

    @Test
    @Rollback
    public void shouldNotCreatePartitionWithoutKeyValues() {
        partitionRepository.save(createPartition(TEST_NAME, null, null));
        assertThrows(ConstraintViolationException.class, () -> entityManager.flush());
    }

    @Test
    @Rollback
    public void shouldCreatePartitionKey() {
        PartitionData partition = createPartition(TEST_NAME, PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1);
        Long partitionId = partitionRepository.save(partition).getId();

        Optional<PartitionData> optionalPartition = partitionRepository.findById(partitionId);

        assertThat(optionalPartition).isPresent();

        PartitionData foundPartition = optionalPartition.get();

        assertThat(foundPartition.getKeyValues()).isEqualTo(PARTITION_KEY_VALUES_JSON_1);
        assertThat(foundPartition.getSystem().getName()).isEqualTo(TEST_NAME);
    }

    @Test
    @Rollback
    public void shouldCreatePartitionWithProperties() {
        PartitionData partition = createPartitionWithProps(TEST_NAME, PARTITION_KEY_VALUES_1, PARTITION_SCHEMA_1,
                new PartitionPropertiesData(true, TimeUtils.TimeSplits.TEN_MINUTE_SPLIT.name(),
                        TimeUtils.getInstantFromString("2024-08-01 00:00:00.000"),
                        TimeUtils.getInstantFromString("2024-08-02 00:00:00.000")));

        Optional<PartitionData> found = partitionRepository.findById(partition.getId());

        assertThat(found).isPresent();

        PartitionData foundPartition = found.get();

        assertThat(foundPartition.getProperties().getUpdatable()).isEqualTo(true);
        assertThat(foundPartition.getProperties().getStagingPartitionType()).isEqualTo("TEN_MINUTE_SPLIT");
        assertThat(foundPartition.getProperties().getStagingPartitionTypeFromStamp())
                .isEqualTo(TimeUtils.getInstantFromString("2024-08-01 00:00:00.000"));
        assertThat(foundPartition.getProperties().getStagingPartitionTypeToStamp())
                .isEqualTo(TimeUtils.getInstantFromString("2024-08-02 00:00:00.000"));

    }
}
