package cern.nxcals.service.repository;

import cern.nxcals.service.BaseTest;
import cern.nxcals.service.domain.EntityData;
import cern.nxcals.service.domain.GroupData;
import cern.nxcals.service.domain.HierarchyData;
import cern.nxcals.service.domain.SystemSpecData;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Transactional(transactionManager = "jpaTransactionManager")
public class HierarchyRepositoryTest extends BaseTest {
    @Autowired
    private HierarchyRepository hierarchyRepository;
    private SystemSpecData system;
    private EntityData entityData;

    @BeforeEach
    public void before() {
        setAuthentication();

        entityData = createAndSaveDefaultTestEntityKey();
        system = entityData.getPartition().getSystem();
    }

    @Test
    @Rollback
    public void shouldNotAllowCreationOfLogicalHierarchyDuplicates() {
        String hierarchyName = "my hierarchy";
        String duplicateHierarchyName = "MY HIERARCHY";

        GroupData group1 = groupData("my-group-1", system);
        GroupData group2 = groupData("my-group-2", system);

        HierarchyData hierarchy = createHierarchy(group1, hierarchyName);
        HierarchyData savedHierarchy = hierarchyRepository.save(hierarchy);

        assertNotNull(savedHierarchy);
        assertNotNull(savedHierarchy.getId());

        HierarchyData duplicateHierarchy = createHierarchy(group2, duplicateHierarchyName);
        hierarchyRepository.save(duplicateHierarchy);
        //explicitly flush changes to trigger unique db constraint rules
        assertThrows(javax.persistence.PersistenceException.class, () -> entityManager.flush());
    }

    private HierarchyData createHierarchy(GroupData groupData, String hierarchyName) {
        HierarchyData hierarchyData = new HierarchyData();
        hierarchyData.setName(hierarchyName);
        hierarchyData.setGroup(groupData);
        hierarchyData.setSystem(system);

        return hierarchyData;
    }
}
