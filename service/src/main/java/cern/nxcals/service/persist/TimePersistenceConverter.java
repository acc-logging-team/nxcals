/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.persist;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Timestamp;
import java.time.Instant;

@Converter(autoApply = true)
public class TimePersistenceConverter implements AttributeConverter<Instant, Timestamp> {
    @Override
    public Timestamp convertToDatabaseColumn(Instant date) {
        return date == null ? null : Timestamp.from(date);
    }

    @Override
    public Instant convertToEntityAttribute(Timestamp date) {
        return date == null ? null : date.toInstant();
    }

}