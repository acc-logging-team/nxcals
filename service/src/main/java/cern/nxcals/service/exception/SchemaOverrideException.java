/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.exception;

public class SchemaOverrideException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public SchemaOverrideException() {
        super();
    }

    /**
     * @param message
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public SchemaOverrideException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    /**
     * @param message
     * @param cause
     */
    public SchemaOverrideException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @param message
     */
    public SchemaOverrideException(String message) {
        super(message);
    }

    /**
     * @param cause
     */
    public SchemaOverrideException(Throwable cause) {
        super(cause);
    }

}
