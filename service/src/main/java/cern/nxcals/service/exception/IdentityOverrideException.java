/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.exception;

/**
 * @author Marcin Sobieszek
 * @date Jul 19, 2016 10:36:49 AM
 */
public class IdentityOverrideException extends RuntimeException {
    private static final long serialVersionUID = 3555183832976314889L;

    public IdentityOverrideException(String message) {
        super(message);
    }

}