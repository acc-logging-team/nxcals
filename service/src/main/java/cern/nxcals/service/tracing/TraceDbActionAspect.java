package cern.nxcals.service.tracing;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Slf4j
@Aspect
@Component
public class TraceDbActionAspect {

    private final PersistenceContextHandler persistenceContextHandler;

    public TraceDbActionAspect(PersistenceContextHandler persistenceContextHandler) {
        this.persistenceContextHandler = persistenceContextHandler;
    }

    @Pointcut("execution (* org.springframework.data.repository.*+.save*(..)) || "
            + "execution (* org.springframework.data.repository.*+.delete*(..))")
    @SuppressWarnings("squid:S1186") //Methods should not be empty
    public void executeTransaction() {
    }

    @Before("executeTransaction()")
    public void setInfo(){
        this.persistenceContextHandler.startAction();
    }

}
