package cern.nxcals.service.tracing;

import cern.nxcals.api.exceptions.UnauthorizedException;
import cern.rbac.common.LocationPrincipal;
import lombok.extern.slf4j.Slf4j;
import oracle.jdbc.internal.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@Transactional(transactionManager = "jpaTransactionManager")
public class PersistenceContextHandler {

    @Qualifier("jdbcTemplate")
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public PersistenceContextHandler(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     *  This method is called before pointcut {@link TraceDbActionAspect#executeTransaction()}.
     *  It sets up client information so that changelog trigger in DB can obtain it.
     *  Please, note that we cannot clean this info after pointcut as the DB trigger can be fired afterwards
     *  and won't be able to access info in such case.
     */
    public void startAction() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth == null) {
            throw new UnauthorizedException(
                    "No authentication was found while tracing database action. Please, ensure that you are authenticated properly.");
        }
        else {
            UserDetails user = (UserDetails) auth.getPrincipal();

            Object details = auth.getDetails();
            String remoteAddress;
            //Maybe we should consider logging just the details object, but must extend the ClientInfo field in the DB.
            //We could avoid this ugly cast.
            if (details instanceof WebAuthenticationDetails) {
                WebAuthenticationDetails authDetails = (WebAuthenticationDetails) details;
                remoteAddress = authDetails.getRemoteAddress();
            } else if (details instanceof LocationPrincipal) {
                //RBAC Type
                LocationPrincipal authDetails = (LocationPrincipal) details;
                //This format is hostname/ip (sometimes host is empty)
                remoteAddress = authDetails.getAddress().toString();
            } else {
                //fallback
                remoteAddress = details.toString();
            }

            String userInfo = user.getUsername() + " [" + remoteAddress + "]";
            SimpleJdbcCall sessionJdbcCall = new SimpleJdbcCall(jdbcTemplate);
            sessionJdbcCall = sessionJdbcCall
                    .withProcedureName("dbms_application_info.set_client_info")
                    .withoutProcedureColumnMetaDataAccess()
                    .declareParameters(
                            new SqlParameter("client_info", OracleTypes.VARCHAR));

            SqlParameterSource parameters = new MapSqlParameterSource()
                    .addValue("client_info", userInfo);
            sessionJdbcCall.execute(parameters);

            log.debug("Trace DB action pointcut was triggered. Setting client_info: " + userInfo);
        }
    }
}
