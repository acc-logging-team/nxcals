package cern.nxcals.service.tracing;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.RemovalCause;
import com.google.common.annotations.VisibleForTesting;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.CommonsRequestLoggingFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.StringTokenizer;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.ToLongFunction;
import java.util.stream.Collectors;

/**
 * A filter that prints the request statistics.
 */
@Slf4j
@Getter
@Setter
public class TracingRequestFilter extends CommonsRequestLoggingFilter {
    private int maxTopElements = 100;
    private Duration inactivityDuration = Duration.of(2, ChronoUnit.HOURS);
    private boolean removeUserRealm = true;
    private Duration displayStatsFreq = Duration.of(60, ChronoUnit.SECONDS);

    private final Runnable onPeriodicStats;
    private final Cache<String, AtomicLong> accessCache = createCache();
    private final ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);

    @Value("${tracing.include-call-details}")
    private boolean isIncludeCallDetails;

    @VisibleForTesting
    TracingRequestFilter(Runnable onPeriodicStats) {
        super();
        this.onPeriodicStats = onPeriodicStats;

    }

    public TracingRequestFilter() {
        super();
        this.onPeriodicStats = this::printCache;
    }

    public void init() {
        scheduledExecutorService
                .scheduleAtFixedRate(onPeriodicStats, displayStatsFreq.toMillis(), displayStatsFreq.toMillis(),
                        TimeUnit.MILLISECONDS);
    }

    private void printCache() {
        try {
            if (log.isDebugEnabled()) {
                log.debug("STATS: top {} user@host:/uri access points with count [{}]", maxTopElements,
                        getTopEntries());
            }
        } catch (Exception ex) {
            log.error("Cannot display cache", ex);
        }

    }

    @VisibleForTesting
    List<Map.Entry<String, AtomicLong>> getTopEntries() {
        return accessCache.asMap().entrySet().stream()
                .sorted(getComparator())
                .limit(maxTopElements).collect(Collectors.toList());
    }

    //Had to do this method splits as Idea is lost in type inference with reversed() comparator.
    private Comparator<Map.Entry<String, AtomicLong>> getComparator() {
        return Comparator.comparingLong(getEntryToLongFunction()).reversed();
    }

    private ToLongFunction<Map.Entry<String, AtomicLong>> getEntryToLongFunction() {
        return e -> e.getValue().get();
    }

    private Cache<String, AtomicLong> createCache() {
        return Caffeine.newBuilder()
                .removalListener(this::onCacheRemoval)
                .expireAfterAccess(inactivityDuration)
                .build();
    }

    private void onCacheRemoval(String key, AtomicLong value, RemovalCause removalCause) {
        log.info("Removing cached request counter {} with value {} and removal cause {} after {} inactivity", key,
                value, removalCause,
                inactivityDuration);
    }

    @Override
    protected void beforeRequest(HttpServletRequest request, String message) {
        //nothing
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        logOccurence(request);

        super.doFilterInternal(request, response, filterChain);
    }

    private void logOccurence(HttpServletRequest request) {
        try {
            String endPoint = getEndPoint(request);
            Objects.requireNonNull(accessCache.get(endPoint, key -> new AtomicLong(0))).incrementAndGet();
        } catch (Exception ex) {
            logger.error("Cannot log tracing occurence", ex);
        }
    }

    private String getEndPoint(HttpServletRequest request) {
        return removeRealm(request.getRemoteUser()) + "@" + request.getRemoteAddr() + ":" + request.getRequestURI();
    }

    private String removeRealm(String user) {
        if (this.removeUserRealm && StringUtils.hasLength(user)) {
            StringTokenizer tokenizer = new StringTokenizer(user, "@");
            return tokenizer.nextToken();
        } else {
            return user;
        }
    }

    /**
     * Override as we don't need a long session id in the output, impossible to mute in the original class.
     * Also it is more readable to use space as field separator instead of semicolon.
     *
     * @param request
     * @param prefix
     * @param suffix
     * @return
     */
    @Override
    protected String createMessage(HttpServletRequest request, String prefix, String suffix) {
        StringBuilder msg = new StringBuilder();
        msg.append(prefix);
        msg.append("uri=").append(request.getRequestURI());

        if (isIncludeQueryString()) {
            String queryString = request.getQueryString();
            if (queryString != null) {
                msg.append('?').append(queryString);
            }
        }

        if (isIncludeClientInfo()) {
            String remoteAddr = request.getRemoteAddr();
            if (StringUtils.hasLength(remoteAddr)) {
                msg.append(", host=").append(remoteAddr);
            }
            String user = request.getRemoteUser();
            if (user != null) {
                msg.append(", user=").append(user);
            }
        }

        if (isIncludeHeaders()) {
            msg.append(", headers=").append(new ServletServerHttpRequest(request).getHeaders());
        }

        if (isIncludePayload()) {
            String payload = getMessagePayload(request);
            if (payload != null) {
                msg.append(", payload=").append(payload);
            }
        }

        if (isIncludeCallDetails()) {

            HttpHeaders headers = new ServletServerHttpRequest(request).getHeaders();

            msg.append(", application_name=").append(addBrackets(headers.getFirst("application-name")));
            msg.append(", declaring_class=").append(addBrackets(headers.getFirst("declaring-class")));
            msg.append(", method_name=").append(addBrackets(headers.getFirst("method-name")));
        }

        msg.append(suffix);
        return msg.toString();
    }

    private String addBrackets(String text) {
        return "<" + text + ">";
    }
}
