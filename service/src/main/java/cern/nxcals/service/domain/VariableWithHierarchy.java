package cern.nxcals.service.domain;

import lombok.Data;

@Data
public class VariableWithHierarchy<T> {
    private final Long variableId;
    private final T hierarchy;
}
