/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.domain;

import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.common.utils.ReflectionUtils;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import java.util.concurrent.TimeUnit;

import static cern.nxcals.service.domain.SequenceType.SYSTEM;

@Entity
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@Table(name = "SYSTEMS")
@Setter
@Getter
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
@SuppressWarnings("squid:S2160") //override equals while we do it in the base class - jwozniak
public class SystemSpecData extends StandardPersistentEntityWithVersion {
    private static final long serialVersionUID = 1205018780230518529L;
    private static final Cache<Long, SystemSpec> cache = Caffeine.newBuilder().expireAfterWrite(2, TimeUnit.HOURS).build();

    @NotNull
    private String name;

    @NotNull
    private String entityKeyDefs;

    @NotNull
    private String partitionKeyDefs;

    @NotNull
    private String timeKeyDefs;

    private String recordVersionKeyDefs;

    @Id
    @Column(name = "SYSTEM_ID")
    @Override
    @EqualsAndHashCode.Include
    public Long getId() {
        return super.getId();
    }

    @Override
    protected SequenceType sequenceType() {
        return SYSTEM;
    }

    @Column(name = "SYSTEM_NAME")
    public String getName() {
        return name;
    }

    @Column(nullable = false)
    public void setEntityKeyDefs(String value) {
        entityKeyDefs = value;
    }

    public SystemSpec toSystemSpec() {
        return cache.get(getId(), this::getSystemSpec);
    }

    private SystemSpec getSystemSpec(Long id) {
        return ReflectionUtils.builderInstance(SystemSpec.InnerBuilder.class)
                .id(id)
                .name(getName())
                .entityKeyDefinitions(getEntityKeyDefs())
                .partitionKeyDefinitions(getPartitionKeyDefs())
                .timeKeyDefinitions(getTimeKeyDefs())
                .recordVersionKeyDefinitions(getRecordVersionKeyDefs())
                .build();
    }
}
