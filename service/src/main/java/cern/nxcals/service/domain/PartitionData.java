/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.domain;

import cern.nxcals.api.domain.Partition;
import cern.nxcals.common.utils.ReflectionUtils;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Set;

import static cern.nxcals.common.utils.KeyValuesUtils.convertKeyValuesStringIntoMap;
import static cern.nxcals.service.domain.SequenceType.PARTITION;

@Entity
@Table(name = "PARTITIONS")
@SuppressWarnings("squid:S2160") //override equals while we do it in the base class - jwozniak
@Setter
public class PartitionData extends StandardPersistentEntityWithVersion {
    public static final String WITH_SYSTEM = "partition-with-system";
    private static final long serialVersionUID = 6293229615370043778L;

    @NotNull
    private String keyValues;

    @NotNull
    private SystemSpecData system;

    private Set<EntityData> entities;

    @Getter
    @Embedded
    private PartitionPropertiesData properties;

    @Id
    @Column(name = "PARTITION_ID")
    @Override
    public Long getId() {
        return super.getId();
    }

    @Override
    protected SequenceType sequenceType() {
        return PARTITION;
    }

    public String getKeyValues() {
        return keyValues;
    }

    @OneToMany(mappedBy = "partition")
    public Set<EntityData> getEntities() {
        return entities;
    }

    @ManyToOne
    @JoinColumn(name = "system_id")
    public SystemSpecData getSystem() {
        return system;
    }

    public Partition toPartition() {
        return ReflectionUtils.builderInstance(Partition.InnerBuilder.class).id(getId())
                .systemSpec(system.toSystemSpec())
                .keyValues(convertKeyValuesStringIntoMap(keyValues)).properties(properties.toPartitionProperties())
                .recVersion(getRecVersion()).build();
    }

    //Setting default value
    @PrePersist
    public void prePersist() {
        if (properties == null) {
            properties = PartitionPropertiesData.empty();
        }
    }

}
