/*
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.domain;

import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.common.utils.ReflectionUtils;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.Instant;

@Entity
@Setter
@Getter
@Table(name = "VARIABLE_CONFIGS")
@Access(value = AccessType.FIELD)
@SuppressWarnings("squid:S2160") //override equals while we do it in the base class - jwozniak
public class VariableConfigData extends StandardPersistentEntityWithVersion {
    public static final String WITH_ENTITY = "configs-with-entities";
    private static final long serialVersionUID = 3665546057719893699L;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "variable_id")
    private VariableData variable;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "entity_id")
    private EntityData entity;

    private String fieldName;
    private Instant validFromStamp;
    private Instant validToStamp;
    private String predicate;

    @Id
    @Column(name = "variable_config_id")
    @Access(value = AccessType.PROPERTY)
    @Override
    public Long getId() {
        return super.getId();
    }

    public VariableConfig toVariableConfig() {
        return ReflectionUtils.builderInstance(VariableConfig.InnerBuilder.class)
                .id(getId())
                .entityId(getEntity().getId())
                .fieldName(getFieldName())
                .variableName(getVariable().getVariableName())
                .validity(validity())
                .recVersion(getRecVersion())
                .predicate(getPredicate())
                .build();
    }

    public TimeWindow validity() {
        return TimeWindow.between(getValidFromStamp(), getValidToStamp());
    }
}
