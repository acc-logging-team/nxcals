/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.domain;

import lombok.Setter;
import lombok.ToString;

import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

/**
 * Base class for all persistent objects which are to be versioned {@see JPA's optimistic locking facility}.
 *
 * @author Marcin Sobieszek
 * @date Jul 19, 2016 10:40:26 AM
 */
@Setter
@MappedSuperclass
@ToString(callSuper = true)
@SuppressWarnings("squid:S2160") //override equals while we do it in the base class - jwozniak
public abstract class StandardPersistentEntityWithVersion extends StandardPersistentEntity {

    private static final long serialVersionUID = 3210332890603414023L;
    private Long recVersion;

    @Version
    public Long getRecVersion() {
        return recVersion;
    }
}
