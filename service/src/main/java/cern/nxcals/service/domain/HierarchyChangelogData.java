package cern.nxcals.service.domain;

import cern.nxcals.api.domain.HierarchyChangelog;
import cern.nxcals.api.domain.OperationType;
import cern.nxcals.common.utils.ReflectionUtils;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.hibernate.annotations.Immutable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.Instant;

@Entity
@Getter
@Table(name = "HIERARCHIES_CHANGELOG")
@Immutable
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class HierarchyChangelogData implements Serializable {
    private static final long serialVersionUID = 3665546057712843699L;

    @Id
    @EqualsAndHashCode.Include
    private long id;
    private long hierarchyId;
    private String oldHierarchyName;
    private String newHierarchyName;
    private Long oldParentId;
    private Long newParentId;
    private Long oldSystemId;
    private Long newSystemId;
    private OperationType opType;
    private Instant createTimeUtc;
    private String transactionId;
    private String module;
    private String action;
    private String clientInfo;
    private Long recVersion;

    public HierarchyChangelog toHierarchyChangelog() {
        return ReflectionUtils.builderInstance(HierarchyChangelog.InnerBuilder.class).id(getId())
                .hierarchyId(getHierarchyId())
                .oldHierarchyName(getOldHierarchyName()).newHierarchyName(getNewHierarchyName())
                .oldParentId(getOldParentId()).newParentId(getNewParentId())
                .oldSystemId(getOldSystemId()).newSystemId(getNewSystemId())
                .createTimeUtc(getCreateTimeUtc()).opType(getOpType())
                .clientInfo(getClientInfo())
                .build();
    }
}
