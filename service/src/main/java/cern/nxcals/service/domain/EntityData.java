/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.domain;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.EntityHistory;
import cern.nxcals.common.utils.ReflectionUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.NamedSubgraph;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

import static cern.nxcals.common.utils.KeyValuesUtils.convertKeyValuesStringIntoMap;
import static cern.nxcals.service.domain.SequenceType.ENTITY;

@NamedEntityGraphs({
        @NamedEntityGraph(
                name = EntityData.WITH_PARTITION,
                attributeNodes = {
                        @NamedAttributeNode("keyValues"),
                        @NamedAttributeNode(value = "partition", subgraph = PartitionData.WITH_SYSTEM),

                },
                subgraphs = {
                        @NamedSubgraph(
                                name = PartitionData.WITH_SYSTEM,
                                attributeNodes = {
                                        @NamedAttributeNode("system")
                                }
                        )
                }

        )
})
@ToString
@Setter
@Getter
@javax.persistence.Entity
@Table(name = "ENTITIES")
@SuppressWarnings({ "squid:S2160", "squid:S1710" }) //override equals while we do it in the base class - jwozniak
public class EntityData extends StandardPersistentEntityWithVersion {
    public static final String WITH_PARTITION = "entity-with-partition";
    private static final long serialVersionUID = -6802891913563005210L;

    @NotNull
    private String keyValues;

    @NotNull
    private PartitionData partition;

    @ToString.Exclude
    private SortedSet<EntityHistoryData> entityHistories = new TreeSet<>();
    private transient Instant lockedUntilStamp;

    @Id
    @Column(name = "ENTITY_ID")
    @Override
    public Long getId() {
        return super.getId();
    }

    @Override
    protected SequenceType sequenceType() {
        return ENTITY;
    }

    @ManyToOne
    @JoinColumns({ @JoinColumn(name = "system_id", referencedColumnName = "system_id"),
            @JoinColumn(name = "partition_id", referencedColumnName = "partition_id") })
    public PartitionData getPartition() {
        return partition;
    }

    @Column(name = "kafka_partition", updatable = false, insertable = false)
    private Integer kafkaPartition;

    @Transient
    public SortedSet<EntityHistoryData> getEntityHistories() {
        return entityHistories;
    }

    public boolean addEntityHist(EntityHistoryData entityHistory) {
        if (entityHistories.isEmpty()) {
            entityHistories = new TreeSet<>();
        }
        entityHistory.setEntity(this);
        return entityHistories.add(entityHistory);
    }

    public Entity toEntity() {
        Entity entity = ReflectionUtils.builderInstance(Entity.InnerBuilder.class)
                .id(getId())
                .entityKeyValues(convertKeyValuesStringIntoMap(keyValues))
                .systemSpec(getPartition().getSystem().toSystemSpec())
                .partition(partition.toPartition())
                .lockedUntilStamp(lockedUntilStamp)
                .recVersion(getRecVersion())
                .kafkaPartition(kafkaPartition)
                .build();
        return entity.toBuilder().entityHistory(createEntityHistoryData(entity)).build();
    }

    private SortedSet<EntityHistory> createEntityHistoryData(Entity entity) {
        return getEntityHistories().stream().map(eh -> eh.toEntityHist(entity))
                .collect(Collectors.toCollection(TreeSet::new));
    }

    public void clearHistory() {
        if (!entityHistories.isEmpty()) {
            entityHistories.clear();
        }
    }
}
