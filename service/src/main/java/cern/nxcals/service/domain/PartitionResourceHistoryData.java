package cern.nxcals.service.domain;

import cern.nxcals.api.domain.PartitionResourceHistory;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.common.utils.ReflectionUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.Instant;

@Entity
@Table(name = "PARTITION_RESOURCE_HISTS")
@Setter
@Getter
@Access(value = AccessType.FIELD)
@ToString
@SuppressWarnings("squid:S2160") //override equals while we do it in the base class
public class PartitionResourceHistoryData extends StandardPersistentEntityWithVersion {

    @NotNull
    @ManyToOne
    @JoinColumn(name = "partition_resource_id")
    private PartitionResourceData partitionResource;

    //TODO - rename to partitionType
    @NotNull
    @Column(name = "partition_resource_information")
    private String information;

    @NotNull
    @Column(name = "compaction_type")
    private String compactionType;

    @NotNull
    @Column(name = "storage_type")
    private String storageType;

    @NotNull
    @Column(name = "is_fixed")
    private boolean isFixedSettings;

    @NotNull
    @Column(name = "valid_from_stamp")
    private Instant validFromStamp;

    @NotNull
    @Column(name = "valid_to_stamp")
    private Instant validToStamp;

    @Id
    @Column(name = "partition_resource_hist_id")
    @Access(value = AccessType.PROPERTY)
    @Override
    public Long getId() {
        return super.getId();
    }

    public TimeWindow validity() {
        return TimeWindow.between(getValidFromStamp(), getValidToStamp());
    }

    @Override
    protected SequenceType sequenceType() {
        return SequenceType.PARTITION_RESOURCE_INFO;
    }

    public PartitionResourceHistory toPartitionResourceHistory() {
        return ReflectionUtils.builderInstance(PartitionResourceHistory.InnerBuilder.class)
                .id(getId())
                .partitionResource(getPartitionResource().toPartitionResource())
                .partitionInformation(getInformation())
                .compactionType(getCompactionType())
                .storageType(getStorageType())
                .isFixedSettings(isFixedSettings())
                .validity(validity())
                .recVersion(getRecVersion()).build();
    }
}
