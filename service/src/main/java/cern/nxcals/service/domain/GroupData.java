package cern.nxcals.service.domain;

import cern.nxcals.api.domain.Group;
import cern.nxcals.common.utils.ReflectionUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.builder.ToStringExclude;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.MapKeyColumn;
import javax.persistence.MapsId;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.NamedSubgraph;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static avro.shaded.com.google.common.base.Objects.firstNonNull;
import static cern.nxcals.service.domain.GroupData.EMPTY_ASSOCIATION;
import static cern.nxcals.service.domain.SequenceType.GROUP;

@NamedEntityGraphs({
        @NamedEntityGraph(
                name = GroupData.WITH_VARIABLES,
                attributeNodes = {
                        @NamedAttributeNode("system"),
                        @NamedAttributeNode("properties"),
                        @NamedAttributeNode(value = "variables", subgraph = VariableDataWithAssociation.WITH_VARIABLE),
                },
                subgraphs = {
                        @NamedSubgraph(
                                name = VariableDataWithAssociation.WITH_VARIABLE,
                                attributeNodes = {
                                        @NamedAttributeNode(value = "variable", subgraph = VariableData.WITH_CONFIGS)
                                }
                        ),
                        @NamedSubgraph(
                                name = VariableData.WITH_CONFIGS,
                                attributeNodes = {
                                        @NamedAttributeNode("system"),
                                        @NamedAttributeNode(value = "variableConfigs", subgraph = VariableConfigData.WITH_ENTITY),
                                }
                        ),
                        @NamedSubgraph(
                                name = VariableConfigData.WITH_ENTITY,
                                attributeNodes = {
                                        @NamedAttributeNode(value = "variable"),
                                        @NamedAttributeNode(value = "entity", subgraph = EntityData.WITH_PARTITION)
                                }
                        ),
                        @NamedSubgraph(
                                name = EntityData.WITH_PARTITION,
                                attributeNodes = {
                                        @NamedAttributeNode(value = "partition", subgraph = PartitionData.WITH_SYSTEM)
                                }
                        ),
                        @NamedSubgraph(
                                name = PartitionData.WITH_SYSTEM,
                                attributeNodes = {
                                        @NamedAttributeNode("system")
                                }
                        )
                }
        ),
        @NamedEntityGraph(
                name = GroupData.WITH_ENTITIES,
                attributeNodes = {
                        @NamedAttributeNode("system"),
                        @NamedAttributeNode("properties"),
                        @NamedAttributeNode(value = "entities", subgraph = EntityDataWithAssociation.WITH_ENTITY)
                },
                subgraphs = {
                        @NamedSubgraph(
                                name = EntityDataWithAssociation.WITH_ENTITY,
                                attributeNodes = {
                                        @NamedAttributeNode(value = "entity", subgraph = EntityData.WITH_PARTITION)
                                }
                        ),
                        @NamedSubgraph(
                                name = VariableConfigData.WITH_ENTITY,
                                attributeNodes = {
                                        @NamedAttributeNode(value = "variable"),
                                        @NamedAttributeNode(value = "entity", subgraph = EntityData.WITH_PARTITION)
                                }
                        ),
                        @NamedSubgraph(
                                name = EntityData.WITH_PARTITION,
                                attributeNodes = {
                                        @NamedAttributeNode(value = "partition", subgraph = PartitionData.WITH_SYSTEM)
                                }
                        ),
                        @NamedSubgraph(
                                name = PartitionData.WITH_SYSTEM,
                                attributeNodes = {
                                        @NamedAttributeNode("system")
                                }
                        )
                }
        ),
        @NamedEntityGraph(
                name = GroupData.PLAIN,
                attributeNodes = {
                        @NamedAttributeNode("system"),
                        @NamedAttributeNode("properties"),
                }
        ),
        @NamedEntityGraph(
                name = GroupData.MINIMUM,
                attributeNodes = {
                        @NamedAttributeNode("system"),
                }
        )
})
@Setter
@Getter
@Entity
@Table(name = "groups")
@ToString
@Access(value = AccessType.FIELD)
@SuppressWarnings({ "squid:S2160", "squid:S1710" }) //override equals while we do it in the base class
public class GroupData extends StandardPersistentEntityWithVersion implements Ownable {
    public static final String WITH_ENTITIES = "group-with-entities";
    public static final String WITH_VARIABLES = "group-with-variables";
    public static final String MINIMUM = "group-minimum";
    public static final String PLAIN = "group-without-variables-and-entities";

    static final String EMPTY_ASSOCIATION = "__empty__association__";

    @NotNull
    private String name;
    private String description;

    @NonNull
    private VisibilityData visibility;
    private String label;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "system_id")
    private SystemSpecData system;

    @NotNull
    @OneToMany(mappedBy = "key.groupId", cascade = CascadeType.ALL, orphanRemoval = true)
    @ToStringExclude
    private Set<VariableDataWithAssociation> variables = new HashSet<>();

    @NotNull
    @OneToMany(mappedBy = "key.groupId", cascade = CascadeType.ALL, orphanRemoval = true)
    @ToStringExclude
    private Set<EntityDataWithAssociation> entities = new HashSet<>();

    @NotNull
    @ElementCollection
    @JoinTable(name = "group_properties", joinColumns = @JoinColumn(name = "group_id"))
    @MapKeyColumn(name = "name")
    @Column(name = "value")
    private Map<String, String> properties = new HashMap<>();

    private String owner;

    @Id
    @Column(name = "group_id", updatable = false)
    @Access(value =  AccessType.PROPERTY)
    @Override
    public Long getId() {
        return super.getId();
    }

    @Override
    protected SequenceType sequenceType() {
        return GROUP;
    }

    public Group toGroup() {
        return ReflectionUtils.builderInstance(Group.InnerBuilder.class)
                .id(getId())
                .recVersion(getRecVersion())
                .label(getLabel())
                .visibility(getVisibility().toVisibility())
                .owner(owner)
                .name(getName())
                .description(getDescription())
                .systemSpec(getSystem().toSystemSpec())
                .properties(getProperties()).build();
    }

    public void setAssociatedEntities(Map<String, Set<EntityData>> newState) {
        getEntities().removeIf(t -> !newState.containsKey(t.getKey().getAssociation()));
        getEntities().removeIf(t -> !newState.get(t.getKey().getAssociation()).contains(t.getEntity()));

        addAssociatedEntities(newState);
    }

    public void addAssociatedEntities(Map<String, Set<EntityData>> toAdd) {
        toAdd.keySet().forEach(association -> toAdd.get(association).forEach(entityData -> add(association, entityData)));
    }

    public void removeAssociatedEntities(Map<String, Set<EntityData>> toRemove) {
        getEntities().removeIf(t -> toRemove.getOrDefault(t.getKey().getAssociation(), Collections.emptySet()).contains(t.getEntity()));
    }

    public void add(String association, EntityData entityData) {
        getEntities().add(new EntityDataWithAssociation(new EntityGroupId(getId(), entityData.getId(), association), entityData));
    }

    public Map<String, Set<EntityData>> getAssociatedEntities() {
        Map<String, Set<EntityData>> result = new HashMap<>();
        getEntities().forEach(t -> collect(t.getKey().getAssociation(), t.getEntity(), result));
        return result;
    }

    public void setAssociatedVariables(Map<String, Set<VariableData>> newState) {
        getVariables().removeIf(t -> !newState.containsKey(t.getKey().getAssociation()));
        getVariables().removeIf(t -> !newState.get(t.getKey().getAssociation()).contains(t.getVariable()));

        addAssociatedVariables(newState);
    }

    public void addAssociatedVariables(Map<String, Set<VariableData>> toAdd) {
        toAdd.keySet().forEach(association -> toAdd.get(association).forEach(variableData -> add(association, variableData)));
    }

    public void removeAssociatedVariables(Map<String, Set<VariableData>> toRemove) {
        getVariables().removeIf(t -> toRemove.getOrDefault(t.getKey().getAssociation(), Collections.emptySet()).contains(t.getVariable()));
    }

    public void add(String association, VariableData variableData) {
        getVariables().add(new VariableDataWithAssociation(new VariableGroupId(getId(), variableData.getId(), association), variableData));
    }

    public Map<String, Set<VariableData>> getAssociatedVariables() {
        Map<String, Set<VariableData>> result = new HashMap<>();
        getVariables().forEach(t -> collect(t.getKey().getAssociation(), t.getVariable(), result));
        return result;
    }

    private <T> void collect(String key, T value, Map<String, Set<T>> acc) {
        acc.computeIfAbsent(key, k -> new HashSet<>()).add(value);
    }
}

@Entity
@Table(name = "group_entities")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
class EntityDataWithAssociation implements Serializable {
    public static final String WITH_ENTITY = "entity-association";
    @EmbeddedId
    @EqualsAndHashCode.Include
    private EntityGroupId key;

    @ManyToOne
    @MapsId("entity_id")
    @JoinColumn(name="entity_id")
    private EntityData entity;
}

@Entity
@Table(name = "group_variables")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
class VariableDataWithAssociation implements Serializable {
    public static final String WITH_VARIABLE = "variable-association";
    @EmbeddedId
    @EqualsAndHashCode.Include
    private VariableGroupId key;

    @ManyToOne
    @MapsId("variable_id")
    @JoinColumn(name="variable_id")
    private VariableData variable;
}

@Data
@NoArgsConstructor
@Embeddable
class EntityGroupId implements Serializable {
    @Column(name = "group_id")
    private long groupId;
    @Column(name = "entity_id")
    private long entityId;
    @NonNull
    @Column(name = "association_name")
    private String association;

    EntityGroupId(long groupId, long entityId, String association) {
        this.groupId = groupId;
        this.entityId = entityId;
        this.association = firstNonNull(association, EMPTY_ASSOCIATION);
    }

    String getAssociation() {
        return EMPTY_ASSOCIATION.equals(association) ? null : association;
    }
}

@Data
@NoArgsConstructor
@Embeddable
class VariableGroupId implements Serializable {
    @Column(name = "group_id")
    private long groupId;
    @Column(name = "variable_id")
    private long variableId;
    @NonNull
    @Column(name = "association_name")
    private String association;

    VariableGroupId(long groupId, long variableId, String association) {
        this.groupId = groupId;
        this.variableId = variableId;
        this.association = firstNonNull(association, EMPTY_ASSOCIATION);
    }

    String getAssociation() {
        return EMPTY_ASSOCIATION.equals(association) ? null : association;
    }
}
