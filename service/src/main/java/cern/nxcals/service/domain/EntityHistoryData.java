/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.domain;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.EntityHistory;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.common.utils.ReflectionUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.Comparator;

import static cern.nxcals.service.domain.SequenceType.ENTITY_HIST;
import static com.google.common.base.MoreObjects.firstNonNull;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;

@Setter
@Getter
@javax.persistence.Entity
@Table(name = "ENTITIES_HIST")
@ToString
@SuppressWarnings("squid:S2160") //override equals while we do it in the base class - jwozniak
public class EntityHistoryData extends StandardPersistentEntityWithVersion implements Comparable<EntityHistoryData> {
    private static final long serialVersionUID = -4406604195344770819L;

    @NotNull
    private PartitionData partition;
    private SystemSpecData system;
    @NotNull
    private transient Instant validFromStamp;
    private transient Instant validToStamp;

    @NotNull
    private EntitySchemaData schema;

    @NotNull
    private EntityData entity;

    @Id
    @Column(name = "ENTITY_HIST_ID")
    @Override
    public Long getId() {
        return super.getId();
    }

    @Override
    protected SequenceType sequenceType() {
        return ENTITY_HIST;
    }

    @ManyToOne
    @JoinColumn(name = "partition_id")
    public PartitionData getPartition() {
        return partition;
    }

    @Transient
    public boolean isLatest() {
        return validToStamp == null;
    }

    @ManyToOne(cascade = { PERSIST, MERGE })
    @JoinColumn(name = "entity_id")
    public EntityData getEntity() {
        return entity;
    }

    @ManyToOne(cascade = { PERSIST, MERGE })
    @JoinColumn(name = "schema_id")
    public EntitySchemaData getSchema() {
        return schema;
    }

    @ManyToOne
    @JoinColumn(name = "system_id")
    public SystemSpecData getSystem() {
        if (partition == null) {
            return null;
        }
        return partition.getSystem();
    }

    public void setSystem(SystemSpecData system) {
        // set through entity
    }

    public EntityHistory toEntityHist(Entity entity) {
        return ReflectionUtils.builderInstance(EntityHistory.InnerBuilder.class).id(getId()).entitySchema(schema.toEntitySchema())
                .partition(partition.toPartition()).validity(TimeWindow.between(validFromStamp, validToStamp))
                .entity(entity).build();
    }

    public TimeWindow validity() {
        return TimeWindow.between(getValidFromStamp(), getValidToStamp());
    }

    @Override
    @SuppressWarnings("squid:S1210") // override equals to comply with compareTo contract (we do it in base class)
    public int compareTo(EntityHistoryData o) {
        return Comparator.<Instant>reverseOrder()
                .compare(firstNonNull(validFromStamp, Instant.EPOCH), firstNonNull(o.validFromStamp, Instant.EPOCH));
    }
}
