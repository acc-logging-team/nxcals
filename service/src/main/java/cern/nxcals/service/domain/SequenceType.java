/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public enum SequenceType {
    SCHEMA("schema_seq"),
    ENTITY("entity_seq"),
    GROUP("group_seq"),
    GROUP_PROPERTY("group_property_seq"),
    PARTITION_RESOURCE("partition_resource_seq"),
    PARTITION_RESOURCE_INFO("partition_resource_info_seq"),
    ENTITY_HIST("entity_hist_seq"),
    PARTITION("partition_seq"),
    SYSTEM("system_seq"),
    VARIABLE("variable_seq"),
    HIERARCHY("hierarchy_seq"),
    DEFAULT("default_seq"),
    PERMISSION("permission_seq"),
    ROLE("role_seq"),
    USER("user_seq"),
    REALM("realm_seq");
    private final String seqName;
}