/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.domain;

import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableDeclaredType;
import cern.nxcals.common.utils.ReflectionUtils;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.NamedSubgraph;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import static java.util.stream.Collectors.toCollection;

@NamedEntityGraphs({
    @NamedEntityGraph(
            name = VariableData.WITH_CONFIGS,
            attributeNodes = {
                    @NamedAttributeNode("system"),
                    @NamedAttributeNode(value = "variableConfigs", subgraph = VariableConfigData.WITH_ENTITY),
            },
            subgraphs = {
                    @NamedSubgraph(
                            name = VariableConfigData.WITH_ENTITY,
                            attributeNodes = {
                                    @NamedAttributeNode(value = "variable"),
                                    @NamedAttributeNode(value = "entity", subgraph = EntityData.WITH_PARTITION)
                            }
                    ),
                    @NamedSubgraph(
                            name = EntityData.WITH_PARTITION,
                            attributeNodes = {
                                    @NamedAttributeNode(value = "partition", subgraph = PartitionData.WITH_SYSTEM)
                            }
                    ),
                    @NamedSubgraph(
                            name = PartitionData.WITH_SYSTEM,
                            attributeNodes = {
                                    @NamedAttributeNode("system")
                            }
                    )
            }
    ),
        @NamedEntityGraph(
                name = VariableData.WITHOUT_CONFIGS,
                attributeNodes = {
                        @NamedAttributeNode("system")
                },
                subgraphs = {
                        @NamedSubgraph(
                                name = PartitionData.WITH_SYSTEM,
                                attributeNodes = {
                                        @NamedAttributeNode("system")
                                }
                        )
                }
        ),
})
@Entity
@Setter
@Getter
@Table(name = "VARIABLES")
@Access(value = AccessType.FIELD)
@SuppressWarnings({ "squid:S2160", "squid:S1710" }) //override equals while we do it in the base class - jwozniak
public class VariableData extends StandardPersistentEntityWithVersion {
    public static final String WITH_CONFIGS = "variables-with-configs";
    public static final String WITHOUT_CONFIGS = "variables-without-configs";

    private static final long serialVersionUID = -8581950832501916844L;
    @NotNull
    private String variableName;
    @NotNull
    @ManyToOne
    @JoinColumn(name = "system_id")
    private SystemSpecData system;
    private String description;
    private String unit;

    @OneToMany(mappedBy = "variable", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    private Set<VariableConfigData> variableConfigs = new HashSet<>();

    @Enumerated(EnumType.STRING)
    @Column(length = 50, name = "DECLARED_TYPE")
    private VariableDeclaredType declaredType;

    @Id
    @Column(name = "VARIABLE_ID")
    @Access(value =  AccessType.PROPERTY)
    @Override
    public Long getId() {
        return super.getId();
    }

    @Override
    protected SequenceType sequenceType() {
        return SequenceType.VARIABLE;
    }

    public void addConfig(VariableConfigData variableConfig) {
        variableConfig.setVariable(this);
        variableConfigs.add(variableConfig);
    }

    public void removeConfig(VariableConfigData variableConfig) {
        variableConfigs.remove(variableConfig);
    }

    private Variable.InnerBuilder toVariableBuilderWithoutConfigs() {
        return ReflectionUtils.builderInstance(Variable.InnerBuilder.class)
                .id(getId())
                .variableName(getVariableName())
                .systemSpec(getSystem().toSystemSpec())
                .declaredType(getDeclaredType())
                .description(getDescription())
                .configs(new TreeSet<>())
                .unit(getUnit())
                .recVersion(getRecVersion());
    }

    public Variable toVariable() {
        return toVariableBuilderWithoutConfigs()
                .configs(getVariableConfigs().stream().map(VariableConfigData::toVariableConfig)
                        .collect(toCollection(TreeSet::new)))
                .build();
    }

    public Variable toVariableWithoutConfigs() {
        return toVariableBuilderWithoutConfigs().build();
    }
}
