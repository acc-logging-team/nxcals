package cern.nxcals.service.domain;

import cern.nxcals.api.domain.OperationType;
import cern.nxcals.api.domain.VariableConfigChangelog;
import cern.nxcals.common.utils.ReflectionUtils;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.hibernate.annotations.Immutable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.Instant;

@Entity
@Getter
@Table(name = "VARIABLE_CONFIGS_CHANGELOG")
@Immutable
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class VariableConfigChangelogData implements Serializable {
    private static final long serialVersionUID = 3665546057712893699L;

    @Id
    @EqualsAndHashCode.Include
    private long id;
    private long variableConfigId;
    private Long oldVariableId;
    private Long newVariableId;
    private Long oldEntityId;
    private Long newEntityId;
    private String oldFieldName;
    private String newFieldName;
    private Instant newValidFromStamp;
    private Instant oldValidFromStamp;
    private Instant oldValidToStamp;
    private Instant newValidToStamp;
    private OperationType opType;
    private Instant createTimeUtc;
    private String transactionId;
    private String module;
    private String action;
    private String clientInfo;
    private Long recVersion;

    public VariableConfigChangelog toVariableConfigChangelog() {
        return ReflectionUtils.builderInstance(VariableConfigChangelog.InnerBuilder.class)
                .id(getId()).variableConfigId(getVariableConfigId())
                .newFieldName(getNewFieldName()).oldFieldName(getOldFieldName())
                .newValidFromStamp(getNewValidFromStamp()).oldValidFromStamp(getOldValidFromStamp())
                .newValidToStamp(getNewValidToStamp()).oldValidToStamp(getOldValidToStamp())
                .newVariableId(getNewVariableId()).oldVariableId(getOldVariableId())
                .newEntityId(getNewEntityId()).oldEntityId(getOldEntityId())
                .createTimeUtc(getCreateTimeUtc()).opType(getOpType()).clientInfo(getClientInfo())
                .build();
    }
}
