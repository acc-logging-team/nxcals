package cern.nxcals.service.domain;

import cern.nxcals.api.domain.PartitionProperties;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.utils.TimeUtils;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.time.Instant;

@Embeddable
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PartitionPropertiesData implements Serializable {
    private static final long serialVersionUID = 6293229615670043778L;

    private Boolean updatable;
    private String stagingPartitionType;
    private Instant stagingPartitionTypeFromStamp;
    private Instant stagingPartitionTypeToStamp;

    public PartitionProperties toPartitionProperties() {
        return new PartitionProperties(updatable,
                stagingPartitionType != null ? TimeUtils.TimeSplits.valueOf(stagingPartitionType) : null,
                TimeWindow.between(stagingPartitionTypeFromStamp, stagingPartitionTypeToStamp));
    }

    public static PartitionPropertiesData from(PartitionProperties partitionProperties) {
        Instant start = null;
        Instant end = null;
        if (partitionProperties.getStagingPartitionTypeValidity() != null) {
            start = partitionProperties.getStagingPartitionTypeValidity().getStartTime();
            end = partitionProperties.getStagingPartitionTypeValidity().getEndTime();
        }
        return new PartitionPropertiesData(partitionProperties.isUpdatable(),
                partitionProperties.getStagingPartitionType() != null ?
                        partitionProperties.getStagingPartitionType().toString() :
                        null,
                start,
                end);
    }

    public static PartitionPropertiesData empty() {
        return new PartitionPropertiesData(false, null, null, null);
    }

}
