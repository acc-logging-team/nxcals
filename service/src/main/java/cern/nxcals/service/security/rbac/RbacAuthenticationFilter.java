package cern.nxcals.service.security.rbac;

import cern.nxcals.service.security.AuthenticationFacade;
import cern.rbac.common.RbaToken;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
public class RbacAuthenticationFilter extends OncePerRequestFilter {
    private final RbacAuthenticationService rbacAuthenticationService;

    @Override
    protected void doFilterInternal(@NonNull HttpServletRequest request, @NonNull HttpServletResponse response,
            FilterChain filterChain) throws ServletException, IOException {

        log.debug("Trying to authenticate via RBAC request={}", request);
        RbaToken rbaToken = rbacAuthenticationService.extractRbacToken(request);
        Optional<Authentication> authentication = rbacAuthenticationService.convertRbacTokenToAuthentication(rbaToken);

        authentication.ifPresent(AuthenticationFacade::setAuthentication);
        filterChain.doFilter(request, response);
    }

}
