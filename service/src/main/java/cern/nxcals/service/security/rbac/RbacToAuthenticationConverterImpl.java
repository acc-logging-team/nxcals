package cern.nxcals.service.security.rbac;

import cern.rbac.common.RbaToken;
import cern.rbac.common.Role;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static cern.nxcals.service.security.UserPrincipalService.EMPTY_PASSWORD;
import static java.util.stream.Collectors.toSet;
import static org.springframework.security.core.authority.AuthorityUtils.createAuthorityList;

/**
 * It converts the token to auth using the roles provided in the token
 */
@Slf4j
public class RbacToAuthenticationConverterImpl implements RbacToAuthenticationConverter {

    @Override
    public Authentication convert(@NonNull RbaToken token) {
        UserDetails userDetails = getDefaultRbacUserDetails(token);

        PreAuthenticatedAuthenticationToken auth = new PreAuthenticatedAuthenticationToken(userDetails, token,
                userDetails.getAuthorities());
        auth.setDetails(token.getLocation());
        return auth;
    }

    private UserDetails getDefaultRbacUserDetails(RbaToken token) {
        Role[] roles = token.getUser().getRoles();
        String username = token.getUser().getName();
        Set<String> roleNames = extractRoleNames(roles);
        log.debug("Token {} converted to Roles: {}", token, roleNames);
        List<GrantedAuthority> authorities = createAuthorityList(roleNames.toArray(new String[0]));
        return User.builder().username(username).password(EMPTY_PASSWORD).authorities(authorities).build();
    }

    private Set<String> extractRoleNames(Role[] roles) {
        return Arrays.stream(roles).map(Role::getName).collect(toSet());
    }

}
