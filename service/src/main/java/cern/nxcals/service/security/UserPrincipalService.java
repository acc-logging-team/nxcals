package cern.nxcals.service.security;

import cern.accsoft.ccs.ccda.client.rbac.Role;
import cern.accsoft.ccs.ccda.client.rbac.User;
import cern.accsoft.ccs.ccda.client.rbac.UserRole;
import cern.nxcals.service.internal.security.RbaUserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Set;

import static java.util.stream.Collectors.toSet;
import static org.apache.commons.lang3.StringUtils.isEmpty;

@Slf4j
@RequiredArgsConstructor
public class UserPrincipalService implements UserDetailsService {

    // required to construct org.springframework.security.core.userdetails.User object that is used as authentication principals
    public static final String EMPTY_PASSWORD = "";

    private final RbaUserService rbaUserService;
    private final String realm;

    @Override
    public UserDetails loadUserByUsername(String name) {
        validate(name);
        User user = getUserFor(name);

        Set<GrantedAuthority> authorities = user.getRoles().stream()
                .map(UserRole::getRole)
                .map(Role::getName)
                .map(SimpleGrantedAuthority::new)
                .collect(toSet());

        return org.springframework.security.core.userdetails.User.builder().username(user.getName())
                .password(EMPTY_PASSWORD).authorities(authorities).build();
    }

    private String sanitize(String name) {
        return isQualified(name) ? name.replace(qualifier(), "") : name;
    }

    private void validate(String username) {
        if (isEmpty(username)) {
            logAndThrowUserNotFoundException("Username has to be provided in order to proceed with authorization",
                    username);
        }
    }

    private boolean isQualified(String username) {
        return username.contains(qualifier());
    }

    private String qualifier() {
        return "@" + realm;
    }

    private User getUserFor(String name) {
        String username = sanitize(name);
        return rbaUserService.findBy(username).orElseThrow(() -> userNotFound(name));
    }

    private static void logAndThrowUserNotFoundException(String message, String userName) {
        log.warn("Unsuccessful authentication from user: {}. Reason: {} ", userName, message);
        throw new UsernameNotFoundException(message);
    }

    private UsernameNotFoundException userNotFound(String qualifiedName) {
        String message = "Not such user is registered";

        log.warn("Unsuccessful authentication from user: {}. Reason: {} ", qualifiedName, message);
        return new UsernameNotFoundException(message);
    }
}
