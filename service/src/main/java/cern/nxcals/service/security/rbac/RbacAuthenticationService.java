package cern.nxcals.service.security.rbac;

import cern.rbac.common.RbaToken;
import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

public interface RbacAuthenticationService {
    RbaToken extractRbacToken(HttpServletRequest httpRequest);
    Optional<Authentication> convertRbacTokenToAuthentication(RbaToken rbaToken);
}
