package cern.nxcals.service.security.resolvers;

/**
 * Implementations of this interface should be annotated as {@link org.springframework.stereotype.Component}.
 * They are automatically registered and used to authorize users in {@link cern.nxcals.service.security.MethodSecurityExpressionRoot}
 */
public interface PermissionResolver {
    String getPermissionString(String permissionArea, String permissionType);
}
