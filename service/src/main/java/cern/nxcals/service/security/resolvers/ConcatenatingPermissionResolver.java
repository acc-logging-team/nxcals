/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.security.resolvers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ConcatenatingPermissionResolver implements PermissionResolver {
    public static final String ROLE_SEPARATOR = "-";
    private final String prefix;

    public ConcatenatingPermissionResolver(@Value(value = "${nxcals.security.rbac.role.prefix}") String rolePrefix) {
        this.prefix = rolePrefix;
    }

    @Override
    public String getPermissionString(String permissionArea, String permissionType) {
        return prefix + permissionArea + ROLE_SEPARATOR + permissionType;
    }
}
