package cern.nxcals.service.config;

import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.common.domain.DataProcessingJob;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

import java.util.EnumMap;
import java.util.Map;

@ConfigurationProperties("compactor")
@Data
@NoArgsConstructor
public class CompactionProperties {
    private long tolerancePeriodHours;
    private long minFileModificationPeriodHours;
    private long maxPartitionSize;
    private long sortThreshold;
    private int avroParquetSizeRatio;
    private TimeUtils.TimeSplits stagingPartitionSplits;

    @NestedConfigurationProperty
    private Map<DataProcessingJob.JobType, HdfsPathSearchProperties> fileSearchProperties = new EnumMap<>(
            DataProcessingJob.JobType.class);
}
