package cern.nxcals.service.config;

import cern.nxcals.service.tracing.TracingRequestFilter;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties({ TracingProperties.class })
public class TracingConfig {
    @Bean
    public TracingRequestFilter logFilter(TracingProperties props) {
        TracingRequestFilter filter
                = new TracingRequestFilter();
        filter.setIncludeQueryString(props.isIncludeQueryString());
        filter.setIncludePayload(props.isIncludePayload());
        filter.setMaxPayloadLength(props.getMaxPayloadLength());
        filter.setIncludeHeaders(props.isIncludeHeaders());
        filter.setAfterMessagePrefix("REQUEST [");
        filter.setIncludeClientInfo(props.isIncludeClientInfo());
        filter.setMaxTopElements(props.getMaxTopElements());
        filter.setInactivityDuration(props.getInactivityDuration());
        filter.setRemoveUserRealm(props.isRemoveUserRealm());
        filter.setDisplayStatsFreq(props.getDisplayStatsFreq());
        filter.init();
        return filter;
    }
}
