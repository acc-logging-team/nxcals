/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.config;

import cern.accsoft.commons.dbaccess.jdbc.DataSourceManager;
import cern.accsoft.commons.dbaccess.jdbc.DataSourceManager.DataSourceIdentifier;
import cern.accsoft.commons.dbaccess.jdbc.DataSourceProperties;
import cern.accsoft.commons.dbaccess.jdbc.DefaultDataSourceProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.ResourceLoader;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.sql.SQLException;

@Configuration
public class DataSourceContext {

    @Autowired
    private ResourceLoader resourceLoader;

    @Bean
    @Primary
    public DataSource dataSource() throws SQLException {
        DBProperties dbProperties = dbProperties();
        DataSourceManager manager = new DataSourceManager(
                this.resourceLoader.getResource(dbProperties.getTnsnamesOraPath()));
        DataSourceProperties dsProperties = dsProperties();
        return manager.getDataSourceFor(DataSourceIdentifier.valueOf(dbProperties.getSchema()), dsProperties);
    }

    @Bean
    public DataSource idGeneratorDataSource() throws SQLException {
        DBProperties dbProperties = idGeneratorDbProperties();
        DataSourceManager manager = new DataSourceManager(
                this.resourceLoader.getResource(dbProperties.getTnsnamesOraPath()));
        DataSourceProperties dsProperties = idGeneratorDsProperties();
        return manager.getDataSourceFor(DataSourceIdentifier.valueOf(dbProperties.getSchema()), dsProperties);
    }

    @Bean
    public PlatformTransactionManager jpaTransactionManager(EntityManagerFactory emFactory) {
        JpaTransactionManager manager = new JpaTransactionManager();
        manager.setEntityManagerFactory(emFactory);
        return manager;
    }

    @Bean
    public JdbcTemplate idGeneratorJdbcTemplate() throws SQLException {
        return new JdbcTemplate(idGeneratorDataSource());
    }

    @Bean
    public JdbcTemplate jdbcTemplate() throws SQLException {
        return new JdbcTemplate(dataSource());
    }

    @ConfigurationProperties(prefix = "spring.datasource.data-source-properties")
    @Bean
    public DataSourceProperties dsProperties() {
        return new DefaultDataSourceProperties();
    }

    @ConfigurationProperties(prefix = "spring.datasource.db-properties")
    @Bean
    public DBProperties dbProperties() {
        return new DBProperties();
    }

    @ConfigurationProperties(prefix = "spring.datasource.id-generator-data-source-properties")
    @Bean
    public DataSourceProperties idGeneratorDsProperties() {
        return new DefaultDataSourceProperties();
    }

    @ConfigurationProperties(prefix = "spring.datasource.id-generator-db-properties")
    @Bean
    public DBProperties idGeneratorDbProperties() {
        return new DBProperties();
    }

    @Bean
    public PlatformTransactionManager idGeneratorTxMgr() throws SQLException {
        return new DataSourceTransactionManager(idGeneratorDataSource());
    }

    static class DBProperties {
        private String tnsnamesOraPath;
        private String schema;

        String getTnsnamesOraPath() {
            return tnsnamesOraPath;
        }

        public void setTnsnamesOraPath(String tnsnamesOraPath) {
            this.tnsnamesOraPath = tnsnamesOraPath;
        }

        public String getSchema() {
            return schema;
        }

        public void setSchema(String schema) {
            this.schema = schema;
        }

    }

}
