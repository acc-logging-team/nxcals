package cern.nxcals.service.config;

import cern.nxcals.service.security.KerberosAuthenticationFailureHandler;
import cern.nxcals.service.security.rbac.DefaultRbacAuthenticationService;
import cern.nxcals.service.security.rbac.RbacAuthenticationFilter;
import cern.nxcals.service.security.rbac.RbacAuthenticationService;
import cern.nxcals.service.security.rbac.RbacToAuthenticationConverter;
import cern.nxcals.service.security.rbac.RbacToAuthenticationConverterImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.kerberos.authentication.KerberosAuthenticationProvider;
import org.springframework.security.kerberos.authentication.KerberosServiceAuthenticationProvider;
import org.springframework.security.kerberos.web.authentication.ResponseHeaderSettingKerberosAuthenticationSuccessHandler;
import org.springframework.security.kerberos.web.authentication.SpnegoAuthenticationProcessingFilter;
import org.springframework.security.kerberos.web.authentication.SpnegoEntryPoint;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

/**
 * Class containing authentication configuration. It set up kerberos based authentication for NXCALS service.
 */
@Slf4j
@Configuration
@EnableWebSecurity
public class AuthConfigProvider {
    @Value("${service.kerberos.realm:CERN.CH}")
    private String realm;

    private SpnegoEntryPoint spnegoEntryPoint() {
        return new SpnegoEntryPoint();
    }

    /**
     * Creates bean responsible for gathering SPNEGO token from HTTP request and transforming it into kerberos token.
     *
     * @return Instance of {@link SpnegoAuthenticationProcessingFilter}
     */
    @Bean
    public SpnegoAuthenticationProcessingFilter spnegoAuthenticationProcessingFilter(
            AuthenticationManager auth) {
        SpnegoAuthenticationProcessingFilter filter = new SpnegoAuthenticationProcessingFilter();
        filter.setAuthenticationManager(auth);
        filter.setSuccessHandler(new ResponseHeaderSettingKerberosAuthenticationSuccessHandler());
        filter.setFailureHandler(new KerberosAuthenticationFailureHandler());
        return filter;
    }

    @Bean
    public RbacToAuthenticationConverter rbacToAuthenticationConverter() {
        return new RbacToAuthenticationConverterImpl();
    }

    @Bean
    public RbacAuthenticationService rbacAuthenticationService(RbacToAuthenticationConverter converter) {
        return new DefaultRbacAuthenticationService(converter);
    }

    @Bean
    public RbacAuthenticationFilter rbacAuthenticationFilter(RbacAuthenticationService rbacAuthenticationService) {
        return new RbacAuthenticationFilter(rbacAuthenticationService);
    }

    @Bean
    public SecurityFilterChain configureHttpSecurity(HttpSecurity http, AuthenticationConfiguration configuration,
            ApplicationContext context)
            throws Exception {
        AuthenticationManager auth = authenticationManager(configuration);
        http.exceptionHandling()
                .authenticationEntryPoint(spnegoEntryPoint())
                .and()
                .authorizeHttpRequests()
                .anyRequest().authenticated()
                .and()
                .addFilterBefore(spnegoAuthenticationProcessingFilter(auth), BasicAuthenticationFilter.class)
                .addFilterBefore(context.getBean(RbacAuthenticationFilter.class),
                        SpnegoAuthenticationProcessingFilter.class);

        //The service won't be used in browser, so we don't need CSRF protection.
        //Reference: Point 18.3 in https://docs.spring.io/spring-security/site/docs/current/reference/html/csrf.html
        http.csrf().disable();
        return http.build();
    }

    /**
     * Method adding necessary Kerberos related beams to @AuthenticationManagerBuilder.
     *
     * @param auth                                  Spring builder that creates @AuthenticationManager
     * @param kerberosAuthenticationProvider        Spring implementation of AuthenticationProvider using Kerberos,
     *                                              for clients.
     * @param kerberosServiceAuthenticationProvider Spring implementation of AuthenticationProvider using Kerberos,
     *                                              for services.
     */
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth,
            KerberosAuthenticationProvider kerberosAuthenticationProvider,
            KerberosServiceAuthenticationProvider kerberosServiceAuthenticationProvider) {
        auth.authenticationProvider(kerberosAuthenticationProvider)
                .authenticationProvider(kerberosServiceAuthenticationProvider);
    }

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration config) throws Exception {
        return config.getAuthenticationManager();
    }
}
