package cern.nxcals.service.config;

import cern.nxcals.service.internal.security.RbaUserService;
import cern.nxcals.service.security.UserPrincipalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.security.kerberos.authentication.KerberosAuthenticationProvider;
import org.springframework.security.kerberos.authentication.KerberosServiceAuthenticationProvider;
import org.springframework.security.kerberos.authentication.sun.SunJaasKerberosClient;
import org.springframework.security.kerberos.authentication.sun.SunJaasKerberosTicketValidator;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Configuration class with definition of all the Spring beams necessary to secure application with Kerberos.
 */
@Configuration
@Slf4j
public class KerberosConfiguration {

    @Value("${service.kerberos.keytab}")
    private String keytabPath;

    @Value("${service.kerberos.realm}")
    private String realm;

    @Value("${service.kerberos.type}")
    private String kerberosType;

    @Value("${service.host:#{null}}")
    private String serviceHost;

    private String servicePrincipal = null;

    /**
     * Service responsible for user authorization.
     *
     * @return Instance of {@link UserPrincipalService}
     */
    @Bean
    public UserPrincipalService nxcalsUserDetailsService(RbaUserService rbaUserService) {
        return new UserPrincipalService(rbaUserService, this.realm);
    }

    /**
     * Spring implementation of AuthenticationProvider using Kerberos, for clients.
     *
     * @param userPrincipalService Authorization service.
     * @return Instance of {@link KerberosAuthenticationProvider}
     */
    @Bean
    public KerberosAuthenticationProvider kerberosAuthenticationProvider(
            UserPrincipalService userPrincipalService) {
        KerberosAuthenticationProvider provider = new KerberosAuthenticationProvider();
        SunJaasKerberosClient client = new SunJaasKerberosClient();
        provider.setKerberosClient(client);
        provider.setUserDetailsService(userPrincipalService);
        return provider;
    }

    /**
     * Spring implementation of AuthenticationProvider using Kerberos, for service.
     *
     * @param sunJaasKerberosTicketValidator Kerberos ticker validator implemented for SUN JRE
     * @param userPrincipalService         Authorization service.
     * @return Instance of {@link KerberosAuthenticationProvider}
     */
    @Bean
    @Autowired
    public KerberosServiceAuthenticationProvider kerberosServiceAuthenticationProvider(
            SunJaasKerberosTicketValidator sunJaasKerberosTicketValidator,
            UserPrincipalService userPrincipalService) {
        KerberosServiceAuthenticationProvider provider = new KerberosServiceAuthenticationProvider();
        provider.setTicketValidator(sunJaasKerberosTicketValidator);
        provider.setUserDetailsService(userPrincipalService);
        return provider;
    }

    /**
     * Bean that validate Kerberos ticker from incoming request.
     * It requires principal of service that it's running in and keytab file path for this service.
     *
     * @return Instance of {@link SunJaasKerberosTicketValidator}
     */
    @Bean
    public SunJaasKerberosTicketValidator sunJaasKerberosTicketValidator() {
        SunJaasKerberosTicketValidator ticketValidator = new SunJaasKerberosTicketValidator();
        ticketValidator.setServicePrincipal(getServicePrincipal());
        ticketValidator.setKeyTabLocation(new FileSystemResource(keytabPath));
        return ticketValidator;
    }

    private String getServicePrincipal() {
        if (servicePrincipal == null) {
            if (this.serviceHost == null) {
                log.warn("service.host property not found, trying to obtain hostname programmatically");
                try {
                    String host = InetAddress.getLocalHost().getHostName();
                    servicePrincipal = kerberosType + "/" + host + "@" + realm;
                    log.info("Host name obtained: {}", host);
                } catch (UnknownHostException ex) {
                    log.error(
                            "Couldn't find service.host property. Backup host name resolving also failed. Cannot create service principal.",
                            ex);
                    throw new IllegalStateException(ex);
                }
            } else {
                servicePrincipal = kerberosType + "/" + serviceHost + "@" + realm;
            }
        }
        return servicePrincipal;
    }

}
