package cern.nxcals.service.config;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class HdfsPathSearchProperties {
    private String topic = "*";
    private String system = "*";
    private String partition = "*";
    private String schema = "*";
    private String date = "*";
}
