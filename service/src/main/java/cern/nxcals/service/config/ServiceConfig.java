package cern.nxcals.service.config;

import cern.accsoft.ccs.ccda.client.core.CcdaClient;
import cern.accsoft.ccs.ccda.client.rbac.UserService;
import cern.nxcals.api.config.AuthenticationContext;
import cern.nxcals.common.metrics.MetricsConfig;
import cern.nxcals.common.utils.GlobalObjectMapperProvider;
import cern.rbac.client.authentication.AuthenticationException;
import cern.rbac.common.authentication.LoginPolicy;
import cern.rbac.util.authentication.LoginService;
import cern.rbac.util.authentication.LoginServiceBuilder;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.micrometer.core.aop.TimedAspect;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.fs.FileSystem;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import java.io.IOException;
import java.io.UncheckedIOException;

import static cern.rbac.common.authentication.LoginPolicy.EXPLICIT;

@Configuration
@Slf4j
@Import({ AuthenticationContext.class, MetricsConfig.class })
public class ServiceConfig {
    @Value("${service.rbac.login.policy:LOCATION}")
    private LoginPolicy loginPolicy;
    @Value("${service.rbac.login.username:#{null}}")
    private String username;
    @Value("${service.rbac.login.password:#{null}}")
    private String password;

    @Bean
    @DependsOn("kerberos")
    public FileSystem hadoopFileSystem(@Value("${data.filesystem.local:false}") boolean shouldUseLocalFilesystem) {
        log.info("Creating HDFS FileSystem");
        org.apache.hadoop.conf.Configuration conf = new org.apache.hadoop.conf.Configuration();
        try {
            if (shouldUseLocalFilesystem) {
                log.warn("Using Local Filesystem!!!");
                return FileSystem.getLocal(conf);
            } else {
                return FileSystem.get(conf);
            }
        } catch (IOException e) {
            throw new UncheckedIOException("Cannot access filesystem", e);
        }
    }

    @Bean
    public TimedAspect timedAspect(MeterRegistry registry) {
        return new TimedAspect(registry);
    }

    @Bean
    @Primary
    public ObjectMapper objectMapper() {
        return GlobalObjectMapperProvider.get();
    }

    @Bean
    @Profile({ "!gitlab-ci & !ci & !dev" })
    public LoginService rbac() throws AuthenticationException {
        log.info("Registering to RBAC service with login policy {}", this.loginPolicy);
        LoginServiceBuilder loginBuilder = LoginServiceBuilder.newInstance().applicationName("nxcals-service")
                .loginPolicy(this.loginPolicy).autoRefresh(true).refreshRateSec(60);
        if (EXPLICIT.equals(this.loginPolicy)) {
            loginBuilder.userName(this.username).userPassword(this.password.toCharArray());
        }
        return loginBuilder.build();
    }

    @Bean
    public UserService userService() {
        return CcdaClient.newInstance().getService(UserService.class);
    }
}
