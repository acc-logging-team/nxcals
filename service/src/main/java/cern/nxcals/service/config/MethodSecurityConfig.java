package cern.nxcals.service.config;

import cern.nxcals.service.security.RootChangeMethodSecurityExpressionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;

@Configuration
@EnableMethodSecurity
public class MethodSecurityConfig {

    @Autowired
    public void createExpressionHandler(ApplicationContext context,
            RootChangeMethodSecurityExpressionHandler handler) {
        handler.setApplicationContext(context);
    }
}