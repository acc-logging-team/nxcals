package cern.nxcals.service.rsql;

import com.github.tennaito.rsql.jpa.AbstractJpaVisitor;
import cz.jirutka.rsql.parser.ast.AndNode;
import cz.jirutka.rsql.parser.ast.ComparisonNode;
import cz.jirutka.rsql.parser.ast.Node;
import cz.jirutka.rsql.parser.ast.OrNode;
import cz.jirutka.rsql.parser.ast.RSQLVisitor;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;

public class JpaCriteriaSubqueryVisitor<T> extends AbstractJpaVisitor<Subquery<T>, T> implements RSQLVisitor<Subquery<T>, EntityManager> {
    private final CriteriaQuery<T> parent;

    @SafeVarargs
    public JpaCriteriaSubqueryVisitor(CriteriaQuery<T> parent, T... t) {
        super(t);
        this.parent = parent;
    }

    @Override
    public Subquery<T> visit(AndNode node, EntityManager entityManager) {
        return visit((Node) node, entityManager);
    }

    @Override
    public Subquery<T> visit(OrNode node, EntityManager entityManager) {
        return visit((Node) node, entityManager);
    }

    @Override
    public Subquery<T> visit(ComparisonNode node, EntityManager entityManager) {
        return visit((Node) node, entityManager);
    }

    private Subquery<T> visit(Node node, EntityManager manager) {
        Subquery<T> criteria = parent.subquery(entityClass);
        Root<T> root = criteria.from(entityClass);
        Predicate predicate = CustomPredicateBuilder.<T>createPredicate(node, root, entityClass, manager, builderTools);
        criteria.select(root);
        return criteria.where(predicate);
    }
}