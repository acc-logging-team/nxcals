package cern.nxcals.service.rsql;

import cz.jirutka.rsql.parser.ast.ComparisonOperator;
import cz.jirutka.rsql.parser.ast.RSQLOperators;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Set;
import java.util.function.Supplier;

import static cern.nxcals.common.rsql.Operators.CASE_INSENSITIVE_IN;
import static cern.nxcals.common.rsql.Operators.CASE_INSENSITIVE_LIKE;
import static cern.nxcals.common.rsql.Operators.CASE_INSENSITIVE_NOT_IN;
import static cern.nxcals.common.rsql.Operators.CASE_INSENSITIVE_NOT_LIKE;
import static cern.nxcals.common.rsql.Operators.CASE_INSENSITIVE_STRING_EQUALS;
import static cern.nxcals.common.rsql.Operators.CASE_INSENSITIVE_STRING_NOT_EQUALS;
import static cern.nxcals.common.rsql.Operators.IS_NULL_OPERATOR;
import static cern.nxcals.common.rsql.Operators.LIKE;
import static cern.nxcals.common.rsql.Operators.NOT_LIKE;
import static cern.nxcals.common.rsql.Operators.PATTERN_OPERATOR;
import static cern.nxcals.common.rsql.Operators.STRING_EQUALS;
import static cern.nxcals.common.rsql.Operators.STRING_NOT_EQUALS;

@Component
public class OperatorsSupplier implements Supplier<Set<ComparisonOperator>> {

    private final Set<ComparisonOperator> operators;

    public OperatorsSupplier() {
        operators = RSQLOperators.defaultOperators();
        //not supported by default.
        operators.add(new ComparisonOperator(IS_NULL_OPERATOR, false));
        operators.add(new ComparisonOperator(PATTERN_OPERATOR, false));
        operators.add(new ComparisonOperator(STRING_EQUALS, false));
        operators.add(new ComparisonOperator(CASE_INSENSITIVE_STRING_EQUALS, false));
        operators.add(new ComparisonOperator(STRING_NOT_EQUALS, false));
        operators.add(new ComparisonOperator(CASE_INSENSITIVE_STRING_NOT_EQUALS, false));
        operators.add(new ComparisonOperator(LIKE, false));
        operators.add(new ComparisonOperator(CASE_INSENSITIVE_LIKE, false));
        operators.add(new ComparisonOperator(NOT_LIKE, false));
        operators.add(new ComparisonOperator(CASE_INSENSITIVE_NOT_LIKE, false));
        operators.add(new ComparisonOperator(CASE_INSENSITIVE_IN, true));
        operators.add(new ComparisonOperator(CASE_INSENSITIVE_NOT_IN, true));


    }

    @Override
    public Set<ComparisonOperator> get() {
        return Collections.unmodifiableSet(operators);
    }
}
