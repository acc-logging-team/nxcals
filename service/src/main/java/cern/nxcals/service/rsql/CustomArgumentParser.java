package cern.nxcals.service.rsql;

import com.github.tennaito.rsql.misc.ArgumentFormatException;
import com.github.tennaito.rsql.misc.DefaultArgumentParser;

import java.time.Instant;

public class CustomArgumentParser extends DefaultArgumentParser {

    @Override
    public <T> T parse(String argument, Class<T> type) {
        try {
            return super.parse(argument, type);
        } catch (ArgumentFormatException | IllegalArgumentException ex) {
            //DefaultArgumentParser doesn't support Instant
            if ("false".equals(argument) || "true".equals(argument)) {
                //This is for EX operator like field=ex="true" (that means field is not null)
                return null; //this is ok as this EX operator is handled with not parsed arguments in our CustomPredicateBuilder.
            } else if (Instant.class.equals(type)) {
                return type.cast(Instant.parse(argument));
            }
            throw ex;
        }
    }
}
