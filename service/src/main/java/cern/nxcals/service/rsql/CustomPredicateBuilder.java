package cern.nxcals.service.rsql;

import cern.nxcals.common.utils.StringUtils;
import com.github.tennaito.rsql.builder.BuilderTools;
import com.github.tennaito.rsql.jpa.PredicateBuilder;
import com.google.common.collect.Lists;
import cz.jirutka.rsql.parser.ast.ComparisonNode;
import cz.jirutka.rsql.parser.ast.LogicalNode;
import cz.jirutka.rsql.parser.ast.LogicalOperator;
import cz.jirutka.rsql.parser.ast.Node;
import lombok.experimental.UtilityClass;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.From;
import javax.persistence.criteria.Predicate;
import java.util.List;
import java.util.stream.Collectors;

import static cern.nxcals.common.rsql.Operators.CASE_INSENSITIVE_IN;
import static cern.nxcals.common.rsql.Operators.CASE_INSENSITIVE_LIKE;
import static cern.nxcals.common.rsql.Operators.CASE_INSENSITIVE_NOT_IN;
import static cern.nxcals.common.rsql.Operators.CASE_INSENSITIVE_NOT_LIKE;
import static cern.nxcals.common.rsql.Operators.CASE_INSENSITIVE_STRING_EQUALS;
import static cern.nxcals.common.rsql.Operators.CASE_INSENSITIVE_STRING_NOT_EQUALS;
import static cern.nxcals.common.rsql.Operators.IN;
import static cern.nxcals.common.rsql.Operators.IS_NULL_OPERATOR;
import static cern.nxcals.common.rsql.Operators.LIKE;
import static cern.nxcals.common.rsql.Operators.NOT_IN;
import static cern.nxcals.common.rsql.Operators.NOT_LIKE;
import static cern.nxcals.common.rsql.Operators.PATTERN_OPERATOR;
import static cern.nxcals.common.rsql.Operators.STRING_EQUALS;
import static cern.nxcals.common.rsql.Operators.STRING_NOT_EQUALS;
import static cern.nxcals.service.repository.CustomRepository.ORACLE_MAX_SIZE;
import static com.github.tennaito.rsql.jpa.PredicateBuilder.findPropertyPath;

/**
 * The instance calls the default one if it cannot resolve the operator. We want our strategies to be invoked before the default ones, to be able to override them.
 * It is needed as it seems that 'is null' operation is not properly supported in the conversion to JPA and to split long item lists into chunks of 1000
 */
@UtilityClass
public class CustomPredicateBuilder {
    public <T> Predicate createPredicate(Node node, From root, Class<T> entity, EntityManager manager, BuilderTools misc) {
        if (node instanceof LogicalNode) {
            return createPredicate((LogicalNode) node, root, entity, manager, misc);
        } else if (node instanceof ComparisonNode) {
            return createPredicate((ComparisonNode) node, root, entity, manager, misc);
        } else {
            throw new IllegalArgumentException("Unknown expression type: " + node.getClass());
        }
    }

    public <T> Predicate createPredicate(LogicalNode logical, From root, Class<T> entity, EntityManager manager, BuilderTools misc) {
        Predicate[] predicates = logical.getChildren().stream()
                .map(node -> createPredicate(node, root, entity, manager, misc))
                .toArray(Predicate[]::new);

        if (logical.getOperator() == LogicalOperator.AND) {
            return manager.getCriteriaBuilder().and(predicates);
        } else if (logical.getOperator() == LogicalOperator.OR) {
            return manager.getCriteriaBuilder().or(predicates);
        } else {
            throw new IllegalArgumentException("Unknown operator: " + logical.getOperator());
        }
    }

    public <T> Predicate createPredicate(ComparisonNode node, From root, Class<T> entity, EntityManager manager, BuilderTools misc)  {
        Expression path = findPropertyPath(node.getSelector(), root, manager, misc);
        CriteriaBuilder builder = manager.getCriteriaBuilder();

        switch (node.getOperator().getSymbol()) {
            case IS_NULL_OPERATOR:
                return exists(path, node.getArguments().get(0), builder);
            case PATTERN_OPERATOR:
                return pattern(path, node.getArguments().get(0), builder);
            case STRING_EQUALS:
                return equalsString(path, node.getArguments().get(0), builder);
            case CASE_INSENSITIVE_STRING_EQUALS:
                return equalsString(toLower(path, builder), toLower(node.getArguments().get(0)), builder);
            case STRING_NOT_EQUALS:
                return builder.not(equalsString(path, node.getArguments().get(0), builder));
            case CASE_INSENSITIVE_STRING_NOT_EQUALS:
                return builder.not(equalsString(toLower(path, builder), toLower(node.getArguments().get(0)), builder));
            case LIKE:
                return likeString(path, node.getArguments().get(0), builder);
            case CASE_INSENSITIVE_LIKE:
                return likeString(toLower(path, builder), toLower(node.getArguments().get(0)), builder);
            case NOT_LIKE:
                return builder.not(likeString(path, node.getArguments().get(0), builder));
            case CASE_INSENSITIVE_NOT_LIKE:
                return builder.not(likeString(toLower(path, builder), toLower(node.getArguments().get(0)), builder));
            case IN: // overriding =in= implementation for base PredicateBuilder
                return in(path, node.getArguments(), builder);
            case CASE_INSENSITIVE_IN:
                return in(toLower(path, builder), toLower(node.getArguments()), builder);
            case NOT_IN: // overriding =out= implementation for base PredicateBuilder
                return builder.not(in(path, node.getArguments(), builder));
            case CASE_INSENSITIVE_NOT_IN:
                return builder.not(in(toLower(path, builder), toLower(node.getArguments()), builder));
            default:
                return PredicateBuilder.<T>createPredicate(node, root, entity, manager, misc);
        }
    }

    private Predicate exists(Expression path, String argument, CriteriaBuilder builder) {
        //is null comes as field=ex=false|true...
        if ("false".equalsIgnoreCase(argument)) {
            return builder.isNull(path);
        } else if ("true".equalsIgnoreCase(argument)) {
            return builder.isNotNull(path);
        } else {
            throw new IllegalArgumentException("Unknown argument "+ argument+ " in expression " + path);
        }
    }

    private Predicate pattern(Expression path, String argument, CriteriaBuilder builder) {
        Expression<String> pattern = builder.literal(argument);
        Expression<Long> regExp = builder.function("REGEXP_INSTR", Long.class, path, pattern);
        return builder.greaterThan(regExp, 0L);
    }

    private Predicate in(Expression path, List<String> arguments, CriteriaBuilder builder) {
        if (arguments.size() < ORACLE_MAX_SIZE) {
            return path.in(arguments);
        } else {
            return builder.or(Lists.partition(arguments, ORACLE_MAX_SIZE).stream().map(path::in).toArray(Predicate[]::new));
        }
    }

    private Predicate likeString(Expression<String> path, String argument, CriteriaBuilder builder) {
        return builder.like(path, argument, StringUtils.ESCAPE_CHARACTER);
    }

    private static Expression toLower(Expression path, CriteriaBuilder builder) {
        return builder.lower(path);
    }

    private String toLower(String argument) {
        return argument.toLowerCase();
    }

    private List<String> toLower(List<String> arguments) {
        return arguments.stream().map(String::toLowerCase).collect(Collectors.toList());
    }

    private Predicate equalsString(Expression<String> path, String argument, CriteriaBuilder builder) {
        return builder.equal(path, argument);
    }
}