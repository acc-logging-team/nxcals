/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.internal.id;

import cern.nxcals.service.domain.SequenceType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.Types;
import java.util.concurrent.TimeUnit;

/**
 * Db based pool of numeric identifiers. The last used value per a given {@link SequenceType} is stored in a db table.
 * Each time {@code getNextIdAndReserveQuantityLongSequence} method is called the pool increases the the last used id according to the following
 * formula: {@code new_last_used_id = last_used_id + quantity}.
 * The implementation also assures that there's no conflict with another pool instance by using optimistic locking
 * approach.
 *
 * @author Marcin Sobieszek
 * @date Jul 25, 2016 4:09:08 PM
 */
@Service
public class DbBasedIdPool {
    private static final Logger LOGGER = LoggerFactory.getLogger(DbBasedIdPool.class);
    /**
     * @formatter:off
     */
    private static final String GET_OFFSET = "select seq_last_value, seq_rec_version"
            + "  from sequence_info "
            + " where seq_name = :seqName";
    private static final String UPDATE_OFFSET = "update sequence_info "
            + "   set seq_last_value = :lastValue,"
            + "       seq_rec_version = :newRecVersion"
            + " where seq_name = :seqName"
            + "   and seq_rec_version = :lastVersion";

    private static final int[] ARG_TYPES = {Types.VARCHAR};
    /**
     * @formatter:on
     */
    @Autowired
    @Qualifier("idGeneratorJdbcTemplate")
    private JdbcTemplate template;

    @Value("${id.generator.pool.update.attempts:5}")
    private int attempts = 5;

    @Transactional(transactionManager = "idGeneratorTxMgr")
    public long getNextIdAndReserveQuantityLongSequence(SequenceType type, int quantity) {
        if (quantity <= 0) {
            throw new IllegalArgumentException("Quantity must not be negative");
        }
        int countAttempts = this.attempts;
        do {
            IdData currentData = this.getCurrentValues(type);
            IdData newData = new IdData(currentData.lastValue + quantity, currentData.lastVersion + 1);
            boolean updated = this.updateCurrentValues(type, currentData, newData);
            if (updated) {
                return currentData.lastValue + 1;
            }
            LOGGER.info("Sleeping for 100 ms to get a successful update window, attempts remaining = {}",
                    countAttempts - 1);
            try {
                TimeUnit.MILLISECONDS.sleep(100);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                throw new IllegalStateException(e.getMessage(), e);
            }
        } while (--countAttempts > 0);
        throw new IllegalStateException("Cannot update squence info for " + type);
    }

    private boolean updateCurrentValues(SequenceType type, IdData currentData, IdData newData) {
        return this.template.update(UPDATE_OFFSET, newData.lastValue, newData.lastVersion, type.getSeqName(),
                currentData.lastVersion) == 1;
    }

    private IdData getCurrentValues(SequenceType type) {
        IdData output = this.template.query(GET_OFFSET, new Object[]{type.getSeqName()}, ARG_TYPES, (ResultSet rs) -> {
            if (!rs.next()) {
                throw new IllegalStateException("No sequence data found for = " + type);
            }
            return new IdData(rs.getLong("seq_last_value"), rs.getLong("seq_rec_version"));
        });
        if (output != null) {
            return output;
        } else {
            //technically this is never going to happen since we throw from the supplier but to make sonar happy...
            throw new NullPointerException("IdData cannot be null");
        }
    }

    private static class IdData {
        private final long lastValue;
        private final long lastVersion;

        private IdData(long lastVal, long lastVer) {
            this.lastValue = lastVal;
            this.lastVersion = lastVer;
        }
    }
}
