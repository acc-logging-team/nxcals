package cern.nxcals.service.internal.extraction;

import cern.nxcals.service.internal.extraction.ExtractionService.SelectionCriteria;

import java.util.List;

public interface PredicateProvider {
    List<String> getHdfsPredicate(SelectionCriteria criteria);

    List<String> getHBasePredicate(SelectionCriteria criteria);
}
