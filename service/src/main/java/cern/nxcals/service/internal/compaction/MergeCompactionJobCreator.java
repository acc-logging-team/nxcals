package cern.nxcals.service.internal.compaction;

import cern.nxcals.common.domain.DataProcessingJob.JobType;
import cern.nxcals.common.domain.MergeCompactionJob;
import cern.nxcals.common.paths.StagingPath;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.util.Collection;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static cern.nxcals.common.domain.DataProcessingJob.JobType.MERGE_COMPACT;

@Slf4j
public class MergeCompactionJobCreator extends AbstractMergeCompactionJobCreator<MergeCompactionJob> {

    public MergeCompactionJobCreator(long sortAbove, long maxPartitionSize, int avroParquetSizeRatio,
            @NonNull String outputPrefix, @NonNull String stagingPrefix, @NonNull FileSystem fs,
            Predicate<StagingPath> adaptivePathChecker) {
        super(sortAbove, maxPartitionSize, avroParquetSizeRatio, outputPrefix, stagingPrefix, fs, adaptivePathChecker);
    }

    @Override
    protected MergeCompactionJob createJob(StagingPath group, Collection<FileStatus> files, long jobSize,
            long totalSize, String filePrefix) {
        return MergeCompactionJob.builder()
                .filePrefix(filePrefix)
                .systemId(group.getSystemId())
                .sourceDir(group.toPath().toUri())
                .destinationDir(getHdfsPathCreator().toOutputUri(group))
                .totalSize(totalSize)
                .jobSize(jobSize)
                .partitionCount(getSizeBasedPartitionCount(jobSize))
                .sortEnabled(jobSize > getSortAbove())
                .files(files.stream().map(FileStatus::getPath).map(Path::toUri).collect(Collectors.toList()))
                .restagingReportFile(getHdfsPathCreator().toRestageReportFileUri(group))
                .build();
    }

    @Override
    public JobType getJobType() {
        return MERGE_COMPACT;
    }

}
