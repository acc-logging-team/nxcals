package cern.nxcals.service.internal.compaction;

import cern.nxcals.common.domain.MergeCompactionJob;
import cern.nxcals.common.paths.StagingPath;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.util.function.Predicate;

@Slf4j
public abstract class AbstractMergeCompactionJobCreator<T extends MergeCompactionJob>
        extends DataProcessingJobCreator<T> {
    @NonNull
    private final Predicate<StagingPath> adaptivePathChecker;

    public AbstractMergeCompactionJobCreator(long sortAbove, long maxPartitionSize, int avroParquetSizeRatio,
            @NonNull String outputPrefix, @NonNull String stagingPrefix,
            @NonNull FileSystem fs, @NonNull Predicate<StagingPath> adaptivePathChecker) {
        super(sortAbove, maxPartitionSize, avroParquetSizeRatio, outputPrefix, stagingPrefix, fs);
        this.adaptivePathChecker = adaptivePathChecker;
    }

    @Override
    protected Predicate<StagingPath> getPathChecker() {
        return adaptivePathChecker.and(this::verifyJobPath);
    }

    private boolean verifyJobPath(StagingPath path) {
        Path restageDataFileReportPath = new Path(getHdfsPathCreator().toRestageReportFileUri(path));
        try {
            return getFs().exists(restageDataFileReportPath);
        } catch (IOException e) {
            log.error("Got exception while checking if provided path exists for {}. Operation would be omitted",
                    getJobType(), e);
            return false;
        }
    }
}
