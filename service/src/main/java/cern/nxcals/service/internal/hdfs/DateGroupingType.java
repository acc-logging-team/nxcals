package cern.nxcals.service.internal.hdfs;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;
import java.time.temporal.TemporalUnit;

import static java.time.format.DateTimeFormatter.ofPattern;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public enum DateGroupingType {
    DAYS(ChronoUnit.DAYS, t -> t, ofPattern("yyyy/M/d"), null),
    MONTHS_AND_DAYS(ChronoUnit.MONTHS, TemporalAdjusters.firstDayOfMonth(), ofPattern("yyyy/M/*"), DAYS),
    YEARS_MONTHS_AND_DAYS(ChronoUnit.YEARS, TemporalAdjusters.firstDayOfYear(), ofPattern("yyyy/*/*"), MONTHS_AND_DAYS);

    private TemporalUnit unit;
    private TemporalAdjuster adjuster;
    private DateTimeFormatter formatter;
    private DateGroupingType next;

    public boolean hasNext() {
        return next != null;
    }
}