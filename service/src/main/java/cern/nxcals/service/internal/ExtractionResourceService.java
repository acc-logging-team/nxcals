/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.internal;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.EntityHistory;
import cern.nxcals.api.domain.Resource;
import cern.nxcals.api.domain.TimeEntityPartitionType;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.common.SystemFields;
import cern.nxcals.common.domain.EntityExtractionResource;
import cern.nxcals.common.domain.ExtractionCriteria;
import cern.nxcals.common.domain.VariableExtractionResource;
import cern.nxcals.common.utils.MathUtils;
import cern.nxcals.service.config.DataLocationProperties;
import cern.nxcals.service.domain.PartitionResourceHistoryData;
import cern.nxcals.service.internal.extraction.ExtractionSourceConfigService;
import cern.nxcals.service.internal.extraction.VariableSourceConfig;
import cern.nxcals.service.internal.hdfs.DateGrouping;
import cern.nxcals.service.repository.SystemSpecRepository;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.google.common.annotations.VisibleForTesting;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.spark.unsafe.hash.Murmur3_x86_32;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.net.URI;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import static cern.nxcals.api.utils.TimeUtils.SECONDS_IN_DAY;
import static cern.nxcals.common.Constants.PART_FIELDS_SEPARATOR;
import static java.time.format.DateTimeFormatter.ofPattern;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;

@Slf4j
@Service
public class ExtractionResourceService {
    private static final int SEED = 42; //This seed is used by Spark
    public static final String EQUAL_SIGN = "=";
    private final DataLocationProperties properties;
    private final DateGrouping dateGrouping;
    private final Function<EntityHistory, String> hbaseTableNameProvider;
    private final Function<EntityHistory, String> hdfsPathProvider;
    private final PartitionResourceService partitionResourceService;
    private final PartitionResourceHistoryService partitionResourceHistoryService;
    private final Connection hbaseConnection;
    private final LoadingCache<TableName, Boolean> tableCache;
    @VisibleForTesting
    @Getter(AccessLevel.PACKAGE)
    final DateTimeFormatter dataDirDatePattern = ofPattern("yyyy/M/d");
    @VisibleForTesting
    @Setter(AccessLevel.PACKAGE)
    private BiFunction<EntityHistory, TimeWindow, List<PartitionResourceHistoryData>> partitionResourceHistoryProvider;
    private final ExtractionSourceConfigService sourceConfigProvider;

    @SuppressWarnings("squid:S00107") //nb of arguments exceeded.
    @Autowired
    public ExtractionResourceService(DataLocationProperties properties, DateGrouping dateGrouping,
            @Qualifier("hbaseTableProvider") Function<EntityHistory, String> hbaseTableProvider,
            @Qualifier("hdfsPathProvider") Function<EntityHistory, String> hdfsPathProvider,
            EntityService entityService, VariableService variableService, SystemSpecRepository systemRepository,
            @NonNull PartitionResourceService partitionResourceService,
            @NonNull PartitionResourceHistoryService partitionResourceHistoryService,
            Connection hbaseConnection, @Value("${service.hbase.cache.expiration:PT2H}") Duration cacheExpiration) {
        this.properties = properties;
        this.dateGrouping = dateGrouping;
        this.hbaseTableNameProvider = hbaseTableProvider;
        this.hdfsPathProvider = hdfsPathProvider;
        this.partitionResourceService = partitionResourceService;
        this.partitionResourceHistoryService = partitionResourceHistoryService;
        this.hbaseConnection = hbaseConnection;
        this.tableCache = Caffeine.newBuilder().expireAfterAccess(cacheExpiration)
                .removalListener((name, table, removalCause) -> log.debug("Removing table {} from cache", table))
                .build(this::isTableAccessible);
        this.partitionResourceHistoryProvider = this::getPartitionResourceHistoryData;
        this.sourceConfigProvider = new ExtractionSourceConfigService(entityService, variableService,
                systemRepository);
    }

    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public Collection<VariableExtractionResource> findVariableResourcesBy(ExtractionCriteria criteria) {
        return getVariableExtractionResources(criteria);
    }

    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public Collection<EntityExtractionResource> findEntityResourcesBy(ExtractionCriteria criteria) {
        return getEntityExtractionResources(criteria);
    }

    private Collection<VariableExtractionResource> getVariableExtractionResources(ExtractionCriteria criteria) {
        Collection<VariableSourceConfig> variableSourceConfigs = sourceConfigProvider.getVariableConfigsFor(criteria);

        TimeWindow timeWindow = criteria.getTimeWindow();
        return variableSourceConfigs.stream()
                .flatMap(config -> getVariableResources(config, timeWindow.intersect(config.configValidity())))
                .collect(toSet());
    }

    private Collection<EntityExtractionResource> getEntityExtractionResources(ExtractionCriteria criteria) {
        Collection<Entity> entities = sourceConfigProvider.getEntitiesFor(criteria);

        TimeWindow timeWindow = criteria.getTimeWindow();
        return entities.stream()
                .flatMap(entity -> getEntityResources(entity, timeWindow))
                .collect(toSet());
    }

    private Stream<VariableExtractionResource> getVariableResources(VariableSourceConfig config, TimeWindow window) {
        return getEntityHistoryWithResourcesFor(config.getEntity(), window)
                .map(ehWithResource -> {
                    EntityHistory entityHistory = ehWithResource.getLeft();
                    Resource resource = ehWithResource.getRight();
                    return buildVariableResource(entityHistory, window, resource, config);
                });
    }

    private Stream<EntityExtractionResource> getEntityResources(Entity entity, TimeWindow window) {
        return getEntityHistoryWithResourcesFor(entity, window)
                .map(ehWithResource -> {
                    EntityHistory entityHistory = ehWithResource.getLeft();
                    Resource resource = ehWithResource.getRight();
                    return buildEntityResource(entityHistory, window, resource);
                });
    }

    private Stream<Pair<EntityHistory, Resource>> getEntityHistoryWithResourcesFor(Entity entity,
            TimeWindow timeWindow) {
        Map<Pair<Long, Long>, Resource> schemaAndPartitionResources = groupResourcesBySchemaAndPartition(entity,
                timeWindow);
        Stream<EntityHistory> validHistories = entity.getEntityHistory().stream()
                .filter(eh -> eh.getValidity().intersects(timeWindow));
        return validHistories.map(eh -> {
            Pair<Long, Long> schemaIdAndPartitionId = Pair.of(eh.getEntitySchema().getId(), eh.getPartition().getId());
            return Pair.of(eh, schemaAndPartitionResources.get(schemaIdAndPartitionId));
        });
    }

    private EntityExtractionResource buildEntityResource(EntityHistory eh, TimeWindow tw, Resource resource) {
        return EntityExtractionResource.builder().entityHistory(eh).resource(resource)
                .timeWindow(tw.intersect(eh.getValidity())).build();
    }

    private VariableExtractionResource buildVariableResource(EntityHistory eh, TimeWindow tw, Resource resource,
            VariableSourceConfig config) {
        return VariableExtractionResource.builder().entityHistory(eh).resource(resource)
                .timeWindow(tw.intersect(eh.getValidity()))
                .variableConfig(config.getVariableConfig()).build();
    }

    private Map<Pair<Long, Long>, Resource> groupResourcesBySchemaAndPartition(Entity entity, TimeWindow timeWindow) {
        return entity.getEntityHistory().stream()
                .collect(groupingBy(t -> Pair.of(t.getEntitySchema().getId(), t.getPartition().getId()))).entrySet()
                .stream().collect(Collectors.toMap(Map.Entry::getKey, e -> buildResource(timeWindow, e.getValue())));
    }

    @VisibleForTesting
    Resource buildResource(TimeWindow configValidity, List<EntityHistory> value) {
        Resource.Builder builder = Resource.builder().hbaseNamespace(properties.getHbaseNamespace());
        for (EntityHistory eh : value) {
            TimeWindow searchWindow = configValidity.intersect(eh.getValidity());
            log.debug("Building resource for TimeWindow {} and  EntityHistory {}, intersected {}", configValidity, eh,
                    searchWindow);

            //find partitioning for a given time window, partition & schema
            List<PartitionResourceHistoryData> prList = this.partitionResourceHistoryProvider.apply(eh, searchWindow);

            if (prList.isEmpty()) {
                //old style
                Set<String> dates = dateGrouping.group(searchWindow);
                builder.hdfsPaths(fileLocations(dates, hdfsPathProvider.apply(eh), properties.getSuffix()));
            } else {
                //adaptive style
                addAdaptivePaths(builder, eh, searchWindow, prList);
            }

            String tableName = hbaseTableNameProvider.apply(eh);
            builder.hbaseTableName(tableName);
            TableName name = TableName.valueOf(properties.getHbaseNamespace(), tableName);
            boolean accessible = this.tableCache.get(name);
            if (!accessible) {
                this.tableCache.invalidate(name);
            }
            builder.hbaseTableAccessible(accessible);
        }

        Resource resource = builder.build();
        log.debug("Resource built {}", resource);
        return resource;
    }

    @NotNull
    private List<PartitionResourceHistoryData> getPartitionResourceHistoryData(EntityHistory eh,
            TimeWindow searchWindow) {
        return this.partitionResourceService
                .findBySystemIdAndPartitionIdAndSchemaId(eh.getEntity().getSystemSpec().getId(),
                        eh.getPartition().getId(), eh.getEntitySchema().getId())
                .map(prd -> this.partitionResourceHistoryService
                        .findByPartitionResourceAndTimeWindow(prd, searchWindow))
                .orElse(Collections.emptyList());
    }

    private void addAdaptivePaths(Resource.Builder builder, EntityHistory eh, TimeWindow searchWindow,
            List<PartitionResourceHistoryData> prhList) {

        //We can use this map for easy queries if the current day is actually partitioned.
        //Here the assumption is made that the partitioning is actually done daily, with the valid from stamp being
        //the start of the day.
        Map<Instant, PartitionResourceHistoryData> dayToPartitionResourceHistoryMap = prhList
                .stream()
                .filter(this::validate)
                .collect(toMap(PartitionResourceHistoryData::getValidFromStamp, Function.identity()));

        long startTimeNanos = searchWindow.getStartTimeNanos();
        long endTimeNanos = searchWindow.getEndTimeNanos();

        Instant startDay = searchWindow.getStartTime().truncatedTo(ChronoUnit.DAYS);
        Instant endDay = searchWindow.getEndTime().truncatedTo(ChronoUnit.DAYS);
        Instant endDayPlusOne = endDay.plus(1, ChronoUnit.DAYS);
        Instant currentDay = startDay; //we go with a day-by-day iteration

        //Must handle start-days, mid-days, end-days, and queries within one day
        //Iteration goes day-by-day
        while (currentDay.isBefore(endDayPlusOne)) {
            PartitionResourceHistoryData prh = dayToPartitionResourceHistoryMap.get(currentDay);
            if (prh != null) {
                TimeEntityPartitionType partitionType = TimeEntityPartitionType.from(prh.getInformation());
                String entityBucketStr = Long.toString(
                        getEntityBucket(eh.getEntity().getId(), partitionType.getEntityBucketCount()));
                long startPartition = getTimePartition(startTimeNanos, partitionType.getTimePartitionCount());
                long endPartition = getTimePartition(endTimeNanos, partitionType.getTimePartitionCount());

                if (currentDay.equals(startDay) && startDay.equals(endDay)) {
                    //query is within one day, we need to calculate the start-end time partition for it
                    addAdaptiveDailyPaths(builder, eh, currentDay, startPartition, endPartition, entityBucketStr);
                } else if (currentDay.equals(startDay)) {
                    //handle start day - from start partition till the end
                    addAdaptiveDailyPaths(builder, eh, currentDay, startPartition,
                            partitionType.getTimePartitionCount() - 1L, entityBucketStr);
                } else if (currentDay.equals(endDay)) {
                    //handle end day
                    addAdaptiveDailyPaths(builder, eh, currentDay, 0, endPartition, entityBucketStr);
                } else {
                    //handle mid-day, all time partitions, only entity bucket selection
                    addAdaptiveDailyPath(builder, eh, currentDay, "*", entityBucketStr);
                }
            } else {
                //no adaptive partitioning for this day, have to generate normal paths.
                Set<String> dates = dateGrouping.group(TimeWindow.between(currentDay, currentDay));
                builder.hdfsPaths(fileLocations(dates, hdfsPathProvider.apply(eh), properties.getSuffix()));
            }
            currentDay = currentDay.plus(1, ChronoUnit.DAYS);
        }

    }

    private boolean validate(PartitionResourceHistoryData prh) {
        Instant validFromStamp = prh.getValidFromStamp();
        if (!validFromStamp.equals(validFromStamp.truncatedTo(ChronoUnit.DAYS))) {
            log.error(
                    "Wrong start date for partition resource, not equal to start of day, PartitionResourceHistory id={}, start date={}",
                    prh.getId(), validFromStamp);
            return false;
        } else {
            return true;
        }
    }

    private void addAdaptiveDailyPath(Resource.Builder builder, EntityHistory eh, Instant currentDay,
            String timePartStr, String entityBucketStr) {
        builder.hdfsPath(
                fileLocation(hdfsPathProvider.apply(eh), currentDay, timePartStr, entityBucketStr,
                        properties.getSuffix()));
    }

    /**
     * We need to add the URIs one by one for each time partition as URI does not accept glob characters like {0,1,2}
     */
    private void addAdaptiveDailyPaths(Resource.Builder builder, EntityHistory eh, Instant currentDay,
            long startTimePartition, long endTimePartition, String entityBucketStr) {
        LongStream.rangeClosed(startTimePartition, endTimePartition)
                .boxed()
                .map(String::valueOf)
                .forEach(timePartStr ->
                        builder.hdfsPath(
                                fileLocation(hdfsPathProvider.apply(eh), currentDay, timePartStr, entityBucketStr,
                                        properties.getSuffix())));
    }

    /**
     * long intervalInSeconds = SECONDS_IN_DAY / partInfo.getTimePartitions();
     * Column partitionColumn = dataset.col(timestampColumn)
     * .divide(functions.lit(TimeUtils.CONVERT_EPOCH_NANOS_TO_SECONDS_VALUE)) to seconds only
     * .mod(functions.lit(SECONDS_IN_DAY)) will have seconds since the beginning of the day
     * .divide(functions.lit(intervalInSeconds)) dividing by interval lenght will get the partition number
     * .cast(DataTypes.LongType); interested in integrals only
     *
     * @param timestamp
     * @param partitionCount
     * @return
     */
    @VisibleForTesting
    static long getTimePartition(long timestamp, int partitionCount) {

        return ((timestamp / TimeUtils.CONVERT_EPOCH_NANOS_TO_SECONDS_VALUE) % SECONDS_IN_DAY) / (SECONDS_IN_DAY
                / partitionCount);
    }

    /**
     * Gets the bucket from the id & bucket count. Uses the same method as as Spark hashing.
     */
    @VisibleForTesting
    static long getEntityBucket(long val, long nbOfBuckets) {
        return MathUtils.pmod(Murmur3_x86_32.hashLong(val, SEED), nbOfBuckets);
    }

    private URI fileLocation(String prefixPath, Instant date, String timePartitions, String entityBuckets,
            String suffix) {

        return URI.create(prefixPath +
                Path.SEPARATOR +
                dataDirDatePattern.format(ZonedDateTime.ofInstant(date, ZoneId.of("UTC"))) +
                Path.SEPARATOR +
                SystemFields.NXC_TIME_PARTITION_COLUMN.getValue() +
                EQUAL_SIGN +
                timePartitions +
                PART_FIELDS_SEPARATOR +
                SystemFields.NXC_ENTITY_BUCKET_COLUMN.getValue() +
                EQUAL_SIGN +
                entityBuckets +
                PART_FIELDS_SEPARATOR +
                suffix
        );

    }

    private Set<URI> fileLocations(Set<String> dates, String prefix, String suffix) {
        return dates.stream().map(date -> String.join(Path.SEPARATOR, prefix, date, suffix)).map(URI::create)
                .collect(toSet());
    }

    private boolean isTableAccessible(TableName name) {
        try (Admin hbaseAdmin = hbaseConnection.getAdmin()) {
            if (hbaseAdmin.tableExists(name)) {
                boolean tableAvailable = hbaseAdmin.isTableAvailable(name);
                boolean tableEnabled = hbaseAdmin.isTableEnabled(name);
                boolean ret = tableAvailable && tableEnabled;
                if (!(ret)) {
                    log.warn("Hbase table exists {} but it is not accessible, available={}, enabled={}", name,
                            tableAvailable, tableEnabled);
                } else {
                    log.debug("Hbase table ok {}", name);
                }
                return ret;
            } else {
                log.debug("Hbase table does not exist {}", name);
                return false;
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
