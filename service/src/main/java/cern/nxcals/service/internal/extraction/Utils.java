package cern.nxcals.service.internal.extraction;

import com.google.common.base.Preconditions;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.avro.Schema;
import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Slf4j
@UtilityClass
public final class Utils {
    public static final Schema STRING_SCHEMA = new Schema.Parser().parse("[\"string\",\"null\"]");

    public static List<Schema.Field> extractSchemaFields(String schemaContent) {
        if (StringUtils.isBlank(schemaContent)) {
            return Collections.emptyList();
        }
        Schema entityKeyDefinitionSchema = new Schema.Parser().parse(schemaContent);
        List<Schema.Field> fields = entityKeyDefinitionSchema.getFields();
        Preconditions.checkArgument(!fields.isEmpty(),
                "Cannot extract timestamp field. No fields in time key definition.");
        return fields;
    }

    public static boolean isMapEmpty(Map<?, ?> map) {
        return map == null || map.isEmpty();
    }

}
