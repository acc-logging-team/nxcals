package cern.nxcals.service.internal.compaction;

import cern.nxcals.common.domain.CompactionJob;
import cern.nxcals.common.domain.DataProcessingJob.JobType;
import cern.nxcals.common.paths.StagingPath;
import lombok.NonNull;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.util.Collection;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static cern.nxcals.common.domain.DataProcessingJob.JobType.COMPACT;

/**
 * This is the previous implementation of the compaction. Kept for information, to be deleted once adaptive is in place
 * and well tested.
 */
public final class CompactionJobCreator extends DataProcessingJobCreator<CompactionJob> {

    private final Predicate<StagingPath> pathChecker;

    public CompactionJobCreator(@NonNull Predicate<StagingPath> pathChecker,
                                long sortAbove, long maxPartitionSize, int avroParquetSizeRatio,
                                @NonNull String outputPrefix, @NonNull String stagingPrefix, @NonNull FileSystem fs) {
        super(sortAbove, maxPartitionSize,
                avroParquetSizeRatio, outputPrefix, stagingPrefix, fs);
        this.pathChecker = pathChecker;
    }

    @Override
    protected CompactionJob createJob(
            @NonNull StagingPath group, @NonNull Collection<FileStatus> files, long jobSize, long totalSize,
            @NonNull String filePrefix) {
        return CompactionJob.builder()
                .filePrefix(filePrefix)
                .systemId(group.getSystemId())
                .sourceDir(group.toPath().toUri())
                .destinationDir(getHdfsPathCreator().toOutputUri(group))
                .totalSize(totalSize)
                .jobSize(jobSize)
                .partitionCount(getSizeBasedPartitionCount(jobSize))
                .sortEnabled(jobSize > getSortAbove())
                .files(files.stream().map(FileStatus::getPath).map(Path::toUri).collect(Collectors.toList()))
                .build();
    }

    /**
     * We process this only if it is not valid for Adaptive Compaction.
     *
     * @return the opposite to Active Compaction
     */
    @Override
    public Predicate<StagingPath> getPathChecker() {
        return this.pathChecker;
    }

    @Override
    public JobType getJobType() {
        return COMPACT;
    }

}
