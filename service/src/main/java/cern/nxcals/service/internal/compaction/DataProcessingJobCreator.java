package cern.nxcals.service.internal.compaction;

import cern.nxcals.common.domain.DataProcessingJob;
import cern.nxcals.common.domain.DataProcessingJob.JobType;
import cern.nxcals.common.paths.StagingPath;
import cern.nxcals.common.utils.HdfsFileUtils;
import com.google.common.base.Preconditions;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Slf4j
public abstract class DataProcessingJobCreator<T extends DataProcessingJob> {

    @Getter(AccessLevel.PROTECTED)
    private final FileSystem fs;

    @Getter(AccessLevel.PROTECTED)
    private final long sortAbove;
    @Getter(AccessLevel.PROTECTED)
    private final long maxPartitionSize;
    @Getter
    private final String stagingPrefix;
    @Getter(AccessLevel.PROTECTED)
    private final HdfsDataProcessPathCreator hdfsPathCreator;

    protected DataProcessingJobCreator(long sortAbove, long maxPartitionSize, int avroParquetSizeRatio,
            @NonNull String outputPrefix, @NonNull String stagingPrefix, @NonNull FileSystem fs) {
        Preconditions.checkArgument(sortAbove >= 0);
        Preconditions.checkArgument(maxPartitionSize > 0);
        Preconditions.checkArgument(avroParquetSizeRatio > 0);
        this.sortAbove = sortAbove * avroParquetSizeRatio;
        this.maxPartitionSize = Math.max(maxPartitionSize * avroParquetSizeRatio, maxPartitionSize);
        this.stagingPrefix = stagingPrefix;
        this.hdfsPathCreator = new HdfsDataProcessPathCreator(outputPrefix);
        this.fs = fs;
    }

    protected abstract Predicate<StagingPath> getPathChecker();

    public Optional<T> create(@NonNull StagingPath group, Collection<FileStatus> files) {
        Preconditions.checkArgument(CollectionUtils.isNotEmpty(files), "No files for compaction job");
        if (!getPathChecker().test(group)) {
            log.warn("Requested job staging path of job type [{}] is not valid for {}, will return empty job "
                    + "collection", getJobType(), group);
            return Optional.empty();
        }
        Map<Long, List<FileStatus>> filesByHour = files.stream().parallel()
                .collect(Collectors.groupingBy(this::toTimePartition));
        if (hasUnbucketedData(filesByHour)) {
            return Optional.ofNullable(dailyJob(group, files));
        } else {
            return Optional.ofNullable(timePartitionedJob(group, filesByHour));
        }
    }

    private boolean hasUnbucketedData(Map<Long, List<FileStatus>> filesByHour) {
        return filesByHour.containsKey(Long.MIN_VALUE);
    }

    private T dailyJob(StagingPath group, Collection<FileStatus> files) {
        long totalSize = sizeOf(files);
        return createJob(group, files, totalSize, totalSize, prefix(0L, null));
    }

    //Returns biggest job for the day (in time partition split)
    private T timePartitionedJob(StagingPath group, Map<Long, List<FileStatus>> filesByPartition) {
        Map<Long, Long> partitionSizes = aggregateSizes(filesByPartition);
        long totalSize = partitionSizes.values().stream().mapToLong(x -> x).sum();

        // sorted partitions
        Deque<Long> partitions = partitionSizes.keySet().stream().sorted()
                .collect(Collectors.toCollection(LinkedList::new));

        T job = null;
        while (!partitions.isEmpty()) {
            long jobSize = 0;
            Long startAt = partitions.peek();
            List<FileStatus> currentFiles = new ArrayList<>();
            while (!partitions.isEmpty() && jobSize < maxPartitionSize) {
                Long currentBucket = partitions.pop();
                jobSize += partitionSizes.get(currentBucket);
                currentFiles.addAll(filesByPartition.get(currentBucket));
            }
            if (job == null || job.getJobSize() < jobSize) {
                job = createJob(group, currentFiles, jobSize, totalSize, prefix(startAt, partitions.peek()));
            }
        }

        //returns the biggest job for the day
        return job;
    }

    /**
     * Should be overwritten by derived classes.
     *
     * @param group - group
     * @param files - list of files
     * @param jobSize - this job size
     * @param totalSize - total size for a given day
     * @param filePrefix - prefix
     * @return - a job
     */
    protected T createJob(StagingPath group, Collection<FileStatus> files, long jobSize, long totalSize,
            String filePrefix) {
        return null;
    }

    public abstract JobType getJobType();

    private Map<Long, Long> aggregateSizes(Map<Long, List<FileStatus>> filesByHour) {
        return filesByHour.entrySet().parallelStream()
                .collect(Collectors.toMap(Map.Entry::getKey, t -> sizeOf(t.getValue())));
    }

    protected static long sizeOf(Collection<FileStatus> files) {
        return files.stream().mapToLong(FileStatus::getLen).sum();
    }

    private String prefix(Long startAt, Long endAt) {
        return startAt + "_" + (endAt == null ? "MAX" : endAt) + "_P-";
    }

    /**
     * Trying to bucket by associated time partition (from ETL) that is after the date /2024-01-01/xxx/*.avro If xxx
     * parses to a Long it will be handled as this time partition. If not it means that this is some non-partitioned
     * data, likely a parquet file, leftover from some failed compaction. It will be than assigned to a MIN_VALUE bucket
     * (to be handled later).
     *
     * @param file
     * @return a parsed time partition or Long.MIN_VALUE if it cannot be done
     */
    protected long toTimePartition(FileStatus file) {
        return HdfsFileUtils.getTimePartition(file.getPath()).orElse(Long.MIN_VALUE);
    }

    protected int getSizeBasedPartitionCount(long size) {
        return ((int) (size / maxPartitionSize)) + 1;
    }
}
