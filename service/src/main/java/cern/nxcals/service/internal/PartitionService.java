package cern.nxcals.service.internal;

import cern.nxcals.api.domain.Partition;
import cern.nxcals.service.domain.PartitionData;
import cern.nxcals.service.domain.PartitionPropertiesData;
import cern.nxcals.service.domain.SystemSpecData;
import cern.nxcals.service.repository.PartitionRepository;
import cern.nxcals.service.repository.SystemSpecRepository;
import cern.nxcals.service.rest.exceptions.ConfigDataConflictException;
import cern.nxcals.service.rest.exceptions.NotFoundRuntimeException;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;

import static cern.nxcals.common.utils.KeyValuesUtils.convertMapIntoAvroSchemaString;

/**
 * The implementation of the {@link cern.nxcals.service.internal.PartitionService} interface.
 */
@Service
@Slf4j
@AllArgsConstructor
public class PartitionService {
    private static final String SYSTEM_NOT_FOUND_ERROR_FORMAT = "No system found for id %s";
    @NonNull
    private final SystemSpecRepository systemRepository;
    @NonNull
    private final PartitionRepository repository;
    @NonNull
    @PersistenceContext
    private final EntityManager entityManager;

    //updates only partitionProperties value
    @Transactional(transactionManager = "jpaTransactionManager")
    public PartitionData update(@NonNull Partition partition) {
        PartitionData oldPartitionData = repository.findById(partition.getId())
                .orElseThrow(
                        () -> new IllegalArgumentException("No partition with id " + partition.getId() + " found"));
        entityManager.detach(oldPartitionData);
        PartitionData newData = updateValues(oldPartitionData, partition);
        return repository.save(newData);
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public PartitionData create(@NonNull Partition partition) {
        if (partition.hasId()) {
            throw existingPartition(partition);
        }
        SystemSpecData system = getSystemSpecOrThrow(partition);
        PartitionData data = new PartitionData();
        data.setKeyValues(convertMapIntoAvroSchemaString(partition.getKeyValues(),
                partition.getSystemSpec().getPartitionKeyDefinitions()));
        if (partition.getProperties() != null) {
            data.setProperties(PartitionPropertiesData.from(partition.getProperties()));
        }
        data.setSystem(system);
        return repository.save(data);
    }

    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public List<PartitionData> findAll(String search) {
        return this.repository.queryAll(search, PartitionData.class);
    }

    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public Optional<PartitionData> findById(long id) {
        return this.repository.findById(id);
    }

    private PartitionData updateValues(PartitionData oldPartitionData, Partition partition) {
        oldPartitionData.setProperties(PartitionPropertiesData.from(partition.getProperties()));
        oldPartitionData.setRecVersion(partition.getRecVersion());
        return oldPartitionData;
    }

    private SystemSpecData getSystemSpecOrThrow(Partition partition) {
        return this.systemRepository.findById(partition.getSystemSpec().getId()).orElseThrow(
                () -> new NotFoundRuntimeException(
                        String.format("Cannot assign partition [%s] to system by id [%s]. System does not exist",
                                partition.getKeyValues(), partition.getSystemSpec().getId())));
    }

    private RuntimeException existingPartition(Partition partition) {
        return new ConfigDataConflictException(
                String.format("Cannot create Partition [%s]. Partition already exists", partition.getId()));
    }
}
