package cern.nxcals.service.internal;

import cern.nxcals.service.domain.HierarchyVariablesChangelogData;
import cern.nxcals.service.repository.HierarchyVariablesChangelogRepository;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Slf4j
@AllArgsConstructor
public class HierarchyVariablesChangelogService {

    @NonNull
    private final HierarchyVariablesChangelogRepository hierarchyVariablesChangelogRepository;

    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public List<HierarchyVariablesChangelogData> findAll(String search) {
        return this.hierarchyVariablesChangelogRepository.queryAll(search, HierarchyVariablesChangelogData.class);
    }
}
