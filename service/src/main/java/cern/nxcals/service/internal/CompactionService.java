package cern.nxcals.service.internal;

import cern.nxcals.common.domain.DataProcessingJob;
import cern.nxcals.common.domain.DataProcessingJob.JobType;
import cern.nxcals.common.paths.StagingPath;
import cern.nxcals.common.utils.HdfsFileUtils;
import cern.nxcals.service.config.CompactionProperties;
import cern.nxcals.service.config.DataLocationProperties;
import cern.nxcals.service.config.HdfsPathSearchProperties;
import cern.nxcals.service.internal.compaction.AdaptiveCompactionJobCreator;
import cern.nxcals.service.internal.compaction.AdaptiveMergeCompactionJobCreator;
import cern.nxcals.service.internal.compaction.CompactionJobCreator;
import cern.nxcals.service.internal.compaction.DataProcessingJobCreator;
import cern.nxcals.service.internal.compaction.MergeCompactionJobCreator;
import cern.nxcals.service.internal.compaction.RestageJobCreator;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.PathFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.time.Clock;
import java.time.Duration;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.EnumMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.time.DurationFormatUtils.formatDurationHMS;

/*
    Careful!

    This algorithm has been designed to consume memory (almost) proportional to the size of the requested
    job batch, *not* the count of the directories on HDFS.

    If you modify it, make sure to maintain this invariant.
 */

@Service
@Slf4j
@EnableConfigurationProperties({ DataLocationProperties.class, CompactionProperties.class })
@SuppressWarnings("java:S107") //too many parameters to constructor

public class CompactionService {
    private final FileSystem fs;
    private final String stagingDir;
    private final String restagingDir;

    private final Duration compactionTolerance;
    private final Duration minModificationMargin;
    private final Clock clock;
    private final Map<JobType, HdfsPathSearchProperties> pathSearchProperties;
    private final PartitionService partitionService;
    private final PartitionResourceService partitionResourceService;
    private final PartitionResourceHistoryService partitionResourceHistoryService;

    private final Map<JobType, DataProcessingJobCreator<?>> jobCreators = new EnumMap<>(JobType.class);
    private final long maxNumberOfStagingPartitions;

    //Only for testing
    private final Predicate<StagingPath> pathChecker;

    @Autowired
    public CompactionService(
            @NonNull PartitionService partitionService,
            @NotNull PartitionResourceService partitionResourceService,
            @NotNull PartitionResourceHistoryService partitionResourceHistoryService,
            @NotNull FileSystem fs,
            @NotNull DataLocationProperties dataLocationProperties,
            @NotNull CompactionProperties compactionProperties) {
        this(null, partitionService, partitionResourceService,
                partitionResourceHistoryService, fs, Clock.systemDefaultZone(),
                dataLocationProperties, compactionProperties);
    }

    CompactionService(
            Predicate<StagingPath> pathChecker,
            @NotNull PartitionService partitionService,
            @NotNull PartitionResourceService partitionResourceService,
            @NotNull PartitionResourceHistoryService partitionResourceHistoryService,
            @NotNull FileSystem fs,
            @NotNull Clock clock,
            @NotNull DataLocationProperties dataLocationProperties,
            @NotNull CompactionProperties config) {

        this.pathChecker = pathChecker;
        this.fs = fs;
        this.clock = clock;

        this.partitionService = partitionService;
        this.partitionResourceService = partitionResourceService;
        this.partitionResourceHistoryService = partitionResourceHistoryService;

        this.stagingDir = dataLocationProperties.getHdfsStagingDir();
        this.restagingDir = dataLocationProperties.getHdfsRestagingDir();
        this.pathSearchProperties = config.getFileSearchProperties();

        this.compactionTolerance = Duration.ofHours(config.getTolerancePeriodHours());
        this.minModificationMargin = Duration.ofHours(config.getMinFileModificationPeriodHours());
        this.maxNumberOfStagingPartitions = config.getStagingPartitionSplits().getSize();
        loadJobCreators(config, dataLocationProperties);
    }

    private void loadJobCreators(CompactionProperties config, DataLocationProperties dataLocationProperties) {

        AdaptiveCompactionJobCreator adaptiveCompactionJobCreator = new AdaptiveCompactionJobCreator(
                this.partitionService,
                this.partitionResourceService,
                this.partitionResourceHistoryService,
                this.maxNumberOfStagingPartitions,
                config.getSortThreshold(),
                config.getMaxPartitionSize(),
                config.getAvroParquetSizeRatio(),
                dataLocationProperties.getHdfsDataDir(),
                stagingDir, fs);
        jobCreators.put(JobType.ADAPTIVE_COMPACT, adaptiveCompactionJobCreator);
        AdaptiveMergeCompactionJobCreator adaptiveMergeCompactionJobCreator = new AdaptiveMergeCompactionJobCreator(
                config.getSortThreshold(),
                config.getMaxPartitionSize(), config.getAvroParquetSizeRatio(),
                dataLocationProperties.getHdfsDataDir(), restagingDir, fs, partitionResourceService,
                partitionResourceHistoryService, adaptiveCompactionJobCreator.getPathChecker());
        jobCreators.put(JobType.ADAPTIVE_MERGE_COMPACT, adaptiveMergeCompactionJobCreator);

        Predicate<StagingPath> compactionPathChecker = adaptiveCompactionJobCreator.getPathChecker().negate();
        jobCreators.put(JobType.COMPACT,
                new CompactionJobCreator(
                        this.pathChecker == null ?
                                compactionPathChecker :
                                this.pathChecker, //for testing only
                        config.getSortThreshold(), config.getMaxPartitionSize(),
                        config.getAvroParquetSizeRatio(), dataLocationProperties.getHdfsDataDir(), stagingDir, fs));

        jobCreators.put(JobType.RESTAGE,
                new RestageJobCreator(config.getSortThreshold(), config.getAvroParquetSizeRatio(),
                        dataLocationProperties.getHdfsDataDir(), restagingDir, fs));

        jobCreators.put(JobType.MERGE_COMPACT,
                new MergeCompactionJobCreator(config.getSortThreshold(), config.getMaxPartitionSize(),
                        config.getAvroParquetSizeRatio(), dataLocationProperties.getHdfsDataDir(), restagingDir, fs,
                        this.pathChecker == null ?
                                compactionPathChecker :
                                this.pathChecker)); // for testing only
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public List<DataProcessingJob> getDataProcessJobs(int maxJobs, JobType jobType) {
        Objects.requireNonNull(jobType, "Data process job type must not be null!");
        if (maxJobs <= 0) {
            throw new IllegalArgumentException("Negative compaction jobs count: " + maxJobs);
        }

        log.info("Getting compaction jobs for [{}] with limit {}", jobType, maxJobs);
        long compactionBefore = clock.instant().minus(compactionTolerance).toEpochMilli();
        long modificationBefore = clock.instant().minus(minModificationMargin).toEpochMilli();
        DataProcessingJobCreator<?> jobCreator = jobCreators.get(jobType);
        List<DataProcessingJob> jobs = collect(modificationBefore, compactionBefore,
                pathSearchProperties.computeIfAbsent(jobType, k -> new HdfsPathSearchProperties()),
                jobCreator, maxJobs);
        log.info("Returning {} jobs for [{}]", jobs.size(), jobType);
        return jobs;
    }

    private List<DataProcessingJob> collect(long modificationBefore, long compactionBefore,
            HdfsPathSearchProperties pathSearchProperties, DataProcessingJobCreator<?> jobCreator, long maxJobs) {
        JobType jobType = jobCreator.getJobType();
        String rootDir = jobCreator.getStagingPrefix();

        long t1 = System.currentTimeMillis();
        log.debug("Start finding all searchable paths for [{}]", jobType);

        List<FileStatus> dateDirs = findDateDirs(compactionBefore, rootDir, pathSearchProperties);

        long t2 = System.currentTimeMillis();
        log.debug("Found searchable paths size={} in {}", dateDirs.size(), formatDurationHMS(t2 - t1));

        Deque<List<Path>> pathsByDay = sortByDay(groupByDay(dateDirs));
        log.info("Creating max {} jobs out of {} dated directories for [{}]", maxJobs,
                pathsByDay.size(), jobType);

        long t3 = System.currentTimeMillis();
        log.debug("Looking at last {} days in {}", pathsByDay.size(), formatDurationHMS(t3 - t2));

        List<DataProcessingJob> jobs = new ArrayList<>();
        int daysIncluded = 0;

        while (!pathsByDay.isEmpty() && jobs.size() < maxJobs) {
            daysIncluded++;
            //find data files
            List<Pair<StagingPath, List<FileStatus>>> files = findDataFiles(modificationBefore, pathsByDay);

            //crating jobs out of data files
            files.forEach(pair -> createJobs(jobCreator, maxJobs, jobs, pair));
        }
        long t4 = System.currentTimeMillis();
        log.debug("Collected jobs from {} last days for [{}] in {}", daysIncluded, jobType,
                formatDurationHMS(t4 - t3));
        log.info("Collected {} compaction jobs for [{}] in {} ms", jobs.size(), jobType, formatDurationHMS(t4 - t1));
        return jobs;
    }

    private List<Pair<StagingPath, List<FileStatus>>> findDataFiles(long modificationBefore,
            @NonNull Deque<List<Path>> pathsByDay) {
        return groupByPath(
                Objects.requireNonNull(pathsByDay.pollLast())) // liberating memory during iteration with poll
                .entrySet()
                .parallelStream() //parallel only to find the target files.
                .map(entry -> { //on each staging path find target data files
                    StagingPath stagingPath = entry.getKey();
                    List<FileStatus> filesOnPath = findFiles(stagingPath, entry.getValue(), modificationBefore);
                    return Pair.of(stagingPath, filesOnPath);
                })
                .filter(pair -> CollectionUtils.isNotEmpty(pair.getValue())).collect(toList()); //remove empty
    }

    private void createJobs(
            DataProcessingJobCreator<? extends DataProcessingJob> jobCreator,
            long maxJobs, List<DataProcessingJob> jobs,
            Pair<StagingPath, List<FileStatus>> filesOnPath
    ) {
        Optional<? extends DataProcessingJob> dataProcessingJob = jobCreator.create(filesOnPath.getKey(), filesOnPath.getValue());
        if (jobs.size() < maxJobs && dataProcessingJob.isPresent()) {
            jobs.add(dataProcessingJob.get());
        }
    }

    private List<FileStatus> findFiles(StagingPath stagingPath, List<String> paths, long modificationBefore) {
        try {
            FileStatus[] files = findFilesForDates(paths, modificationBefore);
            if (files.length > 0) {
                return Arrays.stream(files).filter(file -> file.getLen() > 0).collect(toList());
            }
        } catch (FileNotFoundException ex) {
            // We skip this assuming that it will be processed with next compaction so it is safe
            // this can only happen if a directory cleaning from an ETL is taking place at the same time
            log.warn("File not found while processing group {}", stagingPath);
        } catch (IOException e) {
            throw new UncheckedIOException("Cannot list hadoop files for " + stagingPath, e);
        }
        return Collections.emptyList();
    }

    private List<FileStatus> findDateDirs(long before, String rootDir, HdfsPathSearchProperties searchProps) {
        Objects.requireNonNull(rootDir);
        try {
            PathFilter filter = afterMarginFilter(before);
            Path pathPattern = new Path(StringUtils
                    .joinWith(Path.SEPARATOR, rootDir, searchProps.getTopic(), searchProps.getSystem(),
                            searchProps.getPartition(), searchProps.getSchema(), searchProps.getDate()));
            return Arrays.asList(fs.globStatus(pathPattern, filter));
        } catch (IOException e) {
            throw new UncheckedIOException("Cannot process dirs on hadoop", e);
        }
    }

    private FileStatus[] findFilesForDates(List<String> directories, long beforeMillis) throws IOException {
        /*
         Finds all the files with and without the time split

         This is a concatenation of two alternatives:
         - left side is a full list of etl directories for a given partition
         - right side is either an * (everything) or *\/* (everything in a directory of time split)
        */
        String join = "{" + StringUtils.join(directories, ",") + "}{" + Path.SEPARATOR + "*" + Path.SEPARATOR + "*,"
                + Path.SEPARATOR + "*}";

        // do not replace with calls in a for-loop or a remote iterator on HADOOP 2.x - much slower
        FileStatus[] files = fs.globStatus(new Path(join), HdfsFileUtils::isDataFile);
        if (Arrays.stream(files).anyMatch(file -> file.getModificationTime() >= beforeMillis)) {
            return new FileStatus[0]; // too recently modified
        }
        return files;
    }

    private Map<String, List<Path>> groupByDay(Collection<FileStatus> dates) {
        return dates.stream().map(FileStatus::getPath).collect(Collectors.groupingBy(Path::getName, toList()));
    }

    private LinkedList<List<Path>> sortByDay(Map<String, List<Path>> groupedPaths) {
        // partially sorted. By day only. Within a day it may still be unsorted.
        // avoid using TreeSet or any fully sorted data structure - much slower
        return groupedPaths.entrySet().stream().sorted(Map.Entry.comparingByKey()).map(Map.Entry::getValue)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    private Map<StagingPath, List<String>> groupByPath(List<Path> statuses) {
        return statuses.stream().collect(
                Collectors.groupingBy(StagingPath::new, Collectors.mapping(t -> t.toUri().getPath(), toList())));
    }

    private PathFilter afterMarginFilter(long beforeMargin) {
        return datePath -> {
            long pathDate = toEpochMillis(datePath.getName());
            return pathDate <= beforeMargin;
        };
    }

    private long toEpochMillis(String date) {
        // this has been tested for typical java.time timezone bugs -> seems to be just fine
        return LocalDate.parse(date).atStartOfDay(ZoneOffset.UTC).toInstant().toEpochMilli();
    }
}
