package cern.nxcals.service.internal.security;

import cern.accsoft.ccs.ccda.client.rbac.User;
import cern.accsoft.ccs.ccda.client.rbac.UserService;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.Optional;

@Slf4j
@Service
public class RbaUserService {
    private final Cache<String, User> userByName;
    private final UserService userService;

    public RbaUserService(UserService userService, @Value("${rbac.user.cache.expiration:PT1H}") String expiration) {
        this.userService = userService;
        this.userByName = Caffeine.newBuilder().expireAfterWrite(Duration.parse(expiration)).build();
    }

    public Optional<User> findBy(String username) {
        User user = userByName.get(username, this.userService::findByUserName);
        return Optional.ofNullable(user);
    }
}
