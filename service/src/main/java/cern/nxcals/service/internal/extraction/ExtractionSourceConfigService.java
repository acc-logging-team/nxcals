package cern.nxcals.service.internal.extraction;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.extraction.metadata.queries.Entities;
import cern.nxcals.common.domain.EntityKeyValues;
import cern.nxcals.common.domain.ExtractionCriteria;
import cern.nxcals.service.domain.EntityData;
import cern.nxcals.service.domain.VariableConfigData;
import cern.nxcals.service.domain.VariableData;
import cern.nxcals.service.internal.EntityService;
import cern.nxcals.service.internal.VariableService;
import cern.nxcals.service.internal.component.BulkUtils;
import cern.nxcals.service.repository.SystemSpecRepository;
import cern.nxcals.service.rest.exceptions.NotFoundRuntimeException;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.google.common.collect.Sets;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;
import static org.springframework.util.CollectionUtils.isEmpty;

@AllArgsConstructor
@Service
public class ExtractionSourceConfigService {
    private final EntityService entityService;
    private final VariableService variableService;
    private final SystemSpecRepository systemRepository;

    public Collection<Entity> getEntitiesFor(ExtractionCriteria criteria) {
        TimeWindow window = criteria.getTimeWindow();

        Condition<Entities> query = createCondition(criteria);

        Collection<Entity> entityConfigs = entityService
                .findAll(toRSQL(query), window.getStartTimeNanos(), window.getEndTimeNanos()).stream()
                .map(EntityData::toEntity).collect(toSet());
        verifyIfAllRequiredKeyValuesAreFound(entityConfigs, criteria);
        verifyIfAllRequiredEntityIdsAreFound(entityConfigs, criteria);
        return entityConfigs;
    }

    private Condition<Entities> createCondition(ExtractionCriteria criteria) {
        ArrayList<Condition<Entities>> conditions = createBatchedEntityIdInConditions(criteria.getEntityIds());
        if (!criteria.getEntities().isEmpty()) {
            conditions.add(createKeyValueCondition(criteria));
        }
        return Entities.suchThat().or(conditions);
    }

    private ArrayList<Condition<Entities>> createBatchedEntityIdInConditions(Set<Long> ids) {
        return new ArrayList<>(BulkUtils.batchAndApply(ids, set ->
                Collections.singletonList(createEntityIdInCondition(set))
        ));
    }

    private Condition<Entities> createEntityIdInCondition(Set<Long> ids) {
        return Entities.suchThat().id().in(ids);
    }

    private Condition<Entities> createKeyValueCondition(ExtractionCriteria criteria) {
        SystemSpec system = findSystem(criteria.getSystemName());
        List<Condition<Entities>> conditions = criteria.getEntities().stream()
                .map(entityKeyValues -> entityKeyValues.isAnyWildcard() ?
                        createWildcardCondition(system, entityKeyValues) :
                        createNonWildcardCondition(system, entityKeyValues))
                .collect(Collectors.toList());
        return Entities.suchThat().systemId().eq(system.getId()).and().or(conditions);
    }

    private Condition<Entities> createWildcardCondition(SystemSpec systemSpec, EntityKeyValues entityKeyValues) {
        return Entities.suchThat().keyValues().like(systemSpec, entityKeyValues.getAsMap());
    }

    private Condition<Entities> createNonWildcardCondition(SystemSpec systemSpec, EntityKeyValues entityKeyValues) {
        return Entities.suchThat().keyValues().eq(systemSpec, entityKeyValues.getAsMap());
    }

    private void verifyIfAllRequiredKeyValuesAreFound(Collection<Entity> receivedEntities,
            ExtractionCriteria criteria) {

        Set<Map<String, Object>> foundEntityKeyValues = receivedEntities.stream()
                .map(Entity::getEntityKeyValues)
                .collect(toSet());

        Set<Map<String, Object>> requiredKeyValues = criteria.getEntities().stream()
                .filter(e -> !e.isAnyWildcard())
                .map(EntityKeyValues::getAsMap)
                .collect(Collectors.toSet());

        for (Map<String, Object> keyValues : requiredKeyValues) {
            if (!foundEntityKeyValues.contains(keyValues)) {
                throw new IllegalArgumentException(
                        "Entity with " + keyValues.toString() + " was not found in the system "
                                + criteria.getSystemName());
            }
        }
    }

    private void verifyIfAllRequiredEntityIdsAreFound(Collection<Entity> receivedEntities,
            ExtractionCriteria criteria) {
        Set<Long> foundIds = receivedEntities.stream().map(Entity::getId).collect(toSet());
        for (Long id : criteria.getEntityIds()) {
            if (!foundIds.contains(id)) {
                throw new IllegalArgumentException(
                        "Entity with id: " + id.toString() + " was not found");
            }
        }
    }

    public Collection<VariableSourceConfig> getVariableConfigsFor(ExtractionCriteria criteria) {
        Set<VariableData> variables = getVariablesFor(criteria);
        TimeWindow window = criteria.getTimeWindow();
        Set<VariableConfigData> validConfigs = getValidConfigData(variables, window);

        Map<TimeWindow, Set<Long>> entityIdsByWindow = validConfigs.stream()
                .collect(groupingBy(vc -> vc.validity().intersect(window), HashMap::new,
                        mapping(vc -> vc.getEntity().getId(), toSet())));

        Map<TimeWindow, Map<Long, Entity>> entityByConfigAndQueryWindow = entityIdsByWindow.entrySet().stream()
                .map(this::findEntities)
                .collect(toMap(Pair::getKey, this::groupEntityDataByEntityId));

        return validConfigs.stream().map(vc -> {
            TimeWindow intersect = vc.validity().intersect(window);
            Map<Long, Entity> mapping = entityByConfigAndQueryWindow.get(intersect);
            if (mapping == null) {
                throw new IllegalStateException("Cannot find entity for time window " + intersect);
            }
            Long entityId = vc.getEntity().getId();
            Entity entity = mapping.get(entityId);
            if (entity == null) {
                throw new IllegalStateException("Cannot find entity for id " + entityId);
            }
            return new VariableSourceConfig(entity, vc.toVariableConfig(), vc.getVariable().toVariable());
        }).collect(toSet());
    }

    private Set<VariableData> getVariablesFor(ExtractionCriteria criteria) {
        Set<VariableData> ret = new HashSet<>();
        if (!criteria.getVariables().isEmpty()) {
            ret.addAll(findVariableDataByNames(criteria.getSystemName(), criteria.getVariables()));
        }
        ret.addAll(findVariableDataByIds(criteria.getVariableIds()));
        return ret;
    }

    private Set<VariableData> findVariableDataByNames(String systemName, Map<String, Boolean> variables) {
        SystemSpec system = findSystem(systemName);
        Set<VariableData> ret = new HashSet<>();
        Set<String> names2Query = new HashSet<>();
        for (Map.Entry<String, Boolean> entry : variables.entrySet()) {
            if (entry.getValue()) {
                ret.addAll(variableService.findBySystemIdAndVariableNameLike(system.getId(), entry.getKey()));
            } else {
                names2Query.add(entry.getKey());
            }
        }
        if (!isEmpty(names2Query)) {
            ret.addAll(findAllVariablesByNameAndSystem(system, names2Query));
        }
        return ret;
    }

    private Set<VariableData> findAllVariablesByNameAndSystem(SystemSpec system, Set<String> names) {
        Set<VariableData> foundVariables = variableService
                .findBySystemIdAndVariableNameIn(system.getId(), names);
        if (names.size() == foundVariables.size()) {
            return foundVariables;
        } else {
            Set<String> missingVariables = Sets.difference(names,
                    foundVariables.stream().map(VariableData::getVariableName).collect(toSet()));
            throw new IllegalArgumentException(
                    "Variables: " + missingVariables + " were not found in system " + system.getName());
        }
    }

    private Set<VariableData> findVariableDataByIds(Set<Long> ids) {
        try {
            return variableService.findExactlyByIdIn(ids);
        } catch (NotFoundRuntimeException e) {
            throw new IllegalArgumentException(
                    "Some of variable ids: " + ids + " were not found", e);
        }
    }

    private Set<VariableConfigData> getValidConfigData(Set<VariableData> variables, TimeWindow window) {
        return variables.stream()
                .map(VariableData::getVariableConfigs)
                .flatMap(Collection::stream)
                .filter(c -> isConfigValidFor(c, window))
                .collect(toSet());
    }

    private Map<Long, Entity> groupEntityDataByEntityId(Pair<TimeWindow, Collection<EntityData>> pair) {
        return pair.getValue().stream().collect(toMap(EntityData::getId, EntityData::toEntity));
    }

    private Pair<TimeWindow, Collection<EntityData>> findEntities(Map.Entry<TimeWindow, Set<Long>> entry) {
        TimeWindow window = entry.getKey();
        return Pair.of(window, BulkUtils.batchAndApply(entry.getValue(), ids -> entityService
                .findAll(toRSQL(createEntityIdInCondition(ids)), window.getStartTimeNanos(),
                        window.getEndTimeNanos())));
    }

    private boolean isConfigValidFor(VariableConfigData c, TimeWindow timeWindow) {
        return c.validity().intersects(timeWindow);
    }

    private SystemSpec findSystem(String systemName) {
        return systemRepository.findByName(systemName)
                .orElseThrow(() -> new IllegalStateException("No system found for name = " + systemName))
                .toSystemSpec();
    }
}
