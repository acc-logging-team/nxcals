package cern.nxcals.service.internal;

import cern.nxcals.api.domain.PartitionResource;
import cern.nxcals.api.domain.PartitionResourceHistory;
import cern.nxcals.api.domain.TimeEntityPartitionType;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.service.domain.PartitionResourceData;
import cern.nxcals.service.domain.PartitionResourceHistoryData;
import cern.nxcals.service.repository.PartitionResourceHistoryRepository;
import cern.nxcals.service.repository.PartitionResourceRepository;
import cern.nxcals.service.rest.exceptions.ConfigDataConflictException;
import cern.nxcals.service.rest.exceptions.NotFoundRuntimeException;
import com.google.common.annotations.VisibleForTesting;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@AllArgsConstructor
public class PartitionResourceHistoryService {

    @VisibleForTesting
    static final double MAX_PARTITION_RESOURCE_DIFF = 33.0;

    @NonNull
    @PersistenceContext
    private final EntityManager em;
    @NonNull
    private final PartitionResourceHistoryRepository partitionResourceHistoryRepository;
    @NonNull
    private final PartitionResourceRepository partitionResourceRepository;

    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public Optional<PartitionResourceHistoryData> findById(long id) {
        return partitionResourceHistoryRepository.findById(id);
    }

    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public List<PartitionResourceHistoryData> findAll(String search) {
        return partitionResourceHistoryRepository.queryAll(search, PartitionResourceHistoryData.class);
    }

    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public List<PartitionResourceHistoryData> findByPartitionResourceId(long partitionResourceId) {
        return partitionResourceHistoryRepository.findByPartitionResourceId(partitionResourceId);
    }

    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public List<PartitionResourceHistoryData> findByStorageType(String storageType) {
        return partitionResourceHistoryRepository.findByStorageType(storageType);
    }


    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public Optional<PartitionResourceHistoryData> findByPartitionResourceAndTimestamp(PartitionResourceData partitionResource,
                                                                                      Instant timestamp) {
        return partitionResourceHistoryRepository.findByPartitionResourceAndTimestamp(partitionResource, timestamp);
    }

    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public Optional<PartitionResourceHistoryData> findPreviousFor(PartitionResourceHistory partitionResourceHistory) {

        return partitionResourceHistoryRepository.findByPartitionResourceIdAndValidToStamp(
                partitionResourceHistory.getPartitionResource().getId(),
                partitionResourceHistory.getValidity().getStartTime());
    }


    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public Optional<PartitionResourceHistoryData> findNextFor(PartitionResourceHistory partitionResourceHistory) {
        return partitionResourceHistoryRepository.findByPartitionResourceIdAndValidFromStamp(
                partitionResourceHistory.getPartitionResource().getId(),
                partitionResourceHistory.getValidity().getEndTime()
        );
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public PartitionResourceHistoryData create(@NonNull PartitionResourceHistory partitionResourceHistory) {
        if (partitionResourceHistory.hasId()) {
            throw new ConfigDataConflictException(
                    String.format("There is already a PartitionResourceHistory with id= %d",
                            partitionResourceHistory.getId()));
        }
        PartitionResourceHistoryData result = new PartitionResourceHistoryData();
        PartitionResourceHistoryData res = copyFrom(result, partitionResourceHistory);

        PartitionResourceHistoryData saved = save(res);
        em.flush(); //we have to flush in order for the next find to find the created object.
        return saved;
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public PartitionResourceHistoryData update(@NonNull PartitionResourceHistory partitionResourceHistory) {
        PartitionResourceHistoryData existingPartitionResourceHistory = obtainFrom(partitionResourceHistory);
        em.detach(existingPartitionResourceHistory);
        PartitionResourceHistoryData newPR = copyFrom(existingPartitionResourceHistory, partitionResourceHistory);
        PartitionResourceHistoryData saved = save(newPR);
        em.flush(); //we have to flush in order for the next find to find the created object.
        return saved;
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public void delete(long partitionResourceHistoryDataId) {
        partitionResourceHistoryRepository.deleteById(partitionResourceHistoryDataId);
        em.flush(); //we have to flush in order for the next find to find the created object.
    }

    /**
     * It finds the previous or the next element if any of the exist with preference for the previous.
     */
    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public Optional<PartitionResourceHistoryData> findAdjacentFor(PartitionResourceHistory newPri) {
        return Optional.ofNullable(findPreviousFor(newPri).orElseGet(() -> findNextFor(newPri).orElse(null)));
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public PartitionResourceHistoryData findOrCreatePartitionResourceInfoFor(PartitionResourceData prData, PartitionResourceHistory newPri) {
        return findByPartitionResourceAndTimestamp(prData, newPri.getValidity().getStartTime()).orElseGet(() -> {
            //trying to find the adjacent if exists to see if we can align.
            Optional<PartitionResourceHistoryData> adjacentOpt = findAdjacentFor(newPri);
            if (adjacentOpt.isPresent()) {
                PartitionResourceHistoryData adjacent = adjacentOpt.get();
                TimeEntityPartitionType adjacentType = TimeEntityPartitionType.from(adjacent.getInformation());
                TimeEntityPartitionType newType = TimeEntityPartitionType.from(newPri.getPartitionInformation());
                if (TimeEntityPartitionType.changePercentage(adjacentType, newType) <= MAX_PARTITION_RESOURCE_DIFF) {
                    //the change of type on newPri is within the limits of the previous one, overriding to existing type
                    return create(newPri.toBuilder().partitionInformation(adjacentType.toString()).build());
                }
            }
            //if adjacent not present or not compatible on type, creating a new info
            return create(newPri);
        });
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public PartitionResourceHistory createPartitionResourceHistory(PartitionResourceData partitionResourceData,
            Instant startDate, TimeEntityPartitionType partitionInformation) {

        return PartitionResourceHistory.builder()
                .partitionResource(partitionResourceData.toPartitionResource())
                .validity(TimeWindow.between(startDate, PartitionResourceHistory.to(startDate)))
                .partitionInformation(partitionInformation.toString())
                .isFixedSettings(false)
                .compactionType("daily")
                .storageType("hdfs")
                .build();
    }

    // Utility functions
    private PartitionResourceHistoryData obtainById(long id) {
        return partitionResourceHistoryRepository.findById(id)
                .orElseThrow(() -> NotFoundRuntimeException.missingPartitionResourceHistory(id));
    }

    private PartitionResourceHistoryData obtainFrom(PartitionResourceHistory partitionResourceHistory) {
        return obtainById(partitionResourceHistory.getId());
    }

    private PartitionResourceHistoryData save(PartitionResourceHistoryData partitionResourceHistory) {
        return partitionResourceHistoryRepository.save(partitionResourceHistory);
    }

    private PartitionResourceHistoryData copyFrom(PartitionResourceHistoryData result,
            PartitionResourceHistory partitionResourceHistory) {
        PartitionResourceData partitionResource = findPartitionResource(
                partitionResourceHistory.getPartitionResource());
        result.setPartitionResource(partitionResource);
        result.setInformation(partitionResourceHistory.getPartitionInformation());
        result.setCompactionType(partitionResourceHistory.getCompactionType());
        result.setStorageType(partitionResourceHistory.getStorageType());
        result.setFixedSettings(partitionResourceHistory.isFixedSettings());
        result.setValidFromStamp(partitionResourceHistory.getValidity().getStartTime());
        result.setValidToStamp(partitionResourceHistory.getValidity().getEndTime());
        if (partitionResourceHistory.hasVersion()) {
            result.setRecVersion(partitionResourceHistory.getRecVersion());
        }
        return result;
    }

    private PartitionResourceData findPartitionResource(PartitionResource partitionResource) {
        return partitionResourceRepository.findById(partitionResource.getId())
                .orElseThrow(() -> NotFoundRuntimeException.missingPartitionResource(partitionResource.getId()));
    }

    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public List<PartitionResourceHistoryData> findByPartitionResourceAndTimeWindow(PartitionResourceData prd,
            TimeWindow searchWindow) {
        return partitionResourceHistoryRepository.findByPartitionResourceAndTimestamps(prd, searchWindow.getStartTime(), searchWindow.getEndTime());
    }
}
