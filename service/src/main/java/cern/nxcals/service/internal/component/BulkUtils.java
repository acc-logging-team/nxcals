package cern.nxcals.service.internal.component;

import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;

import static java.util.stream.Collectors.toList;

@Slf4j
public final class BulkUtils {
    private static final int DEFAULT_MAX_BATCH_SIZE = 1000;

    private BulkUtils() {
        throw new IllegalStateException("nope");
    }

    /**
     * Applies a given operation to a collection of elements and provides the result.
     *
     * @param elements the elements to be used as input of the action
     * @param mapper   a {@link Function} that contains the operation to be applied on the elements
     * @return a collection, containing the result of the bulk operation
     */
    public static <T, R> Collection<R> batchAndApply(Collection<T> elements,
            Function<? super Set<T>, Collection<R>> mapper) {
        return batchAndApply(elements, mapper, DEFAULT_MAX_BATCH_SIZE);
    }

    /**
     * Applies a given operation to a collection of elements and provides the result.
     *
     * @param elements the elements to be used as input of the action
     * @param mapper   a {@link Function} that contains the operation to be applied on the elements
     * @param maxBatchSize - max size of the batch
     * @return a collection, containing the result of the bulk operation
     */
    public static <T, R> Collection<R> batchAndApply(Collection<T> elements,
            Function<? super Set<T>, Collection<R>> mapper,
            int maxBatchSize) {
        List<T> sanitizedElements = elements.stream().filter(Objects::nonNull).collect(toList());

        log.trace("Received elements for split bulk operation with total size: {}", elements.size());
        List<Set<T>> elementBatches = Lists.partition(sanitizedElements, maxBatchSize).stream().map(HashSet::new)
                .collect(toList());

        log.trace("Elements separated into {} batches with max size of {} elements each", elementBatches.size(),
                maxBatchSize);

        Set<R> bulkResult = new HashSet<>();
        for (Set<T> elementBatch : elementBatches) {
            log.trace("Applying operation on batch with size: {}", elementBatch.size());
            Collection<R> batchResult = mapper.apply(elementBatch);
            if (batchResult == null) {
                throw new IllegalStateException("The batch operation result can not be null!");
            }
            bulkResult.addAll(batchResult);
        }
        return bulkResult;
    }

}
