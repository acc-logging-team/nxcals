package cern.nxcals.service.internal.compaction;

import cern.nxcals.common.paths.StagingPath;
import cern.nxcals.common.utils.HdfsFileUtils;
import lombok.NonNull;
import org.apache.hadoop.fs.Path;

import java.net.URI;

public final class HdfsDataProcessPathCreator {
    private static final String RESTAGED_DATA_INFO_FILE_NAME = "restaged_data_files.info";

    private final String outputPrefix;

    public HdfsDataProcessPathCreator(@NonNull String outputPrefix) {
        this.outputPrefix = outputPrefix;
    }

    public URI toRestageReportFileUri(StagingPath path) {
        return URI.create(path.toString() + Path.SEPARATOR + RESTAGED_DATA_INFO_FILE_NAME);
    }

    /**
     * @param path staging path
     * @return the root of data directory
     */
    // if we ever create a class of "DataPath" akin to "StagingPath" this logic should be in there (kkrynick)
    public URI toOutputUri(StagingPath path) {
        return URI.create(outputPrefix + Path.SEPARATOR
                + path.getSystemPath().getName() + Path.SEPARATOR
                + path.getPartitionPath().getName() + Path.SEPARATOR
                + path.getSchemaPath().getName() + Path.SEPARATOR
                + HdfsFileUtils.expandDateToNestedPaths(path.getDatePath().getName()));
    }

}
