package cern.nxcals.service.internal;

import cern.nxcals.api.domain.Group;
import cern.nxcals.service.domain.EntityData;
import cern.nxcals.service.domain.GroupData;
import cern.nxcals.service.domain.VariableData;
import cern.nxcals.service.rest.exceptions.AccessDeniedRuntimeException;
import cern.nxcals.service.rest.exceptions.NotFoundRuntimeException;
import cern.nxcals.service.security.AuthenticationFacade;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
@AllArgsConstructor
public class SecureGroupService {
    private final GroupService service;

    @Transactional(transactionManager = "jpaTransactionManager")
    public GroupData create(@NonNull Group group) {
        return service.create(group);
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public GroupData update(@NonNull Group group) {
        Objects.requireNonNull(getExistingWithOwnershipCheck(group)); //validate group
        return service.update(group);
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public void delete(long groupId) {
        GroupData existingGroup = getExistingWithOwnershipCheck(groupId);
        service.delete(existingGroup.getId());
    }

    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public List<GroupData> findAll(@NonNull String search) {
        String query = search.replaceAll("owner.userName", "owner");
        return service.findAll(query).stream().filter(this::isAccessibleForCurrentUser).collect(Collectors.toList());
    }

    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public Map<String, Set<VariableData>> getVariables(long groupId) {
        GroupData existingGroup = getExisting(groupId);
        return service.getVariables(existingGroup.getId());
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public void setVariables(long groupId, @NonNull Map<String, Set<Long>> variableIdsPerAssociation) {
        GroupData existingGroup = getExistingWithOwnershipCheck(groupId);
        service.setVariables(existingGroup.getId(), variableIdsPerAssociation);
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public void addVariables(long groupId, @NonNull Map<String, Set<Long>> variableIdsPerAssociation) {
        GroupData existingGroup = getExistingWithOwnershipCheck(groupId);
        service.addVariables(existingGroup.getId(), variableIdsPerAssociation);
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public void removeVariables(long groupId, @NonNull Map<String, Set<Long>> variableIdsPerAssociation) {
        GroupData existingGroup = getExistingWithOwnershipCheck(groupId);
        service.removeVariables(existingGroup.getId(), variableIdsPerAssociation);
    }

    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public Map<String, Set<EntityData>> getEntities(long groupId) {
        GroupData existingGroup = getExisting(groupId);
        return service.getEntities(existingGroup.getId());
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public void setEntities(long groupId, @NonNull Map<String, Set<Long>> entityIdsPerAssociation) {
        GroupData existingGroup = getExistingWithOwnershipCheck(groupId);
        service.setEntities(existingGroup.getId(), entityIdsPerAssociation);
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public void addEntities(long groupId, @NonNull Map<String, Set<Long>> entityIdsPerAssociation) {
        GroupData existingGroup = getExistingWithOwnershipCheck(groupId);
        service.addEntities(existingGroup.getId(), entityIdsPerAssociation);
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public void removeEntities(long groupId, @NonNull Map<String, Set<Long>> entityIdsPerAssociation) {
        GroupData existingGroup = getExistingWithOwnershipCheck(groupId);
        service.removeEntities(existingGroup.getId(), entityIdsPerAssociation);
    }

    private GroupData getExistingWithOwnershipCheck(@NonNull Group group) {
        if (group.hasId()) {
            return this.getExistingWithOwnershipCheck(group.getId());
        }
        throw new IllegalArgumentException(
                String.format("Cannot fetch group [ %s ] no id was provided", group.getName()));
    }

    private GroupData getExistingWithOwnershipCheck(long groupId) {
        GroupData existing = this.getExisting(groupId);
        if (existing.hasOwner() && !isCurrentUserOwner(getCurrentUser(), existing.getOwner())) {
            throw AccessDeniedRuntimeException.notGroupOwner(existing.getId());
        }
        return existing;
    }

    private GroupData getExisting(long groupId) {
        return service.findById(groupId).filter(this::isAccessibleForCurrentUser)
                .orElseThrow(() -> NotFoundRuntimeException.missingGroup(groupId));
    }

    private boolean isAccessibleForCurrentUser(GroupData group) {
        UserDetails currentUser = getCurrentUser();
        switch (group.getVisibility()) {
        case PUBLIC:
            return true;
        case PROTECTED:
            log.debug("Checking if user [ {} ] is owner of group with id [ {} ]", currentUser.getUsername(),
                    group.getId());
            return isCurrentUserOwner(currentUser, group.getOwner());
        default:
            return false;
        }
    }

    private boolean isCurrentUserOwner(UserDetails currentUser, String owner) {
        return currentUser.getUsername().equals(owner);
    }

    private UserDetails getCurrentUser() {
        return AuthenticationFacade.getCurrentUser();
    }

}
