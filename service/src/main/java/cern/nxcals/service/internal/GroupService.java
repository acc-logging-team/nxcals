package cern.nxcals.service.internal;

import cern.nxcals.api.domain.Group;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.extraction.metadata.queries.Groups;
import cern.nxcals.service.domain.EntityData;
import cern.nxcals.service.domain.GroupData;
import cern.nxcals.service.domain.StandardPersistentEntity;
import cern.nxcals.service.domain.SystemSpecData;
import cern.nxcals.service.domain.VariableData;
import cern.nxcals.service.domain.VisibilityData;
import cern.nxcals.service.repository.GroupRepository;
import cern.nxcals.service.repository.SystemSpecRepository;
import cern.nxcals.service.rest.exceptions.ConfigDataConflictException;
import cern.nxcals.service.rest.exceptions.NotFoundRuntimeException;
import cern.nxcals.service.security.AuthenticationFacade;
import cern.nxcals.service.security.UserPrincipalService;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static java.util.stream.Collectors.toMap;

@Service
@Slf4j
@AllArgsConstructor
public class GroupService {
    @NonNull
    @PersistenceContext
    private final EntityManager em;

    @NonNull
    private final GroupRepository repository;

    @NonNull
    private final UserPrincipalService userService;

    @NonNull
    private final SystemSpecRepository systemRepository;

    @NonNull
    private final VariableService variableService;

    @NonNull
    private final EntityService entityService;

    @Transactional(transactionManager = "jpaTransactionManager")
    public GroupData create(@NonNull Group group) {
        if (group.hasId()) {
            throw ConfigDataConflictException.conflictOnGroup(group);
        }
        GroupData result = new GroupData();
        modifyWith(result, group);
        setOwnerForNewGroup(result, group);
        return save(result);
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public GroupData update(@NonNull Group group) {
        GroupData existingGroup = obtainFrom(group, GroupData.PLAIN);
        em.detach(existingGroup);
        verifyLabel(existingGroup.getName(), existingGroup.getLabel(), group.getLabel());

        modifyWith(existingGroup, group);
        modifyOwnerIfSpecified(existingGroup, group);
        return save(existingGroup);
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public void delete(long groupId) {
        GroupData existingGroup = obtainFrom(groupId, GroupData.MINIMUM);
        repository.delete(existingGroup);
    }

    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public Optional<GroupData> findById(long id) {
        return repository.findById(id);
    }

    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public List<GroupData> findAll(@NonNull String search) {
        return repository.queryAll(search, GroupData.PLAIN, GroupData.class);
    }

    private void modifyWith(GroupData result, Group groupToCopyFrom) {
        result.setLabel(groupToCopyFrom.getLabel());
        result.setName(groupToCopyFrom.getName());
        result.setSystem(obtainFrom(groupToCopyFrom.getSystemSpec()));
        result.setDescription(groupToCopyFrom.getDescription());
        result.setVisibility(VisibilityData.of(groupToCopyFrom.getVisibility()));
        result.setProperties(groupToCopyFrom.getProperties());
        result.setRecVersion(groupToCopyFrom.getRecVersion());
    }

    private void setOwnerForNewGroup(GroupData groupDataToModify, Group groupToCopyFrom) {
        if (groupToCopyFrom.getOwner() != null) {
            groupDataToModify.setOwner(groupToCopyFrom.getOwner());
        } else {
            takeOwnership(groupDataToModify);
        }
    }

    private void modifyOwnerIfSpecified(GroupData groupDataToModify, Group groupToCopyFrom) {
        if (groupToCopyFrom.getOwner() != null) {
            groupDataToModify.setOwner(groupToCopyFrom.getOwner());
        }
    }

    private void takeOwnership(GroupData groupDataToModify) {
        UserDetails currentUserDetails = AuthenticationFacade.getCurrentUser();
        groupDataToModify.setOwner(currentUserDetails.getUsername());
    }

    private <T extends StandardPersistentEntity> Map<String, Set<T>> mapToFetched(
            Map<String, Set<Long>> idsByAssociation,
            Function<Set<Long>, Set<T>> mappingFunction) {
        Set<Long> ids = idsByAssociation.values().stream().flatMap(Collection::stream)
                .collect(Collectors.toSet());
        Map<Long, T> objectsById = mappingFunction.apply(ids).stream()
                .collect(toMap(StandardPersistentEntity::getId, Function.identity()));
        return idsByAssociation.entrySet().stream()
                .collect(toMap(Map.Entry::getKey, var -> replaceWithObjects(var.getValue(), objectsById)));
    }

    private <T> Set<T> replaceWithObjects(Set<Long> ids, Map<Long, T> objectsById) {
        return ids.stream().map(objectsById::get).collect(Collectors.toSet());
    }

    private GroupData obtainFrom(Group group, String graph) {
        if (!group.hasId()) {
            throw NotFoundRuntimeException.missingGroup(group);
        }
        return obtainFrom(group.getId(), graph);
    }

    private GroupData obtainFrom(long groupId, String graph) {
        return repository.queryOne(toRSQL(Groups.suchThat().id().eq(groupId)), graph, GroupData.class)
                .orElseThrow(() -> NotFoundRuntimeException.missingGroup(groupId));
    }

    private SystemSpecData obtainFrom(SystemSpec systemSpec) {
        return systemRepository.findById(systemSpec.getId())
                .orElseThrow(() -> NotFoundRuntimeException.missingSystem(systemSpec));
    }

    private void verifyLabel(String name, String existingLabel, String newLabel) {
        if (!Objects.equals(existingLabel, newLabel)) {
            throw new IllegalStateException(
                    "Unable to change label of group [" + name + "] from " + existingLabel + " to " + newLabel);
        }
    }

    private GroupData save(GroupData entity) {
        try {
            //TODO: remove as per NXCALS-3291
            GroupData data = repository.save(entity);
            // here we flush to induce PersistenceException early and wrap it in ConfigDataConflictException
            em.flush();
            return data;
        } catch (PersistenceException e) {
            throw new ConfigDataConflictException(e);
        }
    }

    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public Map<String, Set<VariableData>> getVariables(long groupId) {
        return obtainFrom(groupId, GroupData.WITH_VARIABLES).getAssociatedVariables();
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public void setVariables(long groupId, @NonNull Map<String, Set<Long>> variableIdsPerAssociation) {
        GroupData group = obtainFrom(groupId, GroupData.WITH_VARIABLES);
        Map<String, Set<VariableData>> variablesPerAssociation = this.mapToFetched(variableIdsPerAssociation,
                variableService::findExactlyByIdIn);
        em.detach(group);
        group.setAssociatedVariables(variablesPerAssociation);
        save(group);
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public void addVariables(long groupId, @NonNull Map<String, Set<Long>> variableIdsPerAssociation) {
        GroupData group = obtainFrom(groupId, GroupData.WITH_VARIABLES);
        Map<String, Set<VariableData>> variablesPerAssociation = this.mapToFetched(variableIdsPerAssociation,
                variableService::findExactlyByIdIn);
        em.detach(group);
        group.addAssociatedVariables(variablesPerAssociation);
        save(group);
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public void removeVariables(long groupId, @NonNull Map<String, Set<Long>> variableIdsPerAssociation) {
        GroupData group = obtainFrom(groupId, GroupData.WITH_VARIABLES);
        Map<String, Set<VariableData>> variablesPerAssociation = this.mapToFetched(variableIdsPerAssociation,
                variableService::findExactlyByIdIn);
        em.detach(group);
        group.removeAssociatedVariables(variablesPerAssociation);
        save(group);
    }

    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public Map<String, Set<EntityData>> getEntities(long groupId) {
        return obtainFrom(groupId, GroupData.WITH_ENTITIES).getAssociatedEntities();
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public void setEntities(long groupId, @NonNull Map<String, Set<Long>> entityIdsPerAssociation) {
        GroupData group = obtainFrom(groupId, GroupData.WITH_ENTITIES);
        Map<String, Set<EntityData>> entitiesPerAssociation = this.mapToFetched(entityIdsPerAssociation,
                entityService::findExactlyByIdIn);
        em.detach(group);
        group.setAssociatedEntities(entitiesPerAssociation);
        save(group);
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public void addEntities(long groupId, @NonNull Map<String, Set<Long>> entityIdsPerAssociation) {
        GroupData group = obtainFrom(groupId, GroupData.WITH_ENTITIES);
        Map<String, Set<EntityData>> entitiesPerAssociation = this.mapToFetched(entityIdsPerAssociation,
                entityService::findExactlyByIdIn);
        em.detach(group);
        group.addAssociatedEntities(entitiesPerAssociation);
        save(group);
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public void removeEntities(long groupId, @NonNull Map<String, Set<Long>> entityIdsPerAssociation) {
        GroupData group = obtainFrom(groupId, GroupData.WITH_ENTITIES);
        Map<String, Set<EntityData>> entitiesPerAssociation = this.mapToFetched(entityIdsPerAssociation,
                entityService::findExactlyByIdIn);
        em.detach(group);
        group.removeAssociatedEntities(entitiesPerAssociation);
        save(group);
    }

}
