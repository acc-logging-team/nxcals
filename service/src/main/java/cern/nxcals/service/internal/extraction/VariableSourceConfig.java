package cern.nxcals.service.internal.extraction;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableConfig;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;

@Data
@AllArgsConstructor
public class VariableSourceConfig {
    @NonNull
    private final Entity entity;
    @NonNull
    private final VariableConfig variableConfig;
    @NonNull
    private final Variable variable;

    public TimeWindow configValidity() {
        TimeWindow validity = variableConfig.getValidity();
        // validity end time, if set, is exclusive
        return validity.getEndTime() != null ?
                validity.rightLimit(validity.getEndTime().minusNanos(1)) :
                validity;
    }
}
