package cern.nxcals.service.internal.metadata;

import cern.nxcals.api.metadata.queries.QueryOptions;
import cern.nxcals.common.utils.UnaryOperatorUtils;
import lombok.experimental.UtilityClass;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.UnaryOperator;

@UtilityClass
public class QueryOptionsProcessors {
    public static <S> BiFunction<CriteriaBuilder, CriteriaQuery<S>, CriteriaQuery<S>> createQueryProcessor(
            QueryOptions<?> queryOptions, Class<S> clazz) {
        BiFunction<CriteriaBuilder, CriteriaQuery<S>, CriteriaQuery<S>> processor = (builder, query) -> query;
        if (queryOptions.shouldOrder()) {
            processor = composeByQuery(processor, createOrderByApplier(queryOptions, clazz));
        }
        return processor;
    }

    private static <S> BiFunction<CriteriaBuilder, CriteriaQuery<S>, CriteriaQuery<S>> createOrderByApplier(
            QueryOptions<?> queryOptions, Class<S> clazz) {
        return (builder, query) -> {
            Root<?> from = findRootInQuery(query, clazz);
            Expression<?> field = from.get(queryOptions.getOrderByField());
            Order order = queryOptions.isAscending() ? builder.asc(field) : builder.desc(field);
            return query.orderBy(order);
        };
    }

    private static <S> Root<?> findRootInQuery(CriteriaQuery<S> query, Class<S> clazz) {
        Set<Root<?>> roots = query.getRoots();
        Optional<Root<?>> maybeCorrectRoot = roots.stream().filter(root -> root.getModel().getJavaType() == clazz)
                .findFirst();
        return maybeCorrectRoot.orElseThrow(() -> new Error("Query has no root, it is incorrectly initialized"));

    }

    private static <S> BiFunction<CriteriaBuilder, CriteriaQuery<S>, CriteriaQuery<S>> composeByQuery(
            BiFunction<CriteriaBuilder, CriteriaQuery<S>, CriteriaQuery<S>> first,
            BiFunction<CriteriaBuilder, CriteriaQuery<S>, CriteriaQuery<S>> second) {
        return (builder, query) -> {
            UnaryOperator<CriteriaQuery<S>> firstOperator = x -> first.apply(builder, x);
            UnaryOperator<CriteriaQuery<S>> secondOperator = x -> second.apply(builder, x);
            return UnaryOperatorUtils.compose(firstOperator, secondOperator).apply(query);
        };
    }

    public static <T> UnaryOperator<TypedQuery<T>> createResultProcessor(
            QueryOptions<?> queryOptions) {

        UnaryOperator<TypedQuery<T>> mapper = UnaryOperator.identity();
        mapper = UnaryOperatorUtils.compose(mapper, getApplyLimit(queryOptions));
        mapper = UnaryOperatorUtils.compose(mapper, getAddOffset(queryOptions));

        return mapper;
    }

    private static <T> UnaryOperator<TypedQuery<T>> getApplyLimit(QueryOptions<?> queryOptions) {
        if (queryOptions.shouldLimit()) {
            return x -> x.setMaxResults(queryOptions.getLimit());
        } else {
            return UnaryOperator.identity();
        }
    }

    private static <T> UnaryOperator<TypedQuery<T>> getAddOffset(QueryOptions<?> queryOptions) {
        if (queryOptions.shouldOffset()) {
            return x -> x.setFirstResult(queryOptions.getOffset());
        } else {
            return UnaryOperator.identity();
        }
    }
}
