package cern.nxcals.service.internal.compaction;

import cern.nxcals.api.domain.TimeEntityPartitionType;
import cern.nxcals.common.domain.AdaptiveMergeCompactionJob;
import cern.nxcals.common.domain.DataProcessingJob;
import cern.nxcals.common.paths.StagingPath;
import cern.nxcals.service.internal.PartitionResourceHistoryService;
import cern.nxcals.service.internal.PartitionResourceService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.util.Collection;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static cern.nxcals.common.domain.DataProcessingJob.JobType.ADAPTIVE_MERGE_COMPACT;

@Slf4j
public class AdaptiveMergeCompactionJobCreator extends AbstractMergeCompactionJobCreator<AdaptiveMergeCompactionJob> {
    @NonNull
    private final PartitionResourceService partitionResourceService;
    @NonNull
    private final PartitionResourceHistoryService partitionResourceHistoryService;

    @SuppressWarnings("squid:S00107")
    public AdaptiveMergeCompactionJobCreator(long sortAbove, long maxPartitionSize, int avroParquetSizeRatio,
            @NonNull String outputPrefix, @NonNull String stagingPrefix, @NonNull FileSystem fs,
            @NonNull PartitionResourceService partitionResourceService,
            @NonNull PartitionResourceHistoryService partitionResourceHistoryService,
            @NonNull Predicate<StagingPath> adaptivePathChecker) {
        super(sortAbove, maxPartitionSize, avroParquetSizeRatio, outputPrefix, stagingPrefix, fs, adaptivePathChecker);
        this.partitionResourceService = partitionResourceService;
        this.partitionResourceHistoryService = partitionResourceHistoryService;
    }

    @Override
    protected AdaptiveMergeCompactionJob createJob(StagingPath group, Collection<FileStatus> files, long size,
            long totalSize,
            String filePrefix) {
        long parquetRowGroupSize = AdaptiveCompactionJobCreator.calculateParquetRowGroupSize(totalSize);
        long hdfsBlockSize = AdaptiveCompactionJobCreator.calculateHdfsBlockSize(parquetRowGroupSize);
        TimeEntityPartitionType partitionType = AdaptiveCompactionJobCreator.getPartitionType(partitionResourceService,
                partitionResourceHistoryService, group, totalSize);

        return AdaptiveMergeCompactionJob.builder()
                .filePrefix("")
                .systemId(group.getSystemId())
                .sourceDir(group.toPath().toUri())
                .destinationDir(getHdfsPathCreator().toOutputUri(group))
                .totalSize(totalSize)
                .jobSize(size)
                .sortEnabled(size > getSortAbove())
                .files(files.stream().map(FileStatus::getPath).map(Path::toUri).collect(Collectors.toList()))
                .restagingReportFile(getHdfsPathCreator().toRestageReportFileUri(group))
                .parquetRowGroupSize(parquetRowGroupSize)
                .hdfsBlockSize(hdfsBlockSize)
                .partitionType(partitionType)
                .build();
    }

    @Override
    public DataProcessingJob.JobType getJobType() {
        return ADAPTIVE_MERGE_COMPACT;
    }
}
