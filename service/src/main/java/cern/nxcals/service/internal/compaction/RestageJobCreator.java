package cern.nxcals.service.internal.compaction;

import cern.nxcals.common.domain.DataProcessingJob.JobType;
import cern.nxcals.common.domain.RestageJob;
import cern.nxcals.common.paths.StagingPath;
import cern.nxcals.common.utils.HdfsFileUtils;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static cern.nxcals.common.domain.DataProcessingJob.JobType.RESTAGE;
import static cern.nxcals.common.utils.HdfsFileUtils.SearchType.NONRECURSIVE;

@Slf4j
public class RestageJobCreator extends DataProcessingJobCreator<RestageJob> {
    public static final long SINGLE_JOB_PARTITION_SIZE = Long.MAX_VALUE;


    public RestageJobCreator(long sortAbove, int avroParquetSizeRatio,
            @NonNull String outputPrefix, @NonNull String stagingPrefix, @NonNull FileSystem fs) {
        super(sortAbove, SINGLE_JOB_PARTITION_SIZE, avroParquetSizeRatio, outputPrefix, stagingPrefix, fs);
    }

    @Override
    protected RestageJob createJob(StagingPath group, Collection<FileStatus> files, long size, long totalSize,
                                   String filePrefix) {
        URI dataDir = getHdfsPathCreator().toOutputUri(group);
        return RestageJob.builder()
                .systemId(group.getSystemId())
                .sourceDir(group.toPath().toUri())
                .destinationDir(dataDir)
                .totalSize(totalSize)
                .jobSize(size)
                .dataFiles(getDataFiles(dataDir))
                .files(files.stream().map(FileStatus::getPath).map(Path::toUri).collect(Collectors.toList()))
                .filePrefix(filePrefix)
                .restagingReportFile(getHdfsPathCreator().toRestageReportFileUri(group))
                .build();
    }

    @Override
    protected Predicate<StagingPath> getPathChecker() {
        return this::verifyJobPath;
    }

    protected boolean verifyJobPath(StagingPath path) {
        Path restageDataFileReportPath = new Path(getHdfsPathCreator().toRestageReportFileUri(path));
        try {
            return !getFs().exists(restageDataFileReportPath);
        } catch (IOException e) {
            log.error("Got exception while checking if provided path exists for {}. Operation would be omitted",
                    getJobType(), e);
            return false;
        }
    }

    @Override
    public JobType getJobType() {
        return RESTAGE;
    }

    private List<URI> getDataFiles(URI dataDir) {
        return HdfsFileUtils.findFilesInPath(getFs(), new Path(dataDir), NONRECURSIVE,
                HdfsFileUtils::isParquetDataFile)
                .stream().map(sf -> sf.getPath().toUri()).collect(Collectors.toList());
    }

}
