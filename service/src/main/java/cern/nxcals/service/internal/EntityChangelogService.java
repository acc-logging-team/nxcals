/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.internal;

import cern.nxcals.service.domain.EntityChangelogData;
import cern.nxcals.service.repository.EntityChangelogRepository;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * The implementation of the {@link EntityChangelogService} interface.
 */
@Service
@Slf4j
@AllArgsConstructor
public class EntityChangelogService {

    @NonNull
    private final EntityChangelogRepository entityChangelogRepository;

    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public List<EntityChangelogData> findAll(String search) {
        return this.entityChangelogRepository.queryAll(search, EntityChangelogData.class);
    }

}
