package cern.nxcals.service.internal.compaction;

import cern.nxcals.api.domain.PartitionResourceHistory;
import cern.nxcals.api.domain.TimeEntityPartitionType;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.common.SystemFields;
import cern.nxcals.common.domain.AdaptiveCompactionJob;
import cern.nxcals.common.domain.DataProcessingJob;
import cern.nxcals.common.paths.StagingPath;
import cern.nxcals.common.utils.HdfsFileUtils;
import cern.nxcals.service.domain.PartitionData;
import cern.nxcals.service.domain.PartitionPropertiesData;
import cern.nxcals.service.domain.PartitionResourceData;
import cern.nxcals.service.domain.PartitionResourceHistoryData;
import cern.nxcals.service.internal.PartitionResourceHistoryService;
import cern.nxcals.service.internal.PartitionResourceService;
import cern.nxcals.service.internal.PartitionService;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.UncheckedIOException;
import java.net.URI;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * A job creator for the adaptive compaction job where we partition by timestamp (1) and bucket by entity_id (2) depending on the size of the data.
 * It creates jobs that are matched by output (data) time partitions. In other words it will take all staging time partitions that belong to some data partitions
 * and creates a job out of it. Output partition number should be divisible by staging time partitions so this is always possible.
 */
@Slf4j
public class AdaptiveCompactionJobCreator extends DataProcessingJobCreator<AdaptiveCompactionJob> {
    @VisibleForTesting
    static final long ONE_GB_FILE_SIZE = 1024 * 1024 * 1024L;
    private static final long ROW_GROUP_MIN_SIZE = 16 * 1024 * 1024L;
    private static final long ROW_GROUP_MAX_SIZE = 512 * 1024 * 1024L;
    private static final long DEFAULT_HDFS_BLOCK_SIZE = 256 * 1024 * 1024L;
    private static final long EXTENDED_HDFS_BLOCK_SIZE = 512 * 1024 * 1024L;
    private static final int ROW_GROUP_COUNT = 200;
    private static final Pattern NUMBER_PATTERN = Pattern.compile("\\d+");
    @NonNull
    private final PartitionResourceService partitionResourceService;
    @NonNull
    private final PartitionResourceHistoryService partitionResourceHistoryService;
    @NonNull
    private final PartitionService partitionService;

    private final long maxNumberOfStagingPartitions;

    @SuppressWarnings("squid:S00107")
    public AdaptiveCompactionJobCreator(
            @NonNull PartitionService partitionService,
            @NonNull PartitionResourceService partitionResourceService,
            @NonNull PartitionResourceHistoryService partitionResourceHistoryService,
            long maxNumberOfStagingPartitions,
            long sortThreshold,
            long maxPartitionSize,
            int avroParquetSizeRatio,
            @NonNull String dataPrefix,
            @NonNull String stagingPrefix,
            @NonNull FileSystem fs) {

        super(sortThreshold, maxPartitionSize, avroParquetSizeRatio, dataPrefix, stagingPrefix, fs);
        this.partitionService = partitionService;
        this.partitionResourceService = partitionResourceService;
        this.partitionResourceHistoryService = partitionResourceHistoryService;
        this.maxNumberOfStagingPartitions = maxNumberOfStagingPartitions;

    }

    private boolean isPathValidForActiveCompaction(StagingPath path) {
        Path outputPath = new Path(this.getHdfsPathCreator().toOutputUri(path));
        try {
            //Either there is no files in the target dir or the resource info already exists (this is the late data case
            //that we process according to existing info).
            return !HdfsFileUtils.hasDataFiles(getFs(), outputPath) || resourceInfoExists(path);
        } catch (UncheckedIOException ex) {
            log.error("Got exception while checking if provided path exists for {}. Operation would be omitted",
                    getJobType(), ex);
            return false;
        }
    }


    //Checks if resource & resourceInfo exist for this path.
    private boolean resourceInfoExists(StagingPath path) {
        return partitionResourceService.findBySystemIdAndPartitionIdAndSchemaId(
                path.getSystemId(),
                path.getPartitionId(),
                path.getSchemaId())
                .filter(partitionResourceData -> partitionResourceHistoryService
                        .findByPartitionResourceAndTimestamp(partitionResourceData, path.getDate().toInstant())
                        .isPresent())
                .isPresent();
    }

    /**
     * This verification must check if there is data in the 'data' directory for this partition/day.
     * If there is data on pro, and the adaptive compaction partitioning is not assigned it means we cannot process this
     * directory with this algorithm (old one has to be used).
     *
     * @return predicate for a path
     */
    @Override
    public Predicate<StagingPath> getPathChecker() {
        return this::isPathValidForActiveCompaction;
    }

    @VisibleForTesting
    static TimeEntityPartitionType selectPartitioningType(long totalDirSize) {
        double targetNbOf1GBFiles = totalDirSize / (double) ONE_GB_FILE_SIZE;
        return TimeEntityPartitionType.ofGbFiles(Math.round(targetNbOf1GBFiles));
    }

    /**
     * We have to override this method from the base class to implement together job creation and estimation of the
     * output partition type (that has to kick in before the job creation).
     *
     * @param stagingPath
     * @param files
     * @return a job Optional (for the moment it returns only one job for a given day, TBD).
     */
    @Override
    public Optional<AdaptiveCompactionJob> create(@NonNull StagingPath stagingPath, Collection<FileStatus> files) {
        Preconditions.checkArgument(CollectionUtils.isNotEmpty(files), "No files for compaction job");
        if (!getPathChecker().test(stagingPath)) {
            log.warn("Requested job staging path of job type [{}] is not valid for {}, will return empty job "
                    + "collection", getJobType(), stagingPath);
            return Optional.empty();
        }
        long totalSize = sizeOf(files);
        TimeEntityPartitionType partitionType = getPartitionType(stagingPath, totalSize);
        int nbOfOutputPartitions = partitionType.getTimePartitionCount();

        Map<Long, List<FileStatus>> filesByStagingPartition
                = files.stream()
                .collect(Collectors.groupingBy(this::toTimePartition));

        return bucketPerOutputPartition(stagingPath.getPartitionId(), stagingPath.getDate().toInstant(),
                filesByStagingPartition, nbOfOutputPartitions)
                .values()
                .stream()
                .map(fileStatuses -> {
                    long jobSize = sizeOf(fileStatuses);
                    return createJob(stagingPath, fileStatuses, jobSize, totalSize, partitionType);
                }).findFirst(); //to be checked if we can return more jobs for a given day, perhaps it is better not to
    }

    private Map<Long, List<FileStatus>> bucketPerOutputPartition(long partitionId, Instant stagingDirDate,
            Map<Long, List<FileStatus>> filesByStagingPartition,
            int maxOutputPartitions) {
        PartitionData partitionData = partitionService.findById(partitionId)
                .orElseThrow(() -> new IllegalArgumentException("No partition found for id " + partitionId));
        long maxNumberOfStagingPartitionsFound = findMaxNumberOfStagingPartitions(partitionData, stagingDirDate,
                this.maxNumberOfStagingPartitions);

        Map<Long, List<FileStatus>> outputMap = new HashMap<>();
        filesByStagingPartition.forEach((stagingTimePartition, files) -> {
            if (stagingTimePartition != Long.MIN_VALUE) { //normal bucketed data, no parquet inside
                processAvroFiles(stagingTimePartition, maxNumberOfStagingPartitionsFound, maxOutputPartitions, files,
                        outputMap);
            } else {
                processParquetFiles(files, outputMap);
            }
        });
        return outputMap;
    }

    /*
    It takes the default value (usually 1h split) or takes it from Partition definition if fields for staging partition nb
    are set and the time of the staging dir is within the range of from/to if they are defined (while "to" is not always defined -> open range).
     */
    @VisibleForTesting
    static long findMaxNumberOfStagingPartitions(PartitionData partitionData, Instant stagingDirDate,
            long defaultMaxNumberOfStatingPartitions) {
        PartitionPropertiesData properties = partitionData.getProperties();
        String stagingPartitionTypeStr = properties.getStagingPartitionType();
        if (stagingPartitionTypeStr != null && stagingPartitionDateInRange(properties, stagingDirDate)) {
            TimeUtils.TimeSplits stagingPartitionType = TimeUtils.TimeSplits.valueOf(stagingPartitionTypeStr);
            return stagingPartitionType.getSize();
        } else {
            //default, usually 1h splits
            return defaultMaxNumberOfStatingPartitions;
        }
    }

    private static boolean stagingPartitionDateInRange(PartitionPropertiesData properties, Instant stagingDirDate) {
        return TimeWindow.between(properties.getStagingPartitionTypeFromStamp(),
                        properties.getStagingPartitionTypeToStamp())
                .isWithinRangeRightExcluded(TimeUtils.getNanosFromInstant(stagingDirDate));
    }

    private void processAvroFiles(Long stagingPartition, long maxStagingTimePartitions,
            int maxOutputPartitions, List<FileStatus> files,
            Map<Long, List<FileStatus>> outputMap) {
        long outputPartition = calculateOutputPartition(stagingPartition, maxStagingTimePartitions,
                maxOutputPartitions);
        List<FileStatus> fileStatuses = outputMap.computeIfAbsent(outputPartition, part -> new ArrayList<>());
        fileStatuses.addAll(files);
    }

    /**
     * Calculates the output (data) partition based on max staging and max output. If the max output is smaller than
     * staging the output partition will equal the input. This is ok as compactor will create N output partition from
     * that one staging but the minimal job is one staging partition (that cannot be split further due to deduplication
     * that must be performed).
     *
     * @param stagingPartition
     * @param maxStagingPartitions
     * @param maxOutputPartitions
     * @return
     */
    @VisibleForTesting
    static long calculateOutputPartition(long stagingPartition, long maxStagingPartitions, int maxOutputPartitions) {
        long intervalSize = Math.max(maxStagingPartitions / maxOutputPartitions, 1L);
        return stagingPartition / intervalSize;
    }

    private static void processParquetFiles(List<FileStatus> files, Map<Long, List<FileStatus>> outputMap) {
        //this is some parquet leftovers from some failed compaction job
        //need to check the time partition on the file to assign it to the correct output bucket.
        files.forEach(file -> {
            if (HdfsFileUtils.isParquetDataFile(file)) {
                Optional<Long> timePartition = parseFilenameForTimePartition(file.getPath().getName());
                if (timePartition.isPresent()) {
                    List<FileStatus> fileStatuses = outputMap.computeIfAbsent(timePartition.get(),
                            part -> new ArrayList<>());
                    fileStatuses.add(file);
                } else {
                    log.error("Unexpected parquet file name format, no time partition found in {}", file.getPath());
                }
            } else {
                log.error("Unexpected file {} while creating compaction jobs", file.getPath());
            }
        });
    }

    /*
    __sys_nxcals_time_partition__=43-__sys_nxcals_entity_bucket__=7-part-00004-3c4ae51c-3035-490a-bc39-17f302a45ee4.c000.snappy.parquet
     */
    @VisibleForTesting
    static Optional<Long> parseFilenameForTimePartition(String name) {
        if (name.startsWith(SystemFields.NXC_TIME_PARTITION_COLUMN.getValue())) {
            Matcher matcher = NUMBER_PATTERN.matcher(name);
            if (matcher.find()) {
                return Optional.of(Long.parseLong(matcher.group()));
            }
        }
        return Optional.empty();
    }

    @VisibleForTesting
    AdaptiveCompactionJob createJob(StagingPath stagingPath, Collection<FileStatus> files, long jobSize,
            long totalSize,
            TimeEntityPartitionType partitionType) {
        List<URI> uris = files.stream().map(FileStatus::getPath).map(Path::toUri).collect(Collectors.toList());
        if (log.isInfoEnabled()) {
            log.info(
                    "Creating AdaptiveCompactionJob for staging path={}, files-count={}, jobSize={}, total day size={}",
                    stagingPath, uris.size(), jobSize, totalSize);
        }

        long parquetRowGroupSize = calculateParquetRowGroupSize(totalSize);
        long hdfsBlockSize = calculateHdfsBlockSize(parquetRowGroupSize);

        return AdaptiveCompactionJob.builder()
                .filePrefix(
                        "") // file prefix is not fixed, it will be given by the compactor according to partition/bucket.
                .systemId(stagingPath.getSystemId())
                .partitionId(stagingPath.getPartitionId())
                .schemaId(stagingPath.getSchemaId())
                .sourceDir(stagingPath.toPath().toUri())
                .destinationDir(getHdfsPathCreator().toOutputUri(stagingPath))
                .totalSize(totalSize)
                .jobSize(jobSize)
                .partitionType(partitionType)
                .sortEnabled(jobSize > getSortAbove())
                .files(uris)
                .parquetRowGroupSize(parquetRowGroupSize)
                .hdfsBlockSize(hdfsBlockSize)
                .build();
    }

    static long calculateParquetRowGroupSize(long totalSize) {
        //aiming at ~200 row groups as default , with min 16MB, max 256MB (HDFS block size),
        //using multiples of 16MB to normalize.
        long parquetRowGroupSize = (((totalSize / ROW_GROUP_COUNT) / ROW_GROUP_MIN_SIZE) + 1) * ROW_GROUP_MIN_SIZE;
        return Math.min(parquetRowGroupSize, ROW_GROUP_MAX_SIZE); //upper bound is needed.
    }

    static long calculateHdfsBlockSize(long parquetRowGroupSize) {
        return parquetRowGroupSize > DEFAULT_HDFS_BLOCK_SIZE ? EXTENDED_HDFS_BLOCK_SIZE : DEFAULT_HDFS_BLOCK_SIZE;
    }

    private TimeEntityPartitionType getPartitionType(StagingPath stagingPath, long totalSize) {
        return getPartitionType(this.partitionResourceService,
                this.partitionResourceHistoryService, stagingPath, totalSize);
    }

    static TimeEntityPartitionType getPartitionType(PartitionResourceService prService,
            PartitionResourceHistoryService prhService,
            StagingPath stagingPath, long totalSize) {
        long systemId = stagingPath.getSystemId();
        long partitionId = stagingPath.getPartitionId();
        long schemaId = stagingPath.getSchemaId();
        ZonedDateTime date = stagingPath.getDate();

        PartitionResourceData prData = prService
                .findOrCreatePartitionResourceData(systemId, partitionId, schemaId);
        TimeEntityPartitionType type = selectPartitioningType(totalSize);

        PartitionResourceHistory newPrh = prhService
                .createPartitionResourceHistory(prData, date.toInstant(), type);

        PartitionResourceHistoryData priData = prhService
                .findOrCreatePartitionResourceInfoFor(prData, newPrh);

        return TimeEntityPartitionType.from(priData.getInformation());
    }

    @Override
    public DataProcessingJob.JobType getJobType() {
        return DataProcessingJob.JobType.ADAPTIVE_COMPACT;
    }
}
