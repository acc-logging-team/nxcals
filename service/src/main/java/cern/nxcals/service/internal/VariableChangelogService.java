/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.internal;

import cern.nxcals.service.domain.VariableChangelogData;
import cern.nxcals.service.repository.VariableChangelogRepository;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * The implementation of the {@link VariableChangelogService} interface.
 */
@Service
@Slf4j
@AllArgsConstructor
public class VariableChangelogService {

    @NonNull
    private final VariableChangelogRepository variableChangelogRepository;

    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public List<VariableChangelogData> findAll(String search) {
        return this.variableChangelogRepository.queryAll(search, VariableChangelogData.class);
    }

}
