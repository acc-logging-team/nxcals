/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.internal;

import cern.nxcals.api.domain.CreateEntityRequest;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.Identifiable;
import cern.nxcals.api.extraction.metadata.queries.Entities;
import cern.nxcals.api.extraction.metadata.queries.EntitySchemas;
import cern.nxcals.api.extraction.metadata.queries.Partitions;
import cern.nxcals.api.metadata.queries.EntityQueryWithOptions;
import cern.nxcals.api.metadata.queries.QueryOptions;
import cern.nxcals.common.concurrent.AutoCloseableLock;
import cern.nxcals.common.metrics.MetricsRegistry;
import cern.nxcals.service.domain.EntityData;
import cern.nxcals.service.domain.EntityHistoryData;
import cern.nxcals.service.domain.EntitySchemaData;
import cern.nxcals.service.domain.PartitionData;
import cern.nxcals.service.domain.SystemSpecData;
import cern.nxcals.service.internal.component.BulkUtils;
import cern.nxcals.service.repository.EntityHistoryRepository;
import cern.nxcals.service.repository.EntityRepository;
import cern.nxcals.service.repository.PartitionRepository;
import cern.nxcals.service.repository.SchemaRepository;
import cern.nxcals.service.repository.SystemSpecRepository;
import cern.nxcals.service.rest.exceptions.ConfigDataConflictException;
import cern.nxcals.service.rest.exceptions.NotFoundRuntimeException;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import java.time.Duration;
import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.stream.Collectors;

import static cern.nxcals.api.utils.TimeUtils.getInstantFromNanos;
import static cern.nxcals.common.utils.KeyValuesUtils.convertMapIntoAvroSchemaString;
import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static cern.nxcals.service.internal.metadata.QueryOptionsProcessors.createQueryProcessor;
import static cern.nxcals.service.internal.metadata.QueryOptionsProcessors.createResultProcessor;
import static java.util.Collections.emptySet;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;
import static org.apache.commons.collections.CollectionUtils.isEmpty;

/**
 * The implementation of the {@link cern.nxcals.service.internal.EntityService} interface.
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class EntityService extends BaseService {
    private static final String SYSTEM_NOT_FOUND_ERROR_FORMAT = "No system found for id %s";
    @NonNull
    @PersistenceContext
    private final EntityManager entityManager;
    @NonNull
    private final EntityRepository entityRepository;
    @NonNull
    private final PartitionRepository partitionRepository;
    @NonNull
    private final SystemSpecRepository systemRepository;
    @NonNull
    private final SchemaRepository schemaRepository;
    @NonNull
    private final EntityHistoryRepository entityHistoryRepository;

    @NonNull
    private final MetricsRegistry metricsRegistry;


    @Value("${entity.hist.read.max.batch.size:100}")
    private int historyReadMaxBatchSize;

    @Value("${entity.hist.count.lookup.time:1d}")
    private Duration historyCountLookupTime;

    @Value("${entity.hist.count.max.allowed:100}")
    private long historyCountMaxAllowed;

    @Value("${entity.hist.count.alert.threshold:5}")
    private long historyCountAlertThreshold;

    private static final String HISTORY_MAX_COUNT_ALERT_METRIC = "history_max_entries";

    @PostConstruct
    public void init() {
        metricsRegistry.incrementCounterBy(HISTORY_MAX_COUNT_ALERT_METRIC, 0);
    }

    /**
     * Finds entities for RSQL query with most recent history if such exists.
     *
     * @param search a {@link String} representation of the RSQL query
     * @return a {@link List} of {@link EntityData} instances that match the query
     */
    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public List<EntityData> findAll(String search) {
        return findAll(search, EntityQueryWithOptions.getDefaultInstance());
    }

    /**
     * Finds entities for RSQL query with predefined history from/to
     *
     * @param search    - {@link String} representation of the RSQL query
     * @param startTime - history start time
     * @param endTime   - history end time
     * @return a {@link List} of {@link EntityData} instances that match the query by the provided time window
     */
    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public List<EntityData> findAll(String search, long startTime, long endTime) {
        verifyBounds(startTime, endTime, ", query=" + search);
        return findAll(search, EntityQueryWithOptions.getDefaultInstance().withHistory(startTime, endTime));
    }

    /**
     * Finds entities for RSQL query, depending on passed query options
     */
    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public List<EntityData> findAll(String search, EntityQueryWithOptions queryOptions) {
        List<EntityData> entities = findAllEntities(search, queryOptions);
        switch (queryOptions.getQueryType()) {
        case WITH_LATEST_HISTORY:
            addLatestHistoryEntriesTo(entities);
            break;
        case WITH_HISTORY:
            Instant start = getInstantFromNanos(queryOptions.getHistoryStartTime());
            Instant end = getInstantFromNanos(queryOptions.getHistoryEndTime());
            addHistoryEntriesTo(entities, start, end);
            break;
        case WITHOUT_HISTORY:
            break;
        default:
        }
        return entities;
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public EntityData findOrCreateEntityFor(long systemId, Map<String, Object> entityKeyValues,
            Map<String, Object> partitionKeyValues, String schemaAsString, long recordTimestamp) {
        log.info("findOrCreateEntityFor system={}, entity={} partition={} schema={} recordTimestamp={}", systemId,
                entityKeyValues, partitionKeyValues, schemaAsString, recordTimestamp);
        SystemSpecData system = getSystemOrThrow(systemId);
        Pair<String, String> entityPartitionSchemas = convertToAvroSchemaStrings(system, entityKeyValues,
                partitionKeyValues);
        try (AutoCloseableLock ignored = this.getLockFor(system.getId() + entityPartitionSchemas.getLeft())) {
            // find or create entity for the given system & entity key values.
            EntityData entity = getEntity(system, entityPartitionSchemas.getLeft());
            PartitionData partition = getPartition(system, entityPartitionSchemas.getRight(), entity);
            EntitySchemaData schema = findOrCreateSchema(schemaAsString);
            return updateEntityHistoryFor(entity, partition, schema, recordTimestamp);
        }
    }

    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public Set<EntityData> findExactlyByIdIn(@NonNull Set<Long> entities) {
        Set<EntityData> found = entityRepository.findAllByIdIn(entities);
        return verifyEntities(entities, found);
    }

    private Set<EntityData> verifyEntities(Set<Long> entities, Set<EntityData> entityData) {
        if (entityData.size() < entities.size()) {
            Set<Long> existing = entityData.stream().map(EntityData::getId).collect(Collectors.toSet());
            throw NotFoundRuntimeException.missingEntities(Sets.difference(entities, existing));
        }
        return entityData;
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public EntityData extendEntityFirstHistoryDataFor(long entityId, String schemaAsString, long fromNanos) {
        //This "ignored" is not really ignored but sonar complains here so we need to stick to this name.
        //I didn't want to put SuppressWarnings since it would suppress them all in this method (jwozniak)
        try (AutoCloseableLock ignored = this.getLockFor(String.valueOf(entityId))) {
            EntityData entity = findEntityByIdOrThrow(entityId);
            EntityHistoryData firstHistory = getFirstEntityHistoryOf(entity);
            Instant historyValidFrom = firstHistory == null ? null : firstHistory.getValidFromStamp();
            EntityHistoryData extendedHistory;
            Instant extendedFrom = getInstantFromNanos(fromNanos);
            if (firstHistory == null || extendedFrom.isBefore(historyValidFrom)) {
                PartitionData partition = entity.getPartition();
                EntitySchemaData schema = findOrCreateSchema(schemaAsString);
                extendedHistory = createAndPersistEntityHistory(entity, partition, schema, extendedFrom,
                        historyValidFrom);
            } else if (firstHistory.isLatest() || extendedFrom.isBefore(firstHistory.getValidToStamp())) {
                String firstSchema = firstHistory.getSchema().getContent();
                if (!firstSchema.equals(schemaAsString)) {
                    throw new ConfigDataConflictException(String.format(
                            "Schema not matching for first history record=%s and given timestamp=%d for entity=%s",
                            firstHistory, fromNanos, entity));
                }
                extendedHistory = firstHistory;
            } else {
                throw new ConfigDataConflictException(
                        String.format("The timestamp=%d is not before or within the first history record for entity=%s",
                                fromNanos, entity));
            }
            entity.clearHistory();
            entity.addEntityHist(extendedHistory);
            return entity;
        }
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public List<EntityData> updateEntities(List<Entity> entityDataList) {
        if (entityDataList == null) {
            throw new IllegalArgumentException("Provided entity information list can not be null!");
        }

        Map<Long, EntityData> fetchedEntitiesMap = fetchEntityIdToEntityMapOrThrow(entityDataList);
        for (Entity entityData : entityDataList) {
            EntityData fetchedEntity = fetchedEntitiesMap.get(entityData.getId());

            // Needs to detach entity to allow explicit version data passing!
            // This can not be achieved differently, cause one can not modify
            // the version property (explicit version changes are not propagated to database).
            entityManager.detach(fetchedEntity);
            fetchedEntity.setRecVersion(entityData.getRecVersion());

            fetchedEntity.setKeyValues(convertMapIntoAvroSchemaString(entityData.getEntityKeyValues(),
                    fetchedEntity.getPartition().getSystem().getEntityKeyDefs()));

            Instant lockedUntilStamp = entityData.getLockedUntilStamp();
            fetchedEntity.setLockedUntilStamp(lockedUntilStamp);
        }
        return Lists.newArrayList(entityRepository.saveAll(fetchedEntitiesMap.values()));
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public EntityData createEntity(long systemId, Map<String, Object> entityKey, Map<String, Object> partitionKey) {
        SystemSpecData system = getSystemOrThrow(systemId);
        Pair<String, String> entityPartitionSchemas = convertToAvroSchemaStrings(system, entityKey, partitionKey);
        EntityData entity = createEntityData(entityPartitionSchemas.getLeft());
        PartitionData partition = getPartition(system, entityPartitionSchemas.getRight(), entity);
        entity.setPartition(partition);
        try {
            //TODO: remove as per NXCALS-3291
            // here we flush to induce PersistenceException early and wrap it in ConfigDataConflictException
            EntityData data = entityRepository.save(entity);
            entityManager.flush();
            return data;
        } catch (PersistenceException e) {
            throw new ConfigDataConflictException(e);
        }
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public Set<EntityData> createEntities(@NonNull Set<CreateEntityRequest> createEntityRequests) {
        Map<Long, SystemSpecData> systemSpecDataMap = findAllSystemDataFor(
                createEntityRequests.stream().map(CreateEntityRequest::getSystemId).collect(toSet()));

        Map<SystemSpecData, Set<CreateEntityRequest>> requestsPerSystem = createEntityRequests.stream()
                .collect(groupingBy(request -> systemSpecDataMap.get(request.getSystemId()), toSet()));

        // get pairs of entity and partition keyValues (as avro schema strings) that are used for partition and entity creation
        Map<SystemSpecData, Set<Pair<String, String>>> entityPartitionSchemasPerSystem = getSchemaStringsFromCreateRequests(
                requestsPerSystem);

        Map<SystemSpecData, Map<String, PartitionData>> partitionDataMap = getPartitionDataMap(
                findOrCreatePartitions(entityPartitionSchemasPerSystem));

        Set<EntityData> entities = entityPartitionSchemasPerSystem.entrySet().stream()
                .flatMap(entry -> entry.getValue().stream()
                        .map(entityPartitionSchema -> {
                            SystemSpecData system = entry.getKey();
                            String entityKeyValues = entityPartitionSchema.getLeft();
                            String partitionKeyValues = entityPartitionSchema.getRight();
                            PartitionData partition = partitionDataMap.get(system).get(partitionKeyValues);
                            return createEntityDataWithPartition(entityKeyValues, partition);
                        }))
                .collect(toSet());

        try {
            //TODO: remove as per NXCALS-3291
            // here we flush to induce PersistenceException early and wrap it in ConfigDataConflictException
            Set<EntityData> data = Sets.newHashSet(entityRepository.saveAll(entities));
            entityManager.flush();
            return data;
        } catch (PersistenceException e) {
            throw new ConfigDataConflictException(e);
        }
    }

//    create a map that allows to get PartitionData object by SystemSpecData and partitionKeyValues string
    private Map<SystemSpecData, Map<String, PartitionData>> getPartitionDataMap(Set<PartitionData> partitionData) {
        Map<SystemSpecData, Set<PartitionData>> partitionDataPerSystem = partitionData.stream()
                .collect(groupingBy(PartitionData::getSystem, toSet()));
        return partitionDataPerSystem.entrySet().stream().collect(toMap(
                Map.Entry::getKey,
                entry -> entry.getValue().stream().collect(toMap(
                        PartitionData::getKeyValues,
                        Function.identity()))));
    }

    // convert sets of CreateEntityRequests to sets of Pair<entityKeyValues, partitionKeyValues> in a map per system
    private Map<SystemSpecData, Set<Pair<String, String>>> getSchemaStringsFromCreateRequests(
            Map<SystemSpecData, Set<CreateEntityRequest>> createEntityRequestsPerSystem) {
        return createEntityRequestsPerSystem.entrySet().stream().collect(toMap(
                Map.Entry::getKey,
                entry -> entry.getValue().stream()
                        .map(request -> convertToAvroSchemaStrings(entry.getKey(), request.getEntityKeyValues(),
                                request.getPartitionKeyValues()))
                        .collect(Collectors.toSet())));
    }

    private Map<Long, SystemSpecData> findAllSystemDataFor(@NonNull Set<Long> systemIds) {
        Map<Long, SystemSpecData> systemSpecMap = systemRepository.findAllByIdIn(systemIds).stream()
                .collect(Collectors.toMap(SystemSpecData::getId, Function.identity()));
        if (systemIds.size() != systemSpecMap.size()) {
            throw NotFoundRuntimeException.missingSystems(Sets.difference(systemIds, systemSpecMap.keySet()));
        }
        return systemSpecMap;
    }

    private EntityData createEntityDataWithPartition(String entityKeyValues, PartitionData partitionData) {
        EntityData entity = createEntityData(entityKeyValues);
        entity.setPartition(partitionData);
        return entity;
    }

    private Pair<String, String> convertToAvroSchemaStrings(SystemSpecData system, Map<String, Object> entityKey,
            Map<String, Object> partitionKey) {
        String entity = convertMapIntoAvroSchemaString(entityKey, system.getEntityKeyDefs());
        String partition = convertMapIntoAvroSchemaString(partitionKey, system.getPartitionKeyDefs());
        return Pair.of(entity, partition);
    }

    private Map<Long, EntityData> fetchEntityIdToEntityMapOrThrow(List<Entity> entityDataList) {
        Set<Long> entityDataIds = Identifiable.obtainIdsFrom(entityDataList);

        Map<Long, EntityData> fetchedEntitiesMap = entityRepository.findAllByIdIn(entityDataIds).stream()
                .collect(toMap(EntityData::getId, Function.identity()));

        if (fetchedEntitiesMap.size() < entityDataList.size()) {
            entityDataIds.removeAll(fetchedEntitiesMap.keySet());
            throw notFound("Entities by the following ids were not found: [ %s ]",
                    StringUtils.join(entityDataIds, ", "));
        }
        return fetchedEntitiesMap;
    }

    private void addLatestHistoryEntriesTo(List<EntityData> entities) {
        if (isEmpty(entities)) {
            return;
        }

        Map<EntityData, EntityHistoryData> entityToFirstHistoryEntryMap = BulkUtils
                .batchAndApply(entities, entityHistoryRepository::findAllLatestEntriesByEntityIn)
                .stream().collect(toMap(EntityHistoryData::getEntity, Function.identity()));

        for (EntityData entity : entities) {
            EntityHistoryData firstEntityHistoryEntry = entityToFirstHistoryEntryMap.get(entity);
            if (firstEntityHistoryEntry == null) {
                log.info("No newest history for entityId={}", entity.getId());
                continue;
            }
            entity.setEntityHistories(ImmutableSortedSet.of(firstEntityHistoryEntry));
        }
    }

    private void addHistoryEntriesTo(List<EntityData> entities, Instant start,
            Instant end) {
        log.debug("Getting history for {} entities between {} and {}", entities.size(), start, end);
        Map<EntityData, List<EntityHistoryData>> historyByEntity = BulkUtils
                .batchAndApply(entities, ents -> entityHistoryRepository.findByEntitiesAndTimestamps(ents, start, end),
                        historyReadMaxBatchSize).stream()
                .collect(groupingBy(EntityHistoryData::getEntity));
        for (EntityData entity : entities) {
            List<EntityHistoryData> entityHistory = historyByEntity.get(entity);
            if (!isEmpty(entityHistory)) {
                entity.setEntityHistories(new TreeSet<>(entityHistory));
            }
        }
    }

    private List<EntityData> findAllEntities(String search,
            QueryOptions<EntityQueryWithOptions> queryOptions) {
        return entityRepository.queryAll(search, EntityData.class,
                Optional.of(EntityData.WITH_PARTITION),
                createQueryProcessor(queryOptions, EntityData.class),
                createResultProcessor(queryOptions));
    }

    private EntityData findEntityByIdOrThrow(long id) {
        return entityRepository.findById(id).orElseThrow(() -> notFound("Entity with id %s not found", id));
    }

    private EntityHistoryData getFirstEntityHistoryOf(EntityData entity) {
        return entityHistoryRepository.findFirstByEntityOrderByValidToStampAsc(entity);
    }

    @Transactional(transactionManager = "jpaTransactionManager")
    public EntityData findOrCreateEntityFor(long systemId, long entityId, long partitionId, String schemaAsString,
            long timestamp) {
        EntityData entity = this.findEntityByIdOrThrow(entityId);
        try (AutoCloseableLock ignored = this.getLockFor(systemId + entity.getKeyValues())) {
            PartitionData partition = this.partitionPartitionByIdOrThrow(partitionId);
            checkSystem(systemId, entity, partition);
            EntitySchemaData schema = findOrCreateSchema(schemaAsString);
            return updateEntityHistoryFor(entity, partition, schema, timestamp);
        }
    }

    private void checkSystem(long systemId, EntityData entity, PartitionData partition) {
        if (systemId != entity.getPartition().getSystem().getId() || systemId != partition.getSystem().getId()) {
            throw new IllegalArgumentException(
                    "System id " + systemId + " does not match system from entityId=" + entity.getId()
                            + " or system from partitionId=" + partition.getId());
        }
    }

    private PartitionData partitionPartitionByIdOrThrow(long partitionId) {
        return this.partitionRepository.findById(partitionId)
                .orElseThrow(() -> new IllegalArgumentException("No such partition with id=" + partitionId));
    }

    private PartitionData getPartition(SystemSpecData system, String partitionKeyValues, EntityData entity) {
        // find or create partition for a given system & partition key values
        if (entity.getPartition() == null || !partitionKeyValues.equals(entity.getPartition().getKeyValues())) {
            // This is different partition than on the entity, maybe changed partition
            return findOrCreatePartition(system, partitionKeyValues);
        }
        return entity.getPartition();
    }

    private EntityData getEntity(SystemSpecData system, String entityKeyValues) {
        return entityRepository.queryOne(
                toRSQL(Entities.suchThat().systemId().eq(system.getId()).and().keyValues().eq(entityKeyValues)),
                EntityData.class).orElseGet(() -> createEntityData(entityKeyValues));
    }

    private EntityData updateEntityHistoryFor(EntityData entity, PartitionData newPartition, EntitySchemaData newSchema,
            long recordTimestamp) {
        Instant recordTime = getInstantFromNanos(recordTimestamp);
        EntityHistoryData entityHistory;

        //new entity will not have the partition set
        if (entity.getPartition() == null) {
            entityHistory = handleNewEntity(entity, newPartition, newSchema, recordTime);
        } else {
            entityHistory = handleExistingEntity(entity, newPartition, newSchema, recordTime);
        }
        return saveEntity(entity, entityHistory);

    }

    private EntityHistoryData handleExistingEntity(EntityData entity, PartitionData newPartition,
            EntitySchemaData newSchema, Instant recordTime) {
        //This is not a new entity so we search in the history
        Optional<EntityHistoryData> entityHistOptional = entityHistoryRepository
                .findByEntityAndTimestamp(entity, recordTime);
        return entityHistOptional
                .map(hist -> handleExistingEntityHist(entity, newPartition, newSchema, recordTime, hist))
                .orElseGet(() -> handleAbsentEntityHist(entity, newPartition, newSchema, recordTime));
    }

    /*
        The recordTime must be before all known history, we have to add this to the begining of the history...
        This can only work if we assume that the schema of this one is the same as the first entry in the history.
        We cannot accept records which come with different schema than the one from the first entry.
        The reasoning is that this would create an entry in the history linking the time of this record to the first known history which this new schema.
        This is not necessarily good as the next bunch of records might come with yet different schema and get rejected.
        The only good solution to this is to create a "good" history somehow by hand and than send data or send late forgotten data with the schema of the first history.
    */
    private EntityHistoryData handleAbsentEntityHist(EntityData entity, PartitionData newPartition,
            EntitySchemaData newSchema, Instant recordTime) {
        EntityHistoryData firstHistory = getFirstEntityHistoryOf(entity);
        if (firstHistory == null) {
            return createAndPersistEntityHistory(entity, newPartition, newSchema, recordTime);
        }
        if (isEntityHistEqualToNewPartitionAndSchema(firstHistory, newPartition, newSchema)) {
            //Since NXCALS-1907 we have to check if the timestamp is not already in range as this history might have
            //been updated by another thread (a race condition can happen when history is read twice in the same thread).
            if (firstHistory.getValidFromStamp().isAfter(recordTime)) {
                if (log.isDebugEnabled()) {
                    log.debug(
                            "Extending entity history for entity={}, entityId={} entityHistId={}, recordVersion={}, current from={}, current to={}, recordTime={}",
                            entity.getKeyValues(), entity.getId(), firstHistory.getId(), firstHistory.getRecVersion(),
                            firstHistory.getValidFromStamp(), firstHistory.getValidToStamp(), recordTime);
                }

                firstHistory.setValidFromStamp(recordTime);
                firstHistory = entityHistoryRepository.save(firstHistory);

                if (log.isTraceEnabled()) {
                    log.trace("History extended for entityId={}, entityHistId={}, recordVersion={}, from={} to={}",
                            entity.getId(), firstHistory.getId(), firstHistory.getRecVersion(),
                            firstHistory.getValidFromStamp(), firstHistory.getValidToStamp());
                }
            } else {
                if (log.isTraceEnabled()) {
                    log.trace(
                            "History not extended due to recordTime={} already within history range for entityId={}, entityHistId={}, recordVersion={}, from={} to={}",
                            recordTime, entity.getId(), firstHistory.getId(), firstHistory.getRecVersion(),
                            firstHistory.getValidFromStamp(), firstHistory.getValidToStamp());
                }
            }

        } else {
            throw lateDataAmbiguity(entity, recordTime, firstHistory);
        }
        return firstHistory;
    }

    private EntityHistoryData handleExistingEntityHist(EntityData entity, PartitionData newPartition,
            EntitySchemaData newSchema, Instant recordTime, EntityHistoryData entityHistory) {
        if (log.isTraceEnabled()) {
            log.trace("Using exising history for entityId={}, entityHistId={}, recordVersion={}, from={} to={}",
                    entity.getId(), entityHistory.getId(), entityHistory.getRecVersion(),
                    entityHistory.getValidFromStamp(), entityHistory.getValidToStamp());
        }
        EntityHistoryData history = entityHistory;
        boolean historyMatchesNewRecord = isEntityHistEqualToNewPartitionAndSchema(history, newPartition, newSchema);

        if (history.isLatest()) {
            if (!historyMatchesNewRecord && history.getValidFromStamp().equals(recordTime)) {
                throw lateDataConflict(entity, recordTime, history);
            }

            if (!historyMatchesNewRecord) {
                // This entity has different state than the last history so we must finish the open history record and create a new one.
                // We have to finish the last history state and open a new one, set the new partition & schema to the entity

                //lets check first if the history frequency is already not too high
                checkEntityHistoryFrequency(entity, recordTime);

                entity.setPartition(newPartition);

                history.setValidToStamp(recordTime);
                entityHistoryRepository.save(history); //save the changed history record

                // Need to flush in order to preserve the order statements are executed in the DB
                // This is because JPA first executes the insert of the new history entry and next the update of the
                // last one.
                // In that case (temporary after the insert) we have two records with null validToStamp which triggers
                // the constraint.
                this.entityManager.flush();

                history = createAndPersistEntityHistory(entity, newPartition, newSchema, recordTime);
                //this one adds this new history to entity.
            }
            //else nothing to do as the entity state matches the history, just add to the entity history

        } else if (!historyMatchesNewRecord) {
            //this is some old data with already closed history record.
            // Late data - the timestamp is in the past, before the last record

            // Late data that has different schema or partition for time period already in history - this is an
            // invalid case, we do not allow it.
            throw lateDataConflict(entity, recordTime, history);
        }

        return history;
    }

    /**
     * Throws exception if the creation frequency exceeds the threshold.
     * The count is very fast for a given entityId and between limited time, thus not caching anything.
     * Also not caching helps fixing problems faster.
     * We can always remove the history by hand from the DB after a misbehaving record is fixed at the source enabling
     * its logging immediately.
     *
     * @param entity
     * @param recordTime
     */
    private void checkEntityHistoryFrequency(EntityData entity, Instant recordTime) {
        Instant start = recordTime.minus(historyCountLookupTime);
        long count = entityHistoryRepository.countByEntityAndValidFromStampBetweenTimestamps(entity, start, recordTime);
        if (count > this.historyCountAlertThreshold) {
            //alert, this helps already spot misbehaving records before we actually block them.
            log.error("Large number of history entries={} for entity={} between {} and {}", count, entity, start, recordTime);
            metricsRegistry.incrementCounterBy(HISTORY_MAX_COUNT_ALERT_METRIC, 1);
        }
        if (count > this.historyCountMaxAllowed) {
            //throw exception to reject
            throw new IllegalArgumentException("Max number of history changes of " + historyCountMaxAllowed + " in " + historyCountLookupTime + " exceeded for " + entity + " between " + start + " and " + recordTime);
        }
    }

    private EntityData saveEntity(EntityData entity, EntityHistoryData entityHistory) {
        EntityData newEntity = entityRepository.save(entity);
        //make sure we return only one history item.
        newEntity.clearHistory();
        newEntity.addEntityHist(entityHistory);
        return newEntity;
    }

    private EntityHistoryData handleNewEntity(EntityData entity, PartitionData newPartition, EntitySchemaData newSchema,
            Instant recordTime) {
        entity.setPartition(newPartition);
        return createAndPersistEntityHistory(entity, newPartition, newSchema, recordTime);
    }

    private EntityHistoryData createAndPersistEntityHistory(EntityData entity, PartitionData partition,
            EntitySchemaData schema, Instant validFromStamp) {
        return createAndPersistEntityHistory(entity, partition, schema, validFromStamp, null);
    }

    private EntityHistoryData createAndPersistEntityHistory(EntityData entity, PartitionData partition,
            EntitySchemaData schema, Instant validFromStamp, Instant validToStamp) {
        EntityHistoryData newEntityHistory = new EntityHistoryData();
        newEntityHistory.setEntity(entity);
        newEntityHistory.setSchema(schema);
        newEntityHistory.setPartition(partition);
        newEntityHistory.setValidFromStamp(validFromStamp);
        newEntityHistory.setValidToStamp(validToStamp);
        return entityHistoryRepository.save(newEntityHistory);
    }

    private boolean isEntityHistEqualToNewPartitionAndSchema(EntityHistoryData lastHistory, PartitionData newPartition,
            EntitySchemaData newSchema) {
        return lastHistory.getPartition().equals(newPartition) && lastHistory.getSchema().equals(newSchema);
    }

    private EntitySchemaData findOrCreateSchema(String schemaAsString) {
        String md5Hash = DigestUtils.md5Hex(schemaAsString);
        try (AutoCloseableLock ignored = this.getLockFor(md5Hash)) {
            return schemaRepository
                    .queryOne(toRSQL(EntitySchemas.suchThat().schemaHash().eq(md5Hash)), EntitySchemaData.class)
                    .orElseGet(() -> createSchema(md5Hash, schemaAsString));
        }
    }

    private PartitionData findOrCreatePartition(SystemSpecData system, String partitionKeyValues) {
        try (AutoCloseableLock ignored = getLockFor(system.getId() + partitionKeyValues)) {
            return partitionRepository.queryOne(
                    toRSQL(Partitions.suchThat().systemId().eq(system.getId()).and().keyValues()
                            .eq(partitionKeyValues)), PartitionData.class)
                    .orElseGet(() -> createAndPersistPartition(system, partitionKeyValues));
        }
    }

    private Set<PartitionData> findOrCreatePartitions(
            Map<SystemSpecData, Set<Pair<String, String>>> entityPartitionSchemasPerSystem) {
        // system + partitionKeyValues (unique for each partition) for the requested partitions
        Map<SystemSpecData, Set<String>> partitionSchemasPerSystem = getPartitionSchemasPerSystem(
                entityPartitionSchemasPerSystem);

        Set<AutoCloseableLock> locks = emptySet();
        try {
            locks = getLocksForPartitions(partitionSchemasPerSystem);

            Set<PartitionData> existingPartitions = partitionSchemasPerSystem.entrySet().stream()
                    .flatMap(entry -> getExistingPartitions(entry.getKey(), entry.getValue()).stream())
                    .collect(toSet());

            // get map of partitionKeyValues strings per system for partitions that are missing and have to be created
            Map<SystemSpecData, Set<String>> missingPartitions = getMissingPartitions(partitionSchemasPerSystem,
                    existingPartitions);

            Set<PartitionData> createdPartitions = createAndPersistPartitions(missingPartitions);
            existingPartitions.addAll(createdPartitions);
            return existingPartitions;
        } finally {
            locks.forEach(AutoCloseableLock::close);
        }
    }

    private Map<SystemSpecData, Set<String>> getMissingPartitions(
            Map<SystemSpecData, Set<String>> requiredPartitionSchemasPerSystem, Set<PartitionData> existingPartitions) {
        Map<SystemSpecData, Set<PartitionData>> existingPartitionDataPerSystem = existingPartitions.stream()
                .collect(groupingBy(PartitionData::getSystem, toSet()));
        Map<SystemSpecData, Set<String>> existingPartitionSchemasPerSystem =
                existingPartitionDataPerSystem.entrySet().stream().collect(toMap(
                        Map.Entry::getKey,
                        entry -> entry.getValue().stream().map(PartitionData::getKeyValues).collect(toSet())));

        return requiredPartitionSchemasPerSystem.entrySet().stream().collect(toMap(
                Map.Entry::getKey,
                entry -> {
                    Set<String> existingPartitionSchemas = existingPartitionSchemasPerSystem.get(entry.getKey());
                    Set<String> requiredPartitionSchemas = entry.getValue();
                    if (existingPartitionSchemas != null) {
                        return Sets.difference(requiredPartitionSchemas, existingPartitionSchemas);
                    } else {
                        return requiredPartitionSchemas;
                    }
                }));
    }

    // get map of partitionKeyValues (as avro schema string) per system
    private Map<SystemSpecData, Set<String>> getPartitionSchemasPerSystem(
            Map<SystemSpecData, Set<Pair<String, String>>> entityPartitionSchemasPerSystem) {
        return entityPartitionSchemasPerSystem.entrySet().stream().collect(toMap(
                Map.Entry::getKey,
                entry -> entry.getValue().stream().map(Pair::getRight).collect(toSet())));
    }

    private Collection<PartitionData> getExistingPartitions(SystemSpecData system, Set<String> partitionKeyValuesSet) {
        return BulkUtils.batchAndApply(partitionKeyValuesSet, partitionKeyValues -> partitionRepository
                .queryAll(toRSQL(Partitions.suchThat().systemId().eq(system.getId()).and().keyValues()
                        .in(partitionKeyValues)), PartitionData.class));
    }

    private Set<AutoCloseableLock> getLocksForPartitions(Map<SystemSpecData, Set<String>> partitionKeyValuesPerSystem) {
        return partitionKeyValuesPerSystem.entrySet().stream()
                .flatMap(entry -> entry.getValue().stream()
                        .map(partitionKeyValues -> getLockFor(entry.getKey().getId() + partitionKeyValues)))
                .collect(toSet());
    }

    private EntityData createEntityData(String entityKeyValues) {
        EntityData entity = new EntityData();
        entity.setKeyValues(entityKeyValues);
        return entity;
    }

    private EntitySchemaData createSchema(String md5Hash, String recordFieldsWithTypes) {
        EntitySchemaData schema = new EntitySchemaData();
        schema.setContent(recordFieldsWithTypes);
        schema.setContentHash(md5Hash);
        return schema;
    }

    private PartitionData createAndPersistPartition(SystemSpecData system, String partitionKeyValues) {
        PartitionData partition = new PartitionData();
        partition.setKeyValues(partitionKeyValues);
        partition.setSystem(system);
        PartitionData partitionData = partitionRepository.save(partition);
        entityManager.flush();
        return partitionData;
    }

    private Set<PartitionData> createAndPersistPartitions(
            Map<SystemSpecData, Set<String>> partitionKeyValuesPerSystem) {
        Set<PartitionData> partitions = partitionKeyValuesPerSystem.entrySet().stream()
                .flatMap(entry -> entry.getValue().stream().map(partitionKeyValues -> {
                    PartitionData partition = new PartitionData();
                    partition.setKeyValues(partitionKeyValues);
                    partition.setSystem(entry.getKey());
                    return partition;
                }))
                .collect(Collectors.toSet());
        Set<PartitionData> createdPartitions = Sets.newHashSet(partitionRepository.saveAll(partitions));
        entityManager.flush();
        return createdPartitions;
    }

    private void verifyBounds(long from, long to, String suffix) {
        Preconditions.checkArgument(from <= to,
                String.format("Start time (%d) must be less or equal end time (%d)", from, to) + suffix);
    }

    private ConfigDataConflictException lateDataConflict(EntityData entity, Instant recordTime,
            EntityHistoryData history) {
        log.warn("Data history conflict for entity={} recordTimestamp={} historyId={}", entity.getKeyValues(),
                recordTime, history.getId());
        return dataConflict(entity, recordTime, history);
    }

    private ConfigDataConflictException lateDataAmbiguity(EntityData entity, Instant recordTime,
            EntityHistoryData history) {
        log.warn("Late data ambiguity conflict for entity={} recordTimestamp={} historyId={}", entity.getKeyValues(),
                recordTime, history.getId());
        return dataConflict(entity, recordTime, history);
    }

    private ConfigDataConflictException dataConflict(EntityData entity, Instant recordTime, EntityHistoryData history) {
        return new ConfigDataConflictException(String.format("%s entityId=%s recordTime=%s historyId=%s",
                "We received late data with different schema or partition that was not used before, ", entity.getId(),
                recordTime, history.getId()));
    }

    private NotFoundRuntimeException notFound(String format, Object... values) {
        return new NotFoundRuntimeException(String.format(format, values));
    }

    private SystemSpecData getSystemOrThrow(long systemId) {
        return systemRepository.findById(systemId).orElseThrow(() -> {
            log.warn("There is no system corresponding to systemId = {}", systemId);
            return notFound(SYSTEM_NOT_FOUND_ERROR_FORMAT, systemId);
        });
    }

}
