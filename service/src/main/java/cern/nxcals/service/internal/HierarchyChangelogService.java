package cern.nxcals.service.internal;

import cern.nxcals.service.domain.HierarchyChangelogData;
import cern.nxcals.service.repository.HierarchyChangelogRepository;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * The implementation of the {@link HierarchyChangelogService} interface.
 */
@Service
@Slf4j
@AllArgsConstructor
public class HierarchyChangelogService {

    @NonNull
    private final HierarchyChangelogRepository hierarchyChangelogRepository;

    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public List<HierarchyChangelogData> findAll(String search) {
        return this.hierarchyChangelogRepository.queryAll(search, HierarchyChangelogData.class);
    }
}
