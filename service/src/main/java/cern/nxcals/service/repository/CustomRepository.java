package cern.nxcals.service.repository;

import cern.nxcals.common.utils.UnaryOperatorUtils;
import cern.nxcals.service.rsql.CustomArgumentParser;
import cern.nxcals.service.rsql.JpaCriteriaSubqueryVisitor;
import cern.nxcals.service.rsql.OperatorsSupplier;
import com.github.tennaito.rsql.jpa.AbstractJpaVisitor;
import com.github.tennaito.rsql.misc.ArgumentParser;
import com.google.common.collect.Iterables;
import cz.jirutka.rsql.parser.RSQLParser;
import cz.jirutka.rsql.parser.ast.ComparisonOperator;
import cz.jirutka.rsql.parser.ast.Node;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Subquery;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.UnaryOperator;

/**
 * N.B. before adding any new method that may fire changelog triggers, please, ensure that this method execution
 * will match the pointcut {@link cern.nxcals.service.tracing.TraceDbActionAspect#executeTransaction()}
 */
@Slf4j
public class CustomRepository<T, I extends Serializable> extends SimpleJpaRepository<T, I> implements RsqlRepository<T>, RsqlGraphRepository<T> {
    public static final int ORACLE_MAX_SIZE = 1000;

    private final EntityManager entityManager;
    private final Set<ComparisonOperator> operators;
    private final ArgumentParser argumentParser = new CustomArgumentParser();

    CustomRepository(JpaEntityInformation entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);
        this.entityManager = entityManager;
        this.operators = new OperatorsSupplier().get();
    }

    @Override
    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public <S extends T> List<S> queryAll(@NonNull String rsqlQuery, @NonNull Class<S> clazz) {
        log.debug("Query Repo:[{}] for class {}", rsqlQuery, clazz);
        return entityManager.createQuery(query(rsqlQuery, clazz))
                .getResultList();
    }

    @Override
    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public <S extends T> List<S> queryAll(@NonNull String rsqlQuery, @NonNull String graphName,
            @NonNull Class<S> clazz) {
        log.debug("Query Repo:[{}] for class {}, using graph {}", rsqlQuery, clazz, graphName);
        EntityGraph entityGraph = entityManager.getEntityGraph(graphName);
        return entityManager.createQuery(query(rsqlQuery, clazz))
                .setHint("javax.persistence.fetchgraph", entityGraph)
                .getResultList();
    }

    @Override
    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public <S extends T> List<S> queryAll(@NonNull String rsqlQuery, @NonNull Class<S> clazz,
            @NonNull Optional<String> maybeGraphName,
            @NonNull BiFunction<CriteriaBuilder, CriteriaQuery<S>, CriteriaQuery<S>> queryPostProcessor,
            @NonNull UnaryOperator<TypedQuery<S>> resultPostProcessor) {
        log.debug("Query Repo:[{}] for class {}, using graph {}", rsqlQuery, clazz, maybeGraphName.orElse(""));

        CriteriaQuery<S> criteriaQuery = query(rsqlQuery, clazz, queryPostProcessor);

        TypedQuery<S> typedQuery = entityManager.createQuery(criteriaQuery);

        TypedQuery<S> processedTypedQuery = composeGraphHint(maybeGraphName, resultPostProcessor).apply(
                typedQuery);

        return processedTypedQuery.getResultList();
    }

    private <S> UnaryOperator<TypedQuery<S>> composeGraphHint(Optional<String> maybeGraphName,
            UnaryOperator<TypedQuery<S>> queryProcessor) {
        if (maybeGraphName.isPresent()) {
            EntityGraph entityGraph = entityManager.getEntityGraph(maybeGraphName.get());
            UnaryOperator<TypedQuery<S>> graphNameApplier = x -> x.setHint("javax.persistence.fetchgraph",
                    entityGraph);
            return UnaryOperatorUtils.compose(queryProcessor, graphNameApplier);
        }
        return queryProcessor;
    }

    @Override
    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public <S extends T> Optional<S> queryOne(@NonNull String rsqlQuery, @NonNull Class<S> clazz) {
        List<S> all = queryAll(rsqlQuery, clazz);
        return Optional.ofNullable(Iterables.getOnlyElement(all, null));
    }

    @Override
    @Transactional(transactionManager = "jpaTransactionManager", readOnly = true)
    public <S extends T> Optional<S> queryOne(@NonNull String rsqlQuery, @NonNull String graphName,
            @NonNull Class<S> clazz) {
        List<S> all = queryAll(rsqlQuery, graphName, clazz);
        return Optional.ofNullable(Iterables.getOnlyElement(all, null));
    }

    private <S extends T> CriteriaQuery<S> query(String rsqlQuery, Class<S> clazz) {
        return query(rsqlQuery, clazz, (x, query) -> query);
    }

    private <S extends T> CriteriaQuery<S> query(String rsqlQuery, Class<S> clazz,
            BiFunction<CriteriaBuilder, CriteriaQuery<S>, CriteriaQuery<S>> queryMapper) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<S> query = builder.createQuery(clazz);
        Predicate predicate = builder.in(query.from(clazz)).value(criteria(rsqlQuery, clazz, query));
        return queryMapper.apply(builder, query.where(predicate));
    }

    private <S extends T> Subquery<S> criteria(String rsqlQuery, Class<S> clazz, CriteriaQuery<S> parent) {
        Node rootNode = new RSQLParser(operators).parse(rsqlQuery);
        AbstractJpaVisitor<Subquery<S>, S> visitor = new JpaCriteriaSubqueryVisitor<>(parent);
        visitor.setEntityClass(clazz);
        visitor.getBuilderTools().setArgumentParser(argumentParser);
        return rootNode.accept(visitor, entityManager);
    }
}
