/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.repository;

import cern.nxcals.service.internal.component.BulkUtils;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static com.google.common.collect.Sets.newHashSet;

/**
 * N.B. before adding any new method that may fire changelog triggers, please, ensure that this method execution
 * will match the pointcut {@link cern.nxcals.service.tracing.TraceDbActionAspect#executeTransaction()}
 */
@NoRepositoryBean
public interface BaseRepository<T> extends CrudRepository<T, Long> {
    @Override
    Optional<T> findById(@NotNull Long id);

    default Set<T> findAllByIdIn(Set<Long> identifiers) {
        return new HashSet<>(BulkUtils.batchAndApply(identifiers, ids -> newHashSet(findAllById(ids))));
    }
}
