package cern.nxcals.service.repository;

import cern.nxcals.service.domain.HierarchyChangelogData;

/**
 * N.B. before adding any new method that may fire changelog triggers, please, ensure that this method execution
 * will match the pointcut {@link cern.nxcals.service.tracing.TraceDbActionAspect#executeTransaction()}
 */
public interface HierarchyChangelogRepository
        extends BaseRepository<HierarchyChangelogData>, RsqlRepository<HierarchyChangelogData> {
}
