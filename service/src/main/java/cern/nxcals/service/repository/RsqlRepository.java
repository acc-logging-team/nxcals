package cern.nxcals.service.repository;

import java.util.List;
import java.util.Optional;

/**
 * N.B. before adding any new method that may fire changelog triggers, please, ensure that this method execution
 * will match the pointcut {@link cern.nxcals.service.tracing.TraceDbActionAspect#executeTransaction()}
 */
interface RsqlRepository<T> {
    <S extends T> List<S> queryAll(String rsqlQuery, Class<S> clazz);
    <S extends T> Optional<S> queryOne(String rsqlQuery, Class<S> clazz);
}