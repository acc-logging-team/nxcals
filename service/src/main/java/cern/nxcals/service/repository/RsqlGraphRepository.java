package cern.nxcals.service.repository;

import lombok.NonNull;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.UnaryOperator;

/**
 * N.B. before adding any new method that may fire changelog triggers, please, ensure that this method execution
 * will match the pointcut {@link cern.nxcals.service.tracing.TraceDbActionAspect#executeTransaction()}
 */
public interface RsqlGraphRepository<T> {
    <S extends T> List<S> queryAll(String rsqlQuery, String graphName, Class<S> clazz);

    <S extends T> Optional<S> queryOne(String rsqlQuery, String graphName, Class<S> clazz);

    <S extends T> List<S> queryAll(@NonNull String rsqlQuery, @NonNull Class<S> clazz,
            @NonNull Optional<String> graphName,
            @NonNull BiFunction<CriteriaBuilder, CriteriaQuery<S>, CriteriaQuery<S>> queryPostProcessor,
            @NonNull UnaryOperator<TypedQuery<S>> resultPostProcessor);
}
