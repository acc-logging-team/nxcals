/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.repository;

import cern.nxcals.service.domain.EntityData;
import cern.nxcals.service.domain.EntityHistoryData;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.Instant;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;
import java.util.SortedSet;

/**
 * N.B. before adding any new method that may fire changelog triggers, please, ensure that this method execution
 * will match the pointcut {@link cern.nxcals.service.tracing.TraceDbActionAspect#executeTransaction()}
 */
public interface EntityHistoryRepository extends BaseRepository<EntityHistoryData>, RsqlRepository<EntityHistoryData> {

    /**
     * Finds all history elements that fall into the specified time ranges.
     *
     * @param entity           an {@link EntityData} instance to be associated with history
     * @param instantFromNanos the time {@link Instant} that corresponds to the beginning of the enty's validity
     * @param instantToNanos   the time {@link Instant} that corresponds to the end of the entry's validity
     * @return a {@link SortedSet} that contains the matched entities, based on the given search criteria
     */
    @Query("select h from EntityHistoryData h where h.entity = :entity  and (h.validFromStamp <= :end and (h.validToStamp > :start or h.validToStamp is null)) order by h.validFromStamp desc")
    SortedSet<EntityHistoryData> findByEntityAndTimestamps(@Param("entity") EntityData entity,
                                                           @Param("start") Instant instantFromNanos, @Param("end") Instant instantToNanos);

    /**
     * Finds the entity history that is valid for this given timestamp (the timestamp is inside the history <validFrom;validToStamp) range.
     *
     * @param entity     an {@link EntityData} instance to be associated with history
     * @param recordTime the time {@link Instant} that corresponds to a point of time that the entry is valid
     * @return an {@link EntityHistoryData} instance that is valid in the provided point of time
     */
    @Query("select h from EntityHistoryData h where h.entity = :entity  and (h.validFromStamp <= :timestamp and (h.validToStamp > :timestamp or h.validToStamp is null))")
    Optional<EntityHistoryData> findByEntityAndTimestamp(@Param("entity") EntityData entity,
            @Param("timestamp") Instant recordTime);

    /**
     * Finds the first (the oldest) element in the history.
     *
     * @param entity an {@link EntityData} instance to be associated with history
     * @return an {@link EntityHistoryData} instance that represents the fist history entry for the given entity
     */
    EntityHistoryData findFirstByEntityOrderByValidToStampAsc(EntityData entity);

    /**
     * Finds the latest (the newest) element in the history.
     *
     * @param entity an {@link EntityData} instance to be associated with history
     * @return an {@link EntityHistoryData} instance that represents the latest history entry for the given entity
     */
    EntityHistoryData findFirstByEntityOrderByValidToStampDesc(EntityData entity);

    /**
     * Finds the first (the oldest) element in the history for each of the provided entities.
     * The expected result is one history entry per each provided entity that exists in the system.
     *
     * @param entities a collection of {@link EntityData} instances to fetch the history for.
     * @return a {@link Set} of {@link EntityHistoryData} instances that correspond to the provided entities.
     */
    @Query("select h from EntityHistoryData h where h.validFromStamp = (select min(hh.validFromStamp) from EntityHistoryData hh where hh.entity = h.entity) and h.entity in :entities")
    Set<EntityHistoryData> findAllFirstEntriesByEntityIn(@Param("entities") Set<EntityData> entities);

    /**
     * Finds the latest (the newest) element in the history for each of the provided entities.
     * The expected result is one history entry per each provided entity that exists in the system.
     *
     * @param entities a collection of {@link EntityData} instances to fetch the history for.
     * @return a {@link Set} of {@link EntityHistoryData} instances that correspond to the provided entities.
     */
    @Query("select h from EntityHistoryData h where h.validFromStamp = (select max(hh.validFromStamp) from EntityHistoryData hh where hh.entity = h.entity) and h.entity in :entities")
    Set<EntityHistoryData> findAllLatestEntriesByEntityIn(@Param("entities") Collection<EntityData> entities);

    /**
     * Finds all history elements of the given entities that fall into the specified time ranges.
     */
    @Query("select h from EntityHistoryData h where h.entity in :entities and (h.validFromStamp <= :end and (h.validToStamp > :start or h.validToStamp is null)) order by h.validFromStamp desc")
    Set<EntityHistoryData> findByEntitiesAndTimestamps(@Param("entities") Collection<EntityData> entities,
                                                       @Param("start") Instant instantFromNanos, @Param("end") Instant instantToNanos);


    /**
     * Counts the number of history entries for a given entity in a time window (inclusive) using the validFromStamp field.
     *
     * @param entity
     * @param instantFromNanos
     * @param instantToNanos
     * @return
     */
    @Query("select count(h) from EntityHistoryData h where h.entity = :entity and h.validFromStamp >= :start and h.validFromStamp <= :end")
    long countByEntityAndValidFromStampBetweenTimestamps(@Param("entity") EntityData entity,
                                                         @Param("start") Instant instantFromNanos, @Param("end") Instant instantToNanos);


}
