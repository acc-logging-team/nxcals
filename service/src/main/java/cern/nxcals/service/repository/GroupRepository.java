package cern.nxcals.service.repository;

import cern.nxcals.service.domain.GroupData;

/**
 * N.B. before adding any new method that may fire changelog triggers, please, ensure that this method execution
 * will match the pointcut {@link cern.nxcals.service.tracing.TraceDbActionAspect#executeTransaction()}
 */
public interface GroupRepository extends BaseRepository<GroupData>, RsqlRepository<GroupData>, RsqlGraphRepository<GroupData> {

}
