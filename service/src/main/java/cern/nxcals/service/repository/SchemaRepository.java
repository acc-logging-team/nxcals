/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.repository;

import cern.nxcals.service.domain.EntitySchemaData;

/**
 * N.B. before adding any new method that may fire changelog triggers, please, ensure that this method execution
 * will match the pointcut {@link cern.nxcals.service.tracing.TraceDbActionAspect#executeTransaction()}
 */
public interface SchemaRepository extends BaseRepository<EntitySchemaData>, RsqlRepository<EntitySchemaData> {

}