package cern.nxcals.service.rest;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.Hierarchy;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableHierarchies;
import cern.nxcals.api.domain.VariableHierarchyIds;
import cern.nxcals.service.domain.EntityData;
import cern.nxcals.service.domain.HierarchyData;
import cern.nxcals.service.domain.VariableData;
import cern.nxcals.service.internal.HierarchyService;
import io.micrometer.core.annotation.Timed;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static cern.nxcals.common.web.Endpoints.BATCH;
import static cern.nxcals.common.web.Endpoints.ENTITIES;
import static cern.nxcals.common.web.Endpoints.HIERARCHIES;
import static cern.nxcals.common.web.Endpoints.HIERARCHIES_FIND_ALL;
import static cern.nxcals.common.web.Endpoints.IDS_SUFFIX;
import static cern.nxcals.common.web.Endpoints.VARIABLES;
import static java.util.stream.Collectors.toSet;

@RestController
@Slf4j
@Timed
@RequiredArgsConstructor
public class HierarchyController {
    private final HierarchyService service;

    @PostMapping(value = HIERARCHIES, consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasPermission('HIERARCHY', ACCESS_WRITE)")
    public Hierarchy create(@RequestBody Hierarchy hierarchy) {
        return service.create(hierarchy).toHierarchy();
    }

    @PutMapping(value = HIERARCHIES, consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasPermission('HIERARCHY', ACCESS_WRITE)")
    public Hierarchy update(@RequestBody Hierarchy hierarchy) {
        return service.update(hierarchy).toHierarchy();
    }

    @DeleteMapping(value = HIERARCHIES + "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasPermission('HIERARCHY', ACCESS_WRITE)")
    public void deleteLeaf(@PathVariable("id") long hierarchyId) {
        service.deleteLeaf(hierarchyId);
    }

    @PostMapping(value = HIERARCHIES + BATCH, consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasPermission('HIERARCHY', ACCESS_WRITE)")
    public Set<Hierarchy> createAll(@RequestBody Set<Hierarchy> hierarchies) {
        return service.createAll(hierarchies).stream().map(HierarchyData::toHierarchy).collect(Collectors.toSet());
    }

    @PutMapping(value = HIERARCHIES + BATCH, consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasPermission('HIERARCHY', ACCESS_WRITE)")
    public Set<Hierarchy> updateAll(@RequestBody Set<Hierarchy> hierarchies) {
        return service.updateAll(hierarchies).stream().map(HierarchyData::toHierarchy).collect(Collectors.toSet());
    }

    @DeleteMapping(value = HIERARCHIES + BATCH, consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasPermission('HIERARCHY', ACCESS_WRITE)")
    public void deleteAllLeaves(@RequestBody Set<Long> hierarchyIds) {
        service.deleteAllLeaves(hierarchyIds);
    }

    /**
     * @deprecated only for compatibility with pre-0.4.46 releases
     */
    @PutMapping(value = HIERARCHIES + "/{id}" + VARIABLES, consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasPermission('HIERARCHY', ACCESS_WRITE)")
    @Deprecated
    public void setVariablesOld(@PathVariable("id") long hierarchyId, @RequestBody Set<Variable> variables) {
        setVariables(hierarchyId, variables.stream().map(Variable::getId).collect(toSet()));
    }

    @PutMapping(value = HIERARCHIES + "/{id}" + VARIABLES + IDS_SUFFIX, consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasPermission('HIERARCHY', ACCESS_WRITE)")
    public void setVariables(@PathVariable("id") long hierarchyId, @RequestBody Set<Long> variables) {
        service.setVariables(hierarchyId, variables);
    }

    @PostMapping(value = HIERARCHIES + "/{id}" + VARIABLES + IDS_SUFFIX, consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasPermission('HIERARCHY', ACCESS_WRITE)")
    public void addVariables(@PathVariable("id") long hierarchyId, @RequestBody Set<Long> variables) {
        service.addVariables(hierarchyId, variables);
    }

    @DeleteMapping(value = HIERARCHIES + "/{id}" + VARIABLES + IDS_SUFFIX, consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasPermission('HIERARCHY', ACCESS_WRITE)")
    public void removeVariables(@PathVariable("id") long hierarchyId, @RequestBody Set<Long> variables) {
        service.removeVariables(hierarchyId, variables);
    }

    @PostMapping(value = HIERARCHIES + "/{id}" + VARIABLES, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Set<Variable> getVariables(@PathVariable("id") long hierarchyId) {
        return service.getVariables(hierarchyId).stream().map(VariableData::toVariable).collect(toSet());
    }

    /**
     * @deprecated only for compatibility with pre-0.4.46 releases
     */
    @PutMapping(value = HIERARCHIES + "/{id}" + ENTITIES, consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasPermission('HIERARCHY', ACCESS_WRITE)")
    @Deprecated
    public void setEntitiesOld(@PathVariable("id") long hierarchyId, @RequestBody Set<Entity> entities) {
        setEntities(hierarchyId, entities.stream().map(Entity::getId).collect(toSet()));
    }

    @PutMapping(value = HIERARCHIES + "/{id}" + ENTITIES + IDS_SUFFIX, consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasPermission('HIERARCHY', ACCESS_WRITE)")
    public void setEntities(@PathVariable("id") long hierarchyId, @RequestBody Set<Long> entities) {
        service.setEntities(hierarchyId, entities);
    }

    @PostMapping(value = HIERARCHIES + "/{id}" + ENTITIES + IDS_SUFFIX, consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasPermission('HIERARCHY', ACCESS_WRITE)")
    public void addEntities(@PathVariable("id") long hierarchyId, @RequestBody Set<Long> entities) {
        service.addEntities(hierarchyId, entities);
    }

    @DeleteMapping(value = HIERARCHIES + "/{id}" + ENTITIES + IDS_SUFFIX, consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasPermission('HIERARCHY', ACCESS_WRITE)")
    public void removeEntities(@PathVariable("id") long hierarchyId, @RequestBody Set<Long> entities) {
        service.removeEntities(hierarchyId, entities);
    }

    @PostMapping(value = HIERARCHIES + "/{id}" + ENTITIES, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Set<Entity> getEntities(@PathVariable("id") long hierarchyId) {
        return service.getEntities(hierarchyId).stream().map(EntityData::toEntity).collect(toSet());
    }

    @PostMapping(value = HIERARCHIES_FIND_ALL, consumes = MediaType.TEXT_PLAIN_VALUE)
    public Set<Hierarchy> findAll(@RequestBody String search) {
        return service.findAll(search).stream().map(HierarchyData::toHierarchy).collect(toSet());
    }

    /**
     * @deprecated maintained for compatibility.
     * Use {@link HierarchyController#getHierarchiesForVariables} instead.
     */
    @Deprecated
    @GetMapping(value = HIERARCHIES + VARIABLES + "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Set<Hierarchy> getHierarchiesForVariable(@PathVariable("id") long variableId) {
        return this.getHierarchiesForVariables(Collections.singleton(variableId)).stream()
                .flatMap(e -> e.getHierarchies().stream()).collect(Collectors.toSet());
    }

    @PostMapping(value = HIERARCHIES + BATCH + VARIABLES + IDS_SUFFIX, produces = MediaType.APPLICATION_JSON_VALUE)
    public Set<VariableHierarchies> getHierarchiesForVariables(@RequestBody Set<Long> variableIds) {
        return service.getHierarchiesForVariables(variableIds).entrySet().stream()
                .map(this::toVariableHierarchies).collect(Collectors.toSet());
    }

    @PostMapping(value = HIERARCHIES + IDS_SUFFIX + BATCH + VARIABLES
            + IDS_SUFFIX, produces = MediaType.APPLICATION_JSON_VALUE)
    public Set<VariableHierarchyIds> getHierarchyIdsForVariables(@RequestBody Set<Long> variableIds) {
        return service.getHierarchyIdsForVariables(variableIds).entrySet().stream()
                .map(this::toVariableHierarchyIds).collect(Collectors.toSet());
    }

    private VariableHierarchies toVariableHierarchies(@NonNull Map.Entry<Long, Set<HierarchyData>> entry) {
        Set<Hierarchy> hierarchies = entry.getValue().stream().map(HierarchyData::toHierarchy).collect(toSet());
        return VariableHierarchies.builder().variableId(entry.getKey()).hierarchies(hierarchies).build();
    }

    private VariableHierarchyIds toVariableHierarchyIds(@NonNull Map.Entry<Long, Set<Long>> entry) {
        return VariableHierarchyIds.builder().variableId(entry.getKey()).hierarchyIds(entry.getValue()).build();
    }
}
