package cern.nxcals.service.rest;

import cern.nxcals.api.domain.PartitionResource;
import cern.nxcals.service.domain.PartitionResourceData;
import cern.nxcals.service.internal.PartitionResourceService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;
import java.util.stream.Collectors;

import static cern.nxcals.common.web.Endpoints.PARTITION_RESOURCES;
import static cern.nxcals.common.web.Endpoints.PARTITION_RESOURCES_FIND_ALL;

@RestController
@Slf4j
@RequiredArgsConstructor
public class PartitionResourceController {
    private final PartitionResourceService service;

    @PostMapping(value = PARTITION_RESOURCES, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public PartitionResource create(@RequestBody PartitionResource partitionResource) {
        return service.create(partitionResource).toPartitionResource();
    }

    @PutMapping(value = PARTITION_RESOURCES, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public PartitionResource update(@RequestBody PartitionResource partitionResource) {
        return service.update(partitionResource).toPartitionResource();
    }

    @DeleteMapping(value = PARTITION_RESOURCES + "/{id}")
    public void delete(@PathVariable("id") long partitionResourceId) {
        service.delete(partitionResourceId);
    }

    @PostMapping(value = PARTITION_RESOURCES_FIND_ALL, consumes = MediaType.TEXT_PLAIN_VALUE)
    public Set<PartitionResource> findAll(@RequestBody String search) {
        log.debug("QueryAll:[{}]", search);
        return service.findAll(search).stream()
                .map(PartitionResourceData::toPartitionResource)
                .collect(Collectors.toSet());
    }
}
