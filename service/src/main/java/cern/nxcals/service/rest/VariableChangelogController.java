/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.rest;

import cern.nxcals.api.domain.VariableChangelog;
import cern.nxcals.service.domain.VariableChangelogData;
import cern.nxcals.service.internal.VariableChangelogService;
import io.micrometer.core.annotation.Timed;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;
import java.util.stream.Collectors;

import static cern.nxcals.common.web.Endpoints.VARIABLE_CHANGELOGS_FIND_ALL;

@RestController
@Slf4j
@RequiredArgsConstructor
@Timed
public class VariableChangelogController {

    private final VariableChangelogService internalVariableChangelogService;

    @PostMapping(value = VARIABLE_CHANGELOGS_FIND_ALL, consumes = MediaType.TEXT_PLAIN_VALUE)
    public Set<VariableChangelog> findAllVariableChangelogs(@RequestBody String search) {
        return internalVariableChangelogService.findAll(search).stream()
                .map(VariableChangelogData::toVariableChangelog).collect(Collectors.toSet());
    }

}
