/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.rest;

import cern.nxcals.api.domain.Partition;
import cern.nxcals.service.domain.PartitionData;
import cern.nxcals.service.internal.PartitionService;
import com.google.common.annotations.VisibleForTesting;
import io.micrometer.core.annotation.Timed;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

import static cern.nxcals.common.web.Endpoints.PARTITIONS;
import static cern.nxcals.common.web.Endpoints.PARTITIONS_FIND_ALL;
import static java.util.stream.Collectors.toSet;

@RestController
@Slf4j
@AllArgsConstructor
@Timed
public class PartitionController {

    @VisibleForTesting
    static final String ERROR_MESSAGE = "Partition for system %s and %s not found";
    private final PartitionService service;

    @PostMapping(value = PARTITIONS_FIND_ALL, consumes = MediaType.TEXT_PLAIN_VALUE)
    public Set<Partition> findAll(@RequestBody String search) {
        log.debug("QueryAll:[{}]", search);
        return service.findAll(search).stream().map(PartitionData::toPartition).collect(toSet());
    }

    @PutMapping(value = PARTITIONS, consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasSystemPermission(#partition.systemSpec.id, ACCESS_WRITE)")
    public Partition update(@RequestBody Partition partition) {
        return service.update(partition).toPartition();
    }

    @PostMapping(value = PARTITIONS, consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasSystemPermission(#partition.systemSpec.id, ACCESS_WRITE)")
    public Partition create(@RequestBody Partition partition) {
        return service.create(partition).toPartition();
    }
}
