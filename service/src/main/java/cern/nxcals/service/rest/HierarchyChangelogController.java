package cern.nxcals.service.rest;

import cern.nxcals.api.domain.HierarchyChangelog;
import cern.nxcals.service.domain.HierarchyChangelogData;
import cern.nxcals.service.internal.HierarchyChangelogService;
import io.micrometer.core.annotation.Timed;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;
import java.util.stream.Collectors;

import static cern.nxcals.common.web.Endpoints.HIERARCHIES_CHANGELOGS_FIND_ALL;

@RestController
@Slf4j
@RequiredArgsConstructor
@Timed
public class HierarchyChangelogController {

    private final HierarchyChangelogService internalHierarchyChangelogService;

    @PostMapping(value = HIERARCHIES_CHANGELOGS_FIND_ALL, consumes = MediaType.TEXT_PLAIN_VALUE)
    public Set<HierarchyChangelog> findAllHierarchyChangelogs(@RequestBody String search) {
        return internalHierarchyChangelogService.findAll(search).stream()
                .map(HierarchyChangelogData::toHierarchyChangelog)
                .collect(Collectors.toSet());
    }

}
