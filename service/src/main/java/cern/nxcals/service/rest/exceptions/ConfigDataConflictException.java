/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.rest.exceptions;

import cern.nxcals.api.domain.Group;
import cern.nxcals.api.domain.Hierarchy;

public class ConfigDataConflictException extends RuntimeException {
    public static ConfigDataConflictException conflictOnHierarchy(Hierarchy hierarchy) {
        return new ConfigDataConflictException(String.format("Cannot create Hierarchy [%1$s]. Hierarchy already exists", hierarchy.getName()));
    }

    public static ConfigDataConflictException conflictOnGroup(Group group) {
        return new ConfigDataConflictException(String.format("Cannot create Group [%1$s]. Group already exists", group.getName()));
    }

    public ConfigDataConflictException(Throwable cause) {
        super(cause);
    }

    public ConfigDataConflictException(String message) {
        super(message);
    }

    public ConfigDataConflictException(String message, Throwable cause) {
        super(message, cause);
    }

}
