/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.rest;

import cern.nxcals.api.domain.CreateEntityRequest;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.metadata.queries.EntityQueryWithOptions;
import cern.nxcals.common.domain.FindOrCreateEntityRequest;
import cern.nxcals.service.domain.EntityData;
import cern.nxcals.service.internal.EntityService;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.micrometer.core.annotation.Timed;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static cern.nxcals.common.web.Endpoints.BATCH;
import static cern.nxcals.common.web.Endpoints.ENTITIES;
import static cern.nxcals.common.web.Endpoints.ENTITIES_FIND_ALL;
import static cern.nxcals.common.web.Endpoints.ENTITIES_FIND_ALL_WITH_HISTORY;
import static cern.nxcals.common.web.Endpoints.ENTITIES_FIND_ALL_WITH_OPTIONS;
import static cern.nxcals.common.web.Endpoints.ENTITY_EXTEND_FIRST_HISTORY;
import static cern.nxcals.common.web.Endpoints.ENTITY_UPDATE;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

/**
 * Remarks (mamajews): http error codes can now be customized by using {@link cern.nxcals.service.rest.GlobalControllerExceptionHandler}
 */
@RestController
@Slf4j
@RequiredArgsConstructor
@Timed
public class EntityController {
    private final EntityService entityService;

    @PutMapping(value = ENTITIES, params = { "systemId", "recordTimestamp" })
    @PreAuthorize("hasSystemPermission(#systemId, ACCESS_WRITE)")
    public Entity findOrCreateEntityFor(@RequestParam long systemId, @RequestParam long recordTimestamp,
            @RequestBody FindOrCreateEntityRequest findOrCreateEntityRequest) {
        return entityService.findOrCreateEntityFor(systemId, findOrCreateEntityRequest.getEntityKeyValues(),
                findOrCreateEntityRequest.getPartitionKeyValues(), findOrCreateEntityRequest.getSchema(),
                recordTimestamp).toEntity();
    }

    @PutMapping(value = ENTITIES, params = { "systemId", "entityId", "partitionId", "recordTimestamp" })
    @PreAuthorize("hasSystemPermission(#systemId, ACCESS_WRITE)")
    public Entity findOrCreateEntityFor(@RequestParam long systemId, @RequestParam long entityId,
            @RequestParam long partitionId, @RequestBody String recordFieldDefinitions,
            @RequestParam long recordTimestamp) {
        return entityService
                .findOrCreateEntityFor(systemId, entityId, partitionId, recordFieldDefinitions, recordTimestamp)
                .toEntity();
    }

    @PutMapping(value = ENTITY_EXTEND_FIRST_HISTORY, params = { "entityId", "from" })
    @PreAuthorize("hasEntityPermission(#entityId, ACCESS_WRITE)")
    public Entity extendEntityFirstHistoryDataFor(@RequestParam long entityId, @RequestParam long from,
            @RequestBody String schema) {
        return entityService.extendEntityFirstHistoryDataFor(entityId, schema, from).toEntity();
    }

    @PutMapping(value = ENTITY_UPDATE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasEntityPermission(#entityDataList, ACCESS_WRITE)")
    public List<Entity> updateEntities(@NotEmpty @RequestBody List<Entity> entityDataList) {
        return entityService.updateEntities(entityDataList).stream().map(EntityData::toEntity).collect(toList());
    }

    @PostMapping(value = ENTITIES_FIND_ALL, consumes = MediaType.TEXT_PLAIN_VALUE)
    public Set<Entity> findAll(@RequestBody String search) {
        log.debug("QueryAll:[{}]", search);
        return entityService.findAll(search).stream().map(EntityData::toEntity).collect(toSet());
    }

    @PostMapping(value = ENTITIES_FIND_ALL_WITH_HISTORY, params = { "startTime",
            "endTime" }, consumes = MediaType.TEXT_PLAIN_VALUE)
    public Set<Entity> findAll(@RequestBody String search, @RequestParam long startTime, @RequestParam long endTime) {
        log.debug("QueryAll:[{}] with history start [{}] end [{}]", search, startTime, endTime);
        return entityService.findAll(search, startTime, endTime).stream().map(EntityData::toEntity).collect(toSet());
    }

    @PostMapping(value = ENTITIES_FIND_ALL_WITH_OPTIONS, params = {
            "queryOptions" }, consumes = MediaType.TEXT_PLAIN_VALUE)
    public List<Entity> findAll(@RequestBody String search, @RequestParam String queryOptions) throws
            JsonProcessingException {
        log.debug("QueryAll:[{}] with options [{}]", search, queryOptions);
        return entityService.findAll(search, EntityQueryWithOptions.fromJson(queryOptions)).stream()
                .map(EntityData::toEntity).collect(Collectors.toList());
    }

    @PutMapping(value = ENTITIES, params = { "systemId" })
    @PreAuthorize("hasSystemPermission(#systemId, ACCESS_WRITE)")
    public Entity createEntity(@RequestParam("systemId") long systemId,
            @RequestBody FindOrCreateEntityRequest request) {
        return entityService.createEntity(systemId, request.getEntityKeyValues(), request.getPartitionKeyValues())
                .toEntity();
    }

    @PostMapping(value = ENTITIES + BATCH)
    @PreAuthorize("hasSystemPermission(#requests, ACCESS_WRITE)")
    public Set<Entity> createEntities(@RequestBody Set<CreateEntityRequest> requests) {
        return entityService.createEntities(requests).stream().map(EntityData::toEntity).collect(toSet());
    }
}
