/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.rest.exceptions;

public class VersionMismatchException extends RuntimeException {

    public VersionMismatchException(String message) {
        super(message);
    }

}
