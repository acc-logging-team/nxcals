/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.rest;

import cern.nxcals.api.domain.VariableConfigChangelog;
import cern.nxcals.service.domain.VariableConfigChangelogData;
import cern.nxcals.service.internal.VariableConfigChangelogService;
import io.micrometer.core.annotation.Timed;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;
import java.util.stream.Collectors;

import static cern.nxcals.common.web.Endpoints.VARIABLE_CONFIG_CHANGELOGS_FIND_ALL;

@RestController
@Slf4j
@RequiredArgsConstructor
@Timed
public class VariableConfigChangelogController {

    private final VariableConfigChangelogService internalVariableConfigChangelogService;

    @PostMapping(value = VARIABLE_CONFIG_CHANGELOGS_FIND_ALL, consumes = MediaType.TEXT_PLAIN_VALUE)
    public Set<VariableConfigChangelog> findAllVariableConfigChangelogs(@RequestBody String search) {
        return internalVariableConfigChangelogService.findAll(search).stream().map(
                VariableConfigChangelogData::toVariableConfigChangelog).collect(Collectors.toSet());
    }

}
