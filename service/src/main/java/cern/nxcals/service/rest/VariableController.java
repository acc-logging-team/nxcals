/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.rest;

import avro.shaded.com.google.common.collect.Maps;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.metadata.queries.VariableQueryWithOptions;
import cern.nxcals.common.utils.MapUtils;
import cern.nxcals.service.domain.VariableData;
import cern.nxcals.service.internal.VariableSchemaService;
import cern.nxcals.service.internal.VariableService;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.micrometer.core.annotation.Timed;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.avro.Schema;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static cern.nxcals.common.web.Endpoints.BATCH;
import static cern.nxcals.common.web.Endpoints.VARIABLES;
import static cern.nxcals.common.web.Endpoints.VARIABLES_FIND_ALL;
import static cern.nxcals.common.web.Endpoints.VARIABLES_FIND_ALL_WITH_OPTIONS;
import static cern.nxcals.common.web.Endpoints.VARIABLE_FIND_SCHEMAS;

@RestController
@Slf4j
@RequiredArgsConstructor
@Timed
public class VariableController {
    private final VariableService internalVariableService;
    private final VariableSchemaService internalVariableSchemaService;

    @PostMapping(value = VARIABLES, consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasPermission('VARIABLE', ACCESS_WRITE)")
    public Variable create(@RequestBody Variable variableData) {
        return internalVariableService.create(variableData).toVariable();
    }

    @PutMapping(value = VARIABLES, consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasPermission('VARIABLE', ACCESS_WRITE)")
    public Variable update(@RequestBody Variable variableData) {
        return internalVariableService.update(variableData).toVariable();
    }

    @PostMapping(value = VARIABLES_FIND_ALL, consumes = MediaType.TEXT_PLAIN_VALUE)
    public Set<Variable> findAll(@RequestBody String search) {
        log.debug("QueryAll:[{}]", search);
        return internalVariableService.findAll(search).stream().map(VariableData::toVariable)
                .collect(Collectors.toSet());
    }

    @PostMapping(value = VARIABLES_FIND_ALL_WITH_OPTIONS, params = {
            "queryOptions" }, consumes = MediaType.TEXT_PLAIN_VALUE)
    public List<Variable> findAll(@RequestBody String search, @RequestParam String queryOptions)
            throws JsonProcessingException {
        log.debug("QueryAll:[{}]", search);
        VariableQueryWithOptions variableQueryWithOptions = VariableQueryWithOptions.fromJson(queryOptions);
        List<VariableData> variableData = internalVariableService.findAll(search, variableQueryWithOptions);
        return convertToVariables(variableData, variableQueryWithOptions.getQueryType());
    }

    @PostMapping(value = VARIABLE_FIND_SCHEMAS, params = {
            "startTime", "endTime"
    }, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Map<Long, Map<String, Set<TimeWindow>>> findSchemas(@RequestBody Set<Long> variableIds,
            @RequestParam("startTime") Instant startTime, @RequestParam("endTime") Instant endTime) {
        log.debug("Find schemas for :[{}]", variableIds);
        return convert(internalVariableSchemaService.findSchemas(variableIds,
                TimeWindow.between(startTime, endTime)));
    }

    @DeleteMapping(value = VARIABLES + "/{id}")
    @PreAuthorize("hasPermission('VARIABLE', ACCESS_WRITE)")
    public void delete(@PathVariable("id") long variableId) {
        internalVariableService.delete(variableId);
    }

    @DeleteMapping(value = VARIABLES + BATCH, consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasPermission('VARIABLE', ACCESS_WRITE)")
    public void deleteAll(@RequestBody Set<Long> variableIds) {
        internalVariableService.deleteAll(variableIds);
    }

    @PostMapping(value = VARIABLES + BATCH, consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasPermission('VARIABLE', ACCESS_WRITE)")
    public Set<Variable> createAll(@RequestBody Set<Variable> variables) {
        return internalVariableService.createAll(variables).stream().map(VariableData::toVariable)
                .collect(Collectors.toSet());
    }

    @PutMapping(value = VARIABLES + BATCH, consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasPermission('VARIABLE', ACCESS_WRITE)")
    public Set<Variable> updateAll(@RequestBody Set<Variable> variables) {
        return internalVariableService.updateAll(variables).stream().map(VariableData::toVariable)
                .collect(Collectors.toSet());
    }

    private List<Variable> convertToVariables(List<VariableData> variableData,
            VariableQueryWithOptions.VariableQueryType queryType) {
        Function<VariableData, Variable> mapper;
        if (queryType == VariableQueryWithOptions.VariableQueryType.WITHOUT_VARIABLE_CONFIGS) {
            mapper = VariableData::toVariableWithoutConfigs;
        } else {
            mapper = VariableData::toVariable;
        }
        return variableData.stream().map(mapper).collect(Collectors.toList());
    }

    private Map<Long, Map<String, Set<TimeWindow>>> convert(
            Map<Long, Map<Schema, Set<TimeWindow>>> toConvert) {

        return Maps.transformValues(toConvert,
                map -> MapUtils.mapKeys(map, Schema::toString)
        );
    }
}
