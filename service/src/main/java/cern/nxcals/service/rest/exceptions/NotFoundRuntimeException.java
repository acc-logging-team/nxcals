/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.rest.exceptions;

import cern.nxcals.api.domain.Group;
import cern.nxcals.api.domain.HierarchyView;
import cern.nxcals.api.domain.SystemSpec;

import java.util.Collection;
import java.util.stream.Collectors;

public class NotFoundRuntimeException extends RuntimeException {

    public static NotFoundRuntimeException missingVariables(Collection<Long> missing) {
        return new NotFoundRuntimeException(String.format("Variables missing [%1$s]", missing));
    }

    public static NotFoundRuntimeException missingEntities(Collection<Long> missing) {
        return new NotFoundRuntimeException(String.format("Entities missing [%1$s]", missing));
    }

    public static NotFoundRuntimeException missingHierarchy(HierarchyView hierarchy) {
        return new NotFoundRuntimeException(String.format("Hierarchy [%1$s] does not exist", hierarchy.getNodePath()));
    }

    public static NotFoundRuntimeException missingHierarchies(Collection<HierarchyView> hierarchies) {
        return new NotFoundRuntimeException(String.format("Hierarchy paths missing [%1$s]",
                hierarchies.stream().map(HierarchyView::getNodePath).collect(Collectors.toSet())));
    }

    public static NotFoundRuntimeException missingHierarchyIds(Collection<Long> missing) {
        return new NotFoundRuntimeException(String.format("Hierarchies missing [%1$s]", missing));
    }

    public static NotFoundRuntimeException missingHierarchy(long hierarchyId) {
        return new NotFoundRuntimeException(String.format("Hierarchy with id [%1$d] does not exist", hierarchyId));
    }

    public static NotFoundRuntimeException missingEntity(long entityId) {
        return new NotFoundRuntimeException(String.format("Entity with id [%1$d] does not exist", entityId));
    }

    public static NotFoundRuntimeException missingVariable(long variableId) {
        return new NotFoundRuntimeException(String.format("Variable with id [%1$d] does not exist", variableId));
    }

    public static NotFoundRuntimeException missingGroup(Group group) {
        return new NotFoundRuntimeException(String.format("Group [%1$s] does not exist", group.getName()));
    }

    public static NotFoundRuntimeException missingGroup(long groupId) {
        return new NotFoundRuntimeException(String.format("Group with id [%1$d] does not exist", groupId));
    }

    public static NotFoundRuntimeException missingSystem(SystemSpec systemSpec) {
        return new NotFoundRuntimeException(String.format("System [%1$s] does not exist", systemSpec.getName()));
    }

    public static NotFoundRuntimeException missingSystems(Collection<Long> missing) {
        return new NotFoundRuntimeException(String.format("Systems missing [%1$s]", missing));
    }

    public static NotFoundRuntimeException missingPartitionResource(long partitionResourceId) {
        return new NotFoundRuntimeException(String.format("PartitionResource with id [%1$d] does not exist", partitionResourceId));
    }

    public static NotFoundRuntimeException missingPartition(long partitionId) {
        return new NotFoundRuntimeException(String.format("Partition with id [%1$d] does not exist", partitionId));
    }

    public static NotFoundRuntimeException missingSchema(long schemaId) {
        return new NotFoundRuntimeException(String.format("EntitySchema with id [%1$d] does not exist", schemaId));
    }

    public static NotFoundRuntimeException missingPartitionResource(long systemId, long partitionId, long schemaId) {
        return new NotFoundRuntimeException(String.format(
                "PartitionResource with systemId [%1$d], partitionId [%2$d], schemaId [%3$d], does not exist", systemId,
                partitionId, schemaId));
    }

    public static NotFoundRuntimeException missingPartitionResourceHistory(long partitionResourceInformationId) {
        return new NotFoundRuntimeException(String.format("PartitionResourceHistory with id [%1$d] does not exist",
                partitionResourceInformationId));
    }

    public NotFoundRuntimeException(String message) {
        super(message);
    }
}
