/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.rest;

import static cern.nxcals.common.web.Endpoints.AUTHORIZATION_TOKENS;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import cern.nxcals.api.custom.domain.DelegationToken;
import cern.nxcals.service.internal.AuthorizationService;
import cern.nxcals.service.security.AuthenticationFacade;

@RestController
public class AuthorizationController {

    @Autowired
    private AuthorizationService internalAuthorizationService;

    @GetMapping(value = AUTHORIZATION_TOKENS)
    public DelegationToken createDelegationToken() {
        String username = AuthenticationFacade.getCurrentUser().getUsername();
        return internalAuthorizationService.createDelegationToken(username);
    }

}
