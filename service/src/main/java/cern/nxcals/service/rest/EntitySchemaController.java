/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.rest;

import cern.nxcals.api.domain.EntitySchema;
import cern.nxcals.service.domain.EntitySchemaData;
import cern.nxcals.service.repository.SchemaRepository;
import io.micrometer.core.annotation.Timed;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

import static cern.nxcals.common.web.Endpoints.SCHEMAS_FIND_ALL;
import static java.util.stream.Collectors.toSet;

/**
 * Schema Controller API.
 */
@RestController
@Slf4j
@RequiredArgsConstructor
@Timed
public class EntitySchemaController {

    private final SchemaRepository schemaService;

    @PostMapping(value = SCHEMAS_FIND_ALL, consumes = MediaType.TEXT_PLAIN_VALUE)
    public Set<EntitySchema> findAll(@RequestBody String search) {
        log.debug("QueryAll:[{}]", search);
        return schemaService.queryAll(search, EntitySchemaData.class).stream().map(EntitySchemaData::toEntitySchema).collect(toSet());
    }
}
