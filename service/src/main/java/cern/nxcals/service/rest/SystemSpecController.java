/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.rest;

import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.service.domain.SystemSpecData;
import cern.nxcals.service.repository.SystemSpecRepository;
import io.micrometer.core.annotation.Timed;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

import static cern.nxcals.common.web.Endpoints.SYSTEMS_FIND_ALL;
import static java.util.stream.Collectors.toSet;

@RestController
@Slf4j
@RequiredArgsConstructor
@Timed
public class SystemSpecController {

    private final SystemSpecRepository systemRepository;

    @PostMapping(value = SYSTEMS_FIND_ALL, consumes = MediaType.TEXT_PLAIN_VALUE)
    public Set<SystemSpec> findAll(@RequestBody String search) {
        log.debug("QueryAll:[{}]", search);
        return systemRepository.queryAll(search, SystemSpecData.class).stream().map(SystemSpecData::toSystemSpec).collect(toSet());
    }

}
