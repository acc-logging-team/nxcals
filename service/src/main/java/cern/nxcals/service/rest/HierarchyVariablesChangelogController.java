package cern.nxcals.service.rest;

import cern.nxcals.api.domain.HierarchyVariablesChangelog;
import cern.nxcals.service.domain.HierarchyVariablesChangelogData;
import cern.nxcals.service.internal.HierarchyVariablesChangelogService;
import io.micrometer.core.annotation.Timed;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;
import java.util.stream.Collectors;

import static cern.nxcals.common.web.Endpoints.HIERARCHY_VARIABLES_CHANGELOGS_FIND_ALL;

@RestController
@Slf4j
@RequiredArgsConstructor
@Timed
public class HierarchyVariablesChangelogController {

    private final HierarchyVariablesChangelogService internalHierarchyChangelogService;

    @PostMapping(value = HIERARCHY_VARIABLES_CHANGELOGS_FIND_ALL, consumes = MediaType.TEXT_PLAIN_VALUE)
    public Set<HierarchyVariablesChangelog> findAllHierarchyChangelogs(@RequestBody String search) {
        return internalHierarchyChangelogService.findAll(search).stream()
                .map(HierarchyVariablesChangelogData::toHierarchyVariablesChangelog)
                .collect(Collectors.toSet());
    }

}
