/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.service.rest;

import cern.nxcals.common.domain.ExtractionCriteria;
import cern.nxcals.common.domain.ExtractionUnit;
import cern.nxcals.service.internal.extraction.ExtractionService;
import io.micrometer.core.annotation.Timed;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static cern.nxcals.common.web.Endpoints.EXTRACTION;

@RestController
@Slf4j
@Timed
@RequiredArgsConstructor
public class EntityResourceController {
    private final ExtractionService extractionService;

    @PostMapping(value = EXTRACTION, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ExtractionUnit findExtractionUnitBy(@RequestBody ExtractionCriteria criteria) {
        log.info("Calling find extraction unit by in controller with criteria: {}", criteria);
        if (criteria.isVariableSearch()) {
            return this.extractionService.findExtractionUnitForVariables(criteria);
        }
        return this.extractionService.findExtractionUnitForEntities(criteria);
    }
}
