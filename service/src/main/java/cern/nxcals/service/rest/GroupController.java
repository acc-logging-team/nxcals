package cern.nxcals.service.rest;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.Group;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.service.domain.EntityData;
import cern.nxcals.service.domain.GroupData;
import cern.nxcals.service.domain.VariableData;
import cern.nxcals.service.internal.SecureGroupService;
import io.micrometer.core.annotation.Timed;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static cern.nxcals.common.web.Endpoints.ENTITIES;
import static cern.nxcals.common.web.Endpoints.GROUPS;
import static cern.nxcals.common.web.Endpoints.GROUPS_FIND_ALL;
import static cern.nxcals.common.web.Endpoints.IDS_SUFFIX;
import static cern.nxcals.common.web.Endpoints.VARIABLES;
import static java.util.stream.Collectors.toSet;

@RestController
@Slf4j
@Timed
@RequiredArgsConstructor
public class GroupController {
    private final SecureGroupService service;

    @PostMapping(value = GROUPS, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasPermission('GROUP', ACCESS_WRITE)")
    public Group create(@RequestBody Group group) {
        return service.create(group).toGroup();
    }

    @PutMapping(value = GROUPS, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasPermission('GROUP', ACCESS_WRITE)")
    public Group update(@RequestBody Group group) {
        return service.update(group).toGroup();
    }

    @DeleteMapping(value = GROUPS + "/{id}")
    @PreAuthorize("hasPermission('GROUP', ACCESS_WRITE)")
    public void delete(@PathVariable("id") long groupId) {
        service.delete(groupId);
    }

    @PostMapping(value = GROUPS_FIND_ALL, consumes = MediaType.TEXT_PLAIN_VALUE)
    public Set<Group> findAll(@RequestBody String search) {
        log.debug("QueryAll:[{}]", search);
        return service.findAll(search).stream()
                .map(GroupData::toGroup)
                .collect(Collectors.toSet());
    }

    @GetMapping(value = GROUPS + "/{id}" + VARIABLES, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, Set<Variable>> getVariables(@PathVariable("id") long groupId) {
        return service.getVariables(groupId).entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey,
                        e -> e.getValue().stream().map(VariableData::toVariable).collect(toSet())));
    }

    @PutMapping(value = GROUPS + "/{id}" + VARIABLES + IDS_SUFFIX, consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasPermission('GROUP', ACCESS_WRITE)")
    public void setVariables(@PathVariable("id") long groupId,
            @RequestBody Map<String, Set<Long>> variableIdsPerAssociation) {
        service.setVariables(groupId, variableIdsPerAssociation);
    }

    @PostMapping(value = GROUPS + "/{id}" + VARIABLES + IDS_SUFFIX, consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasPermission('GROUP', ACCESS_WRITE)")
    public void addVariables(@PathVariable("id") long groupId,
            @RequestBody Map<String, Set<Long>> variableIdsPerAssociation) {
        service.addVariables(groupId, variableIdsPerAssociation);
    }

    @DeleteMapping(value = GROUPS + "/{id}" + VARIABLES + IDS_SUFFIX, consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasPermission('GROUP', ACCESS_WRITE)")
    public void removeVariables(@PathVariable("id") long groupId,
            @RequestBody Map<String, Set<Long>> variableIdsPerAssociation) {
        service.removeVariables(groupId, variableIdsPerAssociation);
    }

    @GetMapping(value = GROUPS + "/{id}" + ENTITIES, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, Set<Entity>> getEntities(@PathVariable("id") long groupId) {
        return service.getEntities(groupId).entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey,
                        e -> e.getValue().stream().map(EntityData::toEntity).collect(toSet())));
    }

    @PutMapping(value = GROUPS + "/{id}" + ENTITIES + IDS_SUFFIX, consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasPermission('GROUP', ACCESS_WRITE)")
    public void setEntities(@PathVariable("id") long groupId,
            @RequestBody Map<String, Set<Long>> entityIdsPerAssociation) {
        service.setEntities(groupId, entityIdsPerAssociation);
    }

    @PostMapping(value = GROUPS + "/{id}" + ENTITIES + IDS_SUFFIX, consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasPermission('GROUP', ACCESS_WRITE)")
    public void addEntities(@PathVariable("id") long groupId,
            @RequestBody Map<String, Set<Long>> entityIdsPerAssociation) {
        service.addEntities(groupId, entityIdsPerAssociation);
    }

    @DeleteMapping(value = GROUPS + "/{id}" + ENTITIES + IDS_SUFFIX, consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasPermission('GROUP', ACCESS_WRITE)")
    public void removeEntities(@PathVariable("id") long groupId,
            @RequestBody Map<String, Set<Long>> entityIdsPerAssociation) {
        service.removeEntities(groupId, entityIdsPerAssociation);
    }

}
