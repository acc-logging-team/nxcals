/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.service.rest;

import cern.nxcals.common.domain.DataProcessingJob;
import cern.nxcals.common.domain.DataProcessingJob.JobType;
import cern.nxcals.service.internal.CompactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static cern.nxcals.common.web.Endpoints.COMPACTION_JOBS;

@RestController
public class CompactionController {

    @Autowired
    private CompactionService internalCompactionService;

    @GetMapping(value = COMPACTION_JOBS, params = { "maxJobs", "jobType" })
    public List<DataProcessingJob> getCompactionJobs(@Param("maxJobs") int maxJobs, @Param("jobType") JobType jobType) {
        return internalCompactionService.getDataProcessJobs(maxJobs, jobType);
    }
}
