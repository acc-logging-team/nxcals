#!/bin/bash

# run_ansible()
# function which runs ansible if ANSIBLE is 1, with playbook specified in FILE, for given INVENTORY
# with specified VERSION and additional ANSIBLE_PARAMS_AUX
# What's more, can run with PREVIOUS_VERSION if API_STABILITY is true
run_ansible() {
	if [ "$ANSIBLE" -eq 1 ]; then

		<<-EOF cat
		-----------------------------------------
		**** Running Ansible using inventory $INVENTORY with $FILE and version $VERSION with additional arguments: $ANSIBLE_PARAMS_AUX ****
		If you want to cancel press CTRL+C now...
		-----------------------------------------
		EOF
		sleep 1

		cd ./ansible

		if [ ! -d './ansible-common/roles' ]; then
			../gradlew build
		fi

		if ! [ -z ${API_STABILITY+x} ] && [ "$API_STABILITY" = true ]; then
			<<-EOF cat
			-----------------------------------------
			**** Running API stability test with previous version $PREVIOUS_VERSION
			-----------------------------------------
			EOF
			../gradlew build --refresh-dependencies --no-daemon --parallel
			ansible-playbook -i "$INVENTORY" "$FILE" --vault-pass-file "$VAULT_PASS" -u $USER -e application_version="$PREVIOUS_VERSION" -e pytimber_major_version="$PYTIMBER_MAJOR_VERSION" $ANSIBLE_PARAMS_AUX
		else
			ansible-playbook -i "$INVENTORY" "$FILE" --vault-pass-file "$VAULT_PASS" -u $USER -e application_version="$VERSION" -e pytimber_major_version="$PYTIMBER_MAJOR_VERSION" $ANSIBLE_PARAMS_AUX
		fi

		cd ..
	else
		<<-EOF cat
		-----------------------------------------
		!!!! Ansible step skipped...
		-----------------------------------------
		EOF
	fi
}

# set_nxcals_version()
# function reads version from gradle.properties and set as VERSION
set_nxcals_version() {
	VERSION="$(cat gradle.properties | grep version | awk 'BEGIN { FS="="} { print $2;}')"

	<<-EOF cat
	-----------------------------------------
	Set NXCALS_VERSION=$VERSION
	-----------------------------------------
	EOF
}

# set_pytimber_major_version()
# function reads version from gradle.properties and set as PYTIMBER_MAJOR_VERSION
set_pytimber_version() {
	PYTIMBER_MAJOR_VERSION=$(cat gradle.properties | grep pytimberMajorVersion | awk 'BEGIN { FS="="} { print $2;}')

	<<-EOF cat
	-----------------------------------------
	Set PYTIMBER_MAJOR_VERSION=$PYTIMBER_MAJOR_VERSION
	-----------------------------------------
	EOF
}

# set_hadoop_cluster()
# function reads hadoop cluster name from INVENTORY and set HADOOP_CLUSTER
set_hadoop_cluster() {
	HADOOP_CLUSTER="$(grep [h]adoop_cluster_name: ./ansible/$INVENTORY/group_vars/all.yml | awk '{print $2}')"

	<<-EOF cat
	-----------------------------------------
	Set HADOOP_CLUSTER=$HADOOP_CLUSTER
	-----------------------------------------
	EOF
}

# run_gradle()
# Run gradle with GRADLE_PARAMS and with all nxcals options like
# HADOOP_CLUSTER, NAMESPACE, SERVICE_URL, KAFKA_SERVERS_URL, SPARK_SERVERS_URL and RBAC_ENV
run_gradle() {
	<<-EOF cat
	-----------------------------------------
	**** Running gradle $1 with params: $GRADLE_PARAMS	****
	-----------------------------------------
	EOF

	./gradlew "$@" -Phadoop_cluster="$HADOOP_CLUSTER" --info --parallel --no-daemon $GRADLE_PARAMS \
	-Dnxcals_namespace="$NAMESPACE" \
	-Dservice.url="$SERVICE_URL" \
	-Dkafka.producer.bootstrap.servers="$KAFKA_SERVERS_URL" \
	-Dspark.servers.url="$SPARK_SERVERS_URL" \
	-Drbac.env="$RBAC_ENV"
}

run_gradle_krb_keytab() {
	(
		export KRB5CCNAME="$(mktemp)"
		trap 'rm "$KRB5CCNAME"' EXIT
		run_gradle "$@" -Dkerberos.principal="$USER" -Dkerberos.keytab="$HOME"/.keytab
	)
}

run_gradle_krb_tgt() {
	(
		export KRB5CCNAME="$(mktemp)"
		trap 'rm "$KRB5CCNAME"' EXIT
		kinit -k -t "$HOME"/.keytab "$USER"
		run_gradle "$@"
	)
}

run_gradle_krb_disabled() {
	(
		export KRB5CCNAME="$(mktemp)"
		trap 'rm "$KRB5CCNAME"' EXIT
		run_gradle "$@"
	)
}
