package cern.nxcals.api.ingestion.retry;

import cern.nxcals.api.exceptions.FatalDataConflictRuntimeException;
import cern.nxcals.api.ingestion.IllegalRecordRuntimeException;
import cern.nxcals.api.ingestion.RecordTooBigRuntimeException;
import com.google.common.collect.ImmutableSet;
import lombok.experimental.UtilityClass;
import org.apache.avro.AvroRuntimeException;
import org.apache.avro.AvroTypeException;
import org.apache.avro.SchemaBuilderException;
import org.apache.avro.SchemaParseException;
import org.apache.avro.UnresolvedUnionException;

import java.util.Set;

@UtilityClass
public class AcceptStrategies {
    public AcceptStrategy noDataErrors() {
        //@formatter:off
        Set<Class<?>> fatalExceptions = ImmutableSet.of(
                IllegalRecordRuntimeException.class,
                IllegalStateException.class,
                IllegalArgumentException.class,
                FatalDataConflictRuntimeException.class,
                RecordTooBigRuntimeException.class,
                AvroRuntimeException.class,
                AvroTypeException.class,
                SchemaParseException.class,
                SchemaBuilderException.class,
                UnresolvedUnionException.class
        );
        //@formatter:on
        return t -> {
            if (t == null) {
                return true;
            }
            Throwable tmp = t;
            while (tmp != null) {
                if (fatalExceptions.contains(tmp.getClass())) {
                    return false;
                }
                tmp = tmp.getCause();
            }
            return true;
        };
    }
}
