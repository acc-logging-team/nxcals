/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.api.ingestion;

import cern.cmw.datax.ImmutableData;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.extraction.metadata.InternalServiceClientFactory;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.api.ingestion.BufferedPublisher.DelayedMessage;
import cern.nxcals.api.ingestion.KafkaDataSink.KafkaSinkConfig;
import cern.nxcals.common.avro.DataEncoderImpl;
import cern.nxcals.common.converters.TimeConverterImpl;
import cern.nxcals.common.utils.ConfigHolder;
import cern.nxcals.internal.extraction.metadata.InternalEntityService;
import com.google.common.annotations.VisibleForTesting;
import com.typesafe.config.Config;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.avro.Schema;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;

import java.io.Closeable;
import java.io.IOException;
import java.time.Clock;
import java.time.Duration;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.function.Function;
import java.util.function.Supplier;

import static cern.nxcals.common.utils.ConfigHolder.getInt;
import static cern.nxcals.common.utils.ConfigHolder.getProperty;
import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Objects.requireNonNull;
import static java.util.concurrent.Executors.newFixedThreadPool;
import static java.util.stream.Collectors.toMap;

/**
 * The main entry point to the NXCALS service. Creates the @see {@link Publisher} instance for a given system name and
 * data converting function.
 */
@Slf4j
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
public final class PublisherFactory {
    private static final int DEFAULT_MAX_RECORD_SIZE = 10 * 1024 * 1024; //10MB
    private static final int UPPER_LIMIT_FOR_MAX_RECORD_SIZE = 50 * 1024 * 1024; //50MB
    private static final int RESERVED_PARTITIONS_SIZE = 0;
    private static final int DEFAULT_BUCKET_SIZE = 5;
    private static final Supplier<Producer<byte[], byte[]>> DEFAULT_KAFKA_PRODUCER_SUPPLIER = () -> {
        Map<String, Object> props = getKafkaConfig();
        log.debug("Creating KafkaProducer with config={}", props);
        return new KafkaProducer<>(props);
    };
    @NonNull
    private final InternalEntityService entityService;
    @NonNull
    private final SystemSpecService systemSpecService;
    @NonNull
    private final Supplier<Producer<byte[], byte[]>> kafkaProducerSupplier;

    private PublisherFactory() {
        this(InternalServiceClientFactory.createEntityService(), InternalServiceClientFactory.createSystemSpecService(),
                DEFAULT_KAFKA_PRODUCER_SUPPLIER);
    }

    private static Map<String, Object> getKafkaConfig() {
        Config config = ConfigHolder.getConfig();
        Config rawProps = config.getConfig("kafka.producer");
        if (rawProps.isEmpty()) {
            throw new IllegalStateException("Cannot find kafka producer properties");
        }
        return rawProps.entrySet().stream().collect(toMap(Entry::getKey, entry -> entry.getValue().unwrapped()));
    }

    /**
     * Creating the publisher from the system name and conversion function.
     *
     * @param systemName         Name of system to publish.
     * @param convertingFunction Function used to convert data.
     * @param <V>                Type of data to publish.
     * @return Publisher which can publish data of type {@code <V>}
     */
    public <V> Publisher<V> createPublisher(String systemName, Function<V, ImmutableData> convertingFunction) {
        PublisherHelper<V> helper = createHelper(systemName, convertingFunction);
        Closer closer = this.createCloser(helper.getDataSink(), null);
        return new WrappedPublisher<>(new PublisherImpl<>(helper), closer);
    }

    /**
     * Creating the publisher from the system name and conversion function.
     *
     * @param systemName        Name of system to publish.
     * @param converter         Function used to convert data.
     * @param bufferTime        specifies how long messages should be internally buffered before being sent to NXCALS
     * @param flushingFrequency specifies how often the internal buffer should be checked for flushing
     * @param <V>               Type of data to publish.
     * @return Publisher which can publish data of type {@code <V>}
     */
    public <V> Publisher<V> createBufferedPublisher(String systemName, Function<V, ImmutableData> converter,
            Duration bufferTime, Duration flushingFrequency) {
        ExecutorService processor = newFixedThreadPool(getProperty("publisher.buffer.processor.threads", 2));
        Buffer<DelayedMessage> buffer = new DelayedQueueBuffer<>();
        PublisherHelper<V> helper = this.createHelper(systemName, converter);
        BufferWriter<V> writer = this.createWriter(helper, buffer, bufferTime);
        BufferReader reader = this.createReader(helper.getDataSink(), buffer, processor);
        Closer closer = this.createCloser(helper.getDataSink(), processor);
        return new WrappedPublisher<>(new BufferedPublisher<>(writer, reader, flushingFrequency), closer);
    }

    private Closer createCloser(DataSink<RecordData, DefaultCallback> sink, ExecutorService executor) {
        Closer closer = new Closer();
        closer.register(sink);
        closer.register(executor);
        return closer;
    }

    private BufferReader createReader(DataSink<RecordData, DefaultCallback> sink, Buffer<DelayedMessage> buffer,
            ExecutorService bufferProcessor) {
        int nbOfEntitiesPerRound = getProperty("publisher.buffer.entities.per.round", 500);
        return new BufferReader(sink, buffer, nbOfEntitiesPerRound, bufferProcessor);
    }

    private <V> BufferWriter<V> createWriter(PublisherHelper<V> helper, Buffer<DelayedMessage> buffer,
            Duration bufferTime) {
        return new BufferWriter<>(buffer, helper, bufferTime, Clock.systemDefaultZone());
    }

    private <V> PublisherHelper<V> createHelper(String systemName, Function<V, ImmutableData> converter) {
        requireNonNull(systemName, "System name cannot be null");
        requireNonNull(converter, "Converting function cannot be null");
        SystemSpec systemData = systemSpecService.findByName(systemName)
                .orElseThrow(() -> new IllegalArgumentException("No such system name " + systemName));
        DataEncoderImpl encoder = createDataToAvroServiceEncoder(systemData);
        DataSink<RecordData, DefaultCallback> dataSink = createInternalDataSink(systemData,
                new RecordDataToBytesConverter());
        return PublisherHelper.<V>builder().converter(converter).dataSink(dataSink).encoder(encoder)
                .entityService(entityService).systemId(systemData.getId()).build();
    }

    private DataEncoderImpl createDataToAvroServiceEncoder(SystemSpec systemData) {
        String recordVersionSchemaStr = systemData.getRecordVersionKeyDefinitions();
        Schema recordVersionSchema = null;
        if (recordVersionSchemaStr != null) {
            recordVersionSchema = new Schema.Parser().parse(recordVersionSchemaStr);
        }
        return new DataEncoderImpl(new Schema.Parser().parse(systemData.getEntityKeyDefinitions()),
                new Schema.Parser().parse(systemData.getPartitionKeyDefinitions()),
                new Schema.Parser().parse(systemData.getTimeKeyDefinitions()), recordVersionSchema,
                new TimeConverterImpl());
    }

    private DataSink<RecordData, DefaultCallback> createInternalDataSink(SystemSpec systemData,
            Function<RecordData, byte[]> converter) {
        KafkaSinkConfig config = getSinkConfig(systemData);
        return new KafkaDataSink(config, kafkaProducerSupplier.get(), converter);
    }

    private KafkaSinkConfig getSinkConfig(SystemSpec systemData) {
        String topicName = getProperty("publisher.topic_name", systemData.getName());
        int maxRecordSize = getInt("publisher.max_record_size", DEFAULT_MAX_RECORD_SIZE);
        checkArgument(maxRecordSize > 0, "Max records size [" + maxRecordSize + "] must be greater than 0");
        checkArgument(maxRecordSize <= UPPER_LIMIT_FOR_MAX_RECORD_SIZE,
                "Max records size [" + maxRecordSize + "] must not be greater than " + UPPER_LIMIT_FOR_MAX_RECORD_SIZE);

        int bucketSize = getInt("publisher.kafka.bucket.size", DEFAULT_BUCKET_SIZE);
        checkArgument(bucketSize > 0, "Bucket size [" + bucketSize + "] must be greater than 0");

        int reservedPartitionsSize = getInt("publisher.kafka.reserved.partitions.size", RESERVED_PARTITIONS_SIZE);
        return new KafkaSinkConfig(topicName, maxRecordSize, bucketSize, reservedPartitionsSize);
    }

    /**
     * Factory method.
     *
     * @return New instance of {@link PublisherFactory}
     */
    public static PublisherFactory newInstance() {
        return new PublisherFactory();
    }

    @Data
    @VisibleForTesting
    static class WrappedPublisher<V> implements Publisher<V> {
        @NonNull
        private final Publisher<V> delegate;
        @NonNull
        private final Closer closer;

        @Override
        public CompletableFuture<Result> publishAsync(V value, Executor executor) {
            return delegate.publishAsync(value, executor);
        }

        @Override
        public void close() throws IOException {
            this.delegate.close();
            this.closer.shutdown();
            this.closer.close();
        }
    }

    @Slf4j
    @VisibleForTesting
    static class Closer {
        private final LinkedList<ExecutorService> executors = new LinkedList<>();
        private final LinkedList<Closeable> closeables = new LinkedList<>();

        void register(ExecutorService instance) {
            if (instance != null) {
                executors.add(instance);
            }
        }

        void register(Closeable instance) {
            if (instance != null) {
                closeables.add(instance);
            }
        }

        void shutdown() {
            while (!executors.isEmpty()) {
                ExecutorService executor = executors.removeFirst();
                log.debug("Shutting down {}", executor);
                executor.shutdown();
            }
        }

        void close() {
            while (!closeables.isEmpty()) {
                Closeable closeable = closeables.removeFirst();
                try {
                    log.debug("Closing {}", closeable);
                    closeable.close();
                } catch (IOException e) {
                    log.error("Error while closing {}", closeable, e);
                }
            }
        }
    }

}
