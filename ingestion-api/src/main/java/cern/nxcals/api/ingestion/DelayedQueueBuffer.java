package cern.nxcals.api.ingestion;

import java.util.Collection;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.Delayed;

public class DelayedQueueBuffer<T extends Delayed> implements Buffer<T> {
    private final DelayQueue<T> queue = new DelayQueue<>();

    @Override
    public void push(T element) {
        this.queue.add(element);
    }

    @Override
    public void drainTo(Collection<T> dest) {
        this.queue.drainTo(dest);
    }
}
