package cern.nxcals.api.ingestion;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.ingestion.BufferedPublisher.DelayedMessage;
import cern.nxcals.api.ingestion.BufferedPublisher.EntityData;
import com.google.common.annotations.VisibleForTesting;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.time.Duration;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;

import static java.util.Comparator.comparing;
import static java.util.concurrent.Executors.newSingleThreadScheduledExecutor;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

@Slf4j
@AllArgsConstructor
class BufferReader {
    private final ScheduledExecutorService reader = newSingleThreadScheduledExecutor();
    @NonNull
    private final DataSink<RecordData, DefaultCallback> sink;
    @NonNull
    private final Buffer<DelayedMessage> buffer;
    private final int nbOfEntitiesPerRound;
    @NonNull
    private final Executor processor;

    void start(Duration period) {
        this.reader.scheduleAtFixedRate(this::flush, period.toMillis(), period.toMillis(), MILLISECONDS);
    }

    void close() {
        this.reader.shutdown();
    }

    @VisibleForTesting
    void flush() {
        LinkedList<DelayedMessage> queue = new LinkedList<>();
        try {
            this.buffer.drainTo(queue);
            if (queue.isEmpty()) {
                log.debug("No messages to send");
                return;
            }
            log.debug("Flushing {} messages to NXCALS", queue.size());
            Map<String, List<DelayedMessage>> msgByEntity = new HashMap<>(this.nbOfEntitiesPerRound);
            queue.forEach(msg -> {
                EntityData entityData = msg.getEntityData();
                msgByEntity.computeIfAbsent(entityData.getEntityKey(), ek -> new LinkedList<>()).add(msg);
            });
            msgByEntity.values().forEach(this::send);
        } catch (Exception e) {
            log.error("This must not happen. If it does it's either a developer error or jvm problem", e);
            completeExceptionally(queue, e);
        }
    }

    private void send(List<DelayedMessage> entityMessages) {
        try {
            processor.execute(() -> {
                try {
                    partitionsOf(entityMessages).forEach(this::send);
                } catch (Exception e) {
                    completeExceptionally(entityMessages, e);
                }
            });
        } catch (RejectedExecutionException e) {
            completeExceptionally(entityMessages, e);
        }
    }

    private void completeExceptionally(Collection<DelayedMessage> messages, Throwable e) {
        messages.forEach(msg -> {
            if (!msg.getResult().isDone()) {
                msg.completeExceptionally(e);
            }
        });
    }

    private Collection<LinkedList<DelayedMessage>> partitionsOf(List<DelayedMessage> messages) {
        messages.sort(comparing(this::msgTime));
        LinkedList<LinkedList<DelayedMessage>> partitions = new LinkedList<>();
        String currentPK = null;
        for (DelayedMessage msg : messages) {
            String pK = getPartitionKeyOf(msg.getEntityData());
            if (!pK.equals(currentPK)) {
                partitions.add(new LinkedList<>());
                currentPK = pK;
            }
            partitions.getLast().add(msg);
        }
        return partitions;
    }

    private Long msgTime(DelayedMessage msg) {
        return msg.getEntityData().getTime();
    }

    private String getPartitionKeyOf(EntityData entityData) {
        return StringUtils.join(entityData.getPartitionKey(), entityData.getSchemaKey());
    }

    private void send(LinkedList<DelayedMessage> messages) {
        DelayedMessage msg = messages.removeFirst();
        sendSync(msg);
        messages.forEach(this::sendAsync);
    }

    private void sendAsync(DelayedMessage msg) {
        try {
            msg.getExecutor().execute(() -> sendSync(msg));
        } catch (RejectedExecutionException e) {
            msg.completeExceptionally(e);
        }
    }

    private void sendSync(DelayedMessage msg) {
        try {
            RecordData record = getRecordFrom(msg);
            this.sink.send(record, (res, ex) -> {
                if (ex != null) {
                    msg.completeExceptionally(ex);
                } else {
                    msg.complete(res);
                }
            });
        } catch (Exception e) {
            msg.completeExceptionally(e);
        }
    }

    private RecordData getRecordFrom(DelayedMessage msg) {
        Entity entity = msg.getEntityData().getEntityMaker().get();
        long timestamp = msg.getEntityData().getTime();
        return new RecordData(entity, msg.getData(), timestamp);
    }

}
