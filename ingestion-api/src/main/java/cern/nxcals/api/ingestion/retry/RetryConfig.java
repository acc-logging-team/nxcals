package cern.nxcals.api.ingestion.retry;

import lombok.Builder;
import lombok.Data;

import java.time.Duration;
import java.util.function.Function;
import java.util.function.Predicate;

@Data
@Builder
public class RetryConfig {
    private final Function<Integer, Duration> delayAttempt;
    private final Predicate<Integer> acceptAttempt;
    private final boolean shouldBackup;
}
