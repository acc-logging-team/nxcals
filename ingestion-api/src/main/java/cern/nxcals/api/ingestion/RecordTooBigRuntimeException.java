package cern.nxcals.api.ingestion;

/**
 * Thrown when record exceeds 1MB.
 * Created by jwozniak on 01/04/17.
 */
public class RecordTooBigRuntimeException extends IllegalRecordRuntimeException {

    /**
     * Exception constructor.
     * @param message Exception message.
     */
    public RecordTooBigRuntimeException(String message) {
        super(message);
    }

}
