package cern.nxcals.api.ingestion.retry;

import cern.nxcals.api.ingestion.Result;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.function.Function;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

@Slf4j
@Builder(toBuilder = true)
public class PublicationRetryer<T> implements Retryer<T> {
    @NonNull
    private final ScheduledExecutorService retryExecutor;
    @NonNull
    private final Function<T, CompletableFuture<Result>> publisher;
    @NonNull
    private final RetryConfig retryConfig;
    @NonNull
    private final AcceptStrategy acceptStrategy;
    @NonNull
    private final BackupStrategy<T> backupStrategy;

    @Override
    public CompletableFuture<Void> retry(T msg, Throwable cause) {
        boolean accept = this.acceptStrategy.test(cause);
        CompletableFuture<Void> ret = new CompletableFuture<>();
        if (accept) {
            this.submitRetry(msg, ret, cause, 1);
        } else {
            String causeMessage = cause == null ? "null" : cause.getMessage();
            ret.completeExceptionally(new IllegalStateException("Not recoverable publishing error: " + causeMessage, cause));
        }
        return ret;
    }

    @Override
    public CompletableFuture<Void> retry(T msg) {
        return this.retry(msg, null);
    }

    private void submitRetry(T msg, CompletableFuture<Void> result, Throwable cause, int attempt) {
        this.retryExecutor.schedule(() -> retry(new RetryData<>(msg, attempt, result, cause)),
                this.retryConfig.getDelayAttempt().apply(attempt).toMillis(), MILLISECONDS);
    }

    @SuppressWarnings("squid:UnusedPrivateMethod") // somehow sonar sees this method as unused
    private void retry(RetryData<T> retryData) {
        T msg = retryData.getMessage();
        log.trace("Retrying {} due to {}", retryData, retryData.getCause());
        try {
            this.publisher.apply(msg).whenCompleteAsync((val, e) -> {
                if (e != null) {
                    this.handleRetryException(retryData, e);
                } else {
                    log.trace("Successfully retried {}", retryData);
                    retryData.getCallback().complete(null);
                }
            }, this.retryExecutor);
        } catch (RejectedExecutionException e) {
            this.handleRetryException(retryData, e);
        } catch (Exception e) {
            log.error("Got exception while submitting data to NXCALS for {}", msg, e);
            retryData.getCallback().completeExceptionally(e);
        }
    }

    private void handleRetryException(RetryData<T> retryData, Throwable cause) {
        log.trace("Handling retry exception of {} due to {}", retryData, cause);
        T msg = retryData.getMessage();
        CompletableFuture<Void> callback = retryData.getCallback();
        if (!this.acceptStrategy.test(cause)) {
            log.error("Not recoverable publishing error for {}", retryData.getMessage(), cause);
            callback.completeExceptionally(cause);
            return;
        }
        int nextAttempt = retryData.getAttempt() + 1;
        if (!this.retryConfig.getAcceptAttempt().test(nextAttempt)) {
            log.trace("Retry attempt {} of {} not accepted trying to back up the message", nextAttempt, retryData);
            doBackup(retryData);
            return;
        }
        submitRetry(msg, callback, cause, nextAttempt);
    }

    private void doBackup(RetryData<T> retryData) {
        CompletableFuture<Void> callback = retryData.getCallback();
        if (!this.retryConfig.isShouldBackup()) {
            log.warn("No backup enabled, skipping ");
            callback.completeExceptionally(new IllegalStateException("No backup enabled"));
            return;
        }
        log.trace("Backup enabled, storing {}", retryData);
        this.backupStrategy.backup(retryData.getMessage()).whenComplete((o, e) -> {
            if (e != null) {
                callback.completeExceptionally(e);
            } else {
                callback.complete(null);
            }
        });
    }

    @Data
    @ToString(onlyExplicitlyIncluded = true)
    private static class RetryData<T> {
        @NonNull
        @ToString.Include
        private final T message;
        @ToString.Include
        private final int attempt;
        @NonNull
        private final CompletableFuture<Void> callback;
        private final Throwable cause;
    }

}
