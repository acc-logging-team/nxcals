package cern.nxcals.api.ingestion.retry;

import com.google.common.annotations.VisibleForTesting;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;

@Slf4j
@UtilityClass
public class BackupStrategies {
    private final BackupEventListener EMPTY_LISTENER = filesCount -> {
    };

    public <T> BackupStrategy<T> asyncJavaSerializationToLocalFile(@NonNull BackupConfig config,
            @NonNull ScheduledExecutorService executor) {
        return asyncJavaSerializationToLocalFile(config, executor, EMPTY_LISTENER);
    }

    public <T> BackupStrategy<T> asyncJavaSerializationToLocalFile(@NonNull BackupConfig config,
            @NonNull ScheduledExecutorService executor, @NonNull BackupStrategies.BackupEventListener eventListener) {
        LocalFileBackupStrategy<T> strategy = new LocalFileBackupStrategy<>(executor, config, new JavaSerializer<>(),
                eventListener);
        strategy.init();
        return strategy;
    }

    @VisibleForTesting
    static class JavaSerializer<T> implements Serializer<T> {
        @Override
        public void writeTo(@NonNull List<T> messages, @NonNull File file) {
            if (!(messages instanceof Serializable)) {
                throw new IllegalArgumentException(
                        "Messages collection " + messages.getClass() + "must be java serializable");
            }
            Serializable toSerialize = (Serializable) messages;
            try (FileOutputStream fileStream = new FileOutputStream(file);
                    ObjectOutputStream out = new ObjectOutputStream(fileStream)) {
                out.writeObject(toSerialize);
            } catch (IOException e) {
                log.error("Unable to save {} records in file {}", messages.size(), file, e);
                throw new UncheckedIOException(e.getMessage(), e);
            }
        }

        @Override
        public List<T> readFrom(@NonNull File file) {
            try (FileInputStream fileIn = new FileInputStream(file);
                    ObjectInputStream in = new ObjectInputStream(fileIn)) {
                return (List<T>) in.readObject();
            } catch (Exception e) {
                log.error("Error while reading {} file ", file, e);
                throw new IllegalStateException(e.getMessage(), e);
            }
        }

        @Override
        public String getBackupFileSuffix() {
            return ".serial";
        }

    }

    @Data
    @Builder
    public static class BackupConfig {
        @NonNull
        private final Path backupPath;
        private final String filePrefix;
        private final int bufferSize;
        private final int nbOfFilesToRestoreInOneGo;
        @NonNull
        private final Duration flushBufferFrequency;
    }

    public interface BackupEventListener {
        void onRestore(long filesCount);
    }
}
