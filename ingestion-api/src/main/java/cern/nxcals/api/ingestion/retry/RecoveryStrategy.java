package cern.nxcals.api.ingestion.retry;

/**
 * Recovery strategy for the rejected data stored outside of the publisher jvm. At this level we impose only the fact
 * that is should be an asynchronous service. All the recovery specific details such as number of external items to
 * recover in one go, recovering frequency, external storage location etc. should be left to the implementation.
 */
public interface RecoveryStrategy {

    /**
     * Starts the recovery service.
     */
    void start();

    /**
     * Stops the recovery service.
     */
    void stop();
}
