package cern.nxcals.api.ingestion;

import cern.cmw.datax.EntryType;
import cern.cmw.datax.ImmutableData;
import cern.cmw.datax.ImmutableEntry;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.KeyValues;
import cern.nxcals.common.SystemFields;
import cern.nxcals.common.avro.DataEncoder;
import cern.nxcals.internal.extraction.metadata.InternalEntityService;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import java.util.function.Function;

@Data
@Builder(builderClassName = "Builder")
class PublisherHelper<V> {
    private final long systemId;
    @NonNull
    private final Function<V, ImmutableData> converter;
    @NonNull
    private final InternalEntityService entityService;
    @NonNull
    private final DataEncoder<KeyValues, KeyValues, String, Long> encoder;
    @NonNull
    private final DataSink<RecordData, DefaultCallback> dataSink;

    public Entity findEntity(ImmutableData record, KeyValues entityCachedRequest, Long timestamp) {
        Long entityId;
        Long partitionId = null;
        //This is like that on purpose to avoid getting both ids when the entityId == null already.
        //We don't check in that case the partitionId, it is ignored (for performance reasons)
        if ((entityId = getId(record, SystemFields.NXC_ENTITY_ID.getValue())) != null) {
            partitionId = getId(record, SystemFields.NXC_PARTITION_ID.getValue());
        }

        if (entityId != null && partitionId != null) {
            //Encoding schema here to check if the required partition fields are ok
            return getEntityService().findOrCreateEntityFor(systemId, entityId, partitionId,
                    encoder.encodeRecordFieldDefinitions(record), timestamp);
        } else if (entityId == null) {
            return getEntityService().findOrCreateEntityFor(systemId, entityCachedRequest,
                    encoder.encodePartitionKeyValues(record),
                    encoder.encodeRecordFieldDefinitions(record), timestamp);
        } else {
            throw new IllegalRecordRuntimeException(
                    "Must have both entityId and partitionId set in a record for " + "entityKey=" + entityCachedRequest
                            .getKeyValues() + " system id=" + systemId + " timestamp=" + timestamp);
        }
    }

    private Long getId(ImmutableData record, String fieldName) {
        ImmutableEntry entry = record.getEntry(fieldName);
        return entry != null ? entry.getAs(EntryType.INT64) : null;
    }
}
