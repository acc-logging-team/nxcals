package cern.nxcals.api.ingestion.retry;

import java.util.concurrent.CompletableFuture;

/**
 * Retries sending a message to NXCALS.
 *
 * @param <T> - the type of the message to be re-sent.
 */
public interface Retryer<T> {
    CompletableFuture<Void> retry(T msg, Throwable cause);

    CompletableFuture<Void> retry(T msg);
}
