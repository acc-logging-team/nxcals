package cern.nxcals.api.ingestion.retry;

import cern.nxcals.api.ingestion.retry.BackupStrategy.DataPartition;
import com.google.common.annotations.VisibleForTesting;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.apache.commons.collections.CollectionUtils.isEmpty;

@UtilityClass
public class RecoveryStrategies {

    public <T> RecoveryStrategy basicStrategy(@NonNull ScheduledExecutorService executor,
            @NonNull BackupStrategy<T> backupStrategy, @NonNull RecoveryConfig config, @NonNull Retryer<T> retryer) {
        return new BasicRecovery<>(backupStrategy, executor, config, retryer);
    }

    @Slf4j
    @Data
    @Builder(toBuilder = true)
    static class BasicRecovery<T> implements RecoveryStrategy {
        private final AtomicBoolean started = new AtomicBoolean();
        private final BackupStrategy<T> backupStrategy;
        private final ScheduledExecutorService executor;
        private final RecoveryConfig config;
        private final Retryer<T> retryer;

        @Override
        public void start() {
            if (!started.compareAndSet(false, true)) {
                log.warn("Service already started, skipping");
                return;
            }
            log.info("Starting recovery service with config: [{}]", this.config);
            executor.schedule(this::recover, config.getCheckFrequencyWithDataOnPreviousRun().getSeconds(), SECONDS);
        }

        @Override
        public void stop() {
            log.info("Stopping recovery service with config: [{}]", this.config);
            this.started.set(false);
        }

        @VisibleForTesting
        void recover() {
            if (!this.started.get()) {
                log.warn("Please start the service first");
                return;
            }
            Duration nextRecoveryDelay = config.getCheckFrequencyWithNoDataOnPreviousRun();
            try {
                List<DataPartition<T>> data = this.backupStrategy.restore();
                if (isEmpty(data)) {
                    log.debug("No data to be restored found, skipping");
                    return;
                }
                log.debug("Recovered {} partitions, processing them", data.size());
                for (DataPartition<T> partition : data) {
                    log.trace("Processing {} messages from {} partition", partition.getMessages().size(), partition);
                    CompletableFuture[] callbacks = partition.getMessages().stream().map(this.retryer::retry)
                            .toArray(CompletableFuture[]::new);
                    CompletableFuture.allOf(callbacks).whenCompleteAsync((o, e) -> {
                        if (e != null && (e instanceof BackupException || e.getCause() instanceof BackupException)) {
                            log.error("Cannot restore partition as it could not have been processed ", e);
                            return;
                        }
                        try {
                            this.backupStrategy.markAsRestored(partition);
                        } catch (Exception exception) {
                            log.error("Cannot mark partition {} as restored, it will be processed once again",
                                    partition, exception);
                        }
                    }, this.executor);
                }
                nextRecoveryDelay = config.getCheckFrequencyWithDataOnPreviousRun();
            } catch (Exception e) {
                log.error("Error while trying to recover backup files", e);
            } finally {
                executor.schedule(this::recover, nextRecoveryDelay.getSeconds(), SECONDS);
            }
        }
    }

    @Data
    public static class RecoveryConfig {
        private final Duration checkFrequencyWithNoDataOnPreviousRun;
        private final Duration checkFrequencyWithDataOnPreviousRun;
    }
}
