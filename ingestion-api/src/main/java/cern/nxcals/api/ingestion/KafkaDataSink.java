/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.api.ingestion;

import cern.cmw.data.Data;
import cern.nxcals.api.domain.RecordKey;
import com.google.common.annotations.VisibleForTesting;
import lombok.Builder;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.PartitionInfo;

import java.text.MessageFormat;
import java.util.Comparator;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * This class uses the @see Serializer to convert the @see {@link Data} into bytes to be sent to Kafka @see
 * {@link Publisher}.
 */
@Slf4j
class KafkaDataSink implements DataSink<RecordData, DefaultCallback> {
    private static final String RECORD_TOO_BIG_MSG_PATTERN = "Record for entity={0} with id={1,number,#} too big, current record size={2,number,#}, max allowed size={3,number,#}";
    private static final Result DUMMY_RESULT = new Result() {
    };
    private final KafkaSinkConfig config;
    private final Producer<byte[], byte[]> kafkaProducer;
    private final Function<RecordData, byte[]> serializer;
    private final PartitionInfo[] sortedPartitions;
    private final int availablePartitions;

    KafkaDataSink(@NonNull KafkaSinkConfig config, @NonNull Producer<byte[], byte[]> producer,
            @NonNull Function<RecordData, byte[]> serializer) {
        this.config = config;
        this.kafkaProducer = producer;
        this.serializer = serializer;
        this.sortedPartitions = getSortedPartitions(producer, config.getTopicName());
        int reservedPartitions = config.getReservedPartitionsSize();
        checkArgument(reservedPartitions >= 0 && reservedPartitions <= sortedPartitions.length,
                "Kafka reserved partitions size must not be greater than the number of partitions: "
                        + sortedPartitions.length + " of topic " + config.getTopicName());
        this.availablePartitions = sortedPartitions.length - reservedPartitions;
    }

    private static PartitionInfo[] getSortedPartitions(Producer<byte[], byte[]> producer, String topicName) {
        //@formatter:off
        return producer
                .partitionsFor(topicName)
                .stream()
                .sorted(Comparator.comparingInt(PartitionInfo::partition))
                .collect(Collectors.toList())
                .toArray(new PartitionInfo[] {});
        //@formatter:on
    }

    @Override
    public void close() {
        this.kafkaProducer.close();
    }

    @Override
    public void send(RecordData record, DefaultCallback callback) {
        Objects.requireNonNull(callback);
        try {

            byte[] key = toKeyBytes(record);
            byte[] value = this.serializer.apply(record);

            if (value.length > this.config.getMaxRecordSize()) {
                String msg = MessageFormat
                        .format(RECORD_TOO_BIG_MSG_PATTERN, record.getEntityKeyValues(), record.getEntityId(),
                                value.length, this.config.getMaxRecordSize());
                callback.accept(null, new RecordTooBigRuntimeException(msg));
                return;
            }
            sendToKafka(record.getEntityId(), key, value, getPartitionNumber(record),
                    (recordMetadata, exception) -> callback.accept(DUMMY_RESULT, exception));
        } catch (Exception exception) {
            callback.accept(null, exception);
        }
    }

    private byte[] toKeyBytes(RecordData record) {
        //there should be only one history entry here - the one related to the record timestamp, the schema should be taken from there
        return RecordKey
                .serialize(record.getSystemId(), record.getPartitionId(), record.getSchemaId(), record.getEntityId(),
                        record.getTimestamp());
    }

    /**
     * This generates the Kafka topic partition number based on NXCALS partition id + entity id.
     * It aims at using the same partition for data that are stored in the same directory to avoid
     * creating many files (as many as Kafka partitions used for a given directory in hdfs).
     * Partitions are sorted so getting by index is fine (and fast).
     * We could have just return the index but I left it in case we would like to be based on some other partition properties later on (replicas, leader, etc) (jwozniak)
     */
    @VisibleForTesting
    Integer getPartitionNumber(RecordData record) {
        Integer kafkaPartition = record.getKafkaPartition();
        if (kafkaPartition != null && kafkaPartition >= 0) {
            // make sure we do not exceed 0-based partition index
            return kafkaPartition % sortedPartitions.length;
        }
        return getDefaultKafkaPartition(record);
    }

    private int getDefaultKafkaPartition(RecordData record) {
        long pid = record.getPartitionId();
        long eid = record.getEntityId();
        int bucketSize = config.getBucketSize();
        int bucketCount = Math.max(1, availablePartitions / bucketSize);
        int index = (int) ((pid % bucketCount) * bucketSize + eid % bucketSize);
        return sortedPartitions[index].partition();
    }

    private void sendToKafka(long entityId, byte[] key, byte[] value, Integer partitionNumber,
            BiConsumer<RecordMetadata, Exception> callback) {
        log.trace("Sending to kafka entityId={}, size={}, kafkaPartition={}", entityId, value.length, partitionNumber);
        this.kafkaProducer
                .send(new ProducerRecord<>(config.getTopicName(), partitionNumber, key, value), callback::accept);
    }

    @lombok.Data
    @Builder(toBuilder = true)
    static class KafkaSinkConfig {
        private final String topicName;
        private final int maxRecordSize;
        private final int bucketSize;
        private final int reservedPartitionsSize;
    }
}
