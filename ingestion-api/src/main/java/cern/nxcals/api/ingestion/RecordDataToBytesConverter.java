package cern.nxcals.api.ingestion;

import cern.nxcals.api.domain.EntitySchema;
import cern.nxcals.common.avro.DefaultGenericRecordToBytesEncoder;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericRecord;

import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

import static cern.nxcals.api.converters.ImmutableDataToAvroConverter.createDataGenericRecord;
import static cern.nxcals.common.Schemas.ENTITY_ID;
import static cern.nxcals.common.Schemas.PARTITION_ID;
import static cern.nxcals.common.Schemas.SCHEMA_ID;
import static cern.nxcals.common.Schemas.SYSTEM_ID;
import static cern.nxcals.common.Schemas.TIMESTAMP;

class RecordDataToBytesConverter implements Function<RecordData, byte[]> {
    private final ConcurrentHashMap<Long, Schema> schemaCache = new ConcurrentHashMap<>();

    @Override
    public byte[] apply(RecordData record) {
        GenericRecord genericRecord = this.convert(record);
        return DefaultGenericRecordToBytesEncoder.convertToBytes(genericRecord);
    }

    private GenericRecord convert(RecordData record) {
        //there should be only one history entry here - the one related to the record timestamp, the schema should be taken from there

        EntitySchema schemaData = record.getSchemaData();
        Schema schema = this.createOrFindSchema(schemaData);
        GenericRecord genericRecord = createDataGenericRecord(record.getData(), schema);
        genericRecord.put(SYSTEM_ID.getFieldName(), record.getSystemId());
        genericRecord.put(ENTITY_ID.getFieldName(), record.getEntityId());
        genericRecord.put(PARTITION_ID.getFieldName(), record.getPartitionId());
        genericRecord.put(SCHEMA_ID.getFieldName(), schemaData.getId());
        genericRecord.put(TIMESTAMP.getFieldName(), System.currentTimeMillis() * 1_000_000); // we store in nanosecond
        // resolution to be
        // compatible with the data
        // timestamp
        return genericRecord;
    }

    /**
     * Creates the schema for the record based on the metadata. It assumes that the schema is correctly returned by the
     * schema provider.
     */
    private Schema createOrFindSchema(EntitySchema entitySchema) {
        return this.schemaCache.computeIfAbsent(entitySchema.getId(), schemaKey -> entitySchema.getSchema());
    }
}
