/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.ingestion;

import cern.cmw.data.Data;
import cern.cmw.datax.ImmutableData;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.KeyValues;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

import static cern.nxcals.api.ingestion.Publisher.Constants.MAX_FIELDS;
import static java.util.Objects.requireNonNull;

/**
 * Default implementation of {@link Publisher}. The responsibility of this class is to use the @see {@code conventer} to
 * convert the user data into @see {@link Data} and to obtain the necessary meta information for the record. Later on
 * the processing of the record is passed over to the @link Sink.
 */
@Slf4j
@RequiredArgsConstructor
class PublisherImpl<V> implements Publisher<V> {
    @NonNull
    private final PublisherHelper<V> helper;

    @Override
    public void close() {
        // nothing to be closed here
    }

    @Override
    public CompletableFuture<Result> publishAsync(V value, Executor executor) {
        requireNonNull(executor);
        // should this be validated in the caller thread or asynchronously?
        requireNonNull(value, "Input value must not be null");
        CompletableFuture<Result> result = new CompletableFuture<>();
        executor.execute(() -> {
            try {
                ImmutableData data = helper.getConverter().apply(value);
                if (data.size() > MAX_FIELDS) {
                    result.completeExceptionally(new IllegalRecordRuntimeException(
                            "Record has " + data.size() + " fields with limit of " + MAX_FIELDS
                                    + ". This large number of fields cannot be processes with Spark at extraction. Please "
                                    + "re-model your data."));
                    return;
                }

                KeyValues cachedRequest = helper.getEncoder().encodeEntityKeyValues(data);

                Long timestamp = helper.getEncoder().encodeTimeKeyValues(data);
                if (timestamp == null || timestamp <= 0) {
                    result.completeExceptionally(new IllegalRecordRuntimeException(
                            "Illegal record timestamp for entityKey=" + cachedRequest + " system id=" + helper
                                    .getSystemId() + " timestamp=" + timestamp));
                    return;
                }

                Entity entityData = helper.findEntity(data, cachedRequest, timestamp);

                helper.getDataSink().send(new RecordData(entityData, data, timestamp), (res, except) -> {
                    if (except != null) {
                        result.completeExceptionally(except);
                    } else {
                        result.complete(res);
                    }
                });
            } catch (Exception exception) {
                result.completeExceptionally(exception);
            }
        });
        return result;
    }
}
