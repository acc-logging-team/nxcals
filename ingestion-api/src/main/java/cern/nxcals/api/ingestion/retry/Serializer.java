package cern.nxcals.api.ingestion.retry;

import java.io.File;
import java.util.List;

public interface Serializer<T> {

    void writeTo(List<T> messages, File file);

    List<T> readFrom(File file);

    String getBackupFileSuffix();
}
