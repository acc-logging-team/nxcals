package cern.nxcals.api.ingestion;

/**
 * Default implementation of {@link Callback} interface.
 */
interface DefaultCallback extends Callback<Result, Exception> {
}
