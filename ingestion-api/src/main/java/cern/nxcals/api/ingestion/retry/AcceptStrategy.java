package cern.nxcals.api.ingestion.retry;

import java.util.function.Predicate;

/**
 * Defines strategy to be used by the publication retryer to decide whether a given exception, obtained while sending
 * a message to NXCALS, can be re-tried.
 */
public interface AcceptStrategy extends Predicate<Throwable> {
}
