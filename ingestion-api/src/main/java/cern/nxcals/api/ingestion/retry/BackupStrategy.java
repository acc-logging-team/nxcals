package cern.nxcals.api.ingestion.retry;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * Backup Strategy for the rejected messages. It takes care of storing messages outside of the publisher jvm as
 * well as restoring them for further processing once the underlying problems are solved. All the backup specific
 * details such external storage location etc. should be left to the implementation.
 *
 * @param <T> - the type of the messages supported by the strategy
 */
public interface BackupStrategy<T> {

    /**
     * Backups the given message
     * @param message - to be stored in the backup
     * @return
     */
    CompletableFuture<Void> backup(T message);

    /**
     * Restores data from the backup
     * @return list of restored data partitions
     */
    List<DataPartition<T>> restore();

    /**
     * Marks a given partition as restored i.e. successfully send to NXCALS
     * @param partition - restored partition
     */
    void markAsRestored(DataPartition<T> partition);

    @Data
    @Builder
    @ToString(onlyExplicitlyIncluded = true)
    class DataPartition<T> {
        @ToString.Include
        private final String id;
        private final List<T> messages;
    }

}
