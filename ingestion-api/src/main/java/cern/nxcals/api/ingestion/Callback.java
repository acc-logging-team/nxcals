package cern.nxcals.api.ingestion;

import java.util.function.BiConsumer;

/**
 * A callback interface that one can implement to allow code to execute when the request is complete.
 * Exactly one of the arguments of the {@link Callback::accept} method will be non-null.
 */
interface Callback<R extends Result, E extends Exception> extends BiConsumer<R, E> {
}
