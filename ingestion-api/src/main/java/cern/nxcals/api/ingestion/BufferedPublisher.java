package cern.nxcals.api.ingestion;

import cern.cmw.datax.ImmutableData;
import cern.nxcals.api.domain.Entity;
import com.google.common.primitives.Ints;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.time.Clock;
import java.time.Duration;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Delayed;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

@Slf4j
class BufferedPublisher<V> implements Publisher<V> {
    private final BufferWriter<V> writer;
    private final BufferReader reader;

    BufferedPublisher(BufferWriter<V> writer, BufferReader reader, Duration flushingPeriod) {
        this.writer = writer;
        this.reader = reader;
        this.reader.start(flushingPeriod);
    }

    @Override
    public CompletableFuture<Result> publishAsync(@NonNull V value, @NonNull Executor executor) {
        CompletableFuture<Result> result = new CompletableFuture<>();
        executor.execute(() -> {
            try {
                this.writer.put(value, result, executor);
            } catch (Exception exception) {
                result.completeExceptionally(exception);
            }
        });
        return result;
    }

    @Override
    public void close() throws IOException {
        this.reader.close();
    }

    @Data
    @Builder(toBuilder = true)
    static class EntityData {
        @NonNull
        private final String entityKey;
        @NonNull
        private final String partitionKey;
        @NonNull
        private final String schemaKey;
        @NonNull
        private final Long time;
        @NonNull
        private final Supplier<Entity> entityMaker;
    }

    @Data
    @Builder(toBuilder = true)
    @ToString(onlyExplicitlyIncluded = true)
    static class DelayedMessage implements Delayed {
        @NonNull
        private final CompletableFuture<Result> result;
        @NonNull
        private final Clock clock;
        @NonNull
        private final ImmutableData data;
        private final long expiredAt;
        @NonNull
        private final Executor executor;
        @NonNull
        @ToString.Include
        private final EntityData entityData;

        @Override
        public long getDelay(TimeUnit unit) {
            long diff = expiredAt - clock.millis();
            return unit.convert(diff, MILLISECONDS);
        }

        @Override
        public int compareTo(@NonNull Delayed other) {
            return Ints.saturatedCast(this.expiredAt - ((DelayedMessage) other).expiredAt);
        }

        void completeExceptionally(Throwable t) {
            this.result.completeExceptionally(t);
        }

        void complete(Result result) {
            this.result.complete(result);
        }
    }
}
