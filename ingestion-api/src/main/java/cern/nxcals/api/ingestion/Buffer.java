package cern.nxcals.api.ingestion;

import java.util.Collection;
import java.util.concurrent.Delayed;

interface Buffer<T extends Delayed> {

    void push(T element);

    void drainTo(Collection<T> dest);
}
