package cern.nxcals.api.ingestion;

import cern.cmw.datax.ImmutableData;
import cern.nxcals.api.domain.KeyValues;
import cern.nxcals.common.SystemFields;
import cern.nxcals.common.avro.DataEncoder;
import cern.nxcals.internal.extraction.metadata.InternalEntityService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Map;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PublisherHelperTest {

    @Test
    void shouldFindEntityByIds() {
        // with
        long entityId = 1L;
        long partitionId = 1L;
        long systemId = 1L;
        long timestamp = 1L;
        String schema = "{}";
        ImmutableData data = ImmutableData.builder().add(SystemFields.NXC_ENTITY_ID.getValue(), entityId)
                .add(SystemFields.NXC_PARTITION_ID.getValue(), partitionId).build();

        InternalEntityService serviceMock = mock(InternalEntityService.class);
        DataEncoder encoderMock = mock(DataEncoder.class);
        when(encoderMock.encodeRecordFieldDefinitions(any())).thenReturn(schema);
        DataSink dataSinkMock = mock(DataSink.class);

        PublisherHelper<ImmutableData> helper = new PublisherHelper<>(systemId, Function.identity(), serviceMock,
                encoderMock,
                dataSinkMock);

        // when
        helper.findEntity(data, null, timestamp);

        // then

        // should use encoder only for record filed, since for other should use ids
        verify(encoderMock, never()).encodeEntityKeyValues(any());
        verify(encoderMock, never()).encodePartitionKeyValues(any());
        verify(encoderMock, never()).encodeTimeKeyValues(any());
        verify(encoderMock, times(1)).encodeRecordFieldDefinitions(any());

        // and call service using ids
        verify(serviceMock, times(1)).findOrCreateEntityFor(anyLong(), anyLong(), anyLong(), anyString(), anyLong());
    }

    @Test
    void shouldFindEntityByKeyValues() {
        // with
        KeyValues entityKeys = new KeyValues(0, Map.of("device", "DEV"));
        KeyValues partitionKeys = new KeyValues(0, Map.of("partition", "DEV"));
        long systemId = 1L;
        long timestamp = 1L;
        String schema = "{}";
        ImmutableData data = ImmutableData.builder().build();

        InternalEntityService serviceMock = mock(InternalEntityService.class);
        DataEncoder encoderMock = mock(DataEncoder.class);
        when(encoderMock.encodeRecordFieldDefinitions(any())).thenReturn(schema);
        when(encoderMock.encodePartitionKeyValues(any())).thenReturn(partitionKeys);
        DataSink dataSinkMock = mock(DataSink.class);

        PublisherHelper<ImmutableData> helper = new PublisherHelper<>(systemId, Function.identity(), serviceMock,
                encoderMock,
                dataSinkMock);

        // when
        helper.findEntity(data, entityKeys, timestamp);

        // then

        verify(encoderMock, never()).encodeEntityKeyValues(any());
        verify(encoderMock, never()).encodeTimeKeyValues(any());
        verify(encoderMock, times(1)).encodeRecordFieldDefinitions(any());
        verify(encoderMock, times(1)).encodePartitionKeyValues(any());

        // and call service using ids
        verify(serviceMock, times(1)).findOrCreateEntityFor(anyLong(), any(), any(), anyString(), anyLong());
    }

    @Test
    void shouldThrowWhenEntityIdExistAndPartitionIdIsNull() {
        // with
        long entityId = 1L;
        KeyValues entityKeys = new KeyValues(0, Map.of("device", "DEV"));
        long systemId = 1L;
        long timestamp = 1L;
        String schema = "{}";
        ImmutableData data = ImmutableData.builder().add(SystemFields.NXC_ENTITY_ID.getValue(), entityId).build();

        InternalEntityService serviceMock = mock(InternalEntityService.class);
        DataEncoder encoderMock = mock(DataEncoder.class);
        DataSink dataSinkMock = mock(DataSink.class);

        PublisherHelper<ImmutableData> helper = new PublisherHelper<>(systemId, Function.identity(), serviceMock,
                encoderMock,
                dataSinkMock);

        // when
        assertThrows(IllegalRecordRuntimeException.class, () -> helper.findEntity(data, entityKeys, timestamp));
    }
}
