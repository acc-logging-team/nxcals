package cern.nxcals.api.ingestion;

import org.apache.avro.Schema;

/**
 * Created by jwozniak on 21/12/16.
 */
public class Base {
    public static final Schema entityKeyDefSchema = new Schema.Parser().parse(
            "{\"type\":\"record\",\"name\":\"CmwKey\",\"namespace\":\"cern.cals\",\"fields\":[{\"name\":\"device\",\"type\":\"string\"},{\"name\":\"property\",\"type\":\"string\"}]}");

    public static final Schema partitionKeyDefSchema = new Schema.Parser().parse(
            "{\"type\":\"record\",\"name\":\"CmwPartition\",\"namespace\":\"cern.cals\",\"fields\":[{\"name\":\"class\",\"type\":\"string\"},{\"name\":\"property\",\"type\":\"string\"}]}");

    public static final Schema recordVersionKeyDefSchemaNullable = new Schema.Parser()
            .parse("{\"type\":\"record\",\"name\":\"RecordVersion\",\"namespace\":\"cern.cals\",\"fields\":\n"
                    + "[{\"name\":\"record_version1\",\"type\":[\"string\",\"null\"]},{\"name\":\"record_version2\",\"type\":[\"int\",\"null\"]}]}");

    public static final Schema recordVersionKeyDefSchema = new Schema.Parser().parse(
            "{\"type\":\"record\",\"name\":\"RecordVersion\",\"namespace\":\"cern.cals\",\"fields\":[{\"name\":\"record_version1\",\"type\":\"string\"},{\"name\":\"record_version2\",\"type\":\"int\"}]}");

    public static final Schema timeKeyDefSchema = new Schema.Parser().parse(
            "{\"type\":\"record\",\"name\":\"CmwPT\",\"namespace\":\"cern.cals\",\"fields\":[{\"name\":\"_p_t_\",\"type\":\"long\"}]}");
}
