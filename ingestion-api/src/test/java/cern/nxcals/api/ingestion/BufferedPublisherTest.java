package cern.nxcals.api.ingestion;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.time.Duration;
import java.util.concurrent.CompletableFuture;

import static cern.nxcals.api.ingestion.Publisher.Constants.CURRENT_THREAD_EXECUTOR;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

public class BufferedPublisherTest {
    private BufferWriter<Integer> writerMock;
    private BufferedPublisher<Integer> instance;

    @BeforeEach
    public void setUp() {
        this.writerMock = mock(BufferWriter.class);
        this.instance = new BufferedPublisher<>(writerMock, mock(BufferReader.class), Duration.ofMillis(1L));

    }

    @Test
    public void shouldCompleteExceptionallyOnWriterError() {
        ArgumentCaptor<CompletableFuture<Result>> resultCapture = ArgumentCaptor.forClass(CompletableFuture.class);
        IllegalArgumentException e = new IllegalArgumentException();
        int value = 1;
        doThrow(e).when(this.writerMock).put(eq(value), any(), eq(CURRENT_THREAD_EXECUTOR));
        this.instance.publishAsync(value, CURRENT_THREAD_EXECUTOR);
        Mockito.verify(this.writerMock, times(1)).put(eq(value), resultCapture.capture(), eq(CURRENT_THREAD_EXECUTOR));
        CompletableFuture<Result> result = resultCapture.getValue();
        assertNotNull(result);
        result.whenComplete((o, ee) -> assertEquals(e, ee));
    }

}
