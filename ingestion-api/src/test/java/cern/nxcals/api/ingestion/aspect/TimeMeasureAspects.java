/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.api.ingestion.aspect;

import cern.nxcals.common.utils.TimeMeasure;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Aspect;

/**
 * In order to time measure the calls please enable AspectJ on a project.
 */
@Aspect
public class TimeMeasureAspects extends TimeMeasure {
    //@Around(
    //"execution(* cern.nxcals.client.KafkaDataSink.*(..)) ||"
    //"execution(* cern.nxcals.client.PublisherImpl.*(..))"
    //+ "execution(* DataServiceEncoderImpl.encode*(..)) ||"
    //+ "execution(* DataServiceEncoderImpl.apply(..))"
    //)
    public Object timeMeasure(ProceedingJoinPoint pjp) throws Throwable {
        return doTimeMeasure(pjp);
    }
}
