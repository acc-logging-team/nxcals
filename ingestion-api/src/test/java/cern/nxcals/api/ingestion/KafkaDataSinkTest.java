/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.api.ingestion;

import cern.cmw.datax.ImmutableData;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.EntityHistory;
import cern.nxcals.api.domain.EntitySchema;
import cern.nxcals.api.domain.Partition;
import cern.nxcals.api.domain.RecordKey;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.ingestion.KafkaDataSink.KafkaSinkConfig;
import cern.nxcals.common.utils.ReflectionUtils;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSortedSet;
import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.PartitionInfo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.SortedSet;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by jwozniak on 19/12/16.
 */
@ExtendWith(MockitoExtension.class)
public class KafkaDataSinkTest {
    private static final byte[] VALUE = new byte[0];
    private static final int MAX_RECORD_SIZE = 2 * 1024 * 1024;
    static final Schema SCHEMA = SchemaBuilder.record("schema1").fields().name("schema1").type().stringType()
            .noDefault().endRecord();
    private static final int BUCKET_SIZE = 5;
    @Mock
    private Producer<byte[], byte[]> producer;
    private static final String topicName = "TEST-TOPIC";
    private static final int NB_OF_PARTITIONS = 20;
    private static final KafkaSinkConfig config = new KafkaSinkConfig(topicName, MAX_RECORD_SIZE, BUCKET_SIZE, 10);

    private Entity entityData;

    @Mock
    private ImmutableData immutableData;

    @BeforeEach
    public void setup() {
        List<PartitionInfo> parts = IntStream.rangeClosed(0, NB_OF_PARTITIONS - 1).mapToObj(this::toMockPartition)
                .collect(Collectors.toList());

        final EntitySchema entitySchema = EntitySchema.builder().schemaJson(SCHEMA.toString()).build();
        final SystemSpec system = SystemSpec.builder().name("system").entityKeyDefinitions("")
                .partitionKeyDefinitions("").timeKeyDefinitions("").build();
        final Partition partition = ReflectionUtils.builderInstance(Partition.InnerBuilder.class).id(10L)
                .systemSpec(system).keyValues(new HashMap<>()).build();
        Entity entity = ReflectionUtils.builderInstance(Entity.InnerBuilder.class).id(1)
                .entityKeyValues(ImmutableMap.of("entityKeyValues", "entityKeyValues")).systemSpec(system)
                .partition(partition).build();
        SortedSet<EntityHistory> history = ImmutableSortedSet
                .of(ReflectionUtils.builderInstance(EntityHistory.InnerBuilder.class).id(10L)
                        .validity(TimeWindow.infinite()).partition(partition).entitySchema(entitySchema).entity(entity)
                        .build());
        entityData = entity.toBuilder().entityHistory(history).build();
        when(producer.partitionsFor(topicName)).thenReturn(parts);
    }

    private PartitionInfo toMockPartition(int number) {
        PartitionInfo ret = Mockito.mock(PartitionInfo.class);
        when(ret.partition()).thenReturn(number);
        return ret;
    }

    @Test
    public void shouldNotPublishTooBigRecord() {
        KafkaDataSink kafkaSink = new KafkaDataSink(config, producer,
                (RecordData data) -> new byte[MAX_RECORD_SIZE + 1]);

        RecordData recordData = new RecordData(entityData, immutableData, 100);

        kafkaSink.send(recordData, (r, e) -> {
            Assertions.assertNotNull(e);
            Assertions.assertNull(r);
            Assertions.assertTrue(e instanceof RecordTooBigRuntimeException);
        });
    }

    @Test
    public void shouldPublish() {
        KafkaDataSink kafkaSink = new KafkaDataSink(config, producer, (RecordData data) -> VALUE);

        //given
        RecordData data = new RecordData(entityData, immutableData, 100);

        when(producer.send(any(ProducerRecord.class), any(Callback.class))).thenAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                ProducerRecord<byte[], byte[]> record = (ProducerRecord<byte[], byte[]>) args[0];
                Callback callback = (Callback) args[1];
                assertEquals(topicName, record.topic());
                assertEquals(VALUE, record.value());

                RecordKey key = RecordKey.deserialize(record.key());
                assertEquals(1L, key.getEntityId());
                assertEquals(10L, key.getPartitionId());
                callback.onCompletion(null, null);
                return null;
            }
        });
        //when
        kafkaSink.send(data, (r, e) -> {
            Assertions.assertNotNull(r);
            Assertions.assertNull(e);
        });

        //then
        verify(producer, times(1)).send(any(ProducerRecord.class), any(Callback.class));
    }

    @Test
    public void shouldCloseProducerOnClose() throws IOException {
        new KafkaDataSink(config, producer, (RecordData data) -> new byte[MAX_RECORD_SIZE + 1]).close();
        verify(producer, times(1)).close();
    }

    @Test
    public void shouldUseExternalPartition() {
        RecordData record = Mockito.mock(RecordData.class);
        int kafkaPartition = NB_OF_PARTITIONS - 1;
        when(record.getKafkaPartition()).thenReturn(kafkaPartition);

        KafkaDataSink kafkaSink = new KafkaDataSink(config, producer, (RecordData data) -> VALUE);
        int partition = kafkaSink.getPartitionNumber(record);

        assertEquals(kafkaPartition, partition);
    }

    @Test
    public void shouldUseExternalPartitionWithoutExceedingKafkaPartitions() {
        RecordData record = Mockito.mock(RecordData.class);
        int kafkaPartition = NB_OF_PARTITIONS * 2;
        when(record.getKafkaPartition()).thenReturn(kafkaPartition);

        KafkaDataSink kafkaSink = new KafkaDataSink(config, producer, (RecordData data) -> VALUE);
        int partition = kafkaSink.getPartitionNumber(record);

        assertEquals(0, partition);
    }

    @Test
    public void shouldNotUseWrongExternalPartition() {
        RecordData record = Mockito.mock(RecordData.class);
        when(record.getKafkaPartition()).thenReturn(-1);
        when(record.getEntityId()).thenReturn(2L);
        when(record.getPartitionId()).thenReturn(2L);

        KafkaDataSink kafkaSink = new KafkaDataSink(config, producer, (RecordData data) -> VALUE);
        int partition = kafkaSink.getPartitionNumber(record);

        assertEquals(2, partition);
    }

    @Test
    public void shouldUseDefaultPartitionAlgorithm() {
        RecordData record = Mockito.mock(RecordData.class);
        when(record.getKafkaPartition()).thenReturn(null);
        when(record.getEntityId()).thenReturn(2L);
        when(record.getPartitionId()).thenReturn(2L);

        KafkaDataSink kafkaSink = new KafkaDataSink(config.toBuilder().reservedPartitionsSize(0).build(), producer,
                (RecordData data) -> VALUE);
        int partition = kafkaSink.getPartitionNumber(record);

        assertEquals(12, partition);
    }

}
