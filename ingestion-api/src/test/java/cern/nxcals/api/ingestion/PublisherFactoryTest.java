package cern.nxcals.api.ingestion;

import cern.cmw.datax.ImmutableData;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.api.ingestion.PublisherFactory.Closer;
import cern.nxcals.api.ingestion.PublisherFactory.WrappedPublisher;
import cern.nxcals.internal.extraction.metadata.InternalEntityService;
import org.apache.kafka.clients.producer.Producer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.Closeable;
import java.io.IOException;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutorService;

import static cern.nxcals.api.ingestion.Publisher.Constants.CURRENT_THREAD_EXECUTOR;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PublisherFactoryTest extends Base {

    private PublisherFactory factory;
    @Mock
    private InternalEntityService entityService;
    @Mock
    private SystemSpecService systemService;

    @Mock
    private Producer<byte[], byte[]> kafkaProducer;

    @BeforeEach
    public void setUp() {
        factory = new PublisherFactory(entityService, systemService, () -> kafkaProducer);
    }

    @Test
    public void shouldCreatePublisher() throws Exception {
        when(systemService.findByName("TEST")).thenReturn(Optional.of(
                SystemSpec.builder().name("TEST").timeKeyDefinitions(timeKeyDefSchema.toString())
                        .recordVersionKeyDefinitions(recordVersionKeyDefSchema.toString())
                        .entityKeyDefinitions(entityKeyDefSchema.toString())
                        .partitionKeyDefinitions(partitionKeyDefSchema.toString()).build()));

        Publisher<Map<String, Object>> publisher = factory
                .createPublisher("TEST", v -> ImmutableData.builder().build());
        assertNotNull(publisher);
    }

    @Test
    public void shouldNotCreatePublisherWithNullSystem() throws Exception {
        assertThrows(NullPointerException.class,
                () -> factory.createPublisher(null, v -> ImmutableData.builder().build()));
    }

    @Test
    public void shouldNotCreatePublisherWithNullFunction() throws Exception {
        assertThrows(NullPointerException.class,
                () -> factory.createPublisher("TEST", null));
    }

    @Test
    public void shouldNotCreatePublisherWithUnknownSystem() throws Exception {
        when(systemService.findByName("UNKNOWN")).thenReturn(Optional.empty());
        assertThrows(IllegalArgumentException.class,
                () -> factory.createPublisher("UNKNOWN", v -> ImmutableData.builder().build())
        );
    }

    @Test
    public void shouldGoViaDelegate() throws IOException {
        // given
        int value = 1;
        Publisher<Integer> delegateMock = mock(Publisher.class);
        ExecutorService executorMock = mock(ExecutorService.class);
        Closeable closeableMock = mock(Closeable.class);
        Closer closer = new Closer();
        closer.register(executorMock);
        closer.register(closeableMock);

        WrappedPublisher<Integer> wrappedPublisher = new WrappedPublisher<>(delegateMock, closer);

        // when
        wrappedPublisher.publishAsync(value, CURRENT_THREAD_EXECUTOR);
        wrappedPublisher.close();

        // then
        verify(delegateMock, times(1)).publishAsync(eq(value), eq(CURRENT_THREAD_EXECUTOR));
        verify(delegateMock, times(1)).close();
        verify(executorMock, times(1)).shutdown();
        verify(closeableMock, times(1)).close();
    }
}
