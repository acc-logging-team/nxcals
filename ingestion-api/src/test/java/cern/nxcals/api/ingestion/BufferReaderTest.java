package cern.nxcals.api.ingestion;

import cern.cmw.datax.ImmutableData;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.ingestion.BufferedPublisher.DelayedMessage;
import cern.nxcals.api.ingestion.BufferedPublisher.EntityData;
import com.google.common.collect.ImmutableList;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;

import static cern.nxcals.api.ingestion.Publisher.Constants.CURRENT_THREAD_EXECUTOR;
import static java.util.concurrent.ThreadLocalRandom.current;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toList;
import static java.util.stream.LongStream.rangeClosed;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@Slf4j
@TestInstance(PER_CLASS)
public class BufferReaderTest {
    private Clock clock = Clock.fixed(Instant.ofEpochMilli(EXPIRED), ZoneOffset.systemDefault());
    private static final ExecutorService EXECUTOR = Executors.newFixedThreadPool(10);
    private static final Result DUMMY_RESULT = new Result() {
    };
    private static final long EXPIRED = 1L;
    private Buffer<DelayedMessage> buffer;
    private TestSink sinkMock;
    private BufferReader instance;

    @BeforeEach
    public void setUp() {
        this.buffer = new DelayedQueueBuffer<>();
        this.sinkMock = Mockito.mock(TestSink.class);
        this.instance = new BufferReader(sinkMock, buffer, 5, CURRENT_THREAD_EXECUTOR);
    }

    @AfterEach
    public void tearDown() throws Exception {
        this.instance.close();
    }

    @AfterAll
    public static void afterClass() {
        EXECUTOR.shutdown();
    }

    @Test
    public void shouldNotProcessEmptyBuffer() {
        this.instance.flush();
        verify(this.sinkMock, times(0)).send(any(), any());
    }

    @Test
    public void shouldNotProcessBufferWithNoExpiredMessages() {
        DelayedMessage msg = message(1L, 1L, 1L, 2L, 2L, CURRENT_THREAD_EXECUTOR);
        this.buffer.push(msg);
        this.instance.flush();
        verify(this.sinkMock, times(0)).send(any(), any());
    }

    @Test
    public void shouldCompleteExceptionallyWhenCannotCreateRecord() {
        // given
        IllegalArgumentException e = new IllegalArgumentException();
        DelayedMessage msg = message(1L, 1L, 1L, 1L, 2L, CURRENT_THREAD_EXECUTOR);
        DelayedMessage throwingMsg = msg.toBuilder().entityData(msg.getEntityData().toBuilder().entityMaker(() -> {
            throw e;
        }).build()).build();
        this.buffer.push(throwingMsg);

        // when
        this.instance.flush();

        // then
        verify(this.sinkMock, times(0)).send(any(), any());
        CompletableFuture<Result> result = throwingMsg.getResult();
        assertTrue(result.isCompletedExceptionally());
        result.whenComplete((o, ee) -> assertNotNull(ee));
        result.whenComplete((o, ee) -> assertEquals(e, ee));
    }

    @Test
    public void shouldCompleteExceptionallyWhenSinkThrows() {
        // given
        DelayedMessage msg = message(1L, 1L, 1L, 1L, 2L, CURRENT_THREAD_EXECUTOR);
        this.buffer.push(msg);
        IllegalStateException e = new IllegalStateException();
        doThrow(e).when(this.sinkMock).send(any(), any());

        // when
        this.instance.flush();

        // then
        verify(this.sinkMock, times(1)).send(any(), any());
        CompletableFuture<Result> result = msg.getResult();
        assertTrue(result.isCompletedExceptionally());
        result.whenComplete((o, ee) -> assertNotNull(ee));
        result.whenComplete((o, ee) -> assertEquals(e, ee));
    }

    @Test
    public void shouldCompleteExceptionallyWhenProcessorRejecting() {
        // given
        Executor executorMock = mock(Executor.class);
        this.instance = new BufferReader(sinkMock, buffer, 5, executorMock);
        DelayedMessage msg = message(1L, 1L, 1L, 1L, 2L, CURRENT_THREAD_EXECUTOR);
        this.buffer.push(msg);
        RejectedExecutionException e = new RejectedExecutionException();
        doThrow(e).when(executorMock).execute(any());

        // when
        this.instance.flush();

        // then
        CompletableFuture<Result> result = msg.getResult();
        assertTrue(result.isCompletedExceptionally());
        result.whenComplete((o, ee) -> assertNotNull(ee));
        result.whenComplete((o, ee) -> assertEquals(e, ee));
    }

    @Test
    public void shouldCompleteExceptionallyOnErrorWhileProcessing() {
        // given
        DelayedMessage msg = message(1L, 1L, 1L, 1L, 2L, CURRENT_THREAD_EXECUTOR);
        DelayedMessage failedMsg = mock(DelayedMessage.class, RETURNS_DEEP_STUBS);
        when(failedMsg.getEntityData().getEntityKey()).thenReturn("1");
        when(failedMsg.getResult()).thenReturn(new CompletableFuture<>());
        doAnswer(i -> {
            Throwable t = i.getArgument(0);
            failedMsg.getResult().completeExceptionally(t);
            return null;
        }).when(failedMsg).completeExceptionally(any());

        IllegalStateException e = new IllegalStateException();
        when(failedMsg.getEntityData().getTime()).thenThrow(e);

        this.buffer.push(msg);
        this.buffer.push(failedMsg);

        // when
        this.instance.flush();

        // then
        CompletableFuture<Result> result = failedMsg.getResult();
        assertTrue(result.isCompletedExceptionally());
        result.whenComplete((o, ee) -> assertNotNull(ee));
        result.whenComplete((o, ee) -> assertEquals(e, ee));
    }

    @ParameterizedTest
    @MethodSource("sequential")
    public void shouldProcessBufferSequentially(TestData testData) {
        // given
        List<DelayedMessage> messages = testData.getInput();
        messages.forEach(this.buffer::push);
        ArgumentCaptor<RecordData> recordCapture = ArgumentCaptor.forClass(RecordData.class);
        ArgumentCaptor<DefaultCallback> callbackCapture = ArgumentCaptor.forClass(DefaultCallback.class);
        // when
        this.instance.flush();
        // then
        verify(this.sinkMock, times(messages.size())).send(recordCapture.capture(), callbackCapture.capture());
        List<RecordData> records = recordCapture.getAllValues();
        assertEquals(messages.size(), records.size());
        callbackCapture.getAllValues().forEach(this::completeWithDummy);
        assertTrue(messages.stream().allMatch(this::completed));
        this.verifyRecords(testData, records, false);
    }

    @ParameterizedTest
    @MethodSource("parallel")
    public void shouldProcessBufferConcurrently(TestData testData) {
        // given
        List<DelayedMessage> messages = testData.getInput();
        messages.forEach(this.buffer::push);
        List<RecordData> records = Collections.synchronizedList(new ArrayList<>());
        doAnswer(i -> {
            RecordData data = i.getArgument(0);
            DefaultCallback callback = i.getArgument(1);
            records.add(data);
            callback.accept(DUMMY_RESULT, null);
            return null;
        }).when(this.sinkMock).send(any(), any());
        // when
        this.instance.flush();
        // wait for all message to have been sent
        List<CompletableFuture<Result>> futures = messages.stream().map(DelayedMessage::getResult).collect(toList());
        CompletableFuture.allOf(futures.toArray(new CompletableFuture[]{})).join();

        // then
        verify(this.sinkMock, times(messages.size())).send(any(), any());
        assertEquals(messages.size(), records.size());
        assertTrue(messages.stream().allMatch(this::completed));
        // we verify only the first message as the order of the rest is unpredictable
        this.verifyRecords(testData, records, true);
    }

    private void verifyRecords(TestData testData, List<RecordData> records, boolean firstMessageOnly) {
        Map<Long, List<RecordData>> recordsByEntity = records.stream().collect(groupingBy(RecordData::getEntityId));
        testData.getOutput().forEach((eId, expectedRecords) -> {
            List<RecordData> entityRecords = recordsByEntity.get(eId);
            assertNotNull(entityRecords);
            assertEquals(expectedRecords.size(), entityRecords.size());
            int count = firstMessageOnly ? 1 : entityRecords.size();
            for (int i = 0; i < count; i++) {
                RecordData record = entityRecords.get(i);
                MessageData expected = expectedRecords.get(i);
                assertEquals(expected.getPartitionId(), record.getPartitionId());
                assertEquals(expected.getSchemaId(), record.getSchemaId());
                assertEquals(expected.getStamp(), record.getTimestamp());
            }
        });
    }

    private boolean completed(DelayedMessage m) {
        return m.getResult().join() == DUMMY_RESULT;
    }

    private void completeWithDummy(DefaultCallback c) {
        c.accept(DUMMY_RESULT, null);
    }

    private Object[][] sequential() {
        return testData(CURRENT_THREAD_EXECUTOR);
    }

    private Object[][] parallel() {
        return testData(EXECUTOR);
    }

    private Object[][] testData(Executor executor) {
        int count = (current().nextInt(100) + 10) * 2;
        // @formatter:off
        TestData s1 = new TestData(
                rangeClosed(1, count).mapToObj(i -> message(1L, 1L, 1L, count + 1 - i, executor)).collect(toList()),
                rangeClosed(1, count).mapToObj(i -> Pair.of(1L, messageData(1L, 1L, i)))
                                    .collect(groupingBy(Pair::getLeft, mapping(Pair::getRight, toList())))
            );
        TestData s2 = new TestData(
                rangeClosed(1, count).mapToObj(i -> message(i % 2, 1L, 1L, count + 1 - i, executor)).collect(toList()),
                rangeClosed(1, count).mapToObj(i -> Pair.of((i + 1) % 2, messageData(1L, 1L, i)))
                                    .collect(groupingBy(Pair::getLeft, mapping(Pair::getRight, toList())))
        );
        TestData s3 = new TestData(
                rangeClosed(1, count).mapToObj(i -> message(1L, i % 2, 1L, count + 1 - i, executor)).collect(toList()),
                rangeClosed(1, count).mapToObj(i -> Pair.of(1L, messageData((i + 1) % 2, 1L, i)))
                                    .collect(groupingBy(Pair::getLeft, mapping(Pair::getRight, toList())))
        );
        TestData s4 = new TestData(
                rangeClosed(1, count).mapToObj(i -> message(1L, 1L, i % 2, count + 1 - i, executor)).collect(toList()),
                rangeClosed(1, count).mapToObj(i -> Pair.of(1L, messageData(1L, (i + 1) % 2,  i)))
                                    .collect(groupingBy(Pair::getLeft, mapping(Pair::getRight, toList())))
        );
        TestData s5 = new TestData(
                rangeClosed(1, count).mapToObj(i -> message(1L, i % 2, i % 2, count + 1 - i, executor)).collect(toList()),
                rangeClosed(1, count).mapToObj(i -> Pair.of(1L, messageData((i + 1) % 2, (i + 1) % 2, i)))
                                    .collect(groupingBy(Pair::getLeft, mapping(Pair::getRight, toList())))
        );
        // @formatter:on
        return ImmutableList.of(s1, s2, s3, s4, s5).stream().map(Collections::singleton).map(Set::toArray)
                .toArray(Object[][]::new);
    }

    private DelayedMessage message(long eId, long pId, long sId, long stamp, Executor executor) {
        return message(eId, pId, sId, EXPIRED, stamp, executor);
    }

    private DelayedMessage message(long eId, long pId, long sId, long expired, long stamp, Executor executor) {
        return message(new CompletableFuture<>(), Triple.of(eId, pId, sId), expired, stamp, executor);
    }

    private DelayedMessage message(CompletableFuture<Result> result, Triple<Long, Long, Long> eps, long expired,
            long stamp, Executor executor) {
        Entity entityMock = mock(Entity.class, RETURNS_DEEP_STUBS);
        when(entityMock.getId()).thenReturn(eps.getLeft());
        when(entityMock.getFirstEntityHistory().getPartition().getId()).thenReturn(eps.getMiddle());
        when(entityMock.getFirstEntityHistory().getEntitySchema().getId()).thenReturn(eps.getRight());
        return new DelayedMessage(result, this.clock, ImmutableData.builder().build(), expired, executor,
                new EntityData(eps.getLeft().toString(), eps.getMiddle().toString(), eps.getRight().toString(), stamp,
                        () -> entityMock));
    }

    private MessageData messageData(long pId, long sId, long stamp) {
        return new MessageData(pId, sId, stamp);
    }

    @Data
    private static class TestData {
        private final List<DelayedMessage> input;
        private final Map<Long, List<MessageData>> output;
    }

    @Data
    private static class MessageData {
        private final long partitionId;
        private final long schemaId;
        private final long stamp;
    }

    private interface TestSink extends DataSink<RecordData, DefaultCallback> {

    }
}
