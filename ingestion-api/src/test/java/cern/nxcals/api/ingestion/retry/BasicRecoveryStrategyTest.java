package cern.nxcals.api.ingestion.retry;

import cern.nxcals.api.ingestion.retry.BackupStrategy.DataPartition;
import cern.nxcals.api.ingestion.retry.PublicationRetryerTest.BackupStrategyMock;
import cern.nxcals.api.ingestion.retry.PublicationRetryerTest.MockScheduledExecutor;
import cern.nxcals.api.ingestion.retry.RecoveryStrategies.BasicRecovery;
import cern.nxcals.api.ingestion.retry.RecoveryStrategies.RecoveryConfig;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

import static java.time.Duration.ofSeconds;
import static java.util.Collections.emptyList;
import static java.util.concurrent.TimeUnit.SECONDS;
import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class BasicRecoveryStrategyTest {
    private final RecoveryConfig recoveryConfig = new RecoveryConfig(ofSeconds(1), ofSeconds(2));
    private BackupStrategy<Integer> backupStrategyMock;
    private ScheduledExecutorService executorMock;
    private Retryer<Integer> retryerMock;
    private BasicRecovery<Integer> instance;

    @BeforeEach
    public void setUp() {
        this.backupStrategyMock = mock(BackupStrategyMock.class);
        this.executorMock = mock(ScheduledExecutorService.class);
        this.retryerMock = mock(RetryerMock.class);
        this.instance = new BasicRecovery<>(this.backupStrategyMock, this.executorMock, this.recoveryConfig,
                this.retryerMock);
    }

    @AfterEach
    public void tearDown() {
        this.instance.stop();
    }

    @Test
    public void shouldInitTriggerRecovery() {
        RecoveryStrategy strategy = RecoveryStrategies
                .basicStrategy(this.executorMock, this.backupStrategyMock, this.recoveryConfig, this.retryerMock);
        strategy.start();
        verify(this.executorMock, times(1)).schedule(any(Runnable.class),
                eq(this.recoveryConfig.getCheckFrequencyWithDataOnPreviousRun().getSeconds()), eq(SECONDS));
    }

    @Test
    public void shouldInitTriggerRecoveryOnlyOnce() {
        instance.start();
        instance.start();
        instance.start();
        verify(this.executorMock, times(1)).schedule(any(Runnable.class),
                eq(this.recoveryConfig.getCheckFrequencyWithDataOnPreviousRun().getSeconds()), eq(SECONDS));
    }

    @Test
    public void shouldScheduleNextRecoveryOnNoDataToRecover() {
        when(this.backupStrategyMock.restore()).thenReturn(emptyList());
        instance.start();
        instance.recover();
        verify(this.executorMock, times(1)).schedule(any(Runnable.class),
                eq(this.recoveryConfig.getCheckFrequencyWithNoDataOnPreviousRun().getSeconds()), eq(SECONDS));
    }

    @Test
    public void shouldNotScheduleNextRecoveryWithoutStartingTheService() {
        when(this.backupStrategyMock.restore()).thenReturn(emptyList());
        instance.recover();
        verify(this.executorMock, times(0)).schedule(any(Runnable.class),
                eq(this.recoveryConfig.getCheckFrequencyWithNoDataOnPreviousRun().getSeconds()), eq(SECONDS));
    }

    @Test
    public void shouldScheduleNextRecoveryOnRecoveryException() {
        when(this.backupStrategyMock.restore()).thenThrow(new IllegalStateException("Recovery exception"));
        instance.start();
        instance.recover();
        verify(this.executorMock, times(1)).schedule(any(Runnable.class),
                eq(this.recoveryConfig.getCheckFrequencyWithNoDataOnPreviousRun().getSeconds()), eq(SECONDS));
    }

    @Test
    public void shouldScheduleNextRecoveryOnMarkingPartitionAsRestoredException() {
        // given
        List<DataPartition<Integer>> partitions = createPartitions();
        long messagesCount = partitions.stream().mapToLong(d -> d.getMessages().size()).sum();
        when(this.backupStrategyMock.restore()).thenReturn(partitions);
        doThrow(new IllegalStateException("Restoring exception")).when(this.backupStrategyMock).markAsRestored(any());
        when(this.retryerMock.retry(any())).thenReturn(CompletableFuture.completedFuture(null));

        int currentThreadExecutions = partitions.size() + 1; // partitions.size -> execute + 1 -> schedule
        BasicRecovery<Integer> underTest = this.instance.toBuilder()
                .executor(new MockScheduledExecutor(currentThreadExecutions, this.executorMock)).build();

        // when
        underTest.start();

        // then
        verify(this.retryerMock, times((int) messagesCount)).retry(any());
        verify(this.backupStrategyMock, times(partitions.size())).markAsRestored(any());
        verify(this.executorMock, times(1)).schedule(any(Runnable.class),
                eq(this.recoveryConfig.getCheckFrequencyWithDataOnPreviousRun().getSeconds()), eq(SECONDS));
    }

    @Test
    public void shouldRecoverRestoredPartitions() {
        // given
        List<DataPartition<Integer>> partitions = createPartitions();
        long messagesCount = partitions.stream().mapToLong(d -> d.getMessages().size()).sum();
        when(this.backupStrategyMock.restore()).thenReturn(partitions);
        when(this.retryerMock.retry(any())).thenReturn(CompletableFuture.completedFuture(null));

        int currentThreadExecutions = partitions.size() + 1; // partitions.size -> execute + 1 -> schedule
        BasicRecovery<Integer> underTest = this.instance.toBuilder()
                .executor(new MockScheduledExecutor(currentThreadExecutions, this.executorMock)).build();

        // when
        underTest.start();

        // then
        verify(this.retryerMock, times((int) messagesCount)).retry(any());
        verify(this.backupStrategyMock, times(partitions.size())).markAsRestored(any());
        verify(this.executorMock, times(1)).schedule(any(Runnable.class),
                eq(this.recoveryConfig.getCheckFrequencyWithDataOnPreviousRun().getSeconds()), eq(SECONDS));

    }

    @Test
    public void shouldNotRecoverRestoredPartitionsOnRetryException() {
        // given
        List<DataPartition<Integer>> partitions = createPartitions();
        long messagesCount = partitions.stream().mapToLong(d -> d.getMessages().size()).sum();
        when(this.backupStrategyMock.restore()).thenReturn(partitions);
        doAnswer(i -> {
            CompletableFuture<Void> ret = new CompletableFuture<>();
            ret.completeExceptionally(new BackupException("Backup exception"));
            return ret;
        }).when(this.retryerMock).retry(any());

        // when
        instance.start();
        instance.recover();

        // then
        verify(this.retryerMock, times((int) messagesCount)).retry(any());
        verify(this.backupStrategyMock, times(0)).markAsRestored(any());
        verify(this.executorMock, times(2)).schedule(any(Runnable.class),
                eq(this.recoveryConfig.getCheckFrequencyWithDataOnPreviousRun().getSeconds()), eq(SECONDS));

    }

    @Test
    public void shouldNotRecoverRestoredPartitionOnRetryException() {
        // given
        List<DataPartition<Integer>> partitions = createPartitions();
        long messagesCount = partitions.stream().mapToLong(d -> d.getMessages().size()).sum();
        when(this.backupStrategyMock.restore()).thenReturn(partitions);
        doAnswer(i -> {
            Integer msg = i.getArgument(0);
            CompletableFuture<Void> ret = new CompletableFuture<>();
            if (msg == 1) {
                ret.completeExceptionally(new BackupException("Backup exception"));
            } else {
                ret.complete(null);
            }
            return ret;
        }).when(this.retryerMock).retry(any());

        int currentThreadExecutions = partitions.size() + 1; // partitions.size -> execute + 1 -> schedule
        BasicRecovery<Integer> underTest = this.instance.toBuilder()
                .executor(new MockScheduledExecutor(currentThreadExecutions, this.executorMock)).build();

        // when
        underTest.start();

        // then
        verify(this.retryerMock, times((int) messagesCount)).retry(any());
        verify(this.backupStrategyMock, times(partitions.size() - 1)).markAsRestored(any());
        verify(this.executorMock, times(1)).schedule(any(Runnable.class),
                eq(this.recoveryConfig.getCheckFrequencyWithDataOnPreviousRun().getSeconds()), eq(SECONDS));

    }

    // to make sonar happy

    @Test
    public void shouldNotCreateStrategyWithNullExecutor() {
        assertThrows(NullPointerException.class, () ->
                RecoveryStrategies.basicStrategy(null, backupStrategyMock, recoveryConfig, retryerMock));
    }

    @Test
    public void shouldNotCreateStrategyWithNullBackupStrategy() {
        assertThrows(NullPointerException.class, () ->
                RecoveryStrategies.basicStrategy(executorMock, null, recoveryConfig, retryerMock));
    }

    @Test
    public void shouldNotCreateStrategyWithNullConfig() {
        assertThrows(NullPointerException.class, () ->
                RecoveryStrategies.basicStrategy(executorMock, backupStrategyMock, null, retryerMock));
    }

    @Test
    public void shouldNotCreateStrategyWithNullRetryer() {
        assertThrows(NullPointerException.class, () ->
                RecoveryStrategies.basicStrategy(executorMock, backupStrategyMock, recoveryConfig, null));
    }

    @Test
    public void shouldNotCreateStrategyWithNullArguments() {
        assertThrows(NullPointerException.class, () -> RecoveryStrategies.basicStrategy(null, null, null, null));
    }

    private List<DataPartition<Integer>> createPartitions() {
        int count = ThreadLocalRandom.current().nextInt(10) + 2;
        return IntStream.rangeClosed(1, count).mapToObj(i -> DataPartition.<Integer>builder().id(String.valueOf(i))
                .messages(IntStream.rangeClosed(i, count).boxed().collect(toList())).build()).collect(toList());
    }

    private interface RetryerMock extends Retryer<Integer> {

    }
}
