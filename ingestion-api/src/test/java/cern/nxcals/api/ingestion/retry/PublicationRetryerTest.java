package cern.nxcals.api.ingestion.retry;

import cern.nxcals.api.ingestion.Result;
import com.google.common.primitives.Ints;
import lombok.Data;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.AbstractExecutorService;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Delayed;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

import static java.time.Duration.ZERO;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PublicationRetryerTest {
    private PublicationRetryer<Integer> instance;
    private ScheduledExecutorService executorMock;
    private Function<Integer, CompletableFuture<Result>> publisherMock;
    private RetryConfig retryConfigMock;
    private AcceptStrategy acceptStrategyMock;
    private BackupStrategy<Integer> backupStrategyMock;

    @BeforeEach
    public void setUp() {
        this.executorMock = mock(ScheduledExecutorService.class);
        this.publisherMock = mock(PublisherMock.class);
        this.retryConfigMock = mock(RetryConfig.class);
        this.acceptStrategyMock = mock(AcceptStrategy.class);
        this.backupStrategyMock = mock(BackupStrategyMock.class);

        when(this.retryConfigMock.getDelayAttempt()).thenReturn(i -> ZERO);
        when(this.retryConfigMock.getAcceptAttempt()).thenReturn(i -> true);
        when(this.retryConfigMock.isShouldBackup()).thenReturn(true);

        this.instance = PublicationRetryer.<Integer>builder().acceptStrategy(this.acceptStrategyMock)
                .backupStrategy(this.backupStrategyMock).publisher(this.publisherMock).retryConfig(this.retryConfigMock)
                .retryExecutor(this.executorMock).build();
    }

    @Test
    public void shouldNotRetryNotAcceptableException() {
        when(this.acceptStrategyMock.test(any())).thenReturn(false);
        CompletableFuture<Void> callback = this.instance.retry(1, null);
        assertNotNull(callback);
        assertTrue(callback.isCompletedExceptionally());
    }

    @Test
    public void shouldRetryAcceptableException() {
        when(this.acceptStrategyMock.test(any())).thenReturn(true);
        CompletableFuture<Void> callback = this.instance.retry(1, null);
        assertNotNull(callback);
        assertFalse(callback.isCompletedExceptionally());
        verify(this.executorMock, times(1)).schedule(any(Runnable.class), eq(0L), eq(MILLISECONDS));
    }

    @Test
    public void shouldCompleteOnSuccessfulPublication() {
        // given
        int value = ThreadLocalRandom.current().nextInt(100);
        when(this.acceptStrategyMock.test(any())).thenReturn(true);
        when(this.publisherMock.apply(eq(value))).thenReturn(CompletableFuture.completedFuture(null));
        PublicationRetryer<Integer> underTest = this.instance.toBuilder()
                .retryExecutor(new MockScheduledExecutor(2, this.executorMock)).build();

        // when
        CompletableFuture<Void> callback = underTest.retry(value);

        // then
        assertTrue(callback.isDone());
        assertFalse(callback.isCompletedExceptionally());
    }

    @Test
    public void shouldRetryOnPublisherAcceptableException() {
        // given
        int value = ThreadLocalRandom.current().nextInt(100);
        when(this.acceptStrategyMock.test(any())).thenReturn(true);

        CompletableFuture<Result> publisherResult = new CompletableFuture<>();
        publisherResult.completeExceptionally(new IllegalStateException("Acceptable exception"));
        when(this.publisherMock.apply(eq(value))).thenReturn(publisherResult);

        PublicationRetryer<Integer> underTest = this.instance.toBuilder()
                .retryExecutor(new MockScheduledExecutor(2, this.executorMock)).build();

        // when
        CompletableFuture<Void> callback = underTest.retry(value);

        // then
        assertFalse(callback.isDone());
        verify(this.executorMock, times(1)).schedule(any(Runnable.class), eq(0L), eq(MILLISECONDS));
    }

    @Test
    public void shouldRetryOnRejectedPublication() {
        // given
        int value = ThreadLocalRandom.current().nextInt(100);
        when(this.acceptStrategyMock.test(any())).thenReturn(true);

        when(this.publisherMock.apply(eq(value))).thenThrow(new RejectedExecutionException());

        PublicationRetryer<Integer> underTest = this.instance.toBuilder()
                .retryExecutor(new MockScheduledExecutor(1, this.executorMock)).build();

        // when
        CompletableFuture<Void> callback = underTest.retry(value);

        // then
        assertFalse(callback.isDone());
        verify(this.executorMock, times(1)).schedule(any(Runnable.class), eq(0L), eq(MILLISECONDS));
    }

    @Test
    public void shouldNotRetryOnPublisherNotAcceptableException() {
        // given
        int value = ThreadLocalRandom.current().nextInt(100);
        Exception notAcceptableException = new IllegalStateException("Not acceptable");
        doAnswer(i -> {
            Throwable t = i.getArgument(0);
            return !notAcceptableException.equals(t);
        }).when(this.acceptStrategyMock).test(any());

        CompletableFuture<Result> publisherResult = new CompletableFuture<>();
        publisherResult.completeExceptionally(notAcceptableException);
        when(this.publisherMock.apply(eq(value))).thenReturn(publisherResult);

        PublicationRetryer<Integer> underTest = this.instance.toBuilder()
                .retryExecutor(new MockScheduledExecutor(2, this.executorMock)).build();

        // when
        CompletableFuture<Void> callback = underTest.retry(value);

        // then
        assertTrue(callback.isDone());
        assertTrue(callback.isCompletedExceptionally());
        callback.whenComplete((o, e) -> {
            assertNotNull(e);
            assertEquals(notAcceptableException, e);
        });
        verify(this.executorMock, times(0)).schedule(any(Runnable.class), eq(0L), eq(MILLISECONDS));

    }

    @Test
    public void shouldNotRetryOnPublicationException() {
        // given
        int value = ThreadLocalRandom.current().nextInt(100);
        when(this.acceptStrategyMock.test(any())).thenReturn(true);

        Exception publicationException = new IllegalStateException("Publication exception");
        when(this.publisherMock.apply(eq(value))).thenThrow(publicationException);

        PublicationRetryer<Integer> underTest = this.instance.toBuilder()
                .retryExecutor(new MockScheduledExecutor(1, this.executorMock)).build();

        // when
        CompletableFuture<Void> callback = underTest.retry(value);

        // then
        assertTrue(callback.isDone());
        assertTrue(callback.isCompletedExceptionally());
        callback.whenComplete((o, e) -> {
            assertNotNull(e);
            assertEquals(publicationException, e);
        });
        verify(this.executorMock, times(0)).schedule(any(Runnable.class), eq(0L), eq(MILLISECONDS));
    }

    @Test
    public void shouldRetryAsLongAsEventuallySucceeded() {
        // given
        int value = ThreadLocalRandom.current().nextInt(100);
        int attempts = ThreadLocalRandom.current().nextInt(10) + 10;
        when(this.acceptStrategyMock.test(any())).thenReturn(true);
        AtomicInteger currentAttempt = new AtomicInteger(0);

        doAnswer(i -> {
            CompletableFuture<Result> ret = new CompletableFuture<>();
            if (currentAttempt.incrementAndGet() < attempts) {
                ret.completeExceptionally(new IllegalStateException("Not successful attempt"));
            } else {
                ret.complete(null);
            }
            return ret;
        }).when(this.publisherMock).apply(eq(value));

        MockScheduledExecutor retryExecutor = new MockScheduledExecutor(attempts * 2, this.executorMock);
        PublicationRetryer<Integer> underTest = this.instance.toBuilder().retryExecutor(retryExecutor).build();

        // when
        CompletableFuture<Void> callback = underTest.retry(value);

        // then
        assertTrue(callback.isDone());
        assertFalse(callback.isCompletedExceptionally());
        assertEquals(retryExecutor.getCurrentAttempt(), attempts * 2);
    }

    @Test
    public void shouldRetryExceptionallyAfterAllAttemptsExhaustedAndNoBackupStrategy() {
        // given
        int value = ThreadLocalRandom.current().nextInt(100);
        int attempts = ThreadLocalRandom.current().nextInt(10) + 10;
        when(this.acceptStrategyMock.test(any())).thenReturn(true);
        when(this.publisherMock.apply(eq(value))).thenThrow(new RejectedExecutionException());
        when(this.retryConfigMock.getAcceptAttempt()).thenReturn(i -> i < attempts);
        when(this.retryConfigMock.isShouldBackup()).thenReturn(false);

        MockScheduledExecutor retryExecutor = new MockScheduledExecutor(attempts, this.executorMock);
        PublicationRetryer<Integer> underTest = this.instance.toBuilder().retryExecutor(retryExecutor).build();

        // when
        CompletableFuture<Void> callback = underTest.retry(value);

        // then
        assertTrue(callback.isDone());
        assertTrue(callback.isCompletedExceptionally());
        verify(this.executorMock, times(0)).schedule(any(Runnable.class), eq(0L), eq(MILLISECONDS));
        assertEquals(retryExecutor.getCurrentAttempt(), attempts - 1);
    }

    @Test
    public void shouldSuccessfullyBackupAfterAllAttemptsExhaustedAndBackupStrategyPresent() {
        // given
        int value = ThreadLocalRandom.current().nextInt(100);
        when(this.acceptStrategyMock.test(any())).thenReturn(true);
        when(this.publisherMock.apply(eq(value))).thenThrow(new RejectedExecutionException());
        when(this.retryConfigMock.getAcceptAttempt()).thenReturn(i -> false);

        when(this.backupStrategyMock.backup(eq(value))).thenReturn(CompletableFuture.completedFuture(null));
        PublicationRetryer<Integer> underTest = this.instance.toBuilder()
                .retryExecutor(new MockScheduledExecutor(1, this.executorMock)).build();

        // when
        CompletableFuture<Void> callback = underTest.retry(value);

        // then
        assertTrue(callback.isDone());
        assertFalse(callback.isCompletedExceptionally());
        verify(this.backupStrategyMock, times(1)).backup(eq(value));
    }

    @Test
    public void shouldUnSuccessfullyBackupAfterAllAttemptsExhaustedAndFailingBackupStrategyPresent() {
        // given
        int value = ThreadLocalRandom.current().nextInt(100);
        when(this.acceptStrategyMock.test(any())).thenReturn(true);
        when(this.publisherMock.apply(eq(value))).thenThrow(new RejectedExecutionException());
        when(this.retryConfigMock.getAcceptAttempt()).thenReturn(i -> false);

        CompletableFuture<Void> backupResult = new CompletableFuture<>();
        IllegalStateException backupExeption = new IllegalStateException("Something wrong with backup");
        backupResult.completeExceptionally(backupExeption);
        when(this.backupStrategyMock.backup(eq(value))).thenReturn(backupResult);
        PublicationRetryer<Integer> underTest = this.instance.toBuilder()
                .retryExecutor(new MockScheduledExecutor(1, this.executorMock)).build();

        // when
        CompletableFuture<Void> callback = underTest.retry(value);

        // then
        assertTrue(callback.isDone());
        assertTrue(callback.isCompletedExceptionally());
        callback.whenComplete((o, e) -> {
            assertNotNull(e);
            assertEquals(backupExeption, e);
        });
        verify(this.backupStrategyMock, times(1)).backup(eq(value));
    }

    private interface PublisherMock extends Function<Integer, CompletableFuture<Result>> {
    }

    interface BackupStrategyMock extends BackupStrategy<Integer> {
    }

    @Data
    static class MockScheduledExecutor extends AbstractExecutorService implements ScheduledExecutorService  {
        private final int callAttempts;
        private final ScheduledExecutorService mock;
        private int currentAttempt = 0;

        @Override
        public ScheduledFuture<?> schedule(Runnable command, long delay, TimeUnit unit) {
            if (currentAttempt++ >= callAttempts) {
                return this.mock.schedule(command, delay, unit);
            }
            FutureTask<Object> task = new FutureTask<>(command, null);
            // run in the caller thread
            task.run();
            return new MockScheduledFuture<>(delay, unit, task);
        }

        @Override
        public <V> ScheduledFuture<V> schedule(Callable<V> callable, long delay, TimeUnit unit) {
            throw new UnsupportedOperationException();
        }

        @Override
        public ScheduledFuture<?> scheduleAtFixedRate(Runnable command, long initialDelay, long period, TimeUnit unit) {
            throw new UnsupportedOperationException();
        }

        @Override
        public ScheduledFuture<?> scheduleWithFixedDelay(Runnable command, long initialDelay, long delay,
                TimeUnit unit) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void shutdown() {

        }

        @Override
        public List<Runnable> shutdownNow() {
            return Collections.emptyList();
        }

        @Override
        public boolean isShutdown() {
            return true;
        }

        @Override
        public boolean isTerminated() {
            return true;
        }

        @Override
        public boolean awaitTermination(long timeout, TimeUnit unit)  {
            return true;
        }

        @Override
        public void execute(Runnable command) {
            if (currentAttempt++ >= callAttempts) {
                this.mock.execute(command);
            } else {
                command.run();
            }
        }

        @Data
        private static class MockScheduledFuture<T> implements ScheduledFuture<T> {
            private final long delay;
            private final TimeUnit unit;
            private final FutureTask<T> task;

            @Override
            public long getDelay(TimeUnit unit) {
                return unit.convert(delay, this.unit);
            }

            @Override
            public int compareTo(Delayed o) {
                return Ints.saturatedCast(this.getDelay(this.unit) - o.getDelay(this.unit));
            }

            @Override
            public boolean cancel(boolean mayInterruptIfRunning) {
                return this.task.cancel(mayInterruptIfRunning);
            }

            @Override
            public boolean isCancelled() {
                return this.task.isCancelled();
            }

            @Override
            public boolean isDone() {
                return this.task.isDone();
            }

            @Override
            public T get() throws InterruptedException, ExecutionException {
                return this.task.get();
            }

            @Override
            public T get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException,
                    TimeoutException {
                return this.task.get(timeout, unit);
            }
        }

    }
}
