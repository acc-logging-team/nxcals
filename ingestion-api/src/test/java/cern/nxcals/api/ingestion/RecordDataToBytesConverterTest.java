/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.api.ingestion;

import avro.shaded.com.google.common.collect.ImmutableMap;
import cern.cmw.data.DataFactory;
import cern.cmw.data.DiscreteFunction;
import cern.cmw.datax.DataBuilder;
import cern.cmw.datax.ImmutableData;
import cern.cmw.datax.ImmutableEntry;
import cern.cmw.datax.enumeration.EnumDefinition;
import cern.cmw.datax.enumeration.EnumFactory;
import cern.cmw.datax.enumeration.EnumValue;
import cern.cmw.datax.enumeration.EnumValueSet;
import cern.nxcals.api.domain.EntitySchema;
import cern.nxcals.common.SystemFields;
import cern.nxcals.common.avro.DataEncoderImpl;
import cern.nxcals.common.avro.DefaultBytesToGenericRecordDecoder;
import cern.nxcals.common.converters.TimeConverter;
import cern.nxcals.common.converters.TimeConverterImpl;
import cern.nxcals.common.utils.ReflectionUtils;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class RecordDataToBytesConverterTest extends Base {
    @Mock
    private RecordData recordData;

    private final TimeConverter timeConverter = new TimeConverterImpl();

    private static boolean checkType(Schema schema, String field, String type) {
        return schema.getField(field).schema().getTypes().stream().map(Schema::getName).anyMatch(type::equals);
    }

    @BeforeEach
    public void setUp() {
        when(recordData.getEntityId()).thenReturn(10L);
        when(recordData.getSystemId()).thenReturn(100L);
        when(recordData.getPartitionId()).thenReturn(1000L);
    }

    @Test
    public void testEncodeSchemaAndSerialize() {
        // given
        DataEncoderImpl encoder = new DataEncoderImpl(
                Base.entityKeyDefSchema, Base.partitionKeyDefSchema,
                Base.timeKeyDefSchema, Base.recordVersionKeyDefSchema, timeConverter);
        ImmutableData data = createTestCmwData(10);
        String recordSchemaStr = encoder.encodeRecordFieldDefinitions(data);

        when(recordData.getData()).thenReturn(data);
        EntitySchema schemaData = ReflectionUtils.builderInstance(EntitySchema.InnerBuilder.class).id(1L)
                .schemaJson(recordSchemaStr).build();

        when(recordData.getSchemaData()).thenReturn(schemaData);

        RecordDataToBytesConverter serializer = new RecordDataToBytesConverter();

        // when
        byte[] output = serializer.apply(recordData);
        GenericRecord record = deserialize(output, schemaData);

        // System.err.println(record);
        // then
        assertNotNull(record);

        //check for the system fields
        assertEquals(1L, record.get(SystemFields.NXC_SCHEMA_ID.getValue()));
        assertEquals(10L, record.get(SystemFields.NXC_ENTITY_ID.getValue()));
        assertEquals(100L, record.get(SystemFields.NXC_SYSTEM_ID.getValue()));
        assertEquals(1000L, record.get(SystemFields.NXC_PARTITION_ID.getValue()));
        assertTrue((Long) record.get(SystemFields.NXC_TIMESTAMP.getValue()) <= System.currentTimeMillis() * 1_000_000);

        assertEquals("dev1", record.get("device").toString());
        assertEquals("class1", record.get("class").toString());
        assertEquals("prop1", record.get("property").toString());
        assertEquals(123456789L, record.get("_p_t_"));
        assertEquals("ver1", record.get("record_version1").toString());
        assertEquals(10, record.get("record_version2"));
        assertEquals(-1, record.get("shortField1"));
        assertEquals(-2, record.get("byteField1"));

        //check for all data fields

        verifyFields(data, record.getSchema());

    }

    private void verifyFields(ImmutableData data, Schema schema) {
        for (ImmutableEntry entry : data.getEntries()) {
            assertNotNull(schema.getField(entry.getName()));
        }
    }

    @Test
    public void testEnums() {
        // given
        DataEncoderImpl encoder = new DataEncoderImpl(
                Base.entityKeyDefSchema, Base.partitionKeyDefSchema,
                Base.timeKeyDefSchema, Base.recordVersionKeyDefSchemaNullable, timeConverter);
        ImmutableData data = createTestCmwData(1);

        DataBuilder builder = ImmutableData.builder();
        builder.addAll(data.getEntries());
        // EnumValue
        Map<String, Long> symbols = new ImmutableMap.Builder<String, Long>().put("ala", 1L).put("ma", 2L)
                .put("kota", 3L).build();
        EnumDefinition def = EnumFactory.createDefinition("symbols", symbols);

        // EnumValue[]
        EnumValue[] values = new EnumValue[3];
        values[0] = def.valueOf("ala");
        values[1] = def.valueOf("ma");
        values[2] = def.valueOf("kota");

        // EnumValueSet
        EnumValueSet enumSet = EnumFactory.createEnumSet(def, values);
        builder.add("enumValue", def.valueOf("ala"));
        builder.add("enumValues", values);
        builder.add("enumValueSet", enumSet);

        data = builder.build();
        String schema = encoder.encodeRecordFieldDefinitions(data);
        assertNotNull(schema);

        String recordSchemaStr = encoder.encodeRecordFieldDefinitions(data);
        when(recordData.getData()).thenReturn(data);
        EntitySchema schemaData = ReflectionUtils.builderInstance(EntitySchema.InnerBuilder.class).id(1L)
                .schemaJson(recordSchemaStr).build();
        when(recordData.getSchemaData()).thenReturn(schemaData);
        RecordDataToBytesConverter serializer = new RecordDataToBytesConverter();
        byte[] output = serializer.apply(recordData);
        GenericRecord record = deserialize(output, schemaData);
        assertNotNull(record);

        String value = record.get("enumValue").toString();
        assertEquals("ala", value);

        GenericData.Record enumValues = (GenericData.Record) record.get("enumValues");
        assertNotNull(enumValues);
        Object elements = enumValues.get("elements");
        assertNotNull(elements);
        assertEquals(Arrays.stream(values).map(EnumValue::getName).collect(Collectors.toSet()),
                Arrays.stream(((GenericData.Array) elements).toArray()).map(Object::toString)
                        .collect(Collectors.toSet()));
        Object dims = enumValues.get("dimensions");
        assertNotNull(dims);
        assertEquals(Collections.singleton(String.valueOf(values.length)),
                Arrays.stream(((GenericData.Array) dims).toArray()).map(Object::toString).collect(Collectors.toSet()));

        GenericData.Record enumValueSet = (GenericData.Record) record.get("enumValueSet");
        assertNotNull(enumValueSet);
        elements = enumValueSet.get("elements");
        assertNotNull(elements);
        assertEquals(Arrays.stream(enumSet.toArray()).map(EnumValue::getName).collect(Collectors.toSet()),
                Arrays.stream(((GenericData.Array) elements).toArray()).map(Object::toString)
                        .collect(Collectors.toSet()));
        dims = enumValueSet.get("dimensions");
        assertNotNull(dims);
        assertEquals(Collections.singleton(String.valueOf(enumSet.toArray().length)),
                Arrays.stream(((GenericData.Array) dims).toArray()).map(Object::toString).collect(Collectors.toSet()));
    }

    private ImmutableData createTestCmwData(Integer recordVersion) {
        return createTestCmwData(recordVersion, null, null, true);
    }

    private ImmutableData createTestCmwData(Integer recordVersion, Long entityId, Long partitionId, boolean addFields) {

        DataBuilder builder = ImmutableData.builder();

        if (entityId != null) {
            builder.add(SystemFields.NXC_ENTITY_ID.getValue(), entityId);
        }

        if (partitionId != null) {
            builder.add(SystemFields.NXC_PARTITION_ID.getValue(), entityId);
        }

        builder.add("device", "dev1");
        builder.add("property", "prop1");
        builder.add("class", "class1");
        builder.add("_p_t_", 123456789L);

        builder.add("boolField", true);
        builder.add("byteField1", (byte) -2);
        builder.add("shortField1", (short) -1);
        builder.add("intField1", 1);
        builder.add("longField1", 1L);
        builder.add("longField2", 1L);
        builder.add("floatField1", 1.0f);
        builder.add("doubleField", 1.0d);
        builder.add("stringField1", "string value");

        builder.add("boolArrayField", new boolean[] { true, false, true });
        builder.add("byteArrayField", new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
        builder.add("shortArrayField", new short[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
        builder.add("intArrayField", new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
        builder.add("longArrayField", new long[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
        builder.add("floatArrayField", new float[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
        builder.add("doubleArrayField", new double[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
        builder.add("stingArrayField", new String[] { "aaa", "bbb" });

        builder.add("boolArray2DField1", new boolean[] { true, false }, 1, 2);
        builder.add("byteArray2DField2", new byte[] { 1, 2 }, 1, 2);
        builder.add("shortArray2DField2", new short[] { 1, 2 }, 1, 2);
        builder.add("intArray2DField2", new int[] { 1, 2 }, 1, 2);
        builder.add("longArray2DField2", new long[] { 1, 2 }, 1, 2);
        builder.add("doubleArray2DField2", new double[] { 1, 2 }, 1, 2);
        builder.add("floatArray2DField2", new float[] { 1, 2 }, 1, 2);
        builder.add("stringArray2DField2", new String[] { "aaaa", "bbbb" }, 1, 2);

        builder.add("boolMultiArrayField1", new boolean[] { true, false, true, false }, 2, 2);
        builder.add("byteMultiArrayField1", new byte[] { 1, 2, 3, 4 }, 2, 2);
        builder.add("shortMultiArrayField1", new short[] { 1, 2, 3, 4 }, 2, 2);
        builder.add("intMultiArrayField1", new int[] { 1, 2, 3, 4 }, 2, 2);
        builder.add("longMultiArrayField1", new long[] { 1, 2, 3, 4 }, 2, 2);
        builder.add("doubleMultiArrayField1", new double[] { 1, 2, 3, 4 }, 2, 2);
        builder.add("floatMultiArrayField1", new float[] { 1, 2, 3, 4 }, 2, 2);
        builder.add("stringMultiArrayField1", new String[] { "aaaa", "bbbb", "cccc", "dddd" }, 2, 2);
        builder.add("stringMultiArrayField2", new String[] { "aaaa", "bbbb", "cccc", "dddd" }, 2, 2);

        builder.add("ddfField1",
                DataFactory.createDiscreteFunction(new double[] { 1.0, 2.0 }, new double[] { 1.0, 2.0 }));
        builder.add("ddfField2",
                DataFactory.createDiscreteFunction(new double[] { 1.0, 2.0 }, new double[] { 1.0, 2.0 }));

        builder.add("ddfArrayField1", new DiscreteFunction[] {
                DataFactory.createDiscreteFunction(new double[] { 1.0, 2.0 }, new double[] { 1.0, 2.0 }),
                DataFactory.createDiscreteFunction(new double[] { 1.0, 2.0, 3.0 }, new double[] { 1.0, 2.0, 3.0 }) });

        builder.add("ddfArrayField2", new DiscreteFunction[] {
                DataFactory.createDiscreteFunction(new double[] { 1.0, 2.0 }, new double[] { 1.0, 2.0 }),
                DataFactory.createDiscreteFunction(new double[] { 1.0, 2.0, 3.0 }, new double[] { 1.0, 2.0, 3.0 }) });

        builder.add("ddfMultiArrayField", new DiscreteFunction[] {
                        DataFactory.createDiscreteFunction(new double[] { 1.0, 2.0 }, new double[] { 1.0, 2.0 }),
                        DataFactory.createDiscreteFunction(new double[] { 1.0, 2.0, 3.0 }, new double[] { 1.0, 2.0, 3.0 }),
                        DataFactory.createDiscreteFunction(new double[] { 1.1, 2.1, 3.1 }, new double[] { 1.2, 2.2, 3.2 }) }, 1,
                1, 1);

        DataBuilder secondNestedBuilder = ImmutableData.builder();
        secondNestedBuilder.add("longField1", 1L);
        secondNestedBuilder.add("discreteFunctionField",
                DataFactory.createDiscreteFunction(new double[] { 1.0, 2.0 }, new double[] { 1.0, 2.0 }));
        secondNestedBuilder.add("multiArrayLong", new short[] { 1, 2, 3, 4 }, 2, 2);

        DataBuilder nestedBuilder = ImmutableData.builder();
        if (addFields) {
            nestedBuilder.add("boolField", true);
            nestedBuilder.add("longField", 1L);
            // here nothing happens, the record version is ignored.
            nestedBuilder.add("record_version1", "ver1");
            nestedBuilder.add("record_version2", 12);
            nestedBuilder.add("boolArray2DField2", new boolean[] { true, false }, 1, 2);
            nestedBuilder.add("byteArrayField", new byte[] { 1, 2, 3, 4 });
            nestedBuilder.add("dataField", secondNestedBuilder.build());
        }
        builder.add("dataField", nestedBuilder.build());

        if (recordVersion != null) {
            builder.add("record_version1", "ver1");
            builder.add("record_version2", recordVersion);
        }
        return builder.build();
    }

    private static GenericRecord deserialize(byte[] bytes, EntitySchema schemaData) {
        DefaultBytesToGenericRecordDecoder decoder = new DefaultBytesToGenericRecordDecoder(schemaId -> schemaData);
        return decoder.apply(schemaData.getId(), bytes);
    }
}
