package cern.nxcals.api.custom.extraction.metadata;

import cern.nxcals.api.custom.domain.GroupType;
import cern.nxcals.api.custom.domain.Tag;
import cern.nxcals.api.custom.extraction.metadata.queries.Tags;
import cern.nxcals.api.domain.Group;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.Visibility;
import cern.nxcals.api.extraction.metadata.GroupService;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.api.utils.TimeUtils;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.google.common.collect.ImmutableMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.time.Instant;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static cern.nxcals.api.custom.extraction.metadata.TagService.TAG_TIMESTAMP;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class TagServiceImplTest {
    private TagService tagService;

    @Mock
    private GroupService groupService;
    @Mock
    private SystemSpecService systemSpecService;

    private Tag tag;

    private Group group;

    @Mock
    private SystemSpec systemSpec;
    private Tag tagUpdated;
    private Group groupUpdated;


    private Map<String, Set<Long>> emptyMap;
    private Condition condition;
    private Instant now = Instant.now();

    @BeforeEach
    void setup() {
        tagService = new TagServiceImpl(groupService, systemSpecService);
        tag = createTag("DESC");
        group = createGroup("DESC");
        groupUpdated = createGroup("DESC2");
        tagUpdated = createTag("DESC2");

        when(groupService.create(any())).thenReturn(group);
        when(systemSpecService.findByName("CMW")).thenReturn(Optional.of(systemSpec));
        when(systemSpec.getName()).thenReturn("CMW");
        emptyMap = new HashMap<>();
        condition = Tags.suchThat().name().eq("TEST");

    }

    private Group createGroup(String desc) {
        return Group.builder().name("TAG").description(desc)
                .properties(ImmutableMap.of(TAG_TIMESTAMP, TimeUtils.getStringFromInstant(now)))
                .visibility(Visibility.PUBLIC)
                .systemSpec(systemSpec).owner("OWNER").label(GroupType.TAG.toString()).build();
    }

    private Tag createTag(String desc) {
        return Tag.builder().name("TAG").description(desc).properties(new HashMap<>()).visibility(Visibility.PUBLIC)
                .timestamp(now).system("CMW").owner("OWNER").build();
    }

    @Test
    void create() {
        try (MockedStatic<ServiceClientFactory> utilities = Mockito.mockStatic(ServiceClientFactory.class)) {
            //given
            utilities.when(ServiceClientFactory::createGroupService).thenReturn(groupService);
            utilities.when(ServiceClientFactory::createSystemSpecService).thenReturn(systemSpecService);

            //when
            Tag tag = tagService
                    .create("TAG", "DESC", Visibility.PUBLIC, "OWNER", "CMW", now,
                            Collections.emptyList(), Collections.emptyList());
            //then
            assertNotNull(tag);
            assertEquals("TAG", tag.getName());
            assertEquals("DESC", tag.getDescription());
            assertEquals("OWNER", tag.getOwner());
            assertEquals("CMW", tag.getSystem());
            assertEquals(TimeUtils.getInstantFromString(group.getProperties().get(TAG_TIMESTAMP)), tag.getTimestamp());

        }
    }

    @Test
    void update() {
        //given
        when(groupService.update(any())).thenReturn(groupUpdated);
        //when
        Tag updated = tagService.update(tag);

        //then
        assertEquals(groupUpdated.getDescription(), updated.getDescription());
    }

    @Test
    void delete() {
        //given
        //when
        tagService.delete(1);
        //then
        verify(groupService).delete(1);
    }

    @Test
    void getVariables() {
        //given
        //when
        tagService.getVariables(1);
        //then
        verify(groupService).getVariables(1);
    }

    @Test
    void setVariables() {
        //given
        //when
        tagService.setVariables(1, emptyMap);
        //then
        verify(groupService).setVariables(1, emptyMap);
    }

    @Test
    void addVariables() {
        //given
        //when
        tagService.addVariables(1, emptyMap);
        //then
        verify(groupService).addVariables(1, emptyMap);
    }

    @Test
    void removeVariables() {
        //given
        //when
        tagService.removeVariables(1, emptyMap);
        //then
        verify(groupService).removeVariables(1, emptyMap);
    }

    @Test
    void getEntities() {
        //given

        //when
        tagService.getEntities(1);

        //then
        verify(groupService).getEntities(1);
    }

    @Test
    void setEntities() {
        //given
        //when
        tagService.setEntities(1, emptyMap);
        //then
        verify(groupService).setEntities(1, emptyMap);
    }

    @Test
    void addEntities() {
        //given
        //when
        tagService.addEntities(1, emptyMap);
        //then
        verify(groupService).addEntities(1, emptyMap);
    }

    @Test
    void removeEntities() {
        //given
        //when
        tagService.removeEntities(1, emptyMap);
        //then
        verify(groupService).removeEntities(1, emptyMap);
    }

    @Test
    void findAll() {
        //given
        //when
        tagService.findAll(condition);
        //then
        verify(groupService).findAll(condition);
    }

    @Test
    void findOne() {
        //given
        //when
        tagService.findOne(condition);
        //then
        verify(groupService).findOne(condition);
    }

    @Test
    void findById() {
        //given
        //when
        tagService.findById(1);
        //then
        verify(groupService).findById(1);
    }
}