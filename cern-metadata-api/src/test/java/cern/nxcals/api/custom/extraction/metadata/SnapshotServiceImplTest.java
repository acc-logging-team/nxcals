package cern.nxcals.api.custom.extraction.metadata;

import cern.nxcals.api.custom.domain.GroupType;
import cern.nxcals.api.custom.domain.Snapshot;
import cern.nxcals.api.custom.domain.SnapshotProperty;
import cern.nxcals.api.custom.domain.constants.BeamModeData;
import cern.nxcals.api.custom.extraction.metadata.queries.Snapshots;
import cern.nxcals.api.domain.Group;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.Visibility;
import cern.nxcals.api.extraction.metadata.GroupService;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.google.common.collect.ImmutableMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class SnapshotServiceImplTest {

    private SnapshotService snapshotService;

    @Mock
    private GroupService groupService;
    @Mock
    private SystemSpecService systemSpecService;

    private Snapshot snapshot;

    private Group group;

    private Variable variable;
    private Variable xAxisVariable;
    private Variable fundamentalFiltersVariable;
    private Variable drivingVariable;
    private Map<String, Set<Variable>> variablesPerAssociation;

    @Mock
    private SystemSpec systemSpec;
    private Group groupUpdated;


    private final Map<String, Set<Long>> emptyMap = Collections.emptyMap();
    private Condition condition;

    @BeforeEach
    void setup() {
        snapshotService = new SnapshotServiceImpl(groupService, systemSpecService);
        snapshot = createSnapshot();
        group = createGroup("DESC");
        groupUpdated = createGroup("DESC2");

        when(groupService.create(any())).thenReturn(group);
        when(systemSpecService.findByName("CMW")).thenReturn(Optional.of(systemSpec));
        when(systemSpec.getName()).thenReturn("CMW");
        condition = Snapshots.suchThat().name().eq("TEST");

        variable = Variable.builder().variableName("VAR").systemSpec(systemSpec).build();
        xAxisVariable = Variable.builder().variableName("X_AXIS_VAR").systemSpec(systemSpec).build();
        fundamentalFiltersVariable = Variable.builder().variableName("FUNDAMENTAL_VAR").systemSpec(systemSpec).build();
        drivingVariable = Variable.builder().variableName("DRIVING_VAR").systemSpec(systemSpec).build();

        variablesPerAssociation = new HashMap<>();

        variablesPerAssociation.put(SnapshotService.AssociationType.DEFAULT.getValue(), new HashSet<>(List.of(variable)));
        variablesPerAssociation.put(SnapshotService.AssociationType.X_AXIS_VARIABLE.getValue(), new HashSet<>(List.of(xAxisVariable)));
        variablesPerAssociation.put(SnapshotService.AssociationType.FUNDAMENTAL_FILTERS.getValue(), new HashSet<>(List.of(fundamentalFiltersVariable)));
        variablesPerAssociation.put(SnapshotService.AssociationType.DRIVING_VARIABLE.getValue(), new HashSet<>(List.of(drivingVariable)));

        when(groupService.getVariables(group.getId())).thenReturn(variablesPerAssociation);
    }

    private Group createGroup(String desc) {
        return Group.builder().name("SNAPSHOT").description(desc)
                .properties(ImmutableMap.of(SnapshotProperty.BEAM_MODE_START.getValue(), BeamModeData.builder().build().toString()))
                .properties(ImmutableMap.of(SnapshotProperty.BEAM_MODE_END.getValue(), BeamModeData.builder().build().toString()))
                .properties(ImmutableMap.of(SnapshotProperty.START_TIME.getValue(), "2023-05-16 05:15:03.533738525"))
                .properties(ImmutableMap.of(SnapshotProperty.END_TIME.getValue(), "2023-06-16 05:15:03.533738525"))
                .visibility(Visibility.PUBLIC)
                .systemSpec(systemSpec).owner("OWNER").label(GroupType.SNAPSHOT.toString()).build();
    }

    private Snapshot createSnapshot() {
        return Snapshot.builder().name("SNAPSHOT").description("DESC").visibility(Visibility.PUBLIC).timeDefinition(
                        Snapshot.TimeDefinition.forFill(100, BeamModeData.builder().build(), BeamModeData.builder().build()))
                .system("CMW").owner("OWNER").build();
    }

    @Test
    void create() {
        try (MockedStatic<ServiceClientFactory> utilities = Mockito.mockStatic(ServiceClientFactory.class)) {
            //given
            utilities.when(ServiceClientFactory::createGroupService).thenReturn(groupService);
            utilities.when(ServiceClientFactory::createSystemSpecService).thenReturn(systemSpecService);

            //when

            Snapshot snapshot = snapshotService
                    .create(Snapshot.builder().name("SNAPSHOT").description("DESC").visibility(Visibility.PUBLIC)
                                    .system("CMW").owner("OWNER")
                                    .timeDefinition(Snapshot.TimeDefinition.forFill(100,
                                            BeamModeData.builder().beamModeValue("2023-05-16 05:15:03.533738525").build(),
                                            BeamModeData.builder().beamModeValue("2023-06-16 05:15:03.533738525").build()))
                                    .build(),
                            Collections.emptyList(),
                            new HashSet<>(List.of(variable)),
                            new HashSet<>(List.of(xAxisVariable)),
                            new HashSet<>(List.of(fundamentalFiltersVariable)),
                            new HashSet<>(List.of(drivingVariable)));
            //then
            assertNotNull(snapshot);
            assertEquals("SNAPSHOT", snapshot.getName());
            assertEquals("DESC", snapshot.getDescription());
            assertEquals("OWNER", snapshot.getOwner());
            assertEquals("CMW", snapshot.getSystem());

            assertEquals(group.getProperties().get(SnapshotProperty.BEAM_MODE_START.getValue()),
                    snapshot.getBeamModeStart().toString());
            assertEquals(group.getProperties().get(SnapshotProperty.BEAM_MODE_END.getValue()),
                    snapshot.getBeamModeEnd().toString());
            assertEquals(groupService.getVariables(group.getId()), variablesPerAssociation);
        }
    }

    @Test
    void update() {
        //given
        when(groupService.update(any())).thenReturn(groupUpdated);
        //when
        Snapshot updated = snapshotService.update(snapshot);

        //then
        assertEquals(groupUpdated.getDescription(), updated.getDescription());
    }

    @Test
    void delete() {
        //given
        //when
        snapshotService.delete(1);
        //then
        verify(groupService).delete(1);
    }


    @Test
    void getVariables() {
        //given
        //when
        snapshotService.getVariables(1);
        //then
        verify(groupService).getVariables(1);
    }

    @Test
    void setVariables() {
        //given
        //when
        snapshotService.setVariables(1, emptyMap);
        //then
        verify(groupService).setVariables(1, emptyMap);
    }

    @Test
    void addVariables() {
        //given
        //when
        snapshotService.addVariables(1, emptyMap);
        //then
        verify(groupService).addVariables(1, emptyMap);
    }

    @Test
    void removeVariables() {
        //given
        //when
        snapshotService.removeVariables(1, emptyMap);
        //then
        verify(groupService).removeVariables(1, emptyMap);
    }

    @Test
    void getEntities() {
        //given

        //when
        snapshotService.getEntities(1);

        //then
        verify(groupService).getEntities(1);
    }

    @Test
    void setEntities() {
        //given
        //when
        snapshotService.setEntities(1, emptyMap);
        //then
        verify(groupService).setEntities(1, emptyMap);
    }

    @Test
    void addEntities() {
        //given
        //when
        snapshotService.addEntities(1, emptyMap);
        //then
        verify(groupService).addEntities(1, emptyMap);
    }

    @Test
    void removeEntities() {
        //given
        //when
        snapshotService.removeEntities(1, emptyMap);
        //then
        verify(groupService).removeEntities(1, emptyMap);
    }

    @Test
    void findAll() {
        //given
        //when
        snapshotService.findAll(condition);
        //then
        verify(groupService).findAll(condition);
    }

    @Test
    void findOne() {
        //given
        //when
        snapshotService.findOne(condition);
        //then
        verify(groupService).findOne(condition);
    }

    @Test
    void findById() {
        //given
        //when
        snapshotService.findById(1);
        //then
        verify(groupService).findById(1);
    }
}