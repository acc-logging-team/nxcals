package cern.nxcals.internal.custom.extraction.metadata;

import cern.nxcals.api.domain.Group;
import cern.nxcals.api.extraction.metadata.GroupService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.HashMap;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


class InternalBaseGroupServiceTest {
    private InternalBaseGroupService baseGroupService;
    private GroupService groupService;

    private int testId = 123;


    @Test
    public void shouldDeleteGroupUsingInt() {
        baseGroupService.delete(testId);

        verify(groupService, times(1)).delete(testId);
    }

    @Test
    void shouldGetVariablesUsingInt() {
        baseGroupService.getVariables(testId);

        verify(groupService, times(1)).getVariables(testId);
    }

    @Test
    void shouldSetVariablesUsingInt() {
        baseGroupService.setVariables(testId, new HashMap<>());

        verify(groupService, times(1)).setVariables(testId, new HashMap<>());
    }

    @Test
    void shouldAddVariablesUsingInt() {
        baseGroupService.addVariables(testId, new HashMap<>());

        verify(groupService, times(1)).addVariables(testId, new HashMap<>());
    }

    @Test
    void shouldRemoveVariablesUsingInt() {
        baseGroupService.removeVariables(testId, new HashMap<>());

        verify(groupService, times(1)).removeVariables(testId, new HashMap<>());
    }

    @Test
    void shouldGetEntitiesUsingInt() {
        baseGroupService.getEntities(testId);

        verify(groupService, times(1)).getEntities(testId);
    }

    @Test
    void shouldSetEntitiesUsingInt() {
        baseGroupService.setEntities(testId, new HashMap<>());

        verify(groupService, times(1)).setEntities(testId, new HashMap<>());
    }

    @Test
    void shouldAddEntitiesUsingInt() {
        baseGroupService.addEntities(testId, new HashMap<>());

        verify(groupService, times(1)).addEntities(testId, new HashMap<>());
    }

    @Test
    void shouldRemoveEntitiesUsingInt() {
        baseGroupService.removeEntities(testId, new HashMap<>());

        verify(groupService, times(1)).removeEntities(testId, new HashMap<>());
    }

    @BeforeEach
    void setUp() throws NoSuchFieldException, IllegalAccessException {
        groupService = Mockito.mock(GroupService.class);

        baseGroupService = new InternalBaseGroupService(groupService) {
            @Override
            protected Object fromGroup(Group group) {
                return null;
            }

            @Override
            protected Group toGroup(Object group) {
                return null;
            }
        };
    }
}