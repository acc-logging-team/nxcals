package cern.nxcals.internal.custom.extraction.metadata;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.Group;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.extraction.metadata.GroupService;
import com.github.rutledgepaulv.qbuilders.builders.QBuilder;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import lombok.RequiredArgsConstructor;

import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public abstract class InternalBaseGroupService<T, C extends QBuilder<C>> {
    protected abstract T fromGroup(Group group);

    protected abstract Group toGroup(T group);

    protected final GroupService groupService;

    public void delete(long id) {
        groupService.delete(id);
    }

    public void delete(int id) {
        groupService.delete(id);
    }

    public Map<String, Set<Variable>> getVariables(long groupId) {
        return groupService.getVariables(groupId);
    }

    public Map<String, Set<Variable>> getVariables(int groupId) {
        return groupService.getVariables(groupId);
    }

    public void setVariables(long groupId, Map<String, Set<Long>> variableIdsPerAssociation) {
        groupService.setVariables(groupId, variableIdsPerAssociation);
    }

    public void setVariables(int groupId, Map<String, Set<Long>> variableIdsPerAssociation) {
        groupService.setVariables(groupId, variableIdsPerAssociation);
    }

    public void addVariables(long groupId, Map<String, Set<Long>> variableIdsPerAssociation) {
        groupService.addVariables(groupId, variableIdsPerAssociation);
    }

    public void addVariables(int groupId, Map<String, Set<Long>> variableIdsPerAssociation) {
        groupService.addVariables(groupId, variableIdsPerAssociation);
    }

    public void removeVariables(long groupId, Map<String, Set<Long>> variableIdsPerAssociation) {
        groupService.removeVariables(groupId, variableIdsPerAssociation);
    }

    public void removeVariables(int groupId, Map<String, Set<Long>> variableIdsPerAssociation) {
        groupService.removeVariables(groupId, variableIdsPerAssociation);
    }

    public Map<String, Set<Entity>> getEntities(long groupId) {
        return groupService.getEntities(groupId);
    }

    public Map<String, Set<Entity>> getEntities(int groupId) {
        return groupService.getEntities(groupId);
    }

    public void setEntities(long groupId, Map<String, Set<Long>> entityIdsPerAssociation) {
        groupService.setEntities(groupId, entityIdsPerAssociation);
    }

    public void setEntities(int groupId, Map<String, Set<Long>> entityIdsPerAssociation) {
        groupService.setEntities(groupId, entityIdsPerAssociation);
    }

    public void addEntities(long groupId, Map<String, Set<Long>> entityIdsPerAssociation) {
        groupService.addEntities(groupId, entityIdsPerAssociation);
    }

    public void addEntities(int groupId, Map<String, Set<Long>> entityIdsPerAssociation) {
        groupService.addEntities(groupId, entityIdsPerAssociation);
    }

    public void removeEntities(long groupId, Map<String, Set<Long>> entityIdsPerAssociation) {
        groupService.removeEntities(groupId, entityIdsPerAssociation);
    }

    public void removeEntities(int groupId, Map<String, Set<Long>> entityIdsPerAssociation) {
        groupService.removeEntities(groupId, entityIdsPerAssociation);
    }

    public Set<T> findAll(Condition<C> condition) {
        Set<Group> all = groupService.findAll((Condition) condition);
        return all.stream().map(this::fromGroup)
                .collect(Collectors.toSet());
    }

    public Optional<T> findOne(Condition<C> condition) {
        Optional<Group> one = groupService.findOne((Condition) condition);
        return one.map(this::fromGroup);
    }

    public Optional<T> findById(long id) {
        return groupService.findById(id).map(this::fromGroup);
    }

    public T create(T object) {
        return fromGroup(groupService.create(toGroup(object)));
    }

    public T update(T object) {
        return fromGroup(groupService.update(toGroup(object)));
    }
}
