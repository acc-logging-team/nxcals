package cern.nxcals.api.custom.extraction.metadata;

import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.common.annotation.Experimental;
import cern.nxcals.common.utils.Lazy;
import lombok.experimental.UtilityClass;

@UtilityClass
@Experimental
public class TagServiceFactory {
    private static final Lazy<TagService> TAG_SERVICE_LAZY = new Lazy<>(() -> new TagServiceImpl(ServiceClientFactory.createGroupService(), ServiceClientFactory.createSystemSpecService()));

    public static TagService createTagService() {
        return TAG_SERVICE_LAZY.get();
    }


}
