package cern.nxcals.api.custom.extraction.metadata;

import cern.nxcals.api.custom.domain.GroupType;
import cern.nxcals.api.custom.domain.Tag;
import cern.nxcals.api.custom.extraction.metadata.queries.Tags;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.Group;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.Visibility;
import cern.nxcals.api.extraction.metadata.GroupService;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.common.utils.ReflectionUtils;
import cern.nxcals.internal.custom.extraction.metadata.InternalBaseGroupService;
import com.google.common.collect.ImmutableMap;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.time.Instant;
import java.util.Collection;
import java.util.HashMap;
import java.util.stream.Collectors;

@Slf4j
class TagServiceImpl extends InternalBaseGroupService<Tag, Tags> implements TagService {
    private final SystemSpecService systemSpecService;

    public TagServiceImpl(GroupService groupService, SystemSpecService systemSpecService) {
        super(groupService);
        this.systemSpecService = systemSpecService;
    }

    protected Tag fromGroup(Group group) {
        Tag tag = Tag.builder()
                .name(group.getName())
                .description(group.getDescription())
                .system(group.getSystemSpec().getName())
                .owner(group.getOwner())
                .visibility(group.getVisibility())
                .properties(new HashMap<>(group.getProperties()))
                .timestamp(TimeUtils.getInstantFromString(group.getProperties().get(TAG_TIMESTAMP)))
                .build();

        ReflectionUtils.enhanceWithId(tag, group.getId());
        ReflectionUtils.enhanceWithRecVersion(tag, group.getRecVersion());
        return tag;
    }

    protected Group toGroup(Tag tag) {
        Group group = Group.builder().name(tag.getName())
                .description(tag.getDescription())
                .systemSpec(systemSpecService.findByName(tag.getSystem())
                        .orElseThrow(() -> new RuntimeException("Cannot find system: " + tag.getSystem())))
                .properties(new HashMap<>(tag.getProperties()))
                .owner(tag.getOwner())
                .visibility(tag.getVisibility())
                .label(GroupType.TAG.toString())
                .build();

        ReflectionUtils.enhanceWithId(group, tag.getId());
        ReflectionUtils.enhanceWithRecVersion(group, tag.getRecVersion());
        return group;
    }

    @Override
    public Tag create(String name, String description, Visibility visibility, String owner, String system,
                      Instant timestamp, @NonNull Collection<Entity> entities, @NonNull Collection<Variable> variables) {
        Tag tag = Tag.builder()
                .name(name)
                .description(description)
                .visibility(visibility)
                .owner(owner)
                .system(system)
                .timestamp(timestamp)
                .properties(new HashMap<>())
                .build();
        Tag created = create(tag);
        if (!entities.isEmpty()) {
            setEntities(created.getId(),
                    ImmutableMap.of(DEFAULT_RELATION, entities.stream().map(Entity::getId).collect(
                            Collectors.toSet())));
        }
        if (!variables.isEmpty()) {
            setVariables(created.getId(),
                    ImmutableMap.of(DEFAULT_RELATION, variables.stream().map(Variable::getId).collect(
                            Collectors.toSet())));
        }
        return created;

    }
}
