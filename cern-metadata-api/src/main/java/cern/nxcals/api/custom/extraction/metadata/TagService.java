package cern.nxcals.api.custom.extraction.metadata;

import cern.nxcals.api.custom.domain.Tag;
import cern.nxcals.api.custom.extraction.metadata.queries.Tags;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.Visibility;
import cern.nxcals.api.extraction.metadata.AbstractGroupService;
import cern.nxcals.common.annotation.Experimental;
import lombok.NonNull;

import java.time.Instant;
import java.util.Collection;

@Experimental
public interface TagService extends AbstractGroupService<Tag, Tags> {
    String DEFAULT_RELATION = "default_relation";
    String TAG_TIMESTAMP = "tag_timestamp";
    @SuppressWarnings("squid:S00107")
        // too many parameters
    Tag create(String name, String description, Visibility visibility, String owner, String system,
            Instant timestamp, @NonNull Collection<Entity> entities, @NonNull Collection<Variable> variables);
}
