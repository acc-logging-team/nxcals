package cern.nxcals.api.custom.extraction.metadata;

import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.common.annotation.Experimental;
import cern.nxcals.common.utils.Lazy;
import lombok.experimental.UtilityClass;

@UtilityClass
@Experimental
public class SnapshotServiceFactory {
    private static final Lazy<SnapshotService> SNAPSHOT_SERVICE_LAZY = new Lazy<>(() -> new SnapshotServiceImpl(ServiceClientFactory.createGroupService(), ServiceClientFactory.createSystemSpecService()));

    public static SnapshotService createSnapshotService() {
        return SNAPSHOT_SERVICE_LAZY.get();
    }
}
