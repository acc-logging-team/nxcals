package cern.nxcals.api.custom.extraction.metadata;

import cern.nxcals.api.custom.domain.Snapshot;
import cern.nxcals.api.custom.extraction.metadata.queries.Snapshots;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.extraction.metadata.AbstractGroupService;
import cern.nxcals.common.annotation.Experimental;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;

import java.util.Collection;

@Experimental
public interface SnapshotService extends AbstractGroupService<Snapshot, Snapshots> {

    @Getter
    @AllArgsConstructor
    enum AssociationType {
        DEFAULT("getSelectedVariables"),
        X_AXIS_VARIABLE("getXAxisVariable"),
        FUNDAMENTAL_FILTERS("getFundamentalFilters"),
        DRIVING_VARIABLE("getDrivingVariable");

        private final String value;
    }

    Snapshot create(Snapshot snapshot, @NonNull Collection<Entity> entities,
                    @NonNull Collection<Variable> variables,
                    @NonNull Collection<Variable> xAxisVariables,
                    @NonNull Collection<Variable> fundamentalFiltersVariables,
                    @NonNull Collection<Variable> drivingVariables);
}
