#!/usr/bin/env bash



for file in $(ls ansible/inventory/*/hosts.yml); do
HOST=$(grep "\[prometheus\]" -A 1 $file | grep -v "\[prometheus\]")

    if [[ !  -z  $HOST  ]]; then
       ENV=$(basename $(dirname $file) | awk '{print toupper($0)}')
       URL="http://$HOST:9191/api/v1/query?query=ALERTS%7Balertstate='firing'%7D"
       echo "$ENV *********************************"
#       echo "$ENV: ($HOST - $URL)"
       ALARMS=$(curl --connect-timeout 3 -m 3 -s "$URL" | json_reformat | grep alertname | sort | uniq | awk '{print $2}' | tr -d '",')

       if [[ !  -z  $ALARMS  ]]; then

        echo "ALARMS:"
        echo $ALARMS | tr -s ' ' '\n'
       else
        echo "OK"
       fi

       echo
    fi


done

