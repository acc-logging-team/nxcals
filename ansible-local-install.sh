#!/usr/bin/env bash
GRADLE_OPTS=-Xmx2g
VERSION=$(cat gradle.properties | grep version | awk 'BEGIN { FS="="} { print $2;}')
PYTIMBER_MAJOR_VERSION=$(cat gradle.properties | grep pytimberMajorVersion | awk 'BEGIN { FS="="} { print $2;}')
ANSIBLE_PARAMS_AUX=
GRADLE_PARAMS_AUX=
INSTALL=1
FILE=nxcals-full-install-with-sudo.yml

while [[ "$#" -gt 0 ]]; do
    case $1 in
        -a|--ansible) ANSIBLE_PARAMS_AUX="$ANSIBLE_PARAMS_AUX $2"; shift ;;
        -g|--gradle) GRADLE_PARAMS_AUX="$GRADLE_PARAMS_AUX $2"; shift ;;
        -f|--file) FILE="$2"; shift ;;
        -si|--skip-install) INSTALL=0 ;;
         -h|--help)
        echo "-----------------------------------------"
        echo "Usage: ansible-local-install.sh <options>"
        echo "-----------------------------------------"
        echo "Options: "
        echo "-a|--ansible <some ansible params> - additional parameters passed to Ansible"
        echo "-f|--file file to run from ansible directory"
        echo "-g|--gradle <some gradle params> - additional parameters passed to Gradle"
        echo "-si|--skip-install - does not run gradle clean install"
        echo "-h|--help - displays help"
        echo "-----------------------------------------"
        exit 0 ;;
        *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
    shift
done



INVENTORY=inventory/dev-$USER
echo "**** Using inventory $INVENTORY with $FILE for install version $VERSION with additional arguments: ${@:2} ****"
echo "If you want to cancel press CTRL+C now..."
sleep 3

if [ "$INSTALL" -eq "1" ]; then
    echo "-----------------------------------------"
    echo "**** Running ./gradlew clean test jacocoTestReport sonar install --info --parallel --no-daemon $GRADLE_PARAMS_AUX first..."
    echo "-----------------------------------------"
    ./gradlew clean test jacocoTestReport sonar install --info --parallel --no-daemon $GRADLE_PARAMS_AUX
fi


if [[ ! $? -eq 0 ]]; then
 echo "gradle build FAILED"
 exit $?
fi

cd ansible;
#Ansible treats values of the extra variables as strings. To pass values that are not strings, we need to use JSON format.
ansible-playbook -i $INVENTORY $FILE --vault-pass-file .pass -u $USER -e application_version="$VERSION" -e pytimber_major_version="$PYTIMBER_MAJOR_VERSION" $ANSIBLE_PARAMS_AUX;
cd ..
