#!/usr/bin/env bash

# Unset previous Python
if ! command -v deactivate &> /dev/null
then
    echo "deactivate command could not be found"
else
    deactivate
fi
unset PYTHONPATH

source "$1"/bin/activate
echo "PYTHONPATH=$PYTHONPATH"

shift

echo "Running $@"
exec "$@"
