#!/usr/bin/env bash
set -euo pipefail

VERSION="${VERSION:-"$(cat gradle.properties | grep version | awk 'BEGIN { FS="="} { print $2;}')"}"
PYTIMBER_MAJOR_VERSION=$(cat gradle.properties | grep pytimberMajorVersion | awk 'BEGIN { FS="="} { print $2;}')

INVENTORY="${INVENTORY:-inventory/dev-$USER}"

(cd ansible && ansible --inventory="$INVENTORY" --vault-password-file=.pass -e application_version="$VERSION" -e pytimber_major_version="$PYTIMBER_MAJOR_VERSION" "$@")
