#!/usr/bin/env bash
set -euo pipefail
#useful for bash debug, please leave commented out
#set -x

source test_utils.sh

ANSIBLE=1
INSTALL=1
REMOVE_CONFIG=1
PROMETHEUS_TESTS=1
THIN_API_TESTS=1
PYTHON_TESTS=1
BASE_TESTS=1
AUTH_TESTS=1
BASE_TESTS_STATUS=0
THIN_API_TESTS_STATUS=0
PYTHON_TESTS_STATUS=0
PROMETHEUS_TESTS_STATUS=0
AUTH_TESTS_STATUS=0
GRADLE_TESTS=
ANSIBLE_PARAMS_AUX=
GRADLE_PARAMS_AUX=

while [[ "$#" -gt 0 ]]; do
	case "$1" in
		-t|--tests) GRADLE_TESTS="$GRADLE_TESTS --tests $2"; shift ;;
		-a|--ansible) ANSIBLE_PARAMS_AUX="$ANSIBLE_PARAMS_AUX $2"; shift ;;
		-g|--gradle) GRADLE_PARAMS_AUX="$GRADLE_PARAMS_AUX $2"; shift ;;
		-sr|--skip-remove-config) REMOVE_CONFIG=0 ;;
		-si|--skip-install) INSTALL=0 ;;
		-sa|--skip-ansible) ANSIBLE=0 ;;
		-sp|--skip-prometheus-tests) PROMETHEUS_TESTS=0 ;;
		-thin|--thin-api-only) PYTHON_TESTS=0; BASE_TESTS=0; PROMETHEUS_TESTS=0; AUTH_TESTS=0 ;;
		-python|--python-api-only) BASE_TESTS=0; THIN_API_TESTS=0; PROMETHEUS_TESTS=0; AUTH_TESTS=0 ;;
		-base|--base-tests-only) PYTHON_TESTS=0; THIN_API_TESTS=0; PROMETHEUS_TESTS=0; AUTH_TESTS=0 ;;
		-op|--only-prometheus-test) BASE_TESTS=0; PYTHON_TESTS=0; THIN_API_TESTS=0; AUTH_TESTS=0 ;;
		-auth|--auth-tests-only) BASE_TESTS=0; PYTHON_TESTS=0; THIN_API_TESTS=0; PROMETHEUS_TESTS=0 ;;
		-h|--help)
		<<-EOF cat
		-----------------------------------------
		Usage: integration-tests.sh <options>
		-----------------------------------------
		Options:
		-t|--tests <test classes only> - name of the test classes to execute by Gradle
		           For selecting specific tests please refer to : https://docs.gradle.org/current/userguide/java_testing.html
		-a|--ansible <some ansible params> - additional parameters passed to Ansible
		-g|--gradle <some gradle params> - additional parameters passed to Gradle
		-sr|--skip-remove-config - does not remove generated application.yml files
		-si|--skip-install - does not run gradle clean install
		-sa|--skip-ansible - does not run ansible to generate application.yml files
		-sp|--skip-prometheus-tests - does not run the prometheus tests
		-op|--only-prometheus-test - run only the prometheus tests
		-thin|--thin-api-only - Thin API tests only (Spark Server)
		-python|--python-api-only - Python tests only
		-base|--base-tests-only - base tests only (no python nor Thin API for Spark Server)
		-op|--only-prometheus-test - only Prometheus tests
		-auth|--auth-tests-only - only authorization tests
		-h|--help - displays help
		-----------------------------------------
		EOF
		exit 0 ;;
		*) echo "Unknown parameter passed: $1"; exit 1 ;;
	esac
	shift
done

if [ -z "${CI_ENV:-}" ]; then
	API_STABILITY=false
	INVENTORY=inventory/dev-"$USER"
	SERVICE_URL=https://nxcals-"$USER"-1.cern.ch:19093,https://nxcals-"$USER"-2.cern.ch:19093
	KAFKA_SERVERS_URL=https://nxcals-"$USER"-3.cern.ch:9092,https://nxcals-"$USER"-4.cern.ch:9092,https://nxcals-"$USER"-5.cern.ch:9092
	SPARK_SERVERS_URL=nxcals-"$USER"-1.cern.ch:15000,nxcals-"$USER"-2.cern.ch:15000,nxcals-"$USER"-3.cern.ch:15000,nxcals-"$USER"-4.cern.ch:15000,nxcals-"$USER"-5.cern.ch:15000
	NAMESPACE=nxcals_dev_"$USER"
	VAULT_PASS=.pass
	HOME=/tmp/"$USER"/nxcals_integration_tests
	RBAC_ENV=test
else
	HOME=/tmp/"$USER"/nxcals_integration_tests
	INVENTORY=inventory/"$CI_ENV"
	NAMESPACE=nxcals_"$CI_ENV"
fi


if [ ! -d './ansible/ansible-common/roles' ]; then
	<<-EOF cat
	-----------------------------------------
	!!!! Ansible common directory does not exist, have to run gradle install first
	-----------------------------------------
	EOF
	INSTALL=1
fi

if [ "$PYTHON_TESTS" -eq 1 ]; then
	FILE=integration-tests-install.yml
else
	FILE=integration-tests-base-install.yml
fi


set_nxcals_version
set_pytimber_version
set_hadoop_cluster

if [ "$INSTALL" -eq 1  ] && [ "$PYTHON_TESTS" -eq 1 ]; then
	<<-EOF cat
	-----------------------------------------
	**** Running gradle clean install -x test --no-daemon --parallel -q -x buildDocs -x :nxcals-docs:publishMavenJavaPublicationToMavenLocal first...
	-----------------------------------------
	EOF
	./gradlew clean install -x test --no-daemon --parallel -q -x buildDocs -x :nxcals-docs:publishMavenJavaPublicationToMavenLocal
fi

# needed, because otherwise ansible complains and won't use ansible.cfg from world-writable dir
if [ "${CI_ENV:-}" ]; then
  chmod 744 ansible
fi


run_ansible
GRADLE_PARAMS="$GRADLE_TESTS $GRADLE_PARAMS_AUX"

#	Setting the user to 'acclog' - all the tests below should run under this user.
USER=acclog

# Run Prometheus tests first to avoid false-positive alerts, generated from the actual integration tests
if [ "$PROMETHEUS_TESTS" -eq 1 ]; then
	run_gradle_krb_keytab prometheusIntegrationTest || PROMETHEUS_TESTS_STATUS="$?"
fi

if [ "$BASE_TESTS" -eq 1 ]; then
	run_gradle_krb_keytab integrationTest || BASE_TESTS_STATUS="$?"
fi

if [ "$THIN_API_TESTS" -eq 1 ]; then
	run_gradle_krb_keytab thinApiIntegrationTest || THIN_API_TESTS_STATUS="$?"
fi

if [ "$PYTHON_TESTS" -eq 1 ]; then
	run_gradle_krb_keytab pythonIntegrationTests --parallel || PYTHON_TESTS_STATUS="$?"
fi

if [ "$AUTH_TESTS" -eq 1 ]; then
	run_gradle_krb_keytab authorizationIntegrationTest || AUTH_TESTS_STATUS="$(( $AUTH_TESTS_STATUS + $? ))"
	run_gradle_krb_tgt authorizationIntegrationTest || AUTH_TESTS_STATUS="$(( $AUTH_TESTS_STATUS + $? ))"
	run_gradle_krb_disabled authorizationIntegrationTest || AUTH_TESTS_STATUS="$(( $AUTH_TESTS_STATUS + $? ))"
fi


if [ "$REMOVE_CONFIG" -eq 1 ]; then
	git checkout -- integration-tests/src/{,thin-api-,prometheus-}integration-test/resources/application.yml
	git checkout -- integration-tests/src/authorization-test/resources/application.properties

	if [ -d /tmp/"$USER"/nxcals_integration_tests ]; then
		rm -rf /tmp/"$USER"/nxcals_integration_tests
	fi
	<<-EOF cat
	-----------------------------------------
	Generated configs and /tmp dirs removed
	-----------------------------------------
	EOF
else
	<<-EOF cat
	-----------------------------------------
	!!! Generated configs and /tmp dirs NOT REMOVED, beware of passwords and git commits
	-----------------------------------------
	EOF
fi

TOTAL="$(( $BASE_TESTS_STATUS + $THIN_API_TESTS_STATUS + $PYTHON_TESTS_STATUS + $PROMETHEUS_TESTS_STATUS + $AUTH_TESTS_STATUS ))"

if [ "$TOTAL" -gt 0 ]; then
	<<-EOF cat
	-----------------------------------------
	!!! BUILD FAILED:
	-----------------------------------------
	EOF
else
	<<-EOF cat
	-----------------------------------------
	BUILD SUCCESSFUL:
	-----------------------------------------
	EOF
fi

<<-EOF cat
BASE TESTS STATUS: $BASE_TESTS_STATUS
PROMETHEURS TESTS STATUS: $PROMETHEUS_TESTS_STATUS
THIN API TESTS STATUS: $THIN_API_TESTS_STATUS
PYTHON TESTS STATUS: $PYTHON_TESTS_STATUS
AUTH TESTS STATUS: $AUTH_TESTS_STATUS
(0 == OK, >0 == FAILED)
-----------------------------------------
EOF

exit "$TOTAL"
