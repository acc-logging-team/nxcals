package cern.nxcals.monitoring.producer.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by jwozniak on 30/09/17.
 */
@ConfigurationProperties("nxcals.producer")
@Validated
@Data
@NoArgsConstructor
public class ProducerConfiguration {
    @Valid
    private int maxAttempts;
    @Valid
    private List<RecordConfig> records;
}
