/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.monitoring.producer.config;

import cern.nxcals.monitoring.producer.domain.ProducerConfiguration;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.support.DefaultConversionService;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;

@EnableConfigurationProperties(ProducerConfiguration.class)
@Configuration
@SuppressWarnings("squid:S1118") //utility class private constructor (this class is not a utility)
public class SpringConfig {

    @Bean
    public static ConversionService conversionService() {
        return new DefaultConversionService();
    }

    @Bean
    public ScheduledExecutorService scheduledExecutor() {
        return Executors.newScheduledThreadPool(1, threadFactory("scheduler-%d"));
    }

    @Bean
    public Executor publisherExecutor(ProducerConfiguration config) {
        return Executors.newFixedThreadPool(Math.min(config.getRecords().size(), 10), threadFactory("publisher-%d"));
    }

    private ThreadFactory threadFactory(String pattern) {
        return new ThreadFactoryBuilder().setNameFormat(pattern).build();
    }
}
