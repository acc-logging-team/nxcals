package cern.nxcals.monitoring.producer.service.generators;

import cern.cmw.datax.DataBuilder;
import cern.nxcals.api.utils.TimeUtils;
import org.springframework.stereotype.Component;

import java.time.ZoneId;

/**
 * Created by jwozniak on 10/01/17.
 */
@Component("monitoring-data")
public class MonitoringDataGenerator extends AbstractDataGenerator {
    private static final String RANDOM_LONG_FIELD_PREFIX = "randomLongField";

    private String getRandomFieldName(long timestamp) {
        return RANDOM_LONG_FIELD_PREFIX + TimeUtils.getInstantFromNanos(timestamp).atZone(ZoneId.of("UTC"))
                .getDayOfMonth();
    }

    @SuppressWarnings("squid:S3878") // some varargs issue but this is not applicable here.
    protected DataBuilder addCustomFields(DataBuilder builder, int scale, long timestamp) {
        return builder
                .add("special-character", "test")
                .add("boolField", random.nextBoolean())
                .add("byteField1", (byte) 2)
                .add("shortField1", (short) 1)
                .add("intField1", random.nextInt())
                .add("longField1", random.nextLong())
                .add("longField2", 1L)

                //those 2 fields should not be checked for logging
                .add("longField3_DoNotLog", 1L)
                .add("longField4_DoNotLog", 1L)
                //This is a random field that is changed to generate schema changes, this is not logged anyway
                //as it is random name and not visible nor set in the CCDB editor.
                //This field will nevertheless be in the schema and change the schema if changed.
                .add(getRandomFieldName(timestamp), 0)

                .add("floatField1", random.nextFloat())
                .add("doubleField", random.nextDouble())
                .add("stringField1", "string value" + random.nextInt())

                .add("boolArrayField", new boolean[] { random.nextBoolean(), false, true })
                .add("byteArrayField", new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 })
                .add("shortArrayField", new short[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 })
                .add("intArrayField", new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 })
                .add("longArrayField", new long[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 })
                .add("floatArrayField", new float[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 })
                .add("doubleArrayField", new double[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 })
                .add("stringArrayField", new String[] { "aaa", "bbb" })

                .add("boolArray2DField1", new boolean[] { true, false }, new int[] { 1, 2 })
                .add("byteArray2DField2", new byte[] { 1, 2 }, new int[] { 1, 2 })
                .add("shortArray2DField2", new short[] { 1, 2 }, new int[] { 1, 2 })
                .add("intArray2DField2", new int[] { 1, 2 }, new int[] { 1, 2 })
                .add("longArray2DField2", new long[] { 1, 2 }, new int[] { 1, 2 })
                .add("doubleArray2DField2", new double[] { 1, 2 }, new int[] { 1, 2 })
                .add("floatArray2DField2", new float[] { 1, 2 }, new int[] { 1, 2 })
                .add("stringArray2DField2", new String[] { "aaaa", "bbbb" }, new int[] { 1, 2 });
    }
}
