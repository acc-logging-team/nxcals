package cern.nxcals.monitoring.producer.service.generators;

import cern.cmw.datax.DataBuilder;
import org.springframework.stereotype.Component;

@Component("winccoa-data")
public class WinccoaDataGenerator extends AbstractDataGenerator {
    @Override
    protected DataBuilder addCustomFields(DataBuilder builder, int scale, long timestamp) {
        return builder
                .add("value", 10000 * random.nextDouble());
    }
}
