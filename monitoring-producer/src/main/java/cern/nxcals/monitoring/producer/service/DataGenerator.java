package cern.nxcals.monitoring.producer.service;

import cern.cmw.datax.ImmutableData;

import java.util.Map;

/**
 * Created by jwozniak on 10/01/17.
 */
public interface DataGenerator {
    ImmutableData generateData(Map<String, String> requiredKeyValues, String timestampKeyName, long timestamp, int scale);
}
