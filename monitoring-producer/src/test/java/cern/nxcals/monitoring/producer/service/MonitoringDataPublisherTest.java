package cern.nxcals.monitoring.producer.service;

import cern.cmw.datax.ImmutableData;
import cern.nxcals.api.ingestion.Publisher;
import cern.nxcals.api.ingestion.Result;
import cern.nxcals.monitoring.producer.domain.ProducerConfiguration;
import cern.nxcals.monitoring.producer.domain.RecordConfig;
import cern.nxcals.monitoring.producer.service.generators.MonitoringDataGenerator;
import com.google.common.collect.ImmutableMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.Duration;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadLocalRandom;

import static java.util.Collections.singletonList;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MonitoringDataPublisherTest {
    private Publisher<ImmutableData> ingestionPublisher;
    private ProducerConfiguration config;
    private RecordConfig recordConfig;
    private CompletableFuture<Result> result;
    private int maxAttempts;

    @BeforeEach
    public void init() {
        recordConfig = new RecordConfig();
        recordConfig.setRecordKeys(ImmutableMap.of("test1", "value1"));
        recordConfig.setSystem("TEST-SYSTEM");
        recordConfig.setDataFlavour("my_generator");
        recordConfig.setTimestampKeyName("testTimestamp");
        recordConfig.setMessageCount(60L);
        recordConfig.setDuration(Duration.ofMinutes(1));
        recordConfig.setSleepTimeAfterFirstMessage(0L);
        recordConfig.setSleepTimeAfterNextMessage(0L);

        maxAttempts = 10;
        config = new ProducerConfiguration();
        config.setRecords(singletonList(recordConfig));
        config.setMaxAttempts(maxAttempts);

        result = new CompletableFuture<>();
        ingestionPublisher = mock(Publisher.class);
        when(ingestionPublisher.publishAsync(any(), any())).thenReturn(result);
    }

    @ParameterizedTest
    @MethodSource("completeExceptionallyOrNot")
    public void shouldPublishAllWithoutWaitingMultipleRecords(boolean exceptionally) {
        DataGenerator dataGenerator = new MonitoringDataGenerator();
        MonitoringDataPublisher dataPublisher = generator(dataGenerator);

        if (exceptionally) {
            result.completeExceptionally(new IllegalArgumentException("bu"));
        } else {
            result.complete(null);
        }
        int count = ThreadLocalRandom.current().nextInt(100) + 10;
        for (int i = 0; i < count; i++) {
            dataPublisher.sendValues(this.recordConfig);
        }
        if (exceptionally) {
            //should have 10 x count sent via ingestionPublisher, because 10 are the max attempts when a message fails to get sent
            verify(ingestionPublisher, times(maxAttempts * count)).publishAsync(any(), any());
        } else {
            //should have 60 x count sent via ingestionPublisher
            verify(ingestionPublisher, times(60 * count)).publishAsync(any(), any());
        }
    }

    private static Object[][] completeExceptionallyOrNot() {
        return new Object[][] { new Object[] { true }, new Object[] { false } };
    }

    private MonitoringDataPublisher generator(DataGenerator dataGenerator) {
        return new MonitoringDataPublisher(ImmutableMap.of("my_generator", dataGenerator), config,
                s -> ingestionPublisher, Runnable::run, mock(ScheduledExecutorService.class));
    }

}
