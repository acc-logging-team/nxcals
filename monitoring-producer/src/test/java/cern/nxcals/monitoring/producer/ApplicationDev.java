/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.monitoring.producer;

import cern.nxcals.monitoring.producer.service.DataPublisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class ApplicationDev {
    static {
        System.setProperty("org.apache.logging.log4j.simplelog.StatusLogger.level", "TRACE");
        System.setProperty("logging.config", "classpath:monitoring-producer-log4j2.yml");

        System.setProperty("service.url",
                "https://cs-ccr-nxcalstb1.cern.ch:19093,https://cs-ccr-nxcalstb2.cern.ch:19093");
        System.setProperty("kafka.producer.bootstrap.servers",
                "nxcals-testbed-kafka1.cern.ch:9092,nxcals-testbed-kafka2.cern.ch:9092,nxcals-testbed-kafka3.cern.ch:9092");
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationDev.class);

    public static void main(String[] args) throws Exception {
        try {

            SpringApplication app = new SpringApplication(ApplicationDev.class);
            app.addListeners(new ApplicationPidFileWriter());
            ApplicationContext ctx = app.run();

            DataPublisher dataPublisher = ctx.getBean(DataPublisher.class);
            dataPublisher.start();

        } catch (Throwable e) {
            e.printStackTrace();
            LOGGER.error("Exception while running publisher app", e);
            System.exit(1);
        }
    }

}
