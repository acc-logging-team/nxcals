/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.compaction.service;

import cern.nxcals.common.domain.DataProcessingJob;
import cern.nxcals.common.domain.DataProcessingJob.JobType;
import cern.nxcals.common.metrics.MetricsRegistry;
import cern.nxcals.common.utils.Lazy;
import cern.nxcals.compaction.config.CompactorProperties;
import cern.nxcals.compaction.metrics.CompactionMetrics;
import cern.nxcals.compaction.processor.JobProcessor;
import cern.nxcals.compaction.processor.ProcessingStatus;
import com.google.common.annotations.VisibleForTesting;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.apache.spark.sql.SparkSession;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * A main class that is used to run the whole compaction process.
 */
@Service
@Slf4j
public class Compactor {

    @lombok.Value
    @EqualsAndHashCode(onlyExplicitlyIncluded = true)
    private static class JobWithTime {
        long startTime;
        @EqualsAndHashCode.Include
        DataProcessingJob job;

        private long getRunningTime() {
            return System.currentTimeMillis() - startTime;
        }

        public String toString() {
            return String.format("job %s running %s", job, runningTime(startTime));
        }
    }

    private final Executor executor;
    private final Queue<Runnable> queue;
    private final ScheduledExecutorService scheduler;
    private final CompactorProperties properties;
    //order of processors is assured because of the Enum & EnumMap (as declared in the Enum JobType) (jwozniak)
    private final Map<JobType, JobProcessor<?>> jobProcessors = new EnumMap<>(JobType.class);
    private final Lazy<SparkSession> sparkSessionLazy;

    private final CompactionMetrics metricsRegistry;
    @VisibleForTesting
    @Setter(AccessLevel.PACKAGE)
    private Runnable exitHandler;

    @VisibleForTesting
    @Getter(AccessLevel.PACKAGE)
    private final ProcessingStatus totalStatus = ProcessingStatus.nothing();
    private final Set<DataProcessingJob> jobsToProcess = ConcurrentHashMap.newKeySet();
    private final Set<JobWithTime> jobsInProcessing = ConcurrentHashMap.newKeySet();
    private final JobProvider jobProvider;

    @Autowired
    public Compactor(@NonNull List<JobProcessor<?>> jobProcessors,
            @NonNull Executor executor,
            @NonNull Queue<Runnable> executorQueue,
            @NonNull ScheduledExecutorService scheduler, @NonNull MetricsRegistry metricsRegistry,
            @NonNull Lazy<SparkSession> sparkSessionLazy,
            @NonNull JobProvider jobProvider,
            @NonNull CompactorProperties properties) {
        if (CollectionUtils.isEmpty(jobProcessors)) {
            throw new IllegalArgumentException("No job processors were provided!");
        }
        this.jobProcessors.putAll(jobProcessors.stream()
                .collect(Collectors.toMap(JobProcessor::getJobType, Function.identity())));
        this.executor = executor;
        this.queue = executorQueue;
        this.scheduler = scheduler;
        this.sparkSessionLazy = sparkSessionLazy;
        this.jobProvider = jobProvider;
        this.properties = properties;
        this.metricsRegistry = new CompactionMetrics(metricsRegistry);
        this.exitHandler = () -> System.exit(-1);
    }

    public void run() {
        metricsRegistry.export(ProcessingStatus.nothing()); //initialize metrics
        process();
    }

    private void process() {
        try {
            //check if Spark is still ok
            verifySpark();
            //log information about what is currently processed and total status of all
            logStatus();
            //if necessary add new compaction tasks from the service
            addCompactionTasks();
            //schedule next execution of this method
            scheduleExecution();
        } catch (Exception ex) {
            log.error("Uncaught exception while processing jobs, should not happen, exiting...", ex);
            exitHandler.run();
        }
    }

    private void verifySpark() {
        SparkSession sparkSession = sparkSessionLazy.get();
        if (sparkSession.sparkContext().isStopped()) {
            log.error("SparkContext is closed, cannot proceed further, exiting...");
            exitHandler.run();
        }
    }

    private void scheduleExecution() {
        if (queue.isEmpty()) {
            //queue still empty, no jobs found
            scheduler.schedule(this::process, properties.getNoJobsSleep().toMillis(), TimeUnit.MILLISECONDS);
            log.info("No jobs found, waiting {} for tasks to accumulate", properties.getNoJobsSleep());
        } else {
            scheduler.schedule(this::process, properties.getExecuteEvery().toMillis(), TimeUnit.MILLISECONDS);
        }
    }

    private void addCompactionTasks() {
        if (queue.isEmpty()) {
            //add additional jobs if queue is empty
            int availableJobs = properties.getMaxJobs();
            for (JobProcessor<?> processor : jobProcessors.values()) {
                if (availableJobs > 0) {
                    availableJobs -= addJobs(availableJobs, processor);
                }
            }
        }
    }

    private void logStatus() {
        metricsRegistry.heartbeat();
        metricsRegistry.export(totalStatus);
        log.info("Current executor queue size: {} jobs, in processing: {} jobs, total: {}", queue.size(),
                jobsInProcessing.size(), totalStatus);
        if (!jobsInProcessing.isEmpty()) {
            log.info("****** Jobs in processing *******");
            jobsInProcessing.stream()
                    .sorted(Comparator.comparing(JobWithTime::getRunningTime).reversed())
                    .forEach(job -> log.info("********* Status: {}", job));
        }
    }

    @VisibleForTesting
    <T extends DataProcessingJob> int addJobs(int maxJobs, JobProcessor<T> processor) {
        Set<T> jobs = jobProvider.getJobs(maxJobs, processor.getJobType());
        if (jobs.isEmpty()) {
            log.info("No compaction jobs of type [{}] found", processor.getJobType());
            return 0;
        }
        log.info("Found {} jobs of type [{}] - adding to execution queue", jobs.size(), processor.getJobType());
        AtomicInteger count = new AtomicInteger(0);

        jobs.stream()
                .filter(job -> !jobsToProcess.contains(
                        job)) //should filter out those that are already registered to process
                .forEach(job -> {
                    count.incrementAndGet();
                    jobsToProcess.add(job);
                    executor.execute(createTask(processor, job));
                });

        log.info("Finished adding {} jobs for execution", count.get());
        return count.get();
    }

    private <T extends DataProcessingJob> @NotNull Runnable createTask(JobProcessor<T> processor, T job) {
        return () -> {
            JobWithTime jobWithTime = new JobWithTime(System.currentTimeMillis(), job);
            try {
                jobsInProcessing.add(jobWithTime);
                //process using processor
                ProcessingStatus status = processor.process(job);

                log.info("Completed job ({}) of type [{}] and {} ", job.getSourceDir(), job.getType(),
                        format(status, jobWithTime.getStartTime()));
                totalStatus.add(status);
            } catch (Exception exception) {
                log.warn(String.format("Failed to complete job (%s) of type [%s]", job.getSourceDir().toString(),
                        job.getType().toString()), exception);
                totalStatus.add(ProcessingStatus.failure());
            } finally {
                jobsToProcess.remove(job);
                jobsInProcessing.remove(jobWithTime);
                metricsRegistry.heartbeat();
            }
        };
    }

    private static String format(ProcessingStatus status, long startTime) {
        return String.format("processed %d files (%.2f Mb) in %s", status.getFileCount(),
                status.getByteCount() / (1024 * 1024.0), runningTime(startTime));
    }

    private static String runningTime(long startTime) {
        return DurationFormatUtils.formatDurationHMS(System.currentTimeMillis() - startTime);
    }

}
