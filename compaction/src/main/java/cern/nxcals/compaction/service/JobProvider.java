package cern.nxcals.compaction.service;

import cern.nxcals.common.domain.DataProcessingJob;
import cern.nxcals.internal.extraction.metadata.InternalCompactionService;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import static cern.nxcals.common.domain.DataProcessingJob.JobType;

/**
 * Abstraction for providing jobs to Compactor.
 **/
@Service
@RequiredArgsConstructor
public class JobProvider {
    private final InternalCompactionService compactionService;

    private final Map<JobType, Set<? extends DataProcessingJob>> savedJobs = Collections.synchronizedMap(
            new EnumMap<>(JobType.class));

    private @NotNull <T extends DataProcessingJob> Set<T> getComputed(JobType jobType) {
        return (Set<T>) this.savedJobs.computeIfAbsent(jobType, k -> ConcurrentHashMap.newKeySet());
    }

    <T extends DataProcessingJob> Set<T> getJobs(int maxJobs, JobType jobType) {
        Set<T> retJobs = new HashSet<>();
        Set<T> jobs = getComputed(jobType);
        if (!jobs.isEmpty()) {
            jobs.stream().limit(maxJobs).forEach(retJobs::add);
            jobs.removeAll(retJobs);
        }

        //Adds jobs that are not already present (from HashSet.add() description).
        //Since saved jobs contain ones that failed, it will be possible for the call to compaction service to
        //return a job that is also in those saved ones (for a given day, but with larger content).
        //We prefer that from the saved in such a case (will be the smaller, divided one).
        if (retJobs.size() < maxJobs) {
            retJobs.addAll(compactionService.getDataProcessJobs(maxJobs - retJobs.size(), jobType));
        }
        return retJobs;
    }

    public <T extends DataProcessingJob> void addJob(JobType type, T job) {
        getComputed(type).add(job);
    }
}
