package cern.nxcals.compaction.processor;

import cern.nxcals.common.domain.DataProcessingJob;
import cern.nxcals.common.utils.Lazy;
import cern.nxcals.compaction.config.CompactorProperties;
import cern.nxcals.internal.extraction.metadata.InternalSystemSpecService;
import com.google.common.annotations.VisibleForTesting;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;

/**
 * Common abstraction for {@link AdaptiveCompactProcessor} and {@link CompactProcessor}, since process is the same in
 * both except type of writers
 *
 * @param <T>
 */
@Slf4j
public abstract class AbstractCompactProcessor<T extends DataProcessingJob> extends BaseJobProcessor<T> {
    @VisibleForTesting
    @Setter(AccessLevel.PACKAGE)
    private Function<List<Path>, Collection<Dataset<Row>>> frameLoader;

    protected AbstractCompactProcessor(@NonNull FileSystem fileSystem,
            @NonNull Lazy<SparkSession> ctx,
            @NonNull InternalSystemSpecService systemService,
            @NonNull CompactorProperties properties) {
        super(fileSystem, ctx, systemService, properties);
        frameLoader = this::loadDatasets;

    }

    @Override
    protected Path getOperationPath(T job) {
        return createTempCompactionPath(job, job.getFilePrefix());
    }

    @Override
    protected void applyActions(Path operationPath, T job) throws IOException {
        List<Path> stagingFiles = toPaths(job.getFiles());
        //It is very important to have those steps in that order as the process might fail at any moment.
        //By the order we assure that no data is lost in such event (as the compaction will be restarted on the same folder is files are there)
        compactFiles(operationPath, stagingFiles, job);
        moveCompactedFilesToDestination(stagingFiles, operationPath, job);
    }

    private void compactFiles(Path operationPath, List<Path> files, T job) {
        try {
            log.info("{} | Start with {} target output files", logPrefix(job), job.getPartitionCount());

            Collection<Dataset<Row>> frames = frameLoader.apply(files);
            log.debug("{} | {} files converted into {} datasets", logPrefix(job), files.size(), frames.size());

            Dataset<Row> dataset = frames.stream()
                    .reduce(Dataset::union)
                    .orElseThrow(() -> emptyDatasetException(job));

            saveDataset(operationPath, dataset, job);

            log.info("{} | Finished", logPrefix(job));
        } catch (Exception e) {
            throw genericException(job, e);
        }
    }

    protected abstract Collection<Dataset<Row>> loadDatasets(List<Path> paths);

    protected abstract void saveDataset(Path operationalPath, Dataset<Row> dataset, T job) throws IOException;
}
