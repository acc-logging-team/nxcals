package cern.nxcals.compaction.processor;

import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.common.domain.DataProcessingJob;
import cern.nxcals.common.domain.DataProcessingJob.JobType;
import cern.nxcals.common.domain.MergeCompactionJob;
import cern.nxcals.common.domain.RestageJob;
import cern.nxcals.common.utils.Lazy;
import cern.nxcals.compaction.config.CompactorProperties;
import cern.nxcals.compaction.processor.handler.HdfsFileWriter;
import cern.nxcals.compaction.processor.writer.BaseDatasetWriter;
import cern.nxcals.internal.extraction.metadata.InternalSystemSpecService;
import com.google.common.annotations.VisibleForTesting;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.DataTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.apache.spark.sql.functions.col;

/**
 * Represents an implementation of {@link JobProcessor}, capable of processing re-stage jobs that involves
 * re-staging of already stored data (in the form of parquet files).
 * This processor is being triggered before {@link MergeCompactionJob} and it's responsible for the following actions:
 * 1) Loads data files (parquet) as a Spark dataset
 * 2) Re-partitions the data files by hour (24-hour based, results to partitions from 00 to 23)
 * 3) Writes the newly hourly-partitioned dataset into a root re-staging directory by bucketing each partition to
 * it's corresponding directory (each directory is named with respect to it's partition)
 * 4) Writes a re-stage report that contains the original paths leading to data files that were re-staged.
 * Later, the {@link MergeCompactProcessor} will consult this report, to delete the original data files (parquet).
 *
 * @see BaseJobProcessor
 * @see RestageJob
 * @see MergeCompactProcessor
 */
@Component
@Slf4j
public class RestageProcessor extends BaseJobProcessor<RestageJob> {
    @VisibleForTesting
    static final String PARTITION_BY_HOUR_COLUMN = "tmp_partition_hour";
    private static final String HOUR_DIRECTORY_NAME_FORMAT = "%02d";

    @NonNull
    private final HdfsFileWriter reportFileWriter;

    @Autowired
    public RestageProcessor(@NonNull FileSystem fileSystem,
            @NonNull Lazy<SparkSession> ctx,
            @NonNull InternalSystemSpecService systemService,
            @NonNull CompactorProperties properties,
            @NonNull HdfsFileWriter reportFileWriter) {
        super(fileSystem, ctx, systemService, properties);
        this.reportFileWriter = reportFileWriter;
    }

    @Override
    public JobType getJobType() {
        return JobType.RESTAGE;
    }

    @Override
    protected void postProcess(Path operationPath, RestageJob job) {
        //do nothing
    }

    @Override
    protected Path getOperationPath(RestageJob job) {
        return new Path(job.getSourceDir());
    }

    @Override
    protected void applyActions(Path operationPath, RestageJob job) throws IOException {
        List<Path> dataFiles = toPaths(job.getDataFiles());
        if (CollectionUtils.isNotEmpty(dataFiles)) {
            log.debug("{} | Got {} parquet files on destination path {}, will proceed with individual processing",
                    logPrefix(job), dataFiles.size(), job.getDestinationDir());
            for (Path dataFile : dataFiles) {
                log.debug("{} | Repartitioning parquet data file with path {}", logPrefix(job), dataFile.toString());
                Dataset<Row> dataset = convertToDataFrameByType(Collections.singleton(dataFile)).values().stream()
                        .reduce(Dataset::union)
                        .orElseThrow(() -> emptyDatasetException(job));
                Map<String, Dataset<Row>> datasetByHour = repartitionByHourMap(job, dataset);
                if (log.isDebugEnabled()) {
                    long numOfHourlyPartitions = datasetByHour.values().stream().filter(ds -> !ds.isEmpty()).count();
                    log.debug(
                            "{} | Writing processed parquet file, now split on [{}] partitions to temp operation path {}",
                            logPrefix(job), numOfHourlyPartitions, operationPath.toString());
                }
                datasetByHour.forEach((h, ds) -> {
                    if (!ds.isEmpty()) {
                        BaseDatasetWriter.appendDataToFile(toHourDirPath(operationPath, h), ds);
                    }
                });
            }
        } else {
            log.info("No data files found for re-stage on path [{}]. Will produce empty report!",
                    job.getDestinationDir());
        }
        log.debug("{} | Writing re-stage report file to path {}", logPrefix(job), job.getSourceDir());
        writeRestagedFilesReport(job, dataFiles);
        log.info("{} | Finished", logPrefix(job));
    }

    private Dataset<Row> repartitionByHour(DataProcessingJob job, Dataset<Row> dataset) {
        String timestampField = getTimestampFieldsCache().get(job.getSystemId());
        Column epochTimestampCol = dataset.col(timestampField)
                .divide(functions.lit(TimeUtils.CONVERT_EPOCH_NANOS_TO_SECONDS_VALUE))
                .cast(DataTypes.TimestampType);
        return dataset.withColumn(PARTITION_BY_HOUR_COLUMN, functions.hour(epochTimestampCol))
                .repartition(col(PARTITION_BY_HOUR_COLUMN));
    }

    private Map<String, Dataset<Row>> repartitionByHourMap(DataProcessingJob job, Dataset<Row> dataset) {
        Dataset<Row> datasetPartitionedByHour = repartitionByHour(job, dataset);
        //remove redundant repartition column at the end of operation
        return IntStream.range(0, 24).boxed()
                .collect(Collectors.toMap(hour -> String.format(HOUR_DIRECTORY_NAME_FORMAT, hour),
                        hour -> datasetPartitionedByHour.where(col(PARTITION_BY_HOUR_COLUMN)
                                .equalTo(functions.lit(hour)))
                                .drop(PARTITION_BY_HOUR_COLUMN)));
    }

    private void writeRestagedFilesReport(RestageJob job, List<Path> dataFiles) throws IOException {
        Path restagedFilesReportPath = new Path(job.getRestagingReportFile());
        String content = dataFiles.stream().map(Path::toString)
                .collect(Collectors.joining(System.lineSeparator()));
        reportFileWriter.write(restagedFilesReportPath, content);
    }

    private String toHourDirPath(Path outputPath, String hour) {
        return Objects.requireNonNull(outputPath).toString() + Path.SEPARATOR + hour;
    }

}
