package cern.nxcals.compaction.processor.writer;

import cern.nxcals.api.domain.TimeEntityPartitionType;
import cern.nxcals.common.domain.AdaptiveCompactionJob;
import cern.nxcals.common.domain.AdaptiveMergeCompactionJob;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder(toBuilder = true)
public class AdaptiveDatasetWriterJob extends DatasetWriterJob {
    private final long parquetRowGroupSize;
    private final long hdfsBlockSize;

    private final TimeEntityPartitionType partitionType;

    @Override
    public int getPartitionCount() {
        return partitionType.slots();
    }

    public static AdaptiveDatasetWriterJob from(AdaptiveMergeCompactionJob job) {
        return AdaptiveDatasetWriterJob.builder()
                .sortEnabled(job.isSortEnabled())
                .systemId(job.getSystemId())
                .parquetRowGroupSize(job.getParquetRowGroupSize())
                .hdfsBlockSize(job.getHdfsBlockSize())
                .partitionType(job.getPartitionType())
                .build();
    }

    public static AdaptiveDatasetWriterJob from(AdaptiveCompactionJob job) {
        return AdaptiveDatasetWriterJob.builder()
                .sortEnabled(job.isSortEnabled())
                .systemId(job.getSystemId())
                .parquetRowGroupSize(job.getParquetRowGroupSize())
                .hdfsBlockSize(job.getHdfsBlockSize())
                .partitionType(job.getPartitionType())
                .build();
    }
}
