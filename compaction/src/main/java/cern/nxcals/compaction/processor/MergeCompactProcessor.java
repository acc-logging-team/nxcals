package cern.nxcals.compaction.processor;

import cern.nxcals.common.domain.DataProcessingJob.JobType;
import cern.nxcals.common.domain.MergeCompactionJob;
import cern.nxcals.common.domain.RestageJob;
import cern.nxcals.common.utils.Lazy;
import cern.nxcals.compaction.config.CompactorProperties;
import cern.nxcals.compaction.processor.handler.HdfsFileReader;
import cern.nxcals.compaction.processor.writer.BaseDatasetWriter;
import cern.nxcals.compaction.processor.writer.DatasetWriterJob;
import cern.nxcals.internal.extraction.metadata.InternalSystemSpecService;
import com.google.common.annotations.VisibleForTesting;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static cern.nxcals.common.SystemFields.NXC_ENTITY_ID;
import static org.apache.spark.sql.functions.coalesce;

/**
 * Represents an implementation of {@link JobProcessor}, capable of processing compactions jobs that involves
 * merging re-staged with already stored data (in the form of avro and parquet files).
 * This processor is being triggered as a follow-up of {@link RestageJob} and it's responsible for the following actions:
 * 1) Loads staged files (avro) as a Spark dataset
 * 2) Loads re-staged data files (parquet) as a Spark dataset
 * 3) Performs a join (full-outer) of the two datasets, with respect on avro files. By that, if any matching
 * row (based on timestamp) is found, it updates the parquet column values with the ones present on the avro file.
 * Any non-maching rows are treated as new entries and appended to the final dataset.
 * 4) Performs repartition and sort (if needed) to the resulted dataset
 * 5) Writes the newly partitioned dataset to an output path
 * 6) Consults the re-stage report file, placed previously from {@link RestageProcessor}, that contains a list of paths
 * leading to the original data files (parquet) and deletes them.
 *
 * @see BaseJobProcessor
 * @see MergeCompactionJob
 * @see RestageProcessor
 */
@Component
@Slf4j
public class MergeCompactProcessor<T extends MergeCompactionJob> extends BaseJobProcessor<T> {
    private static final String DATASET_JOIN_TYPE = "fullouter";
    private static final String ENTITY_ID_FIELD = NXC_ENTITY_ID.getValue();

    @NonNull
    private final HdfsFileReader<List<Path>> reportFileReader;
    @VisibleForTesting
    @Getter(AccessLevel.PACKAGE)
    private final BaseDatasetWriter writer;

    @Autowired
    public MergeCompactProcessor(@NonNull FileSystem fileSystem,
            @NonNull Lazy<SparkSession> ctx,
            @NonNull InternalSystemSpecService systemService,
            @NonNull CompactorProperties properties,
            @NonNull HdfsFileReader<List<Path>> reportFileReader) {
        super(fileSystem, ctx, systemService, properties);
        this.writer = new BaseDatasetWriter(getDeduplicateCache(), getTimestampFieldsCache());
        this.reportFileReader = reportFileReader;
    }

    @Override
    public JobType getJobType() {
        return JobType.MERGE_COMPACT;
    }


    @Override
    protected Path getOperationPath(T job) {
        return createTempCompactionPath(job, job.getFilePrefix());
    }

    @Override
    protected void applyActions(Path operationPath, T job) throws IOException {
        List<Path> stagingFiles = toPaths(job.getFiles());
        //It is very important to have those steps in that order as the process might fail at any moment.
        //By the order we assure that no data is lost in such event (as the compaction will be restarted on the same folder is files are there)
        mergeCompactFiles(operationPath, stagingFiles, job);
        moveCompactedFilesToDestination(stagingFiles, operationPath, job);
        deleteFiles(reportFileReader.read(new Path(job.getRestagingReportFile())), job);
    }

    protected void mergeCompactFiles(Path operationPath, List<Path> files, T job) {
        try {
            log.info("{} | Start with {} target output files", logPrefix(job), job.getPartitionCount());

            Map<FileTypeFormat, Dataset<Row>> frames = convertToDataFrameByType(files);
            log.debug("{} | {} files converted into {} datasets by type", logPrefix(job), files.size(), frames.size());
            if (CollectionUtils.isEmpty(frames.values())) {
                throw emptyDatasetException(job);
            }

            Dataset<Row> dataset = joinAndReplaceColumnsFromAvro(job,
                    frames.get(FileTypeFormat.PARQUET), frames.get(FileTypeFormat.AVRO));

            saveDataset(operationPath, dataset, job);

            log.info("{} | Finished", logPrefix(job));
        } catch (Exception e) {
            throw genericException(job, e);
        }
    }

    @VisibleForTesting
    Dataset<Row> joinAndReplaceColumnsFromAvro(MergeCompactionJob job, Dataset<Row> parquetDataset,
            Dataset<Row> avroDataset) {
        if (this.isNullOrEmpty(avroDataset)) {
            return parquetDataset; //avoid joining with empty avro dataset
        }
        if (this.isNullOrEmpty(parquetDataset)) {
            return avroDataset; //avoid joining with empty parquet dataset
        }
        Dataset<Row> joinedDs = parquetDataset
                .join(avroDataset, toJoinColumnExpr(job, parquetDataset, avroDataset), DATASET_JOIN_TYPE);
        for (String colName : parquetDataset.columns()) {
            String tmpFieldName = "tmp." + colName;
            joinedDs = joinedDs
                    .withColumn(tmpFieldName, coalesce(avroDataset.col(colName), parquetDataset.col(colName)))
                    .drop(colName).withColumnRenamed(tmpFieldName, colName);
        }
        return joinedDs;
    }

    private Column toJoinColumnExpr(MergeCompactionJob job, Dataset<Row> left, Dataset<Row> right) {
        String joinField = getTimestampFieldsCache().get(job.getSystemId());
        return left.col(joinField).equalTo(right.col(joinField))
                .and(left.col(ENTITY_ID_FIELD).equalTo(right.col(ENTITY_ID_FIELD)));
    }

    private boolean isNullOrEmpty(Dataset<Row> dataset) {
        return (dataset == null || dataset.isEmpty());
    }

    protected void saveDataset(Path operationPath, Dataset<Row> dataset, T job) throws IOException {
        writer.saveDataset(operationPath, dataset, DatasetWriterJob.from(job));
    }
}
