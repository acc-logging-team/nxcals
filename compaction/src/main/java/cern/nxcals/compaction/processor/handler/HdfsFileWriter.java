package cern.nxcals.compaction.processor.handler;

import org.apache.hadoop.fs.Path;

import java.io.IOException;

public interface HdfsFileWriter {
    void write(Path filePath, String content) throws IOException;
}
