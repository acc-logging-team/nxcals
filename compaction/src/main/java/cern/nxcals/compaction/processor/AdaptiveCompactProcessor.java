/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.compaction.processor;

import cern.nxcals.common.domain.AdaptiveCompactionJob;
import cern.nxcals.common.domain.DataProcessingJob.JobType;
import cern.nxcals.common.utils.HdfsFileUtils;
import cern.nxcals.common.utils.Lazy;
import cern.nxcals.compaction.config.CompactorProperties;
import cern.nxcals.compaction.processor.writer.AdaptiveDatasetWriter;
import cern.nxcals.compaction.processor.writer.AdaptiveDatasetWriterJob;
import cern.nxcals.compaction.service.JobProvider;
import cern.nxcals.internal.extraction.metadata.InternalSystemSpecService;
import com.google.common.annotations.VisibleForTesting;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Compacts the avro and parquet files in the directory.
 * If the process fails in the middle of operation there will be next compaction that actually will fix the duplicated files.
 * Using Adaptive Partitioning that partitions by time and buckets by entity id.
 * Also using adaptive row group sizes & hdfs block sizes.
 * Should only be included if enabled
 */
@Component
@Slf4j
public class AdaptiveCompactProcessor extends AbstractCompactProcessor<AdaptiveCompactionJob> {
    @VisibleForTesting
    @Setter(AccessLevel.PACKAGE)
    private AdaptiveDatasetWriter writer;
    private final JobProvider jobProvider;


    @Autowired
    public AdaptiveCompactProcessor(@NonNull FileSystem fileSystem, @NonNull Lazy<SparkSession> ctx,
            @NonNull InternalSystemSpecService systemService,
            @NonNull JobProvider jobProvider,
            @NonNull CompactorProperties properties) {
        super(fileSystem, ctx, systemService, properties);
        writer = new AdaptiveDatasetWriter(getDeduplicateCache(), getTimestampFieldsCache(), fileSystem);
        this.jobProvider = jobProvider;
    }

    @Override
    public JobType getJobType() {
        return JobType.ADAPTIVE_COMPACT;
    }

    protected Collection<Dataset<Row>> loadDatasets(List<Path> paths) {
        return convertToDataFrameByType(paths).values();
    }

    @Override
    protected void saveDataset(Path operationalPath, Dataset<Row> dataset, AdaptiveCompactionJob job)
            throws IOException {
        writer.saveDataset(operationalPath, dataset, AdaptiveDatasetWriterJob.from(job));
    }


    @Override
    protected boolean handleException(Exception e, AdaptiveCompactionJob job) {
        Optional<AdaptiveCompactionJob> jobOptional = splitJob(job);
        if (jobOptional.isPresent()) {
            AdaptiveCompactionJob newJob = jobOptional.get();
            log.info("Handling exception for job by splitting it, new job: {}", newJob);
            jobProvider.addJob(getJobType(), newJob);
            return true;
        } else {
            log.error("Cannot handle exception for job {}, splitting is not possible", job, e);
            return false;
        }
    }

    private Optional<AdaptiveCompactionJob> splitJob(AdaptiveCompactionJob job) {
        Map<Long, List<URI>> groupedByTimePartitions = job.getFiles().stream()
                .collect(Collectors.groupingBy(AdaptiveCompactProcessor::toTimePartition));
        if (groupedByTimePartitions.containsKey(Long.MIN_VALUE) || groupedByTimePartitions.size() == 1) {
            //What to do if this contains parquet files
            //(since those are already grouped by output time partition) or you cannot split further?
            return Optional.empty();
        } else {
            int max = groupedByTimePartitions.size() / 2;
            List<URI> newFiles = new ArrayList<>();
            groupedByTimePartitions.entrySet().stream()
                    .limit(max)
                    .forEach(entry -> newFiles.addAll(entry.getValue()));
            return Optional.of(job.toBuilder().files(newFiles).build());
        }
    }

    @VisibleForTesting
    static long toTimePartition(URI file) {
        return HdfsFileUtils.getTimePartition(new Path(file)).orElse(Long.MIN_VALUE);
    }

}
