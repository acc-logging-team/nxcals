package cern.nxcals.compaction.processor;

import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.extraction.metadata.queries.SystemSpecs;
import cern.nxcals.common.domain.DataProcessingJob;
import cern.nxcals.common.utils.HdfsFileUtils;
import cern.nxcals.common.utils.Lazy;
import cern.nxcals.compaction.SparkContextStoppedRuntimeException;
import cern.nxcals.compaction.config.CompactorProperties;
import cern.nxcals.internal.extraction.metadata.InternalSystemSpecService;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.avro.Schema;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.permission.FsPermission;
import org.apache.spark.sql.DataFrameReader;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;

import static cern.nxcals.common.SystemFields.NXC_ENTITY_ID;

@Slf4j
public abstract class BaseJobProcessor<T extends DataProcessingJob> implements JobProcessor<T> {
    @AllArgsConstructor
    protected enum FileTypeFormat {
        AVRO("avro"), PARQUET("parquet"), INVALID(null);

        private final String fileFormat;

        static FileTypeFormat valueOf(Path file) {
            if (HdfsFileUtils.isAvroDataFile(file)) {
                return FileTypeFormat.AVRO;
            } else if (HdfsFileUtils.isParquetDataFile(file)) {
                return FileTypeFormat.PARQUET;
            } else {
                return FileTypeFormat.INVALID;
            }
        }
    }

    private static final int MAXIMUM_CACHE_SIZE = 20;
    protected static final FsPermission READ_ONLY = new FsPermission("444");

    private final String compactionDir;
    private final Lazy<SparkSession> sparkSessionLazy;
    private final InternalSystemSpecService systemService;

    @Getter
    @NonNull
    private final FileSystem fs;

    @Getter(AccessLevel.PROTECTED)
    private final LoadingCache<Long, Set<String>> deduplicateCache;
    @Getter(AccessLevel.PROTECTED)
    private final LoadingCache<Long, String> timestampFieldsCache;

    protected BaseJobProcessor(@NonNull FileSystem fileSystem,
            @NonNull Lazy<SparkSession> ctx,
            @NonNull InternalSystemSpecService systemService,
            @NonNull CompactorProperties properties) {
        this.fs = fileSystem;
        this.sparkSessionLazy = ctx;
        this.systemService = systemService;
        this.compactionDir = properties.getWorkDir();

        this.deduplicateCache = Caffeine.newBuilder()
                .maximumSize(MAXIMUM_CACHE_SIZE)
                .build(this::findDeduplicationColumnNamesForSystem);

        this.timestampFieldsCache = Caffeine.newBuilder()
                .maximumSize(MAXIMUM_CACHE_SIZE)
                .build(this::getTimestampField);
    }

    @Override
    public String toString() {
        return this.getJobType().name() + "-files";
    }

    public ProcessingStatus process(T job) {
        try {
            long start = System.currentTimeMillis();
            log.info("##### {} | {}", logPrefix(job), job);
            if (!verifyJob(job)) {
                log.warn("##### {} | Verification of the job failed, skipping...", logPrefix(job));
                return ProcessingStatus.nothing();
            }
            verifySparkContext();
            Path operationPath = getOperationPath(job);
            ProcessingStatus report = processJob(operationPath, job);
            postProcess(operationPath, job);
            if (report.getTaskCount() > 0) {
                log.info("##### {} | finished in {} | {} ", logPrefix(job), timeFrom(start), report);
            } else if (!report.isSuccess()) {
                log.warn("##### {} | failed (with problem handling, should be better next time) in {} | {} ",
                        logPrefix(job), timeFrom(start), report);
            } else {
                log.warn("##### {} | empty job", logPrefix(job));
            }
            return report;
        } catch (Exception ex) {
            log.error("##### {} | failed with exception", logPrefix(job), ex);
            throw ex;
        }
    }

    protected void postProcess(Path operationPath, T job) {
        deleteFiles(Collections.singleton(operationPath), job);
    }

    protected abstract Path getOperationPath(T job);

    private boolean verifyJob(T job) {
        Objects.requireNonNull(job);
        if (job.getDestinationDir() == null || CollectionUtils.isEmpty(job.getFiles())) {
            throw new IllegalArgumentException(
                    logPrefix(job) + " | provided job is illegal, no destination dir or no files to process");
        }
        return checkFiles(job);

    }

    /**
     * This situation can happen due to the distributed nature of the flow between service and compactor.
     *
     * @param job - a given job
     * @return true if all files exist
     */
    private boolean checkFiles(T job) {
        Predicate<Path> notExists = p -> {
            try {
                return !fs.exists(p);
            } catch (IOException e) {
                throw new SparkCompactionRuntimeException("Cannot check path " + p, e);
            }
        };
        boolean value = job.getFiles().stream().map(Path::new).noneMatch(notExists);
        if (!value) {
            log.info("##### {} | provided job has non existent files", logPrefix(job));
        }
        return value;
    }

    private void verifySparkContext() {
        if (sparkSessionLazy.get().sparkContext().isStopped()) {
            throw new SparkContextStoppedRuntimeException("The Spark context is closed!");
        }
    }

    /**
     * Processes the job -  the final file(s) are places in the moveToPath
     *
     * @param operationPath - the path where the operation is taking place
     * @param job           - compaction job
     */
    private ProcessingStatus processJob(Path operationPath, T job) {
        try {
            long start = System.currentTimeMillis();
            startLog(job);
            applyActions(operationPath, job);
            finishLog(job, start);
            return ProcessingStatus.success(job.getFiles().size(), job.getJobSize());
        } catch (Exception e) {
            if (!handleException(e, job)) {
                log.error("##### {} | Cannot handle exception for job", logPrefix(job));
                throw genericException(job, e);
            } else {
                log.warn("##### {} | Job failed but handling exception was successful", logPrefix(job));
            }
            return ProcessingStatus.nothing(); // nothing, since we handle exception
        }
    }

    /**
     * Tries to handle the job exception, makes some actions that next time the job passes.
     * Handle to some post actions that might be necessary to react to an exception.
     * It is not expected to re-process the job here, just some counter-measures that might help
     * in the next round, changing parameters to the job, etc.
     *
     * @param e   - may be used in derived classes
     * @param job - may be used in derived classes
     * @return true if handling was successful (whatever that means in the job context)
     */
    protected boolean handleException(Exception e, T job) {
        return false;
    }

    protected abstract void applyActions(Path operationPath, T job) throws IOException;

    /*
     * We need to put the output final parquet files in one of the directories from which we take the original files.
     * This is done for safety to make sure that no data is lost if the process of moving files around is interrupted.
     * It is assured by the fact that if it happens the next compaction will pick up the parquet file and continue normally.
     */
    protected void moveCompactedFilesToDestination(List<Path> stagingFiles, Path operationPath, T job)
            throws IOException {
        // gathering the result of compaction
        List<Path> compactedFiles = HdfsFileUtils.getParquetFilesIn(getFs(), operationPath).stream()
                .map(FileStatus::getPath)
                .collect(Collectors.toList());
        // moving compacted files back to staging directory
        List<Path> newStagingFiles = moveFiles(compactedFiles, job,
                name -> job.getSourceDir() + Path.SEPARATOR + job.getFilePrefix() + name);

        // deleting original files (even if output file list is empty).
        deleteFiles(stagingFiles, job);

        if (!newStagingFiles.isEmpty()) {
            // moving files from staging to the data directory
            createDirectory(new Path(job.getDestinationDir()), job);
            List<Path> dataFiles = moveFiles(newStagingFiles, job,
                    name -> job.getDestinationDir() + Path.SEPARATOR + name);
            // making data files read only
            makeReadOnly(dataFiles, job);

        } else {
            //Output files are empty. This can happen if there are empty avro files as input due to issues with HDFS like
            //https://issues.cern.ch/browse/NXCALS-6912
            log.error(
                    "{} - The compaction did not produce any output files, the following staging files got nevertheless deleted {}",
                    logPrefix(job), stagingFiles);
        }
    }

    private String timeFrom(long start) {
        return DurationFormatUtils.formatDurationHMS(System.currentTimeMillis() - start);
    }

    protected Path createTempCompactionPath(T job, String filePrefix) {
        return new Path(compactionDir + Path.SEPARATOR + "compaction" + job.getDestinationDir().getPath()
                .replace(Path.SEPARATOR_CHAR, '-') + "-" + filePrefix + UUID.randomUUID());
    }

    private void startLog(T job) {
        if (log.isDebugEnabled()) {
            log.debug(
                    "##### {} | Start processing {} job with {} files ({} MB) in directory {}, from hours/days {}, files (sample) {} for destination to {}",
                    job.getClass().getSimpleName(),
                    logPrefix(job),
                    job.getFiles().size(),
                    String.format("%.3f", job.getJobSize() / (1024 * 1024.0)),
                    job.getSourceDir(),
                    parents(job.getFiles()),
                    sample(job.getFiles()),
                    job.getDestinationDir()
            );
        }
    }

    private Set<String> parents(List<URI> files) {
        return files.stream().map(uri -> new Path(uri).getParent().getName()).collect(Collectors.toSet());
    }

    private void finishLog(T job, long start) {
        if (log.isDebugEnabled()) {
            log.debug(
                    "##### {} | Finish processing {} job with {} files ({} MB) in {} sec in directory {}",
                    job.getClass().getSimpleName(),
                    logPrefix(job),
                    job.getFiles().size(),
                    String.format("%.3f", job.getJobSize() / (1024 * 1024.0)),
                    timeFrom(start),
                    job.getDestinationDir());
        }
    }

    protected Map<FileTypeFormat, Dataset<Row>> convertToDataFrameByType(Collection<Path> files) {
        Map<FileTypeFormat, List<Path>> pathsByType = files.stream()
                .collect(Collectors.groupingBy(FileTypeFormat::valueOf));
        if (log.isDebugEnabled()) {
            pathsByType.forEach((key, value) -> log
                    .debug("Grouped {} files of type {}", value.size(), key));
        }

        if (pathsByType.containsKey(FileTypeFormat.INVALID)) {
            log.warn("Ignoring invalid (not avro/parquet) files found {}", pathsByType.get(FileTypeFormat.INVALID));
            pathsByType.remove(FileTypeFormat.INVALID);
        }

        Map<FileTypeFormat, Dataset<Row>> dataFrameByType = new EnumMap<>(FileTypeFormat.class);
        for (Map.Entry<FileTypeFormat, List<Path>> entry : pathsByType.entrySet()) {
            Dataset<Row> dataset = read(entry.getKey())
                    .load(entry.getValue().stream().map(Path::toString).toArray(String[]::new));
            dataFrameByType.put(entry.getKey(), dataset);
        }
        return dataFrameByType;
    }

    protected List<Path> moveFiles(List<Path> sources, T job, UnaryOperator<String> nameFunction)
            throws IOException {
        List<Path> destinations = new ArrayList<>();
        log.debug("{} | Moving {} files", logPrefix(job), sources.size());
        for (Path source : sources) {
            Path destination = new Path(nameFunction.apply(source.getName()));
            log.debug("{} | Moving {} file to {}", logPrefix(job), source, destination);
            HdfsFileUtils.rename(getFs(), source, destination);
            destinations.add(destination);
        }
        return destinations;
    }

    private void createDirectory(Path directory, T job) throws IOException {
        if (!fs.exists(directory)) {
            log.debug("{} | Creating directory {}", logPrefix(job), directory);
            fs.mkdirs(directory);
        }
    }

    private DataFrameReader read(FileTypeFormat format) {
        return sparkSessionLazy.get().read().format(format.fileFormat);
    }

    private void makeReadOnly(List<Path> files, T job)
            throws IOException {
        log.debug("{} | Changing {} permissions: (sample) {}", logPrefix(job), files.size(), sample(files));
        for (Path filePath : files) {
            log.debug("{} | Changing rights on the final file {} to {} for all", logPrefix(job), filePath,
                    READ_ONLY.toString());
            fs.setPermission(filePath, READ_ONLY);
        }
    }

    protected void deleteFiles(Collection<Path> files, T job) {
        if (CollectionUtils.isNotEmpty(files)) {
            log.debug("{} | Deleting {} files: (sample) {}", logPrefix(job), files.size(), sample(files));
            files.forEach(this::deleteFile);
        } else {
            log.debug("{} | No paths were provided to delete! Will do nothing", logPrefix(job));
        }
    }

    private void deleteFile(Path path) {
        try {
            if (fs.exists(path)) {
                fs.delete(path, true);
            }
        } catch (IOException e) {
            log.error("Cannot delete file " + path, e);
        }
    }

    private Set<String> findDeduplicationColumnNamesForSystem(long systemId) {
        log.debug("Finding SystemData for system id {}", systemId);
        SystemSpec systemData = getSystemData(systemId);
        Set<String> columns = new HashSet<>();
        //Changed from entity keys to entity_id for performance (jwozniak)
        columns.add(NXC_ENTITY_ID.getValue());
        columns.addAll(getFieldNames(systemData.getTimeKeyDefinitions()));
        if (systemData.getRecordVersionKeyDefinitions() != null) {
            columns.addAll(getFieldNames(systemData.getRecordVersionKeyDefinitions()));
        }
        return columns;
    }

    private SystemSpec getSystemData(long systemId) {
        return systemService.findOne(SystemSpecs.suchThat().id().eq(systemId))
                .orElseThrow(() -> new IllegalArgumentException("No such system id " + systemId));
    }

    private String getTimestampField(long systemId) {
        SystemSpec systemData = getSystemData(systemId);
        Schema timeKeySchema = new Schema.Parser().parse(systemData.getTimeKeyDefinitions());
        return timeKeySchema.getFields().stream()
                .map(Schema.Field::name)
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Cannot obtain time schema for system id" + systemId));
    }

    private Collection<? extends String> getFieldNames(String schema) {
        return new Schema.Parser().parse(schema).getFields().stream().map(Schema.Field::name)
                .collect(Collectors.toSet());
    }

    protected List<Path> toPaths(List<URI> files) {
        return files.stream().map(Path::new).collect(Collectors.toList());
    }

    protected String logPrefix(DataProcessingJob job) {
        return String.format("%s job ", Objects.requireNonNull(job).getType().name()) + job.getSourceDir();
    }

    protected SparkCompactionRuntimeException emptyDatasetException(T job) {
        return new SparkCompactionRuntimeException(
                String.format("%s | Input files in %s (%d files) resulted in an empty dataset, sample: %s",
                        logPrefix(job), job.getDestinationDir(), job.getFiles().size(), sample(job.getFiles())));
    }

    protected SparkCompactionRuntimeException genericException(T job, Exception e) {
        return new SparkCompactionRuntimeException(
                String.format("%s | Error processing for path: %s (%d files), sample: %s", logPrefix(job),
                        job.getDestinationDir(), job.getFiles().size(), sample(job.getFiles())), e);
    }

    private <E> Collection<E> sample(Collection<E> stagingFiles) {
        return stagingFiles.stream().limit(3).collect(Collectors.toList());
    }
}
