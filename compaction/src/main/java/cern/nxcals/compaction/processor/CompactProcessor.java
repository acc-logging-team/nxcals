/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.compaction.processor;

import cern.nxcals.common.domain.CompactionJob;
import cern.nxcals.common.domain.DataProcessingJob.JobType;
import cern.nxcals.common.utils.Lazy;
import cern.nxcals.compaction.config.CompactorProperties;
import cern.nxcals.compaction.processor.writer.BaseDatasetWriter;
import cern.nxcals.compaction.processor.writer.DatasetWriterJob;
import cern.nxcals.internal.extraction.metadata.InternalSystemSpecService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

/**
 * Compacts the avro and parquet files in the directory.
 * If the process fails in the middle of operation there will be next compaction that actually will fix the duplicated files.
 */
@Component
@Slf4j
public class CompactProcessor extends AbstractCompactProcessor<CompactionJob> {
    private final BaseDatasetWriter writer;

    @Autowired
    public CompactProcessor(@NonNull FileSystem fileSystem,
            @NonNull Lazy<SparkSession> ctx,
            @NonNull InternalSystemSpecService systemService,
            @NonNull CompactorProperties properties) {
        super(fileSystem, ctx, systemService, properties);
        writer = new BaseDatasetWriter(getDeduplicateCache(), getTimestampFieldsCache());
    }

    @Override
    public JobType getJobType() {
        return JobType.COMPACT;
    }

    @Override
    protected Collection<Dataset<Row>> loadDatasets(List<Path> paths) {
        return convertToDataFrameByType(paths).values();
    }

    @Override
    protected void saveDataset(Path operationalPath, Dataset<Row> dataset, CompactionJob job) throws IOException {
        writer.saveDataset(operationalPath, dataset, DatasetWriterJob.from(job));
    }

}
