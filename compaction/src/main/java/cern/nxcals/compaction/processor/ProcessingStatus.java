package cern.nxcals.compaction.processor;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author jwozniak
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ProcessingStatus {
    private long taskCount;
    private long fileCount;
    private long byteCount;
    private long errorCount;

    public static ProcessingStatus nothing() {
        return new ProcessingStatus(0, 0, 0, 0);
    }

    public static ProcessingStatus success(long processedFiles, long processedBytes) {
        return new ProcessingStatus(1, processedFiles, processedBytes, 0);
    }

    public static ProcessingStatus failure() {
        return new ProcessingStatus(1, 0, 0, 1);
    }

    public synchronized ProcessingStatus add(ProcessingStatus that) {
        this.taskCount += that.taskCount;
        this.fileCount += that.fileCount;
        this.byteCount += that.byteCount;
        this.errorCount += that.errorCount;

        return new ProcessingStatus(taskCount, fileCount, byteCount, errorCount);
    }

    @Override
    public String toString() {
        return String.format("Processing Status: %d files (%.3f Mb) in %d jobs (%d issues)",
                fileCount, byteCount / (1024 * 1024.0), taskCount, errorCount);
    }

    public boolean isSuccess() {
        return errorCount == 0;
    }
}
