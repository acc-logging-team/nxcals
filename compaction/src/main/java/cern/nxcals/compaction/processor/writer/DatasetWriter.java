package cern.nxcals.compaction.processor.writer;

import com.github.benmanes.caffeine.cache.LoadingCache;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.hadoop.fs.Path;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.io.IOException;
import java.util.Set;

@RequiredArgsConstructor
public abstract class DatasetWriter<T extends DatasetWriterJob> {
    @Getter
    private final LoadingCache<Long, Set<String>> deduplicateCache;

    public abstract void saveDataset(Path operationPath, Dataset<Row> dataset, T job) throws IOException;

    protected Dataset<Row> deduplicateDataset(long systemId, Dataset<Row> dataset) {
        Set<String> deduplicateOn = deduplicateCache.get(systemId);
        return dataset.dropDuplicates(deduplicateOn.toArray(new String[0]));
    }
}
