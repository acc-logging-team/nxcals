package cern.nxcals.compaction.config;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by msobiesz on 05/12/16.
 */
@Configuration
public class CuratorConfig {

    @Value(value = "${zookeeper.location}")
    private String zookeeperLocation;

    @Bean(destroyMethod = "close", initMethod = "start")
    public CuratorFramework curatorFramework() {
        return CuratorFrameworkFactory.newClient(this.zookeeperLocation, new ExponentialBackoffRetry(1000, 3));
    }

}
