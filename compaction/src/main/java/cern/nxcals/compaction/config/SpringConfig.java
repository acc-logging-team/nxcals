/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.compaction.config;

import cern.nxcals.api.config.AuthenticationContext;
import cern.nxcals.api.extraction.metadata.InternalServiceClientFactory;
import cern.nxcals.common.metrics.MetricsConfig;
import cern.nxcals.internal.extraction.metadata.InternalCompactionService;
import cern.nxcals.internal.extraction.metadata.InternalPartitionResourceHistoryService;
import cern.nxcals.internal.extraction.metadata.InternalPartitionResourceService;
import cern.nxcals.internal.extraction.metadata.InternalSystemSpecService;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.fs.FileSystem;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Import;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@EnableConfigurationProperties(CompactorProperties.class)
@Slf4j
@Configuration
@Import({MetricsConfig.class, AuthenticationContext.class})
public class SpringConfig {

    @Bean(name = "queue")
    public LinkedBlockingQueue<Runnable> createQueue() {
        return new LinkedBlockingQueue<>();
    }

    @Bean(name = "executor")
    public ExecutorService createExecutor(LinkedBlockingQueue<Runnable> queue, CompactorProperties properties) {
        return new ThreadPoolExecutor(properties.getThreads(), properties.getThreads(), 0L, TimeUnit.MILLISECONDS,
                queue);
    }

    @Bean(name = "scheduledExecutor")
    public ScheduledExecutorService createScheduledExecutor() {
        return Executors.newSingleThreadScheduledExecutor();
    }

    @Bean(name = "fileSystem")
    @DependsOn("kerberos")
    public FileSystem createFileSystem() {
        log.info("Creating HDFS FileSystem");
        org.apache.hadoop.conf.Configuration conf = new org.apache.hadoop.conf.Configuration();
        try {
            return FileSystem.get(conf);
        } catch (IOException e) {
            throw new UncheckedIOException("Cannot access filesystem", e);
        }
    }

    @Bean(name = "systemService")
    public InternalSystemSpecService createSystemSpecService() {
        return InternalServiceClientFactory.createSystemSpecService();
    }

    @Bean(name = "compactionService")
    public InternalCompactionService compactionService() {
        return InternalServiceClientFactory.createCompactionService();
    }

    @Bean(name = "partitionResourceService")
    public InternalPartitionResourceService createPartitionResourceService() {
        return InternalServiceClientFactory.createPartitionResourceService();
    }

    @Bean(name = "partitionResourceHistoryService")
    public InternalPartitionResourceHistoryService createPartitionResourceHistoryService() {
        return InternalServiceClientFactory.createPartitionResourceHistoryService();
    }
}
