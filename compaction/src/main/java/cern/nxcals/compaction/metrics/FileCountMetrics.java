package cern.nxcals.compaction.metrics;

import cern.nxcals.common.metrics.MetricsRegistry;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.IOException;
import java.util.Map;

@Slf4j
@AllArgsConstructor
public class FileCountMetrics {
    private final MetricsRegistry registry;
    private final FileSystem fs;
    private final Map<String, String> baseDirs;
    private final String metricPrefix;

    @Scheduled(cron = "${metrics.file-count.cron}", zone = "UTC")
    public void publish() {
        baseDirs.forEach(this::publish);
    }

    private void publish(String metric, String dir) {
        try {
            long count = countFiles(dir);
            log.info("Found {} files in the directory {}", count, dir);
            registry.updateGaugeTo(metricPrefix + "_" + metric, count);
        } catch (IOException ex) {
            log.error("HDFS read error when performing file count", ex);
        }
    }

    private long countFiles(String dir) throws IOException {
        return fs.getContentSummary(new Path(dir)).getFileCount();
    }
}
