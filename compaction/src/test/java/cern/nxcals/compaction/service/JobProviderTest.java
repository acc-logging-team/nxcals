package cern.nxcals.compaction.service;

import cern.nxcals.api.domain.TimeEntityPartitionType;
import cern.nxcals.common.domain.AdaptiveCompactionJob;
import cern.nxcals.common.domain.DataProcessingJob;
import cern.nxcals.internal.extraction.metadata.InternalCompactionService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class JobProviderTest {
    @Mock
    private InternalCompactionService compactionService;
    @Mock
    private AdaptiveCompactionJob job1, job2, job3;
    @Test
    void getJobs() {
        JobProvider jobProvider = new JobProvider(compactionService);
        List<DataProcessingJob> serviceJobs = new ArrayList<>();
        serviceJobs.add(job1);
        serviceJobs.add(job2);
        serviceJobs.add(job3);

        when(compactionService.getDataProcessJobs(8, DataProcessingJob.JobType.ADAPTIVE_COMPACT)).thenReturn(
                serviceJobs);

        jobProvider.addJob(DataProcessingJob.JobType.ADAPTIVE_COMPACT, mock(AdaptiveCompactionJob.class));
        jobProvider.addJob(DataProcessingJob.JobType.ADAPTIVE_COMPACT, mock(AdaptiveCompactionJob.class));
        //different type
        jobProvider.addJob(DataProcessingJob.JobType.COMPACT, mock(AdaptiveCompactionJob.class));

        Set<AdaptiveCompactionJob> jobs = jobProvider.getJobs(10, DataProcessingJob.JobType.ADAPTIVE_COMPACT);

        assertEquals(5, jobs.size());

        assertEquals(1, jobProvider.getJobs(10, DataProcessingJob.JobType.COMPACT).size());

    }

    @Test
    void addJobShouldTakePrecedenceOverServiceJobs() {
        JobProvider jobProvider = new JobProvider(compactionService);
        List<DataProcessingJob> serviceJobs = new ArrayList<>();
        serviceJobs.add(createJob("/staging/ETL-1/1/1/1/2022-09-01", 1));

        when(compactionService.getDataProcessJobs(9, DataProcessingJob.JobType.ADAPTIVE_COMPACT)).thenReturn(
                serviceJobs);

        //Same dir for equals() to be the same, different system id to check for it later
        AdaptiveCompactionJob added = createJob("/staging/ETL-1/1/1/1/2022-09-01", 0);

        jobProvider.addJob(DataProcessingJob.JobType.ADAPTIVE_COMPACT, added);

        Set<AdaptiveCompactionJob> jobs = jobProvider.getJobs(10, DataProcessingJob.JobType.ADAPTIVE_COMPACT);

        assertEquals(1, jobs.size());
        assertEquals(0, jobs.stream().findFirst().get().getSystemId());

    }

    private AdaptiveCompactionJob createJob(String sourceDir, long systemId) {
        return AdaptiveCompactionJob.builder()
                .destinationDir(URI.create("outputDir"))
                .files(new ArrayList<>())
                .systemId(systemId)
                .filePrefix("")
                .sourceDir(URI.create(sourceDir))
                .partitionType(TimeEntityPartitionType.of(1, 1))
                .sortEnabled(false)
                .build();
    }
}