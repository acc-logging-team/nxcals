/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.compaction.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ConfigurationProperties(prefix = "spark")
@Profile("dev")
public class SparkConfigurationDev {

// TODO: this code is very old. Had to comment it out to have compactor run locally (kkrynick)
//    @Bean(name = "sparkCtx")
//    public SQLContext createSpark() {
//
//        //@formatter:off
//        SparkConf conf = new SparkConf()
//                .setAppName("Compactor-test")
//                .setMaster("local[*]");
//        //@formatter:on
//        JavaSparkContext sc = new JavaSparkContext(conf);
//        SQLContext sqlContext = new SQLContext(sc);
//        sqlContext.setConf("spark.sql.parquet.compression.codec", "snappy");
//        return sqlContext;
//    }

}
