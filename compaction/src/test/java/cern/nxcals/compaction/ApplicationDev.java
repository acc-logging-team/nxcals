/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.compaction;

import cern.nxcals.compaction.service.Compactor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class ApplicationDev {
    static {
        System.setProperty("org.apache.logging.log4j.simplelog.StatusLogger.level", "TRACE");
        System.setProperty("logging.config", "classpath:compaction-log4j2.yml");

        String user = System.getProperty("user.name");

        System.setProperty("kerberos.principal", user);
        System.setProperty("kerberos.keytab", "/opt/" + user + "/.keytab");
        System.setProperty("kerberos.relogin", "true");
        System.setProperty("javax.persistence.validation.mode", "NONE");
        System.setProperty("java.security.krb5.realm", "CERN.CH");
        System.setProperty("java.security.krb5.kdc", "cerndc.cern.ch");
        System.setProperty("java.security.krb5.conf", "/dev/null");
        System.setProperty("service.url",
                "https://nxcals-" + user + "1.cern.ch:19093,https://nxcals-" + user + "2.cern.ch:19093");
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationDev.class);

    public static void main(String[] args) throws Exception {
        try {
            ApplicationContext ctx = SpringApplication.run(ApplicationDev.class);
            Compactor compactor = ctx.getBean(Compactor.class);

            compactor.run();
        } catch (Throwable e) {
            e.printStackTrace();
            LOGGER.error("Exception while running compaction app", e);
        } finally {
            //has to exit to allow for the next run from outside scrip
            System.exit(0);
        }


        /*
         * System.out.println("Let's inspect the beans provided by Spring Boot:");
         *
         * String[] beanNames = ctx.getBeanDefinitionNames(); Arrays.sort(beanNames); for (String beanName : beanNames)
         * { System.out.println(beanName); }
         */

        // String initPath = "/tmp/cals/MDB4";

    }

    //FIXME - for the moment this is not runnable - tbd
//    private static void runCompactor(Compactor compactor, MeterRegistry meterRegistry) throws InterruptedException {
//        Instant start = Instant.now();
//        Supplier<List<CompactionJobData>> jobSupplier = createJobSupplier();
//        while (Duration.between(start, Instant.now()).toMillis() <= 30_000) {
//            // we don't filter paths here...
//            ProcessingStatus status = compactor.run(jobSupplier);
//            CompactionMetrics.export(meterRegistry, status);
//            TimeUnit.SECONDS.sleep(2);
//        }
//
//        System.err.println("!!! Finished ALL !!!");
//
//    }
//
//    private static Supplier<List<CompactionJobData>> createJobSupplier() {
//        //create here a job supplier, please put your jobs accordingly to your development case.
//        return () -> {
//            List<CompactionJobData> jobs = new ArrayList<>();
//            jobs.add(CompactionJobData.builder().destinationDir(URI.create("/test")).files(Collections.singletonList(
//                    URI.create("/src/f1.avro"))).build());
//            return jobs;
//        };
//    }

}
