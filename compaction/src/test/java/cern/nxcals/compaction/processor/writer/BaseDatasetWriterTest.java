package cern.nxcals.compaction.processor.writer;

import cern.nxcals.api.custom.domain.CmwSystemConstants;
import org.apache.hadoop.fs.Path;
import org.apache.spark.sql.DataFrameWriter;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class BaseDatasetWriterTest extends DatasetWriterTest {
    private static final int PARTITION_COUNT = 10;
    private BaseDatasetWriter writer;
    DataFrameWriter dfWriter = mock(DataFrameWriter.class);

    private static DatasetWriterJob getJob(boolean withSorting) {
        return DatasetWriterJob.builder()
                .systemId(SYSTEM_ID)
                .sortEnabled(withSorting)
                .partitionCount(PARTITION_COUNT)
                .build();
    }

    @AfterEach
    public void tearDown() {
        reset(fileSystem, dataFrame);
    }

    @BeforeEach
    public void setup() {
        initMockDataframe();
        writer = spy(new BaseDatasetWriter(deduplicateCache, timestampFieldCache));
    }

    private void initMockDataframe() {
        when(dataFrame.union(any())).thenReturn(dataFrame);
        when(dataFrame.dropDuplicates(any(String[].class))).thenReturn(dataFrame);
        when(dataFrame.repartition(PARTITION_COUNT)).thenReturn(dataFrame);
        when(dataFrame.sortWithinPartitions(any(String.class))).thenReturn(dataFrame);

        when(dataFrame.write()).thenReturn(dfWriter);
        when(dfWriter.mode(any(SaveMode.class))).thenReturn(dfWriter);
        when(dfWriter.option(anyString(), anyLong())).thenReturn(dfWriter);
        when(dfWriter.partitionBy(anyString(), anyString())).thenReturn(dfWriter);
    }

    @Test
    public void shouldSortWhenNeeded() throws Exception {
        DatasetWriterJob job = getJob(true);
        writer.saveDataset(mock(Path.class), dataFrame, job);
        verify(dataFrame, times(1)).sortWithinPartitions(CmwSystemConstants.RECORD_TIMESTAMP);
    }

    @Test
    public void shouldNotSortWhenNotNeeded() throws Exception {
        writer.saveDataset(mock(Path.class), dataFrame, getJob(false));
        verify(dataFrame, times(0)).sortWithinPartitions(anyString());
    }

    @Test
    public void shouldWriteParquet() throws Exception {
        writer.saveDataset(mock(Path.class), dataFrame, getJob(false));
        verify(dataFrame, times(1)).write();
        verify(dfWriter, times(1)).parquet(anyString());
    }

    @Test
    void shouldPerformMergeCompactionOnProperEntityAndAppendNewRows() throws IOException {
        Dataset<Row> dataDs = createOriginalDataset();
        Dataset<Row> expectedDs = createExpectedMergedDataset();
        // apply actions except save
        Dataset<Row> deduplicateDataset = writer.deduplicateDataset(SYSTEM_ID, dataDs);
        Dataset<Row> repartitionedDataSet = writer.repartitionDataSet(getJob(false), deduplicateDataset);

        assertAll(
                () -> assertTrue(toSet(repartitionedDataSet.columns()).containsAll(toSet(expectedDs.columns()))),
                () -> assertEquals(4, repartitionedDataSet.columns().length),
                () -> assertEquals(expectedDs.count(), repartitionedDataSet.count()),
                () -> assertEquals(PARTITION_COUNT, repartitionedDataSet.rdd().getNumPartitions())
        );
    }

    private <T> Set<T> toSet(T[] array) {
        return Arrays.stream(array).collect(Collectors.toSet());
    }
}
