/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.compaction.processor;

import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.extraction.metadata.queries.SystemSpecs;
import cern.nxcals.common.domain.MergeCompactionJob;
import cern.nxcals.common.utils.Lazy;
import cern.nxcals.common.utils.ReflectionUtils;
import cern.nxcals.compaction.SparkContextStoppedRuntimeException;
import cern.nxcals.compaction.config.CompactorProperties;
import cern.nxcals.compaction.processor.handler.HdfsFileReader;
import cern.nxcals.internal.extraction.metadata.InternalSystemSpecService;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static org.apache.spark.sql.types.DataTypes.DoubleType;
import static org.apache.spark.sql.types.DataTypes.LongType;
import static org.apache.spark.sql.types.DataTypes.StringType;
import static org.apache.spark.sql.types.DataTypes.createStructField;
import static org.apache.spark.sql.types.DataTypes.createStructType;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MergeCompactProcessorTest extends BaseJobProcessorTest {

    private String basePath1 = System.getProperty("user.dir") + "/staging/CMW-1/1/1/1/2016-3-21";
    private String basePath2 = System.getProperty("user.dir") + "/staging/CMW-2/1/1/1/2016-3-21";

    private String file1 = basePath1 + "/f1.avro";
    private String file2 = basePath1 + "/f2.avro";
    private String file3 = basePath1 + "/f3.parquet";

    private String file4 = basePath2 + "/f4.avro";
    private String file5 = basePath2 + "/f5.avro";
    private String file6 = basePath2 + "/f6.parquet";

    private String outFile1 = basePath1 + "/out1.parquet";
    private String outFile2 = basePath2 + "/out2.parquet";

    @Mock
    private FileStatus fileStatus1, fileStatus2, fileStatus3, fileStatus4, fileStatus5, fileStatus6;

    @Mock
    private LocatedFileStatus outputParquetFile1;

    @Mock
    protected HdfsFileReader<List<Path>> reportFileReader;

    private MergeCompactionJob jobData;
    private MergeCompactProcessor jobProcessor;

    @AfterEach
    public void tearDown() {
        reset(fileSystem, dataFrame);
    }

    @BeforeEach
    public void setup() throws IOException {
        jobData = MergeCompactionJob.builder()
                .destinationDir(URI.create(DESTINATION_DIR))
                .files(createFiles())
                .partitionCount(3)
                .restagingReportFile(RESTAGING_REPORT_FILE)
                .filePrefix("")
                .sortEnabled(false)
                .systemId(SYSTEM_ID)
                .build();

        doReturn(fileStatus1).when(fileSystem).getFileStatus(argThat(path -> path.toString().equals(file1)));
        doReturn(fileStatus2).when(fileSystem).getFileStatus(argThat(path -> path.toString().equals(file2)));
        doReturn(fileStatus3).when(fileSystem).getFileStatus(argThat(path -> path.toString().equals(file3)));
        doReturn(fileStatus4).when(fileSystem).getFileStatus(argThat(path -> path.toString().equals(file4)));
        doReturn(fileStatus5).when(fileSystem).getFileStatus(argThat(path -> path.toString().equals(file5)));
        doReturn(fileStatus6).when(fileSystem).getFileStatus(argThat(path -> path.toString().equals(file6)));

        when(fileSystem.exists(any(Path.class))).thenReturn(true);
        //this is important for batches as it should return one file for each batch.

        doReturn(iteratorForOutput).when(fileSystem).listFiles(any(Path.class), anyBoolean());
        properties = new CompactorProperties();
        properties.setWorkDir(COMPACTION_DIR);
        jobProcessor = spy(
                new MergeCompactProcessor(fileSystem, new Lazy<>(() -> sparkSession), systemService, properties,
                        reportFileReader));
    }

    private void initMocks() throws IOException {
        initMockSparkSession();
        initMockDataframe();
        initMockSystemService();
        initMockOutputFileIterator();
        initMockReportFileReader();
    }

    protected void initMockSparkSession() {
        when(sparkSession.sparkContext()).thenReturn(sparkContext);
        when(sparkContext.isStopped()).thenReturn(false);
    }

    protected void initMockReportFileReader() throws IOException {
        when(reportFileReader.read(new Path(RESTAGING_REPORT_FILE)))
                .thenReturn(Arrays.asList(new Path(outFile1), new Path(outFile2)));

    }

    protected void initMockSystemService() {
        Condition<SystemSpecs> condition = SystemSpecs.suchThat().id().eq(SYSTEM_ID);
        when(systemService.findOne(argThat((Condition<SystemSpecs> cond) -> toRSQL(cond).equals(toRSQL(condition)))))
                .thenReturn(
                        Optional.of(
                                ReflectionUtils.builderInstance(SystemSpec.InnerBuilder.class).id(SYSTEM_ID)
                                        .name("CMW").entityKeyDefinitions(entityKeyDefSchema)
                                        .partitionKeyDefinitions(partitionKeyDefSchema)
                                        .timeKeyDefinitions(timeKeyDefSchema)
                                        .recordVersionKeyDefinitions(recordVersionKeyDefSchemaNullable).build()));
    }

    protected void initMockDataframe() {
        when(sparkSession.read()).thenReturn(reader);
        when(reader.format(anyString())).thenReturn(reader);
        when(reader.load(any(String[].class))).thenReturn(dataFrame);

        when(dataFrame.union(any())).thenReturn(dataFrame);
        when(dataFrame.join(any(Dataset.class), any(Column.class), anyString())).thenReturn(dataFrame);
        when(dataFrame.columns()).thenReturn(new String[] { TIMESTAMP_FIELD });
        when(dataFrame.drop(anyString())).thenReturn(dataFrame);
        when(dataFrame.withColumn(eq("tmp." + TIMESTAMP_FIELD), any(Column.class))).thenReturn(dataFrame);
        when(dataFrame.withColumnRenamed("tmp." + TIMESTAMP_FIELD, TIMESTAMP_FIELD)).thenReturn(dataFrame);

        when(dataFrame.dropDuplicates(any(String[].class))).thenReturn(dataFrame);
        when(dataFrame.repartition(anyInt())).thenReturn(dataFrame);

        when(dataFrame.col(TIMESTAMP_FIELD)).thenReturn(functions.col(TIMESTAMP_FIELD));
        when(dataFrame.col(ENTITY_ID_FIELD)).thenReturn(functions.col(ENTITY_ID_FIELD));
    }

    private void initMockOutputFileIterator() throws IOException {

        when(iteratorForOutput.hasNext()).thenReturn(true, false);
        when(iteratorForOutput.next()).thenReturn(outputParquetFile1);

        when(outputParquetFile1.isFile()).thenReturn(true);
        when(outputParquetFile1.getPath()).thenReturn(new Path(outFile1));
    }

    private List<URI> createFiles() {
        List<URI> files = new ArrayList<>();
        files.add(URI.create(file1));
        files.add(URI.create(file2));
        files.add(URI.create(file3));
        files.add(URI.create(file4));
        files.add(URI.create(file5));
        files.add(URI.create(file6));
        return files;
    }

    @Test
    public void testSparkContextClosed() {
        when(sparkSession.sparkContext()).thenReturn(sparkContext);
        when(sparkContext.isStopped()).thenReturn(true);
        MergeCompactProcessor job = new MergeCompactProcessor(fileSystem, new Lazy<>(() -> sparkSession), systemService,
                properties,
                reportFileReader);
        assertThrows(SparkContextStoppedRuntimeException.class, () -> job.process(jobData));
    }

    @Test
    public void shouldRethrowOnException() {
        initMockSparkSession();
        when(dataFrame.join(any(), any(Column.class), anyString())).thenThrow(new RuntimeException());
        assertThrows(SparkCompactionRuntimeException.class, () -> jobProcessor.process(jobData));
    }

    @Test
    public void shouldCreateDestinationDir() throws IOException {
        initMocks();
        when(fileSystem.exists(argThat(path -> path.toUri().equals(jobData.getDestinationDir())))).thenReturn(false);

        jobProcessor.process(jobData);
        verify(fileSystem, times(1)).mkdirs(any());
    }

    @Test
    public void shouldSortWhenNeeded() throws IOException {
        initMocks();
        jobData = MergeCompactionJob.builder()
                .destinationDir(URI.create(DESTINATION_DIR))
                .files(createFiles())
                .restagingReportFile(RESTAGING_REPORT_FILE)
                .systemId(SYSTEM_ID)
                .sortEnabled(true)
                .build();

        jobProcessor.process(jobData);
        verify(dataFrame, times(1)).sortWithinPartitions(TIMESTAMP_FIELD);
    }

    @Test
    public void shouldNotAcceptEmptyJob() {
        jobData = MergeCompactionJob.builder()
                .systemId(SYSTEM_ID)
                .files(Collections.emptyList())
                .restagingReportFile(RESTAGING_REPORT_FILE)
                .build();
        assertThrows(IllegalArgumentException.class, () -> jobProcessor.process(jobData));
    }

    @Test
    public void shouldCreateNoDataDirOnEmptyOutputFiles() throws IOException {
        initMockSparkSession();
        initMockDataframe();
        initMockSystemService();
        when(iteratorForOutput.hasNext()).thenReturn(false);

        jobProcessor.process(jobData);
        //output dir should not be created
        verify(fileSystem, times(0)).mkdirs(any());
        //the staging files should get deleted
        verify(fileSystem, times(7)).delete(any(), anyBoolean());
    }

    @Test
    public void shouldNotPerformJoinIfAvroDatasetIsEmpty() throws IOException {
        initMocks();
        jobData = MergeCompactionJob.builder()
                .systemId(SYSTEM_ID)
                .files(Collections.singletonList(URI.create(file3))) // contains only parquet files
                .destinationDir(URI.create(DESTINATION_DIR))
                .restagingReportFile(RESTAGING_REPORT_FILE)
                .build();

        ProcessingStatus process = jobProcessor.process(jobData);

        assertEquals(1, process.getTaskCount());

        verify(dataFrame, never()).join(any());
    }

    @Test
    public void shouldNotPerformJoinIfParquetDatasetIsEmpty() throws IOException {
        initMocks();
        jobData = MergeCompactionJob.builder()
                .systemId(SYSTEM_ID)
                .files(Collections.singletonList(URI.create(file2))) // contains only avro files
                .destinationDir(URI.create(DESTINATION_DIR))
                .restagingReportFile(RESTAGING_REPORT_FILE)
                .build();

        ProcessingStatus process = jobProcessor.process(jobData);

        assertEquals(1, process.getTaskCount());

        verify(dataFrame, never()).join(any());
    }

    @Test
    public void shouldPerformMergeCompaction() throws IOException {
        initMocks();
        Path reportFilePath = new Path(RESTAGING_REPORT_FILE);

        when(dataFrame.isEmpty()).thenReturn(false);

        ProcessingStatus process = jobProcessor.process(jobData);

        assertEquals(0, process.getErrorCount());

        verify(dataFrame, times(1)).join(any(), any(Column.class), eq("fullouter"));
        verify(dataFrame, times(1)).repartition(jobData.getPartitionCount());
        verify(reportFileReader, times(1)).read(reportFilePath);
    }

    @Test
    public void shouldPerformMergeCompactionOnProperEntityAndAppendNewRows() {
        SparkSession spark = SparkSession.builder()
                .master("local")
                .appName("merge_compact_test")
                .getOrCreate();

        Condition<SystemSpecs> condition = SystemSpecs.suchThat().id().eq(SYSTEM_ID);
        InternalSystemSpecService systemSpecService = Mockito.mock(InternalSystemSpecService.class);
        when(systemSpecService.findOne(argThat((Condition<SystemSpecs> cond) ->
                toRSQL(cond).equals(toRSQL(condition))))).thenReturn(
                Optional.of(ReflectionUtils.builderInstance(SystemSpec.InnerBuilder.class)
                        .id(SYSTEM_ID).name("CMW").entityKeyDefinitions(entityKeyDefSchema)
                        .partitionKeyDefinitions(partitionKeyDefSchema)
                        .timeKeyDefinitions(cmwTimeKeyDefSchema).build()));

        MergeCompactionJob job = MergeCompactionJob.builder()
                .sortEnabled(true)
                .partitionCount(1)
                .systemId(SYSTEM_ID)
                .build();
        MergeCompactProcessor<MergeCompactionJob> jobProcessor = new MergeCompactProcessor<>(fileSystem,
                new Lazy<>(() -> spark), systemSpecService,
                properties, reportFileReader);

        Dataset<Row> dataDs = createOriginalDataset(spark);
        Dataset<Row> remigratedDs = createRemigratedDataset(spark);

        // apply actions on merge-compact role
        Dataset<Row> mergedDs = jobProcessor.joinAndReplaceColumnsFromAvro(job, dataDs, remigratedDs);

        // compare result with expected dataset
        Dataset<Row> expectedDs = createExpectedMergedDataset(spark);

        assertArrayEquals(expectedDs.columns(), mergedDs.columns());
        assertEquals(expectedDs.count(), mergedDs.count());

        Dataset<Row> nonMatchingRows = expectedDs.except(mergedDs);
        assertEquals(0, nonMatchingRows.count());
    }

    // helper methods

    private Dataset<Row> createOriginalDataset(SparkSession spark) {
        List<List<Object>> rows = Arrays.asList(
                Arrays.asList(1L, 1L, "value_old", null),   // record_1_1
                Arrays.asList(1L, 2L, null, 1.5D),          // record_2_1
                Arrays.asList(3L, 3L, "value_a", 2.0D),     // record_3_1
                Arrays.asList(5L, 1L, "value_old", null),   // record_1_2
                Arrays.asList(10L, 2L, "value_old", null),  // record_2_2
                Arrays.asList(10L, 4L, "value_b", 2.0D)     // record_4_1
        );
        return createDataset(spark, rows);
    }

    private Dataset<Row> createRemigratedDataset(SparkSession spark) {
        List<List<Object>> rows = Arrays.asList(
                Arrays.asList(1L, 2L, "value_new", 1.5D),   // updates record_2_1
                Arrays.asList(2L, 2L, null, 5.0D),          // new record_2_4 -- missing data fix
                Arrays.asList(3L, 5L, "value", 2.5D),       // new record_5_1
                Arrays.asList(5L, 1L, "value_old", null),   // duplicates record_1_2
                Arrays.asList(5L, 2L, "value_new", 2.0D),   // new record_2_3 -- missing data fix
                Arrays.asList(10L, 2L, null, 2.0D),         // updates record_2_2
                Arrays.asList(15L, 6L, "value", 2.5D)       // new record_6_1
        );
        return createDataset(spark, rows);
    }

    private Dataset<Row> createExpectedMergedDataset(SparkSession spark) {
        List<List<Object>> rows = Arrays.asList(
                Arrays.asList(1L, 1L, "value_old", null),     // record_1_1
                Arrays.asList(1L, 2L, "value_new", 1.5D),     // updated record_2_1
                Arrays.asList(2L, 2L, null, 5.0D),            // new record_2_4
                Arrays.asList(3L, 3L, "value_a", 2.0D),       // record_3_1
                Arrays.asList(3L, 5L, "value", 2.5D),         // new record_5_1
                Arrays.asList(5L, 1L, "value_old", null),     // record_1_2
                Arrays.asList(5L, 2L, "value_new", 2.0D),     // new record_2_3
                Arrays.asList(10L, 2L, "value_old", 2.0D),    // updated record_2_2
                Arrays.asList(10L, 4L, "value_b", 2.0D),      // record_4_1
                Arrays.asList(15L, 6L, "value", 2.5D)         // new record_6_1
        );
        return createDataset(spark, rows);
    }

    private Dataset<Row> createDataset(SparkSession spark, List<List<Object>> rows) {
        List<Row> dsRows = rows.stream().map(r -> RowFactory.create(r.toArray(new Object[0])))
                .collect(Collectors.toList());
        StructType schema = createStructType(new StructField[] {
                createStructField("__record_timestamp__", LongType, false),
                createStructField("__sys_nxcals_entity_id__", LongType, false),
                createStructField("fieldA", StringType, true),
                createStructField("fieldB", DoubleType, true)
        });
        return spark.sqlContext().createDataFrame(dsRows, schema);
    }

}
