/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.compaction.processor;

import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.extraction.metadata.queries.SystemSpecs;
import cern.nxcals.common.domain.CompactionJob;
import cern.nxcals.common.utils.Lazy;
import cern.nxcals.common.utils.ReflectionUtils;
import cern.nxcals.compaction.SparkContextStoppedRuntimeException;
import cern.nxcals.compaction.config.CompactorProperties;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CompactProcessorTest extends BaseJobProcessorTest {

    private final String basePath1 = System.getProperty("user.dir") + "/staging/CMW-1/1/1/1/2016-3-21";
    private final String basePath2 = System.getProperty("user.dir") + "/staging/CMW-2/1/1/1/2016-3-21";

    private final String file1 = basePath1 + "/f1.avro";
    private final String file2 = basePath1 + "/f2.avro";
    private final String file3 = basePath1 + "/f3.parquet";

    private final String file4 = basePath2 + "/f4.avro";
    private final String file5 = basePath2 + "/f5.avro";
    private final String file6 = basePath2 + "/f6.parquet";

    private final String outFile1 = basePath1 + "/out1.parquet";

    @Mock
    private FileStatus fileStatus1, fileStatus2, fileStatus3, fileStatus4, fileStatus5, fileStatus6;

    @Mock
    private LocatedFileStatus outputParquetFile1;

    private CompactionJob job;
    private CompactProcessor processor;

    @AfterEach
    public void tearDown() {
        reset(fileSystem, dataFrame);
    }

    @BeforeEach
    public void setup() throws IOException {
        job = CompactionJob.builder()
                .destinationDir(URI.create(DESTINATION_DIR))
                .files(createFiles())
                .systemId(SYSTEM_ID)
                .build();

        doReturn(fileStatus1).when(fileSystem).getFileStatus(argThat(path -> path.toString().equals(file1)));
        doReturn(fileStatus2).when(fileSystem).getFileStatus(argThat(path -> path.toString().equals(file2)));
        doReturn(fileStatus3).when(fileSystem).getFileStatus(argThat(path -> path.toString().equals(file3)));
        doReturn(fileStatus4).when(fileSystem).getFileStatus(argThat(path -> path.toString().equals(file4)));
        doReturn(fileStatus5).when(fileSystem).getFileStatus(argThat(path -> path.toString().equals(file5)));
        doReturn(fileStatus6).when(fileSystem).getFileStatus(argThat(path -> path.toString().equals(file6)));

        when(fileSystem.exists(any(Path.class))).thenReturn(true);
        //this is important for batches as it should return one file for each batch.

        doReturn(iteratorForOutput).when(fileSystem).listFiles(any(Path.class), anyBoolean());

        initMockDataframe();
        properties = new CompactorProperties();
        properties.setWorkDir(COMPACTION_DIR);
        processor = spy(new CompactProcessor(fileSystem, new Lazy<>(() -> sparkSession), systemService, properties));
    }

    private void initMockDataframe() {
        when(dataFrame.union(any())).thenReturn(dataFrame);
        when(dataFrame.dropDuplicates(any(String[].class))).thenReturn(dataFrame);
        when(dataFrame.repartition(anyInt())).thenReturn(dataFrame);
    }

    private void initMockFileReader() {
        when(sparkSession.read()).thenReturn(reader);
        when(reader.format(anyString())).thenReturn(reader);
        when(reader.load(any(String[].class))).thenReturn(dataFrame);
    }

    private void initMockSystemService() {
        Condition<SystemSpecs> condition = SystemSpecs.suchThat().id().eq(SYSTEM_ID);
        when(systemService.findOne(argThat((Condition<SystemSpecs> cond) -> toRSQL(cond).equals(toRSQL(condition)))))
                .thenReturn(
                        Optional.of(
                                ReflectionUtils.builderInstance(SystemSpec.InnerBuilder.class).id(SYSTEM_ID).name("CMW")
                                        .entityKeyDefinitions(entityKeyDefSchema)
                                        .partitionKeyDefinitions(partitionKeyDefSchema)
                                        .timeKeyDefinitions(timeKeyDefSchema)
                                        .recordVersionKeyDefinitions(recordVersionKeyDefSchemaNullable).build()));
    }

    private void initMockOutputFileIterator() throws IOException {
        when(iteratorForOutput.hasNext()).thenReturn(true, false);
        when(iteratorForOutput.next()).thenReturn(outputParquetFile1);
        when(outputParquetFile1.isFile()).thenReturn(true);
        when(outputParquetFile1.getPath()).thenReturn(new Path(outFile1));
    }

    private void initMocks() throws IOException {
        when(sparkSession.sparkContext()).thenReturn(sparkContext);
        when(sparkContext.isStopped()).thenReturn(false);
        initMockFileReader();
        initMockOutputFileIterator();
        initMockSystemService();
    }

    private List<URI> createFiles() {
        List<URI> files = new ArrayList<>();
        files.add(URI.create(file1));
        files.add(URI.create(file2));
        files.add(URI.create(file3));
        files.add(URI.create(file4));
        files.add(URI.create(file5));
        files.add(URI.create(file6));
        return files;
    }

    @Test
    public void testSparkContextClosed() {
        when(sparkSession.sparkContext()).thenReturn(sparkContext);
        when(sparkContext.isStopped()).thenReturn(true);
        CompactProcessor processor = new CompactProcessor(fileSystem, new Lazy<>(() -> sparkSession), systemService,
                properties);
        assertThrows(SparkContextStoppedRuntimeException.class, () -> processor.process(this.job));
    }

    @Test
    public void shouldRethrowOnException() {
        when(sparkSession.sparkContext()).thenReturn(sparkContext);
        when(sparkContext.isStopped()).thenReturn(false);
        initMockFileReader();
        when(dataFrame.union(any())).thenThrow(new RuntimeException());
        assertThrows(SparkCompactionRuntimeException.class, () -> processor.process(job));
    }

    @Test
    public void shouldCreateDestinationDir() throws IOException {
        initMocks();
        when(fileSystem.exists(argThat(path -> path.toUri().equals(job.getDestinationDir())))).thenReturn(false);
        processor.process(job);
        verify(fileSystem, times(1)).mkdirs(any());
    }

    @Test
    public void shouldSortWhenNeeded() throws Exception {
        initMocks();
        job = CompactionJob.builder()
                .destinationDir(URI.create(DESTINATION_DIR))
                .files(createFiles())
                .systemId(SYSTEM_ID)
                .sortEnabled(true)
                .build();
        processor.process(job);
        verify(dataFrame, times(1)).sortWithinPartitions(TIMESTAMP_FIELD);
    }

    @Test
    public void shouldNotAcceptEmptyJob() {
        job = CompactionJob.builder()
                .files(Collections.emptyList())
                .build();
        assertThrows(IllegalArgumentException.class, () -> processor.process(job));
    }

    @Test
    public void shouldCreateNoDataDirOnEmptyOutputFiles() throws IOException {
        when(sparkSession.sparkContext()).thenReturn(sparkContext);
        when(sparkContext.isStopped()).thenReturn(false);
        initMockFileReader();
        initMockSystemService();

        processor.process(job);
        //output dir should not be created
        verify(fileSystem, times(0)).mkdirs(any());
        //the staging files should get deleted
        verify(fileSystem, times(7)).delete(any(), anyBoolean());

        verify(iteratorForOutput, times(1)).hasNext();
    }

}
