package cern.nxcals.compaction.processor.handler;

import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ReportFileHandlerTest {

    @Mock
    private FileSystem fileSystem;
    @Mock
    private FSDataOutputStream outputStream;

    private ReportFileHandler reportFileHandler;

    @BeforeEach
    public void init() {
        reportFileHandler = new ReportFileHandler(fileSystem);
    }

    @Nested
    class ReportFileHandlerTestNested {
        @BeforeEach
        public void setUp() throws Exception {
            doNothing().when(outputStream).write(any());
            when(fileSystem.create(any(Path.class))).thenReturn(outputStream);
        }

        @Test
        public void shouldWriteEmptyContent() throws IOException {
            Path path = new Path("/test/path/file.txt");
            String content = "";

            reportFileHandler.write(path, content);

            verify(fileSystem, times(1)).create(path);
            verify(outputStream, times(1)).write(content.getBytes());
        }

        @Test
        public void shouldWriteWhenContentIsBlank() throws IOException {
            Path path = new Path("/test/path/file.txt");
            String content = "  ";
            reportFileHandler.write(path, content);

            verify(fileSystem, times(1)).create(path);
            verify(outputStream, times(1)).write(content.getBytes());
        }

        @Test
        public void shouldWriteContent() throws IOException {
            Path path = new Path("/test/path/file.txt");
            String content = "/this/is/test/path/as/contenxt.txt";
            reportFileHandler.write(path, content);

            verify(fileSystem, times(1)).create(path);
            verify(outputStream, times(1)).write(content.getBytes());
        }
    }

    @Test
    public void shouldNotPermitCreationWhenFileSystemIsNull() {
        assertThrows(NullPointerException.class,
                () -> new ReportFileHandler(null),
                "Handler failed to throw exception and permitted creation!"
        );
    }

    @Test
    public void shouldThrowsExceptionOnFileSystemErrorWhileRead() throws IOException {
        Path path = new Path("/test/path/file.txt");
        when(fileSystem.open(path)).thenThrow(IOException.class);
        assertThrows(IOException.class,
                () -> reportFileHandler.read(path),
                "Read should rethrow IO exception!"
        );
        verify(fileSystem, Mockito.only()).open(path);
    }

    @Test
    public void shouldThrowsExceptionOnFileSystemErrorWhileWrite() throws IOException {
        when(fileSystem.create(any(Path.class))).thenReturn(outputStream);
        Path path = new Path("/test/path/file.txt");
        String content = "test content goes here...";
        when(fileSystem.create(path)).thenThrow(IOException.class);

        assertThrows(IOException.class,
                () -> reportFileHandler.write(path, content),
                "Write should rethrow IO exception!"
        );
        verify(fileSystem, Mockito.only()).create(path);
    }

    @Test
    public void shouldThrowExceptionOnWriteWhenContentIsNull() throws IOException {
        Path path = new Path("/test/path/file.txt");
        assertThrows(IllegalArgumentException.class,
                () -> reportFileHandler.write(path, null),
                "Write should throw exception on illegal content!"
        );
        verify(fileSystem, Mockito.never()).create(path);
    }

}
