package cern.nxcals.compaction.processor;

import cern.nxcals.common.SystemFields;
import cern.nxcals.compaction.config.CompactorProperties;
import cern.nxcals.internal.extraction.metadata.InternalPartitionResourceHistoryService;
import cern.nxcals.internal.extraction.metadata.InternalPartitionResourceService;
import cern.nxcals.internal.extraction.metadata.InternalSystemSpecService;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.spark.SparkContext;
import org.apache.spark.sql.DataFrameReader;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.mockito.Answers;
import org.mockito.Mock;

import java.net.URI;

public abstract class BaseJobProcessorTest {
    protected static final long SYSTEM_ID = 1L;

    protected static final String TIMESTAMP_FIELD = "timestamp";
    protected static final String ENTITY_ID_FIELD = SystemFields.NXC_ENTITY_ID.getValue();
    protected static final String REC_VER1 = "record_version1";
    protected static final String REC_VER2 = "record_version2";

    protected static final String entityKeyDefSchema =
            "{\"type\":\"record\",\"name\":\"CmwKey\",\"namespace\":\"cern.cals\",\"fields\":[{\"name\":\"device\",\"type\":\"string\"},{\"name\":\"property\",\"type\":\"string\"}]}";
    protected static final String partitionKeyDefSchema =
            "{\"type\":\"record\",\"name\":\"CmwPartition\",\"namespace\":\"cern.cals\",\"fields\":[{\"name\":\"class\",\"type\":\"string\"},{\"name\":\"property\",\"type\":\"string\"}]}";
    protected static final String recordVersionKeyDefSchemaNullable =
            "{\"type\":\"record\",\"name\":\"RecordVersion\",\"namespace\":\"cern.cals\",\"fields\":\n"
                    + "[{\"name\":\"" + REC_VER1 + "\",\"type\":[\"string\",\"null\"]},{\"name\":\"" + REC_VER2
                    + "\",\"type\":[\"int\",\"null\"]}]}";
    protected static final String timeKeyDefSchema =
            "{\"type\":\"record\",\"name\":\"CmwPT\",\"namespace\":\"cern.cals\",\"fields\":[{\"name\":\""
                    + TIMESTAMP_FIELD + "\",\"type\":\"long\"}]}";
    protected static final String cmwTimeKeyDefSchema =
            "{\"type\":\"record\",\"name\":\"CmwPT\",\"namespace\":\"cern.cals\",\"fields\":[{\"name\":\"__record_timestamp__\",\"type\":\"long\"}]}";
    protected static final String COMPACTION_DIR = System.getProperty("user.dir") + "/compaction";
    protected static final String DESTINATION_DIR = System.getProperty("user.dir") + "/data/1/1/1/2016/3/21";
    protected static final String RESTAGING_DIR = System.getProperty("user.dir") + "/restage";

    protected static final URI RESTAGING_REPORT_FILE = URI.create(
            RESTAGING_DIR + Path.SEPARATOR + "restage_report.file");

    @Mock
    protected FileSystem fileSystem;

    @Mock
    protected SparkSession sparkSession;

    @Mock
    protected SparkContext sparkContext;

    @Mock
    protected RemoteIterator<LocatedFileStatus> iteratorForOutput;

    @Mock
    protected DataFrameReader reader;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    protected Dataset<Row> dataFrame;

    @Mock
    protected InternalSystemSpecService systemService;

    @Mock
    protected InternalPartitionResourceService partitionResourceService;

    @Mock
    protected InternalPartitionResourceHistoryService partitionResourceHistoryService;

    protected CompactorProperties properties;
}
