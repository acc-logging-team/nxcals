package cern.nxcals.common;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Constants {
    public static final String HIERARCHY_SEPARATOR = "/";
    public static final String SERVICE_TYPE = "HTTPS";
    public static final String HBASE_TABLE_NAME_DELIMITER = "__";
    public static final String HBASE_ROW_KEY_DELIMITER = "__";
    public static final String PERMANENT_TABLE_PREFIX = "permanent";
    public static final String RBAC_AUTH_DISABLE = "rbac.auth.disable";
    public static final String KERBEROS_AUTH_DISABLE = "kerberos.auth.disable";
    public static final String DATASET_BUILD_PARALLELIZE_ABOVE_SIZE = "dataset.build.parallelize.above.size";
    public static final int DEFAULT_PARALLELIZE_ABOVE_SIZE = 20;
    public static final String PART_FIELDS_SEPARATOR = "-";
}
