package cern.nxcals.common.config;

import org.apache.hadoop.fs.FileSystem;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import java.io.IOException;

/**
 * A default FileSystem config that can be reused.
 */
@Configuration
public class FileSystemConfig {
    @Bean
    @DependsOn("kerberos")
    public FileSystem fileSystem() throws IOException {
        return FileSystem.get(new org.apache.hadoop.conf.Configuration());
    }
}
