package cern.nxcals.common.config;

import org.apache.spark.sql.SparkSession;

import java.util.function.Consumer;

/**
 * Represents a {@link SparkSession} modifier, capable of modifying an already existing session.
 */
public interface SparkSessionModifier extends Consumer<SparkSession> {
}
