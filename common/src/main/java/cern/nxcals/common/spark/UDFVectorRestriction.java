package cern.nxcals.common.spark;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * The UDFVectorRestriction defines the possible selection applied for filtering vector numeric value.
 * It is done this way to avoid moving the backport domain to common (that creates a circular dependency).
 */
@AllArgsConstructor
@Getter
public enum UDFVectorRestriction {
    /**
     * Filters the vector numeric data, if at least one of the element respects the filtering criteria.
     */
    ANY, ALL;

}
