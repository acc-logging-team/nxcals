package cern.nxcals.common.spark;

import cern.nxcals.api.domain.TimeWindow;
import lombok.NonNull;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.api.java.UDF1;
import org.apache.spark.sql.api.java.UDF2;
import org.apache.spark.sql.api.java.UDF3;
import org.apache.spark.sql.types.ArrayType;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import scala.collection.JavaConverters;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static cern.nxcals.api.utils.EncodingUtils.desanitizeIfNeeded;
import static cern.nxcals.common.avro.SchemaConstants.ARRAY_DIMENSIONS_FIELD_NAME;
import static cern.nxcals.common.avro.SchemaConstants.ARRAY_ELEMENTS_FIELD_NAME;

@Slf4j
@UtilityClass
public final class MoreFunctions {
    private static final double DOUBLE_COMPARE_THRESHOLD = .0000000001;

    /**
     * Kept for backward compatibility
     *
     * @deprecated use MoreFunctions::lookupWith instead
     */
    @Deprecated
    public static UDF2<Object, Long, String> lookupWithinTime(Map<Object, Map<TimeWindow, String>> lookupSource) {
        return (id, time) -> {
            Map<TimeWindow, String> mappings = lookupSource.get(id);
            if (mappings != null) {
                for (Map.Entry<TimeWindow, String> entry : mappings.entrySet()) {
                    if (entry.getKey().contains(time)) {
                        return entry.getValue();
                    }
                }
            }
            return "Unknown";
        };
    }

    public static UDF3<Object, Long, String, String> lookupWith(
            Map<Object, Map<TimeWindow, Map<String, String>>> source) {
        return (id, time, field) -> {
            Map<TimeWindow, Map<String, String>> mappings = source.get(id);
            if (mappings != null) {
                for (Map.Entry<TimeWindow, Map<String, String>> entry : mappings.entrySet()) {
                    if (entry.getKey().contains(time)) {
                        return entry.getValue().get(field);
                    }
                }
            }
            return "Unknown";
        };
    }

    //We accept some poor-man types to avoid circular links with api.backport.domain filter objects.
    public static <T> UDF1<Row, Boolean> vectorFilter(@NonNull List<T> predicateValues,
            @NonNull UDFFilterOperand operator,
            @NonNull UDFVectorRestriction restrictionType) {
        return (Row cell) -> {

            StructField field = getArrayField(cell);
            List<Object> cellValues = cell.getList(cell.fieldIndex(ARRAY_ELEMENTS_FIELD_NAME));

            DataType dataType = ((ArrayType) field.dataType()).elementType();
            Predicate<Object> predicate = getPredicate(predicateValues, operator, dataType);

            return UDFVectorRestriction.ALL.equals(restrictionType) ?
                    cellValues.stream().allMatch(predicate) : cellValues.stream().anyMatch(predicate);

        };
    }

    //This UDF filters the element from the vector that match the condition
    public static <T> UDF1<Row, Row> vectorFilterMatchedElements(@NonNull List<T> predicateValues,
            @NonNull UDFFilterOperand operator) {
        return (Row cell) -> {

            StructField field = getArrayField(cell);
            List<Object> cellValues = cell.getList(cell.fieldIndex(ARRAY_ELEMENTS_FIELD_NAME));

            DataType dataType = ((ArrayType) field.dataType()).elementType();
            Predicate<Object> predicate = getPredicate(predicateValues, operator, dataType);

            List<Object> filteredValues = cellValues.stream().filter(predicate).collect(
                    Collectors.toList());

            List<Object> dims = cell.getList(cell.fieldIndex(ARRAY_DIMENSIONS_FIELD_NAME));

            return RowFactory.create(filteredValues, dims);

        };
    }

    private static StructField getArrayField(Row cell) {
        int index = cell.schema().fieldIndex(ARRAY_ELEMENTS_FIELD_NAME);
        if (index < 0) {
            throw new IllegalArgumentException(
                    "This field schema [" + cell.schema() + "] is not an array supported by NXCALS, missing "
                            + ARRAY_ELEMENTS_FIELD_NAME + " field");
        }
        return cell.schema().fields()[index];
    }

    /**
     * We have to support BOOLEAN, INT, LONG, FLOAT, DOUBLE, STRING types only, all below INT like SHORT is converted to INT at ingestion.
     * Currently cannot have Boolean based Filters in BackportAPI but kept the implementation still.
     * This returns a predicate to execute on each/any element of the input array.
     */
    @SuppressWarnings("squid:S3776") //complexity
    private static <T> Predicate<Object> getPredicate(@NonNull List<T> values,
            @NonNull UDFFilterOperand operator, @NonNull DataType dataType) {

        boolean intOrLong = DataTypes.IntegerType.equals(dataType) || DataTypes.LongType.equals(dataType);
        boolean floatOrDouble = DataTypes.FloatType.equals(dataType) || DataTypes.DoubleType.equals(dataType);

        switch (operator) {
        case EQUALS:
            if (intOrLong) {
                return val -> longVal(val) == longVal(values.get(0));
            } else if (floatOrDouble) {
                return val -> Math.abs(doubleVal(val) - doubleVal(values.get(0))) < DOUBLE_COMPARE_THRESHOLD;
            } else {
                return val -> val.equals(values.get(0));
            }
        case IN:
            if (intOrLong) {
                return val -> values.stream().anyMatch(v -> longVal(v) == longVal(val));
            } else if (floatOrDouble) {
                return val -> values.stream()
                        .anyMatch(v -> Math.abs(doubleVal(v) - doubleVal(val)) < DOUBLE_COMPARE_THRESHOLD);
            } else {
                return val -> values.stream().anyMatch(v -> v.equals(val));
            }

        case NOT_IN:
            if (intOrLong) {
                return val -> values.stream().noneMatch(v -> longVal(v) == longVal(val));
            } else if (floatOrDouble) {
                return val -> values.stream()
                        .noneMatch(v -> Math.abs(doubleVal(v) - doubleVal(val)) < DOUBLE_COMPARE_THRESHOLD);
            } else {
                return val -> values.stream().noneMatch(v -> v.equals(val));
            }
        case GREATER:
            if (intOrLong) {
                return val -> longVal(val) > longVal(values.get(0));
            } else if (floatOrDouble) {
                return val -> doubleVal(val) > doubleVal(values.get(0));
            }
            break;
        case GREATER_OR_EQUALS:
            if (intOrLong) {
                return val -> longVal(val) >= longVal(values.get(0));
            } else if (floatOrDouble) {
                return val -> doubleVal(val) >= doubleVal(values.get(0));
            }
            break;
        case LESS:
            if (intOrLong) {
                return val -> longVal(val) < longVal(values.get(0));
            } else if (floatOrDouble) {
                return val -> doubleVal(val) < doubleVal(values.get(0));
            }
            break;
        case LESS_OR_EQUALS:
            if (intOrLong) {
                return val -> longVal(val) <= longVal(values.get(0));
            } else if (floatOrDouble) {
                return val -> doubleVal(val) <= doubleVal(values.get(0));
            }
            break;
        case BETWEEN:
            if (values.size() != 2) {
                throw new IllegalArgumentException("BETWEEN operator accepts only 2 values for input, given:" + values);
            }
            if (intOrLong) {
                return val -> longVal(val) >= longVal(values.get(0)) &&
                        longVal(val) <= longVal(values.get(1));
            } else if (floatOrDouble) {
                return val -> doubleVal(val) >= doubleVal(values.get(0)) &&
                        doubleVal(val) <= doubleVal(values.get(1));
            }
            break;
        default:
            throw notSupportedException(operator, dataType);
        }
        throw notSupportedException(operator, dataType);
    }

    private static long longVal(Object val) {
        return ((Number) val).longValue();
    }

    private static double doubleVal(Object val) {
        return ((Number) val).doubleValue();
    }

    private static IllegalArgumentException notSupportedException(@NonNull UDFFilterOperand operator,
            DataType dataType) {
        return new IllegalArgumentException("Operator " + operator + " not supported for " + dataType + " arrays");
    }

    /**
     * UDF to identify the first element (if it exists) that is less than or equal to the provided timestamp.
     * It is achieved by identifying the index of the first greater timestamp in the available start ranges and
     * selecting the exact previous element on that array.
     * <b>Note: The provided array must be sorted to avoid undefined results on binary search</>
     *
     * @param startRanges a {@code long} array that contains all available start ranges
     * @return a {@link Long} value representing the matching start range for the given timestamp
     * or {@code null} if the query timestamp is null or less than the minimum start range
     */
    public UDF1<Long, Long> findFirstLessThanOrEqual(@NonNull Broadcast<long[]> startRanges) {
        return timestamp -> {
            if (timestamp != null) {
                long[] arr = startRanges.value();
                int upperBoundIndex = upperBound(arr, timestamp);
                if (upperBoundIndex > 0) {
                    return arr[upperBoundIndex - 1];
                }
            }
            //indicates that provided timestamp is null or less than min start range
            return null;
        };
    }

    /**
     * This method is utilizing the standard {@link Arrays#binarySearch} convention, to identify the index of the first
     * greater element (if it exists) in the array, based on the provided search key.
     * The behaviour is expected to be similar with standard upper bound algorithm (ex. std::upper_bound in C++)
     *
     * @param arr a {@code long} array of elements
     * @param key a {@code long} value key to search for
     * @return a {@code int} value representing the index of the immediate next element that is greater
     * than the provided search key. The return value is guaranteed to be >= 0.
     * If the key is less than the first element of the array, the return value would be equal to {@literal 0}.
     * Accordingly, if the key is bigger than the all the elements of the array, the return value would the length of the array itself.
     * @see <a href="https://en.cppreference.com/w/cpp/algorithm/upper_bound">std::upper_bound</a>
     */
    private int upperBound(long[] arr, long key) {
        int index = Arrays.binarySearch(arr, key);
        return Math.abs(index + 1);
    }

    public Row asRow(Object... items) {
        return Row.fromSeq(JavaConverters.asScalaBuffer(Arrays.asList(items)));
    }

    public StructField createNullableField(String name, DataType type) {
        return DataTypes.createStructField(name, type, true);
    }

    public StructType createTypeFromFields(StructField... fields) {
        return DataTypes.createStructType(fields);
    }

    public StructType decodeColumnNames(StructType schema) {
        StructField[] fields = Arrays.stream(schema.fields()).map(field -> {
            String fieldName = desanitizeIfNeeded(field.name());
            DataType fieldType = field.dataType();
            if (fieldType instanceof StructType) {
                StructType innerSchema = (StructType) fieldType;
                fieldType = decodeColumnNames(innerSchema);
            }
            return field.copy(fieldName, fieldType, field.nullable(), field.metadata());
        }).toArray(StructField[]::new);
        return new StructType(fields);
    }
}