package cern.nxcals.common.domain;

public interface ExtractionTaskProcessor<T> {
    T execute(HBaseExtractionTask task);

    T execute(HdfsExtractionTask task);
}
