/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.common.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.net.URI;
import java.util.List;

import static com.fasterxml.jackson.annotation.JsonTypeInfo.As.PROPERTY;
import static com.fasterxml.jackson.annotation.JsonTypeInfo.Id.NAME;

@JsonTypeInfo(use = NAME, include = PROPERTY)
@JsonSubTypes({
        @JsonSubTypes.Type(value = CompactionJob.class, name = "CompactionJob"),
        @JsonSubTypes.Type(value = MergeCompactionJob.class, name = "MergeCompactionJob"),
        @JsonSubTypes.Type(value = AdaptiveCompactionJob.class, name = "AdaptiveCompactionJob"),
        @JsonSubTypes.Type(value = AdaptiveMergeCompactionJob.class, name = "AdaptiveMergeCompactionJob"),
        @JsonSubTypes.Type(value = RestageJob.class, name = "RestageJob"),
})
public interface DataProcessingJob  {
    enum JobType {
        /**
         * The same as @see COMPACT but using adaptive partitioning schemas (like time partitions/bucketing/row group size, etc).
         */
        ADAPTIVE_COMPACT,
        /**
         * OBSOLETE, @see ADAPTIVE_COMPACT
         * De-duplicates and compacts a given collection of files (usually of Avro format)
         * and exports the result to the destination directory
         */
        COMPACT,

        /**
         * Collects parquet files from destination directory, partitions them by hour and place them on
         * re-staging directories separated by hour
         */
        RESTAGE,

        /**
         * OBSOLETE, @see ADAPTIVE_MERGE_COMPACT
         * Merges and replaces re-staged data with the re-processed files from destination directory.
         * Then, de-duplicates and compacts the merged data and exports the result to the destination directory
         */
        MERGE_COMPACT,
        /**
         * Merges and replaces re-staged data with the re-processed files from destination directory.
         * Then, de-duplicates and compacts the merged data and exports the result to the destination directory
         * However, to save files, it uses newer, adaptive format
         */
        ADAPTIVE_MERGE_COMPACT
    }

    long getSystemId();

    URI getDestinationDir();

    URI getSourceDir();

    List<URI> getFiles();

    String getFilePrefix();

    long getTotalSize();

    long getJobSize();

    @JsonIgnore
    JobType getType();

    int getPartitionCount();

    boolean isSortEnabled();

}
