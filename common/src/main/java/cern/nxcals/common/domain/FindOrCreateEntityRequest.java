/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.common.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import java.util.Map;

@Data
@Builder(builderClassName = "Builder")
@JsonDeserialize(builder = FindOrCreateEntityRequest.Builder.class)
public class FindOrCreateEntityRequest {
    @NonNull
    private final Map<String, Object> entityKeyValues;
    @NonNull
    private final Map<String, Object> partitionKeyValues;
    private final String schema;

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
    }
}
