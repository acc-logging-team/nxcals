package cern.nxcals.common.domain;

import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;

import java.net.URI;
import java.util.List;

@Value
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
@SuperBuilder
@Jacksonized
public class RestageJob extends AbstractCompactionJob {

    URI destinationDir;
    URI sourceDir;

    URI restagingReportFile;

    //This is a list instead of set as it is much easier to test it afterwards.
    @ToString.Exclude
    List<URI> files;
    @ToString.Exclude
    List<URI> dataFiles;
    String filePrefix;
    long totalSize;
    long jobSize;
    long systemId;

    @Override
    public JobType getType() {
        return JobType.RESTAGE;
    }

    @Override
    public int getPartitionCount() {
        return 0;
    }

    @Override
    public boolean isSortEnabled() {
        return false;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
    }
}
