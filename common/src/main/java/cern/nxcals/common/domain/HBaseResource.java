package cern.nxcals.common.domain;

import cern.nxcals.common.utils.AvroUtils;
import cern.nxcals.common.utils.Lazy;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.jackson.Jacksonized;
import org.apache.avro.Schema;

@Getter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
//@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder(toBuilder = true, builderClassName = "Builder")
@Jacksonized
public class HBaseResource {
    @NonNull
    @EqualsAndHashCode.Include
    private final String tableName;
    @EqualsAndHashCode.Include
    @NonNull
    private final String schemaJson;
    @NonNull
    @EqualsAndHashCode.Include
    private final String namespace;
    @JsonIgnore
    @NonNull
    private final Lazy<Schema> schema;
    private final boolean isAccessible;

    public static class Builder {
        public HBaseResource build() {
            return new HBaseResource(tableName, schemaJson, namespace, AvroUtils.schemaLazy(schemaJson), isAccessible);
        }
    }

}
