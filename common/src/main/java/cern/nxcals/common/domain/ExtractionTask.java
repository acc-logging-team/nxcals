package cern.nxcals.common.domain;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;
import lombok.NonNull;
import lombok.Singular;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.util.Collection;
import java.util.List;

//@formatter:off
@JsonTypeInfo( use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = HBaseExtractionTask.class, name = "hbase"),
        @JsonSubTypes.Type(value = HdfsExtractionTask.class, name = "hdfs"),
})
//@formatter:on
@Getter
@SuperBuilder(toBuilder = true)
@ToString
public abstract class ExtractionTask {
    @NonNull
    @Singular
    private final List<ColumnMapping> columns;
    @NonNull
    private final Collection<String> predicates;
    private final ColumnMapping variableFieldBoundColumnMapping;

    public abstract <T> T processWith(ExtractionTaskProcessor<T> processor);

}
