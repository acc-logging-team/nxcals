package cern.nxcals.common.domain;

import cern.nxcals.api.domain.TimeEntityPartitionType;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.Value;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;

@Value
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
@SuperBuilder
@Jacksonized
public class AdaptiveMergeCompactionJob extends MergeCompactionJob {
    @NonNull
    TimeEntityPartitionType partitionType;

    long parquetRowGroupSize;
    long hdfsBlockSize;

    @Override
    public JobType getType() {
        return JobType.ADAPTIVE_MERGE_COMPACT;
    }

    @Override
    public int getPartitionCount() {
        return partitionType.slots();
    }
}
