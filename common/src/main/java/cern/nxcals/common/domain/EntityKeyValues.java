package cern.nxcals.common.domain;

import cern.nxcals.common.utils.StringUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.annotation.concurrent.NotThreadSafe;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Represents a container for key:value list with wildcard handling.
 */
@NotThreadSafe
@Builder(builderClassName = "Builder", toBuilder = true)
@JsonDeserialize(builder = EntityKeyValues.Builder.class)
@Data
@ToString
@EqualsAndHashCode
public class EntityKeyValues {
    private final Map<String, EntityKeyValue> keyValues;

    @JsonIgnore
    public boolean isAnyWildcard() {
        return keyValues.values().stream().anyMatch(EntityKeyValue::isWildcard);
    }

    @JsonIgnore
    public boolean isEmpty() {
        return keyValues.isEmpty();
    }

    public Collection<EntityKeyValue> get() {
        return Collections.unmodifiableCollection(this.keyValues.values());
    }

    @JsonIgnore
    public Map<String, Object> getAsMap() {
        boolean isAnyWildcard = isAnyWildcard();
        return keyValues.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey,
                        e -> escapeIfNeeded(isAnyWildcard, e.getValue())));
    }

    private Object escapeIfNeeded(boolean isAnyWildcard, EntityKeyValue entityKeyValue) {
        Object value = entityKeyValue.getValue();
        // We have to escape those that are not with wildcard for the overall wildcard condition
        // (when there are some keys with wildcards)
        if (isAnyWildcard && !entityKeyValue.isWildcard()) {
            return escape(value);
        } else {
            return value;
        }
    }

    /**
     * Escaping only String type value objects.
     *
     * @param value value to escape - if String, will escape wildcard characters
     * @return object with escaped wildcard character (if was String)
     */
    private Object escape(Object value) {
        if (value instanceof String) {
            return StringUtils.escapeWildcards((String) value);
        } else {
            return value;
        }
    }

    @JsonPOJOBuilder(withPrefix = "")
    @SuppressWarnings("squid:S1450") // parameter can be made local
    public static final class Builder {
        private Map<String, EntityKeyValue> keyValues = new HashMap<>();

        public Builder fromMap(boolean isWildcard, Map<String, Object> keyValuesMap) {
            keyValuesMap.forEach((key, value) -> {
                EntityKeyValue entityKeyValue = EntityKeyValue.builder().key(key).value(value)
                        .wildcard(isWildcard).build();
                keyValue(entityKeyValue);
            });
            return this;
        }

        public Builder keyValue(EntityKeyValue value) {
            if (keyValues.containsKey(value.getKey())) {
                throw new IllegalArgumentException("Key already exists: " + value.getKey());
            }
            keyValues.put(value.getKey(), value);
            return this;
        }
    }
}
