package cern.nxcals.common.domain;

import cern.nxcals.api.domain.TimeWindow;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import lombok.extern.jackson.Jacksonized;

import java.util.List;
import java.util.Map;

@Data
@Builder(toBuilder = true)
@Jacksonized
public class ExtractionUnit {
    @NonNull
    private final List<ExtractionTask> tasks;
    @NonNull
    private final ExtractionCriteria criteria;
    @NonNull
    private final Map<Long, List<VariableMapping>> mappings;
    @NonNull
    private final List<ColumnMapping> emptyDatasetMapping;

    public boolean isVariableSearch() {
        return criteria.isVariableSearch();
    }

    @Data
    @lombok.Builder(toBuilder = true)
    @Jacksonized
    public static class VariableMapping {
        private final TimeWindow window;
        private final Map<String, String> fieldToVariableName;
    }
}
