package cern.nxcals.common.domain;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;

import java.net.URI;
import java.util.List;

/**
 * Represents the compaction job data required by the Compactor to work
 */
@Value
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
@SuperBuilder
@Jacksonized
public class CompactionJob extends AbstractCompactionJob {

    String filePrefix;
    URI destinationDir;
    URI sourceDir;

    //This is a list instead of set as it is much easier to test it afterwards.
    @ToString.Exclude
    List<URI> files;

    long totalSize;
    long jobSize;
    int partitionCount;
    long systemId;
    boolean sortEnabled;

    @Override
    public JobType getType() {
        return JobType.COMPACT;
    }
}
