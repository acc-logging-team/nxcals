package cern.nxcals.common.domain;

import lombok.Getter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;

import java.net.URI;
import java.util.Set;

@Getter
@Jacksonized
@SuperBuilder(toBuilder = true)
@ToString(callSuper = true)
public class HdfsExtractionTask extends ExtractionTask {
    private final Set<URI> paths;

    @Override
    public <T> T processWith(ExtractionTaskProcessor<T> processor) {
        return processor.execute(this);
    }

}
