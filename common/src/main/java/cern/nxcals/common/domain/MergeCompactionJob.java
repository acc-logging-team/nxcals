package cern.nxcals.common.domain;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;
import lombok.experimental.NonFinal;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;

import java.net.URI;
import java.util.List;

@Value
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
@SuperBuilder
@Jacksonized
@NonFinal
public class MergeCompactionJob extends AbstractCompactionJob {

    String filePrefix;
    URI destinationDir;
    URI sourceDir;
    URI restagingReportFile;
    //This is a list instead of set as it is much easier to test it afterwards.
    @ToString.Exclude
    List<URI> files;

    long totalSize;
    long jobSize;
    int partitionCount;
    long systemId;
    boolean sortEnabled;

    @Override
    public JobType getType() {
        return JobType.MERGE_COMPACT;
    }

}
