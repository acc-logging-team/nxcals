package cern.nxcals.common.domain;

import lombok.Builder;
import lombok.Value;

@Value
@Builder(toBuilder = true, builderClassName = "Builder")
public class CallDetails {
    private String applicationName;
    private String declaringClass;
    private String methodName;
}
