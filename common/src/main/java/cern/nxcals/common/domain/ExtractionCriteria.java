package cern.nxcals.common.domain;

import cern.nxcals.api.domain.TimeWindow;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import lombok.Singular;
import lombok.extern.jackson.Jacksonized;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Data
@Builder(toBuilder = true, builderClassName = "Builder")
@Jacksonized
public class ExtractionCriteria {
    private final String systemName;
    @Singular
    @NonNull
    private final Map<String, Boolean> variables;
    @Singular
    private final Set<Long> variableIds;
    @Singular
    @NonNull
    private final List<EntityKeyValues> entities;
    @Singular
    private final Set<Long> entityIds;
    @NonNull
    private final TimeWindow timeWindow;
    @Singular
    private final Map<String, List<String>> aliasFields;

    @JsonIgnore
    public boolean isVariableSearch() {
        return !this.variables.isEmpty() || !this.variableIds.isEmpty();
    }
}
