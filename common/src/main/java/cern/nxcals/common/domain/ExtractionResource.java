package cern.nxcals.common.domain;

import cern.nxcals.api.domain.EntityHistory;
import cern.nxcals.api.domain.Resource;
import cern.nxcals.api.domain.TimeWindow;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
public class ExtractionResource {
    @NonNull
    private final EntityHistory entityHistory;
    @NonNull
    private final Resource resource;
    @NonNull
    private final TimeWindow timeWindow;
}
