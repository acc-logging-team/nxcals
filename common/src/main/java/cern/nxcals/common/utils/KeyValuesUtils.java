/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.common.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.experimental.UtilityClass;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericRecordBuilder;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static java.lang.String.format;

@UtilityClass
public class KeyValuesUtils {
    private static final String ERROR_MISSING_KEY_IN_KEY_VALUES = "There is not field %s in key values but it was defined in key values definition";
    private static final String ERROR_MISMATCHED_NUMBER_OF_KEYS = "Number of fields in key values definition must match key values, expected fields: %s, got: %s";
    private static final ObjectMapper OBJECT_MAPPER = GlobalObjectMapperProvider.get();
    private static final Map<String, Schema> SYSTEM_SCHEMA_CACHE = new ConcurrentHashMap<>();

    public static String convertMapIntoAvroSchemaString(Map<String, Object> entityKeyValuesMap,
            String keyValuesSchema) {
        Schema avroSchema = SYSTEM_SCHEMA_CACHE.computeIfAbsent(keyValuesSchema, k -> new Schema.Parser().parse(k));
        GenericRecordBuilder genericRecordBuilder = new GenericRecordBuilder(avroSchema);
        List<Schema.Field> fields = avroSchema.getFields();
        if (fields.size() != entityKeyValuesMap.size()) {
            throw new IllegalArgumentException(format(ERROR_MISMATCHED_NUMBER_OF_KEYS, fields, entityKeyValuesMap));
        }

        for (Schema.Field field : fields) {
            String fieldName = field.name();
            if (!entityKeyValuesMap.containsKey(fieldName)) {
                throw new IllegalArgumentException(format(ERROR_MISSING_KEY_IN_KEY_VALUES, fieldName));
            }
            genericRecordBuilder.set(field, entityKeyValuesMap.get(fieldName));
        }

        return genericRecordBuilder.build().toString();
    }

    public static Map<String, Object> convertKeyValuesStringIntoMap(String keyValues) {
        try {
            return OBJECT_MAPPER.readValue(keyValues, new TypeReference<Map<String, Object>>() {
            });
        } catch (IOException exception) {
            throw new UncheckedIOException("Cannot deserialize from json", exception);
        }
    }
}
