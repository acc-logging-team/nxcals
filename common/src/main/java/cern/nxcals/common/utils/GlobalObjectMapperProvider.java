package cern.nxcals.common.utils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.util.Arrays;
import java.util.List;

public enum GlobalObjectMapperProvider {
    INSTANCE;

    private final List<Module> modules = Arrays.asList(new JavaTimeModule(), new CustomJacksonModule(), new Jdk8Module());
    private final ObjectMapper mapper = new ObjectMapper();

    GlobalObjectMapperProvider() {
        mapper.registerModules(modules);
        mapper.getSerializerProvider().setNullKeySerializer(new CustomJacksonModule.NullKeySerializer(Object.class));
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public static ObjectMapper get() {
        return GlobalObjectMapperProvider.INSTANCE.mapper;
    }
}
