package cern.nxcals.common.utils;

import lombok.NonNull;
import lombok.experimental.UtilityClass;

import java.util.function.UnaryOperator;

@UtilityClass
public class UnaryOperatorUtils {
    public static <T> UnaryOperator<T> compose(@NonNull UnaryOperator<T> first, @NonNull UnaryOperator<T> second) {
        return x -> second.apply(first.apply(x));
    }

}
