package cern.nxcals.common.utils;

import com.google.common.collect.Iterables;
import lombok.experimental.UtilityClass;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Stream;

@UtilityClass
public class OptionalUtils {
    // lack of Java 9 API
    public static <T> Stream<T> toStream(Optional<T> optional) {
        return optional.map(Stream::of).orElseGet(Stream::empty);
    }

    public static <T> Optional<T> toOptional(@Nullable Collection<T> collection) {
        return collection == null || collection.isEmpty() ?
                Optional.empty() :
                Optional.of(Iterables.getOnlyElement(collection));
    }
}
