/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.common.utils;

import lombok.NonNull;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.avro.Schema;
import org.apache.avro.Schema.Field;
import org.apache.avro.file.CodecFactory;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.BinaryDecoder;
import org.apache.avro.io.BinaryEncoder;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.DecoderFactory;
import org.apache.avro.io.EncoderFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static org.apache.avro.Schema.Type.BOOLEAN;
import static org.apache.avro.Schema.Type.DOUBLE;
import static org.apache.avro.Schema.Type.FLOAT;
import static org.apache.avro.Schema.Type.INT;
import static org.apache.avro.Schema.Type.LONG;
import static org.apache.avro.Schema.Type.RECORD;
import static org.apache.avro.Schema.Type.STRING;
import static org.apache.avro.Schema.Type.UNION;

/**
 * Utility class for Avro stuff.
 */
@UtilityClass
@Slf4j
public class AvroUtils {
    private final Set<Schema.Type> PRIMITIVE_NUMERIC_TYPES = Collections.unmodifiableSet(
            EnumSet.of(BOOLEAN, INT, LONG, FLOAT, DOUBLE));

    public boolean isPrimitiveNumericType(@NonNull Schema.Type type) {
        return PRIMITIVE_NUMERIC_TYPES.contains(type);
    }

    public boolean isPrimitiveType(@NonNull Schema.Type type) {
        return STRING.equals(type) || isPrimitiveNumericType(type);
    }

    public boolean isUnion(@NonNull Schema.Type type) {
        return UNION.equals(type);
    }

    /**
     * Checks if the given set of Avro field types, contains any non-primitive type definitions.
     *
     * @param types a {@link Set} of {@link Schema.Type} instances denoting all the available avro schema types
     * @return {@code true} value that dictates that the given set of types, contains non-primitive values.
     * {@code false} instead (if that set of types contains only primitive types).
     */
    public boolean hasNonPrimitiveTypes(@NonNull Set<Schema.Type> types) {
        return types.stream().anyMatch(type -> !isPrimitiveNumericType(type));
    }

    public static Schema.Type getSchemaTypeFor(@NonNull Schema schema) {
        if (isUnion(schema.getType())) {
            return getSchemaTypeFor(getSchemaUnionType(schema));
        }
        return schema.getType();
    }

    public static Schema getSchemaUnionType(@NonNull Schema schema) {
        return schema.getTypes().stream().filter(s -> s.getType() != Schema.Type.NULL).findFirst().orElseThrow(
                () -> new IllegalArgumentException("Provided UNION schema is missing field type " + schema));
    }

    public byte[] serializeToAvro(Object record, BinaryEncoder encoder, GenericDatumWriter<Object> writer) {
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
            encoder = EncoderFactory.get().directBinaryEncoder(bos, encoder);
            writer.write(record, encoder);
            return bos.toByteArray();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public GenericRecord deserializeFromAvro(byte[] content, DatumReader<GenericRecord> reader, BinaryDecoder decoder,
            GenericRecord reuse) {
        try {
            decoder = DecoderFactory.get().binaryDecoder(content, decoder);
            return reader.read(reuse, decoder);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    /**
     * Checks if the given schema definition is present with non-empty fields in the given record
     */
    public boolean isChildSchemaCoveredWithNonEmptyValues(GenericRecord record, Schema schema) {
        for (Field f : schema.getFields()) {
            if (record.get(f.name()) == null) {
                return false;
            }
        }
        return true;
    }

    /**
     * <p>
     * Merges two schemas. The schemas must be compatible which means they can contain fields with the same names as
     * long as their definition is also the same.
     * </p>
     * Uses {@code newSchema} meta data for the merged schema.
     */
    public Schema mergeSchemas(Schema oldSchema, Schema newSchema) {
        if (oldSchema == null) {
            return newSchema;
        }
        if (newSchema == null) {
            return oldSchema;
        }

        if (!(RECORD.equals(oldSchema.getType()) && RECORD.equals(newSchema.getType()))) {
            return newSchema;
        }

        verifyCompatibility(oldSchema, newSchema);
        List<Field> fields = new ArrayList<>(oldSchema.getFields());
        fields.addAll(newSchema.getFields());
        fields = copyFieldsFrom(fields.stream());

        Schema schema = Schema.createRecord(newSchema.getName(), newSchema.getDoc(), newSchema.getNamespace(),
                newSchema.isError());
        schema.setFields(fields);
        return schema;
    }

    /**
     * Verifies if the given schemas contain compatible fields, that is if both schema contain a field with the same
     * name its definitions must be the same.
     */
    private void verifyCompatibility(Schema oldSchema, Schema newSchema) {
        for (Field f : oldSchema.getFields()) {
            Field field = newSchema.getField(f.name());
            if (field == null) {
                continue;
            }
            if (!f.equals(field)) {
                throw new IllegalStateException(
                        "Conflict between schemas detected with field " + f + " and field " + field);
            }
        }
    }

    private List<Field> copyFieldsFrom(Stream<Field> fields) {
        return fields.distinct().map(f -> new Field(f.name(), f.schema(), f.doc(), f.defaultVal())).collect(toList());
    }

    public DataFileWriter<Object> createDataFileWriter(Schema schema, OutputStream outstream, String codec,
            int syncIntervalBytes) throws IOException {

        DataFileWriter<Object> result = null;
        try {
            result = new DataFileWriter<>(new GenericDatumWriter<>(schema));
            //TODO - check if this is needed realy - first glance - this is changing the number of xceivers on hadoop side as the flush is much more often.
            //Has to be balanced with the memory pressure - too big value causes OOM.
            result.setSyncInterval(syncIntervalBytes);
            result.setFlushOnEveryBlock(false);

            CodecFactory codecFactory = CodecFactory.fromString(codec);
            result.setCodec(codecFactory);
            result.create(schema, outstream);

            return result;
        } catch (IOException e) {
            log.error("Cannot create DataFileWriter for schema {} and {}", schema, e);
            silentClose(result);
            throw e;
        }
    }

    public void silentClose(DataFileWriter<Object> writer) {
        try {
            if (writer != null) {
                writer.close();
            }
        } catch (IOException ex) {
            log.error("Error closing DataFileWriter", ex);
        }
    }

    public static Lazy<Schema> schemaLazy(String schemaJson) {
        return new Lazy<>(() -> parseSchema(schemaJson));
    }

    public static Schema parseSchema(String schemaJson) {
        return schemaJson != null ? new Schema.Parser().parse(schemaJson) : null;
    }

    /**
     * Get Schema.Field which is under given path
     */
    public Optional<Schema.Field> findField(Schema schema, String path) {
        String[] fields = path.split("\\.");
        return findField(schema, fields, 0);
    }

    /**
     * @param schema    schema from which x should be extracted
     * @param pathParts parts of path to given field
     * @param index     index in pathParts - how deep is recursion
     * @return field from schema, which is under given path, or Optional.Empty if there is no such field
     */
    private Optional<Schema.Field> findField(Schema schema, String[] pathParts, int index) {
        if (index >= pathParts.length - 1 || schema.getField(pathParts[index]) == null) {
            return Optional.ofNullable(schema.getField(pathParts[index]));
        }
        Optional<Schema> maybeNestedFieldSchema = schema.getField(pathParts[index]).schema().getTypes().stream()
                .filter(x -> x.getType() == Schema.Type.RECORD).findFirst();

        return maybeNestedFieldSchema.flatMap(
                nestedFieldSchema -> findField(nestedFieldSchema, pathParts, index + 1));
    }

    public static Set<String> getAllFieldPaths(Field field) {
        HashSet<String> result = new HashSet<>();
        Schema.Type type = AvroUtils.getSchemaTypeFor(field.schema());
        result.add(field.name());
        if (type == Schema.Type.RECORD) {
            Schema record = AvroUtils.getSchemaUnionType(field.schema());
            for (Field childField : record.getFields()) {
                for (String child : getAllFieldPaths(childField)) {
                    String newKey = field.name() + "." + child;
                    result.add(newKey);
                }
            }
        }
        return result;
    }

    public static Set<String> getAllFieldPaths(Schema schema) {
        return schema.getFields().stream()
                .flatMap(field -> getAllFieldPaths(field).stream()).collect(toSet());
    }
}
