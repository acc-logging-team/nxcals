package cern.nxcals.common.utils;

import lombok.experimental.UtilityClass;

import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@UtilityClass
public class SetUtils {
    public static <S1, S2> Set<S2> map(Set<S1> set, Function<S1, S2> mapper) {
        return set.stream().map(mapper).collect(Collectors.toSet());
    }
}
