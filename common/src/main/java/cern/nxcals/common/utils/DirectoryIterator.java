package cern.nxcals.common.utils;

import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;

public class DirectoryIterator implements Iterator<Path> {

    private final FileSystem fs;
    private final Path root;
    private final Predicate<FileStatus> skip;

    private final Deque<Path> subdirectories;

    private DirectoryIterator inner;
    private Optional<Path> next = Optional.empty();
    private boolean consumed;
    private boolean complete;

    private DirectoryIterator() {
        this.inner = null;
        this.consumed = true;
        this.complete = true;
        this.fs = null;
        this.root = null;
        this.skip = null;
        this.subdirectories = null;
    }

    public DirectoryIterator(FileSystem fs, Path root) throws IOException {
        this(fs, root, t -> false);
    }

    public DirectoryIterator(FileSystem fs, Path root, Predicate<FileStatus> skipOnContent) throws IOException {
        this.inner = new DirectoryIterator();
        this.consumed = true;
        this.complete = false;
        this.fs = Objects.requireNonNull(fs);
        this.root = Objects.requireNonNull(root);
        this.skip = Objects.requireNonNull(skipOnContent);
        this.subdirectories = subdirectories(root);
    }

    private Deque<Path> subdirectories(Path root) throws IOException {
        RemoteIterator<FileStatus> children = fs.listStatusIterator(root);

        Deque<Path> result = new LinkedList<>();
        while (children.hasNext()) {
            FileStatus child = children.next();
            if (child.isDirectory()) {
                result.push(child.getPath());
            }
        }

        return result;
    }

    @Override
    public Path next() {
        return consumeNext().orElseThrow(NoSuchElementException::new);
    }

    @Override
    public boolean hasNext() {
        next = findNext();
        return next.isPresent();
    }

    @Override
    public void remove() {
        try {
            fs.delete(next.orElseThrow(IllegalStateException::new), true);
            consumed = true;
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }

    private Optional<Path> consumeNext() {
        next = findNext();

        consumed = true;
        return next;
    }

    private Optional<Path> findNext() {
        if (!consumed) {
            return next;
        }

        if (complete) {
            return Optional.empty();
        }

        try {
            consumed = false;

            while (!subdirectories.isEmpty() && !inner.hasNext()) {
                inner = innerIterator(subdirectories.pop());
            }

            if (inner.hasNext()) {
                return inner.consumeNext();
            }

            complete = true;
            return includeRoot() ? Optional.of(root) : Optional.empty();
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }

    private DirectoryIterator innerIterator(Path root) throws IOException {
        return new DirectoryIterator(fs, root, skip);
    }

    private boolean includeRoot() throws IOException {
        RemoteIterator<FileStatus> children = fs.listStatusIterator(root);

        while (children.hasNext()) {
            if (skip.test(children.next())) {
                return false;
            }
        }

        return true;
    }
}
