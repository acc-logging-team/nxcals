package cern.nxcals.common.utils;

import javax.annotation.concurrent.ThreadSafe;
import java.util.function.Supplier;

@ThreadSafe
public class Lazy<T> {
    private Supplier<T> source;
    private volatile T value = null;

    public Lazy(Supplier<T> supplier) {
        source = setter(supplier);
    }

    public T get() {
        return source.get();
    }

    private Supplier<T> setter(Supplier<T> supplier) {
        return () -> {
            if (value == null) {
                synchronized (this) {
                    if (value == null) {
                        value = supplier.get();
                        source = getter();
                    }
                }
            }
            return value;
        };
    }

    private Supplier<T> getter() {
        return () -> value;
    }
}