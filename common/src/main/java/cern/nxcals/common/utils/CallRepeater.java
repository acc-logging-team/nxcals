package cern.nxcals.common.utils;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

@UtilityClass
@Slf4j
public class CallRepeater {
    public <T> T call(Supplier<T> supplier, String repeatMessage,
            int maxRetries, Function<Exception, RuntimeException> failException,
            List<Class<?>> nonRepeatableExceptions) {
        int retryCount = 0;
        Exception lastException;
        do {
            try {
                if (retryCount > 0) {
                    log.debug("Retrying call retry count={}", retryCount);
                }
                return supplier.get();
            } catch (Exception ex) {
                if (nonRepeatableExceptions.stream().anyMatch(clazz -> clazz.isInstance(ex))) {
                    throw ex;
                }
                ++retryCount;
                log.debug("Called failed, will retry if count={} < maxRetries={} - {}", retryCount, maxRetries,
                        repeatMessage, ex);

                lastException = ex;
            }
        } while (retryCount < maxRetries);

        throw failException.apply(lastException);
    }

}
