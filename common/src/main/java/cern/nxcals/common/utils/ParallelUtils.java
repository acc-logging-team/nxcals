package cern.nxcals.common.utils;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import lombok.experimental.UtilityClass;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static cern.nxcals.common.utils.ParallelUtils.IOParallelUtils.IORunnableExecutor.handleIO;

@UtilityClass
public class ParallelUtils {

    public static class IOParallelUtils {

        public interface IOConsumer<T> {
            void apply(T arg) throws IOException;
        }

        public interface IORunnable {
            void run() throws IOException;
        }

        /*
        This class aims to handle problems with IO operations in Future's and streams.
        Main point is to wrap Consumer or Callable like method and eventually return IOException if appeared during
        execution
         */
        public static class IORunnableExecutor implements Supplier<Optional<IOException>> {
            IORunnable operation;

            private IORunnableExecutor(IORunnable operation) {
                this.operation = operation;
            }

            @Override
            public Optional<IOException> get() {
                try {
                    operation.run();
                    return Optional.empty();
                } catch (IOException e) {
                    return Optional.of(e);
                }
            }

            public static Supplier<Optional<IOException>> handleIO(IORunnable operation) {
                return new IORunnableExecutor(operation);
            }

            public static <T> Function<T, Optional<IOException>> handleIO(IOConsumer<T> operation) {
                return arg -> handleIO(() -> operation.apply(arg)).get();
            }
        }

        public static <T> List<IOException> doIOOperations(Collection<T> elements, IOConsumer<T> consumer,
                ExecutorService executor) {
            List<CompletableFuture<Optional<IOException>>> futures = processParallelIfNeeded(elements,
                    handleIO(consumer), executor);

            return futures.stream()
                    .map(CompletableFuture::join) // wait for future to finish
                    .flatMap(OptionalUtils::toStream) // flatten and gather exceptions streams to list
                    .collect(Collectors.toList());
        }
    }

    public static <T, R> List<CompletableFuture<R>> processParallelIfNeeded(Collection<T> parts, Function<T, R> mapper,
            ExecutorService executor) {
        return processParallelIfNeeded(parts, mapper, executor, 1);
    }

    public static <T, R> List<CompletableFuture<R>> processParallelIfNeeded(Collection<T> parts, Function<T, R> mapper,
            ExecutorService executor, int parallelizationThreshold) {
        if (parts.size() > parallelizationThreshold) {
            Stream<CompletableFuture<R>> futures = parts.stream()
                    .map(part -> CompletableFuture.supplyAsync(() -> mapper.apply(part), executor));
            return futures.collect(Collectors.toList());
        } else {
            return parts.stream().map(part -> CompletableFuture.completedFuture(mapper.apply(part)))
                    .collect(Collectors.toList());
        }
    }

    public static ExecutorService createExecutor(int threadPoolSize, String threadNamePattern) {
        ThreadFactory namedThreadFactory = new ThreadFactoryBuilder().setNameFormat(threadNamePattern).build();
        return Executors.newFixedThreadPool(threadPoolSize, namedThreadFactory);
    }
}
