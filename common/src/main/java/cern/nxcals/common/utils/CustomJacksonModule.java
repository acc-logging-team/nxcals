package cern.nxcals.common.utils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.std.StdKeyDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

public class CustomJacksonModule extends SimpleModule {
    private static final String NULL_KEY_PLACEHOLDER = "NULL_KEY";

    public CustomJacksonModule() {
        addKeyDeserializer(String.class, new NullKeyDeserializer(Object.class));
    }

    static class NullKeyDeserializer extends StdKeyDeserializer {
        NullKeyDeserializer(Class<Object> cls) {
            super(-1, cls);
        }

        @Override
        public Object deserializeKey(String key, DeserializationContext ctxt) throws IOException {
            return NULL_KEY_PLACEHOLDER.equals(key) ? null : key;
        }
    }

    static class NullKeySerializer extends StdSerializer<Object> {
        NullKeySerializer(Class<Object> cls) {
            super(cls);
        }

        @Override
        public void serialize(Object nullKey, JsonGenerator jsonGenerator, SerializerProvider unused) throws IOException {
            jsonGenerator.writeFieldName(NULL_KEY_PLACEHOLDER);
        }
    }
}
