package cern.nxcals.common.utils;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.reflect.FieldUtils;

import java.lang.reflect.Constructor;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * For internal NXCALS use only, this is not a public API.
 */
@UtilityClass
public class ReflectionUtils {
    private static Map<Class, Constructor> cache = new ConcurrentHashMap<>();

    @CanIgnoreReturnValue
    @SneakyThrows
    public static <T> T enhanceWithId(T object, Long id) {
        FieldUtils.writeField(object, "id", id, true);
        return object;
    }

    @CanIgnoreReturnValue
    @SneakyThrows
    public static <T> T enhanceWithRecVersion(T object, Long version) {
        FieldUtils.writeField(object, "recVersion", version, true);
        return object;
    }


    @NonNull
    @SneakyThrows
    public static <T> T builderInstance(@NonNull Class<T> clazz) {
        return clazz.cast(cache.computeIfAbsent(clazz, ReflectionUtils::constructor).newInstance());
    }

    @SneakyThrows
    private static <T> Constructor<T> constructor(@NonNull Class<T> clazz) {
        Constructor<T> constructor = clazz.getDeclaredConstructor();
        constructor.setAccessible(true);
        return constructor;
    }
}
