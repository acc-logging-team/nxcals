/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.common.utils;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigException;
import com.typesafe.config.ConfigFactory;
import lombok.extern.slf4j.Slf4j;

/**
 * Simple wrapper over {@code com.typesafe.config.Config}.
 * jwozniak: replaced double-checking with synchronized lazy load into safe method using ENUM.
 * https://en.wikipedia.org/wiki/Double-checked_locking#Usage_in_Java
 *
 * @author Marcin Sobieszek, jwozniak
 * @date Jul 6, 2016 5:42:07 PM
 */
@Slf4j
public enum ConfigHolder {
    INSTANCE;

    private final Config config;

    ConfigHolder() {
        ConfigFactory.invalidateCaches();
        this.config = ConfigFactory.load();
    }

    public static Config getConfig() {
        return INSTANCE.config;
    }

    /**
     * This method loads options if they are present in the jvm, otherwise returns empty map.
     *
     * @param path Absolute path in config.
     * @return Map of requested options.
     */
    public static Config getConfigIfPresent(String path) {
        try {
            return INSTANCE.config.getConfig(path);
        } catch (ConfigException.Missing exception) {
            log.info("No {} config was provided", path);
            return ConfigFactory.empty();
        } catch (ConfigException.WrongType exception) {
            log.warn("{} config is malformed.", path);
            return ConfigFactory.empty();
        }
    }

    @SuppressWarnings("unchecked")
    public static <T> T getProperty(String name, T defaultValue) {
        T value = defaultValue;
        if (INSTANCE.config.hasPath(name)) {
            value = (T) INSTANCE.config.getAnyRef(name);
        }
        return value;
    }

    public static int getInt(String name, int defaultValue) {
        int value = defaultValue;
        if (INSTANCE.config.hasPath(name)) {
            value = INSTANCE.config.getInt(name);
        }
        return value;
    }

    public static boolean getBoolean(String name, boolean defaultValue) {
        boolean value = defaultValue;
        if (INSTANCE.config.hasPath(name)) {
            value = INSTANCE.config.getBoolean(name);
        }
        return value;
    }

}
