package cern.nxcals.common.utils;

import lombok.experimental.UtilityClass;

@UtilityClass
public class MathUtils {
    //positive modulo (copied from Spark pmod() as we need exactly the same), could not find this in any common utility libs...
    public long pmod(long a, long n) {
        long r = a % n;
        if (r < 0) {
            return (r + n) % n;
        } else {
            return r;
        }
    }

}
