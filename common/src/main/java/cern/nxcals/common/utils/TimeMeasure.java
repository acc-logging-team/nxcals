/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.common.utils;

import org.aspectj.lang.ProceedingJoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A base class that can be used for measuring execution time of methods in Eclipse.
 * Please configure your project to use AspectJ and define appropriate annotations for Aspects & advises.
 *
 * @author jwozniak
 */
public class TimeMeasure {
    private static final Logger LOGGER = LoggerFactory.getLogger(TimeMeasure.class);

    public Object doTimeMeasure(ProceedingJoinPoint pjp) throws Throwable {
        long start = System.nanoTime();
        Object retVal = pjp.proceed();
        long stop = System.nanoTime() - start;

        LOGGER.trace("Method {} took {} us", pjp, stop / 1000);
        return retVal;
    }
}
