/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.common.utils;

import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.codec.binary.Base32;
import org.apache.commons.lang3.StringUtils;

import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Class that converts name to comply with the avro field name convention. <br>
 * Previously converted values are cached for faster conversions of the same name.
 */
public final class IllegalCharacterConverter {

    @VisibleForTesting
    static final String PREFIX = "__NX_";
    @VisibleForTesting
    static final String CANNOT_DECODE_FIELD_NAME_ERROR = "Cannot decode field name [%s]";
    @VisibleForTesting
    static final String PREFIX_EXISTS_ERROR = "Field name [%s] cannot start with " + PREFIX;
    private static final int PREFIX_LENGTH = PREFIX.length();

    private static IllegalCharacterConverter illegalCharacterConverter;

    private final Map<String, String> encodingCache;
    private final Base32 base32 = new Base32();

    public static synchronized IllegalCharacterConverter get() {
        if (illegalCharacterConverter == null) {
            illegalCharacterConverter = new IllegalCharacterConverter(new ConcurrentHashMap<>());
        }

        return illegalCharacterConverter;
    }

    public static boolean isEncoded(String toTest) {
        return toTest != null && toTest.startsWith(PREFIX);
    }

    /**
     * This method verifies if the string is a legal avro field name
     * As in:{@link org.apache.avro.Schema.validateName} (private method)
     *
     * @param toTest the string to be tested
     * @return true is name is avro-compatible false otherwise.
     */
    public static boolean isLegal(String toTest) {
        if (StringUtils.isEmpty(toTest)) {
            return false;
        }

        char first = toTest.charAt(0);
        if (!(Character.isLetter(first) || first == '_')) {
            return false;
        }

        for (int i = 1; i < toTest.length(); i++) {
            char c = toTest.charAt(i);
            if (!(Character.isLetterOrDigit(c) || c == '_')) {
                return false;
            }
        }

        return true;
    }

    @VisibleForTesting
    IllegalCharacterConverter(Map<String, String> encodingCache) {
        this.encodingCache = encodingCache;
    }

    public String convertFromLegal(String name) {
        Objects.requireNonNull(name, "name to decode cannot be null!");
        if (!name.startsWith(PREFIX) || name.length() < PREFIX_LENGTH + 2) {
            throw new FieldNameException(String.format(CANNOT_DECODE_FIELD_NAME_ERROR, name));
        }
        return internalConvertFromLegal(name);
    }

    public String convertToLegal(String name) {
        Objects.requireNonNull(name, "name to encode cannot be null!");
        if (name.startsWith(PREFIX)) {
            throw new FieldNameException(String.format(PREFIX_EXISTS_ERROR, name));
        }
        return encodingCache.computeIfAbsent(name, this::internalConvertToLegal);
    }

    public String getLegalIfCached(String toConvert) {
        return encodingCache.getOrDefault(toConvert, toConvert);
    }

    /**
     * Converts any name into an avro field legal name based on Base32 encoding removing the trailing =. </br>
     * For signaling that the name is encoded a tag is added to the beginning of the encoded name, followed by the a
     * single digit with the length of the padding followed then by the encoded name. </br>
     * See RFC 4648 section 6 (https://tools.ietf.org/html/rfc4648#section-6)
     *
     * @param toEncode the string to encode
     * @return a String with the format {@link IllegalCharacterConverter#PREFIX} + number of trailing '=' characters + encoded string
     */
    private String internalConvertToLegal(String toEncode) {
        String encodedString = base32.encodeAsString(toEncode.getBytes(StandardCharsets.UTF_8));
        encodedString = StringUtils.stripEnd(encodedString, "=");
        return PREFIX + paddingLength(encodedString) + encodedString;
    }

    private int paddingLength(String encodedString) {
        return (8 - encodedString.length() % 8) % 8;
    }

    private String internalConvertFromLegal(String toDecode) {
        return new String(base32.decode(toDecode.substring(PREFIX_LENGTH + 1)), StandardCharsets.UTF_8);
    }

    static class FieldNameException extends RuntimeException {
        private FieldNameException(String message) {
            super(message);
        }
    }
}
