/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.common.avro;

import cern.nxcals.api.domain.EntitySchema;
import lombok.extern.slf4j.Slf4j;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData.Record;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.Decoder;
import org.apache.avro.io.DecoderFactory;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Decodes array of bytes into Avro record.
 */
@Slf4j
public class DefaultBytesToGenericRecordDecoder implements BiFunction<Long, byte[], GenericRecord> {
    private final Function<Long, EntitySchema> schemaProvider;
    private final ConcurrentHashMap<Long, Schema> schemaCache = new ConcurrentHashMap<>();

    public DefaultBytesToGenericRecordDecoder(Function<Long, EntitySchema> schemaProvider) {
        this.schemaProvider = Objects.requireNonNull(schemaProvider);
    }

    @Override
    public GenericRecord apply(Long schemaId, byte[] data) {
        Schema schema = this.schemaCache.computeIfAbsent(schemaId,
                schemaKey -> new Schema.Parser().parse(this.schemaProvider.apply(schemaId).getSchemaJson()));

        if (schema == null) {
            throw new IllegalArgumentException("Unknown schema data for id " + schemaId);
        }
        return this.decodeData(data, schema);
    }

    private GenericRecord decodeData(byte[] buffer, Schema schema) {
        Decoder decoder = DecoderFactory.get().binaryDecoder(buffer, null);
        GenericDatumReader<Record> rr = new GenericDatumReader<>(schema);
        try {
            return rr.read(null, decoder);
        } catch (IOException e) {
            log.error("Error while decoding record", e);
            throw new UncheckedIOException(e);
        }
    }
}
