/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.common.avro;

import cern.cmw.datax.EntryType;
import cern.cmw.datax.ImmutableData;
import cern.cmw.datax.ImmutableEntry;
import cern.nxcals.api.converters.ImmutableDataToAvroConverter;
import cern.nxcals.api.domain.KeyValues;
import cern.nxcals.common.converters.TimeConverter;
import cern.nxcals.common.utils.AvroUtils;
import cern.nxcals.common.utils.IllegalCharacterConverter;
import com.google.common.annotations.VisibleForTesting;
import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;
import org.apache.avro.SchemaBuilder.FieldAssembler;
import org.apache.avro.SchemaParseException;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static cern.cmw.datax.EntryType.DATA;
import static cern.nxcals.api.converters.ImmutableDataToAvroConverter.createGenericRecordFieldValue;
import static cern.nxcals.common.Schemas.ENTITY_ID;
import static cern.nxcals.common.Schemas.PARTITION_ID;
import static cern.nxcals.common.Schemas.SCHEMA_ID;
import static cern.nxcals.common.Schemas.SYSTEM_ID;
import static cern.nxcals.common.Schemas.TIMESTAMP;
import static cern.nxcals.common.avro.SchemaConstants.RECORD_NAMESPACE;
import static cern.nxcals.common.avro.SchemaConstants.RECORD_NAME_PREFIX;
import static java.util.Comparator.comparing;

public class DataEncoderImpl implements DataEncoder<KeyValues, KeyValues, String, Long> {

    private final Schema entityKeySchema;
    private final Schema partitionKeySchema;
    private final Schema timeKeyRecordSchema;
    private final Map<String, Schema.Field> specialFieldsSchemaMap;
    private final int specialFieldsCount;
    private final TimeConverter timeConverter;
    private boolean includeSystemFields = true;

    /**
     * Constructor that builds encoder based on supplied schemas.
     *
     * @param entityKeyDefs          Entity schema definition.
     * @param partitionKeyDefs       Partition schema definition.
     * @param timeKeyDefs            Time schema definition.
     * @param recordVersionKeySchema Record version schema definition.
     * @param timeConverter          Timestamp converter from generic record.
     */
    @SuppressWarnings("WeakerAccess")
    public DataEncoderImpl(Schema entityKeyDefs, Schema partitionKeyDefs, Schema timeKeyDefs,
            Schema recordVersionKeySchema, TimeConverter timeConverter) {
        this.timeConverter = timeConverter;
        this.entityKeySchema = Objects.requireNonNull(entityKeyDefs);
        this.partitionKeySchema = Objects.requireNonNull(partitionKeyDefs);
        this.timeKeyRecordSchema = Objects.requireNonNull(timeKeyDefs);

        // this schema is a merge of all special fields and its fields override the field from the record (for
        // non-nullability)
        this.specialFieldsSchemaMap = Collections.unmodifiableMap(
                Stream.of(this.entityKeySchema, this.partitionKeySchema, recordVersionKeySchema,
                        this.timeKeyRecordSchema).reduce(AvroUtils::mergeSchemas)
                        .orElseThrow(() -> new IllegalStateException("Schema is empty?")).getFields().stream()
                        .collect(Collectors.toMap(Schema.Field::name, Function.identity())));
        this.specialFieldsCount = this.specialFieldsSchemaMap.size();
    }

    /**
     * This is useful in tests where we have to create a schema from ImmutableData without taking account of any system fields.
     */
    @VisibleForTesting
    public DataEncoderImpl() {
        this.timeConverter = null;
        this.entityKeySchema = null;
        this.partitionKeySchema = null;
        this.timeKeyRecordSchema = null;
        this.specialFieldsSchemaMap = new HashMap<>();
        this.specialFieldsCount = 0;
        this.includeSystemFields = false;
    }

    @Override
    public Long encodeTimeKeyValues(ImmutableData record) {
        return this.timeConverter.convert(encodeKeyValuesOrThrow(this.timeKeyRecordSchema, record));
    }

    private static GenericRecord encodeKeyValuesOrThrow(Schema schema, ImmutableData data) {
        GenericRecord genericRecord = new GenericData.Record(schema);
        for (Schema.Field field : schema.getFields()) {
            String cmwDataFieldName = field.name(); // I assume that we won't allow illegal characters in the names of
            // the system def keys.
            ImmutableEntry entry = data.getEntry(cmwDataFieldName);
            if (entry != null) {
                genericRecord.put(cmwDataFieldName, createGenericRecordFieldValue(entry, schema, cmwDataFieldName));
            } else {
                throw new IllegalArgumentException(String.format(
                        "Record does not contain the expected field [%s]. Given record is: %n[%s]",
                        cmwDataFieldName, data));
            }
        }

        return genericRecord;
    }

    @Override
    public KeyValues encodeEntityKeyValues(ImmutableData data) {
        return new KeyValues(encodeKeyValuesOrThrow(entityKeySchema, data));
    }

    @Override
    public KeyValues encodePartitionKeyValues(ImmutableData data) {
        return new KeyValues(encodeKeyValuesOrThrow(partitionKeySchema, data));
    }

    /**
     * Creates a Schema from a given CMW Data record.
     */
    @Override
    public String encodeRecordFieldDefinitions(ImmutableData record) {
        FieldAssembler<Schema> fieldAssembler = SchemaBuilder.builder().record(RECORD_NAME_PREFIX + 0)
                .namespace(RECORD_NAMESPACE).fields();
        // add system fields, ignoring is useful for testing encoders, simplifies tests.
        if (includeSystemFields) {
            fieldAssembler.name(SYSTEM_ID.getFieldName()).type(SYSTEM_ID.getSchema()).noDefault();
            fieldAssembler.name(ENTITY_ID.getFieldName()).type(ENTITY_ID.getSchema()).noDefault();
            fieldAssembler.name(PARTITION_ID.getFieldName()).type(PARTITION_ID.getSchema()).noDefault();
            fieldAssembler.name(SCHEMA_ID.getFieldName()).type(SCHEMA_ID.getSchema()).noDefault();
            fieldAssembler.name(TIMESTAMP.getFieldName()).type(TIMESTAMP.getSchema()).noDefault();
        }
        // add fields from Data
        this.addFieldsFromData(fieldAssembler, record, 0);

        return fieldAssembler.endRecord().toString();
    }

    // Adds fields from Data to a FieldAssembler
    // Checks if all the special fields on level 0 are found. The check is very simple and based on the number of fields found.
    // It is based on the assumption that we cannot have twice the same field name in CMW Data.
    private void addFieldsFromData(FieldAssembler<Schema> fieldAssembler, ImmutableData record, int recordLevel) {
        if (recordLevel > 0 && record.size() == 0) {
            throw new IllegalArgumentException("There are no fields in nested record");
        }
        //This is a simple check if all the special fields are found.
        int fieldsCount = 0;
        // sort needed to create the same schema every time for the same record field definitions.
        List<ImmutableEntry> entries = new ArrayList<>(record.getEntries());
        entries.sort(comparing(ImmutableEntry::getName));

        for (ImmutableEntry entry : entries) {
            String fieldName = IllegalCharacterConverter.get().getLegalIfCached(entry.getName());
            if (recordLevel == 0 && (fieldName.equals(ENTITY_ID.getFieldName()) || fieldName
                    .equals(PARTITION_ID.getFieldName()))) {
                //We don't process further ENTITY_ID and PARTITION_ID fields at 0 level if passed from the client in the record.
                //They are already added to the field assembler in the calling method.
                continue;
            }
            Schema fieldSchema;
            Schema.Field field;
            if (recordLevel == 0 && (field = this.specialFieldsSchemaMap.get(fieldName)) != null) {
                // Do not generate anything for special fields at 0 level (main record level).
                // The schema for this special field should be the one defined in the system.
                fieldSchema = field.schema();
                fieldsCount++;
            } else {
                //Generate the field schema based on the Entry definition.
                fieldSchema = this.getSchemaForEntry(entry, recordLevel);
            }

            tryFieldName(fieldAssembler, fieldName, fieldSchema);
        }

        throwIfDataDoesNotContainAllRequiredFields(recordLevel, fieldsCount, record);
    }

    private void tryFieldName(FieldAssembler<Schema> fieldAssembler,
            String fieldName, Schema fieldSchema) {
        try {
            fieldAssembler.name(fieldName).type(fieldSchema).noDefault();
        } catch (SchemaParseException exception) {
            fieldName = IllegalCharacterConverter.get().convertToLegal(fieldName);
            fieldAssembler.name(fieldName).type(fieldSchema).noDefault();
        }
    }

    private void throwIfDataDoesNotContainAllRequiredFields(int recordLevel, int fieldsCount, ImmutableData record) {
        if (recordLevel == 0 && fieldsCount != this.specialFieldsCount) {
            throw new IllegalArgumentException(String.format(
                    "Record does not contain all the following required (system defined) fields: %s. "
                            + "Given record is: %n[%s]", this.specialFieldsSchemaMap.keySet(), record));
        }
    }

    /**
     * Creates the schema for a given @see {@link ImmutableEntry} object with all the fields nullable by default.
     */
    @SuppressWarnings("squid:S1698") //== instead of equals
    private Schema getSchemaForEntry(ImmutableEntry cmwDataEntry, int recordLevel) {
        EntryType<?> type = cmwDataEntry.getType();
        Schema schema;
        if (type == DATA) {
            int nestedRecordLevel = recordLevel + 1;
            FieldAssembler<Schema> fieldAssembler = SchemaBuilder.nullable()
                    .record(RECORD_NAME_PREFIX + nestedRecordLevel).namespace(RECORD_NAMESPACE).fields();
            // this is built dynamically
            this.addFieldsFromData(fieldAssembler, cmwDataEntry.getAs(DATA), nestedRecordLevel);
            schema = fieldAssembler.endRecord();
        } else {
            schema = ImmutableDataToAvroConverter.TYPE_TO_SCHEMA_MAP.get(type);
        }

        if (schema == null) {
            throw new IllegalArgumentException("Unknown DataType: " + type);
        }

        return schema;
    }
}
