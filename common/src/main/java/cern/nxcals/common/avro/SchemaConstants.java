package cern.nxcals.common.avro;

import lombok.experimental.UtilityClass;

@UtilityClass
public final class SchemaConstants {
    public static final String DDF_Y_ARRAY_FIELD_NAME = "yArray";
    public static final String DDF_X_ARRAY_FIELD_NAME = "xArray";
    public static final String ARRAY_DIMENSIONS_FIELD_NAME = "dimensions";
    public static final String ARRAY_ELEMENTS_FIELD_NAME = "elements";
    public static final String RECORD_NAME_PREFIX = "data";
    public static final String RECORD_NAMESPACE = "cern.nxcals";
    public static final String INT_MULTI_ARRAY_SCHEMA_NAME = "int_multi_array";
    public static final String LONG_MULTI_ARRAY_SCHEMA_NAME = "long_multi_array";
    public static final String DOUBLE_MULTI_ARRAY_SCHEMA_NAME = "double_multi_array";
    public static final String FLOAT_MULTI_ARRAY_SCHEMA_NAME = "float_multi_array";
    public static final String STRING_MULTI_ARRAY_SCHEMA_NAME = "string_multi_array";
    public static final String DF_MULTI_ARRAY_SCHEMA_NAME = "df_multi_array";
    public static final String DF_LIST_SCHEMA_NAME = "df_list";
    public static final String DF_LIST_MULTI_ARRAY_SCHEMA_NAME = "df_list_array";
    public static final String BOOLEAN_MULTI_ARRAY_SCHEMA_NAME = "boolean_multi_array";
    public static final String DF_LIST_FIELD_NAME = "discrete_functions";
    public static final String DF_SCHEMA_NAME = "discrete_function";

}