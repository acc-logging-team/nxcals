package cern.nxcals.common.errors;

public class JsonSerializationError extends Error {
    public JsonSerializationError(String message, Throwable cause) {
        super(message, cause);
    }
}
