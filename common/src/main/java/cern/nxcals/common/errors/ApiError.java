package cern.nxcals.common.errors;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;

@Value
@Builder(builderClassName = "Builder")
@JsonDeserialize(builder = ApiError.Builder.class)
@Slf4j
public class ApiError {
    private String code;
    @NonNull
    @lombok.Builder.Default
    private String message = "";
    private String stacktrace;
    @lombok.Builder.Default
    private boolean repeatable = true;

    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (code != null) {
            builder.append("[").append(code).append("] ");
        }
        builder.append(message);
        if (stacktrace != null) {
            builder.append("\nStacktrace: ").append(stacktrace);
        }
        return builder.toString();
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class Builder { }
}
