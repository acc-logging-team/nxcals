package cern.nxcals.common.qbuilders.delegates;

import cern.nxcals.api.metadata.qbuilders.properties.GenericExtendedInstantProperty;
import com.github.rutledgepaulv.qbuilders.builders.QBuilder;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.github.rutledgepaulv.qbuilders.delegates.virtual.InstantLikePropertyDelegate;
import com.github.rutledgepaulv.qbuilders.structures.FieldPath;

import java.time.Duration;
import java.time.Instant;
import java.util.function.Function;

import static java.util.function.UnaryOperator.identity;

@SuppressWarnings("squid:MaximumInheritanceDepth")
public class GenericExtendedInstantPropertyDelegate<T extends QBuilder<T>, C extends Condition<T>>
        extends GenericPropertyDelegate<T, C>
        implements GenericExtendedInstantProperty<T, C> {
    private final InstantLikePropertyDelegate<T, Instant> propertyDelegate;

    public GenericExtendedInstantPropertyDelegate(FieldPath field, T canonical, Function<Condition<T>, C> wrapper) {
        super(field, canonical, wrapper);
        this.propertyDelegate = new InstantLikePropertyDelegate<T, Instant>(
                field, canonical) {
            @Override
            protected Instant normalize(Instant dateTime) {
                return dateTime;
            }
        };
    }

    public C notOlderThan(Duration value) {
        return wrapResult(after(Instant.from(value.subtractFrom(Instant.now())), false));
    }

    public C before(Instant dateTime, boolean exclusive) {
        return wrapResult(propertyDelegate.before(dateTime, exclusive));
    }

    public C after(Instant dateTime, boolean exclusive) {
        return wrapResult(propertyDelegate.after(dateTime, exclusive));
    }

    public C between(Instant after, boolean exclusiveAfter, Instant before, boolean exclusiveBefore) {
        return wrapResult(propertyDelegate.between(after, exclusiveAfter, before, exclusiveBefore));
    }

    public C exists() {
        return wrapResult(propertyDelegate.exists());
    }

    public C doesNotExist() {
        return wrapResult(propertyDelegate.doesNotExist());
    }

    public C eq(Instant value) {
        return wrapResult(propertyDelegate.eq(value));
    }

    public C ne(Instant value) {
        return wrapResult(propertyDelegate.ne(value));
    }

    public static <T extends QBuilder<T>> GenericExtendedInstantPropertyDelegate<T, Condition<T>> of(FieldPath field,
            T canonical) {
        return new GenericExtendedInstantPropertyDelegate<>(field, canonical, identity());
    }

}
