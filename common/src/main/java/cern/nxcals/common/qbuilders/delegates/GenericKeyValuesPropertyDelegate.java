package cern.nxcals.common.qbuilders.delegates;

import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.metadata.qbuilders.properties.GenericKeyValuesProperty;
import com.github.rutledgepaulv.qbuilders.builders.QBuilder;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.github.rutledgepaulv.qbuilders.structures.FieldPath;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static cern.nxcals.common.utils.KeyValuesUtils.convertMapIntoAvroSchemaString;
import static java.util.function.UnaryOperator.identity;

/**
 * Custom operator accepting Map of keyValues. We convert the map to avro string using the given SystemSpec.
 *
 * @param <T>
 */
@SuppressWarnings("squid:MaximumInheritanceDepth")
public class GenericKeyValuesPropertyDelegate<T extends QBuilder<T>, C extends Condition<T>>
        extends GenericStringLikePropertyDelegate<T, C>
        implements GenericKeyValuesProperty<T, C> {
    private final Function<SystemSpec, String> systemToKeyValuesSchemaMapper;

    public GenericKeyValuesPropertyDelegate(Function<SystemSpec, String> systemToKeyValues, FieldPath field,
            T canonical, Function<Condition<T>, C> conditionWrapper) {
        super(field, canonical, conditionWrapper);
        this.systemToKeyValuesSchemaMapper = systemToKeyValues;
    }

    @Override
    public final C eq(SystemSpec system, Map<String, Object> value) {
        return eq(asString(value, system));
    }

    @Override
    public final C ne(SystemSpec system, Map<String, Object> value) {
        return ne(asString(value, system));
    }

    @SafeVarargs
    @Override
    public final C in(SystemSpec system, Map<String, Object>... value) {
        return in(Arrays.stream(value).map(v -> asString(v, system)).collect(Collectors.toList()));
    }

    @SafeVarargs
    @Override
    public final C nin(SystemSpec system, Map<String, Object>... value) {
        return nin(Arrays.stream(value).map(v -> asString(v, system)).collect(Collectors.toList()));
    }

    @Override
    public final C like(SystemSpec system, Map<String, Object> value) {
        return wrapResult(like(asString(value, system)));
    }

    @Override
    public final C notLike(SystemSpec system, Map<String, Object> value) {
        return wrapResult(notLike(asString(value, system)));
    }

    private String asString(Map<String, Object> value, SystemSpec system) {
        return convertMapIntoAvroSchemaString(value, systemToKeyValuesSchemaMapper.apply(system));
    }

    public static <T extends QBuilder<T>> GenericKeyValuesPropertyDelegate<T, Condition<T>> of(
            Function<SystemSpec, String> systemToKeyValues, FieldPath field, T canonical) {
        return new GenericKeyValuesPropertyDelegate<>(systemToKeyValues, field, canonical, identity());
    }
}
