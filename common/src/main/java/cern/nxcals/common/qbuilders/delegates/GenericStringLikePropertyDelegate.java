package cern.nxcals.common.qbuilders.delegates;

import cern.nxcals.api.metadata.qbuilders.properties.GenericStringLikeCaseInsensitiveProperty;
import cern.nxcals.api.metadata.qbuilders.properties.GenericStringLikeProperty;
import cern.nxcals.common.qbuilders.CustomOperatorFactory;
import com.github.rutledgepaulv.qbuilders.builders.QBuilder;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.github.rutledgepaulv.qbuilders.properties.concrete.StringProperty;
import com.github.rutledgepaulv.qbuilders.structures.FieldPath;

import java.util.Collection;
import java.util.Collections;
import java.util.function.Function;

import static java.util.Arrays.asList;
import static java.util.function.UnaryOperator.identity;

/**
 * This is a proper implementation of the like/notLike eq/ne operators that uses separate operators for them.
 * This way we are not using like for eq operator and we don't have to escape values.
 *
 * @param <T>
 */
public class GenericStringLikePropertyDelegate<T extends QBuilder<T>, C extends Condition<T>>
        extends GenericPropertyDelegate<T, C>
        implements GenericStringLikeProperty<T, C> {
    private final StringProperty<T> stringProperty;
    private final boolean caseSensitive;

    public GenericStringLikePropertyDelegate(FieldPath field, T canonical, Function<Condition<T>, C> conditionWrapper) {
        this(field, canonical, true, conditionWrapper);
    }

    GenericStringLikePropertyDelegate(FieldPath field, T canonical, boolean caseSensitive,
            Function<Condition<T>, C> conditionWrapper) {
        super(field, canonical, conditionWrapper);
        this.caseSensitive = caseSensitive;
        this.stringProperty = string(field.toString());
    }

    @Override
    public GenericStringLikeCaseInsensitiveProperty<T, C> caseInsensitive() {
        return new GenericStringLikePropertyDelegate<>(getField(), self(), false, getWrapper());
    }

    @Override
    public C eq(String value) {
        return wrapResult(condition(getField(), CustomOperatorFactory.stringEquals(caseSensitive),
                Collections.singletonList(value)));
    }

    @Override
    public C ne(String value) {
        return wrapResult(condition(getField(), CustomOperatorFactory.stringNotEquals(caseSensitive),
                Collections.singletonList(value)));
    }

    @Override
    public C like(String value) {
        return wrapResult(condition(getField(), CustomOperatorFactory.stringLike(caseSensitive),
                Collections.singletonList(value)));
    }

    @Override
    public C notLike(String value) {
        return wrapResult(condition(getField(), CustomOperatorFactory.stringNotLike(caseSensitive),
                Collections.singletonList(value)));
    }

    @Override
    public C lexicallyAfter(String value) {
        return wrapResult(stringProperty.lexicallyAfter(value));
    }

    @Override
    public C lexicallyBefore(String value) {
        return wrapResult(stringProperty.lexicallyBefore(value));
    }

    @Override
    public C lexicallyNotAfter(String value) {
        return wrapResult(stringProperty.lexicallyNotAfter(value));
    }

    @Override
    public C lexicallyNotBefore(String value) {
        return wrapResult(stringProperty.lexicallyNotBefore(value));
    }

    @Override
    public C pattern(String pattern) {
        return wrapResult(stringProperty.pattern(pattern));
    }

    @Override
    public C exists() {
        return wrapResult(stringProperty.exists());
    }

    @Override
    public C doesNotExist() {
        return wrapResult(stringProperty.doesNotExist());
    }

    @Override
    public C in(String... values) {
        return in(asList(values));
    }

    @Override
    public C in(Collection<String> values) {
        return wrapResult(condition(getField(), CustomOperatorFactory.stringIn(caseSensitive), values));
    }

    @Override
    public C nin(String... values) {
        return nin(asList(values));
    }

    @Override
    public C nin(Collection<String> values) {
        return wrapResult(condition(getField(), CustomOperatorFactory.stringNotIn(caseSensitive), values));
    }

    public static <T extends QBuilder<T>> GenericStringLikePropertyDelegate<T, Condition<T>> of(FieldPath field,
            T canonical) {
        return new GenericStringLikePropertyDelegate<>(field, canonical, identity());
    }
}
