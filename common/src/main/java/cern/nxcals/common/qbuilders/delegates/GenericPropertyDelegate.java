package cern.nxcals.common.qbuilders.delegates;

import com.github.rutledgepaulv.qbuilders.builders.QBuilder;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.github.rutledgepaulv.qbuilders.delegates.virtual.PropertyDelegate;
import com.github.rutledgepaulv.qbuilders.structures.FieldPath;
import lombok.AccessLevel;
import lombok.Getter;

import java.util.function.Function;

public abstract class GenericPropertyDelegate<T extends QBuilder<T>, C extends Condition<T>>
        extends PropertyDelegate<T> {
    @Getter(AccessLevel.PACKAGE)
    private final Function<Condition<T>, C> wrapper;

    protected GenericPropertyDelegate(FieldPath field, T canonical, Function<Condition<T>, C> wrapper) {
        super(field, canonical);
        this.wrapper = wrapper;
    }

    protected C wrapResult(Condition<T> condition) {
        return wrapper.apply(condition);
    }
}
