package cern.nxcals.common.qbuilders.delegates;

import cern.nxcals.api.metadata.qbuilders.properties.GenericEnumProperty;
import com.github.rutledgepaulv.qbuilders.builders.QBuilder;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.github.rutledgepaulv.qbuilders.delegates.concrete.EnumPropertyDelegate;
import com.github.rutledgepaulv.qbuilders.structures.FieldPath;

import java.util.Collection;
import java.util.function.Function;

public class GenericEnumPropertyDelegate<T extends QBuilder<T>, S extends Enum<S>, C extends Condition<T>>
        extends GenericPropertyDelegate<T, C>
        implements GenericEnumProperty<T, S, C> {
    private final EnumPropertyDelegate<T, S> enumProperty;

    public GenericEnumPropertyDelegate(FieldPath field, T canonical, Function<Condition<T>, C> wrapper) {
        super(field, canonical, wrapper);
        this.enumProperty = new EnumPropertyDelegate<>(field, canonical);
    }

    @Override
    public C in(S... values) {
        return wrapResult(enumProperty.in(values));
    }

    @Override
    public C in(Collection<S> values) {
        return wrapResult(enumProperty.in(values));
    }

    @Override
    public C nin(S... values) {
        return wrapResult(enumProperty.nin(values));
    }

    @Override
    public C nin(Collection<S> values) {
        return wrapResult(enumProperty.nin(values));
    }

    @Override
    public C eq(S value) {
        return wrapResult(enumProperty.eq(value));
    }

    @Override
    public C ne(S value) {
        return wrapResult(enumProperty.ne(value));
    }

    @Override
    public C exists() {
        return wrapResult(enumProperty.exists());
    }

    @Override
    public C doesNotExist() {
        return wrapResult(enumProperty.doesNotExist());
    }
}
