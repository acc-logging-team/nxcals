package cern.nxcals.common.grpc;

import io.grpc.CallOptions;
import io.grpc.Channel;
import io.grpc.ClientCall;
import io.grpc.ClientInterceptor;
import io.grpc.ForwardingClientCall;
import io.grpc.Metadata;
import io.grpc.MethodDescriptor;
import lombok.AllArgsConstructor;
import lombok.Builder;

import java.util.function.Supplier;

import static io.grpc.Metadata.BINARY_BYTE_MARSHALLER;
import static io.grpc.Metadata.BINARY_HEADER_SUFFIX;

@AllArgsConstructor
@Builder
public class RbacTokenInterceptor implements ClientInterceptor {
    public static final Metadata.Key<byte[]> TOKEN_KEY = Metadata.Key
            .of("nxcals_rbac_token" + BINARY_HEADER_SUFFIX, BINARY_BYTE_MARSHALLER);
    private final Supplier<byte[]> token;

    @Override
    public <R, S> ClientCall<R, S> interceptCall(MethodDescriptor<R, S> method,
            CallOptions callOptions, Channel next) {
        return new ForwardingClientCall.SimpleForwardingClientCall<R, S>(next.newCall(method, callOptions)) {
            @Override
            public void start(Listener<S> responseListener, Metadata headers) {
                headers.put(TOKEN_KEY, token.get());
                super.start(responseListener, headers);
            }
        };
    }

}
