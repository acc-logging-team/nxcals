package cern.nxcals.common.grpc;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableSet;
import io.grpc.Attributes;
import io.grpc.EquivalentAddressGroup;
import io.grpc.NameResolver;
import io.grpc.NameResolverProvider;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import java.net.InetSocketAddress;
import java.net.URI;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import static com.google.common.base.Preconditions.checkArgument;
import static io.grpc.Attributes.EMPTY;
import static io.grpc.EquivalentAddressGroup.ATTR_AUTHORITY_OVERRIDE;
import static java.util.stream.Collectors.toList;

public class MultiAddressNameResolverFactory extends NameResolverProvider {
    public static final String SCHEME = "multi-address";

    public static class MultiAddressNameResolver extends NameResolver {
        @VisibleForTesting
        static final String HOST_PORT_SEPARATOR = ":";
        @VisibleForTesting
        static final Set<String> MACHINE_DELIMS = ImmutableSet.of(",", ";");
        private static final Pattern MACHINE_DELIM_PATTERN = Pattern.compile(
                "[" + String.join("", MACHINE_DELIMS) + "]");

        @Getter
        private final List<EquivalentAddressGroup> addresses;

        MultiAddressNameResolver(URI uri) {
            Preconditions.checkArgument(StringUtils.isNotEmpty(uri.getAuthority()), "Address must not be empty/null");
            addresses = parseAddress(uri.getAuthority()).stream()
                    .map(ad -> new EquivalentAddressGroup(ad,
                            Attributes.newBuilder().set(ATTR_AUTHORITY_OVERRIDE, ad.getHostName()).build()))
                    .collect(toList());
        }

        private List<InetSocketAddress> parseAddress(String address) {
            return Splitter.on(MACHINE_DELIM_PATTERN).splitToList(address).stream().map(host -> {
                String[] strings = host.split(HOST_PORT_SEPARATOR);
                checkArgument(strings.length == 2, "URI [" + host + "] should have both address and port");
                return new InetSocketAddress(strings[0], Integer.parseInt(strings[1]));
            }).collect(toList());
        }

        @Override
        public String getServiceAuthority() {
            return "to_be_ignored";
        }

        @Override
        public void start(Listener2 listener) {
            listener.onResult(ResolutionResult.newBuilder().setAddresses(addresses).setAttributes(EMPTY).build());
        }

        public void shutdown() {
            //nothing to do
        }
    }

    @Override
    public MultiAddressNameResolver newNameResolver(URI uri, NameResolver.Args args) {
        return new MultiAddressNameResolver(uri);
    }

    @Override
    public String getDefaultScheme() {
        return SCHEME;
    }

    @Override
    protected boolean isAvailable() {
        return true;
    }

    @Override
    protected int priority() {
        return 5;
    }
}
