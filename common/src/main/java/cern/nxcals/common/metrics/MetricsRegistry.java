package cern.nxcals.common.metrics;

/**
 * Defines behaviour for metrics registry that is capable of registering and managing the
 * values of defined metric instances without exposing the real vendor that handles the process
 */
public interface MetricsRegistry {

    /**
     * Registers (if not present) and updates a gauge metric as referenced by the provided name
     *
     * @param metricName a {@link String} representation of the metric name
     * @param value      the value to be exported for the metric
     */
    void updateGaugeTo(String metricName, double value);

    /**
     * Registers (if not present) and updates a gauge metric as referenced by the provided name
     *
     * @param metricName a {@link String} representation of the metric name
     * @param value      the value to be exported for the metric
     */
    void updateGaugeTo(String metricName, long value);

    /**
     * Registers (if not present) and increments the metric counter by the provided amount.
     * This method is also useful when the target action is just to initialize the counter
     * at a specific value (ex. zero)
     *
     * @param metricName        a {@link String} representation of the metric name
     * @param amountToIncrement amount to add to the counter
     */
    void incrementCounterBy(String metricName, double amountToIncrement);

}
