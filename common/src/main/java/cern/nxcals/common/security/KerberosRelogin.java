/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.common.security;

import com.google.common.base.Preconditions;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.security.UserGroupInformation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Paths;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * A class that updates Kerberos token.
 *
 * @author jwozniak, ntsvetko
 */
/**
 * @deprecated this class has been moved to cern.nxcals.api.security package
 */
@Deprecated
public class KerberosRelogin {
    private static final Logger LOGGER = LoggerFactory.getLogger(KerberosRelogin.class);
    private final String principal;
    private final String keytab;
    private boolean enableRelogin;

    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1, runnable -> {
        Thread thread = new Thread(runnable);
        thread.setDaemon(true);
        return thread;
    });

    public KerberosRelogin(String principal, String keytab, boolean enableRelogin) {
        this.principal = principal;
        this.keytab = verifyKeytab(keytab);
        this.enableRelogin = enableRelogin;
    }

    private String verifyKeytab(String keytab) {
        Objects.requireNonNull(keytab, "Keytab file is null");
        Preconditions.checkArgument(fileExists(keytab), "Keytab file not found: %s", keytab);
        return keytab;
    }

    private boolean fileExists(String keytab) {
        return Paths.get(keytab).toFile().exists();
    }

    public boolean getEnableRelogin() {
        return enableRelogin;
    }

    public String getKeytab() {
        return keytab;
    }

    public String getPrincipal() {
        return principal;
    }

    /**
     * Start this scheduler daemon.
     */
    public void start() {
        if (this.enableRelogin) {
            LOGGER.info(
                    "Starting the Kerberos re-login daemon with parameters (initialDelay: 1, period: 10, unit: MINUTES)");
            loginFromKeytab();
            this.scheduler.scheduleAtFixedRate(this::relogin, 1, 10, TimeUnit.MINUTES);
        } else {
            LOGGER.info("Not starting the Kerberos re-login daemon, just login once");
            loginFromKeytab();
        }
    }

    /**
     * Login the user from a given keytab file.
     */
    private void loginFromKeytab() {
        try {
            if (!UserGroupInformation.isSecurityEnabled()) {
                Configuration conf = new Configuration(false);
                conf.set("hadoop.security.authentication", "kerberos");
                UserGroupInformation.setConfiguration(conf);
            }
            UserGroupInformation.loginUserFromKeytab(principal, keytab);
            UserGroupInformation loginUser = UserGroupInformation.getLoginUser();

            LOGGER.info("Logged in from keytab to hadoop as {}", loginUser);
        } catch (Exception e) {
            LOGGER.error("Error while relogging user from keytab ", e);
        }
    }

    private void relogin() {
        try {
            UserGroupInformation loginUser = UserGroupInformation.getLoginUser();

            LOGGER.info("Re-Login attempted from keytab to hadoop as {}", loginUser);
            loginUser.checkTGTAndReloginFromKeytab();
        } catch (Throwable e) {
            LOGGER.error("Error while relogging user from keytab ", e);
        }
    }
}
