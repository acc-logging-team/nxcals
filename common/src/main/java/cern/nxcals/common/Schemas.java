/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.common;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;

/**
 * @author Marcin Sobieszek
 * @date Jul 14, 2016 11:53:10 AM
 */
@Getter
@AllArgsConstructor
public enum Schemas {

    /**
     * @formatter:off
     */
    SYSTEM_ID(SchemaBuilder.builder().longType(), SystemFields.NXC_SYSTEM_ID.getValue()),
    ENTITY_ID(SchemaBuilder.builder().longType(), SystemFields.NXC_ENTITY_ID.getValue()),
    PARTITION_ID(SchemaBuilder.builder().longType(), SystemFields.NXC_PARTITION_ID.getValue()),
    SCHEMA_ID(SchemaBuilder.builder().longType(), SystemFields.NXC_SCHEMA_ID.getValue()),
    TIMESTAMP(SchemaBuilder.builder().longType(), SystemFields.NXC_TIMESTAMP.getValue());

    /**
     * @formatter:on
     */

    private final Schema schema;
    private final String fieldName;
}
