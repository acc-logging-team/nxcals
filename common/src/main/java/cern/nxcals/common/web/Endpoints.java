/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.common.web;

import lombok.experimental.UtilityClass;

/**
 * This class is to provide constants that corresponds to service endpoints.
 */
@UtilityClass
public final class Endpoints {
    public static final String ENTITIES = "/entities";

    public static final String ENTITY_EXTEND_FIRST_HISTORY = ENTITIES + "extendFirstHistoryData";

    public static final String ENTITY_UPDATE = ENTITIES + "/update";

    private static final String COMPACTION = "/compaction";

    public static final String COMPACTION_JOBS = COMPACTION + "/jobs";

    public static final String RESOURCES = "/resources";
    public static final String EXTRACTION = "/extraction";

    public static final String GROUPS = "/groups";

    public static final String PARTITIONS = "/partitions";

    public static final String PARTITION_RESOURCES = "/partitionResources";

    public static final String PARTITION_RESOURCE_HISTORIES = "/partitionResourceHistories";

    private static final String SCHEMAS = "/schemas";

    public static final String SYSTEMS = "/systems";

    public static final String VARIABLES = "/variables";
    public static final String IDS_SUFFIX = "-ids";

    public static final String HIERARCHIES = "/hierarchies";

    public static final String HIERARCHY_VARIABLES = "/hierarchyVariables";

    public static final String FIND_ALL = "/findAll";

    public static final String FIND_ALL_WITH_HISTORY = "/findAllWithHistory";
    public static final String FIND_ALL_WITH_OPTIONS = "/findAllWithOptions";

    public static final String FIND_ALL_CHANGELOGS = "/findAllChangelogs";
    public static final String FIND_ALL_CONFIG_CHANGELOGS = "/findAllConfigChangelogs";
    public static final String BATCH = "/batch";

    public static final String AUTHORIZATION = "/authorization";
    public static final String AUTHORIZATION_TOKENS = AUTHORIZATION + "/tokens";

    public static final String VARIABLES_FIND_ALL = VARIABLES + FIND_ALL;
    public static final String VARIABLES_FIND_ALL_WITH_OPTIONS = VARIABLES + FIND_ALL_WITH_OPTIONS;
    public static final String VARIABLE_CHANGELOGS_FIND_ALL = VARIABLES + FIND_ALL_CHANGELOGS;
    public static final String VARIABLE_CONFIG_CHANGELOGS_FIND_ALL = VARIABLES + FIND_ALL_CONFIG_CHANGELOGS;
    public static final String VARIABLE_FIND_SCHEMAS = VARIABLES + SCHEMAS;
    public static final String SYSTEMS_FIND_ALL = SYSTEMS + FIND_ALL;
    public static final String SCHEMAS_FIND_ALL = SCHEMAS + FIND_ALL;
    public static final String PARTITIONS_FIND_ALL = PARTITIONS + FIND_ALL;
    public static final String HIERARCHIES_FIND_ALL = HIERARCHIES + FIND_ALL;
    public static final String HIERARCHIES_CHANGELOGS_FIND_ALL = HIERARCHIES + FIND_ALL_CHANGELOGS;
    public static final String HIERARCHY_VARIABLES_CHANGELOGS_FIND_ALL = HIERARCHY_VARIABLES + FIND_ALL_CHANGELOGS;
    public static final String ENTITIES_FIND_ALL = ENTITIES + FIND_ALL;
    public static final String GROUPS_FIND_ALL = GROUPS + FIND_ALL;
    public static final String PARTITION_RESOURCES_FIND_ALL = PARTITION_RESOURCES + FIND_ALL;
    public static final String PARTITION_RESOURCE_HISTORIES_FIND_ALL = PARTITION_RESOURCE_HISTORIES + FIND_ALL;
    public static final String ENTITIES_FIND_ALL_WITH_HISTORY = ENTITIES + FIND_ALL_WITH_HISTORY;
    public static final String ENTITIES_FIND_ALL_WITH_OPTIONS = ENTITIES + FIND_ALL_WITH_OPTIONS;
    public static final String ENTITY_CHANGELOGS_FIND_ALL = ENTITIES + FIND_ALL_CHANGELOGS;

    public static final String COMPACT_JOB_PARAMETER = "COMPACT";
    public static final String RESTAGE_JOB_PARAMETER = "RESTAGE";
    public static final String MERGE_COMPACT_JOB_PARAMETER = "MERGE_COMPACT";
}
