/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.common.web;

import lombok.experimental.UtilityClass;

/**
 * Constants with the HTTP verbs.
 */
@UtilityClass
public final class HttpVerbs {
    public static final String GET = "GET ";
    public static final String POST = "POST ";
    public static final String PUT = "PUT ";
    public static final String PATCH = "PATCH ";
    public static final String DELETE = "DELETE ";
    public static final String HEAD = "HEAD ";
    public static final String CONNECT = "CONNECT ";
    public static final String OPTIONS = "OPTIONS ";
    public static final String TRACE = "TRACE ";
}
