package cern.nxcals.api.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.Instant;

@Data
@JsonDeserialize(builder = HierarchyVariablesChangelog.InnerBuilder.class)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class HierarchyVariablesChangelog implements Identifiable {

    private final long id;
    private Long oldVariableId;
    private Long newVariableId;
    private Long oldHierarchyId;
    private Long newHierarchyId;
    private Long oldSystemId;
    private Long newSystemId;
    private Instant createTimeUtc;
    private OperationType opType;
    private String clientInfo;


    private static HierarchyVariablesChangelog.InnerBuilder innerBuilder() {
        return new HierarchyVariablesChangelog.InnerBuilder();
    }

    @lombok.Builder(builderMethodName = "innerBuilder", builderClassName = "InnerBuilder")
    private static HierarchyVariablesChangelog innerBuilder(long id, Long oldVariableId,
            Long newVariableId, Long oldHierarchyId, Long newHierarchyId, Long oldSystemId, Long newSystemId,
          Instant createTimeUtc, OperationType opType, String clientInfo) {
        return new HierarchyVariablesChangelog(id, oldVariableId, newVariableId, oldHierarchyId, newHierarchyId, oldSystemId, newSystemId,
                createTimeUtc, opType, clientInfo);
    }

    @lombok.Builder(builderMethodName = "builder", builderClassName = "Builder")
    private static HierarchyVariablesChangelog userBuilder(Long oldVariableId,
            Long newVariableId, Long oldHierarchyId, Long newHierarchyId,  Long oldSystemId, Long newSystemId,
            Instant createTimeUtc, OperationType opType, String clientInfo){
        return new HierarchyVariablesChangelog(Identifiable.NOT_SET, oldVariableId, newVariableId, oldHierarchyId,
                newHierarchyId, oldSystemId, newSystemId, createTimeUtc, opType, clientInfo);
    }

    public HierarchyVariablesChangelog.Builder toBuilder() {
        return HierarchyVariablesChangelog.innerBuilder()
                .id(id)
                .oldVariableId(oldVariableId)
                .newVariableId(newVariableId)
                .oldHierarchyId(oldHierarchyId)
                .newHierarchyId(newHierarchyId)
                .oldSystemId(oldSystemId)
                .newSystemId(newSystemId)
                .createTimeUtc(createTimeUtc)
                .opType(opType)
                .clientInfo(clientInfo);
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class InnerBuilder extends HierarchyVariablesChangelog.Builder {
    }
}
