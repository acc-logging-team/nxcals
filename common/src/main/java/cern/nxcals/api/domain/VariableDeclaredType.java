package cern.nxcals.api.domain;

/**
 * This enum represents a declared type on a variable which is desired by the user.
 * It is used for searching for variables that have that type set.
 * It is not used for any validation if the actual configuration on the variable (like mapping to entity, fields or schema types) match the declared type.
 * We can consider it more like an additional naming label (together with Unit and Description).
 * It was introduced to allow more backwards compatibility with the old CALS API.
 */
public enum VariableDeclaredType {
    NUMERIC,
    NUMERIC_STATUS,
    TEXT,
    VECTOR_NUMERIC,
    FUNDAMENTAL,
    VECTOR_STRING,
    MATRIX_NUMERIC
}
