package cern.nxcals.api.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@JsonDeserialize(builder = PartitionResourceHistory.InnerBuilder.class)
@SuppressWarnings("squid:S00107") // More than 7 parameters in method signature
public class PartitionResourceHistory implements Identifiable, Versionable {
    private final long id;
    @NonNull
    private final PartitionResource partitionResource;
    @NonNull
    private final String partitionInformation;

    //Those 3 are not really used now, it is for later.
    @NonNull
    private final String compactionType;
    @NonNull
    private final String storageType;
    private final boolean isFixedSettings;
    @NonNull
    private final TimeWindow validity;
    private final long recVersion;


    private static PartitionResourceHistory.InnerBuilder innerBuilder() {
        return new PartitionResourceHistory.InnerBuilder();
    }

    @lombok.Builder(builderMethodName = "innerBuilder", builderClassName = "InnerBuilder")
    private static PartitionResourceHistory innerBuilder(long id,
                                                         PartitionResource partitionResource,
                                                         String partitionInformation,
                                                         String compactionType,
                                                         String storageType,
                                                         boolean isFixedSettings,
                                                         TimeWindow validity,
                                                         long recVersion) {
        checkValidity(validity);
        return new PartitionResourceHistory(id, partitionResource, partitionInformation, compactionType,
                storageType, isFixedSettings, validity, recVersion);
    }

    @lombok.Builder(builderClassName = "Builder")
    private static PartitionResourceHistory userBuilder(PartitionResource partitionResource,
                                                        String partitionInformation,
                                                        String compactionType,
                                                        String storageType,
                                                        boolean isFixedSettings,
                                                        TimeWindow validity) {
        checkValidity(validity);
        return new PartitionResourceHistory(Identifiable.NOT_SET, partitionResource, partitionInformation,
                compactionType, storageType, isFixedSettings, validity, Versionable.NOT_SET);
    }

    private static void checkValidity(TimeWindow validity) {
        if (validity.getStartTime() == null || validity.getEndTime() == null) {
            throw new NullPointerException("Both start time & end time must be set for PartitionResourceHistory");
        }
    }

    public Builder toBuilder() {
        return PartitionResourceHistory.innerBuilder()
                .id(id)
                .partitionResource(partitionResource)
                .partitionInformation(partitionInformation)
                .compactionType(compactionType)
                .storageType(storageType)
                .isFixedSettings(isFixedSettings)
                .validity(validity)
                .recVersion(recVersion);
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class InnerBuilder extends Builder {
    }

    public static Instant to(Instant from) {
        return from.plus(1, ChronoUnit.DAYS);
    }
}
