package cern.nxcals.api.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

import java.util.Comparator;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@JsonDeserialize(builder = VariableConfig.InnerBuilder.class)
public class VariableConfig implements Comparable<VariableConfig>, Identifiable, Versionable {
    private final long id;

    @EqualsAndHashCode.Include
    private final long entityId;
    @EqualsAndHashCode.Include
    private final String fieldName;
    private final String variableName;
    @NonNull
    @EqualsAndHashCode.Include
    private final TimeWindow validity;
    private final long recVersion;

    @EqualsAndHashCode.Include
    private final String predicate;

    private static InnerBuilder innerBuilder() {
        return new InnerBuilder();
    }

    @lombok.Builder(builderMethodName = "innerBuilder", builderClassName = "InnerBuilder")
    private static VariableConfig innerBuilder(long id, long entityId, String fieldName, String variableName,
            TimeWindow validity, long recVersion, String predicate) {
        return new VariableConfig(id, entityId, fieldName, variableName, validity, recVersion, predicate);
    }

    @lombok.Builder(builderMethodName = "builder", builderClassName = "Builder")
    private static VariableConfig userBuilder(long entityId, String fieldName, String variableName, TimeWindow validity,
            String predicate) {
        return new VariableConfig(Identifiable.NOT_SET, entityId, fieldName, variableName,
                validity == null ? TimeWindow.infinite() : validity, Versionable.NOT_SET, predicate);
    }

    @Override
    public int compareTo(VariableConfig o) {
        return Comparator.<Long>reverseOrder().compare(validity.getStartTimeNanos(), o.validity.getStartTimeNanos());
    }

    public Builder toBuilder() {
        return VariableConfig.innerBuilder()
                .id(id)
                .entityId(entityId)
                .fieldName(fieldName)
                .variableName(variableName)
                .validity(validity)
                .recVersion(recVersion)
                .predicate(predicate);
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class InnerBuilder extends Builder {
    }

    public boolean isFieldBound() {
        return fieldName != null;
    }
}
