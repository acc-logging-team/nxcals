package cern.nxcals.api.domain;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@SuppressWarnings("all") // soon to be fixed
public interface Identifiable {
    public static <T extends Identifiable> Set<Long> obtainIdsFrom(Collection<T> identifiables) {
        return identifiables.stream().mapToLong(Identifiable::getId).boxed().collect(Collectors.toSet());
    }

    long NOT_SET = Long.MIN_VALUE;

    long getId();

    default boolean hasId() {
        return getId() != NOT_SET;
    }
}
