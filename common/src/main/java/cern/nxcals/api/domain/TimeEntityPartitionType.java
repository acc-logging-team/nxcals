package cern.nxcals.api.domain;

import cern.nxcals.common.utils.KeyValuesUtils;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import lombok.Builder;
import lombok.Data;
import lombok.extern.jackson.Jacksonized;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;

/**
 * Specific implementation of the partitioning using time partitions and entity buckets.
 */
@Data
@Builder(builderClassName = "Builder")
@Jacksonized
@Slf4j
public class TimeEntityPartitionType {
    private static final String PARTITION_INFO_SCHEMA =
            "{\"type\":\"record\",\"name\":\"PartitionInfo\",\"namespace\":\"cern.nxcals\",\"fields\":[{\"name\":\"timePartitions\",\"type\":\"string\"},{\"name\":\"entityBuckets\",\"type\":\"int\"}]}";
    private final int timePartitionCount;
    private final int entityBucketCount;

    //It would be great to come up with an algo that calculates those values on the fly (instead of hardcoding).
    //The time partitions should be as such that an hour is dividable by them.
    private static final List<TimeEntityPartitionType> ENTITY_PARTITION_TYPES = Lists.newArrayList(
            TimeEntityPartitionType.of(1, 1), //small
            TimeEntityPartitionType.of(2, 2), //4 1GB - 4GB
            TimeEntityPartitionType.of(3, 2), //6 4GB - 6GB
            TimeEntityPartitionType.of(4, 2), //8
            TimeEntityPartitionType.of(4, 3), //12
            TimeEntityPartitionType.of(4, 4), //16
            TimeEntityPartitionType.of(6, 3), //18
            TimeEntityPartitionType.of(8, 3), //24
            TimeEntityPartitionType.of(8, 4), //32
            TimeEntityPartitionType.of(12, 3),//36
            TimeEntityPartitionType.of(12, 4), //48
            TimeEntityPartitionType.of(12, 5), //60
            TimeEntityPartitionType.of(12, 6), //72
            TimeEntityPartitionType.of(12, 7), //84
            TimeEntityPartitionType.of(24, 4), //96
            TimeEntityPartitionType.of(24, 5), //120
            TimeEntityPartitionType.of(24, 6), //144
            TimeEntityPartitionType.of(24, 7), //168
            TimeEntityPartitionType.of(24, 8), //192
            TimeEntityPartitionType.of(24, 9), //216
            TimeEntityPartitionType.of(24, 10), //240
            TimeEntityPartitionType.of(24, 12), //288
            TimeEntityPartitionType.of(48, 7), //336
            TimeEntityPartitionType.of(48, 8), //384 - max we currently have...
            TimeEntityPartitionType.of(48, 9), //432
            TimeEntityPartitionType.of(48, 10), //480
            TimeEntityPartitionType.of(48, 12), //576
            TimeEntityPartitionType.of(48, 14), //672
            TimeEntityPartitionType.of(48, 16), //768
            TimeEntityPartitionType.of(48, 18), //864
            TimeEntityPartitionType.of(96, 10), //960
            TimeEntityPartitionType.of(96, 12), //1152
            TimeEntityPartitionType.of(96, 14) //1344 GB and bigger...
    );

    public static TimeEntityPartitionType ofGbFiles(long targetNbOf1GBFiles) {

        if (targetNbOf1GBFiles < 0) {
            throw new IllegalArgumentException("targetNbOf1GBFiles < 0");
        }

        for (TimeEntityPartitionType timeEntityPartitionType : ENTITY_PARTITION_TYPES) {
            //return the first bigger than number of 1GB files.
            if (timeEntityPartitionType.slots() >= targetNbOf1GBFiles) {
                return timeEntityPartitionType;
            }
        }
        log.error("Data size bigger than the algorithm currently supports, returning the biggest element for now");
        //if not return the biggest one
        return ENTITY_PARTITION_TYPES.get(ENTITY_PARTITION_TYPES.size() - 1);
    }

    public static TimeEntityPartitionType next(TimeEntityPartitionType input, int increment) {
        int index = ENTITY_PARTITION_TYPES.indexOf(input);
        if(index < 0) {
            throw new IllegalArgumentException("Cannot find TimeEntityPartitionType in indexed values: " + input);
        }
        int target = Math.min(Math.max(0,index + increment), ENTITY_PARTITION_TYPES.size()-1);

        return ENTITY_PARTITION_TYPES.get(target);
    }

    public static TimeEntityPartitionType of(int timePartitions, int entityBuckets) {
        return new TimeEntityPartitionType(timePartitions, entityBuckets);
    }

    public static TimeEntityPartitionType from(String partInfoAsString) {
        Map<String, Object> stringObjectMap = KeyValuesUtils.convertKeyValuesStringIntoMap(partInfoAsString);
        return TimeEntityPartitionType
                .of((int) stringObjectMap.get("timePartitions"), (int) stringObjectMap.get("entityBuckets"));
    }

    public int slots() {
        return timePartitionCount * entityBucketCount;
    }

    @Override
    public String toString() {
        return KeyValuesUtils.convertMapIntoAvroSchemaString(
                ImmutableMap.of("timePartitions", timePartitionCount, "entityBuckets", entityBucketCount),
                PARTITION_INFO_SCHEMA);
    }

    public static double changePercentage(TimeEntityPartitionType oldValue, TimeEntityPartitionType newValue) {
        return Math.abs(100 * ((newValue.slots() - oldValue.slots()) / (double) oldValue.slots()));
    }
}
