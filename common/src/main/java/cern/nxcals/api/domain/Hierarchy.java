package cern.nxcals.api.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.Singular;

import java.util.Collections;
import java.util.Set;
import java.util.regex.Pattern;

import static cern.nxcals.common.Constants.HIERARCHY_SEPARATOR;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@JsonDeserialize(builder = Hierarchy.InnerBuilder.class)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Hierarchy implements Identifiable, HierarchyView {
    private static final String PATTERN_STRING = "[^\\s\\/][^\\/]*";
    private static final Pattern PATTERN = Pattern.compile(PATTERN_STRING);

    private final long id;

    @NonNull
    private final String name;
    private final String description;
    @NonNull
    @EqualsAndHashCode.Include
    private final SystemSpec systemSpec;
    private final HierarchyView parent;
    private final Set<HierarchyView> children;

    @EqualsAndHashCode.Include
    private final String nodePath;

    private final boolean leaf;
    private final int level;

    private final long recVersion;

    private static InnerBuilder innerBuilder() {
        return new InnerBuilder();
    }

    @SuppressWarnings("squid:S00107") // too many parameters
    @lombok.Builder(builderMethodName = "innerBuilder", builderClassName = "InnerBuilder")
    private static Hierarchy innerBuilder(long id, String name, String description, SystemSpec systemSpec,
                                          @Singular Set<HierarchyView> children, HierarchyView parent, String nodePath, boolean leaf, int level, long recVersion) {
        validate(name);
        validate(parent);
        return new Hierarchy(id, name, description, systemSpec, parent, children, nodePath != null ? nodePath : getNodePath(parent, name), leaf, level, recVersion);
    }

    @lombok.Builder(builderMethodName = "builder", builderClassName = "Builder")
    private static Hierarchy userBuilder(@NonNull String name, String description, SystemSpec systemSpec, HierarchyView parent) {
        validate(name);
        validate(parent);
        return new Hierarchy(Identifiable.NOT_SET, name, description, systemSpec, parent, Collections.emptySet(),
                getNodePath(parent, name), true, getLevel(parent), Versionable.NOT_SET);
    }

    private static String getNodePath(HierarchyView parent, String name) {
        return (parent == null ? "" : parent.getNodePath()) + HIERARCHY_SEPARATOR + name;
    }

    private static int getLevel(HierarchyView parent) {
        return parent == null ? 0 : (parent.getLevel() + 1);
    }

    private static void validate(String name) {
        if (!PATTERN.matcher(name).matches()) {
            throw new IllegalArgumentException("Hierarchy node name [" + name + "] illegal, must follow pattern " + PATTERN_STRING);
        }
    }

    private static void validate(HierarchyView parent) {
        if (parent != null && !parent.hasId()) {
            throw new IllegalArgumentException("Hierarchy parent must have an id");
        }
    }

    public Builder toBuilder() {
        return innerBuilder()
                .id(id)
                .name(name)
                .description(description)
                .systemSpec(systemSpec)
                .parent(parent)
                .children(children)
                .leaf(leaf)
                .level(level)
                .recVersion(recVersion);
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class InnerBuilder extends Builder { }
}
