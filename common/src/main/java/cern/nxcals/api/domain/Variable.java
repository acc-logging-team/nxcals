/*
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.api.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

import java.util.Collections;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@JsonDeserialize(builder = Variable.InnerBuilder.class)
public class Variable implements Identifiable, Versionable {
    private final long id;
    @NonNull
    @EqualsAndHashCode.Include
    private final String variableName;
    @NonNull
    @EqualsAndHashCode.Include
    private final SystemSpec systemSpec;
    private final String description;
    private final String unit;
    @NonNull
    private final SortedSet<VariableConfig> configs;
    private final VariableDeclaredType declaredType;
    private final long recVersion;

    private static Variable.InnerBuilder innerBuilder() {
        return new Variable.InnerBuilder();
    }

    @SuppressWarnings("squid:S00107") // too many parameters
    @lombok.Builder(builderMethodName = "innerBuilder", builderClassName = "InnerBuilder")
    private static Variable innerBuilder(long id, String variableName, SystemSpec systemSpec, String description, String unit, SortedSet<VariableConfig> configs, VariableDeclaredType declaredType, long recVersion) {
        verify(configs);
        return new Variable(id, variableName, systemSpec, description, unit, immutable(configs), declaredType, recVersion);
    }

    @lombok.Builder(builderMethodName = "builder", builderClassName = "Builder")
    private static Variable userBuilder(String variableName, SystemSpec systemSpec, String description, String unit, SortedSet<VariableConfig> configs, VariableDeclaredType declaredType) {
        verify(configs);
        return new Variable(Identifiable.NOT_SET, variableName, systemSpec, description, unit, immutable(configs), declaredType, Versionable.NOT_SET);
    }

    private static <S> SortedSet<S> immutable(SortedSet<S> in) {
        if (in == null) {
            return Collections.emptySortedSet();
        } else {
            return Collections.unmodifiableSortedSet(new TreeSet<>(in));
        }
    }

    private static void verify(SortedSet<VariableConfig> configs) {
        if (areTimeStampsIntersecting(configs)) {
            throw new IllegalStateException(String.format("Variable configuration data is not valid! There are intersecting time windows : %s", configs));
        }
    }

    private static boolean areTimeStampsIntersecting(SortedSet<VariableConfig> configs) {
        if (configs == null || configs.isEmpty()) {
            return false;
        }
        Iterator<VariableConfig> it = configs.iterator();
        Iterator<VariableConfig> itSkipOne = configs.iterator();
        itSkipOne.next();

        while (itSkipOne.hasNext()) {
            TimeWindow next = it.next().getValidity();
            TimeWindow prev = itSkipOne.next().getValidity();

            if (prev.getEndTimeNanos() > next.getStartTimeNanos()) {
                return true;
            }
        }
        return false;
    }

    public Variable.Builder toBuilder() {
        return Variable.innerBuilder()
                .id(id)
                .variableName(variableName)
                .systemSpec(systemSpec)
                .description(description)
                .unit(unit)
                .configs(configs)
                .declaredType(declaredType)
                .recVersion(recVersion);
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class InnerBuilder extends Builder { }
}
