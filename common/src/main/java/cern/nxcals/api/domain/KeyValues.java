/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.api.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericRecord;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

// this class and the whole package it is in will go away
// (currently the package cern.nxcals.common.extraction.domain spans two modules) -- Kamil

/**
 * Wrapper that provides a way to identify quickly if the key values are identical or not.
 */
@Getter
@SuppressWarnings("squid:S1700") // class member and class name the same
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class KeyValues {
    @EqualsAndHashCode.Include
    private final int id;
    private final Map<String, Object> keyValues;

    public KeyValues(GenericRecord genericRecord) {
        this(genericRecord.hashCode(), toMap(genericRecord));
    }

    public KeyValues(int id, Map<String, Object> keyValues) {
        this.id = id;
        this.keyValues = Collections.unmodifiableMap(keyValues);
    }

    private static Map<String, Object> toMap(GenericRecord genericRecord) {
        Map<String, Object> keyValues = new HashMap<>();
        for (Schema.Field field : genericRecord.getSchema().getFields()) {
            String fieldName = field.name();
            keyValues.put(fieldName, genericRecord.get(fieldName));
        }
        return keyValues;
    }
}
