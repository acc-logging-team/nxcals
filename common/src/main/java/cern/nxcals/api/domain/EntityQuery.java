package cern.nxcals.api.domain;

import cern.nxcals.common.utils.StringUtils;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class EntityQuery {
    @NonNull
    private final Map<String, Object> keyValues;
    @NonNull
    private final Map<String, Object> keyValuesLike;

    public EntityQuery(@NonNull Map<String, Object> keyValues) {
        this(keyValues, new HashMap<>());
    }

    /**
     * Return key values and key values patterns as one map. Values from keyValues have escaped wildcard characters if are String
     *
     * @return map with key-values and patterns
     */
    public Map<String, Object> toMap() {
        Map<String, Object> keyValuesWithEscapedWildcards = keyValues.entrySet().stream()
                .map(entry -> ImmutablePair.of(entry.getKey(), escapeWildcards(entry.getValue())))
                .collect(Collectors.toMap(Pair::getKey, Pair::getValue));
        keyValuesWithEscapedWildcards.putAll(keyValuesLike);
        return keyValuesWithEscapedWildcards;
    }

    private static Object escapeWildcards(Object value) {
        if (value instanceof String) {
            return StringUtils.escapeWildcards((String) value);
        } else {
            return value;
        }
    }

    public boolean hasPatterns() {
        return !keyValuesLike.isEmpty();
    }
}
