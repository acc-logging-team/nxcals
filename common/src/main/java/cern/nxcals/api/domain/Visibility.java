package cern.nxcals.api.domain;

public enum Visibility {
    PUBLIC, PROTECTED
}
