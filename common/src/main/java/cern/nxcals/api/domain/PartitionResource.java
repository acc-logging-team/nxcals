package cern.nxcals.api.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;


@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@JsonDeserialize(builder = PartitionResource.InnerBuilder.class)
public class PartitionResource implements Identifiable, Versionable {

    private final long id;
    @NonNull
    @EqualsAndHashCode.Include
    private final SystemSpec systemSpec;
    @NonNull
    @EqualsAndHashCode.Include
    private final Partition partition;
    @NonNull
    @EqualsAndHashCode.Include
    private final EntitySchema schema;

    private final long recVersion;


    private static PartitionResource.InnerBuilder innerBuilder() {
        return new PartitionResource.InnerBuilder();
    }

    @lombok.Builder(builderMethodName = "innerBuilder", builderClassName = "InnerBuilder")
    private static PartitionResource innerBuilder(long id, SystemSpec systemSpec, Partition partition,
            EntitySchema schema, long recVersion) {
        return new PartitionResource(id, systemSpec, partition, schema, recVersion);
    }

    @lombok.Builder(builderMethodName = "builder", builderClassName = "Builder")
    private static PartitionResource userBuilder(SystemSpec systemSpec, Partition partition, EntitySchema schema) {
        return new PartitionResource(Identifiable.NOT_SET, systemSpec, partition, schema, Versionable.NOT_SET);
    }

    public Builder toBuilder() {
        return PartitionResource.innerBuilder()
                .id(id)
                .systemSpec(systemSpec)
                .partition(partition)
                .schema(schema)
                .recVersion(recVersion);
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class InnerBuilder extends Builder { }

}
