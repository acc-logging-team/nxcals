package cern.nxcals.api.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.Singular;
import lombok.ToString;
import lombok.Value;
import lombok.experimental.SuperBuilder;

import java.beans.Transient;
import java.util.Map;

@Value
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
@JsonDeserialize(builder = Group.InnerBuilder.class)
@ToString(callSuper = true)
public class Group extends BaseGroup {

    private final String label;
    @NonNull
    @EqualsAndHashCode.Include
    private final SystemSpec systemSpec;

    @SuppressWarnings("squid:S00107") // too many parameters
    private Group(long id, String name, String description, SystemSpec systemSpec,
            @Singular Map<String, String> properties, String owner,
            String label, Visibility visibility, long recVersion ) {
        super(id,name,description,properties,owner,visibility,recVersion);
        this.label = label;
        this.systemSpec = systemSpec;
    }

    private static InnerBuilder innerBuilder() {
        return new InnerBuilder();
    }

    @SuppressWarnings("squid:S00107") // too many parameters
    @lombok.Builder(builderMethodName = "builder", builderClassName = "Builder")
    private static Group userBuilder(String name, String description, SystemSpec systemSpec,
            @Singular Map<String, String> properties, String label, String owner, Visibility visibility) {
        return new Group(Identifiable.NOT_SET, name, description, systemSpec, properties,  owner, label, visibility,
                Versionable.NOT_SET);
    }

    @SuppressWarnings("squid:S00107") // too many parameters
    @lombok.Builder(builderMethodName = "innerBuilder", builderClassName = "InnerBuilder")
    private static Group innerBuilder(long id, String name, String description, SystemSpec systemSpec,
            @Singular Map<String, String> properties, String owner,
            String label, Visibility visibility, long recVersion) {
        return new Group(id, name, description, systemSpec, properties, owner, label, visibility, recVersion);
    }



    public Builder toBuilder() {
        return innerBuilder()
                .id(getId())
                .name(getName())
                .description(getDescription())
                .systemSpec(getSystemSpec())
                .properties(getProperties())
                .label(label)
                .owner(getOwner())
                .visibility(getVisibility())
                .recVersion(getRecVersion());
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class InnerBuilder extends Builder {
    }
}
