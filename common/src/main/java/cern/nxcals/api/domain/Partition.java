/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.api.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Data
@JsonDeserialize(builder = Partition.InnerBuilder.class)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Partition implements Identifiable, Versionable {
    private final long id;

    @NonNull
    @EqualsAndHashCode.Include
    private final Map<String, Object> keyValues;

    @NonNull
    @EqualsAndHashCode.Include
    private final SystemSpec systemSpec;

    private final PartitionProperties properties;
    private final long recVersion;

    private static Partition.InnerBuilder innerBuilder() {
        return new Partition.InnerBuilder();
    }

    @lombok.Builder(builderMethodName = "innerBuilder", builderClassName = "InnerBuilder")
    private static Partition innerBuilder(long id, Map<String, Object> keyValues, SystemSpec systemSpec, PartitionProperties properties, long recVersion) {
        return new Partition(id, immutable(keyValues), systemSpec, properties, recVersion);
    }

    @lombok.Builder(builderMethodName = "builder", builderClassName = "Builder")
    private static Partition userBuilder(@NonNull Map<String, Object> keyValues, SystemSpec systemSpec, PartitionProperties properties) {
        return new Partition(Identifiable.NOT_SET, immutable(keyValues), systemSpec, properties, Versionable.NOT_SET);
    }

    private static <K, V> Map<K, V> immutable(Map<K, V> in) {
        return Collections.unmodifiableMap(new HashMap<>(in));
    }

    public Partition.Builder toBuilder() {
        return Partition.innerBuilder()
                .id(id)
                .keyValues(keyValues)
                .systemSpec(systemSpec)
                .properties(properties)
                .recVersion(recVersion);
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class InnerBuilder extends Builder { }
}
