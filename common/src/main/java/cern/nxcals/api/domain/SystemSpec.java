package cern.nxcals.api.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@JsonDeserialize(builder = SystemSpec.InnerBuilder.class)
public class SystemSpec implements Identifiable {

    private final long id;
    @EqualsAndHashCode.Include
    @NonNull
    private final String name;
    @NonNull
    private final String entityKeyDefinitions;
    @NonNull
    private final String timeKeyDefinitions;
    @NonNull
    private final String partitionKeyDefinitions;
    private final String recordVersionKeyDefinitions;

    private static SystemSpec.InnerBuilder innerBuilder() {
        return new SystemSpec.InnerBuilder();
    }

    @lombok.Builder(builderMethodName = "innerBuilder", builderClassName = "InnerBuilder")
    private static SystemSpec innerBuilder(long id, String name, String entityKeyDefinitions, String timeKeyDefinitions, String partitionKeyDefinitions, String recordVersionKeyDefinitions) {
        return new SystemSpec(id, name, entityKeyDefinitions, timeKeyDefinitions, partitionKeyDefinitions, recordVersionKeyDefinitions);
    }

    @lombok.Builder(builderMethodName = "builder", builderClassName = "Builder")
    private static SystemSpec userBuilder(String name, String entityKeyDefinitions, String timeKeyDefinitions, String partitionKeyDefinitions, String recordVersionKeyDefinitions) {
        return new SystemSpec(Identifiable.NOT_SET, name, entityKeyDefinitions, timeKeyDefinitions, partitionKeyDefinitions, recordVersionKeyDefinitions);
    }

    public Builder toBuilder() {
        return SystemSpec.innerBuilder()
                .id(id)
                .name(name)
                .entityKeyDefinitions(entityKeyDefinitions)
                .timeKeyDefinitions(timeKeyDefinitions)
                .partitionKeyDefinitions(partitionKeyDefinitions)
                .recordVersionKeyDefinitions(recordVersionKeyDefinitions);
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class InnerBuilder extends Builder { }
}
