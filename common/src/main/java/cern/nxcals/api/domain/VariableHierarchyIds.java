package cern.nxcals.api.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

import java.util.Set;

@Value
@Builder(builderClassName = "Builder")
@JsonDeserialize(builder = VariableHierarchyIds.Builder.class)
public class VariableHierarchyIds {
    long variableId;
    @NonNull
    Set<Long> hierarchyIds;

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
    }
}
