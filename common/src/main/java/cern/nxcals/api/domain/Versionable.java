package cern.nxcals.api.domain;

@SuppressWarnings("all")
public interface Versionable {
    long NOT_SET = Long.MIN_VALUE;

    long getRecVersion();

    default boolean hasVersion() {
        return getRecVersion() != NOT_SET;
    }
}
