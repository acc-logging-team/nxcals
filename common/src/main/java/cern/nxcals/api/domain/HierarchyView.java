package cern.nxcals.api.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(as = Hierarchy.class)
public interface HierarchyView extends Identifiable {
    String getName();
    String getDescription();
    String getNodePath();
    boolean isLeaf();
    int getLevel();
}
