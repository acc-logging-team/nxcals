package cern.nxcals.api.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.Instant;

@Data
@JsonDeserialize(builder = VariableConfigChangelog.InnerBuilder.class)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class VariableConfigChangelog implements Identifiable {

    private final long id;
    private long variableConfigId;
    private Long oldVariableId;
    private Long newVariableId;
    private Long oldEntityId;
    private Long newEntityId;
    private String oldFieldName;
    private String newFieldName;
    private Instant newValidFromStamp;
    private Instant oldValidFromStamp;
    private Instant oldValidToStamp;
    private Instant newValidToStamp;
    private Instant createTimeUtc;
    private OperationType opType;
    private String clientInfo;

    private static InnerBuilder innerBuilder() {
        return new InnerBuilder();
    }

    @lombok.Builder(builderMethodName = "innerBuilder", builderClassName = "InnerBuilder")
    private static VariableConfigChangelog innerBuilder(long id, long variableConfigId, Long oldVariableId,
            Long newVariableId, Long oldEntityId, Long newEntityId, String oldFieldName, String newFieldName,
            Instant newValidFromStamp, Instant oldValidFromStamp, Instant oldValidToStamp, Instant newValidToStamp,
            Instant createTimeUtc, OperationType opType, String clientInfo) {
        return new VariableConfigChangelog(id, variableConfigId, oldVariableId, newVariableId, oldEntityId, newEntityId,
                oldFieldName, newFieldName, newValidFromStamp, oldValidFromStamp, oldValidToStamp, newValidToStamp,
                createTimeUtc, opType, clientInfo);
    }

    @lombok.Builder(builderMethodName = "builder", builderClassName = "Builder")
    private static VariableConfigChangelog userBuilder(long variableConfigId, Long oldVariableId, Long newVariableId,
            Long oldEntityId, Long newEntityId, String oldFieldName, String newFieldName, Instant newValidFromStamp,
            Instant oldValidFromStamp, Instant oldValidToStamp, Instant newValidToStamp, Instant createTimeUtc,
            OperationType opType, String clientInfo) {
        return new VariableConfigChangelog(Identifiable.NOT_SET, variableConfigId, oldVariableId, newVariableId,
                oldEntityId, newEntityId, oldFieldName, newFieldName, newValidFromStamp, oldValidFromStamp,
                oldValidToStamp, newValidToStamp, createTimeUtc, opType, clientInfo);
    }

    public Builder toBuilder() {
        return VariableConfigChangelog.innerBuilder()
                .id(id)
                .variableConfigId(variableConfigId)
                .oldVariableId(oldVariableId)
                .newVariableId(newVariableId)
                .oldEntityId(oldEntityId)
                .newEntityId(newEntityId)
                .oldFieldName(oldFieldName)
                .newFieldName(newFieldName)
                .oldValidFromStamp(oldValidFromStamp)
                .newValidFromStamp(newValidFromStamp)
                .oldValidToStamp(oldValidToStamp)
                .newValidToStamp(newValidToStamp)
                .createTimeUtc(createTimeUtc)
                .opType(opType)
                .clientInfo(clientInfo);
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class InnerBuilder extends Builder {
    }
}
