package cern.nxcals.api.domain;

import cern.nxcals.api.utils.TimeUtils;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Builder;
import lombok.Data;

@Data
@JsonDeserialize
@Builder(builderClassName = "Builder")
public class PartitionProperties {

    private final boolean updatable;
    private final TimeUtils.TimeSplits stagingPartitionType;
    private final TimeWindow stagingPartitionTypeValidity;

    @JsonCreator
    public PartitionProperties(@JsonProperty("updatable") boolean updatable,
            @JsonProperty("stagingPartitionType") TimeUtils.TimeSplits stagingPartitionType,
            @JsonProperty("stagingPartitionTypeValidity") TimeWindow stagingPartitionTypeValidity) {
        this.updatable = updatable;
        this.stagingPartitionType = stagingPartitionType;
        this.stagingPartitionTypeValidity = stagingPartitionTypeValidity;
    }
}