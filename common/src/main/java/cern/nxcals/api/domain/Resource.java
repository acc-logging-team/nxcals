package cern.nxcals.api.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.Singular;
import lombok.ToString;

import java.net.URI;
import java.util.Set;

@Getter
@Builder(builderClassName = "Builder")
@JsonDeserialize(builder = Resource.Builder.class)
@ToString
public class Resource {
    @NonNull
    private final String hbaseNamespace;
    @Singular
    @NonNull
    private final Set<URI> hdfsPaths;
    @Singular
    @NonNull
    private final Set<String> hbaseTableNames;

    private final boolean hbaseTableAccessible;

    @JsonPOJOBuilder(withPrefix = "")
    @SuppressWarnings("squid:S1068") // unused parameter
    public static final class Builder {
        private String hbaseNamespace = "default";
    }
}
