package cern.nxcals.api.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import java.util.Map;

@Data
@Builder(builderClassName = "Builder")
@JsonDeserialize(builder = CreateEntityRequest.Builder.class)
public class CreateEntityRequest {
    private final long systemId;
    @NonNull
    private final Map<String, Object> entityKeyValues;
    @NonNull
    private final Map<String, Object> partitionKeyValues;

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
    }
}
