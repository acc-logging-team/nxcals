package cern.nxcals.api.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

import java.util.Set;

@Value
@Builder(builderClassName = "Builder")
@JsonDeserialize(builder = VariableHierarchies.Builder.class)
public class VariableHierarchies {
    private final long variableId;
    @NonNull
    private final Set<Hierarchy> hierarchies;

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
    }
}
