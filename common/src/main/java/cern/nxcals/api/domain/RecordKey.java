package cern.nxcals.api.domain;

import lombok.Builder;
import lombok.Value;

import java.nio.ByteBuffer;

/**
 * This class represents the Kafka record key for ingested data.
 */
@Builder
@Value
public class RecordKey {
    private static final int SIZE = 2 * Byte.BYTES + Short.BYTES + 5 * Long.BYTES + /*unused*/ 4 * Long.BYTES;
    private static final byte[] MAGIC_BYTES = new byte[] { Byte.MIN_VALUE, Byte.MIN_VALUE };

    private static final short PROTOCOL_VERSION = 1; //currently unused, for future changes
    private final long systemId;
    private final long partitionId;
    private final long schemaId;
    private final long entityId;
    private final long timestamp;

    public byte[] serialize() {
        return serialize(this.systemId, this.partitionId, this.schemaId, this.entityId, this.timestamp);
    }

    public static byte[] serialize(long systemId, long partitionId, long schemaId, long entityId, long timestamp) {
        ByteBuffer buffer = ByteBuffer.allocate(SIZE);
        return buffer
                .put(MAGIC_BYTES)
                .putShort(PROTOCOL_VERSION)
                .putLong(systemId)
                .putLong(partitionId)
                .putLong(schemaId)
                .putLong(entityId)
                .putLong(timestamp)
                .putLong(0) // unused (for future use)
                .putLong(0) // unused
                .putLong(0) // unused
                .putLong(0) // unused
                .array();
    }

    public static RecordKey deserialize(byte[] array) {
        if (!isValid(array)) {
            throw new IllegalArgumentException("Invalid record key array");
        }
        ByteBuffer buffer = ByteBuffer.wrap(array, MAGIC_BYTES.length, array.length - MAGIC_BYTES.length);
        buffer.getShort(); //protocol version, unused currently
        return new RecordKey(buffer.getLong(), buffer.getLong(), buffer.getLong(), buffer.getLong(), buffer.getLong());
    }

    public static boolean isValid(byte[] array) {
        return array.length == SIZE && array[0] == MAGIC_BYTES[0] && array[1] == MAGIC_BYTES[1];
    }
}
