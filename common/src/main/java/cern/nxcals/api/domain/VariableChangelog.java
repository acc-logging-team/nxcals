package cern.nxcals.api.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.Instant;

@Data
@JsonDeserialize(builder = VariableChangelog.InnerBuilder.class)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class VariableChangelog implements Identifiable {

    private final long id;
    private long variableId;
    private String oldVariableName;
    private String newVariableName;
    private String oldDescription;
    private String newDescription;
    private String oldUnit;
    private String newUnit;
    private Long oldSystemId;
    private Long newSystemId;
    private VariableDeclaredType oldDeclaredType;
    private VariableDeclaredType newDeclaredType;
    private Instant createTimeUtc;
    private OperationType opType;
    private String clientInfo;

    private static InnerBuilder innerBuilder() {
        return new InnerBuilder();
    }

    @lombok.Builder(builderMethodName = "innerBuilder", builderClassName = "InnerBuilder")
    private static VariableChangelog innerBuilder(long id, long variableId, String oldVariableName,
            String newVariableName, String oldDescription, String newDescription, String oldUnit, String newUnit,
            Long oldSystemId, Long newSystemId, VariableDeclaredType oldDeclaredType,
            VariableDeclaredType newDeclaredType, Instant createTimeUtc, OperationType opType, String clientInfo) {
        return new VariableChangelog(id, variableId, oldVariableName, newVariableName, oldDescription, newDescription,
                oldUnit, newUnit, oldSystemId, newSystemId, oldDeclaredType, newDeclaredType, createTimeUtc, opType,
                clientInfo);
    }

    @lombok.Builder(builderMethodName = "builder", builderClassName = "Builder")
    private static VariableChangelog userBuilder(long variableId, String oldVariableName, String newVariableName,
            String oldDescription, String newDescription, String oldUnit, String newUnit, Long oldSystemId,
            Long newSystemId, VariableDeclaredType oldDeclaredType, VariableDeclaredType newDeclaredType,
            Instant createTimeUtc, OperationType opType, String clientInfo) {
        return new VariableChangelog(Identifiable.NOT_SET, variableId, oldVariableName, newVariableName, oldDescription,
                newDescription, oldUnit, newUnit, oldSystemId, newSystemId, oldDeclaredType, newDeclaredType,
                createTimeUtc, opType, clientInfo);
    }

    public Builder toBuilder() {
        return VariableChangelog.innerBuilder()
                .id(id)
                .variableId(variableId)
                .oldVariableName(oldVariableName)
                .newVariableName(newVariableName)
                .oldDescription(oldDescription)
                .newDescription(newDescription)
                .oldUnit(oldUnit)
                .newUnit(newUnit)
                .oldSystemId(oldSystemId)
                .newSystemId(newSystemId)
                .oldDeclaredType(oldDeclaredType)
                .newDeclaredType(newDeclaredType)
                .createTimeUtc(createTimeUtc)
                .opType(opType)
                .clientInfo(clientInfo);
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class InnerBuilder extends Builder {
    }
}
