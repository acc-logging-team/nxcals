package cern.nxcals.api.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.Instant;

@Data
@JsonDeserialize(builder = HierarchyChangelog.InnerBuilder.class)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class HierarchyChangelog implements Identifiable {

    private final long id;
    private long hierarchyId;
    private String oldHierarchyName;
    private String newHierarchyName;
    private Long oldParentId;
    private Long newParentId;
    private Long oldSystemId;
    private Long newSystemId;
    private Instant createTimeUtc;
    private OperationType opType;
    private String clientInfo;

    private static InnerBuilder innerBuilder() {
        return new InnerBuilder();
    }

    @lombok.Builder(builderMethodName = "innerBuilder", builderClassName = "InnerBuilder")
    private static HierarchyChangelog innerBuilder(long id, long hierarchyId, String oldHierarchyName,
            String newHierarchyName, Long oldParentId, Long newParentId,
            Long oldSystemId, Long newSystemId, Instant createTimeUtc, OperationType opType, String clientInfo) {
        return new HierarchyChangelog(id, hierarchyId, oldHierarchyName, newHierarchyName, oldParentId, newParentId,
                oldSystemId, newSystemId, createTimeUtc, opType, clientInfo);
    }

    @lombok.Builder(builderMethodName = "builder", builderClassName = "Builder")
    private static HierarchyChangelog userBuilder(long hierarchyId, String oldHierarchyName, String newHierarchyName,
            Long oldParentId, Long newParentId, Long oldSystemId, Long newSystemId,
            Instant createTimeUtc, OperationType opType, String clientInfo) {
        return new HierarchyChangelog(Identifiable.NOT_SET, hierarchyId, oldHierarchyName, newHierarchyName,
                oldParentId, newParentId, oldSystemId, newSystemId, createTimeUtc, opType,
                clientInfo);
    }

    public Builder toBuilder() {
        return HierarchyChangelog.innerBuilder()
                .id(id)
                .hierarchyId(hierarchyId)
                .oldHierarchyName(oldHierarchyName)
                .newHierarchyName(newHierarchyName)
                .oldParentId(oldParentId)
                .newParentId(newParentId)
                .oldSystemId(oldSystemId)
                .newSystemId(newSystemId)
                .createTimeUtc(createTimeUtc)
                .opType(opType)
                .clientInfo(clientInfo);
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class InnerBuilder extends Builder {
    }
}
