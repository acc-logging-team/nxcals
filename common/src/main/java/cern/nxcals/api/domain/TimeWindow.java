package cern.nxcals.api.domain;

import cern.nxcals.api.utils.TimeUtils;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.ObjectUtils;

import java.io.Serializable;
import java.time.Instant;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@JsonDeserialize
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public final class TimeWindow implements Serializable {
    private static final long serialVersionUID = -7460156775582386986L;
    private static final TimeWindow EMPTY = new TimeWindow(Long.MIN_VALUE, Long.MIN_VALUE);

    @EqualsAndHashCode.Include
    private final Instant startTime;
    @EqualsAndHashCode.Include
    private final Instant endTime;

    private TimeWindow(long startNanos, long endNanos) {
        this(TimeUtils.getInstantFromNanos(startNanos), TimeUtils.getInstantFromNanos(endNanos));
    }

    public static Comparator<TimeWindow> compareByStartTime() {
        return Comparator.nullsFirst(Comparator.comparingLong(TimeWindow::getStartTimeNanos));
    }

    public static Comparator<TimeWindow> compareByEndTime() {
        return Comparator.nullsLast(Comparator.comparingLong(TimeWindow::getEndTimeNanos));
    }

    public static Comparator<TimeWindow> compareByStartAndEndTime() {
        return TimeWindow.compareByStartTime().thenComparing(TimeWindow.compareByEndTime());
    }

    public static TimeWindow infinite() {
        return new TimeWindow(null, null);
    }

    public static TimeWindow empty() {
        return EMPTY;
    }

    public static TimeWindow between(long startNanos, long endNanos) {
        if (startNanos <= endNanos) {
            return new TimeWindow(startNanos, endNanos);
        }
        return empty();
    }

    public static TimeWindow after(long startNanos) {
        return after(TimeUtils.getInstantFromNanos(startNanos));
    }

    public static TimeWindow after(Instant start) {
        return new TimeWindow(start, null);
    }

    public static TimeWindow before(long endNanos) {
        return before(TimeUtils.getInstantFromNanos(endNanos));
    }

    public static TimeWindow before(Instant end) {
        return new TimeWindow(null, end);
    }

    @JsonCreator
    public static TimeWindow between(@JsonProperty("startTime") Instant startTime,
            @JsonProperty("endTime") Instant endTime) {
        if (startTime == null || endTime == null || !endTime.isBefore(startTime)) {
            return new TimeWindow(startTime, endTime);
        }
        return empty();
    }

    public static TimeWindow fromStrings(String startTime, String endTime) {
        return TimeWindow.between(
                startTime == null ? null : TimeUtils.getInstantFromString(startTime),
                endTime == null ? null : TimeUtils.getInstantFromString(endTime)
        );
    }

    private static boolean isInfinite(Instant edge) {
        return edge == null;
    }

    @JsonIgnore
    public long getStartTimeNanos() {
        return isLeftInfinite() ? Long.MIN_VALUE : TimeUtils.getNanosFromInstant(startTime);
    }

    @JsonIgnore
    public long getEndTimeNanos() {
        return isRightInfinite() ? Long.MAX_VALUE : TimeUtils.getNanosFromInstant(endTime);
    }

    public TimeWindow expand(TimeWindow window) {
        if (window.isEmpty() && isEmpty()) {
            return empty();
        } else if (bounds(window)) {
            return this;
        } else if (window.bounds(this)) {
            return window;
        }

        return between(minStart(startTime, window.startTime), maxEnd(endTime, window.endTime));
    }

    public TimeWindow intersect(TimeWindow window) {
        if (window.isEmpty() || isEmpty()) {
            return empty();
        } else if (window.bounds(this)) {
            return this;
        } else if (bounds(window)) {
            return window;
        }

        return between(maxStart(startTime, window.startTime), minEnd(endTime, window.endTime));
    }

    public boolean contains(Instant instant) {
        return !instant.isBefore(safeStartTime()) && !instant.isAfter(safeEndTime());
    }

    public boolean contains(long time) {
        return contains(TimeUtils.getInstantFromNanos(time));
    }

    public boolean bounds(TimeWindow window) {
        if (window.isEmpty() && isEmpty()) {
            return false;
        } else if (window.isEmpty()) {
            return true;
        } else if (isEmpty()) {
            return false;
        }

        return !window.safeStartTime().isBefore(safeStartTime()) && !window.safeEndTime().isAfter(safeEndTime());
    }

    public boolean intersects(TimeWindow window) {
        if (window.isEmpty() && isEmpty()) {
            return true;
        } else if (window.isEmpty()) {
            return false;
        } else if (isEmpty()) {
            return true;
        }
        //(StartA <= EndB) and (EndA >= StartB)
        return !window.safeStartTime().isAfter(safeEndTime()) && !window.safeEndTime().isBefore(safeStartTime());
    }

    public TimeWindow leftLimit(Instant limit) {
        if (isInfinite(limit) || isEmpty() || limit.isBefore(safeStartTime())) {
            return this;
        } else if (limit.isAfter(safeEndTime())) {
            return empty();
        }

        return between(maxStart(startTime, limit), maxEnd(endTime, limit));
    }

    public TimeWindow rightLimit(Instant limit) {
        if (isInfinite(limit) || isEmpty() || limit.isAfter(safeEndTime())) {
            return this;
        } else if (limit.isBefore(safeStartTime())) {
            return empty();
        }

        return between(minStart(startTime, limit), minEnd(endTime, limit));
    }

    @JsonIgnore
    public boolean isWithinRangeRightExcluded(@NonNull long nanos) {
        return (isLeftInfinite() || nanos >= TimeUtils.getNanosFromInstant(startTime))
                && (isRightInfinite() || nanos < TimeUtils.getNanosFromInstant(endTime));
    }

    @JsonIgnore
    public boolean isEmpty() {
        return this == EMPTY;
    }

    @JsonIgnore
    public boolean isFinite() {
        return !isLeftInfinite() && !isRightInfinite();
    }

    @JsonIgnore
    public boolean isInfinite() {
        return isLeftInfinite() && isRightInfinite();
    }

    @JsonIgnore
    public boolean isLeftInfinite() {
        return isInfinite(startTime);
    }

    @JsonIgnore
    public boolean isRightInfinite() {
        return isInfinite(endTime);
    }

    private Instant maxEnd(Instant a, Instant b) {
        return isInfinite(a) || isInfinite(b) ? null : ObjectUtils.max(a, b);
    }

    private Instant minEnd(Instant a, Instant b) {
        return isInfinite(a) || isInfinite(b) ? ObjectUtils.firstNonNull(a, b) : ObjectUtils.min(a, b);
    }

    private Instant maxStart(Instant a, Instant b) {
        return isInfinite(a) || isInfinite(b) ? ObjectUtils.firstNonNull(a, b) : ObjectUtils.max(a, b);
    }

    private Instant minStart(Instant a, Instant b) {
        return isInfinite(a) || isInfinite(b) ? null : ObjectUtils.min(a, b);
    }

    private Instant safeStartTime() {
        return isLeftInfinite() ? Instant.MIN : startTime;
    }

    private Instant safeEndTime() {
        return isRightInfinite() ? Instant.MAX : endTime;
    }

    public static Set<TimeWindow> mergeTimeWindows(Collection<TimeWindow> windows) {
        List<TimeWindow> sortedWindows = windows.stream().sorted(Comparator.comparing(TimeWindow::getStartTimeNanos))
                .collect(Collectors.toList());

        TimeWindow lastWindow = null;
        HashSet<TimeWindow> mergedWindows = new HashSet<>();
        for (TimeWindow currentWindow : sortedWindows) {
            if (lastWindow == null) {
                lastWindow = currentWindow;
            } else {
                if (lastWindow.intersects(currentWindow)) {
                    lastWindow = lastWindow.expand(currentWindow);
                } else {
                    mergedWindows.add(lastWindow);
                    lastWindow = currentWindow;
                }
            }
        }

        if (lastWindow != null) {
            mergedWindows.add(lastWindow);
        }

        return mergedWindows;
    }
}
