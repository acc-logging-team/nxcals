package cern.nxcals.api.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;

import java.beans.Transient;
import java.util.Collections;
import java.util.Map;

@Getter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@ToString
public class BaseGroup implements Identifiable, Versionable {

    private final long id;
    @NonNull
    @EqualsAndHashCode.Include
    private final String name;
    private final String description;

    @NonNull
    private final Map<String, String> properties;

    private final String owner;
    @NonNull
    private final Visibility visibility;
    private final long recVersion;

    @Transient
    public boolean isVisible() {
        return getVisibility().equals(Visibility.PUBLIC);
    }

    public Map<String, String> getProperties() {
        return Collections.unmodifiableMap(properties);
    }

    protected Map<String, String> getPropertiesToModify() {
        return properties;
    }
}
