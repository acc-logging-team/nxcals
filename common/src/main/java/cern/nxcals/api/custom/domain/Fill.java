package cern.nxcals.api.custom.domain;

import cern.nxcals.api.domain.TimeWindow;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.Value;

import java.util.List;

@Value
@AllArgsConstructor
public class Fill {
    private final int number;
    @NonNull
    private final TimeWindow validity;
    @NonNull
    private final List<BeamMode> beamModes;
}
