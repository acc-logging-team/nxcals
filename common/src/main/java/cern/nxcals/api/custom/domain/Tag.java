package cern.nxcals.api.custom.domain;

import cern.nxcals.api.domain.BaseGroup;
import cern.nxcals.api.domain.Identifiable;
import cern.nxcals.api.domain.Versionable;
import cern.nxcals.api.domain.Visibility;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.common.annotation.Experimental;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.ToString;
import lombok.Value;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

@Value
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
@JsonDeserialize(builder = Tag.InnerBuilder.class)
@ToString(callSuper = true)
@Experimental
public class Tag extends BaseGroup {
    @NonNull
    @EqualsAndHashCode.Include
    private final String system;

    @SuppressWarnings("squid:S00107") // too many parameters
    private Tag(long id, String name, String description, String system,
            String owner,
            Visibility visibility, long recVersion, Instant timestamp, Map<String, String> props) {
        super(id, name, description, new HashMap<>(props), owner, visibility, recVersion);
        this.system = system;

        getPropertiesToModify()
                .put("tag_timestamp", TimeUtils.getStringFromNanos(TimeUtils.getNanosFromInstant(timestamp)));
    }

    private static Tag.InnerBuilder innerBuilder() {
        return new Tag.InnerBuilder();
    }

    @SuppressWarnings("squid:S00107") // too many parameters
    @lombok.Builder(builderMethodName = "builder", builderClassName = "Builder")
    private static Tag userBuilder(String name, String description, String system,
            String owner, Visibility visibility, Instant timestamp, Map<String, String> properties) {
        return new Tag(Identifiable.NOT_SET, name, description, system, owner, visibility,
                Versionable.NOT_SET, timestamp, properties);
    }

    @SuppressWarnings("squid:S00107") // too many parameters
    @lombok.Builder(builderMethodName = "innerBuilder", builderClassName = "InnerBuilder")
    private static Tag innerBuilder(long id, String name, String description, String system,
            String owner,
            Visibility visibility, long recVersion, Instant timestamp, Map<String, String> properties) {
        return new Tag(id, name, description, system, owner, visibility, recVersion, timestamp, properties);
    }

    public Instant getTimestamp() {
        return TimeUtils.getInstantFromString(getProperties().get("tag_timestamp"));
    }

    public Tag.Builder toBuilder() {
        return innerBuilder()
                .id(getId())
                .name(getName())
                .description(getDescription())
                .system(getSystem())
                .owner(getOwner())
                .visibility(getVisibility())
                .recVersion(getRecVersion())
                .properties(getProperties())
                .timestamp(getTimestamp());

    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class InnerBuilder extends Tag.Builder {
    }

}
