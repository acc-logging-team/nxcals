package cern.nxcals.api.custom.domain.constants;

import cern.nxcals.common.utils.GlobalObjectMapperProvider;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class BeamModeData {
    private BeamModeValidity validity;
    private String beamModeValue;

    @JsonCreator
    public BeamModeData(@JsonProperty("validity") BeamModeValidity beamModeValidity, @JsonProperty("beamModeValue") String beamModeValue) {
        this.validity = beamModeValidity;
        this.beamModeValue = beamModeValue;
    }

    @Override
    public String toString() {
        try {
            return GlobalObjectMapperProvider.get().writeValueAsString(this);
        } catch (Exception e) {
            throw new IllegalArgumentException("Cannot convert to JSON");
        }
    }

    public static BeamModeData fromJson(String json) {
        try {
            return json != null ? GlobalObjectMapperProvider.get().readValue(json, BeamModeData.class) : null;
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException("Cannot convert to BeamModeData");
        }
    }
}
