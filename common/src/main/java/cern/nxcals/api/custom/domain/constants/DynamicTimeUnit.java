package cern.nxcals.api.custom.domain.constants;

public enum DynamicTimeUnit {
    SECONDS, MINUTES, HOURS, DAYS, WEEKS, MONTHS, YEARS, FILLS, BEAM_MODES
}
