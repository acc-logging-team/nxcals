package cern.nxcals.api.custom.domain;

import com.fasterxml.jackson.annotation.JacksonInject;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.OptBoolean;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.Value;

import java.util.UUID;

@Value
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@JsonDeserialize(builder = FundamentalFilter.InnerBuilder.class)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class FundamentalFilter {
    @JsonIgnore
    @NonNull
    String id;
    @EqualsAndHashCode.Include
    String destination;
    @EqualsAndHashCode.Include
    String lsaCycle;
    @EqualsAndHashCode.Include
    String timingUser;
    @EqualsAndHashCode.Include
    String accelerator;

    private static InnerBuilder innerBuilder() {
        return new InnerBuilder();
    }

    @lombok.Builder(builderMethodName = "builder", builderClassName = "Builder")
    private static FundamentalFilter userBuilder(String destination, String lsaCycle, String timingUser,
            String accelerator) {
        return innerBuilder(UUID.randomUUID().toString(), destination, lsaCycle, timingUser, accelerator);
    }

    @lombok.Builder(builderMethodName = "innerBuilder", builderClassName = "InnerBuilder")
    private static FundamentalFilter innerBuilder(String id, String destination, String lsaCycle, String timingUser,
            String accelerator) {
        return new FundamentalFilter(id, destination, lsaCycle, timingUser, accelerator);
    }

    public Builder toBuilder() {
        return innerBuilder()
                .id(id)
                .destination(destination)
                .lsaCycle(lsaCycle)
                .timingUser(timingUser)
                .accelerator(accelerator);
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class InnerBuilder extends Builder {
        @JacksonInject(value = "id", useInput = OptBoolean.FALSE)
        public void setId(String id) {
            this.id = id;
        }
    }
}
