package cern.nxcals.api.custom.domain.snapshot;

import com.google.common.annotations.VisibleForTesting;
import lombok.AllArgsConstructor;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.function.Supplier;

@AllArgsConstructor
public enum PriorTime {

    NOW("Now", LocalDateTime::now),
    START_OF_HOUR("Start of hour", () -> LocalDateTime.now().withMinute(0).withSecond(0).withNano(0)),
    START_OF_DAY("Start of day", () -> LocalDateTime.now().withHour(0).withMinute(0).withSecond(0).withNano(0)),
    START_OF_MONTH("Start of month", () -> YearMonth.from(LocalDateTime.now()).atDay(1).atStartOfDay()),
    START_OF_YEAR("Start of year", () -> LocalDate.of(LocalDateTime.now().getYear(), 1, 1).atStartOfDay()),

    PREVIOUS_MONDAY("Previous Monday", () -> previousWeekDay(LocalDateTime.now(), DayOfWeek.MONDAY)),
    PREVIOUS_TUESDAY("Previous Tuesday", () -> previousWeekDay(LocalDateTime.now(), DayOfWeek.TUESDAY)),
    PREVIOUS_WEDNESDAY("Previous Wednesday", () -> previousWeekDay(LocalDateTime.now(), DayOfWeek.WEDNESDAY)),
    PREVIOUS_THURSDAY("Previous Thursday", () -> previousWeekDay(LocalDateTime.now(), DayOfWeek.THURSDAY)),
    PREVIOUS_FRIDAY("Previous Friday", () -> previousWeekDay(LocalDateTime.now(), DayOfWeek.FRIDAY)),
    PREVIOUS_SATURDAY("Previous Saturday", () -> previousWeekDay(LocalDateTime.now(), DayOfWeek.SATURDAY)),
    PREVIOUS_SUNDAY("Previous Sunday", () -> previousWeekDay(LocalDateTime.now(), DayOfWeek.SUNDAY));

    private final String description;
    private final Supplier<LocalDateTime> time;


    public String description() {
        return this.description;
    }


    public LocalDateTime time() {
        return this.time.get();
    }

    public static PriorTime from(String desc) {
        for (PriorTime pT : values()) {
            if (pT.description().equals(desc)) {
                return pT;
            }
        }
        throw new IllegalArgumentException("Invalid string supplied");
    }

    @Override
    public String toString() {
        return this.description();
    }

    @VisibleForTesting
    static LocalDateTime previousWeekDay(LocalDateTime refDateTime, DayOfWeek dayOfWeek) {
        return refDateTime.with(TemporalAdjusters.previousOrSame(dayOfWeek)).toLocalDate().atStartOfDay();
    }

    public static LocalDateTime getEndTime(String priorTime, LocalDateTime refTime) {
        switch (PriorTime.from(priorTime)) {
            case NOW:
                return refTime;
            case START_OF_HOUR:
                return refTime.truncatedTo(ChronoUnit.HOURS);
            case START_OF_DAY:
                return refTime.truncatedTo(ChronoUnit.DAYS);
            case START_OF_MONTH:
                return refTime.withDayOfMonth(1).truncatedTo(ChronoUnit.DAYS);
            case START_OF_YEAR:
                return refTime.withDayOfYear(1).truncatedTo(ChronoUnit.DAYS);
            case PREVIOUS_MONDAY:
                return previousWeekDay(refTime, DayOfWeek.MONDAY);
            case PREVIOUS_TUESDAY:
                return previousWeekDay(refTime, DayOfWeek.TUESDAY);
            case PREVIOUS_WEDNESDAY:
                return previousWeekDay(refTime, DayOfWeek.WEDNESDAY);
            case PREVIOUS_THURSDAY:
                return previousWeekDay(refTime, DayOfWeek.THURSDAY);
            case PREVIOUS_FRIDAY:
                return previousWeekDay(refTime, DayOfWeek.FRIDAY);
            case PREVIOUS_SATURDAY:
                return previousWeekDay(refTime, DayOfWeek.SATURDAY);
            case PREVIOUS_SUNDAY:
                return previousWeekDay(refTime, DayOfWeek.SUNDAY);
            default:
                throw new IllegalArgumentException(priorTime + " not supported as a prior time");
        }
    }

    public LocalDateTime withRespectTo(LocalDateTime refTime) {
        return getEndTime(description, refTime);
    }
}
