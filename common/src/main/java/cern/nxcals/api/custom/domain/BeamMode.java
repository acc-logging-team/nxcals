package cern.nxcals.api.custom.domain;

import cern.nxcals.api.domain.TimeWindow;
import lombok.NonNull;
import lombok.Value;

@Value
public class BeamMode {
    @NonNull
    private final TimeWindow validity;
    @NonNull
    private final String beamModeValue;

    public BeamMode limitTo(TimeWindow limit) {
        if (limit.bounds(validity)) {
            return this;
        }
        return new BeamMode(validity.intersect(limit), beamModeValue);
    }
}
