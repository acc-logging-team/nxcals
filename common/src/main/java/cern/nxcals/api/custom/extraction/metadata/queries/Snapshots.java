package cern.nxcals.api.custom.extraction.metadata.queries;

import cern.nxcals.api.custom.domain.GroupType;
import cern.nxcals.api.domain.Visibility;
import cern.nxcals.api.extraction.metadata.queries.BaseBuilder;
import cern.nxcals.api.extraction.metadata.queries.KeyValuesProperty;
import cern.nxcals.api.extraction.metadata.queries.StringLikeProperty;
import com.github.rutledgepaulv.qbuilders.properties.concrete.EnumProperty;
import com.github.rutledgepaulv.qbuilders.properties.concrete.LongProperty;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Snapshots extends BaseBuilder<Snapshots> {

    public static Snapshots suchThat() {
        return new Snapshots().stringLike("label").eq(GroupType.SNAPSHOT.toString()).and();
    }

    public LongProperty<Snapshots> id() {
        return longNum("id");
    }

    public LongProperty<Snapshots> systemId() {
        return longNum("system.id");
    }


    public EnumProperty<Snapshots, Visibility> visibility() {
        return enumeration("visibility");
    }

    public StringLikeProperty<Snapshots> systemName() {
        return stringLike("system.name");
    }

    public StringLikeProperty<Snapshots> name() {
        return stringLike("name");
    }

    public StringLikeProperty<Snapshots> description() {
        return stringLike("description");
    }

    public StringLikeProperty<Snapshots> variableName() {
        return stringLike("variables.variable.variableName");
    }

    public KeyValuesProperty<Snapshots> entityKeyValues() {
        return entityKeyValues("entities.entity.keyValues");
    }


    public StringLikeProperty<Snapshots> ownerName() {
        return stringLike("owner");
    }
}

