package cern.nxcals.api.custom.domain.constants;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.time.Instant;

@Data
@Builder
public class BeamModeValidity {
    private Instant startTime;
    private Instant endTime;

    @JsonCreator
    public BeamModeValidity(@JsonProperty("startTime") Instant startTime, @JsonProperty("endTime") Instant endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }
}
