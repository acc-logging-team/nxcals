package cern.nxcals.api.custom.domain;

import cern.nxcals.api.custom.domain.constants.BeamModeData;
import cern.nxcals.api.custom.domain.constants.DynamicTimeUnit;
import cern.nxcals.api.custom.domain.constants.LoggingTimeZone;
import cern.nxcals.api.custom.domain.snapshot.PriorTime;
import cern.nxcals.api.custom.domain.util.FundamentalMapper;
import cern.nxcals.api.domain.BaseGroup;
import cern.nxcals.api.domain.Identifiable;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Versionable;
import cern.nxcals.api.domain.Visibility;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.common.annotation.Experimental;
import cern.nxcals.common.utils.MapUtils;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.ToString;
import lombok.Value;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Value
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = true)
@JsonDeserialize(builder = Snapshot.InnerBuilder.class)
@ToString(callSuper = true)
@Experimental
public class Snapshot extends BaseGroup {
    @EqualsAndHashCode.Include
    String system;

    @SuppressWarnings("squid:S00107") // too many parameters
    private Snapshot(long id, String name, String description, @NonNull String system,
                     String owner, Visibility visibility, long recVersion,
                     BeamModeData beamModeEnd,
                     BeamModeData beamModeStart,
                     Integer fillNumber,
                     Integer dynamicDuration,
                     Instant endTime,
                     Set<FundamentalFilter> fundamentalFilterSet,
                     PriorTime priorTime,
                     Instant startTime,
                     DynamicTimeUnit time,
                     LoggingTimeZone timeZone,
                     Boolean isEndTimeDynamic,
                     TimeDefinition timeDefinition
    ) {
        super(id, name, description, new HashMap<>(), owner, visibility, recVersion);
        this.system = system;

        Map<String, String> propertiesToModify = getPropertiesToModify();

        setPropertyAsString(SnapshotProperty.BEAM_MODE_END, beamModeEnd);
        setPropertyAsString(SnapshotProperty.BEAM_MODE_START, beamModeStart);
        setPropertyAsString(SnapshotProperty.FILL_NUMBER, fillNumber);
        setPropertyAsString(SnapshotProperty.DYNAMIC_DURATION, dynamicDuration);
        setPropertyAsString(SnapshotProperty.END_TIME, endTime);
        propertiesToModify.putAll(FundamentalMapper.toGroupProperties(fundamentalFilterSet));
        setPropertyAsString(SnapshotProperty.PRIOR_TIME, priorTime);
        setPropertyAsString(SnapshotProperty.START_TIME, startTime);
        setPropertyAsString(SnapshotProperty.TIME, time);
        setPropertyAsString(SnapshotProperty.TIME_ZONE, timeZone);
        setPropertyAsString(SnapshotProperty.IS_END_TIME_DYNAMIC, isEndTimeDynamic);

        if (timeDefinition != null) {
            propertiesToModify.putAll(MapUtils.mapKeys(timeDefinition.getProperties(), SnapshotProperty::getValue));
        }
    }

    @SuppressWarnings("squid:S00107") // too many parameters
    private Snapshot(long id, String name, String description, @NonNull String system,
                     String owner, Visibility visibility, long recVersion,
                     Set<FundamentalFilter> fundamentalFilterSet, TimeDefinition timeDefinition
    ) {
        super(id, name, description, new HashMap<>(), owner, visibility, recVersion);
        this.system = system;

        Map<String, String> propertiesToModify = getPropertiesToModify();

        propertiesToModify.putAll(MapUtils.mapKeys(timeDefinition.getProperties(), SnapshotProperty::getValue));
        propertiesToModify.putAll(FundamentalMapper.toGroupProperties(fundamentalFilterSet));
    }

    private static Snapshot.InnerBuilder innerBuilder() {
        return new Snapshot.InnerBuilder();
    }

    @SuppressWarnings("squid:S00107") // too many parameters
    @lombok.Builder(builderClassName = "Builder")
    private static Snapshot userBuilder(String name, String description, String system,
                                        String owner, Visibility visibility,
                                        Set<FundamentalFilter> fundamentalFilterSet,
                                        TimeDefinition timeDefinition
    ) {
        return new Snapshot(Identifiable.NOT_SET, name, description, system, owner, visibility, Versionable.NOT_SET,
                fundamentalFilterSet, timeDefinition
        );
    }

    @SuppressWarnings("squid:S00107") // too many parameters
    @lombok.Builder(builderMethodName = "innerBuilder", builderClassName = "InnerBuilder")
    private static Snapshot innerBuilder(long id, String name, String description, String system,
                                         String owner, Visibility visibility, long recVersion,
                                         BeamModeData beamModeEnd,
                                         BeamModeData beamModeStart,
                                         Integer fillNumber,
                                         Integer dynamicDuration,
                                         Instant endTime,
                                         Set<FundamentalFilter> fundamentalFilterSet,
                                         PriorTime priorTime,
                                         Instant startTime,
                                         DynamicTimeUnit timeUnit,
                                         LoggingTimeZone timeZone,
                                         Boolean isEndTimeDynamic,
                                         TimeDefinition timeDefinition
    ) {
        return new Snapshot(id, name, description, system, owner, visibility, recVersion,
                beamModeEnd,
                beamModeStart,
                fillNumber,
                dynamicDuration,
                endTime,
                fundamentalFilterSet,
                priorTime,
                startTime,
                timeUnit,
                timeZone,
                isEndTimeDynamic,
                timeDefinition
        );
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class InnerBuilder extends Snapshot.Builder {
    }

    public Snapshot.Builder toBuilder() {
        return innerBuilder()
                .id(getId())
                .name(getName())
                .description(getDescription())
                .system(getSystem())
                .owner(getOwner())
                .visibility(getVisibility())
                .recVersion(getRecVersion())
                .beamModeEnd(getBeamModeEnd())
                .beamModeStart(getBeamModeStart())
                .fillNumber(getFillNumber())
                .dynamicDuration(getDynamicDuration())
                .endTime(getEndTime())
                .fundamentalFilterSet(getFundamentalFilterSet())
                .priorTime(getPriorTime())
                .startTime(getStartTime())
                .timeUnit(getTime())
                .timeZone(getTimeZone())
                .isEndTimeDynamic(isEndTimeDynamic())
                .timeDefinition(null);
    }

    public BeamModeData getBeamModeEnd() {
        return BeamModeData.fromJson(getProperties().get(SnapshotProperty.BEAM_MODE_END.getValue()));
    }

    public BeamModeData getBeamModeStart() {
        return BeamModeData.fromJson(getProperties().get(SnapshotProperty.BEAM_MODE_START.getValue()));
    }

    public Integer getFillNumber() {
        return getPropertyAsInteger(getProperties(), SnapshotProperty.FILL_NUMBER);
    }

    public Integer getDynamicDuration() {
        return getPropertyAsInteger(getProperties(), SnapshotProperty.DYNAMIC_DURATION);
    }

    public Instant getEndTime() {
        return getPropertyAsInstant(getProperties(), SnapshotProperty.END_TIME);
    }

    public Set<FundamentalFilter> getFundamentalFilterSet() {
        Map<String, String> fundamentalFilterProperties = new HashMap<>();

        getProperties().forEach((key, value) -> {
            if (key.startsWith(FundamentalMapper.FUNDAMENTAL_GROUP_PROPERTY_KEY_PREFIX)) {
                fundamentalFilterProperties.put(key, value);
            }
        });

        return FundamentalMapper.toFundamentals(fundamentalFilterProperties);
    }

    public PriorTime getPriorTime() {
        String value = getProperties().get(SnapshotProperty.PRIOR_TIME.getValue());
        return value == null ? null : PriorTime.from(value);
    }

    public Instant getStartTime() {
        return getPropertyAsInstant(getProperties(), SnapshotProperty.START_TIME);
    }

    public DynamicTimeUnit getTime() {
        String value = getProperties().get(SnapshotProperty.TIME.getValue());
        return value == null ? null : DynamicTimeUnit.valueOf(value.toUpperCase());
    }

    public LoggingTimeZone getTimeZone() {
        String value = getProperties().get(SnapshotProperty.TIME_ZONE.getValue());
        return value == null ? null : LoggingTimeZone.valueOf(value);
    }

    public Boolean isEndTimeDynamic() {
        String value = getProperties().get(SnapshotProperty.IS_END_TIME_DYNAMIC.getValue());
        return value == null ? null : Boolean.parseBoolean(value);
    }

    public TimeWindow calculateTimeWindow() {
        Integer fillNumber = this.getFillNumber();
        Boolean isEndTimeDynamic = this.isEndTimeDynamic();
        try {
            if (fillNumber != null) {
                return FillTimeDefinition.fromProperties(getProperties()).getTimeWindow();
            } else if (isEndTimeDynamic != null && isEndTimeDynamic) {
                return DynamicTimeDefinition.fromProperties(getProperties()).getTimeWindow();
            }

            return FixedTimeDefinition.fromProperties(getProperties()).getTimeWindow();
        } catch (RuntimeException ex) {
            throw new IllegalArgumentException(
                    String.format(
                            "Can't calculate time window for snapshot with id: %d, not enough information. Reason: ",
                            getId()), ex);
        }
    }

    private void setPropertyAsString(SnapshotProperty property, Object value) {
        getPropertiesToModify().put(property.getValue(), property.valueToJson(value));
    }

    private static Instant getPropertyAsInstant(Map<String, String> properties, SnapshotProperty propertyName) {
        String value = properties.get(propertyName.getValue());
        return value == null ? null : TimeUtils.getInstantFromString(value);
    }

    private static Integer getPropertyAsInteger(Map<String, String> properties, SnapshotProperty propertyName) {
        String value = properties.get(propertyName.getValue());
        return value == null ? null : Integer.parseInt(value);
    }

    public interface TimeDefinition {
        TimeWindow getTimeWindow();

        EnumMap<SnapshotProperty, String> getProperties();

        static TimeDefinition fixedDates(Instant startTime, Instant endTime) {
            return new FixedTimeDefinition(startTime, endTime);
        }

        static TimeDefinition forFill(int fillNumber, BeamModeData beamStart, BeamModeData beamEnd) {
            return new FillTimeDefinition(fillNumber, beamStart, beamEnd);
        }

        static TimeDefinition dynamic(int duration, LoggingTimeZone timezone, PriorTime priorTime,
                                      DynamicTimeUnit timeUnit) {

            return new DynamicTimeDefinition(timeUnit, timezone, priorTime, duration);
        }
    }

    @AllArgsConstructor
    private static class FixedTimeDefinition implements TimeDefinition {
        private final Instant startTime;
        private final Instant endTime;

        @Override
        public TimeWindow getTimeWindow() {
            return TimeWindow.between(startTime, endTime);
        }

        @Override
        public EnumMap<SnapshotProperty, String> getProperties() {
            EnumMap<SnapshotProperty, String> props = new EnumMap<>(SnapshotProperty.class);
            props.put(SnapshotProperty.FILL_NUMBER, null);
            props.put(SnapshotProperty.IS_END_TIME_DYNAMIC, null);

            props.put(SnapshotProperty.START_TIME, startTime.toString());
            props.put(SnapshotProperty.END_TIME, endTime.toString());
            return props;
        }

        static FixedTimeDefinition fromProperties(Map<String, String> properties) {
            return new FixedTimeDefinition(
                    Instant.parse(properties.get(SnapshotProperty.START_TIME.getValue())),
                    Instant.parse(properties.get(SnapshotProperty.END_TIME.getValue()))
            );
        }
    }

    @AllArgsConstructor
    private static class FillTimeDefinition implements TimeDefinition {
        private Integer fillNumber;
        private BeamModeData startBeam;
        private BeamModeData endBeam;

        @Override
        public TimeWindow getTimeWindow() {
            return TimeWindow.between(startBeam.getValidity().getStartTime(), endBeam.getValidity().getEndTime());
        }

        @Override
        public EnumMap<SnapshotProperty, String> getProperties() {
            EnumMap<SnapshotProperty, String> props = new EnumMap<>(SnapshotProperty.class);
            props.put(SnapshotProperty.IS_END_TIME_DYNAMIC, null);

            props.put(SnapshotProperty.BEAM_MODE_START, startBeam.toString());
            props.put(SnapshotProperty.BEAM_MODE_END, endBeam.toString());
            props.put(SnapshotProperty.FILL_NUMBER, fillNumber.toString());
            return props;
        }

        static FillTimeDefinition fromProperties(Map<String, String> properties) {
            return new FillTimeDefinition(
                    getPropertyAsInteger(properties, SnapshotProperty.FILL_NUMBER),
                    BeamModeData.fromJson(properties.get(SnapshotProperty.BEAM_MODE_START.getValue())),
                    BeamModeData.fromJson(properties.get(SnapshotProperty.BEAM_MODE_END.getValue()))
            );
        }
    }

    @AllArgsConstructor
    private static class DynamicTimeDefinition implements TimeDefinition {
        private DynamicTimeUnit timeUnit;
        private LoggingTimeZone timeZone;
        private PriorTime priorTime;
        private Integer duration;

        @Override
        public TimeWindow getTimeWindow() {
            if (timeUnit.equals(DynamicTimeUnit.BEAM_MODES) || timeUnit.equals(DynamicTimeUnit.FILLS)) {
                throw new IllegalArgumentException(
                        "getTime cannot be set to neither BEAM_MODES nor FILLS for dynamic time window calculation");
            }

            ZoneId zoneId = timeZone.getTimeZone().toZoneId();
            LocalDateTime refTime = LocalDateTime.now(zoneId);

            LocalDateTime t2 = priorTime.withRespectTo(refTime);
            LocalDateTime t1;

            switch (ChronoUnit.valueOf(timeUnit.name())) {
                case SECONDS:
                    t1 = t2.minusSeconds(duration);
                    break;
                case MINUTES:
                    t1 = t2.minusMinutes(duration);
                    break;
                case HOURS:
                    t1 = t2.minusHours(duration);
                    break;
                case DAYS:
                    t1 = t2.minusDays(duration);
                    break;
                case WEEKS:
                    t1 = t2.minusWeeks(duration);
                    break;
                case MONTHS:
                    t1 = t2.minusMonths(duration);
                    break;
                case YEARS:
                    t1 = t2.minusYears(duration);
                    break;
                default:
                    throw new IllegalArgumentException("Unsupported time unit: " + timeUnit);
            }

            return TimeWindow.between(TimeUtils.getNanosFromLocal(t1, zoneId), TimeUtils.getNanosFromLocal(t2, zoneId));
        }

        @Override
        public EnumMap<SnapshotProperty, String> getProperties() {
            EnumMap<SnapshotProperty, String> props = new EnumMap<>(SnapshotProperty.class);
            props.put(SnapshotProperty.FILL_NUMBER, null);

            props.put(SnapshotProperty.IS_END_TIME_DYNAMIC, Boolean.toString(true));
            props.put(SnapshotProperty.TIME, timeUnit.toString());
            props.put(SnapshotProperty.TIME_ZONE, timeZone.toString());
            props.put(SnapshotProperty.PRIOR_TIME, priorTime.toString());
            props.put(SnapshotProperty.DYNAMIC_DURATION, duration.toString());
            return props;
        }

        static DynamicTimeDefinition fromProperties(Map<String, String> properties) {
            return new DynamicTimeDefinition(
                    DynamicTimeUnit.valueOf(properties.get(SnapshotProperty.TIME.getValue())),
                    LoggingTimeZone.valueOf(properties.get(SnapshotProperty.TIME_ZONE.getValue())),
                    PriorTime.from(properties.get(SnapshotProperty.PRIOR_TIME.getValue())),
                    Integer.valueOf(properties.get(SnapshotProperty.DYNAMIC_DURATION.getValue()))
            );
        }
    }
}
