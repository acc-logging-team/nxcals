package cern.nxcals.api.custom.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * An enumeration of the well known (system default) Group properties
 */
@Getter
@AllArgsConstructor
@SuppressWarnings("squid:S00115") // incorrect name format
public enum GroupPropertyName {
    getChartType("Chart Type"),
    getCorrelationSettingFreq("Correlation Setting Freq"),
    getCorrelationSettingScale("Correlation Setting Scale"),
    getDataLocationPreferences("Data Source Preferences"),
    getDerivationSelection("Derivation Selection"),
    getDerivationTime("Derivation Time"),
    getDeviceName("Device Name"),
    getDrivingVariable("Driving Variable"),
    getDynamicDuration("Dynamic Duration"),
    getEndTime("End Time"),
    getFileFormat("File Format"),
    getFillsFiltersBeamModes("Fill filters beam modes"),
    getFundamentalFilters("Fundamental Filters"),
    getNoOfIntervalsForMultiFiles("No Of Intervals For Multi Files"),
    getPriorTime("Prior Time"),
    getPropertyName("Property Name"),
    getSelectedBeamModes("Selected beam modes to display"),
    getSelectedVariables("Selected Variables"),
    getSelectionOutput("Selection Output"),
    getStartTime("Start Time"),
    getTime("Time"),
    getTimeIntervalTypeForMultiFiles("Time Interval Type For Multi Files"),
    getTimeZone("Time Zone"),
    getTimescalingProperties("Timescaling Properties"),
    getXAxisVariable("X Axis Variable"),
    isDistributionChartIncluded("Distribution Chart Included"),
    isEndTimeDynamic("End Time Dynamic"),
    isGroupByTimestamp("Group By Timestamp"),
    isLastValueAligned("Last Value Aligned"),
    isLastValueSearchedIfNoDataFound("Last Value Searched If No Data Found"),
    isMetaDataIncluded("Meta Data Included"),
    isMultiFilesActivated("Multi Files Activated"),
    isSelectableTimestampsIncluded("Selectable Timestamps Included"),
    isAlignToWindow("Align To Window"),
    isAlignToEnd("Align To End"),
    isAlignToStart("Align to Start");

    private final String displayName;
}
