package cern.nxcals.api.custom.domain;

import lombok.experimental.UtilityClass;

@UtilityClass
public class CernSystemConstants {
    public static final String CERN_SYSTEM = "CERN";
}
