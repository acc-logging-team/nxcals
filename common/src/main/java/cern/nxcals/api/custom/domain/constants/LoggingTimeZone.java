package cern.nxcals.api.custom.domain.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.TimeZone;

@AllArgsConstructor
@Getter
public enum LoggingTimeZone {
    UTC_TIME(TimeZone.getTimeZone("GMT")),
    LOCAL_TIME(TimeZone.getDefault());

    public static final LoggingTimeZone UTC = UTC_TIME;
    public static final LoggingTimeZone LOCAL = LOCAL_TIME;

    private final TimeZone timeZone;
}
