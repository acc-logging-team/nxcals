package cern.nxcals.api.custom.domain;

import lombok.experimental.UtilityClass;

@UtilityClass
public class CmwSystemConstants {
    public static final String CMW_SYSTEM = "CMW";
    public static final String CLASS_KEY_NAME = "class";
    public static final String DEVICE_KEY_NAME = "device";
    public static final String PROPERTY_KEY_NAME = "property";
    public static final String ACQSTAMP = "acqStamp";
    public static final String CYCLESTAMP = "cyclestamp";
    public static final String SELECTOR = "selector";
    public static final String RECORD_TIMESTAMP = "__record_timestamp__";
    public static final String RECORD_VERSION = "__record_version__";
}
