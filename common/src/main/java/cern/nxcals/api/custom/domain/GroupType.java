package cern.nxcals.api.custom.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum GroupType {
    GROUP("GROUP"), SNAPSHOT("SNAPSHOT"), TAG("TAG");
    private String value;

    @Override
    public String toString() {
        return value;
    }
}
