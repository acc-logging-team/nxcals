package cern.nxcals.api.custom.domain.util;

import cern.nxcals.api.custom.domain.FundamentalFilter;
import cern.nxcals.common.utils.GlobalObjectMapperProvider;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.InjectableValues;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import lombok.NonNull;
import lombok.experimental.UtilityClass;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.io.UncheckedIOException;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@UtilityClass
public class FundamentalMapper {
    public static final String FUNDAMENTAL_GROUP_PROPERTY_KEY_PREFIX = "fundamentalFilter_";

    private static final Pattern FUNDAMENTAL_GROUP_PROPERTY_KEY_PREFIX_PATTERN =
            Pattern.compile(FUNDAMENTAL_GROUP_PROPERTY_KEY_PREFIX);

    private static final ObjectMapper OBJECT_MAPPER = GlobalObjectMapperProvider.get();

    public static Set<FundamentalFilter> toFundamentals(Map<String, String> groupProperties) {
        if (MapUtils.isEmpty(groupProperties)) {
            return Collections.emptySet();
        }
        return groupProperties.entrySet().stream()
                .filter(e -> e.getKey().startsWith(FUNDAMENTAL_GROUP_PROPERTY_KEY_PREFIX))
                .map(FundamentalMapper::toFilter).collect(Collectors.toSet());
    }

    public static Map<String, String> toGroupProperties(Set<FundamentalFilter> fundamentalFilters) {
        if (CollectionUtils.isEmpty(fundamentalFilters)) {
            return Collections.emptyMap();
        }
        return fundamentalFilters.stream().map(FundamentalMapper::toGroupProperty)
                .collect(Collectors.toMap(Pair::getKey, Pair::getValue));
    }

    private static FundamentalFilter toFilter(Map.Entry<String, String> filterPropertyEntry) {
        try {
            return getPropertyReaderFor(filterPropertyEntry.getKey()).readValue(filterPropertyEntry.getValue());
        } catch (JsonProcessingException e) {
            throw new UncheckedIOException("Error while deserializing filter with id: " +
                    filterPropertyEntry.getKey(), e);
        }
    }

    private static Pair<String, String> toGroupProperty(@NonNull FundamentalFilter filter) {
        try {
            String filterJson = OBJECT_MAPPER.writeValueAsString(filter);
            return ImmutablePair.of(FUNDAMENTAL_GROUP_PROPERTY_KEY_PREFIX + filter.getId(), filterJson);
        } catch (JsonProcessingException e) {
            throw new UncheckedIOException(String.format("Error while serializing filter [%s]", filter), e);
        }
    }

    private static ObjectReader getPropertyReaderFor(@NonNull String propertyName) {
        InjectableValues.Std injectableValues = new InjectableValues.Std()
                .addValue("id", extractIdFromPropertyName(propertyName));
        return OBJECT_MAPPER.reader(injectableValues).forType(FundamentalFilter.class);
    }

    private static String extractIdFromPropertyName(String propertyName) {
        String filterId = FUNDAMENTAL_GROUP_PROPERTY_KEY_PREFIX_PATTERN.matcher(propertyName).replaceFirst("").trim();
        if (StringUtils.isBlank(filterId)) {
            throw new IllegalStateException("Fundamental property name is missing valid id suffix!");
        }
        return filterId;
    }

}
