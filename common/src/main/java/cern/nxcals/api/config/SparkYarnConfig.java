package cern.nxcals.api.config;

import cern.nxcals.api.utils.YarnProperties;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import lombok.Getter;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;

import static java.util.stream.Collectors.toMap;

@Getter
public class SparkYarnConfig implements YarnProperties {
    private static final String NXCALS_OPTIONS = "nxcals_spark_options";
    private static Config config;
    private final Map<String, String> properties;

    public static Map<String, String> getConfig(String prefix) {
        Config config = getConfig();
        Config rawProps = config.getConfig(prefix);
        if (rawProps.isEmpty()) {
            throw new IllegalStateException("Cannot find spark properties");
        }
        return rawProps.entrySet().stream()
                .collect(toMap(Map.Entry::getKey, entry -> entry.getValue().unwrapped().toString()));
    }

    static Map<String, String> getConfigForEnv(String prefix) {
        Map<String, String> configMap = getConfig(NXCALS_OPTIONS);
        configMap.computeIfPresent("spark.yarn.jars", (k, v) -> v.replace("ENVIRONMENT",
                Objects.requireNonNullElse(prefix, "pro").toLowerCase()));
        return configMap;
    }

    SparkYarnConfig(Map<String, String> config) {
        this.properties = Collections.unmodifiableMap(config);
    }

    private static synchronized Config getConfig() {
        if (config == null) {
            ConfigFactory.invalidateCaches();
            config = ConfigFactory.load();
        }
        return config;
    }

}
