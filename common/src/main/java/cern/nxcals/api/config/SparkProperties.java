package cern.nxcals.api.config;

import cern.nxcals.api.utils.YarnProperties;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * Used to configure Spark from Spring.
 * 
 */
@Data
public class SparkProperties {

    private String appName;
    private String masterType;
    private String[] jars = new String[0];
    private Map<String, String> properties = new HashMap<>();

    public static SparkProperties defaults(String appName) {
        SparkProperties defaults = new SparkProperties();
        defaults.appName = appName;
        defaults.masterType = "local[*]";
        defaults.setProperty("spark.sql.caseSensitive", "true");
        return defaults;
    }

    public static SparkProperties remoteSessionProperties(String applicationName,
            YarnProperties flavor, String environment) {
        SparkProperties sparkProperties = new SparkProperties();
        sparkProperties.appName = applicationName;
        sparkProperties.masterType = "yarn";
        sparkProperties.getProperties().putAll(SparkYarnConfig.getConfigForEnv(environment));
        sparkProperties.getProperties().putAll(flavor.getProperties());
        return sparkProperties;
    }

    public void setProperty(String propertyKey, String value) {
        properties.put(propertyKey, value);
    }

}