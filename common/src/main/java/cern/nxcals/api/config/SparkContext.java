package cern.nxcals.api.config;

import cern.nxcals.api.utils.SparkUtils;
import cern.nxcals.common.config.SparkSessionModifier;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.SparkSession;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Import;

import java.util.List;

/**
 * A default Spark Spring config that can be reused.
 * Created by jwozniak on 05/04/17.
 */
@Configuration
@Import(AuthenticationContext.class)
public class SparkContext {

    /**
     * Creates Spark configuration
     * @param config Spark properties config
     * @return Configuration for a Spark application
     */
    @Bean
    public SparkConf createSparkConf(SparkProperties config) {
        return SparkUtils.createSparkConf(config);
    }

    /**
     * Creates Spark session
     * @param conf Configuration for a Spark application
     * @return The entry point to programming Spark with the Dataset and DataFrame API.
     */
    @Bean
    @DependsOn("kerberos")
    public SparkSession createSparkSession(SparkConf conf, List<SparkSessionModifier> sessionModifiers) {
        final SparkSession session = SparkUtils.createSparkSession(conf);
        SparkUtils.modifySparkSession(session, sessionModifiers);
        return session;
    }

    /**
     * Creates Spark properties config
     * from application name, master type, list of jars and list of properties
     * @return Spark properties config
     */
    @Bean
    @ConfigurationProperties(prefix = "spark")
    public SparkProperties createSparkPropertiesConfig() {
        return new SparkProperties();
    }

}
