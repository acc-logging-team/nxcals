package cern.nxcals.api.utils;

import cern.nxcals.common.utils.IllegalCharacterConverter;
import lombok.experimental.UtilityClass;

import java.util.StringJoiner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@UtilityClass
public class EncodingUtils {
    // match string surrounded by backquotes
    private final Pattern pattern = Pattern.compile("`([^`]*)`", Pattern.CASE_INSENSITIVE);

    private final IllegalCharacterConverter converter = IllegalCharacterConverter.get();

    /**
     * If path contains fields with special characters, then it will be encoded.
     * If field contain special character, then should be surrounded by '`' like it is in Spark.
     * So that to access field "nestedAndEncod.ed" in such structure:
     * A
     * |-B
     * |-nestedAndEncod.ed
     * in query must be typed as "A.B.`nestedAndEncod.ed`"
     *
     * @param fieldPath raw path with fields with special characters surrounded by '`' like "A.B.`nestedAndEncod.ed`"
     * @return path with encoded parts, which was surrounded by '`'
     */
    public static String encodeFields(String fieldPath) {
        Matcher matcher = pattern.matcher(fieldPath);
        while (matcher.find()) {
            fieldPath = fieldPath.replace(matcher.group(),
                    converter.convertToLegal(matcher.group(1)));
        }
        return fieldPath;
    }

    /**
     * Decode parts in fieldPath which are encoded
     *
     * @param fieldPath fieldPath (possibly nested) with encoded parts
     * @return fieldPath with decoded fields, decoded parts are surrounded by '`'
     */
    public static String decodeFields(String fieldPath) {
        if (fieldPath == null) {
            return null;
        }
        StringJoiner decodedFieldName = new StringJoiner(".");
        for (String pathPart : fieldPath.split("\\.")) {
            if (IllegalCharacterConverter.isEncoded(pathPart)) {
                decodedFieldName.add("`" + converter.convertFromLegal(pathPart) + "`");
            } else {
                decodedFieldName.add(pathPart);
            }
        }
        return decodedFieldName.toString();
    }

    /**
     * Decode field name if is encoded
     *
     * @param fieldName possible encoded field name
     * @return field name in decoded form
     */
    public static String desanitizeIfNeeded(String fieldName) {
        if (IllegalCharacterConverter.isEncoded(fieldName)) {
            return converter.convertFromLegal(fieldName);
        }
        return fieldName;
    }

    /**
     * Encode field name if contains not allowed characters
     *
     * @param fieldName field name to encode
     * @return encoded (if contained illegal characters) field name
     */
    public static String sanitizeIfNeeded(String fieldName) {
        if (IllegalCharacterConverter.isLegal(fieldName)) {
            return fieldName;
        }
        return converter.convertToLegal(fieldName);
    }
}
