package cern.nxcals.api.utils;

import cern.nxcals.api.config.SparkProperties;
import cern.nxcals.common.config.SparkSessionModifier;
import lombok.NonNull;
import lombok.experimental.UtilityClass;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.SparkSession;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Supplier;

@UtilityClass
public class SparkUtils {

    /**
     * Creates Spark configuration
     *
     * @param config Spark properties config
     * @return Configuration for a Spark application
     */
    public SparkConf createSparkConf(SparkProperties config) {
        SparkConf conf = new SparkConf().setAppName(config.getAppName()).setMaster(config.getMasterType());
        config.getProperties().forEach(conf::set);
        final String[] jars = config.getJars();
        if (jars != null && jars.length > 0) {
            conf.setJars(jars);
        }
        return conf;
    }

    /**
     * Creates Spark session with the given config
     *
     * @param config config of the session
     * @return spark session
     */
    public SparkSession createSparkSession(SparkConf config) {
        return SparkSession.builder().config(config).getOrCreate();
    }

    /**
     * Modifies Spark session according to the collection of modifiers
     *
     * @param session   session to modify
     * @param modifiers to apply to the session
     */
    public void modifySparkSession(SparkSession session, Collection<SparkSessionModifier> modifiers) {
        for (SparkSessionModifier modifier : modifiers) {
            modifier.accept(session);
        }
    }

    /**
     * Creates Spark session with the given properties
     *
     * @param properties properties of the session
     * @return spark session
     */
    public SparkSession createSparkSession(SparkProperties properties) {
        return SparkSession.builder().config(createSparkConf(properties)).getOrCreate();
    }

    /**
     * Creates Spark session supplier that is able to recreate and return a new instance of the SparkSession
     * in case the current one was closed.
     *
     * @param sparkConf        config of the session
     * @param sessionModifiers to apply to the session
     */
    public Supplier<SparkSession> createSparkSessionSupplier(@NonNull SparkConf sparkConf,
            @NonNull List<SparkSessionModifier> sessionModifiers) {
        return new AutoRecreatingSparkSessionSupplier(sparkConf, sessionModifiers);
    }

    /**
     * Creates Spark session supplier that is able to recreate and return a new instance of the SparkSession
     * in case the current one was closed.
     *
     * @param sparkConf config of the session
     */
    public Supplier<SparkSession> createSparkSessionSupplier(@NonNull SparkConf sparkConf) {
        return createSparkSessionSupplier(sparkConf, Collections.emptyList());
    }

    /**
     * Creates Spark session supplier that is able to recreate and return a new instance of the SparkSession
     * in case the current one was closed.
     *
     * @param sparkProperties Spark properties config
     */
    public Supplier<SparkSession> createSparkSessionSupplier(@NonNull SparkProperties sparkProperties) {
        return createSparkSessionSupplier(SparkUtils.createSparkConf(sparkProperties));
    }

}
