/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.utils;

import com.google.common.annotations.VisibleForTesting;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.UtilityClass;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE;
import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE_TIME;

@UtilityClass
public final class TimeUtils {

    public static final long CONVERT_EPOCH_NANOS_TO_SECONDS_VALUE = 1_000_000_000L;
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.n");
    private static final DateTimeFormatter FORMATTER_DATE = ISO_LOCAL_DATE;
    public static final long SECONDS_IN_DAY = 24 * 60 * 60L;
    public static final long NANOS_IN_DAY = SECONDS_IN_DAY * CONVERT_EPOCH_NANOS_TO_SECONDS_VALUE;
    //Number of 5 minutes periods in a day
    public static final long MILLIS_IN_HOUR = TimeUnit.HOURS.toMillis(1);

    /**
     * Represents various time splits used in NXCALS. Created to have type & time safety (like equal divisions by time
     * per day).
     */
    @Getter
    @RequiredArgsConstructor
    public enum TimeSplits {
        FIVE_MINUTE_SPLIT(SECONDS_IN_DAY / (5 * 60)),
        TEN_MINUTE_SPLIT(SECONDS_IN_DAY / (10 * 60)),
        FIFTEEN_MINUTE_SPLIT(SECONDS_IN_DAY / (15 * 60)),
        TWENTY_MINUTE_SPLIT(SECONDS_IN_DAY / (20 * 60)),
        THIRTY_MINUTE_SPLIT(SECONDS_IN_DAY / (30 * 60)),
        ONE_HOUR_SPLIT(24);
        private final long size;
    }

    public static long truncateToHours(long timestamp) {
        long millis = TimeUnit.NANOSECONDS.toMillis(timestamp);
        return millis - millis % MILLIS_IN_HOUR;
    }

    public static long getTimePartition(long timestamp, TimeSplits max) {
        return getTimePartitionInternal(timestamp, max.size);
    }

    @VisibleForTesting
    static long getTimePartitionInternal(long timestamp, long maxPartitions) {
        long intervalInNanos = TimeUtils.NANOS_IN_DAY / maxPartitions;
        return (timestamp % TimeUtils.NANOS_IN_DAY) // nanos since the beginning of the day
                / intervalInNanos; //dividing by interval length will get the partition number
    }


    /**
     * Converts a long with the number of nanoseconds since epoch into its {@link Instant} counterpart.
     *
     * @param timeInNanos the number of seconds since epoch.
     * @return a new {@link Instant}.
     */
    public static Instant getInstantFromNanos(long timeInNanos) {
        return Instant.ofEpochSecond(TimeUnit.NANOSECONDS.toSeconds(timeInNanos),
                (int) (timeInNanos % CONVERT_EPOCH_NANOS_TO_SECONDS_VALUE));
    }

    /**
     * Converts a long with the number of nanoseconds since epoch into Timestamp.
     *
     * @param timeInNanos
     * @return a new Timestamp
     */
    public static Timestamp getTimestampFromNanos(long timeInNanos) {
        return getTimestampFromInstant(getInstantFromNanos(timeInNanos));
    }

    /**
     * Converts an {@link Instant} into a long with the number of nanoseconds since epoch.
     *
     * @param instantTime the {@link Instant} to be converter.
     * @return a long with the number of nanoseconds since epoch.
     */
    public static long getNanosFromInstant(@NonNull Instant instantTime) {
        return TimeUnit.SECONDS.toNanos(instantTime.getEpochSecond()) + instantTime.getNano();
    }

    /**
     * Converts a {@link String} into a long with the number of nanoseconds since epoch.
     *
     * @param timeString a string with the following format: yyyy-MM-dd HH:mm:ss.nnnnnnnnn
     * @return a long with the number of nanoseconds since epoch.
     */
    public static long getNanosFromString(@NonNull String timeString) {
        return getNanosFromInstant(getInstantFromString(timeString));
    }

    /**
     * Converts an current instant into a long with the number of nanoseconds since epoch.
     *
     * @return a long with the number of nanoseconds since epoch.
     */
    public static long getNanosFromNow() {
        return getNanosFromInstant(ZonedDateTime.now(ZoneId.of("UTC")).toInstant());
    }

    public static Timestamp getTimestampFromInstant(@NonNull Instant instant) {
        return Timestamp.from(instant);
    }

    public static Timestamp getTimestampFromString(@NonNull String timeString) {
        return getTimestampFromInstant(getInstantFromString(timeString));
    }

    /**
     * Converts a {@link String} into its {@link Instant} counterpart.
     *
     * @param timeString a string with the following format: yyyy-MM-dd HH:mm:ss.nnnnnnnnn
     * @return a new instance of {@link Instant}.
     */
    public static Instant getInstantFromString(@NonNull String timeString) {
        return LocalDateTime.parse(timeString.replace(" ", "T"), ISO_LOCAL_DATE_TIME).toInstant(ZoneOffset.UTC);
    }

    /**
     * Converts an {@link Instant} into its {@link String} counterpart. It will use UTC timezone
     *
     * @param instant the instant to be converted.
     * @return a {@link String} with the following format: yyyy-MM-dd HH:mm:ss.nnnnnnnnn
     * @deprecated This method will be removed in a future release.
     * <p> Use {@link Instant#toString()} instead.
     */
    @Deprecated
    public static String getStringFromInstant(@NonNull Instant instant) {
        return FORMATTER.format(ZonedDateTime.ofInstant(instant, ZoneId.of("UTC")));
    }

    public static String getStringDateFromInstant(@NonNull Instant instant) {
        return FORMATTER_DATE.format(ZonedDateTime.ofInstant(instant, ZoneId.of("UTC")));
    }

    /**
     * @deprecated - Typo here, please use TimeUtils.getStringDateFromNanos()
     */
    @Deprecated
    public static String getStringDateFromNano(long timestampInNano) {
        return FORMATTER_DATE.format(ZonedDateTime.ofInstant(getInstantFromNanos(timestampInNano), ZoneId.of("UTC")));
    }

    public static String getStringDateFromNanos(long timestampInNanos) {
        return FORMATTER_DATE.format(ZonedDateTime.ofInstant(getInstantFromNanos(timestampInNanos), ZoneId.of("UTC")));
    }

    public static String getStringFromNanos(long timestampInNanos) {
        return FORMATTER.format(ZonedDateTime.ofInstant(getInstantFromNanos(timestampInNanos), ZoneId.of("UTC")));
    }

    public static long getMilisFromStringDate(@NonNull String timeString) {
        return LocalDate.parse(timeString, FORMATTER_DATE).atStartOfDay().toInstant(ZoneOffset.UTC).toEpochMilli();
    }

    public static long getNanosFromLocal(LocalDateTime localDateTime, ZoneId zoneId) {
        return TimeUtils.getNanosFromInstant(localDateTime.atZone(zoneId).toInstant());
    }
}
