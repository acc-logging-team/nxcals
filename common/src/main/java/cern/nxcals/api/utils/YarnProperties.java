package cern.nxcals.api.utils;

import java.util.Map;
@FunctionalInterface
public interface YarnProperties {
    Map<String, String> getProperties();
}
