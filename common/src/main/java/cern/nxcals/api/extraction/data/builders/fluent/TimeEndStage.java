package cern.nxcals.api.extraction.data.builders.fluent;

import cern.nxcals.api.utils.TimeUtils;
import lombok.NonNull;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;

import static com.google.common.base.Preconditions.checkArgument;

public class TimeEndStage<N extends Stage<?, T>, T> extends Stage<N, T> {
    TimeEndStage(N next) {
        super(next);
    }

    public N endTime(@NonNull Instant endTime) {
        data().setEndTime(endTime);
        return next();
    }

    /**
     * @param nanos end time in nanoseconds elapsed since 1970-01-01 00:00:00 in the UTC timezone
     */
    public N endTime(long nanos) {
        return endTime(TimeUtils.getInstantFromNanos(nanos));
    }

    /**
     * @param endTimeUtc end time in UTC timezone as string using "yyyy-MM-dd HH:mm:ss.n" format
     */
    public N endTime(@NonNull String endTimeUtc) {
        return endTime(TimeUtils.getInstantFromString(endTimeUtc));
    }

    public N duration(@NonNull Duration duration) {
        checkArgument(!duration.isNegative(), "Query duration must be positive.");
        return endTime(data().getStartTime().plus(duration));
    }

    /**
     * @param nanos duration time in nanoseconds
     */
    public N duration(long nanos) {
        return duration(nanos, ChronoUnit.NANOS);
    }

    public N duration(long amount, @NonNull TemporalUnit unit) {
        return duration(Duration.of(amount, unit));
    }
}