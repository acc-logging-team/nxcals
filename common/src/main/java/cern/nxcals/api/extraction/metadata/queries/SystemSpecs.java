package cern.nxcals.api.extraction.metadata.queries;

import com.github.rutledgepaulv.qbuilders.properties.concrete.LongProperty;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Query builder for SystemSpec meta-data.
 * Build your queries SystemSpecs.suchThat().name().eq("name") etc.
 * Please note that eq() operator also allows for 'like' queries with characters '%' and '_'.
 * You can use regexp with pattern() operator.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class SystemSpecs extends BaseBuilder<SystemSpecs> {
    public static SystemSpecs suchThat() {
        return new SystemSpecs();
    }

    public LongProperty<SystemSpecs> id() {
        return longNum("id");
    }

    public StringLikeProperty<SystemSpecs> name() {
        return stringLike("name");
    }
}