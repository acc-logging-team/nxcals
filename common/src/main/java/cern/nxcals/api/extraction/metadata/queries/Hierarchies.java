package cern.nxcals.api.extraction.metadata.queries;

import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.github.rutledgepaulv.qbuilders.properties.concrete.LongProperty;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/*
 * Query builder for Hierarchy meta-data.
 * Build your queries like Hierarchies.suchThat().name().eq("xxx") etc.
 * Please note that eq() operator also allows for 'like' queries with characters '%' and '_'.
 * You can use regexp with pattern() operator.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@SuppressWarnings("squid:CommentedOutCodeLine") // we must keep the comment
public final class Hierarchies extends BaseBuilder<Hierarchies> {
    public static Hierarchies suchThat() {
        return new Hierarchies();
    }

    public LongProperty<Hierarchies> id() {
        return longNum("id");
    }

    public LongProperty<Hierarchies> systemId() {
        return longNum("group.system.id");
    }

    public StringLikeProperty<Hierarchies> systemName() {
        return stringLike("group.system.name");
    }

    public StringLikeProperty<Hierarchies> name() {
        return stringLike("name");
    }

    public StringLikeProperty<Hierarchies> description() {
        return stringLike("group.description");
    }

    // TODO: figure this out
//
//    public StringLikeProperty<Hierarchies> variableName() {
//        return stringLike("group.variables.;
//    }

    public Condition<Hierarchies> areTopLevel() {
        return parentId().doesNotExist();
    }

    public LongProperty<Hierarchies> parentId() {
        return longNum("parentId");
    }

    public StringLikeProperty<Hierarchies> parentName() {
        return stringLike("parent.name");
    }

    public StringLikeProperty<Hierarchies> path() {
        return stringLike("viewData.nodePath");
    }
}