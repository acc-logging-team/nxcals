package cern.nxcals.api.extraction.data.builders.fluent;

public class KeyValueStageLoop<T> extends KeyValueStage<T> {
    KeyValueStageLoop(QueryData<T> data) {
        super(data);
    }

    /**
     * Start stage for query for certain entity. Example of usage:
     * <pre>
     * byEntities().entity().keyValues("Device", "device1").build();
     * </pre>
     *
     * @return stage for passing entity key values
     */
    public KeyValueStage<T> entity() {
        data().closeEntity();
        return new KeyValueStage<>(data());
    }

    /**
     * @return T
     * @deprecated - Please use build()
     */
    @Deprecated
    public T buildDataset() {
        return build();
    }

    public T build() {
        return data().build();
    }
}