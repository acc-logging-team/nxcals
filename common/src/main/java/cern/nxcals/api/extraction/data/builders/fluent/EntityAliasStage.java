package cern.nxcals.api.extraction.data.builders.fluent;

import lombok.NonNull;

import java.util.List;
import java.util.Map;

import static java.util.Objects.requireNonNull;

public class EntityAliasStage<N extends Stage<?, T>, T> extends EntityStage<N, T> {
    EntityAliasStage(N next) {
        super(next);
    }

    public EntityAliasStage<N, T> fieldAliases(@NonNull String alias, @NonNull List<String> fields) {
        data().addAlias(alias, fields);
        return this;
    }

    public EntityAliasStage<N, T> fieldAliases(@NonNull Map<String, List<String>> aliasMap) {
        requireNonNull(aliasMap).forEach((alias, fields) -> data().addAlias(alias, fields));
        return this;
    }

    public EntityAliasStage<N, T> fieldAlias(@NonNull String alias, @NonNull String field) {
        data().addAlias(alias, field);
        return this;
    }
}