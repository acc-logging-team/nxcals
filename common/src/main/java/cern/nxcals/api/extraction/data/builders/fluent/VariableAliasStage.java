package cern.nxcals.api.extraction.data.builders.fluent;

import lombok.NonNull;

import java.util.List;
import java.util.Map;

import static java.util.Objects.requireNonNull;

public class VariableAliasStage<T> extends VariableStage<T> {
    VariableAliasStage(QueryData<T> data) {
        super(data);
    }

    public VariableAliasStage<T> fieldAliases(@NonNull String alias, @NonNull List<String> fields) {
        data().addAlias(alias, fields);
        return new VariableAliasStage<>(data());
    }

    public VariableAliasStage<T> fieldAliases(@NonNull Map<String, List<String>> aliasMap) {
        requireNonNull(aliasMap).forEach((alias, fields) -> data().addAlias(alias, fields));
        return new VariableAliasStage<>(data());
    }

    public VariableAliasStage<T> fieldAlias(@NonNull String alias, @NonNull String field) {
        data().addAlias(alias, field);
        return new VariableAliasStage<>(data());
    }
}