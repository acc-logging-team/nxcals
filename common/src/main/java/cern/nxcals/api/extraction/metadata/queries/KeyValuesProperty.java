package cern.nxcals.api.extraction.metadata.queries;

import cern.nxcals.api.domain.SystemSpec;
import com.github.rutledgepaulv.qbuilders.builders.QBuilder;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;

import java.util.Map;

public interface KeyValuesProperty<T extends QBuilder<T>> extends StringLikeProperty<T> {
    Condition<T> eq(SystemSpec system, Map<String, Object> value);
    Condition<T> ne(SystemSpec system, Map<String, Object> value);
    Condition<T> in(SystemSpec system, Map<String, Object>... value);
    Condition<T> nin(SystemSpec system, Map<String, Object>... value);
    Condition<T> like(SystemSpec system, Map<String, Object> value);
    Condition<T> notLike(SystemSpec system, Map<String, Object> value);
}