package cern.nxcals.api.extraction.data.builders.fluent.v2;

import cern.nxcals.api.extraction.data.builders.fluent.QueryData;
import lombok.NonNull;

import java.time.Instant;

public class VariableStageLoop<T> extends VariableStage<T> implements TimeStage<BuildStage<T>> {
    VariableStageLoop(QueryData<T> data) {
        super(data);
    }

    public BuildStage<T> timeWindow(@NonNull Instant startTime, @NonNull Instant endTime) {
        data().setStartTime(startTime);
        data().setEndTime(endTime);
        return new BuildStage<>(data());
    }
}