package cern.nxcals.api.extraction.metadata.queries;

import cern.nxcals.common.qbuilders.CustomOperatorFactory;
import com.github.rutledgepaulv.qbuilders.builders.QBuilder;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.github.rutledgepaulv.qbuilders.delegates.virtual.PropertyDelegate;
import com.github.rutledgepaulv.qbuilders.properties.concrete.StringProperty;
import com.github.rutledgepaulv.qbuilders.structures.FieldPath;

import java.util.Collection;
import java.util.Collections;

import static java.util.Arrays.asList;

/**
 * This is a proper implementation of the like/notLike eq/ne operators that uses separate operators for them.
 * This way we are not using like for eq operator and we don't have to escape values.
 *
 * @param <T>
 */
class StringLikePropertyDelegate<T extends QBuilder<T>> extends PropertyDelegate<T> implements StringLikeProperty<T> {
    private final StringProperty<T> stringProperty;
    private final boolean caseSensitive;

    StringLikePropertyDelegate(FieldPath field, T canonical) {
        this(field, canonical, true);
    }

    StringLikePropertyDelegate(FieldPath field, T canonical, boolean caseSensitive) {
        super(field, canonical);
        this.caseSensitive = caseSensitive;
        this.stringProperty = string(field.toString());
    }

    @Override
    public StringLikeCaseInsensitiveProperty<T> caseInsensitive() {
        return new StringLikePropertyDelegate<>(getField(), self(), false);
    }

    @Override
    public Condition<T> eq(String value) {
        return condition(getField(), CustomOperatorFactory.stringEquals(caseSensitive), Collections.singletonList(value));
    }

    @Override
    public Condition<T> ne(String value) {
        return condition(getField(), CustomOperatorFactory.stringNotEquals(caseSensitive), Collections.singletonList(value));
    }

    @Override
    public Condition<T> like(String value) {
        return condition(getField(), CustomOperatorFactory.stringLike(caseSensitive), Collections.singletonList(value));
    }

    @Override
    public Condition<T> notLike(String value) {
        return condition(getField(), CustomOperatorFactory.stringNotLike(caseSensitive), Collections.singletonList(value));
    }

    @Override
    public Condition<T> lexicallyAfter(String value) {
        return stringProperty.lexicallyAfter(value);
    }

    @Override
    public Condition<T> lexicallyBefore(String value) {
        return stringProperty.lexicallyBefore(value);
    }

    @Override
    public Condition<T> lexicallyNotAfter(String value) {
        return stringProperty.lexicallyNotAfter(value);
    }

    @Override
    public Condition<T> lexicallyNotBefore(String value) {
        return stringProperty.lexicallyNotBefore(value);
    }

    @Override
    public Condition<T> pattern(String pattern) {
        return stringProperty.pattern(pattern);
    }

    @Override
    public Condition<T> exists() {
        return stringProperty.exists();
    }

    @Override
    public Condition<T> doesNotExist() {
        return stringProperty.doesNotExist();
    }

    @Override
    public Condition<T> in(String... values) {
        return in(asList(values));
    }

    @Override
    public Condition<T> in(Collection<String> values) {
        return condition(getField(), CustomOperatorFactory.stringIn(caseSensitive), values);
    }

    @Override
    public Condition<T> nin(String... values) {
        return nin(asList(values));
    }

    @Override
    public Condition<T> nin(Collection<String> values) {
        return condition(getField(), CustomOperatorFactory.stringNotIn(caseSensitive), values);
    }
}