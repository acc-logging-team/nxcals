/*
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.api.extraction.data.builders.fluent.v2;

import cern.nxcals.api.extraction.data.builders.fluent.QueryData;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.function.Function;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class StageSequenceDeviceProperty {
    public static <T> Function<QueryData<T>, SystemStage<DeviceStage<T>, T>> sequence() {
        return data -> new SystemStage<>(new DeviceStage<>(data));
    }
}