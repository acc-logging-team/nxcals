package cern.nxcals.api.extraction.metadata.queries;

import cern.nxcals.api.domain.OperationType;
import com.github.rutledgepaulv.qbuilders.properties.concrete.EnumProperty;
import com.github.rutledgepaulv.qbuilders.properties.concrete.LongProperty;
import com.github.rutledgepaulv.qbuilders.structures.FieldPath;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Query builder for HierarchyVariablesChangelog meta-data.
 * Build your queries HierarchyVariablesChangelogs.suchThat().newHierarchyName().eq("name") etc.
 * Please note that eq() operator also allows for 'like' queries with characters '%' and '_'.
 * You can use regexp with pattern() operator.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class HierarchyVariablesChangelogs extends BaseBuilder<HierarchyVariablesChangelogs> {

    public static HierarchyVariablesChangelogs suchThat() {
        return new HierarchyVariablesChangelogs();
    }

    public LongProperty<HierarchyVariablesChangelogs> id() {
        return longNum("id");
    }

    public LongProperty<HierarchyVariablesChangelogs> oldVariableId() {
        return longNum("oldVariableId");
    }

    public LongProperty<HierarchyVariablesChangelogs> newVariableId() {
        return longNum("newVariableId");
    }

    public LongProperty<HierarchyVariablesChangelogs> oldHierarchyId() {
        return longNum("oldHierarchyId");
    }

    public LongProperty<HierarchyVariablesChangelogs> newHierarchyId() {
        return longNum("newHierarchyId");
    }

    public LongProperty<HierarchyVariablesChangelogs> oldSystemId() {
        return longNum("oldSystemId");
    }

    public LongProperty<HierarchyVariablesChangelogs> newSystemId() {
        return longNum("newSystemId");
    }

    public EnumProperty<HierarchyVariablesChangelogs, OperationType> opType() {
        return enumeration("opType");
    }

    public InstantPropertyDelegate<HierarchyVariablesChangelogs> creationTimeUtc() {
        return new InstantPropertyDelegate<>(new FieldPath("createTimeUtc"), self());
    }

    public StringLikeProperty<HierarchyVariablesChangelogs> clientInfo() {
        return stringLike("clientInfo");
    }

}
