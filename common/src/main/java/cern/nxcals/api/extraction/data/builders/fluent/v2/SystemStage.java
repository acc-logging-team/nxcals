package cern.nxcals.api.extraction.data.builders.fluent.v2;

import cern.nxcals.api.extraction.data.builders.fluent.Stage;
import lombok.NonNull;

public class SystemStage<N extends Stage<?, T>, T> extends Stage<N, T> {
    SystemStage(N next) {
        super(next);
    }

    public N system(@NonNull String system) {
        data().setSystem(system);
        return next();
    }
}
