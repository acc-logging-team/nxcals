package cern.nxcals.api.extraction.metadata.queries;

public interface EntityFields<S, L, K> {
    L longField(String fieldName);

    S stringLike(String fieldName);

    K entityKeyValues(String fieldName);

    K partitionKeyValues(String fieldName);

    default L id() {
        return longField("id");
    }

    /**
     * @deprecated this method will be removed in a future release. Please use keyValues()
     */
    @Deprecated
    default S keyValuesString() {
        return stringLike("keyValues");
    }

    default L systemId() {
        return longField("partition.system.id");
    }

    default S systemName() {
        return stringLike("partition.system.name");
    }

    /**
     * @deprecated this method will be removed in a future release. Please use partitionKeyValues()
     */
    @Deprecated
    default S partitionKeyValuesString() {
        return stringLike("partition.keyValues");
    }

    default K keyValues() {
        return entityKeyValues("keyValues");
    }

    default K partitionKeyValues() {
        return partitionKeyValues("partition.keyValues");
    }
}
