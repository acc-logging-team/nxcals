package cern.nxcals.api.extraction.data.builders.fluent.v2;

import cern.nxcals.api.extraction.data.builders.fluent.Stage;

import java.util.Set;

public class SystemOrIdStage<N extends Stage<?, T> & IdStage<N1>, N1 extends N, T> extends SystemStage<N, T>
        implements IdStage<SystemOrIdStageLoop<N1, T>> {

    SystemOrIdStage(N next) {
        super(next);
    }

    @Override
    public SystemOrIdStageLoop<N1, T> idIn(Set<Long> ids) {
        return new SystemOrIdStageLoop<>(next().idIn(ids));
    }
}
