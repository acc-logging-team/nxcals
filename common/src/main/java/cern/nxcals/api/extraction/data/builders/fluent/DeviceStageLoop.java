package cern.nxcals.api.extraction.data.builders.fluent;

public class DeviceStageLoop<T> extends Stage<DeviceStageLoop<T>, T> {
    DeviceStageLoop(QueryData<T> data) {
        super(data);
    }

    /**
     * Start stage for query for certain entity. Example of usage:
     * <pre>
     * byEntities().entity().device("DEV").property("PROP").entity().device("DEV2").property("PROP2").build();
     * </pre>
     *
     * @return stage for passing parameters or device/properties
     */
    public DeviceStage<T> entity() {
        data().closeEntity();
        return new DeviceStage<>(data());
    }

    /**
     * @return T
     * @deprecated - Please use build()
     */
    @Deprecated
    public T buildDataset() {
        return build();
    }

    public T build() {
        return data().build();
    }
}