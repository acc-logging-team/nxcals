/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.data.builders.fluent;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.EntityHistory;
import cern.nxcals.api.domain.Partition;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.common.domain.EntityKeyValue;
import cern.nxcals.common.domain.EntityKeyValues;
import cern.nxcals.common.domain.ExtractionCriteria;
import com.google.common.annotations.VisibleForTesting;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import javax.annotation.concurrent.NotThreadSafe;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Objects.requireNonNull;

@NotThreadSafe
@Getter
@RequiredArgsConstructor
@ToString
@EqualsAndHashCode
public class QueryData<T> {
    @VisibleForTesting
    public static final String DEVICE_KEY = "device";
    @VisibleForTesting
    public static final String PROPERTY_KEY = "property";
    @VisibleForTesting
    static final String ENTITIES_AND_VARIABLES_ARE_EMPTY_MSG = "Please define some entities or variables to query data for";
    @VisibleForTesting
    static final String END_BEFORE_START_MSG = "Start time (%s) cannot be after end time (%s)";
    private final Function<QueryData<T>, T> finalizer;
    private final Map<String, Boolean> variables = new HashMap<>();
    private final Set<Long> variableIds = new HashSet<>();
    private final List<EntityKeyValues> entities = new ArrayList<>(10);
    private final Set<Long> entityIds = new HashSet<>();
    // alias, list<fields>
    private final Map<String, Set<String>> aliasFields = new HashMap<>();
    /**
     * System information as provided by the query author in order to fetch resources
     * This property is valid only for the current (active) state of the matching resources!
     * If your intention is to collect information about the resource(s) history, consult the associated
     * {@link SystemSpec} property on {@link Entity} or {@link Partition} via corresponding {@link EntityHistory}
     */
    private String system;
    private Instant startTime;
    private Instant endTime;
    private EntityKeyValues.Builder ongoingEntityBuilder = EntityKeyValues.builder();

    public void setVariableKey(@NonNull String key, boolean enableWildcard) {
        variables.putIfAbsent(key, enableWildcard);
    }

    public void addVariableId(long id) {
        variableIds.add(id);
    }

    public void setSystem(@NonNull String system) {
        this.system = system;
    }

    public void setStartTime(@NonNull Instant startTime) {
        this.startTime = startTime;
    }

    public void setEndTime(@NonNull Instant endTime) {
        this.endTime = endTime;
    }

    public void addToEntity(@NonNull String key, @NonNull Object value, boolean wildcard) {
        ongoingEntityBuilder.keyValue(EntityKeyValue.builder().key(key).value(value).wildcard(wildcard).build());
    }

    public void addEntityId(long id) {
        entityIds.add(id);
    }

    public void addDevice(String device, boolean wildcard) {
        addToEntity(DEVICE_KEY, device, wildcard);
    }

    public void addProperty(String property, boolean wildcard) {
        addToEntity(PROPERTY_KEY, property, wildcard);
    }

    public void closeEntity() {
        EntityKeyValues entityKeyValues = ongoingEntityBuilder.build();
        if (!entityKeyValues.isEmpty()) {
            entities.add(entityKeyValues);
        }
        ongoingEntityBuilder = EntityKeyValues.builder();
    }

    public void addAlias(@NonNull String alias, @NonNull Collection<String> fields) {
        checkArgument(fields.stream().noneMatch(Objects::isNull), "Aliased field cannot be null");
        aliasFields.computeIfAbsent(alias, k -> new LinkedHashSet<>()).addAll(fields);
    }

    public void addAlias(@NonNull String alias, @NonNull String field) {
        this.addAlias(alias, Collections.singletonList(field));
    }

    public boolean isVariableSearch() {
        return !variables.isEmpty() || !variableIds.isEmpty();
    }

    public TimeWindow getTimeWindow() {
        return TimeWindow.between(startTime, endTime);
    }

    public ExtractionCriteria toExtractionCriteria() {
        return ExtractionCriteria.builder().systemName(system).variables(variables).variableIds(variableIds)
                .entities(entities).entityIds(entityIds).timeWindow(getTimeWindow())
                .aliasFields(getAliasFieldsWithFieldsAsList()).build();
    }

    private Map<String, List<String>> getAliasFieldsWithFieldsAsList() {
        return aliasFields.entrySet().stream().collect(Collectors.toMap(
                Map.Entry::getKey,
                entry -> new ArrayList<>(entry.getValue())
        ));
    }

    private void validate() {
        if (!idQuery()) {
            requireNonNull(system, "System cannot be null!");
        }
        requireNonNull(startTime, "StartTime cannot be null!");
        requireNonNull(endTime, "EndTime cannot be null!");
        if (this.entities.isEmpty() && this.entityIds.isEmpty() && this.variables.isEmpty()
                && this.variableIds.isEmpty()) {
            throw new IllegalArgumentException(ENTITIES_AND_VARIABLES_ARE_EMPTY_MSG);
        }
        if (startTime.isAfter(endTime)) {
            throw new IllegalArgumentException(String.format(END_BEFORE_START_MSG, startTime, endTime));
        }
    }

    private boolean idQuery() {
        return (!entityIds.isEmpty() || !variableIds.isEmpty()) && entities.isEmpty() && variables.isEmpty();
    }

    public T build() {
        closeEntity();
        validate();
        return finalizer.apply(this);
    }
}
