package cern.nxcals.api.extraction.metadata.queries;

import cern.nxcals.api.domain.Visibility;
import com.github.rutledgepaulv.qbuilders.properties.concrete.EnumProperty;
import com.github.rutledgepaulv.qbuilders.properties.concrete.LongProperty;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Query builder for Groups meta-data.
 * Build your queries like Groups.suchThat().systemName().eq("CMW") etc.
 * Please note that eq() operator also allows for 'like' queries with characters '%' and '_'.
 * You can use regexp with pattern() operator.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Groups extends BaseBuilder<Groups> {

    public static Groups suchThat() {
        return new Groups();
    }

    public LongProperty<Groups> id() {
        return longNum("id");
    }

    public LongProperty<Groups> systemId() {
        return longNum("system.id");
    }

    public StringLikeProperty<Groups> label() {
        return stringLike("label");
    }

    public EnumProperty<Groups, Visibility> visibility() {
        return enumeration("visibility");
    }

    public StringLikeProperty<Groups> systemName() {
        return stringLike("system.name");
    }

    public StringLikeProperty<Groups> name() {
        return stringLike("name");
    }

    public StringLikeProperty<Groups> description() {
        return stringLike("description");
    }


    public StringLikeProperty<Groups> variableName() {
        return stringLike("variables.variable.variableName");
    }

    public KeyValuesProperty<Groups> entityKeyValues() {
        return entityKeyValues("entities.entity.keyValues");
    }

    /**
     * @deprecated this method will be removed in a future release. Please use entityKeyValues()
     */
    @Deprecated
    public StringLikeProperty<Groups> entityKeyValuesString() {
        return stringLike("entities.entity.keyValues");
    }

    public StringLikeProperty<Groups> ownerName() {
        return stringLike("owner");
    }
}
