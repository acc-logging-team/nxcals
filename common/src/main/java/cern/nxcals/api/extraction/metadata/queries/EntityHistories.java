package cern.nxcals.api.extraction.metadata.queries;

import com.github.rutledgepaulv.qbuilders.properties.concrete.InstantProperty;
import com.github.rutledgepaulv.qbuilders.properties.concrete.LongProperty;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Query builder for EntityHistory meta-data.
 * Build your queries like EntityHistories.suchThat().id().eq(10) etc.
 * Please note that eq() operator also allows for 'like' queries with characters '%' and '_'.
 * You can use regexp with pattern() operator.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class EntityHistories extends BaseBuilder<EntityHistories> {
    public static EntityHistories suchThat() {
        return new EntityHistories();
    }

    public LongProperty<EntityHistories> id() {
        return longNum("id");
    }
    public LongProperty<EntityHistories> entityId() {
        return longNum("entity.id");
    }
    public LongProperty<EntityHistories> partitionId() {
        return longNum("partition.id");
    }
    public LongProperty<EntityHistories> systemId() {
        return longNum("system.id");
    }

    public InstantProperty<EntityHistories> validFromStamp() { return instant("validFromStamp");}
    public InstantProperty<EntityHistories> validToStamp() { return instant("validToStamp");}
}