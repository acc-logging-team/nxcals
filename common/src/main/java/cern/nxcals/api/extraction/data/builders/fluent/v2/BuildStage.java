package cern.nxcals.api.extraction.data.builders.fluent.v2;

import cern.nxcals.api.extraction.data.builders.fluent.QueryData;
import cern.nxcals.api.extraction.data.builders.fluent.Stage;
import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.Set;

public class BuildStage<T> extends Stage<BuildStage<T>, T> implements AliasStage<BuildStage<T>> {

    public BuildStage<T> fieldAliases(@NotNull Map<String, Set<String>> aliasFieldsMap) {
        aliasFieldsMap.forEach((alias, fields) -> data().addAlias(alias, fields));
        return this;
    }

    BuildStage(QueryData<T> data) {
        super(data);
    }

    public T build() {
        return data().build();
    }

}
