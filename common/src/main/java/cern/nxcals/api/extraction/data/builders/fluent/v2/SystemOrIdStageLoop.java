package cern.nxcals.api.extraction.data.builders.fluent.v2;

import cern.nxcals.api.extraction.data.builders.fluent.Stage;
import lombok.NonNull;

import java.time.Instant;

public class SystemOrIdStageLoop<N extends Stage<?, T> & IdStage<N>, T> extends SystemOrIdStage<N, N, T>
        implements TimeStage<BuildStage<T>> {

    SystemOrIdStageLoop(N next) {
        super(next);
    }

    public BuildStage<T> timeWindow(@NonNull Instant startTime, @NonNull Instant endTime) {
        data().setStartTime(startTime);
        data().setEndTime(endTime);
        return new BuildStage<>(data());
    }
}
