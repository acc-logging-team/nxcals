package cern.nxcals.api.extraction.data.builders.fluent.v2;

import com.google.common.collect.ImmutableSet;

import java.util.Set;

public interface IdStage<S> {
    S idIn(Set<Long> ids);

    /**
     * Search for given id
     *
     * @param id id
     * @return next query stage
     */
    default S idEq(long id) {
        return idIn(ImmutableSet.of(id));
    }

    /**
     * Search for given ids
     *
     * @param ids ids
     * @return next query stage
     */
    default S idIn(Long... ids) {
        return idIn(ImmutableSet.copyOf(ids));
    }
}
