package cern.nxcals.api.extraction.metadata.queries;

import cern.nxcals.api.domain.SystemSpec;
import com.github.rutledgepaulv.qbuilders.builders.QBuilder;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.github.rutledgepaulv.qbuilders.structures.FieldPath;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static cern.nxcals.common.utils.KeyValuesUtils.convertMapIntoAvroSchemaString;

/**
 * Custom operator accepting Map of keyValues. We convert the map to avro string using the given SystemSpec.
 * @param <T>
 */
class KeyValuesPropertyDelegate<T extends QBuilder<T>> extends StringLikePropertyDelegate<T>
        implements KeyValuesProperty<T> {
    private Function<SystemSpec, String> toString;

    KeyValuesPropertyDelegate(Function<SystemSpec, String> systemToKeyValues, FieldPath field, T canonical) {
        super(field, canonical);
        this.toString = systemToKeyValues;
    }

    @Override
    public final Condition<T> eq(SystemSpec system, Map<String, Object> value) {
        return eq(asString(value, system));
    }

    @Override
    public final Condition<T> ne(SystemSpec system, Map<String, Object> value) {
        return ne(asString(value, system));
    }

    @SafeVarargs
    @Override
    public final Condition<T> in(SystemSpec system, Map<String, Object>... value) {
        return in(Arrays.stream(value).map(v -> asString(v, system)).collect(Collectors.toList()));
    }

    @SafeVarargs
    @Override
    public final Condition<T> nin(SystemSpec system, Map<String, Object>... value) {
        return nin(Arrays.stream(value).map(v -> asString(v, system)).collect(Collectors.toList()));
    }

    @Override
    public final Condition<T> like(SystemSpec system, Map<String, Object> value) {
        return like(asString(value, system));
    }

    @Override
    public final Condition<T> notLike(SystemSpec system, Map<String, Object> value) {
        return notLike(asString(value, system));
    }

    private String asString(Map<String, Object> value, SystemSpec system) {
        return convertMapIntoAvroSchemaString(value, toString.apply(system));
    }
}