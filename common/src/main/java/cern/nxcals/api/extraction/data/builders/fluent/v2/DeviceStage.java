package cern.nxcals.api.extraction.data.builders.fluent.v2;

import cern.nxcals.api.extraction.data.builders.fluent.QueryData;
import cern.nxcals.api.extraction.data.builders.fluent.Stage;
import com.google.common.base.Preconditions;
import lombok.NonNull;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DeviceStage<T> extends Stage<DeviceStageLoop<T>, T> {

    private static final String ILLEGAL_FORMAT_ERROR = "Illegal parameter name, expected <device>/<property> got %s";

    private static final Pattern PARAMETER_PATTERN = Pattern.compile("^([.a-zA-Z0-9_-]+)/([.a-zA-Z0-9_-]+)$");
    private static final Pattern PARAMETER_PATTERN_WITH_WILDCARD = Pattern
            .compile("^([%.a-zA-Z0-9\\\\_-]+)/([%.a-zA-Z0-9\\\\_-]+)$");

    DeviceStage(QueryData<T> data) {
        super(data);
    }

    /**
     * Search for given parameter. Parameter should be in form "device/property".
     *
     * @param parameter parameter in form device/property
     * @return next device query stage
     */
    public DeviceStageLoop<T> parameterEq(@NonNull String parameter) {
        extractDeviceProperty(parameter, PARAMETER_PATTERN, false);
        return new DeviceStageLoop<>(data());
    }

    /**
     * Search for given parameters. Every parameter should be in form "device/property".
     *
     * @param parameters list of parameters in form device/property
     * @return next device query stage
     */
    public DeviceStageLoop<T> parameterIn(@NonNull List<String> parameters) {
        parameters.forEach(parameter -> extractDeviceProperty(parameter, PARAMETER_PATTERN, false));
        return new DeviceStageLoop<>(data());
    }

    /**
     * Search for given parameters. Every parameter should be in form "device/property".
     *
     * @param parameters list of parameters in form device/property
     * @return next device query stage
     */
    public DeviceStageLoop<T> parameterIn(String... parameters) {
        Arrays.stream(parameters).forEach(
                parameter -> extractDeviceProperty(parameter, PARAMETER_PATTERN, false));
        return new DeviceStageLoop<>(data());
    }

    /**
     * Search for given parameters, which match given pattern. Pattern should be in form "device_pattern/property_pattern"
     * and in Oracle SQL form (% and _ as wildcards).
     *
     * @param pattern parameter pattern in form device pattern/property pattern
     * @return next device query stage
     */
    public DeviceStageLoop<T> parameterLike(@NonNull String pattern) {
        extractDeviceProperty(pattern, PARAMETER_PATTERN_WITH_WILDCARD, true);
        return new DeviceStageLoop<>(data());
    }

    /**
     * Allow to query for entities from CMW by device-property names.
     *
     * @param device device name. Equivalent to keyValues(Map.of("device", device, "property", property))
     * @return stage to specify property
     */
    public PropertyStage<T> deviceEq(@NonNull String device) {
        data().addDevice(device, false);
        return new PropertyStage<>(data());
    }

    /**
     * Allow to query for entities from CMW by device-property names. Device pattern should be in Oracle SQL form (% and _ as wildcards).
     *
     * @param pattern pattern for device name
     * @return stage to specify property
     */
    public PropertyStage<T> deviceLike(@NonNull String pattern) {
        data().addDevice(pattern, true);
        return new PropertyStage<>(data());
    }

    private void extractDeviceProperty(String parameter, Pattern pattern, boolean wildcard) {
        Matcher matcher = pattern.matcher(parameter);
        Preconditions.checkArgument(matcher.matches(), String.format(ILLEGAL_FORMAT_ERROR, parameter));
        data().addDevice(matcher.group(1), wildcard);
        data().addProperty(matcher.group(2), wildcard);
        data().closeEntity();
    }
}
