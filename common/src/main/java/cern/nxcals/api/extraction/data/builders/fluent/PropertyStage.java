package cern.nxcals.api.extraction.data.builders.fluent;

import lombok.NonNull;

public class PropertyStage<T> extends Stage<DeviceStageLoop<T>, T> {
    PropertyStage(QueryData<T> data) {
        super(data);
    }

    /**
     * Specify the property.
     *
     * @param property property name
     * @return builder with query for given device
     */
    public DeviceStageLoop<T> property(@NonNull String property) {
        data().addProperty(property, false);
        return new DeviceStageLoop<>(data());
    }

    /**
     * Specify the property pattern. Pattern should be in Oracle SQL form (% and _ as wildcards).
     *
     * @param pattern property pattern
     * @return builder with query for given device
     */
    public DeviceStageLoop<T> propertyLike(@NonNull String pattern) {
        data().addProperty(pattern, true);
        return new DeviceStageLoop<>(data());
    }
}