package cern.nxcals.api.extraction.metadata.queries;

import cern.nxcals.api.domain.OperationType;
import com.github.rutledgepaulv.qbuilders.properties.concrete.EnumProperty;
import com.github.rutledgepaulv.qbuilders.properties.concrete.LongProperty;
import com.github.rutledgepaulv.qbuilders.structures.FieldPath;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Query builder for EntityChangelog meta-data.
 * Build your queries EntityChangelogs.suchThat().id().eq(0) etc.
 * Please note that eq() operator also allows for 'like' queries with characters '%' and '_'.
 * You can use regexp with pattern() operator.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class EntityChangelogs extends BaseBuilder<EntityChangelogs> {

    public static EntityChangelogs suchThat() {
        return new EntityChangelogs();
    }

    public LongProperty<EntityChangelogs> id() {
        return longNum("id");
    }

    public LongProperty<EntityChangelogs> entityId() {
        return longNum("entityId");
    }

    public KeyValuesProperty<EntityChangelogs> oldKeyValues() {
        return entityKeyValues("oldKeyValues");
    }

    public KeyValuesProperty<EntityChangelogs> newKeyValues() {
        return entityKeyValues("newKeyValues");
    }

    public LongProperty<EntityChangelogs> oldPartitionId() {
        return longNum("oldPartitionId");
    }

    public LongProperty<EntityChangelogs> newPartitionId() {
        return longNum("newPartitionId");
    }

    public LongProperty<EntityChangelogs> oldSystemId() {
        return longNum("oldSystemId");
    }

    public LongProperty<EntityChangelogs> newSystemId() {
        return longNum("newSystemId");
    }

    public StringLikeProperty<EntityChangelogs> clientInfo() { return stringLike("clientInfo"); }

    public InstantPropertyDelegate<EntityChangelogs> oldLockedUntilStamp() {
        return new InstantPropertyDelegate<>(new FieldPath("oldLockedUntilStamp"), self());
    }

    public InstantPropertyDelegate<EntityChangelogs> newLockedUntilStamp() {
        return new InstantPropertyDelegate<>(new FieldPath("newLockedUntilStamp"), self());
    }

    public InstantPropertyDelegate<EntityChangelogs> creationTimeUtc() {
        return new InstantPropertyDelegate<>(new FieldPath("createTimeUtc"), self());
    }

    public EnumProperty<EntityChangelogs, OperationType> opType() {
        return enumeration("opType");
    }
}