package cern.nxcals.api.extraction.data.builders.fluent.v2;

import cern.nxcals.api.extraction.data.builders.fluent.QueryData;
import cern.nxcals.api.extraction.data.builders.fluent.Stage;
import lombok.NonNull;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class VariableStage<T> extends Stage<VariableStageLoop<T>, T> implements IdStage<VariableStageLoop<T>> {
    VariableStage(QueryData<T> data) {
        super(data);
    }

    /**
     * Search for variable with given name.
     *
     * @param name variable name
     * @return next query builder stage
     */
    public VariableStageLoop<T> nameEq(@NonNull String name) {
        data().setVariableKey(name, false);
        return new VariableStageLoop<>(data());
    }

    /**
     * Search for variables with given names.
     *
     * @param names list of variable names
     * @return next query builder stage
     */
    public VariableStageLoop<T> nameIn(@NonNull List<String> names) {
        names.forEach(name -> data().setVariableKey(name, false));
        return new VariableStageLoop<>(data());
    }

    /**
     * Search for variables with given names.
     *
     * @param names list of variable names
     * @return next query builder stage
     */
    public VariableStageLoop<T> nameIn(String... names) {
        Arrays.stream(names).forEach(name -> data().setVariableKey(name, false));
        return new VariableStageLoop<>(data());
    }

    /**
     * Search for variables, which names match to pattern. Patterns should be in Oracle SQL form (% and _ as wildcards).
     *
     * @param pattern variable name pattern
     * @return next query builder stage
     */
    public VariableStageLoop<T> nameLike(@NonNull String pattern) {
        data().setVariableKey(pattern, true);
        return new VariableStageLoop<>(data());
    }

    /**
     * Search for variables with ids in set
     *
     * @param ids set with variable ids
     * @return variable query stage
     */
    public VariableStageLoop<T> idIn(@NonNull Set<Long> ids) {
        ids.forEach(data()::addVariableId);
        return new VariableStageLoop<>(data());
    }
}