package cern.nxcals.api.extraction.data.builders.fluent;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NonNull;

/**
 * @param <N> next stage type
 * @param <T> result type
 */
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Stage<N extends Stage<?, T>, T> {
    private final N next;
    @NonNull
    private final QueryData<T> data;

    protected Stage(@NonNull N next) {
        this(next, next.data());
    }

    protected Stage(QueryData<T> data) {
        this(null, data);
    }

    protected QueryData<T> data() {
        return data;
    }

    protected N next() {
        return next;
    }
}