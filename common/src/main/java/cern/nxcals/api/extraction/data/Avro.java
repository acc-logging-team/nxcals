package cern.nxcals.api.extraction.data;

import cern.nxcals.api.extraction.thin.AvroData;
import lombok.NonNull;
import lombok.experimental.UtilityClass;
import org.apache.avro.Schema;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.file.SeekableByteArrayInput;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericRecord;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Converter from {@link AvroData} to list of {@link GenericRecord} and {@link Schema}.
 */
@UtilityClass
public class Avro {
    public List<GenericRecord> records(@NonNull byte[] bytes) {
        GenericDatumReader<GenericRecord> datum = new GenericDatumReader<>();
        try (DataFileReader<GenericRecord> reader = new DataFileReader<>(
                new SeekableByteArrayInput(bytes), datum)) {
            ArrayList<GenericRecord> records = new ArrayList<>();
            while (reader.hasNext()) {
                records.add(reader.next());
            }
            return records;
        } catch (IOException e) {
            throw new IllegalArgumentException("Cannot read bytes", e);
        }
    }

    public List<GenericRecord> records(@NonNull AvroData response) {
        return records(response.getAvroBytes().toByteArray());
    }

    public Schema schema(@NonNull AvroData response) {
        return new Schema.Parser().parse(response.getAvroSchema());
    }
}
