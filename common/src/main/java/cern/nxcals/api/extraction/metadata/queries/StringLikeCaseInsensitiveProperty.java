package cern.nxcals.api.extraction.metadata.queries;

import com.github.rutledgepaulv.qbuilders.builders.QBuilder;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.github.rutledgepaulv.qbuilders.properties.virtual.EquitableProperty;
import com.github.rutledgepaulv.qbuilders.properties.virtual.ListableProperty;

public interface StringLikeCaseInsensitiveProperty<T extends QBuilder<T>> extends EquitableProperty<T, String>, ListableProperty<T, String> {
    Condition<T> like(String value);
    Condition<T> notLike(String value);
}
