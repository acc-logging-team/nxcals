package cern.nxcals.api.extraction.metadata.queries;

import cern.nxcals.api.domain.SystemSpec;
import com.github.rutledgepaulv.qbuilders.builders.QBuilder;
import com.github.rutledgepaulv.qbuilders.structures.FieldPath;

public abstract class BaseBuilder<T extends QBuilder<T>> extends QBuilder<T> {
    protected StringLikeProperty<T> stringLike(String field) {
        return new StringLikePropertyDelegate<>(new FieldPath(field), self());
    }

    protected KeyValuesProperty<T> entityKeyValues(String field) {
        return new KeyValuesPropertyDelegate<>(SystemSpec::getEntityKeyDefinitions, new FieldPath(field), self());
    }

    protected KeyValuesProperty<T> partitionKeyValues(String field) {
        return new KeyValuesPropertyDelegate<>(SystemSpec::getPartitionKeyDefinitions, new FieldPath(field), self());
    }
}
