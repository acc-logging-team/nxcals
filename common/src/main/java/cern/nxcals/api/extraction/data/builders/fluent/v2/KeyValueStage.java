package cern.nxcals.api.extraction.data.builders.fluent.v2;

import cern.nxcals.api.extraction.data.builders.fluent.QueryData;
import cern.nxcals.api.extraction.data.builders.fluent.Stage;
import lombok.NonNull;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class KeyValueStage<T> extends Stage<KeyValueStageLoop<T>, T> implements IdStage<KeyValueStageLoop<T>> {
    KeyValueStage(QueryData<T> data) {
        super(data);
    }

    /**
     * Search for entity, which contains given key-values
     *
     * @param keyValues key and values which entity must contain
     * @return entity query stage
     */
    public KeyValueStageLoop<T> keyValuesEq(@NonNull Map<String, Object> keyValues) {
        keyValues.forEach((key, value) -> data().addToEntity(key, value, false));
        data().closeEntity();
        return new KeyValueStageLoop<>(data());
    }

    /**
     * Search for entities, which contains given key-values
     *
     * @param keyValues list with keys and values which entity must contain
     * @return entity query stage
     */
    public KeyValueStageLoop<T> keyValuesIn(@NonNull List<Map<String, Object>> keyValues) {
        keyValues.forEach(this::keyValuesEq);
        return new KeyValueStageLoop<>(data());
    }

    /**
     * Search for entities, which contains given key-values
     *
     * @param keyValues key and values which entity must contain
     * @return entity query stage
     */
    @SafeVarargs
    public final KeyValueStageLoop<T> keyValuesIn(Map<String, Object>... keyValues) {
        Arrays.stream(keyValues).forEach(this::keyValuesEq);
        return new KeyValueStageLoop<>(data());
    }

    /**
     * Search for entity, which contains key-values, which match given patterns. Pattern should be in Oracle SQL form (% and _ as wildcards).
     *
     * @param keyValues key and values patterns, which entity must contain
     * @return entity query stage
     */
    public KeyValueStageLoop<T> keyValuesLike(@NonNull Map<String, Object> keyValues) {
        keyValues.forEach((key, value) -> data().addToEntity(key, value, true));
        data().closeEntity();
        return new KeyValueStageLoop<>(data());
    }

    /**
     * Search for entities with ids in set
     *
     * @param ids set with entity ids
     * @return entity query stage
     */
    public KeyValueStageLoop<T> idIn(@NonNull Set<Long> ids) {
        ids.forEach(data()::addEntityId);
        return new KeyValueStageLoop<>(data());
    }
}
