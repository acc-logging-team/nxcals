package cern.nxcals.api.extraction.metadata.queries;

import cern.nxcals.api.domain.VariableDeclaredType;
import com.github.rutledgepaulv.qbuilders.delegates.concrete.InstantPropertyDelegate;
import com.github.rutledgepaulv.qbuilders.properties.concrete.EnumProperty;
import com.github.rutledgepaulv.qbuilders.properties.concrete.InstantProperty;
import com.github.rutledgepaulv.qbuilders.properties.concrete.LongProperty;
import com.github.rutledgepaulv.qbuilders.structures.FieldPath;

/**
 * Query builder for Variable meta-data.
 * Build your queries Variables.suchThat().variableName().eq("name") etc.
 * Please note that eq() operator also allows for 'like' queries with characters '%' and '_'.
 * You can use regexp with pattern() operator.
 */
public final class Variables
        extends BaseBuilder<Variables>
        implements VariableFields<
        StringLikeProperty<Variables>,
        InstantProperty<Variables>,
        LongProperty<Variables>,
        EnumProperty<Variables, VariableDeclaredType>,
        KeyValuesProperty<Variables>> {
    public static Variables suchThat() {
        return new Variables();
    }

    private Variables() {
        super();
    }

    @Override
    public LongProperty<Variables> longField(String fieldName) {
        return super.longNum(fieldName);
    }

    @Override
    public StringLikeProperty<Variables> stringLike(String fieldName) {
        return super.stringLike(fieldName);
    }

    @Override
    public InstantProperty<Variables> instantField(String fieldName) {
        return new InstantPropertyDelegate<>(new FieldPath(fieldName), this);
    }

    @Override
    public KeyValuesProperty<Variables> entityKeyValues(String fieldName) {
        return super.entityKeyValues(fieldName);
    }

    @Override
    public EnumProperty<Variables, VariableDeclaredType> enumerationField(String fieldName) {
        return super.enumeration(fieldName);
    }
}
