package cern.nxcals.api.extraction.data.builders.fluent;

public class VariableStageLoop<T> extends VariableStage<T> {
    VariableStageLoop(QueryData<T> data) {
        super(data);
    }

    /**
     * @return T
     * @deprecated - Please use build()
     */
    @Deprecated
    public T buildDataset() {
        return build();
    }

    public T build() {
        return data().build();
    }
}