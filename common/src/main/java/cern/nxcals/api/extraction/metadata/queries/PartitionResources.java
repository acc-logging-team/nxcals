package cern.nxcals.api.extraction.metadata.queries;

import com.github.rutledgepaulv.qbuilders.properties.concrete.LongProperty;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class PartitionResources extends BaseBuilder<PartitionResources> {
    public static PartitionResources suchThat() {
        return new PartitionResources();
    }

    public LongProperty<PartitionResources> id() {
        return longNum("id");
    }

    public LongProperty<PartitionResources> systemId() {
        return longNum("system.id");
    }

    public LongProperty<PartitionResources> partitionId() {
        return longNum("partition.id");
    }

    public LongProperty<PartitionResources> schemaId() { return longNum("schema.id"); }
}
