package cern.nxcals.api.extraction.metadata.queries;

import cern.nxcals.api.domain.OperationType;
import com.github.rutledgepaulv.qbuilders.properties.concrete.EnumProperty;
import com.github.rutledgepaulv.qbuilders.properties.concrete.LongProperty;
import com.github.rutledgepaulv.qbuilders.structures.FieldPath;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Query builder for VariableChangelog meta-data.
 * Build your queries VariableChangelogs.suchThat().newVariableName().eq("name") etc.
 * Please note that eq() operator also allows for 'like' queries with characters '%' and '_'.
 * You can use regexp with pattern() operator.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class VariableChangelogs extends BaseBuilder<VariableChangelogs> {
    public static VariableChangelogs suchThat() {
        return new VariableChangelogs();
    }

    public InstantPropertyDelegate<VariableChangelogs> creationTimeUtc() {
        return new InstantPropertyDelegate<>(new FieldPath("createTimeUtc"), self());
    }

    public StringLikeProperty<VariableChangelogs> newVariableName() {
        return stringLike("newVariableName");
    }

    public StringLikeProperty<VariableChangelogs> oldVariableName() {
        return stringLike("oldVariableName");
    }

    public StringLikeProperty<VariableChangelogs> oldUnit() {
        return stringLike("oldUnit");
    }

    public StringLikeProperty<VariableChangelogs> newUnit() {
        return stringLike("newUnit");
    }

    public StringLikeProperty<VariableChangelogs> clientInfo() { return stringLike("clientInfo"); }

    public StringLikeProperty<VariableChangelogs> newDescription() {
        return stringLike("newDescription");
    }

    public StringLikeProperty<VariableChangelogs> oldDescription() {
        return stringLike("oldDescription");
    }

    public LongProperty<VariableChangelogs> variableId() {
        return longNum("variableId");
    }

    public LongProperty<VariableChangelogs> id() {
        return longNum("id");
    }

    public LongProperty<VariableChangelogs> oldSystemId() {
        return longNum("oldSystemId");
    }

    public LongProperty<VariableChangelogs> newSystemId() {
        return longNum("newSystemId");
    }

    public EnumProperty<VariableChangelogs, OperationType> opType() {
        return enumeration("opType");
    }

}