package cern.nxcals.api.extraction.data.builders.fluent;

import cern.nxcals.api.utils.TimeUtils;
import lombok.NonNull;

import java.time.Instant;

public class TimeStartStage<N extends Stage<?, T>, T> extends Stage<N, T> {
    TimeStartStage(N next) {
        super(next);
    }

    public TimeEndStage<N, T> startTime(@NonNull Instant startTime) {
        data().setStartTime(startTime);
        return new TimeEndStage<>(next());
    }

    /**
     * @param nanos start time in nanoseconds elapsed since 1970-01-01 00:00:00 in the UTC timezone
     */
    public TimeEndStage<N, T> startTime(long nanos) {
        return startTime(TimeUtils.getInstantFromNanos(nanos));
    }

    /**
     * @param startTimeUtc start time in UTC timezone as string using "yyyy-MM-dd HH:mm:ss.n" format
     */
    public TimeEndStage<N, T> startTime(@NonNull String startTimeUtc) {
        return startTime(TimeUtils.getInstantFromString(startTimeUtc));
    }

    public N atTime(@NonNull Instant timeStamp) {
        return startTime(timeStamp).duration(0);
    }

    /**
     * @param nanos start time in nanoseconds elapsed since 1970-01-01 00:00:00 in the UTC timezone
     */
    public N atTime(long nanos) {
        return atTime(TimeUtils.getInstantFromNanos(nanos));
    }

    /**
     * @param timeStampUtc at time in UTC timezone as string using "yyyy-MM-dd HH:mm:ss.n" format
     */
    public N atTime(@NonNull String timeStampUtc) {
        return atTime(TimeUtils.getInstantFromString(timeStampUtc));
    }
}