package cern.nxcals.api.extraction.metadata.queries;

import cern.nxcals.api.domain.OperationType;
import com.github.rutledgepaulv.qbuilders.properties.concrete.EnumProperty;
import com.github.rutledgepaulv.qbuilders.properties.concrete.LongProperty;
import com.github.rutledgepaulv.qbuilders.structures.FieldPath;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Query builder for VariableConfigChangelog meta-data.
 * Build your queries VariableConfigChangelogs.suchThat().newFieldName().eq("name") etc.
 * Please note that eq() operator also allows for 'like' queries with characters '%' and '_'.
 * You can use regexp with pattern() operator.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class VariableConfigChangelogs extends BaseBuilder<VariableConfigChangelogs> {
    public static VariableConfigChangelogs suchThat() {
        return new VariableConfigChangelogs();
    }

    public LongProperty<VariableConfigChangelogs> id() {
        return longNum("id");
    }

    public InstantPropertyDelegate<VariableConfigChangelogs> creationTimeUtc() {
        return new InstantPropertyDelegate<>(new FieldPath("createTimeUtc"), self());
    }

    public LongProperty<VariableConfigChangelogs> variableConfigId() {
        return longNum("variableConfigId");
    }

    public LongProperty<VariableConfigChangelogs> oldVariableId() {
        return longNum("oldVariableId");
    }

    public LongProperty<VariableConfigChangelogs> newVariableId() {
        return longNum("newVariableId");
    }

    public LongProperty<VariableConfigChangelogs> oldEntityId() {
        return longNum("oldEntityId");
    }

    public LongProperty<VariableConfigChangelogs> newEntityId() {
        return longNum("newEntityId");
    }

    public StringLikeProperty<VariableConfigChangelogs> oldFieldName() {
        return stringLike("oldFieldName");
    }

    public StringLikeProperty<VariableConfigChangelogs> newFieldName() {
        return stringLike("newFieldName");
    }

    public StringLikeProperty<VariableConfigChangelogs> clientInfo() { return stringLike("clientInfo"); }

    public InstantPropertyDelegate<VariableConfigChangelogs> oldValidFromStamp() {
        return new InstantPropertyDelegate<>(new FieldPath("oldValidFromStamp"), self());
    }

    public InstantPropertyDelegate<VariableConfigChangelogs> newValidFromStamp() {
        return new InstantPropertyDelegate<>(new FieldPath("newValidFromStamp"), self());
    }

    public InstantPropertyDelegate<VariableConfigChangelogs> oldValidToStamp() {
        return new InstantPropertyDelegate<>(new FieldPath("oldValidToStamp"), self());
    }

    public InstantPropertyDelegate<VariableConfigChangelogs> newValidToStamp() {
        return new InstantPropertyDelegate<>(new FieldPath("newValidToStamp"), self());
    }

    public EnumProperty<VariableConfigChangelogs, OperationType> opType() {
        return enumeration("opType");
    }
}
