package cern.nxcals.api.extraction.data.builders.fluent;

public class EntityStage<N extends Stage<?, T>, T> extends Stage<N, T> {

    EntityStage(N next) {
        super(next);
    }

    /**
     * Start stage for query for certain entity. Example of usage:
     * <pre>
     * byEntities().entity().keyValues("Device", "device1").build();
     * </pre>
     *
     * @return stage for passing entity key values
     */
    public N entity() {
        return next();
    }
}
