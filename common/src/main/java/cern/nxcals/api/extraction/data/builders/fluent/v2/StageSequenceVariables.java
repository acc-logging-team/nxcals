
/*
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.api.extraction.data.builders.fluent.v2;

import cern.nxcals.api.extraction.data.builders.fluent.QueryData;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.function.Function;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class StageSequenceVariables {
    public static <T> Function<QueryData<T>, SystemOrIdStage<VariableStage<T>, VariableStageLoop<T>, T>> sequence() {
        return data -> new SystemOrIdStage<>(new VariableStage<>(data));
    }
}
