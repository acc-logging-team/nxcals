package cern.nxcals.api.metadata.qbuilders.properties;

import com.github.rutledgepaulv.qbuilders.builders.QBuilder;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.github.rutledgepaulv.qbuilders.properties.concrete.InstantProperty;

import java.time.Duration;
import java.time.Instant;

public interface GenericExtendedInstantProperty<T extends QBuilder<T>, C extends Condition<T>>
        extends InstantProperty<T> {
    C before(Instant dateTime, boolean exclusive);

    C after(Instant dateTime, boolean exclusive);

    C between(Instant after, boolean exclusiveAfter, Instant before, boolean exclusiveBefore);

    C exists();

    C doesNotExist();

    C eq(Instant value);

    C ne(Instant value);

    C notOlderThan(Duration value);
}
