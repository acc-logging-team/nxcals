package cern.nxcals.api.metadata.qbuilders.properties;

import com.github.rutledgepaulv.qbuilders.builders.QBuilder;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.github.rutledgepaulv.qbuilders.properties.concrete.EnumProperty;

import java.util.Collection;

public interface GenericEnumProperty<T extends QBuilder<T>, E extends Enum<E>, C extends Condition<T>>
        extends EnumProperty<T, E> {
    C in(E... values);

    C in(Collection<E> values);

    C nin(E... values);

    C nin(Collection<E> values);

    C eq(E value);

    C ne(E value);

    C exists();

    C doesNotExist();
}
