package cern.nxcals.api.metadata.queries;

class OrderOptions<T extends QueryOptions<?>> {

    private final T returnedBuilder;

    protected OrderOptions(T returnedBuilder) {
        this.returnedBuilder = returnedBuilder;
    }

    public class OrderAction {
        public T desc() {
            returnedBuilder.setAscending(false);
            return returnedBuilder;
        }

        public T asc() {
            returnedBuilder.setAscending(true);
            return returnedBuilder;
        }
    }

    protected OrderAction orderBy(String field) {
        returnedBuilder.setOrderByField(field);
        return new OrderAction();
    }

    public OrderAction longField(String fieldName) {
        return orderBy(fieldName);
    }

    public OrderAction stringLike(String fieldName) {
        return orderBy(fieldName);
    }

    public OrderAction entityKeyValues(String fieldName) {
        return orderBy(fieldName);
    }

    public OrderAction partitionKeyValues(String fieldName) {
        return orderBy(fieldName);
    }

    public OrderAction instantField(String fieldName) {
        return orderBy(fieldName);
    }

    public OrderAction enumerationField(String fieldName) {
        return orderBy(fieldName);
    }
}
