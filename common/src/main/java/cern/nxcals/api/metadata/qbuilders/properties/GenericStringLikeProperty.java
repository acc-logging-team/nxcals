package cern.nxcals.api.metadata.qbuilders.properties;

import cern.nxcals.api.extraction.metadata.queries.StringLikeProperty;
import com.github.rutledgepaulv.qbuilders.builders.QBuilder;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.github.rutledgepaulv.qbuilders.properties.concrete.StringProperty;

public interface GenericStringLikeProperty<T extends QBuilder<T>, C extends Condition<T>>
        extends StringLikeProperty<T>, GenericStringLikeCaseInsensitiveProperty<T, C>, StringProperty<T> {
    GenericStringLikeCaseInsensitiveProperty<T, C> caseInsensitive();
}
