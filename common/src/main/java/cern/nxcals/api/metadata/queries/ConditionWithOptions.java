package cern.nxcals.api.metadata.queries;

import cern.nxcals.common.annotation.Experimental;
import com.github.rutledgepaulv.qbuilders.builders.QBuilder;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.github.rutledgepaulv.qbuilders.delegates.virtual.Delegate;
import com.github.rutledgepaulv.qbuilders.visitors.ContextualNodeVisitor;
import com.google.common.annotations.VisibleForTesting;
import lombok.AccessLevel;
import lombok.Getter;

import java.util.function.Function;

@Experimental
public class ConditionWithOptions<T extends QBuilder<T>, O> extends Delegate<T> implements Condition<T> {
    @VisibleForTesting
    @Getter(AccessLevel.PROTECTED)
    private final Condition<T> condition;
    private final Function<Condition<T>, O> optionsConstructor;

    public ConditionWithOptions(Condition<T> condition, T canonical, Function<Condition<T>, O> optionsConstructor) {
        super(canonical);
        this.condition = condition;
        this.optionsConstructor = optionsConstructor;
    }

    @Override
    public T and() {
        return condition.and();
    }

    @Override
    public T or() {
        return condition.or();
    }

    @Override
    public <Q> Q query(ContextualNodeVisitor<Q, Void> visitor) {
        return condition.query(visitor);
    }

    @Override
    public <Q, S> Q query(ContextualNodeVisitor<Q, S> visitor, S context) {
        return condition.query(visitor, context);
    }

    @Experimental
    public O withOptions() {
        return optionsConstructor.apply(condition);
    }
}
