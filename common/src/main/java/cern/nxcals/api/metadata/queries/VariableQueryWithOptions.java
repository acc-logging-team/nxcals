package cern.nxcals.api.metadata.queries;

import cern.nxcals.common.annotation.Experimental;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Experimental
@Getter
@EqualsAndHashCode(callSuper = true)
@ToString
@NoArgsConstructor(access = AccessLevel.PUBLIC)
public final class VariableQueryWithOptions extends QueryOptions<VariableQueryWithOptions> {
    @EqualsAndHashCode.Exclude
    @JsonIgnore
    private Condition<Variables> condition;

    public VariableQueryWithOptions(Condition<Variables> condition) {
        this.condition = condition;
    }

    public enum VariableQueryType {
        WITH_VARIABLE_CONFIGS, WITHOUT_VARIABLE_CONFIGS
    }

    private VariableQueryType queryType = VariableQueryType.WITH_VARIABLE_CONFIGS;

    @Override
    protected VariableQueryWithOptions self() {
        return this;
    }

    public VariableQueryWithOptions noConfigs() {
        queryType = VariableQueryType.WITHOUT_VARIABLE_CONFIGS;
        return this;
    }

    public static VariableQueryWithOptions fromJson(String json) throws JsonProcessingException {
        return mapper.readValue(json, VariableQueryWithOptions.class);
    }

    public VariableOrderOptions orderBy() {
        return new VariableOrderOptions(this);
    }

    public static VariableQueryWithOptions getDefaultInstance() {
        return new VariableQueryWithOptions();
    }
}
