package cern.nxcals.api.metadata.queries;

import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.metadata.qbuilders.properties.GenericEnumProperty;
import cern.nxcals.api.metadata.qbuilders.properties.GenericExtendedInstantProperty;
import cern.nxcals.api.metadata.qbuilders.properties.GenericKeyValuesProperty;
import cern.nxcals.api.metadata.qbuilders.properties.GenericLongProperty;
import cern.nxcals.api.metadata.qbuilders.properties.GenericStringLikeProperty;
import cern.nxcals.common.qbuilders.delegates.GenericEnumPropertyDelegate;
import cern.nxcals.common.qbuilders.delegates.GenericExtendedInstantPropertyDelegate;
import cern.nxcals.common.qbuilders.delegates.GenericKeyValuesPropertyDelegate;
import cern.nxcals.common.qbuilders.delegates.GenericLongPropertyDelegate;
import cern.nxcals.common.qbuilders.delegates.GenericStringLikePropertyDelegate;
import com.github.rutledgepaulv.qbuilders.builders.QBuilder;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.github.rutledgepaulv.qbuilders.structures.FieldPath;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

import static com.google.common.collect.Lists.asList;

@RequiredArgsConstructor
public abstract class ExtendedBuilder<T extends QBuilder<T>, C extends Condition<T>> extends QBuilder<T> {
    private final BiFunction<Condition<T>, T, C> conditionWrapper;

    protected Function<Condition<T>, C> getConditionWrapper() {
        return condition -> conditionWrapper.apply(condition, self());
    }

    public final GenericStringLikeProperty<T, C> stringLike(String field) {
        return new GenericStringLikePropertyDelegate<>(new FieldPath(field), self(), getConditionWrapper());
    }

    public final GenericKeyValuesProperty<T, C> entityKeyValues(String field) {
        return new GenericKeyValuesPropertyDelegate<>(SystemSpec::getEntityKeyDefinitions, new FieldPath(field), self(),
                getConditionWrapper());
    }

    public final GenericKeyValuesProperty<T, C> partitionKeyValues(String field) {
        return new GenericKeyValuesPropertyDelegate<>(SystemSpec::getPartitionKeyDefinitions, new FieldPath(field),
                self(),
                getConditionWrapper()
        );
    }

    public final GenericLongProperty<T, C> longField(String field) {
        return new GenericLongPropertyDelegate<>(new FieldPath(field), self(), getConditionWrapper());
    }

    public final GenericExtendedInstantProperty<T, C> instantField(String field) {
        return new GenericExtendedInstantPropertyDelegate<>(new FieldPath(field), self(), getConditionWrapper());
    }

    public final <E extends Enum<E>> GenericEnumProperty<T, E, C> genericEnumeration(String fieldName) {
        return new GenericEnumPropertyDelegate<>(new FieldPath(fieldName), self(), getConditionWrapper());
    }

    @SafeVarargs
    @Override
    public final C and(Condition<T> c1, Condition<T> c2, Condition<T>... conditions) {
        return and(asList(c1, c2, conditions));
    }

    @Override
    public C and(List<Condition<T>> conditions) {
        return conditionWrapper.apply(super.and(conditions), self());
    }

    @SafeVarargs
    @Override
    public final C or(Condition<T> c1, Condition<T> c2, Condition<T>... conditions) {
        return or(asList(c1, c2, conditions));
    }

    @Override
    public C or(List<Condition<T>> conditions) {
        return conditionWrapper.apply(super.or(conditions), self());
    }
}
