package cern.nxcals.api.metadata.qbuilders.properties;

import cern.nxcals.api.extraction.metadata.queries.StringLikeCaseInsensitiveProperty;
import com.github.rutledgepaulv.qbuilders.builders.QBuilder;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;

public interface GenericStringLikeCaseInsensitiveProperty<T extends QBuilder<T>, C extends Condition<T>>
        extends StringLikeCaseInsensitiveProperty<T> {
    C like(String value);

    C notLike(String value);

    C eq(String value);

    C ne(String value);

    C exists();

    C doesNotExist();
}
