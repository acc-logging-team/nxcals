package cern.nxcals.api.metadata.qbuilders.properties;

import com.github.rutledgepaulv.qbuilders.builders.QBuilder;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.github.rutledgepaulv.qbuilders.properties.concrete.LongProperty;

import java.util.Collection;

public interface GenericLongProperty<T extends QBuilder<T>, C extends Condition<T>>
        extends LongProperty<T> {
    C gt(Long number);

    C lt(Long number);

    C gte(Long number);

    C lte(Long number);

    C in(Long... values);

    C in(Collection<Long> values);

    C nin(Long... values);

    C nin(Collection<Long> values);

    C eq(Long value);

    C ne(Long value);

    C exists();

    C doesNotExist();
}
