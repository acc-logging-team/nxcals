package cern.nxcals.api.metadata.queries;

import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.common.annotation.Experimental;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.Instant;

@Getter
@EqualsAndHashCode(callSuper = true)
@ToString
@NoArgsConstructor(access = AccessLevel.PUBLIC)
public final class EntityQueryWithOptions extends QueryOptions<EntityQueryWithOptions> {
    @JsonIgnore
    @EqualsAndHashCode.Exclude
    private Condition<Entities> condition;
    private EntityQueryWithOptions.EntityQueryType queryType = EntityQueryType.WITH_LATEST_HISTORY;
    private long historyStartTime = 0;
    private long historyEndTime = 0;

    public enum EntityQueryType {
        WITH_LATEST_HISTORY, WITH_HISTORY, WITHOUT_HISTORY
    }

    public EntityQueryWithOptions(Condition<Entities> condition) {
        this.condition = condition;
    }

    @Override
    protected EntityQueryWithOptions self() {
        return this;
    }

    @Experimental
    public EntityOrderOptions orderBy() {
        return new EntityOrderOptions(this);
    }

    public static EntityQueryWithOptions fromJson(String json) throws JsonProcessingException {
        return mapper.readValue(json, EntityQueryWithOptions.class);
    }

    /**
     * Allows to specify time range, for which entity's history should be queried.
     *
     * @param historyStartTime begin of time range, from which entity's history entries should be added to entities
     * @param historyEndTime   end of time range, from which entity's history entries should be added to entities
     * @return {@link EntityQueryWithOptions}
     */
    @Experimental
    public EntityQueryWithOptions withHistory(long historyStartTime, long historyEndTime) {
        this.historyStartTime = historyStartTime;
        this.historyEndTime = historyEndTime;
        this.queryType = EntityQueryType.WITH_HISTORY;
        return this;
    }

    @Experimental
    public EntityQueryWithOptions withHistory(String historyStartTime, String historyEndTime) {
        return withHistory(TimeUtils.getNanosFromString(historyStartTime),
                TimeUtils.getNanosFromString(historyEndTime));
    }

    @Experimental
    public EntityQueryWithOptions withHistory(Instant historyStartTime, Instant historyEndTime) {
        return withHistory(
                TimeUtils.getNanosFromInstant(historyStartTime),
                TimeUtils.getNanosFromInstant(historyEndTime));
    }

    @Experimental
    public EntityQueryWithOptions noHistory() {
        this.queryType = EntityQueryType.WITHOUT_HISTORY;
        return this;
    }

    public static EntityQueryWithOptions getDefaultInstance() {
        return new EntityQueryWithOptions();
    }
}
