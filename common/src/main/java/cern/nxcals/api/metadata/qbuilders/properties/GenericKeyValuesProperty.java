package cern.nxcals.api.metadata.qbuilders.properties;

import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.extraction.metadata.queries.KeyValuesProperty;
import com.github.rutledgepaulv.qbuilders.builders.QBuilder;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;

import java.util.Map;

public interface GenericKeyValuesProperty<T extends QBuilder<T>, C extends Condition<T>>
        extends GenericStringLikeProperty<T, C>, KeyValuesProperty<T> {
    C eq(SystemSpec system, Map<String, Object> value);

    C ne(SystemSpec system, Map<String, Object> value);

    C in(SystemSpec system, Map<String, Object>... value);

    C nin(SystemSpec system, Map<String, Object>... value);

    C like(SystemSpec system, Map<String, Object> value);

    C notLike(SystemSpec system, Map<String, Object> value);
}
