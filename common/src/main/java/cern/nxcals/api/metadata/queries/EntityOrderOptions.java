package cern.nxcals.api.metadata.queries;

import cern.nxcals.api.extraction.metadata.queries.EntityFields;

public final class EntityOrderOptions extends OrderOptions<EntityQueryWithOptions> implements EntityFields<
        OrderOptions<EntityQueryWithOptions>.OrderAction,
        OrderOptions<EntityQueryWithOptions>.OrderAction,
        OrderOptions<EntityQueryWithOptions>.OrderAction> {

    EntityOrderOptions(EntityQueryWithOptions returnedBuilder) {
        super(returnedBuilder);
    }
}
