package cern.nxcals.api.metadata.queries;

import cern.nxcals.common.annotation.Experimental;
import cern.nxcals.common.errors.JsonSerializationError;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Experimental
@Getter
@EqualsAndHashCode
public abstract class QueryOptions<T extends QueryOptions<?>> {
    protected static final ObjectMapper mapper = new ObjectMapper();
    private int limit = -1;
    private int offset = -1;
    @Setter(AccessLevel.PACKAGE)
    private String orderByField = "";
    @Setter(AccessLevel.PACKAGE)
    private boolean ascending = true;

    protected abstract T self();

    public T limit(int limit) {
        this.limit = limit;
        return self();
    }

    public T offset(int offset) {
        this.offset = offset;
        return self();
    }

    public String toJson() {
        try {
            return QueryOptions.mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            throw new JsonSerializationError("Cannot serialize QueryOptions to JSON. QueryOptions: " + this, e);
        }
    }

    public boolean shouldLimit() {
        return limit >= 0;
    }

    public boolean shouldOffset() {
        return offset >= 0;
    }

    public boolean shouldOrder() {
        return !orderByField.isEmpty();
    }
}
