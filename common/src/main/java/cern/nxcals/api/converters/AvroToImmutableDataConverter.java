package cern.nxcals.api.converters;

import cern.cmw.data.DataFactory;
import cern.cmw.data.DiscreteFunction;
import cern.cmw.data.DiscreteFunctionList;
import cern.cmw.datax.DataBuilder;
import cern.cmw.datax.EntryType;
import cern.cmw.datax.ImmutableData;
import cern.cmw.datax.ImmutableEntry;
import cern.nxcals.common.utils.IllegalCharacterConverter;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.primitives.Booleans;
import com.google.common.primitives.Doubles;
import com.google.common.primitives.Floats;
import com.google.common.primitives.Ints;
import com.google.common.primitives.Longs;
import lombok.NonNull;
import lombok.experimental.UtilityClass;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.util.Utf8;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static cern.cmw.datax.EntryType.BOOL;
import static cern.cmw.datax.EntryType.BOOL_ARRAY;
import static cern.cmw.datax.EntryType.DISCRETE_FUNCTION;
import static cern.cmw.datax.EntryType.DOUBLE;
import static cern.cmw.datax.EntryType.DOUBLE_ARRAY;
import static cern.cmw.datax.EntryType.FLOAT;
import static cern.cmw.datax.EntryType.FLOAT_ARRAY;
import static cern.cmw.datax.EntryType.INT32;
import static cern.cmw.datax.EntryType.INT32_ARRAY;
import static cern.cmw.datax.EntryType.INT64;
import static cern.cmw.datax.EntryType.INT64_ARRAY;
import static cern.cmw.datax.EntryType.STRING;
import static cern.cmw.datax.EntryType.STRING_ARRAY;
import static cern.nxcals.api.converters.ImmutableDataToAvroConverter.booleanFieldSchemaNullable;
import static cern.nxcals.api.converters.ImmutableDataToAvroConverter.doubleFieldSchemaNullable;
import static cern.nxcals.api.converters.ImmutableDataToAvroConverter.floatFieldSchemaNullable;
import static cern.nxcals.api.converters.ImmutableDataToAvroConverter.intFieldSchemaNullable;
import static cern.nxcals.api.converters.ImmutableDataToAvroConverter.longFieldSchemaNullable;
import static cern.nxcals.api.converters.ImmutableDataToAvroConverter.stringFieldSchemaNullable;
import static cern.nxcals.common.avro.SchemaConstants.ARRAY_DIMENSIONS_FIELD_NAME;
import static cern.nxcals.common.avro.SchemaConstants.ARRAY_ELEMENTS_FIELD_NAME;
import static cern.nxcals.common.avro.SchemaConstants.DDF_X_ARRAY_FIELD_NAME;
import static cern.nxcals.common.avro.SchemaConstants.DDF_Y_ARRAY_FIELD_NAME;
import static cern.nxcals.common.avro.SchemaConstants.DF_LIST_FIELD_NAME;
import static cern.nxcals.common.utils.AvroUtils.getSchemaTypeFor;
import static cern.nxcals.common.utils.AvroUtils.getSchemaUnionType;

@UtilityClass
public class AvroToImmutableDataConverter {
    public static final Map<Schema, EntryType<?>> PRIMITIVE_SCHEMA_TO_TYPE_MAP = new ImmutableMap.Builder<Schema, EntryType<?>>()
            .put(booleanFieldSchemaNullable, BOOL)
            .put(intFieldSchemaNullable, INT32)
            .put(longFieldSchemaNullable, INT64)
            .put(floatFieldSchemaNullable, FLOAT)
            .put(doubleFieldSchemaNullable, DOUBLE)
            .put(stringFieldSchemaNullable, STRING)
            .build();

    public static final Map<Schema.Type, EntryType<?>> SCHEMA_TO_ENTRY_TYPE_MAP = new ImmutableMap.Builder<Schema.Type, EntryType<?>>()
            .put(Schema.Type.BOOLEAN, BOOL_ARRAY)
            .put(Schema.Type.INT, INT32_ARRAY)
            .put(Schema.Type.LONG, INT64_ARRAY)
            .put(Schema.Type.FLOAT, FLOAT_ARRAY)
            .put(Schema.Type.DOUBLE, DOUBLE_ARRAY)
            .put(Schema.Type.STRING, STRING_ARRAY)
            .build();

    private static final String[] EMPTY = new String[0];


    public static ImmutableData convert(@NonNull GenericRecord record) {
        final DataBuilder builder = ImmutableData.builder();
        final Schema schema = record.getSchema();
        for (Schema.Field field : schema.getFields()) {
            String name = field.name();
            if (IllegalCharacterConverter.isEncoded(name)) {
                name = IllegalCharacterConverter.get().convertFromLegal(name);
            }
            builder.add(entry(record.get(name), field.schema(), name));
        }

        return builder.build();
    }

    private static ImmutableEntry entry(Object value, Schema schema, String fieldName) {
        if (value == null) {
            return ImmutableEntry.ofNull(fieldName, toEntryType(schema));
        }
        switch (schema.getType()) {
            case RECORD:
                return record(value, schema, fieldName);
            case ENUM:
                break;
            case FIXED:
            case ARRAY:
            case MAP:
            case UNION:
                return entry(value, getSchemaUnionType(schema), fieldName);
            case STRING:
                // must do to string as the String might mean a class of org.apache.avro.util.Utf8 type.
                return ImmutableEntry.of(fieldName, value.toString());
            case BYTES:
                break;
            case INT:
                return ImmutableEntry.of(fieldName, value);
            case LONG:
                return ImmutableEntry.of(fieldName, value);
            case FLOAT:
                return ImmutableEntry.of(fieldName, value);
            case DOUBLE:
                return ImmutableEntry.of(fieldName, value);
            case BOOLEAN:
                return ImmutableEntry.of(fieldName, value);
            case NULL:
                break;
        }
        return null;
    }

    private static ImmutableEntry record(Object value, Schema schema, String name) {
        if (isArray(schema)) {
            Object elements = ((GenericRecord) value).get(ARRAY_ELEMENTS_FIELD_NAME);
            int[] dimensions = Ints.toArray((List<Integer>) ((GenericRecord) value).get(ARRAY_DIMENSIONS_FIELD_NAME));
            //each element type can be union with null like for Strings
            Schema elementsSchema = schema.getField(ARRAY_ELEMENTS_FIELD_NAME).schema();
            Schema schemaUnionType = getSchemaUnionType(elementsSchema);
            Schema elementSchema = schemaUnionType.getElementType();
            Schema.Type elementType = getSchemaTypeFor(elementSchema);

            switch (elementType) {
                case STRING:
                    return handleStringAsArrayElement(name, elements, dimensions);
                case BYTES:
                    break;
                case INT:
                    return ImmutableEntry.of(name, Ints.toArray((List<Integer>) elements), dimensions);
                case LONG:
                    return ImmutableEntry.of(name, Longs.toArray((List<Long>) elements), dimensions);
                case FLOAT:
                    return ImmutableEntry.of(name, Floats.toArray((List<Float>) elements), dimensions);
                case DOUBLE:
                    return ImmutableEntry.of(name, Doubles.toArray((List<Double>) elements), dimensions);
                case BOOLEAN:
                    return ImmutableEntry.of(name, Booleans.toArray((List<Boolean>) elements), dimensions);
                case RECORD:
                    ImmutableEntry discreteFunctions = handleRecordAsArrayElement(name, (List<GenericRecord>) elements, elementSchema);
                    if (discreteFunctions != null) return discreteFunctions;
                    break;
                case NULL:
                    break;
                case ENUM:
                    break;
                case ARRAY:
                    break;
                case MAP:
                    break;
                case UNION:
                    break;
                case FIXED:
                    break;
            }

        } else {
            //not array
            if (isDiscreteFunction(schema)) {
                return ImmutableEntry.of(name, createDiscreteFunction((GenericRecord) value));
            }

            if (isDiscreteFunctionList(schema)) {
                return ImmutableEntry.of(name, createDiscreteFunctionList((GenericRecord) value));
            }
        }

        throw new IllegalArgumentException("Unknown schema type [" + schema + "] for given field [" + name + "]");
    }

    private static ImmutableEntry handleStringAsArrayElement(String name, Object elements, int[] dimensions) {
        List<?> values = (List<?>) elements;
        if (values.isEmpty()) {
            return ImmutableEntry.of(name, EMPTY, dimensions);
        } else if (values.get(0) instanceof Utf8) {
            //Must convert from Utf8 to String...
            List<Utf8> utf8s = (List<Utf8>) elements;
            String[] strings = utf8s.stream().map(Utf8::toString).toArray(String[]::new);
            return ImmutableEntry.of(name, strings, dimensions);
        } else {
            return ImmutableEntry.of(name, ((List<String>) elements).toArray(EMPTY), dimensions);
        }
    }

    private static ImmutableEntry handleRecordAsArrayElement(String name, List<GenericRecord> elements, Schema elementSchema) {
        //Array of DiscreteFunction
        if (isDiscreteFunction(elementSchema)) {
            return ImmutableEntry.of(name, convertTo(AvroToImmutableDataConverter::createDiscreteFunction, elements).toArray(new DiscreteFunction[0]));
        }
        //Array of or DiscreteFunctionList
        if (isDiscreteFunctionList(elementSchema)) {
            return ImmutableEntry.of(name, convertTo(AvroToImmutableDataConverter::createDiscreteFunctionList, elements).toArray(new DiscreteFunctionList[0]));
        }
        return null;
    }

    private static <T> List<T> convertTo(Function<GenericRecord,T> converter, List<GenericRecord> elements) {
        return elements.stream()
                .map(converter).collect(Collectors.toList());
    }


    private static DiscreteFunctionList createDiscreteFunctionList(GenericRecord value) {
        List<Object> values = (List<Object>) value.get(DF_LIST_FIELD_NAME);
        List<DiscreteFunction> discreteFunctions = values.stream().map(val -> createDiscreteFunction(((GenericRecord) val))).collect(Collectors.toList());
        return DataFactory.createDiscreteFunctionList(discreteFunctions);
    }

    private static boolean isDiscreteFunctionList(Schema schema) {
        return checkSizeAndContent(schema, ImmutableSet.of(DF_LIST_FIELD_NAME));
    }

    private static DiscreteFunction createDiscreteFunction(GenericRecord value) {
        List<Double> x = (List<Double>) value.get(DDF_X_ARRAY_FIELD_NAME);
        List<Double> y = (List<Double>) value.get(DDF_Y_ARRAY_FIELD_NAME);
        return DataFactory.createDiscreteFunction(Doubles.toArray(x), Doubles.toArray(y));
    }

    private static boolean isDiscreteFunction(Schema schema) {
        return checkSizeAndContent(schema, ImmutableSet.of(DDF_Y_ARRAY_FIELD_NAME, DDF_X_ARRAY_FIELD_NAME));
    }

    private static boolean isArray(Schema schema) {
        return checkSizeAndContent(schema, ImmutableSet.of(ARRAY_DIMENSIONS_FIELD_NAME, ARRAY_ELEMENTS_FIELD_NAME));
    }

    private static boolean checkSizeAndContent(Schema schema, Set<String> fields) {
        if (schema.getFields().size() != fields.size()) {
            return false;
        }
        return schema.getFields().stream()
                .map(Schema.Field::name)
                .filter(fields::contains)
                .count() == fields.size();

    }

    private static EntryType<?> toEntryType(Schema schema) {
        EntryType<?> type = PRIMITIVE_SCHEMA_TO_TYPE_MAP.get(schema);
        if (type != null) {
            return type;
        }
        if (schema.getType() == Schema.Type.UNION) {
            Schema nonNullSchema = getSchemaUnionType(schema);

            if (isArray(nonNullSchema)) {
                Schema elementsSchema = nonNullSchema.getField(ARRAY_ELEMENTS_FIELD_NAME).schema();
                Schema schemaUnionType = getSchemaUnionType(elementsSchema);
                Schema elementSchema = schemaUnionType.getElementType();

                type = SCHEMA_TO_ENTRY_TYPE_MAP.get(getSchemaTypeFor(elementSchema));
            } else if (isDiscreteFunction(nonNullSchema)) {
                type = DISCRETE_FUNCTION;
            }
        }
        if (type != null) {
            return type;
        }
        throw new IllegalArgumentException("Cannot map avro schema to ImmutableData EntryType: " + schema);
    }
}
