/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.api.exceptions;

public class NotFoundRuntimeException extends RuntimeException {
    public NotFoundRuntimeException(String message) {
        super(message);
    }
}
