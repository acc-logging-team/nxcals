package cern.nxcals.api.exceptions;

/**
 * This exception is thrown when there is a data conflict detected, like a history rewrite.
 * This exception is meant to be fatal. No retries or any recovery will be attempted.
 */
public class FatalDataConflictRuntimeException extends RuntimeException {

    @SuppressWarnings("squid:UndocumentedApi")
    public FatalDataConflictRuntimeException() {
    }

    @SuppressWarnings("squid:UndocumentedApi")
    public FatalDataConflictRuntimeException(String message) {
        super(message);
    }

    public FatalDataConflictRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }
}
