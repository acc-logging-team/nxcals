package cern.nxcals.api.extraction.metadata.queries;

import cern.nxcals.api.domain.SystemSpec;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.Map;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class EntitiesTest {
    public static final String SCHEMA = "{\"type\":\"record\",\"name\":\"device\",\"fields\":[{\"name\":\"device\",\"type\":\"string\"}]}";
    @Mock
    private SystemSpec systemSpec;
    @Test
    public void shouldCreateCondition() {
        when(systemSpec.getPartitionKeyDefinitions()).thenReturn(SCHEMA);
        when(systemSpec.getEntityKeyDefinitions()).thenReturn(SCHEMA);
        Map<String, Object> keyValues = new HashMap<>();
        keyValues.put("device", "test_device");

        String rsql = toRSQL(Entities.suchThat().systemName().eq("name").and()
                .systemId().in(1L,2L).and()
                .id().exists().and()
                .keyValues().like("value").and()
                .partitionKeyValues().ne("value").and()
                .keyValues().like(systemSpec,keyValues ).and()
                .partitionKeyValues().ne(systemSpec,keyValues));

        assertEquals(7, rsql.split(";").length);
    }

    @Test
    public void shouldNotQuoteBackslashWithDoubleBackslashWhenConvertingToString() {
        String rsql = toRSQL(Entities.suchThat().systemName().like("\\_name\\%"));

        assertTrue(rsql.contains("\\_name\\%"));
    }

}