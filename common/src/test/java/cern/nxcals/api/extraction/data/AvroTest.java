package cern.nxcals.api.extraction.data;

import cern.nxcals.api.extraction.thin.AvroData;
import cern.nxcals.common.avro.DefaultGenericRecordToBytesEncoder;
import cern.nxcals.common.utils.AvroUtils;
import com.google.protobuf.ByteString;
import org.apache.avro.Schema;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.apache.avro.Schema.Type.STRING;
import static org.apache.avro.Schema.create;
import static org.apache.avro.SchemaBuilder.record;
import static org.assertj.core.api.Assertions.assertThat;

public class AvroTest {
    private static final String CLASS = "class";
    private static final String PROPERTY = "property";

    private final Schema schema = record("TEST").namespace("ns").fields().name(CLASS).type(create(STRING)).noDefault()
            .name(PROPERTY).type(create(STRING)).noDefault().endRecord();

    private byte[] output;

    @BeforeEach
    public void setup() throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        DataFileWriter<Object> writer = AvroUtils.createDataFileWriter(schema, outputStream, "snappy", 1024);
        generateListOfRecords().forEach(record -> {
            byte[] bytes = DefaultGenericRecordToBytesEncoder.convertToBytes(record);
            try {
                writer.appendEncoded(ByteBuffer.wrap(bytes));
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        });
        writer.flush();
        writer.close();
        output = outputStream.toByteArray();
    }

    private List<GenericRecord> generateListOfRecords() {
        return IntStream.range(0, 10).mapToObj(i -> {
            GenericRecord genericRecord = new GenericData.Record(schema);
            genericRecord.put(CLASS, "C" + i);
            genericRecord.put(PROPERTY, "P" + i);
            return genericRecord;
        }).collect(Collectors.toList());
    }

    @Test
    public void toGenericRecordList() {
        List<GenericRecord> genericRecords = Avro.records(output);
        assertThat(genericRecords).containsExactly(generateListOfRecords().toArray(new GenericRecord[]{}));
    }

    @Test
    public void testToGenericRecordList() {
        List<GenericRecord> genericRecords = Avro
                .records(AvroData.newBuilder().setAvroBytes(ByteString.copyFrom(output)).build());
        assertThat(genericRecords).containsExactly(generateListOfRecords().toArray(new GenericRecord[]{}));
    }
}
