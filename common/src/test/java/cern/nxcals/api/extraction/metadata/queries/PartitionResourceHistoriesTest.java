package cern.nxcals.api.extraction.metadata.queries;

import org.junit.jupiter.api.Test;

import java.time.Instant;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PartitionResourceHistoriesTest {

    @Test
    void shouldCreateCondition() {
        String rsql = toRSQL(PartitionResourceHistories.suchThat()
                .id().eq(1L)
                .and()
                .partitionResourceId().eq(2L)
                .and()
                .storageType().eq("type")
                .and()
                .validFromStamp().eq(Instant.now())
                .and()
                .validToStamp().eq(Instant.now()));

        assertEquals(5, rsql.split(";").length);
        assertTrue(rsql.contains("id"));
        assertTrue(rsql.contains("partitionResource.id"));
        assertTrue(rsql.contains("storageType"));
        assertTrue(rsql.contains("validFromStamp"));
        assertTrue(rsql.contains("validToStamp"));
    }
}