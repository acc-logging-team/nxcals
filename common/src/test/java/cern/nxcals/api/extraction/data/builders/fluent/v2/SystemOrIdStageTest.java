package cern.nxcals.api.extraction.data.builders.fluent.v2;

import cern.nxcals.api.extraction.data.builders.fluent.QueryData;
import cern.nxcals.api.extraction.data.builders.fluent.Stage;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class SystemOrIdStageTest {
    static class NoOpStage extends Stage<NoOpStage, Object> implements IdStage<NoOpStage> {

        NoOpStage(QueryData<Object> data) {
            super(data);
        }

        static NoOpStage getInstance() {
            return new NoOpStage(new QueryData<>(x -> 1));
        }

        public QueryData<Object> data() {
            return super.data();
        }

        @Override
        public NoOpStage idIn(Set<Long> ids) {
            ids.forEach(x -> data().addEntityId(x));
            return this;
        }
    }

    @Test
    void shouldProperlySetSystemInQueryData() {
        SystemOrIdStage<NoOpStage, NoOpStage, Object> stage = new SystemOrIdStage<>(NoOpStage.getInstance());
        String system = "SYSTEM";
        NoOpStage spyStage = stage.system(system);
        assertEquals(system, spyStage.data().getSystem());
    }

    @Test
    void shouldProperlySetIdInQueryData() {
        SystemOrIdStage<NoOpStage, NoOpStage, Object> stage = new SystemOrIdStage<>(NoOpStage.getInstance());
        long id = 1;
        SystemOrIdStage<NoOpStage, NoOpStage, Object> stageWithId = stage.idEq(id);
        String system = "SYSTEM";
        NoOpStage spyStage = stageWithId.system(system);
        assertAll(
                () -> assertEquals(Set.of(id), spyStage.data().getEntityIds()),
                () -> assertEquals(system, spyStage.data().getSystem())
        );
    }
}
