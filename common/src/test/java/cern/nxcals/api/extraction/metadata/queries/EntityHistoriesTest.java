package cern.nxcals.api.extraction.metadata.queries;

import org.junit.jupiter.api.Test;

import java.time.Instant;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class EntityHistoriesTest {
    @Test
    public void shouldCreateCondition() {
        String rsql = toRSQL(EntityHistories.suchThat().entityId().eq(1L).and()
                .id().exists().and()
                .partitionId().eq(2L).and()
                .systemId().exists().and()
                .validFromStamp().eq(Instant.now()).and()
                .validToStamp().doesNotExist());
        assertEquals(6, rsql.split(";").length);
    }
}