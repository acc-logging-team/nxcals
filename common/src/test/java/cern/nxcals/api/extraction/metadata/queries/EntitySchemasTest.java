package cern.nxcals.api.extraction.metadata.queries;

import org.junit.jupiter.api.Test;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class EntitySchemasTest {
    @Test
    public void shouldCreateCondition() {
        String rsql = toRSQL(EntitySchemas.suchThat().id().eq(1L).and()
                .schemaHash().like("%").and()
                .schema().eq("schema"));

        assertEquals(3, rsql.split(";").length);
    }
}