package cern.nxcals.api.extraction.data.builders.fluent.v2;

import cern.nxcals.api.extraction.data.builders.fluent.QueryData;
import cern.nxcals.api.extraction.data.builders.fluent.Stage;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SystemStageTest {
    static class NoOpStage extends Stage<NoOpStage, Object> {

        NoOpStage(QueryData<Object> data) {
            super(data);
        }

        static NoOpStage getInstance() {
            return new NoOpStage(new QueryData<>(x -> 1));
        }

        public QueryData<Object> data() {
            return super.data();
        }
    }

    @Test
    void shouldProperlySetSystemInQueryData() {
        SystemStage<NoOpStage, Object> stage = new SystemStage<>(NoOpStage.getInstance());
        String system = "SYSTEM";
        NoOpStage spyStage = stage.system(system);
        assertEquals(system, spyStage.data().getSystem());
    }
}
