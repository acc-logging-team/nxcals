/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.data.builders.fluent;

import cern.nxcals.common.domain.EntityKeyValues;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.Duration;
import java.time.Instant;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.function.Function;

import static cern.nxcals.api.utils.TimeUtils.getNanosFromInstant;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;
import static org.mockito.Mockito.mock;

/**
 * Test suite for {@link StageSequenceEntities}.
 */
@TestInstance(PER_CLASS)
public class StageSequenceEntitiesTest {
    private static final String SYSTEM_1 = "CMW";

    private static final String KEY_1 = "KEY_1";
    private static final String KEY_2 = "KEY_2";
    private static final String VALUE_1 = "VALUE_1";
    private static final String VALUE_2 = "VALUE_2";
    private static final Map<String, Object> KEY_VALUES = ImmutableMap.of(KEY_1, VALUE_1, KEY_2, VALUE_2);

    private static final Instant TIME_1 = Instant.ofEpochSecond(1505313230, 123456789);
    private static final Instant TIME_2 = Instant.ofEpochSecond(1505316830, 123456789);
    private static final String TIME_1_STRING = "2017-09-13 14:33:50.123456789";
    private static final String TIME_2_STRING = "2017-09-13 15:33:50.123456789";
    private static final long TIME_1_NANOS = getNanosFromInstant(TIME_1);
    private static final long TIME_2_NANOS = getNanosFromInstant(TIME_2);

    @Test
    public void shouldThrowWhenCreatingBuilderWithSystemToNull() {
        assertThrows(NullPointerException.class, () -> builder(null));
    }

    @Test
    public void shouldCreateQueryFromStringTimeDates() {
        //when
        QueryData<Dataset<Row>> query = builder(SYSTEM_1).startTime(TIME_1_STRING).endTime(TIME_2_STRING).entity()
                .keyValue(KEY_1, VALUE_1).data();

        //then
        assertEquals(TIME_1, query.getStartTime());
        assertEquals(TIME_2, query.getEndTime());
    }

    @Test
    public void shouldCreateQueryFromNanosTimeDates() {
        //when
        QueryData query = builder(SYSTEM_1).startTime(TIME_1_NANOS).endTime(TIME_2_NANOS).entity()
                .keyValue(KEY_1, VALUE_1).data();

        //then
        assertEquals(TIME_1, query.getStartTime());
        assertEquals(TIME_2, query.getEndTime());
    }

    @ParameterizedTest
    @MethodSource("wrongDateTimeStringFormats")
    public void shouldFailForInvalidStartTimeFormat(String dateTimeString, Class<? extends Throwable> ex) {
        assertThrows(ex, () -> builder(SYSTEM_1).startTime(dateTimeString));
    }

    @ParameterizedTest
    @MethodSource("wrongDateTimeStringFormats")
    public void shouldFailForInvalidEndTimeFormat(String dateTimeString, Class<? extends Throwable> ex) {
        assertThrows(ex, () -> builder(SYSTEM_1).startTime(TIME_1).endTime(dateTimeString));
    }

    @Test
    public void shouldCreateQueryWithStartInstantAndDuration() {
        //when
        QueryData query = builder(SYSTEM_1).startTime(TIME_1).duration(Duration.between(TIME_1, TIME_2)).entity()
                .keyValue(KEY_1, VALUE_1).data();

        //then
        assertEquals(TIME_1, query.getStartTime());
        assertEquals(TIME_2, query.getEndTime());
    }

    @Test
    public void shouldCreateQueryWithStartInstantAndDurationNanos() {
        //when
        QueryData query = builder(SYSTEM_1).startTime(TIME_1).duration(Duration.between(TIME_1, TIME_2).toNanos())
                .entity().keyValue(KEY_1, VALUE_1).data();

        //then
        assertEquals(TIME_1, query.getStartTime());
        assertEquals(TIME_2, query.getEndTime());
    }

    @Test
    public void shouldCreateQueryWithStartInstantAndDurationSeconds() {
        //when
        QueryData query = builder(SYSTEM_1).startTime(TIME_1).duration(10, ChronoUnit.SECONDS).entity()
                .keyValue(KEY_1, VALUE_1).data();

        //then
        assertEquals(TIME_1, query.getStartTime());
        assertEquals(TIME_1.plusSeconds(10), query.getEndTime());
    }

    @Test
    public void shouldFailForNegativeDuration() {
        assertThrows(RuntimeException.class,
                () -> builder(SYSTEM_1).startTime(TIME_1).duration(Duration.ofSeconds(-1)));
    }

    @Test
    public void shouldCreateQueryWithSpecificTimestampInstant() {
        //when
        QueryData query = builder(SYSTEM_1).atTime(TIME_1).entity().keyValue(KEY_1, VALUE_1).data();

        //then
        assertEquals(TIME_1, query.getStartTime());
        assertEquals(TIME_1, query.getEndTime());
    }

    @Test
    public void shouldCreateQueryWithSpecificTimestampString() {
        //when
        QueryData query = builder(SYSTEM_1).atTime(TIME_1_STRING).entity().keyValue(KEY_1, VALUE_1).data();

        //then
        assertEquals(TIME_1, query.getStartTime());
        assertEquals(TIME_1, query.getEndTime());
    }

    @Test
    public void shouldCreateQueryWithSpecificTimestampNanos() {
        //when
        QueryData query = builder(SYSTEM_1).atTime(TIME_1_NANOS).entity().keyValue(KEY_1, VALUE_1).data();

        //then
        assertEquals(TIME_1, query.getStartTime());
        assertEquals(TIME_1, query.getEndTime());
    }

    @ParameterizedTest
    @MethodSource("wrongDateTimeStringFormats")
    public void shouldFailForInvalidTimestampString(String dateTimeString, Class<? extends Throwable> ex) {
        assertThrows(ex, () -> builder(SYSTEM_1).atTime(dateTimeString).entity().keyValue(KEY_1, VALUE_1));
    }

    @Test
    public void shouldCreateKeyValueQuery() {
        //when
        QueryData<Dataset<Row>> query = builder(SYSTEM_1).startTime(TIME_1).endTime(TIME_2).entity()
                .keyValue(KEY_1, VALUE_1).entity()
                .data();

        //then
        assertNotNull(query);
        assertEquals(SYSTEM_1, query.getSystem());
        assertTrue(query.getEntities().stream()
                .anyMatch(entityKeyValues -> KEY_1.equals(entityKeyValues.get().iterator().next().getKey())));
        assertEquals(TIME_1, query.getStartTime());
        assertEquals(TIME_2, query.getEndTime());
    }

    @Test
    public void shouldCreateKeyValuesWithWildcardsQuery() {
        //when
        QueryData<Dataset<Row>> query = builder(SYSTEM_1).startTime(TIME_1).endTime(TIME_2).entity()
                .keyValuesLike(KEY_VALUES)
                .entity().data();

        //then
        assertNotNull(query);
        assertEquals(SYSTEM_1, query.getSystem());
        assertTrue(query.getEntities().stream()
                .anyMatch(entityKeyValues -> KEY_VALUES.equals(entityKeyValues.getAsMap())));
        assertEquals(TIME_1, query.getStartTime());
        assertEquals(TIME_2, query.getEndTime());
    }

    @Test
    public void shouldUseKeyValuesLikeOverKeyValuesQuery() {
        //when
        QueryData<Dataset<Row>> query = builder(SYSTEM_1).startTime(TIME_1).endTime(TIME_2).entity()
                .keyValuesLike(KEY_VALUES)
                .entity().keyValues(KEY_VALUES).entity().data();

        //then
        assertNotNull(query);
        assertEquals(SYSTEM_1, query.getSystem());
        assertTrue(query.getEntities().stream()
                .anyMatch(entityKeyValues -> KEY_VALUES.equals(entityKeyValues.getAsMap())));
        assertTrue(query.getEntities().stream().anyMatch(EntityKeyValues::isAnyWildcard));
        assertEquals(2, query.getEntities().size());
        assertEquals(TIME_1, query.getStartTime());
        assertEquals(TIME_2, query.getEndTime());
    }

    @Test
    public void shouldCreateKeyValuesQueryWithFieldAliases() {
        //when
        QueryData<Dataset<Row>> query = builder(SYSTEM_1).startTime(TIME_1).endTime(TIME_2)
                .fieldAliases(ImmutableMap.of("ALIAS1", Lists.newArrayList("field1", "field2")))
                .fieldAliases("ALIAS2", Lists.newArrayList("field3", "field4"))
                .fieldAlias("ALIAS2", "field5")
                .fieldAlias("ALIAS3", "field6")
                .entity().keyValues(KEY_VALUES).entity()
                .data();

        //then
        assertNotNull(query);
        assertEquals(SYSTEM_1, query.getSystem());
        assertTrue(query.getEntities().stream()
                .anyMatch(entityKeyValues -> KEY_VALUES.equals(entityKeyValues.getAsMap())));
        assertEquals(ImmutableSet.of("field1", "field2"), query.getAliasFields().get("ALIAS1"));
        assertEquals(ImmutableSet.of("field3", "field4", "field5"), query.getAliasFields().get("ALIAS2"));
        assertEquals(ImmutableSet.of("field6"), query.getAliasFields().get("ALIAS3"));

        assertEquals(TIME_1, query.getStartTime());
        assertEquals(TIME_2, query.getEndTime());
    }

    public Object[][] wrongDateTimeStringFormats() {
        return new Object[][] { new Object[] { null, NullPointerException.class },
                new Object[] { "", DateTimeParseException.class },
                new Object[] { "SOMETHING", DateTimeParseException.class },
                new Object[] { "2017-09-13", DateTimeParseException.class },
                new Object[] { "14:33:50", DateTimeParseException.class },
        };
    }

    private TimeStartStage<EntityAliasStage<KeyValueStage<Dataset<Row>>, Dataset<Row>>, Dataset<Row>> builder(
            String system) {
        Function<QueryData<Dataset<Row>>, Dataset<Row>> producer = queryData -> mock(Dataset.class);
        return StageSequenceEntities.<Dataset<Row>>sequence()
                .apply(new QueryData<>(producer)).system(system);
    }
}
