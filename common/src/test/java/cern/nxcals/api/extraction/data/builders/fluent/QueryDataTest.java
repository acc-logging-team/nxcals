package cern.nxcals.api.extraction.data.builders.fluent;

import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.common.domain.EntityKeyValue;
import cern.nxcals.common.domain.EntityKeyValues;
import cern.nxcals.common.domain.ExtractionCriteria;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import static cern.nxcals.api.extraction.data.builders.fluent.QueryData.DEVICE_KEY;
import static cern.nxcals.api.extraction.data.builders.fluent.QueryData.PROPERTY_KEY;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class QueryDataTest {
    private static final Instant TIME = Instant.now();
    private static final String SYSTEM = "SYSTEM";
    private static final String VARIABLE = "VARIABLE";

    private QueryData<Integer> queryData() {
        return new QueryData<>((x) -> 1);
    }

    @Test
    void shouldThrowWhenStartAfterEnd() {
        QueryData<Integer> queryData = queryData();
        queryData.setSystem(SYSTEM);
        queryData.setVariableKey(VARIABLE, false);
        queryData.setStartTime(TIME);
        Instant endTime = TIME.minus(1, ChronoUnit.SECONDS);
        queryData.setEndTime(endTime);

        Throwable exceptionThatWasThrown = assertThrows(IllegalArgumentException.class, queryData::build);
        assertEquals(String.format(QueryData.END_BEFORE_START_MSG, TIME, endTime),
                exceptionThatWasThrown.getMessage());
    }

    @Test
    void shouldThrowWhenEntitiesAndVariablesAreEmpty() {
        QueryData<Integer> queryData = queryData();
        queryData.setSystem(SYSTEM);
        queryData.setStartTime(TIME);
        queryData.setEndTime(TIME);

        Throwable exceptionThatWasThrown = assertThrows(IllegalArgumentException.class, queryData::build);
        assertEquals(QueryData.ENTITIES_AND_VARIABLES_ARE_EMPTY_MSG, exceptionThatWasThrown.getMessage());
    }

    @Test
    void shouldCorrectlyAppendNewAlias() {
        QueryData<Integer> queryData = queryData();
        String alias = "alias";
        String field = "field";
        String field1 = "field1";
        queryData.addAlias(alias, Set.of(field));
        queryData.addAlias(alias, field1);
        assertEquals(Map.of(alias, Set.of(field, field1)), queryData.getAliasFields());
    }

    @Test
    void shouldKeepOrderOfAliases() {
        QueryData<Integer> queryData = queryData();
        String alias = "alias";
        List<String> fields = new Random().ints().limit(100).mapToObj(c -> String.format("field%s", c))
                .collect(Collectors.toList());
        queryData.addAlias(alias, fields.subList(0, 60));
        queryData.addAlias(alias, fields.subList(50, 100));
        assertEquals(fields, new ArrayList<>(queryData.getAliasFields().get(alias)));
    }

    @Test
    void shouldCorrectlyAddTwoEntities() {
        QueryData<Integer> queryData = queryData();
        String key = "key";
        String keyWithWildcard = key + "%";
        String value = "value";
        queryData.addToEntity(key, value, false);
        queryData.closeEntity();
        queryData.addToEntity(keyWithWildcard, value, true);
        queryData.addToEntity(key, value, false);
        queryData.closeEntity();
        EntityKeyValue entityKeyValue = EntityKeyValue.builder().key(key).value(value).wildcard(false).build();
        EntityKeyValue entityKeyValueWithWildcard = EntityKeyValue.builder().key(keyWithWildcard).value(value)
                .wildcard(true).build();
        EntityKeyValues firstEntity = EntityKeyValues.builder().keyValue(entityKeyValue).build();
        EntityKeyValues secondEntity = EntityKeyValues.builder().keyValue(entityKeyValue)
                .keyValue(entityKeyValueWithWildcard).build();
        assertEquals(
                Set.of(firstEntity, secondEntity),
                new HashSet<>(queryData.getEntities())
        );
    }

    @ParameterizedTest
    @CsvSource({ "false, false", "true, false", "false, true", "true, true" })
    void shouldCorrectlyAddDeviceAndProperty(boolean deviceAsWildcard, boolean propertyAsWildcard) {
        QueryData<Integer> queryData = queryData();
        String device = "device";
        String property = "property";

        queryData.addDevice(device, deviceAsWildcard);
        queryData.addProperty(property, propertyAsWildcard);
        queryData.closeEntity();
        EntityKeyValue deviceKeyValue = EntityKeyValue.builder().key(DEVICE_KEY).value(device)
                .wildcard(deviceAsWildcard).build();
        EntityKeyValue propertyKeyValue = EntityKeyValue.builder().key(PROPERTY_KEY).value(property)
                .wildcard(propertyAsWildcard)
                .build();

        EntityKeyValues entity = EntityKeyValues.builder().keyValue(deviceKeyValue).keyValue(propertyKeyValue).build();
        assertEquals(
                List.of(entity),
                queryData.getEntities()
        );
    }

    @Test
    void shouldNotOverridePreviouslyDefinedVariable() {
        QueryData<Integer> queryData = queryData();
        String variable = "VAR";
        queryData.setVariableKey(variable, true);
        queryData.setVariableKey(variable, false);
        assertTrue(queryData.getVariables().get(variable));
    }

    @Test
    void shouldCorrectlyConvertToExtractionCriteria() {
        String alias = "alias";
        String field = "field";

        String key = "key";
        String value = "value";
        EntityKeyValue entityKeyValue = EntityKeyValue.builder().key(key).value(value).wildcard(false).build();
        EntityKeyValues entity = EntityKeyValues.builder().keyValue(entityKeyValue).build();

        String variable = "VAR";
        String system = "SYS";

        Instant startTime = Instant.now();
        Instant endTime = startTime.plusSeconds(10);

        long entityId1 = 100L;
        long entityId2 = 101L;
        long variableId1 = 10L;
        long variableId2 = 11L;

        QueryData<Integer> queryData = queryData();

        queryData.addAlias(alias, Set.of(field));

        queryData.addToEntity(key, value, false);
        queryData.closeEntity();

        queryData.setVariableKey(variable, true);

        queryData.setSystem(system);

        queryData.setStartTime(startTime);
        queryData.setEndTime(endTime);

        queryData.addVariableId(variableId1);
        queryData.addVariableId(variableId2);
        queryData.addEntityId(entityId1);
        queryData.addEntityId(entityId2);

        ExtractionCriteria criteria = queryData.toExtractionCriteria();
        assertEquals(system, criteria.getSystemName());
        assertEquals(TimeWindow.between(startTime, endTime), criteria.getTimeWindow());
        assertEquals(Map.of(variable, true), criteria.getVariables());
        assertEquals(List.of(entity), criteria.getEntities());
        assertEquals(Map.of(alias, List.of(field)), criteria.getAliasFields());
        assertEquals(Set.of(variableId1, variableId2), criteria.getVariableIds());
        assertEquals(Set.of(entityId1, entityId2), criteria.getEntityIds());
    }

    @Test
    void shouldCorrectlyDetermineEntityQuery() {
        QueryData<Integer> queryData = queryData();
        String key = "key";
        String value = "value";
        queryData.addToEntity(key, value, false);
        queryData.closeEntity();
        assertFalse(queryData.isVariableSearch());
    }

    @Test
    void shouldCorrectlyDetermineVariableQueryWhenSpecifiedVariableName() {
        QueryData<Integer> queryData = queryData();
        String variable = "VAR";
        queryData.setVariableKey(variable, true);
        assertTrue(queryData.isVariableSearch());
    }

    @Test
    void shouldCorrectlyDetermineVariableQueryWhenSpecifiedVariableID() {
        QueryData<Integer> queryData = queryData();
        queryData.addVariableId(0L);
        assertTrue(queryData.isVariableSearch());
    }
}