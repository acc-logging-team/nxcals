package cern.nxcals.api.extraction.data.builders.fluent.v2;

import cern.nxcals.api.domain.TimeWindow;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.Instant;
import java.time.format.DateTimeParseException;

import static cern.nxcals.api.utils.TimeUtils.getNanosFromInstant;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TimeStageTest {

    private static final Instant TIME_1 = Instant.ofEpochSecond(1505313230, 123456789);
    private static final Instant TIME_2 = Instant.ofEpochSecond(1505316830, 123456789);
    private static final String TIME_1_STRING = "2017-09-13 14:33:50.123456789";
    private static final String TIME_2_STRING = "2017-09-13 15:33:50.123456789";
    private static final long TIME_1_NANOS = getNanosFromInstant(TIME_1);
    private static final long TIME_2_NANOS = getNanosFromInstant(TIME_2);

    @Test
    public void shouldCreateQueryFromInstants() {
        //when
        TimeStageImpl stage = builder()
                .timeWindow(TIME_1, TIME_2);

        //then
        assertEquals(TIME_1, stage.getStartTime());
        assertEquals(TIME_2, stage.getEndTime());
    }

    @Test
    public void shouldCreateQueryFromStringTimeDates() {
        //when
        TimeStageImpl stage = builder()
                .timeWindow(TIME_1_STRING, TIME_2_STRING);

        //then
        assertEquals(TIME_1, stage.getStartTime());
        assertEquals(TIME_2, stage.getEndTime());
    }

    @Test
    public void shouldCreateQueryFromNanosTimeDates() {
        //when
        TimeStageImpl stage = builder().timeWindow(TIME_1_NANOS, TIME_2_NANOS);

        //then
        assertEquals(TIME_1, stage.getStartTime());
        assertEquals(TIME_2, stage.getEndTime());
    }

    @Test
    public void shouldCreateQueryFromTimeWindow() {
        //when
        TimeStageImpl stage = builder().timeWindow(TimeWindow.between(TIME_1, TIME_2));

        //then
        assertEquals(TIME_1, stage.getStartTime());
        assertEquals(TIME_2, stage.getEndTime());
    }

    @ParameterizedTest
    @MethodSource("wrongDateTimeStringFormats")
    public void shouldFailForInvalidStartTimeFormat(String dateTimeString, Class<? extends Throwable> ex) {
        assertThrows(ex,
                () -> builder().timeWindow(dateTimeString, TIME_2_STRING));
    }

    @ParameterizedTest
    @MethodSource("wrongDateTimeStringFormats")
    public void shouldFailForInvalidEndTimeFormat(String dateTimeString, Class<? extends Throwable> ex) {
        assertThrows(ex,
                () -> builder().timeWindow(TIME_1_STRING, dateTimeString));
    }

    @Test
    public void shouldFailIfStartTimeInTimeWindowIsNull() {
        assertThrows(NullPointerException.class,
                () -> builder().timeWindow(TimeWindow.before(1000)));
    }

    @Test
    public void shouldFailIfEndTimeInTimeWindowIsNull() {
        assertThrows(NullPointerException.class,
                () -> builder().timeWindow(TimeWindow.after(1000)));
    }

    @Test
    public void shouldCreateQueryWithSpecificTimestampInstant() {
        //when
        TimeStageImpl stage = builder().atTime(TIME_1_STRING);

        //then
        assertEquals(TIME_1, stage.getStartTime());
        assertEquals(TIME_1, stage.getEndTime());
    }

    @Test
    public void shouldCreateQueryWithSpecificTimestampString() {
        //when
        TimeStageImpl stage = builder().atTime(TIME_1_STRING);

        //then
        assertEquals(TIME_1, stage.getStartTime());
        assertEquals(TIME_1, stage.getEndTime());
    }

    @Test
    public void shouldCreateQueryWithSpecificTimestampNanos() {
        //when
        TimeStageImpl stage = builder().atTime(TIME_1_NANOS);

        //then
        assertEquals(TIME_1, stage.getStartTime());
        assertEquals(TIME_1, stage.getEndTime());
    }

    @ParameterizedTest
    @MethodSource("wrongDateTimeStringFormats")
    public void shouldFailForInvalidTimestampString(String dateTimeString, Class<? extends Throwable> ex) {
        assertThrows(ex, () -> builder().atTime(dateTimeString));
    }

    static TimeStageImpl builder() {
        return new TimeStageImpl();
    }

    @NoArgsConstructor
    static class TimeStageImpl implements TimeStage<TimeStageImpl> {
        @Getter
        private Instant startTime;
        @Getter
        private Instant endTime;

        public TimeStageImpl timeWindow(@NonNull Instant startTime, @NonNull Instant endTime) {
            this.startTime = startTime;
            this.endTime = endTime;
            return this;
        }
    }

    public static Object[][] wrongDateTimeStringFormats() {
        return new Object[][] { new Object[] { null, NullPointerException.class },
                new Object[] { "", DateTimeParseException.class },
                new Object[] { "SOMETHING", DateTimeParseException.class },
                new Object[] { "2017-09-13", DateTimeParseException.class },
                new Object[] { "14:33:50", DateTimeParseException.class },
        };
    }
}
