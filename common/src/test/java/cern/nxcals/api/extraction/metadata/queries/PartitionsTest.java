package cern.nxcals.api.extraction.metadata.queries;

import cern.nxcals.api.domain.SystemSpec;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.Map;

import static cern.nxcals.common.utils.KeyValuesUtils.convertMapIntoAvroSchemaString;
import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PartitionsTest {

    public static final String SCHEMA = "{\"type\":\"record\",\"name\":\"device\",\"fields\":[{\"name\":\"device\",\"type\":\"string\"}]}";
    @Mock
    private SystemSpec systemSpec;


    @Test
    public void shouldCreateCondition() {
        //given
        when(systemSpec.getPartitionKeyDefinitions()).thenReturn(SCHEMA);
        Map<String, Object> keyValues = new HashMap<>();
        keyValues.put("device", "test_device");

        //when
        String rsql = toRSQL(Partitions.suchThat().id().eq(1L).and()
                .systemId().doesNotExist().and()
                .systemName().notLike("TEST%").and()
                .keyValues().like("key_value").and()
                .keyValues().like(systemSpec, keyValues));

        //then (; is the and() operator)
        assertEquals(5, rsql.split(";").length);
    }


    @Test
    public void shouldCreateKeyValueCondition() {
        //given
        when(systemSpec.getPartitionKeyDefinitions()).thenReturn(SCHEMA);
        Map<String, Object> keyValues = new HashMap<>();
        keyValues.put("device", "test_device");

        //when
        String rsql1 = toRSQL(Partitions.suchThat().keyValues()
                .eq(systemSpec, keyValues)
                .and()
                .id()
                .eq(10L));

        String rsql2 = toRSQL(Partitions.suchThat().keyValues()
                .eq(convertMapIntoAvroSchemaString(keyValues, systemSpec.getPartitionKeyDefinitions()))
                .and()
                .id()
                .eq(10L));

        //then
        assertEquals(rsql1, rsql2);
    }

    @Test
    public void shouldCreateNestedCondition() {
        String rsql = toRSQL(Partitions.suchThat().keyValues().eq("test").or().and(Partitions.suchThat().id().exists(), Partitions.suchThat().keyValues().like("value")));
        assertEquals("keyValues=seq=\"test\",(id=ex=\"true\";keyValues=like=\"value\")", rsql);
    }
}