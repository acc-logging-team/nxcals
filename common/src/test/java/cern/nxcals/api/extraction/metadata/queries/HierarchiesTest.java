package cern.nxcals.api.extraction.metadata.queries;

import org.junit.jupiter.api.Test;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class HierarchiesTest {
    @Test
    public void shouldCreateCondition() {
        String rsql = toRSQL(Hierarchies.suchThat().id().eq(1L).and()
                .description().notLike("desc%").and()
                .name().lexicallyAfter("value"));

        assertEquals(3,rsql.split(";").length);
    }
}