/*
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN),All Rights Reserved.
 */

package cern.nxcals.api.extraction.data.builders.fluent.v2;

import cern.nxcals.api.extraction.data.builders.fluent.QueryData;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

import static cern.nxcals.api.utils.TimeUtils.getInstantFromString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class StageSequenceVariablesTest {

    private static final String SYSTEM = "SYSTEM_NAME";

    private static final Instant TIME_1 = getInstantFromString("2017-09-13 14:33:50.123456789");
    private static final Instant TIME_2 = getInstantFromString("2017-09-13 15:33:50.123456789");

    private static final String VARIABLE = "VARIABLE_NAME";
    private static final String VARIABLE_WITH_WILDCARD = "VARI%LE_NAME";
    private static final Boolean VARIABLE_WILDCARD_DISABLED = false;
    private static final Boolean VARIABLE_WILDCARD_ENABLED = true;
    private final static Map<String, Boolean> VARIABLE_MAP_SINGLE = ImmutableMap
            .of(VARIABLE, VARIABLE_WILDCARD_DISABLED);
    private final static Map<String, Boolean> VARIABLE_MAP_SINGLE_WILDCARD = ImmutableMap
            .of(VARIABLE_WITH_WILDCARD, VARIABLE_WILDCARD_ENABLED);

    private final static long VARIABLE_ID = 1L;
    private final static long VARIABLE_ID_2 = 2L;
    private final static long VARIABLE_ID_3 = 3L;

    @Test
    public void shouldCreateVariableQuery() {
        // given
        QueryData<Object> query = queryData();
        // when
        testBuilder(query)
                .nameEq(VARIABLE)
                .timeWindow(TIME_1, TIME_2)
                .build();

        // then
        assertEquals(SYSTEM, query.getSystem());
        assertEquals(VARIABLE_MAP_SINGLE, query.getVariables());
        assertEquals(TIME_1, query.getStartTime());
        assertEquals(TIME_2, query.getEndTime());
    }

    @Test
    public void shouldCreateVariableQueryWithWildcard() {
        // given
        QueryData<Object> query = queryData();
        // when
        testBuilder(query).nameLike(VARIABLE_WITH_WILDCARD)
                .timeWindow(TIME_1, TIME_2)
                .build();

        // then
        assertEquals(SYSTEM, query.getSystem());
        assertEquals(VARIABLE_MAP_SINGLE_WILDCARD, query.getVariables());
        assertEquals(TIME_1, query.getStartTime());
        assertEquals(TIME_2, query.getEndTime());
    }

    @Test
    public void shouldAllowMultipleVariables() {
        // given
        QueryData<Object> query = queryData();
        // when
        testBuilder(query).nameEq(VARIABLE)
                .nameEq(VARIABLE_WITH_WILDCARD)
                .timeWindow(TIME_1, TIME_2)
                .build();

        // then
        assertTrue(query.getVariables().containsKey(VARIABLE));
        assertTrue(query.getVariables().containsKey(VARIABLE_WITH_WILDCARD));
    }

    @Test
    public void shouldAllowMultipleVariablesWithSingleCall() {
        // given
        QueryData<Object> query = queryData();
        // when
        testBuilder(query)
                .nameIn(ImmutableList.of(VARIABLE, VARIABLE_WITH_WILDCARD))
                .timeWindow(TIME_1, TIME_2)
                .build();
        // then
        assertTrue(query.getVariables().containsKey(VARIABLE));
        assertTrue(query.getVariables().containsKey(VARIABLE_WITH_WILDCARD));
    }

    @Test
    public void shouldAllowMultipleVariablesWithSingleCallWithVarArgs() {
        // given
        QueryData<Object> query = queryData();
        // when
        testBuilder(query).nameIn(VARIABLE, VARIABLE_WITH_WILDCARD)
                .timeWindow(TIME_1, TIME_2)
                .build();
        // then
        assertTrue(query.getVariables().containsKey(VARIABLE));
        assertTrue(query.getVariables().containsKey(VARIABLE_WITH_WILDCARD));
    }

    @ParameterizedTest
    @MethodSource("idsQueryBuilders")
    public void shouldCreateVariableQueryWithIds(Consumer<QueryData<Object>> builder) {
        // given
        QueryData<Object> query = queryData();
        // when
        builder.accept(query);
        // then
        assertEquals(Set.of(VARIABLE_ID, VARIABLE_ID_2, VARIABLE_ID_3), query.getVariableIds());
    }

    static Object[] idsQueryBuilders() {
        List<Consumer<QueryData<Object>>> consumers = List.of((query) -> testBuilder(query)
                        .idIn(Set.of(VARIABLE_ID, VARIABLE_ID_2)).idEq(VARIABLE_ID_3)
                        .timeWindow(TIME_1, TIME_2)
                        .build(),
                (query) -> StageSequenceVariables.sequence().apply(query)
                        .idIn(Set.of(VARIABLE_ID, VARIABLE_ID_2)).idEq(VARIABLE_ID_3)
                        .timeWindow(TIME_1, TIME_2)
                        .build(),
                (query) -> StageSequenceVariables.sequence().apply(query)
                        .idIn(VARIABLE_ID, VARIABLE_ID_2, VARIABLE_ID_3)
                        .timeWindow(TIME_1, TIME_2)
                        .build()
        );
        return consumers.toArray();
    }

    @Test
    public void shouldCreateVariableQueryWithIdsAndThrowOnNull() {
        // given
        QueryData<Object> query = queryData();
        Set<Long> idsWithNull = new HashSet<>();
        idsWithNull.add(null);
        idsWithNull.addAll(Set.of(VARIABLE_ID, VARIABLE_ID_2, VARIABLE_ID_3));
        VariableStage<Object> builder = testBuilder(query);
        // when
        assertThrows(NullPointerException.class, () -> builder.idIn(idsWithNull));
    }

    @Test
    public void shouldCreateVariableQueryWithIdsAndFilterDuplicates() {
        // given
        QueryData<Object> query = queryData();
        // when
        testBuilder(query)
                .idIn(Set.of(VARIABLE_ID, VARIABLE_ID_2, VARIABLE_ID_3)).idEq(VARIABLE_ID_3)
                .timeWindow(TIME_1, TIME_2)
                .build();
        // then
        assertEquals(Set.of(VARIABLE_ID, VARIABLE_ID_2, VARIABLE_ID_3), query.getVariableIds());
    }

    private QueryData<Object> queryData() {
        return new QueryData<>(x -> 1);
    }

    private static VariableStage<Object> testBuilder(QueryData<Object> queryData) {
        return StageSequenceVariables.sequence().apply(queryData).system(SYSTEM);
    }
}
