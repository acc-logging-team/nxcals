package cern.nxcals.api.extraction.data.builders.fluent;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.Instant;
import java.util.Map;
import java.util.function.Function;

import static cern.nxcals.api.extraction.data.builders.fluent.QueryData.DEVICE_KEY;
import static cern.nxcals.api.extraction.data.builders.fluent.QueryData.PROPERTY_KEY;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;
import static org.mockito.Mockito.mock;

/**
 *
 */
@TestInstance(PER_CLASS)
public class StageSequenceDevicePropertyTest {
    private static final String SYSTEM = "SYSTEM";
    private static final String DEVICE_1 = "DEVICE_1.SAMPLE";
    private static final String PROPERTY_1 = "PROPERTY_1.SAMPLE";
    private static final String DEVICE_2 = "DEVICE_2.SAMPLE";
    private static final String PROPERTY_2 = "PROPERTY_2.SAMPLE";

    private static final String DEVICE_1_WITH_USER_ESCAPING = "DEVICE\\_1.SAMPLE";
    private static final String PROPERTY_1_WITH_USER_ESCAPING = "PRO\\%RTY_1.SAMPLE";

    private static final Instant TIME_1 = Instant.now();
    private static final Instant TIME_2 = Instant.now();

    @Test
    public void shouldThrowWhenSystemIsNull() {
        assertThrows(NullPointerException.class, () -> builder(null));
    }

    @Test
    public void shouldThrowWhenParameterIsNull() {
        assertThrows(NullPointerException.class,
                () -> builder(SYSTEM).startTime(TIME_1).endTime(TIME_2).entity().parameter(null));
    }

    @ParameterizedTest
    @MethodSource("wrongParameterFormattedStrings")
    public void shouldThrowWhenParameterHasWrongFormat(String parameter) {
        assertThrows(IllegalArgumentException.class,
                () -> builder(SYSTEM).startTime(TIME_1).endTime(TIME_2).entity().parameter(parameter));
    }

    @Test
    public void shouldThrowWhenDeviceIsNull() {
        assertThrows(NullPointerException.class,
                () -> builder(SYSTEM).startTime(TIME_1).endTime(TIME_2).entity().device(null));
    }

    @Test
    public void shouldThrowWhenPropertyIsNull() {
        assertThrows(NullPointerException.class,
                () -> builder(SYSTEM).startTime(TIME_1).endTime(TIME_2).entity().device(DEVICE_1).property(null));
    }

    @Test
    public void shouldCreateDevicePropertyQuery() {
        //when
        QueryData<Dataset<Row>> query = builder(SYSTEM).startTime(TIME_1).endTime(TIME_2).entity().device(DEVICE_1)
                .property(PROPERTY_1).entity().data();

        //then
        assertNotNull(query);
        Map<String, Object> result = ImmutableMap.of(DEVICE_KEY, DEVICE_1, PROPERTY_KEY, PROPERTY_1);
        assertTrue(query.getEntities().stream().anyMatch(entityKeyValues -> result.equals(entityKeyValues.getAsMap())));
        assertEquals(TIME_1, query.getStartTime());
        assertEquals(TIME_2, query.getEndTime());
    }

    @Test
    public void shouldCreateDevicePropertyPatternQuery() {
        //when
        QueryData<Dataset<Row>> query = builder(SYSTEM).startTime(TIME_1).endTime(TIME_2).entity().deviceLike(DEVICE_1)
                .propertyLike(PROPERTY_1).entity().data();

        //then
        assertNotNull(query);
        Map<String, Object> result = ImmutableMap.of(DEVICE_KEY, DEVICE_1, PROPERTY_KEY, PROPERTY_1);
        assertTrue(query.getEntities().stream().anyMatch(entityKeyValues -> result.equals(entityKeyValues.getAsMap())));
        assertEquals(TIME_1, query.getStartTime());
        assertEquals(TIME_2, query.getEndTime());
    }

    @Test
    public void shouldAcceptParameterWithEscapedByUserWildcards() {
        //when
        QueryData<Dataset<Row>> query = builder(SYSTEM).startTime(TIME_1).endTime(TIME_2).entity()
                .parameterLike(DEVICE_1_WITH_USER_ESCAPING + "/" + PROPERTY_1_WITH_USER_ESCAPING).entity().data();

        //then
        assertNotNull(query);
        Map<String, Object> result = ImmutableMap
                .of(DEVICE_KEY, DEVICE_1_WITH_USER_ESCAPING, PROPERTY_KEY, PROPERTY_1_WITH_USER_ESCAPING);
        assertTrue(query.getEntities().stream().anyMatch(entityKeyValues -> result.equals(entityKeyValues.getAsMap())));
        assertEquals(TIME_1, query.getStartTime());
        assertEquals(TIME_2, query.getEndTime());
    }

    @Test
    public void shouldAcceptParameter() {
        //when
        QueryData<Dataset<Row>> query = builder(SYSTEM).startTime(TIME_1).endTime(TIME_2).entity()
                .parameter(DEVICE_1 + "/" + PROPERTY_1).entity().data();

        //then
        assertNotNull(query);
        Map<String, Object> result = ImmutableMap
                .of(DEVICE_KEY, DEVICE_1, PROPERTY_KEY, PROPERTY_1);
        assertTrue(query.getEntities().stream().anyMatch(entityKeyValues -> result.equals(entityKeyValues.getAsMap())));
        assertEquals(TIME_1, query.getStartTime());
        assertEquals(TIME_2, query.getEndTime());
    }

    @Test
    public void shouldAcceptMultipleParameters() {
        //when
        QueryData<Dataset<Row>> query = builder(SYSTEM).startTime(TIME_1).endTime(TIME_2).entity()
                .parameter(DEVICE_1 + "/" + PROPERTY_1).entity().parameter(DEVICE_2 + "/" + PROPERTY_2).entity().data();

        //then
        assertNotNull(query);
        Map<String, Object> result1 = ImmutableMap.of(DEVICE_KEY, DEVICE_1, PROPERTY_KEY, PROPERTY_1);
        Map<String, Object> result2 = ImmutableMap.of(DEVICE_KEY, DEVICE_2, PROPERTY_KEY, PROPERTY_2);
        assertTrue(
                query.getEntities().stream().anyMatch(entityKeyValues -> result1.equals(entityKeyValues.getAsMap())));
        assertTrue(
                query.getEntities().stream().anyMatch(entityKeyValues -> result2.equals(entityKeyValues.getAsMap())));
        assertEquals(TIME_1, query.getStartTime());
        assertEquals(TIME_2, query.getEndTime());
    }

    @Test
    public void shouldAcceptMultipleParametersAsList() {
        //when
        QueryData<Dataset<Row>> query = builder(SYSTEM).startTime(TIME_1).endTime(TIME_2).entity()
                .parameters(ImmutableList.of(DEVICE_1 + "/" + PROPERTY_1, DEVICE_2 + "/" + PROPERTY_2)).entity().data();

        //then
        assertNotNull(query);
        Map<String, Object> result1 = ImmutableMap.of(DEVICE_KEY, DEVICE_1, PROPERTY_KEY, PROPERTY_1);
        Map<String, Object> result2 = ImmutableMap.of(DEVICE_KEY, DEVICE_2, PROPERTY_KEY, PROPERTY_2);
        assertTrue(
                query.getEntities().stream().anyMatch(entityKeyValues -> result1.equals(entityKeyValues.getAsMap())));
        assertTrue(
                query.getEntities().stream().anyMatch(entityKeyValues -> result2.equals(entityKeyValues.getAsMap())));
        assertEquals(TIME_1, query.getStartTime());
        assertEquals(TIME_2, query.getEndTime());
    }

    public Object[] wrongParameterFormattedStrings() {
        return new Object[] { DEVICE_1 + "#" + PROPERTY_1, DEVICE_1 + "#/" + PROPERTY_1, DEVICE_1 + "/#" + PROPERTY_1,
                DEVICE_1 + "$/" + PROPERTY_1, DEVICE_1 + "/$" + PROPERTY_1, DEVICE_1 + "*/*" + PROPERTY_1,
                DEVICE_1 + "/*" + PROPERTY_1,

                "/" + PROPERTY_1, DEVICE_1 + "/", DEVICE_1 + "/" + PROPERTY_1 + "/",
                "/" + DEVICE_1 + "/" + PROPERTY_1 };
    }

    private TimeStartStage<EntityAliasStage<DeviceStage<Dataset<Row>>, Dataset<Row>>, Dataset<Row>> builder(
            String system) {
        Function<QueryData<Dataset<Row>>, Dataset<Row>> producer = queryData -> mock(Dataset.class);
        return StageSequenceDeviceProperty.<Dataset<Row>>sequence()
                .apply(new QueryData<>(producer)).system(system);
    }
}