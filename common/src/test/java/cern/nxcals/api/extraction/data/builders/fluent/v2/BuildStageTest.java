package cern.nxcals.api.extraction.data.builders.fluent.v2;

import cern.nxcals.api.extraction.data.builders.fluent.QueryData;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BuildStageTest {
    @Test
    public void shouldAcceptParameterAndFieldAliases() {
        // given
        String alias1 = "alias1";
        String alias2 = "alias2";
        String field1 = "field1";
        String field2 = "field2";
        String field3 = "field3";

        QueryData<Object> queryData = new QueryData<>(x -> 1);
        // build also cause verification of QueryData - it must be initialized
        queryData.setSystem("CMW");
        queryData.setVariableKey("any", false);
        queryData.setStartTime(Instant.now());
        queryData.setEndTime(Instant.now());

        // when
        new BuildStage<>(queryData)
                .fieldAliases(Map.of(alias1, Set.of(field1)))
                .fieldAliases(Map.of(alias1, Set.of(field2, field3), alias2, Set.of())).build();

        // then
        assertEquals(Map.of(alias1, Set.of(field1, field2, field3), alias2, Set.of()), queryData.getAliasFields());
    }
}
