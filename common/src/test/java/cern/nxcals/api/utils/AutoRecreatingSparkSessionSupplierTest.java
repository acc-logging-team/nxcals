package cern.nxcals.api.utils;

import cern.nxcals.common.config.SparkSessionModifier;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AutoRecreatingSparkSessionSupplierTest {

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    SparkSession sparkSession;

    @Mock
    SparkConf sparkConf;

    @Mock
    List<SparkSessionModifier> sessionModifiers = Collections.emptyList();

    @Test
    void shouldCreateSparkSessionOnlyOnce() {
        try (MockedStatic<SparkUtils> utilities = Mockito.mockStatic(SparkUtils.class)) {
            //given
            utilities.when(() -> SparkUtils.createSparkSession(sparkConf)).thenReturn(sparkSession);
            AutoRecreatingSparkSessionSupplier supplier = new AutoRecreatingSparkSessionSupplier(sparkConf,
                    sessionModifiers);
            when(sparkSession.sparkContext().isStopped()).thenReturn(false);
            //when
            supplier.get();
            //then
            utilities.verify(() -> SparkUtils.createSparkSession(sparkConf), times(1));
            //when
            supplier.get();
            //then
            utilities.verify(() -> SparkUtils.createSparkSession(sparkConf), times(1));
        }
    }

    @Test
    void shouldReCreateSparkSessionIfDisabled() {
        try (MockedStatic<SparkUtils> utilities = Mockito.mockStatic(SparkUtils.class)) {
            //given
            AutoRecreatingSparkSessionSupplier supplier = new AutoRecreatingSparkSessionSupplier(sparkConf,
                    sessionModifiers);
            when(sparkSession.sparkContext().isStopped()).thenReturn(true);
            utilities.when(() -> SparkUtils.createSparkSession(sparkConf)).thenReturn(sparkSession);
            //when
            supplier.get();
            //then
            utilities.verify(() -> SparkUtils.createSparkSession(sparkConf), times(1));
            //when
            supplier.get();
            //then
            utilities.verify(() -> SparkUtils.createSparkSession(sparkConf), times(2));
        }
    }

}