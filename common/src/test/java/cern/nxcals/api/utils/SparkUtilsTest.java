package cern.nxcals.api.utils;

import cern.nxcals.api.config.SparkProperties;
import com.google.common.collect.ImmutableMap;
import org.apache.spark.SparkConf;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class SparkUtilsTest {

    private static final String APP_NAME = "name";
    private static final String KEY = "key";
    private static final String VALUE = "value";
    private static final String MASTER_TYPE = "test";

    @Test
    public void shouldCreateConfig() {
        //given
        SparkProperties props = new SparkProperties();
        props.setJars(new String[] {"jar1", "jar2"});
        props.setAppName(APP_NAME);
        Map<String,String> map = new HashMap<>();
        map.put(KEY, VALUE);
        props.setProperties(map);
        props.setMasterType(MASTER_TYPE);

        //when
        SparkConf sparkConf = SparkUtils.createSparkConf(props);

        //then
        assertEquals(VALUE,sparkConf.get(KEY));
        assertEquals(APP_NAME, sparkConf.get("spark.app.name"));
        assertEquals(MASTER_TYPE, sparkConf.get("spark.master"));
        assertEquals("jar1,jar2", sparkConf.get("spark.jars"));
    }

    @Test
    public void shouldCreateRemoteSessionProperties() {
        //given
        String testProperty = "test";
        Map<String, String> testProperties = ImmutableMap.of(testProperty, "2");
        SparkProperties props = SparkProperties.remoteSessionProperties(APP_NAME, () -> testProperties, "test");
        //then
        assertEquals("yarn", props.getMasterType());
        assertEquals(APP_NAME, props.getAppName());
        assertEquals(testProperties.get(testProperty), props.getProperties().get(testProperty));
    }
}