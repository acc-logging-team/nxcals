package cern.nxcals.api.config;

import cern.nxcals.api.security.KerberosRelogin;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.MockedConstruction;
import org.springframework.boot.test.context.runner.ApplicationContextRunner;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mockConstruction;

class AuthenticationContextTest {
    private final ApplicationContextRunner runner = new ApplicationContextRunner()
            .withUserConfiguration(AuthenticationContext.class);

    @ParameterizedTest
    @MethodSource("providePropertiesForKerberosReloginBeanPresence")
    void kerberosReloginBeanShouldBeCreatedWhenProperSystemProperties(String... systemProperties) {
        // Mocking creation of KerberosRelogin to avoid exceptions
        try (MockedConstruction<KerberosRelogin> mockKerberosRelogin = mockConstruction(KerberosRelogin.class)) {
            runner.withSystemProperties(systemProperties)
                    .run(context -> {
                        assertTrue(context.containsBean("kerberos"));
                    });
        }
    }

    @ParameterizedTest
    @MethodSource("providePropertiesForKerberosReloginBeanPresence")
    void kerberosReloginBeanShouldBeCreatedWhenProperProperties(String... properties) {
        // Mocking creation of KerberosRelogin to avoid exceptions
        try (MockedConstruction<KerberosRelogin> mockKerberosRelogin = mockConstruction(KerberosRelogin.class)) {
            runner.withPropertyValues(properties)
                    .run(context -> {
                        assertTrue(context.containsBean("kerberos"));
                    });
        }
    }

    private static Stream<Arguments> providePropertiesForKerberosReloginBeanPresence() {
        return Stream.of(
                Arguments
                        .of((Object) new String[] { "kerberos.principal=testPrincipal", "kerberos.keytab=testKeytab" }),
                Arguments.of((Object) new String[] { "kerberos.principal=testPrincipal", "kerberos.keytab=testKeytab",
                        "kerberos.auth.disable=false" }),
                Arguments.of((Object) new String[] { "kerberos.principal=testPrincipal", "kerberos.keytab=testKeytab",
                        "kerberos.auth.disable=dummy" })
        );
    }

    @ParameterizedTest
    @MethodSource("providePropertiesForKerberosReloginBeanAbsence")
    void kerberosReloginBeanShouldNotBeCreatedWhenUnsatisfyingSystemProperties(String... systemProperties) {
        runner.withSystemProperties(systemProperties)
                .run(context -> {
                    assertTrue(context.containsBean("kerberos"));
                    assertNotNull(context.getBean("kerberos", String.class));
                });
    }

    @ParameterizedTest
    @MethodSource("providePropertiesForKerberosReloginBeanAbsence")
    void kerberosReloginBeanShouldNotBeCreatedWhenUnsatisfyingProperties(String... systemProperties) {
        runner.withPropertyValues(systemProperties)
                .run(context -> {
                    assertTrue(context.containsBean("kerberos"));
                    assertNotNull(context.getBean("kerberos", String.class));

                });
    }

    private static Stream<Arguments> providePropertiesForKerberosReloginBeanAbsence() {
        return Stream.of(
                Arguments.of((Object) new String[] { "kerberos.principal=testPrincipal" }),
                Arguments.of((Object) new String[] { "kerberos.keytab=testKeytab" }),
                Arguments.of((Object) new String[] { "kerberos.principal=testPrincipal", "kerberos.keytab=testKeytab",
                        "kerberos.auth.disable=true" }),
                Arguments.of((Object) new String[] { "kerberos.auth.disable=true" }),
                Arguments.of((Object) new String[] { "kerberos.auth.disable=false" }),
                Arguments.of((Object) new String[] { "kerberos.auth.disable=dummy" }),
                Arguments.of((Object) new String[] { "kerberos.principal=testPrincipal",
                        "kerberos.auth.disable=false" }),
                Arguments.of((Object) new String[] {})
        );
    }
}