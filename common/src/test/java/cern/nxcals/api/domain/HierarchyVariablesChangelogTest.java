package cern.nxcals.api.domain;

import cern.nxcals.common.utils.GlobalObjectMapperProvider;
import cern.nxcals.common.utils.ReflectionUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.time.Instant;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HierarchyVariablesChangelogTest {

    static HierarchyVariablesChangelog HIERARCHY_VARIABLES_CHANGELOG = ReflectionUtils.builderInstance(HierarchyVariablesChangelog.InnerBuilder.class).id(10L)
            .id(1)
            .newHierarchyId(1L).oldHierarchyId(2L).newVariableId(2L).oldVariableId(3L)
            .newSystemId(6L).oldSystemId(7L).createTimeUtc(Instant.now())
            .opType(OperationType.INSERT).clientInfo("client_info").build();

    @Test
    public void shouldSerialize() throws IOException {
        ObjectMapper ob = GlobalObjectMapperProvider.get();
        String s = ob.writeValueAsString(HIERARCHY_VARIABLES_CHANGELOG);
        HierarchyVariablesChangelog newHierarchyChangelog = ob.readValue(s, HierarchyVariablesChangelog.class);
        assertEquals(HIERARCHY_VARIABLES_CHANGELOG.getNewHierarchyId(), newHierarchyChangelog.getNewHierarchyId());
        assertEquals(HIERARCHY_VARIABLES_CHANGELOG, newHierarchyChangelog);
    }

}