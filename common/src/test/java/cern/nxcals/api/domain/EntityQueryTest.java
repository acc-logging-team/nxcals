package cern.nxcals.api.domain;

import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class EntityQueryTest {

    @Test
    void shouldThrowNPEIfNullIsPassedInConstructor() {
        assertAll("For every arguments combination",
                () -> assertThrows(NullPointerException.class, () -> new EntityQuery(null)),
                () -> assertThrows(NullPointerException.class,
                        () -> new EntityQuery(null, Collections.emptyMap())),
                () -> assertThrows(NullPointerException.class,
                        () -> new EntityQuery(Collections.emptyMap(), null)));
    }

    @Test
    void shouldHaveWorkingGetters() {
        Map<String, Object> keyValue = Collections.emptyMap();
        EntityQuery entityQuery = new EntityQuery(keyValue, keyValue);
        assertAll(
                () -> assertEquals(keyValue, entityQuery.getKeyValues()),
                () -> assertEquals(keyValue, entityQuery.getKeyValuesLike())
        );
    }

    @Test
    void shouldCorrectlyTransformToMapAndEscapeWildcardCharacters() {
        String keyWithWildcard = "key%";
        String key2 = "key";
        String valueWithWildcard = "value%";
        String valueWithEscapedWildcard = "value\\%";
        Map<String, Object> keyValues = Map.of(keyWithWildcard, valueWithWildcard);
        Map<String, Object> keyValuesLike = Map.of(key2, valueWithWildcard);
        EntityQuery entityQuery = new EntityQuery(keyValues, keyValuesLike);
        assertEquals(Map.of(keyWithWildcard, valueWithEscapedWildcard, key2, valueWithWildcard),
                entityQuery.toMap());
    }
}