package cern.nxcals.api.domain;

import cern.nxcals.common.utils.GlobalObjectMapperProvider;
import cern.nxcals.common.utils.ReflectionUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.avro.SchemaBuilder;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static cern.nxcals.api.domain.TimeWindow.between;
import static java.util.Collections.emptySortedSet;
import static java.util.Collections.singletonMap;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class EntityHistoryTest {
    private static final SystemSpec SYSTEM_SPEC = SystemSpec.builder().name("TEST").entityKeyDefinitions("test")
            .partitionKeyDefinitions("test").timeKeyDefinitions("test").build();
    private static final Partition PARTITION = Partition.builder().systemSpec(SYSTEM_SPEC)
            .keyValues(singletonMap("a", "b")).build();
    private static final String SCHEMA = SchemaBuilder.record("schema1").fields().name("schema1").type().stringType()
            .noDefault().endRecord().toString();
    private static final Entity ENTITY = Entity.builder().systemSpec(SYSTEM_SPEC).partition(PARTITION)
            .entityKeyValues(singletonMap("a", "b")).entityHistory(emptySortedSet()).build();

    @Test
    public void shouldCompareEntityHistories() {
        TimeWindow firstWindow = between(10, 20);
        TimeWindow secondWindow = between(20, 30);
        EntityHistory firstEntityHistory = createHistory(firstWindow, 10L, SCHEMA, PARTITION);
        EntityHistory secondEntityHistory = createHistory(secondWindow, 10L, SCHEMA, PARTITION);
        assertEquals(1, firstEntityHistory.compareTo(secondEntityHistory));
    }

    @Test
    public void shouldNotCreateHistoryWithoutSchema() {
        assertThrows(NullPointerException.class, () -> createHistory(between(10, 20), 10L, null, PARTITION));
    }

    @Test
    public void shouldNotCreateHistoryWithoutPartition() {
        assertThrows(NullPointerException.class, () -> createHistory(between(10, 20), 10L, SCHEMA, null));
    }

    @Test
    public void shouldNotCreateHistoryWithoutValidity() {
        assertThrows(NullPointerException.class, () -> createHistory(null, 10L, SCHEMA, PARTITION));
    }

    @Test
    public void shouldSerialize() throws IOException {
        EntityHistory entityHistory1 = createHistory(between(10, 20), 10L, SCHEMA, PARTITION);
        ObjectMapper ob = GlobalObjectMapperProvider.get();
        String s = ob.writeValueAsString(entityHistory1);
        EntityHistory newEntityHistory = ob.readValue(s, EntityHistory.class);

        assertEquals(newEntityHistory.getValidity(), entityHistory1.getValidity());
        assertEquals(newEntityHistory.getPartition(), entityHistory1.getPartition());
        assertEquals(newEntityHistory.getEntitySchema(), entityHistory1.getEntitySchema());
        assertEquals(newEntityHistory.getId(), entityHistory1.getId());
        assertEquals(newEntityHistory, entityHistory1);
    }

    private EntityHistory createHistory(TimeWindow between, long id, String schema, Partition partition) {
        return ReflectionUtils.builderInstance(EntityHistory.InnerBuilder.class).id(id).entitySchema(createSchema(schema, 1L))
                .entity(ENTITY).partition(partition).validity(between).build();
    }

    private static EntitySchema createSchema(String s, long id) {
        return ReflectionUtils.builderInstance(EntitySchema.InnerBuilder.class).id(id).schemaJson(s).build();
    }

}
