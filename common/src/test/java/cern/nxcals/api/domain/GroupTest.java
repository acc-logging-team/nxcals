package cern.nxcals.api.domain;

import cern.nxcals.common.utils.GlobalObjectMapperProvider;
import cern.nxcals.common.utils.ReflectionUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableSet;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

import static java.util.Collections.singleton;
import static java.util.Collections.singletonMap;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class GroupTest {
    private ObjectMapper ob = GlobalObjectMapperProvider.get();

    @Test
    public void shouldNotAllowNullForName() {
        assertThrows(NullPointerException.class, () -> groupBuilder(null).build());
    }

    @Test
    public void shouldAllowNullForLabel() {
        assertNull(groupBuilder("name").label(null).build().getLabel());
    }

    @Test
    public void shouldEqualCorrectly() {
        Group group = groupBuilder("name").build();
        assertEquals(group, group);
        assertEquals(group, group.toBuilder().build());
        assertEquals(group, group.toBuilder().build());
        assertNotEquals(group,
                group.toBuilder().systemSpec(systemSpec().toBuilder().name("other-system").build()).build());
        assertNotEquals(group, group.toBuilder().name("other-name").build());
    }

    @Test
    public void shouldHashCorrectly() {
        Group group = groupBuilder("name").build();

        Set<Group> setA = ImmutableSet.of(group);
        assertThat(setA).contains(group);
        assertThat(setA).contains(group.toBuilder().build());
        assertThat(setA).contains(group.toBuilder().build());
        assertThat(setA).doesNotContain(
                group.toBuilder().systemSpec(systemSpec().toBuilder().name("other-system").build()).build());
        assertThat(setA).doesNotContain(group.toBuilder().name("other-name").build());

        Set<Group> setB = ImmutableSet.of(group, group.toBuilder().name("yet-another-name").build());
        assertThat(setB).contains(group);
        assertThat(setB).contains(group.toBuilder().build());
        assertThat(setB).contains(group.toBuilder().build());
        assertThat(setB).doesNotContain(
                group.toBuilder().systemSpec(systemSpec().toBuilder().name("other-system").build()).build());
        assertThat(setB).doesNotContain(group.toBuilder().name("other-name").build());

        Set<Group> setC = ImmutableSet.of(group, group.toBuilder().build());
        assertEquals(1, setC.size());
    }

    @Test
    public void shouldSerializeRaw() throws IOException {
        Group groupA = groupBuilder("name")
                .description("DESCRIPTION")
                .property("prop_name", "prop_value")
                .visibility(Visibility.PROTECTED)
                .build();

        String s = ob.writeValueAsString(groupA);
        Group groupB = ob.readValue(s, Group.class);

        assertEqual(groupA, groupB);
        assertEqual(groupA.toBuilder().build(), groupB);
        assertEqual(groupA, groupB.toBuilder().build());
    }

    @Test
    public void shouldSerializeSnapshot() throws IOException {
        Group groupA = Group.builder().label("my-snapshot-label")
                .systemSpec(systemSpec())
                .name("name")
                .description("DESCRIPTION")
                .property("pn1", "pv1")
                .property("pn2", "pv2")
                .visibility(Visibility.PUBLIC)
                .owner("OWNER")
                .build();

        String s = ob.writeValueAsString(groupA);

        Group groupB = ob.readValue(s, Group.class);

        assertEqual(groupA, groupB);
        assertEqual(groupA.toBuilder().build(), groupB);
        assertEqual(groupA, groupB.toBuilder().build());
    }

    @Test
    public void shouldSerializeGroup() throws IOException {
        Group groupA = Group.builder().label("my-group-label")
                .systemSpec(systemSpec())
                .name("name")
                .description("DESCRIPTION")
                .property("pn1", "pv1")
                .property("pn2", "pv2")
                .visibility(Visibility.PUBLIC)
                .owner("OWNER")
                .build();

        String s = ob.writeValueAsString(groupA);

        Group groupB = ob.readValue(s, Group.class);

        assertEqual(groupA, groupB);
        assertEqual(groupA.toBuilder().build(), groupB);
        assertEqual(groupA, groupB.toBuilder().build());
    }

    @Test
    public void shouldCopyAllDataOnToBuilder() {
        Group groupA = groupBuilder("name")
                .description("DESCRIPTION")
                .property("prop_name", "prop_value")
                .visibility(Visibility.PUBLIC)
                .owner("OWNER")
                .build();

        Group groupB = groupA.toBuilder().build();

        assertEqual(groupA, groupB);
        assertEqual(groupA.toBuilder().build(), groupB);
        assertEqual(groupA, groupB.toBuilder().build());
    }

    private Group.Builder groupBuilder(String name) {
        return ReflectionUtils.builderInstance(Group.InnerBuilder.class).id(10L)
                .recVersion(20L).owner("OWNER").systemSpec(systemSpec())
                .visibility(Visibility.PROTECTED)
                .label("my-snapshot-label").name(name);
    }

    private Variable variable(String variable) {
        return Variable.builder()
                .variableName(variable)
                .configs(new TreeSet<>(singleton(VariableConfig.builder()
                        .entityId(1L)
                        .fieldName("field")
                        .variableName(variable)
                        .validity(TimeWindow.infinite())
                        .build())))
                .systemSpec(systemSpec())
                .build();
    }

    private Entity entity(String key) {
        return Entity.builder()
                .partition(Partition.builder()
                        .systemSpec(systemSpec())
                        .keyValues(singletonMap(key, "b"))
                        .build())
                .entityKeyValues(singletonMap(key, "b"))
                .systemSpec(systemSpec())
                .build();
    }

    private SystemSpec systemSpec() {
        return SystemSpec.builder().name("name").partitionKeyDefinitions("").timeKeyDefinitions("")
                .entityKeyDefinitions("").recordVersionKeyDefinitions("").build();
    }

    private void assertEqual(Group groupA, Group groupB) {
        assertEquals(groupA, groupB);
        assertEquals(groupA.getId(), groupB.getId());
        assertEquals(groupA.getRecVersion(), groupB.getRecVersion());
        assertEquals(groupA.getName(), groupB.getName());
        assertEquals(groupA.getSystemSpec(), groupB.getSystemSpec());
        assertEquals(groupA.getLabel(), groupB.getLabel());
        assertThat(groupA.getProperties()).containsAllEntriesOf(groupB.getProperties());
        assertThat(groupB.getProperties()).containsAllEntriesOf(groupA.getProperties());
        assertEquals(groupA.getDescription(), groupB.getDescription());
        assertEquals(groupA.getOwner(), groupB.getOwner());
        assertEquals(groupA.isVisible(), groupB.isVisible());
    }
}
