package cern.nxcals.api.domain;

import cern.nxcals.common.utils.GlobalObjectMapperProvider;
import cern.nxcals.common.utils.ReflectionUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class EntitySchemaTest {

    static final Schema SCHEMA = SchemaBuilder.record("schema1").fields()
            .name("schema1").type().stringType().noDefault().endRecord();

    @Test
    public void shouldNotCreateSchemaWithoutSchemaJson() {
        assertThrows(NullPointerException.class, () -> createSchema(null));
    }

    @Test
    public void shouldSerialize() throws IOException {
        EntitySchema entitySchema1 = createSchema(SCHEMA.toString());
        ObjectMapper ob = GlobalObjectMapperProvider.get();
        String s = ob.writeValueAsString(entitySchema1);
        EntitySchema entitySchema2 = ob.readValue(s, EntitySchema.class);
        assertEquals(entitySchema1.getSchema(), entitySchema2.getSchema());
        assertEquals(entitySchema1.getSchemaJson(), entitySchema2.getSchemaJson());
        assertEquals(entitySchema1.getId(), entitySchema2.getId());
        assertEquals(entitySchema1, entitySchema2);
    }

    private EntitySchema createSchema(String s) {
        return ReflectionUtils.builderInstance(EntitySchema.InnerBuilder.class).id(0).schemaJson(s).build();
    }
}
