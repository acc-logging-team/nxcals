package cern.nxcals.api.domain;

import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.common.utils.GlobalObjectMapperProvider;
import cern.nxcals.common.utils.ReflectionUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableSet;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Set;

import static java.util.Collections.singletonMap;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PartitionResourceHistoryTest {
    private final Instant startTime = TimeUtils.getInstantFromNanos((System.currentTimeMillis() - 100000) * 1_000_000);
    private final Instant endTime = Instant.now();
    private ObjectMapper ob = GlobalObjectMapperProvider.get();

    @Test
    public void shouldNotAllowNullForPartitionResource() {
        PartitionResourceHistory.Builder partitionResouceInfoWithouthPartitionResource = partitionResourceHistoryBuilder()
                .partitionResource(null);
        assertThrows(NullPointerException.class,
                partitionResouceInfoWithouthPartitionResource::build);
    }

    @Test
    public void shouldNotAllowNullForPartitionInformation() {
        PartitionResourceHistory.Builder partitionResouceInfoWithouthPartitionResource = partitionResourceHistoryBuilder()
                .partitionInformation(null);
        assertThrows(NullPointerException.class,
                partitionResouceInfoWithouthPartitionResource::build);
    }

    @Test
    public void shouldNotAllowNullForCompactionType() {
        PartitionResourceHistory.Builder partitionResouceInfoWithouthCompactionType = partitionResourceHistoryBuilder()
                .compactionType(null);
        assertThrows(NullPointerException.class,
                partitionResouceInfoWithouthCompactionType::build);
    }

    @Test
    public void shouldNotAllowNullForStorageType() {
        PartitionResourceHistory.Builder partitionResouceInfoWithouthStorageType = partitionResourceHistoryBuilder()
                .storageType(null);
        assertThrows(NullPointerException.class, partitionResouceInfoWithouthStorageType::build);
    }

    @Test
    public void shouldNotAllowNullForValidty() {
        PartitionResourceHistory.Builder partitionResouceInfoWithouthStartTime = partitionResourceHistoryBuilder()
                .validity(null).partitionInformation("info").compactionType("type").storageType("type");
        assertThrows(NullPointerException.class, partitionResouceInfoWithouthStartTime::build);
    }

    @Test
    public void shouldEqualCorrectly() {
        PartitionResourceHistory partitionResourceHistory = partitionResourceHistoryBuilder().build();
        assertAll(
                () -> assertEquals(partitionResourceHistory, partitionResourceHistory),
                () -> assertEquals(partitionResourceHistory, partitionResourceHistory.toBuilder().build()),
                () -> assertEquals(partitionResourceHistory, partitionResourceHistory.toBuilder().build()),
                () -> assertNotEquals(partitionResourceHistory,
                        partitionResourceHistory.toBuilder()
                                .partitionResource(partitionResource().toBuilder()
                                        .systemSpec(systemSpec().toBuilder().name("other-system").build())
                                        .build())
                                .build()),
                () -> assertNotEquals(partitionResourceHistory,
                        partitionResourceHistory.toBuilder().partitionInformation("New Information").build()),
                () -> assertNotEquals(partitionResourceHistory,
                        partitionResourceHistory.toBuilder().compactionType("New CompactionType").build()),
                () -> assertNotEquals(partitionResourceHistory,
                        partitionResourceHistory.toBuilder().storageType("New StorageType").build()),
                () -> assertNotEquals(partitionResourceHistory,
                        partitionResourceHistory.toBuilder()
                                .validity(TimeWindow.between(Instant.now(), Instant.now().plus(1, ChronoUnit.DAYS)))
                                .build())
        );
    }

    @Test
    public void shouldHashCorrectly() {
        PartitionResourceHistory partitionResourceHistory = partitionResourceHistoryBuilder().build();

        Set<PartitionResourceHistory> setA = ImmutableSet.of(partitionResourceHistory);
        assertThat(setA).contains(partitionResourceHistory)
                .contains(partitionResourceHistory.toBuilder().build())
                .contains(partitionResourceHistory.toBuilder().build())
                .doesNotContain(
                        partitionResourceHistory.toBuilder().partitionResource(partitionResource().toBuilder()
                                .systemSpec(systemSpec().toBuilder().name("other-system").build())
                                .build())
                                .build());
        Set<PartitionResourceHistory> setB = ImmutableSet.of(partitionResourceHistory,
                partitionResourceHistory.toBuilder().partitionResource(partitionResource().toBuilder()
                        .systemSpec(systemSpec().toBuilder().name("yet-another-system").build())
                        .build())
                        .build());
        assertThat(setB).contains(partitionResourceHistory)
                .contains(partitionResourceHistory.toBuilder().build())
                .contains(partitionResourceHistory.toBuilder().build())
                .doesNotContain(
                        partitionResourceHistory.toBuilder().partitionResource(partitionResource().toBuilder()
                                .systemSpec(systemSpec().toBuilder().name("other-system").build())
                                .build())
                                .build());
        Set<PartitionResourceHistory> setC = ImmutableSet
                .of(partitionResourceHistory, partitionResourceHistory.toBuilder().build());
        assertEquals(1, setC.size());
    }

    @Test
    public void shouldSerializeRaw() throws IOException {
        PartitionResourceHistory partitionResourceHistoryA = partitionResourceHistoryBuilder()
                .partitionResource(partitionResource())
                .partitionInformation("Information")
                .compactionType("CompactionType")
                .storageType("StorageType")
                .isFixedSettings(false)
                .validity(TimeWindow.between(startTime, endTime))
                .build();

        assertCorrectness(partitionResourceHistoryA);
    }

    @Test
    public void shouldSerializeSnapshot() throws IOException {
        PartitionResourceHistory partitionResourceHistoryA = partitionResourceHistoryBuilder()
                .partitionResource(partitionResource())
                .partitionInformation("Information")
                .compactionType("CompactionType")
                .storageType("StorageType")
                .isFixedSettings(false)
                .validity(TimeWindow.between(startTime, endTime))
                .build();

        assertCorrectness(partitionResourceHistoryA);
    }

    @Test
    public void shouldSerializePartitionResourceHistory() throws IOException {
        PartitionResourceHistory partitionResourceHistoryA = PartitionResourceHistory.builder()
                .partitionResource(partitionResource())
                .partitionInformation("Information")
                .compactionType("CompactionType")
                .storageType("StorageType")
                .isFixedSettings(false)
                .validity(TimeWindow.between(startTime, endTime))
                .build();

        assertCorrectness(partitionResourceHistoryA);
    }

    @Test
    public void shouldCopyAllDataOnToBuilder() throws IOException {
        PartitionResourceHistory partitionResourceHistoryA = partitionResourceHistoryBuilder()
                .partitionResource(partitionResource())
                .partitionInformation("Information")
                .compactionType("CompactionType")
                .storageType("StorageType")
                .isFixedSettings(false)
                .validity(TimeWindow.between(startTime, endTime))
                .build();

        assertCorrectness(partitionResourceHistoryA);
    }

    @Test
    public void shouldReturnToInstant() {
        //given
        Instant from = TimeUtils.getInstantFromString("2024-01-01 01:00:00.0");
        //when
        Instant to = PartitionResourceHistory.to(from);
        //then
        assertEquals("2024-01-02 01:00:00.0", TimeUtils.getStringFromInstant(to));

    }

    private PartitionResourceHistory.Builder partitionResourceHistoryBuilder() {
        return ReflectionUtils.builderInstance(PartitionResourceHistory.InnerBuilder.class)
                .id(10L)
                .partitionResource(partitionResource())
                .partitionInformation("Information")
                .compactionType("CompactionType")
                .storageType("StorageType")
                .isFixedSettings(false)
                .validity(TimeWindow.between(startTime, endTime))
                .recVersion(20L);
    }

    private PartitionResource partitionResource() {
        return PartitionResource.builder().systemSpec(systemSpec()).partition(partition()).schema(schema()).build();
    }

    private SystemSpec systemSpec() {
        return SystemSpec.builder().name("name").partitionKeyDefinitions("").timeKeyDefinitions("")
                .entityKeyDefinitions("").recordVersionKeyDefinitions("").build();
    }

    private Partition partition() {
        return Partition.builder().keyValues(singletonMap("a", "b")).systemSpec(systemSpec()).build();
    }

    private EntitySchema schema() {
        return EntitySchema.builder().schemaJson("").build();
    }

    private void assertEqual(PartitionResourceHistory partitionResourceHistoryA,
            PartitionResourceHistory partitionResourceHistoryB) {
        assertAll(
                () -> assertEquals(partitionResourceHistoryA, partitionResourceHistoryB),
                () -> assertEquals(partitionResourceHistoryA.getId(), partitionResourceHistoryB.getId()),
                () -> assertEquals(partitionResourceHistoryA.getRecVersion(),
                        partitionResourceHistoryB.getRecVersion()),
                () -> assertEquals(partitionResourceHistoryA.getPartitionResource(),
                        partitionResourceHistoryB.getPartitionResource()),
                () -> assertEquals(partitionResourceHistoryA.getPartitionInformation(),
                        partitionResourceHistoryB.getPartitionInformation()),
                () -> assertEquals(partitionResourceHistoryA.getCompactionType(),
                        partitionResourceHistoryB.getCompactionType()),
                () -> assertEquals(partitionResourceHistoryA.getStorageType(),
                        partitionResourceHistoryB.getStorageType()),
                () -> assertEquals(partitionResourceHistoryA.isFixedSettings(),
                        partitionResourceHistoryB.isFixedSettings()),
                () -> assertEquals(partitionResourceHistoryA.getValidity(), partitionResourceHistoryB.getValidity())
        );
    }

    private void assertCorrectness(PartitionResourceHistory partitionResourceHistoryA) throws JsonProcessingException {
        String s = ob.writeValueAsString(partitionResourceHistoryA);

        PartitionResourceHistory partitionResourceHistoryB = ob.readValue(s, PartitionResourceHistory.class);

        assertEqual(partitionResourceHistoryA, partitionResourceHistoryB);
        assertEqual(partitionResourceHistoryA.toBuilder().build(), partitionResourceHistoryB);
        assertEqual(partitionResourceHistoryA, partitionResourceHistoryB.toBuilder().build());
    }
}
