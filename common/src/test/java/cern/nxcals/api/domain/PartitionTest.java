package cern.nxcals.api.domain;

import cern.nxcals.common.utils.GlobalObjectMapperProvider;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static java.util.Collections.singletonMap;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PartitionTest {

    static final SystemSpec SYSTEM_SPEC = SystemSpec.builder().entityKeyDefinitions("test")
            .partitionKeyDefinitions("test")
            .name("TEST").timeKeyDefinitions("test").build();
    static final Map<String, Object> PARTITION_KEY_VALUES = ImmutableMap.of("name", "3");

    @Test
    public void shouldNotCreatePartitionWithoutSystemSpec() {
        assertThrows(NullPointerException.class, () -> Partition.builder().keyValues(PARTITION_KEY_VALUES).build());
    }

    @Test
    public void shouldNotCreatePartitionWithoutKeyValues() {
        assertThrows(NullPointerException.class, () -> Partition.builder().systemSpec(SYSTEM_SPEC).build());
    }

    @Test
    public void shouldSerialize() throws IOException {
        Partition p1 = Partition.builder().systemSpec(SYSTEM_SPEC).keyValues(PARTITION_KEY_VALUES).build();
        ObjectMapper ob = GlobalObjectMapperProvider.get();
        String s = ob.writeValueAsString(p1);
        Partition p2 = ob.readValue(s, Partition.class);

        assertEquals(p1.getId(), p2.getId());
        assertEquals(p1.getKeyValues(), p2.getKeyValues());
        assertEquals(p1.getSystemSpec(), p2.getSystemSpec());
        assertEquals(p1.getProperties(), p2.getProperties());
        assertEquals(p1, p2);
    }

    @Test
    public void shouldCopyInputMapAndChangeWithoutAffectingOriginalMap() {
        Map<String, Object> kv1 = new HashMap<>();
        kv1.put("nameA", "val");

        Partition p = Partition.builder().systemSpec(SYSTEM_SPEC).keyValues(kv1).build();
        assertThat(p.getKeyValues()).hasSize(1);

        kv1.put("nameB", "val");

        assertThat(p.getKeyValues()).hasSize(1);

        Map<String, Object> kv2 = new HashMap<>();
        kv2.put("nameA", "val");
        Partition p2 = p.toBuilder().keyValues(kv2).build();
        assertThat(p2.getKeyValues()).hasSize(1);
        kv2.put("nameB", "val");

        assertThat(p.getKeyValues()).hasSize(1);
    }

    @Test
    public void shouldOutputMapBeImmutable() {
        Partition p = Partition.builder().systemSpec(SYSTEM_SPEC).keyValues(singletonMap("nameA", "val")).build();

        assertThrows(UnsupportedOperationException.class, () -> p.getKeyValues().put("c", "d"));
    }
}
