package cern.nxcals.api.domain;

import cern.nxcals.common.utils.GlobalObjectMapperProvider;
import cern.nxcals.common.utils.ReflectionUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.time.Instant;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HierarchyChangelogTest {

    static HierarchyChangelog HIERARCHY_CHANGELOG = ReflectionUtils.builderInstance(HierarchyChangelog.InnerBuilder.class).id(10L)
            .hierarchyId(1)
            .newHierarchyName("new_h_name").oldHierarchyName("old_h_name").newParentId(2L).oldParentId(3L)
            .newSystemId(6L).oldSystemId(7L).createTimeUtc(Instant.now())
            .opType(OperationType.INSERT).clientInfo("client_info").build();

    @Test
    public void shouldSerialize() throws IOException {
        ObjectMapper ob = GlobalObjectMapperProvider.get();
        String s = ob.writeValueAsString(HIERARCHY_CHANGELOG);
        HierarchyChangelog newHierarchyChangelog = ob.readValue(s, HierarchyChangelog.class);
        assertEquals(HIERARCHY_CHANGELOG.getHierarchyId(), newHierarchyChangelog.getHierarchyId());
        assertEquals(HIERARCHY_CHANGELOG, newHierarchyChangelog);
    }

}