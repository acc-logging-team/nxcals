package cern.nxcals.api.domain;

import cern.nxcals.common.utils.GlobalObjectMapperProvider;
import cern.nxcals.common.utils.ReflectionUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class HierarchyTest {
    private ObjectMapper ob = GlobalObjectMapperProvider.get();

    private static final SystemSpec SYSTEM_SPEC = SystemSpec.builder().name("").partitionKeyDefinitions("")
            .timeKeyDefinitions("").entityKeyDefinitions("").build();
    private static final String LEGAL_NAME = "0Aa_11 B-.";
    private static final String ILLEGAL_NAME = "A/B";

    @Test
    public void shouldNotBuildHierarchyWithSpaceAsName() {
        assertThrows(IllegalArgumentException.class, () -> Hierarchy.builder().name(" ").description("description").systemSpec(SYSTEM_SPEC).build());
    }

    @Test
    public void shouldNotBuildHierarchyWithLeadingSpaceAsName() {
        assertThrows(IllegalArgumentException.class, () -> Hierarchy.builder().name(" \\test").description("description").systemSpec(SYSTEM_SPEC).build());
    }

    @Test
    public void shouldNotBuildHierarchyWithSlashInName() {
        assertThrows(IllegalArgumentException.class, () -> Hierarchy.builder().name("aaa/bbbb").description("description").systemSpec(SYSTEM_SPEC).build());
    }

    @Test
    public void shouldNotCreateHierarchyWithoutSystem() {
        assertThrows(NullPointerException.class, () -> Hierarchy.builder().name(LEGAL_NAME).description("description").build());
    }

    @Test
    public void shouldNotAllowIllegalCharactersInName() {
        assertThrows(IllegalArgumentException.class, () -> Hierarchy.builder().name(ILLEGAL_NAME).description("description").systemSpec(SYSTEM_SPEC).build());
    }

    @Test
    public void shouldNotAllowIllegalCharactersInNameAfterToBuilder() {
        assertThrows(IllegalArgumentException.class, () -> Hierarchy.builder().name(LEGAL_NAME).description("description").systemSpec(SYSTEM_SPEC).build().toBuilder()
                .name(ILLEGAL_NAME).build());
    }

    @Test
    public void shouldPermitBackSlashCharactersInName() {
        Hierarchy.builder().name("\\valid \\ name\\").description("description").systemSpec(SYSTEM_SPEC).build()
                .toBuilder().build();
    }

    @Test
    public void shouldParentHaveId() {
        Hierarchy h1 = Hierarchy.builder().name("A").systemSpec(SYSTEM_SPEC).build();
        assertThrows(IllegalArgumentException.class, () -> Hierarchy.builder().name("B").parent(h1).systemSpec(SYSTEM_SPEC).build());
    }

    @Test
    public void shouldChangePathOnNameChange() {
        Hierarchy h1 = ReflectionUtils.builderInstance(Hierarchy.InnerBuilder.class).id(10).name("A")
                .systemSpec(SYSTEM_SPEC).build();
        Hierarchy h2 = ReflectionUtils.builderInstance(Hierarchy.InnerBuilder.class).id(20).name("B").systemSpec(SYSTEM_SPEC).build();
        Hierarchy h3 = ReflectionUtils.builderInstance(Hierarchy.InnerBuilder.class).id(30).name("C").parent(h1).systemSpec(SYSTEM_SPEC).build();

        assertEquals("/A/C", h3.getNodePath());
        assertEquals("/A/D", h3.toBuilder().name("D").build().getNodePath());
        assertEquals("/B/C", h3.toBuilder().parent(h2).build().getNodePath());
    }

    @Test
    public void childrenShouldNeverBeNull() {
        Hierarchy h1 = Hierarchy.builder().name("A").systemSpec(SYSTEM_SPEC).build();
        Hierarchy h2 = h1.toBuilder().build();
        Hierarchy h3 = ReflectionUtils.builderInstance(Hierarchy.InnerBuilder.class).name("A").systemSpec(SYSTEM_SPEC).build();

        assertNotNull(h1.getChildren());
        assertNotNull(h2.getChildren());
        assertNotNull(h3.getChildren());
    }

    @Test
    public void childrenShouldBeImmutable() {
        Hierarchy h1 = Hierarchy.builder().name("child").systemSpec(SYSTEM_SPEC).build();
        Hierarchy h2 = ReflectionUtils.builderInstance(Hierarchy.InnerBuilder.class).name("A")
                .systemSpec(SYSTEM_SPEC)
                .child(h1)
                .build();

        assertThrows(UnsupportedOperationException.class, () -> h1.getChildren().add(h2));
    }

    @Test
    public void serializationTest() throws IOException {
        Hierarchy h1 = ReflectionUtils.builderInstance(Hierarchy.InnerBuilder.class)
                .id(10)
                .name("A")
                .nodePath("/A")
                .description("description_A")
                .systemSpec(SYSTEM_SPEC)
                .leaf(false)
                .level(1)
                .recVersion(1)
                .build();

        Hierarchy h3 = ReflectionUtils.builderInstance(Hierarchy.InnerBuilder.class)
                .id(11)
                .name("C")
                .nodePath("/A/B/C")
                .description("description_C")
                .systemSpec(SYSTEM_SPEC)
                .leaf(true)
                .level(3)
                .recVersion(2)
                .build();

        Hierarchy h4 = ReflectionUtils.builderInstance(Hierarchy.InnerBuilder.class)
                .id(12)
                .name("D")
                .nodePath("/A/B/D")
                .description("description_D")
                .systemSpec(SYSTEM_SPEC)
                .leaf(true)
                .level(3)
                .recVersion(3)
                .build();

        Hierarchy h2 = ReflectionUtils.builderInstance(Hierarchy.InnerBuilder.class)
                .id(13)
                .name("B")
                .nodePath("/A/B")
                .parent(h1)
                .child(h3)
                .child(h4)
                .description("description_B")
                .systemSpec(SYSTEM_SPEC)
                .leaf(false)
                .level(2)
                .recVersion(4)
                .build();

        String s = ob.writeValueAsString(h2);

        Hierarchy deserialized = ob.readValue(s, Hierarchy.class);

        assertEquals(2, deserialized.getChildren().size());

        assertEqual(h2, deserialized);
        assertEqual(h2.toBuilder().build(), deserialized);
        assertEqual(h2, deserialized.toBuilder().build());
    }

    private void assertEqual(Hierarchy hierarchyA, Hierarchy hierarchyB) {
        assertEquals(hierarchyA, hierarchyB);
        assertEquals(hierarchyA.getId(), hierarchyB.getId());
        assertEquals(hierarchyA.getName(), hierarchyB.getName());
        assertEquals(hierarchyA.getNodePath(), hierarchyB.getNodePath());
        assertEquals(hierarchyA.getSystemSpec(), hierarchyB.getSystemSpec());
        assertEquals(hierarchyA.getRecVersion(), hierarchyB.getRecVersion());
        assertEquals(hierarchyA.getLevel(), hierarchyB.getLevel());
        assertEquals(hierarchyA.isLeaf(), hierarchyB.isLeaf());
        assertEquals(hierarchyA.getDescription(), hierarchyB.getDescription());

        assertEqual(hierarchyA.getParent(), hierarchyB.getParent());

        List<HierarchyView> childrenA = new ArrayList<>(hierarchyA.getChildren());
        childrenA.sort((o1, o2) -> o1.getName().compareTo(o2.getName()));

        List<HierarchyView> childrenB = new ArrayList<>(hierarchyB.getChildren());
        childrenB.sort((o1, o2) -> o1.getName().compareTo(o2.getName()));

        assertEquals(childrenA.size(), childrenB.size());

        for (int i = 0; i < childrenA.size(); i++) {
            assertEqual(childrenA.get(i), childrenB.get(i));
        }
    }

    private void assertEqual(HierarchyView hierarchyA, HierarchyView hierarchyB) {
        assertEquals(hierarchyA, hierarchyB);
        assertEquals(hierarchyA.getId(), hierarchyB.getId());
        assertEquals(hierarchyA.getName(), hierarchyB.getName());
        assertEquals(hierarchyA.getDescription(), hierarchyB.getDescription());
        assertEquals(hierarchyA.getLevel(), hierarchyB.getLevel());
        assertEquals(hierarchyA.isLeaf(), hierarchyB.isLeaf());
    }
}
