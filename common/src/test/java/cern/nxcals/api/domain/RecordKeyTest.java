package cern.nxcals.api.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RecordKeyTest {

    @Test
    public void shouldSerialize() {
        //given
        RecordKey rk = RecordKey.builder().entityId(1).partitionId(2).schemaId(3).systemId(4).timestamp(Long.MAX_VALUE)
                .build();

        //when
        byte[] array = rk.serialize();

        //them
        assertTrue(RecordKey.isValid(array));

    }

    @Test
    public void shouldDeserialize() {
        //given
        RecordKey rk = RecordKey.builder().entityId(1).partitionId(2).schemaId(3).systemId(4).timestamp(Long.MAX_VALUE)
                .build();
        byte[] array = rk.serialize();
        //when
        RecordKey rk2 = RecordKey.deserialize(array);

        //them
        assertEquals(rk.getEntityId(), rk2.getEntityId());
        assertEquals(rk.getPartitionId(), rk2.getPartitionId());
        assertEquals(rk.getSchemaId(), rk2.getSchemaId());
        assertEquals(rk.getSystemId(), rk2.getSystemId());
        assertEquals(rk.getTimestamp(), rk2.getTimestamp());
    }

    @Test
    public void shouldThrowOnWrongMagicBytes() {
        //given
        RecordKey rk = RecordKey.builder().entityId(1).partitionId(2).schemaId(3).systemId(4).timestamp(Long.MAX_VALUE)
                .build();
        byte[] array = rk.serialize();
        array[0] = 1;

        //when
        assertThrows(IllegalArgumentException.class, () -> RecordKey.deserialize(array));

        //then exception

    }

    @Test
    public void shouldThrowOnWrongSize() {
        //given
        RecordKey rk = RecordKey.builder().entityId(1).partitionId(2).schemaId(3).systemId(4).timestamp(Long.MAX_VALUE)
                .build();
        byte[] array = rk.serialize();
        byte[] duplicate = new byte[array.length - 1];

        System.arraycopy(array, 0, duplicate, 0, array.length - 1);

        //when
        assertThrows(IllegalArgumentException.class, () -> RecordKey.deserialize(duplicate));

        //then exception

    }

}
