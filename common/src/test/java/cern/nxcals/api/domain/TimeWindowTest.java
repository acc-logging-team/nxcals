package cern.nxcals.api.domain;

import cern.nxcals.api.utils.TimeUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.Instant;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.NavigableSet;
import java.util.Set;
import java.util.TreeSet;

import static cern.nxcals.api.domain.TimeWindow.after;
import static cern.nxcals.api.domain.TimeWindow.before;
import static cern.nxcals.api.domain.TimeWindow.between;
import static cern.nxcals.api.domain.TimeWindow.empty;
import static cern.nxcals.api.domain.TimeWindow.fromStrings;
import static cern.nxcals.api.domain.TimeWindow.infinite;
import static java.time.temporal.ChronoUnit.DAYS;
import static java.util.Collections.emptySet;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;

@TestInstance(PER_CLASS)
public class TimeWindowTest {

    @Test
    public void shouldCreateEmptyOnInverseTimeWindow() {
        assertTrue(between(10, 9).isEmpty());
    }

    @Test
    public void shouldEmptyTimeWindow() {
        assertTrue(empty().isEmpty());
    }

    @Test
    public void shouldCompareByStart() {
        assertEquals(0, TimeWindow.compareByStartTime().compare(between(0, 100), between(0, 100)));
        assertEquals(0, TimeWindow.compareByStartTime().compare(between(0, 101), between(0, 100)));
        assertEquals(0, TimeWindow.compareByStartTime().compare(between(0, 100), between(0, 101)));

        assertTrue(TimeWindow.compareByStartTime().compare(TimeWindow.between(1, 100), TimeWindow.between(0, 100)) > 0);
        assertTrue(TimeWindow.compareByStartTime().compare(TimeWindow.between(0, 100), TimeWindow.between(1, 100)) < 0);

        assertTrue(TimeWindow.compareByStartTime().compare(TimeWindow.before(0), TimeWindow.between(0, 100)) < 0);
        assertTrue(TimeWindow.compareByStartTime().compare(TimeWindow.between(0, 100), TimeWindow.before(0)) > 0);
        assertEquals(0, TimeWindow.compareByStartTime().compare(before(0), before(0)));
    }

    @Test
    public void shouldCompareByEnd() {
        assertEquals(0, TimeWindow.compareByEndTime().compare(between(0, 100), between(0, 100)));
        assertEquals(0, TimeWindow.compareByEndTime().compare(between(1, 100), between(0, 100)));
        assertEquals(0, TimeWindow.compareByEndTime().compare(between(0, 100), between(1, 100)));

        assertTrue(TimeWindow.compareByEndTime().compare(TimeWindow.between(0, 101), TimeWindow.between(0, 100)) > 0);
        assertTrue(TimeWindow.compareByEndTime().compare(TimeWindow.between(0, 100), TimeWindow.between(0, 101)) < 0);

        assertTrue(TimeWindow.compareByEndTime().compare(TimeWindow.between(0, 100), TimeWindow.after(0)) < 0);
        assertTrue(TimeWindow.compareByEndTime().compare(TimeWindow.after(100), TimeWindow.between(0, 100)) > 0);
        assertEquals(0, TimeWindow.compareByEndTime().compare(after(100), after(100)));
    }

    @Test
    public void shouldCompareByStartAndEnd() {
        Comparator<TimeWindow> byStartAndEndTime = TimeWindow.compareByStartAndEndTime();

        assertEquals(0, byStartAndEndTime.compare(between(0, 100), between(0, 100)));
        assertEquals(1, byStartAndEndTime.compare(between(0, 101), between(0, 100)));
        assertEquals(-1, byStartAndEndTime.compare(between(0, 100), between(0, 101)));

        assertTrue(byStartAndEndTime.compare(TimeWindow.between(1, 100), TimeWindow.between(0, 100)) > 0);
        assertTrue(byStartAndEndTime.compare(TimeWindow.between(0, 100), TimeWindow.between(1, 100)) < 0);

        assertTrue(byStartAndEndTime.compare(TimeWindow.before(0), TimeWindow.between(0, 100)) < 0);
        assertTrue(byStartAndEndTime.compare(TimeWindow.between(0, 100), TimeWindow.before(0)) > 0);
        assertEquals(0, byStartAndEndTime.compare(before(0), before(0)));
    }

    @Test
    public void shouldCompareByStartAndEndInDataStructure() {
        NavigableSet<TimeWindow> timeWindows = new TreeSet<>(TimeWindow.compareByStartAndEndTime());

        TimeWindow t1 = between(0, 100);
        TimeWindow t2 = TimeWindow.between(1, 50);
        TimeWindow t3 = TimeWindow.between(1, 150);
        TimeWindow t4 = TimeWindow.between(150, 200);
        TimeWindow t5 = TimeWindow.between(400, 500);

        List<TimeWindow> timeWindowsInExpectedOrder = Arrays.asList(t1, t2, t3, t4, t5);

        timeWindows.addAll(timeWindowsInExpectedOrder); //add to apply comparator logic

        // assert size to make sure that comparator has not swallowed any elements during the sort process
        assertEquals(timeWindowsInExpectedOrder.size(), timeWindows.size());

        for (TimeWindow expectedTimeWindow : timeWindowsInExpectedOrder) {
            assertEquals(expectedTimeWindow, timeWindows.pollFirst());
        }
        assertTrue(timeWindows.isEmpty()); //make sure that all elements have been polled and tested
    }

    @Test
    public void shouldInfiniteTimeWindow() {
        TimeWindow between = infinite();
        assertTrue(between.isLeftInfinite());
        assertTrue(between.isRightInfinite());
        assertTrue(between.isInfinite());
        assertFalse(between.isEmpty());
    }

    @Test
    public void shouldLeftOpenTimeWindow() {
        TimeWindow between = before(10);
        assertTrue(between.isLeftInfinite());
        assertEquals(10, TimeUtils.getNanosFromInstant(between.getEndTime()));
        assertFalse(between.isEmpty());
    }

    @Test
    public void shouldRightOpenTimeWindow() {
        TimeWindow between = after(9);
        assertEquals(9, TimeUtils.getNanosFromInstant(between.getStartTime()));
        assertTrue(between.isRightInfinite());
        assertFalse(between.isEmpty());
    }

    @Test
    public void shouldCreateClosedTimeWindow() {
        TimeWindow between = between(9, 10);
        assertEquals(9, TimeUtils.getNanosFromInstant(between.getStartTime()));
        assertEquals(10, TimeUtils.getNanosFromInstant(between.getEndTime()));
        assertFalse(between.isEmpty());
    }

    @ParameterizedTest
    @MethodSource("leftLimitParams")
    public void limitLeftTests(TimeWindow in, Instant limit, TimeWindow expected) {
        assertEquals(expected, in.leftLimit(limit));
    }

    public Object[][] leftLimitParams() {
        return new Object[][] { new Object[] { between(8, 10), unlimited(), between(8, 10) },
                new Object[] { between(8, 10), nano(7), between(8, 10) },
                new Object[] { between(8, 10), nano(9), between(9, 10) },
                new Object[] { between(8, 10), nano(11), empty() },
                new Object[] { before(10), unlimited(), before(10) },
                new Object[] { before(10), nano(9), between(9, 10) }, new Object[] { before(10), nano(11), empty() },
                new Object[] { after(8), unlimited(), after(8) }, new Object[] { after(8), nano(7), after(8) },
                new Object[] { after(8), nano(9), after(9) }, new Object[] { infinite(), unlimited(), infinite() },
                new Object[] { infinite(), nano(7), after(7) }, };
    }

    @ParameterizedTest
    @MethodSource("leftLimitSamenessParams")
    public void limitLeftSamenessTests(TimeWindow in, Instant limit) {
        assertSame(in, in.leftLimit(limit));
    }

    public Object[][] leftLimitSamenessParams() {
        return new Object[][] { new Object[] { between(8, 10), unlimited() }, new Object[] { between(8, 10), nano(7) },
                new Object[] { before(10), unlimited() }, new Object[] { after(8), unlimited() },
                new Object[] { infinite(), unlimited() }, };
    }

    @ParameterizedTest
    @MethodSource("rightLimitParams")
    public void limitRightTests(TimeWindow in, Instant limit, TimeWindow expected) {
        assertEquals(expected, in.rightLimit(limit));
    }

    public Object[][] rightLimitParams() {
        return new Object[][] { new Object[] { between(8, 10), unlimited(), between(8, 10) },
                new Object[] { between(8, 10), nano(7), empty() },
                new Object[] { between(8, 10), nano(9), between(8, 9) },
                new Object[] { between(8, 10), nano(11), between(8, 10) },
                new Object[] { before(10), unlimited(), before(10) }, new Object[] { before(10), nano(9), before(9) },
                new Object[] { before(10), nano(11), before(10) }, new Object[] { after(8), unlimited(), after(8) },
                new Object[] { after(8), nano(7), empty() }, new Object[] { after(8), nano(9), between(8, 9) },
                new Object[] { infinite(), unlimited(), infinite() },
                new Object[] { infinite(), nano(7), before(7) }, };
    }

    @ParameterizedTest
    @MethodSource("rightLimitSamenessParams")
    public void limitRightSamenessTests(TimeWindow in, Instant limit) {
        assertSame(in, in.rightLimit(limit));
    }

    public Object[][] rightLimitSamenessParams() {
        return new Object[][] { new Object[] { between(8, 10), unlimited() }, new Object[] { between(8, 10), nano(11) },
                new Object[] { before(10), unlimited() }, new Object[] { before(10), nano(11) },
                new Object[] { after(8), unlimited() }, new Object[] { infinite(), unlimited() }, };
    }

    @ParameterizedTest
    @MethodSource("intersectParams")
    public void intersectTests(TimeWindow a, TimeWindow b, TimeWindow expected) {
        assertEquals(a.intersect(b), expected);
        assertEquals(b.intersect(a), expected);
    }

    public Object[][] intersectParams() {
        return new Object[][] { new Object[] { empty(), between(12, 14), empty() },
                new Object[] { between(8, 10), empty(), empty() }, new Object[] { empty(), empty(), empty() },
                new Object[] { between(8, 10), between(12, 14), empty() },
                new Object[] { between(8, 10), between(9, 14), between(9, 10) },
                new Object[] { between(8, 10), after(9), between(9, 10) },
                new Object[] { before(10), between(9, 14), between(9, 10) },
                new Object[] { before(10), after(9), between(9, 10) },
                new Object[] { between(8, 14), between(9, 12), between(9, 12) },
                new Object[] { before(14), between(9, 12), between(9, 12) },
                new Object[] { after(8), between(9, 12), between(9, 12) },
                new Object[] { infinite(), between(9, 12), between(9, 12) }, };
    }

    @ParameterizedTest
    @MethodSource("intersectSamenessParams")
    public void intersectSamenessTests(TimeWindow a, TimeWindow b) {
        assertSame(a.intersect(b), a);
    }

    public Object[][] intersectSamenessParams() {
        return new Object[][] { new Object[] { empty(), between(12, 14) }, new Object[] { empty(), empty() },
                new Object[] { between(10, 12), between(8, 14) }, new Object[] { between(10, 12), after(8) },
                new Object[] { between(10, 12), before(14) }, new Object[] { between(10, 12), infinite() },
                new Object[] { before(12), before(14) }, new Object[] { after(10), after(8) },
                new Object[] { infinite(), infinite() }, };
    }

    @ParameterizedTest
    @MethodSource("expandParams")
    public void expandTests(TimeWindow a, TimeWindow b, TimeWindow expected) {
        assertEquals(a.expand(b), expected);
        assertEquals(b.expand(a), expected);
    }

    public Object[][] expandParams() {
        return new Object[][] { new Object[] { empty(), between(12, 14), between(12, 14) },
                new Object[] { between(8, 10), empty(), between(8, 10) }, new Object[] { empty(), empty(), empty() },
                new Object[] { between(8, 10), between(12, 14), between(8, 14) },
                new Object[] { between(8, 10), before(14), before(14) },
                new Object[] { between(8, 10), after(12), after(8) },
                new Object[] { after(8), between(12, 14), after(8) },
                new Object[] { before(10), between(12, 14), before(14) },
                new Object[] { between(8, 12), between(10, 14), between(8, 14) },
                new Object[] { between(8, 12), before(14), before(14) },
                new Object[] { between(8, 12), after(10), after(8) },
                new Object[] { before(12), between(10, 14), before(14) },
                new Object[] { after(8), between(10, 14), after(8) },
                new Object[] { infinite(), between(10, 14), infinite() },
                new Object[] { infinite(), infinite(), infinite() }, };
    }

    @ParameterizedTest
    @MethodSource("expandSamenessParams")
    public void expandSamenessTests(TimeWindow a, TimeWindow b) {
        assertSame(a.expand(b), a);
    }

    @Test
    public void shouldNotBeEqual() {
        assertNotEquals(between(1, 2), between(10, 20));
    }

    public Object[][] expandSamenessParams() {
        return new Object[][] { new Object[] { between(12, 14), empty() }, new Object[] { before(14), empty() },
                new Object[] { after(12), empty() }, new Object[] { infinite(), empty() },
                new Object[] { empty(), empty() }, new Object[] { between(8, 14), between(10, 12) },
                new Object[] { before(14), between(10, 12) }, new Object[] { after(8), between(10, 12) },
                new Object[] { infinite(), between(10, 12) }, };
    }

    private Instant nano(Integer i) {
        return TimeUtils.getInstantFromNanos(i);
    }

    private Integer unlimited() {
        return null;
    }

    @ParameterizedTest
    @MethodSource("intersectionParams")
    public void shouldIntersects(TimeWindow t1, TimeWindow t2, boolean intersects) {
        assertEquals(intersects, t1.intersects(t2));
        assertEquals(intersects, t2.intersects(t1));
    }

    public Object[][] intersectionParams() {
        return new Object[][] { new Object[] { between(10, 15), between(16, 20), false },
                new Object[] { between(10, 15), between(15, 20), true },
                new Object[] { between(10, 15), between(11, 20), true }, };

    }

    @ParameterizedTest
    @MethodSource("betweenUsingStringParams")
    public void shouldCreateFromStrings(String startTime, String endTime, boolean fromInf, boolean toInf) {
        TimeWindow window = fromStrings(startTime, endTime);
        assertEquals(fromInf, window.isLeftInfinite());
        assertEquals(toInf, window.isRightInfinite());
    }

    public Object[][] betweenUsingStringParams() {
        return new Object[][] {
                new Object[] { "2018-06-15 00:00:00.000", "2018-06-16 00:00:00.000", false, false },
                new Object[] { "2018-06-15 00:00:00.000", null, false, true },
                new Object[] { null, "2018-06-16 00:00:00.000", true, false },
                new Object[] { null, null, true, true }, };

    }

    @ParameterizedTest
    @MethodSource("inRangeParams")
    public void shouldCheckForInRangeExcludedRight(TimeWindow t1, long ts, boolean result) {
        assertEquals(result, t1.isWithinRangeRightExcluded(ts));
    }

    public Object[][] inRangeParams() {
        return new Object[][] {
                new Object[] { TimeWindow.between(null, null), 1L, true },
                new Object[] { TimeWindow.between(null, TimeUtils.getInstantFromNanos(11L)), 10L, true },
                new Object[] { TimeWindow.between(null, TimeUtils.getInstantFromNanos(10L)), 10L, false },
                new Object[] { TimeWindow.between(TimeUtils.getInstantFromNanos(10L), null), 10L, true },
                new Object[] { TimeWindow.between(TimeUtils.getInstantFromNanos(10L), null), 9L, false },
                new Object[] { TimeWindow.between(TimeUtils.getInstantFromNanos(10L), null), 20L, true },
                new Object[] { TimeWindow.between(10, 20), 20L, false },
                new Object[] { TimeWindow.between(10, 20), 10L, true },
                new Object[] { TimeWindow.between(10, 20), 11L, true },
                new Object[] { TimeWindow.between(10, 20), 9L, false },

        };

    }

    @ParameterizedTest
    @MethodSource("mergeWindows")
    public void shouldMergeTimeWindows(Collection<TimeWindow> input, Set<TimeWindow> expectedOutput) {
        Set<TimeWindow> result = TimeWindow.mergeTimeWindows(input);
        assertEquals(expectedOutput, result);
    }

    public Object[][] mergeWindows() {
        Instant now = Instant.now();
        Instant monthBefore = now.minus(30, DAYS);
        Instant monthAndDayBefore = now.minus(31, DAYS);
        Instant dayBefore = now.minus(1, DAYS);

        TimeWindow inf = TimeWindow.infinite();
        TimeWindow lastMonth = TimeWindow.between(monthBefore, now);
        TimeWindow beforeLastMonth = TimeWindow.before(monthBefore);
        TimeWindow past = TimeWindow.before(now);
        TimeWindow lastDay = TimeWindow.between(dayBefore, now);
        TimeWindow lastMonthToLastDay = TimeWindow.between(monthBefore, dayBefore);
        TimeWindow future = TimeWindow.after(now);
        TimeWindow dayMonthBefore = TimeWindow.between(monthAndDayBefore, monthBefore);
        return new Object[][] {
                new Object[] { emptySet(), emptySet() },
                new Object[] { Set.of(beforeLastMonth, lastMonth), Set.of(past) },
                new Object[] { Set.of(inf, beforeLastMonth, lastMonth), Set.of(inf) },
                new Object[] { Set.of(past, future), Set.of(inf) },
                new Object[] { Set.of(lastDay, lastMonth), Set.of(lastMonth) },
                new Object[] { Set.of(lastDay, dayMonthBefore), Set.of(lastDay, dayMonthBefore) },
                new Object[] { Set.of(lastDay, lastMonthToLastDay), Set.of(lastMonth) }
        };

    }
}
