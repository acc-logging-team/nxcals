package cern.nxcals.api.domain;

import cern.nxcals.common.utils.GlobalObjectMapperProvider;
import cern.nxcals.common.utils.ReflectionUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableSet;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Set;

import static java.util.Collections.singletonMap;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PartitionResourceTest {
    private ObjectMapper ob = GlobalObjectMapperProvider.get();
    private final Timestamp startTime = new Timestamp(System.currentTimeMillis() - 100000);
    private final Timestamp endTime = new Timestamp(System.currentTimeMillis());

    @Test
    public void shouldNotAllowNullForSystemSpec() {
        assertThrows(NullPointerException.class, () -> partitionResourceBuilder().systemSpec(null).build());
    }

    @Test
    public void shouldNotAllowNullForPartition() {
        assertThrows(NullPointerException.class, () -> partitionResourceBuilder().partition(null).build());
    }

    @Test
    public void shouldNotAllowNullForSchema() {
        assertThrows(NullPointerException.class, () -> partitionResourceBuilder().schema(null).build());
    }

    @Test
    public void shouldEqualCorrectly() {
        PartitionResource partitionResource = partitionResourceBuilder().build();
        assertEquals(partitionResource, partitionResource);
        assertEquals(partitionResource, partitionResource.toBuilder().build());
        assertEquals(partitionResource, partitionResource.toBuilder().build());
        assertNotEquals(partitionResource,
                partitionResource.toBuilder().systemSpec(systemSpec().toBuilder().name("other-system").build()).build());
        assertNotEquals(partitionResource, partitionResource.toBuilder().partition(partition().toBuilder().systemSpec(systemSpec().toBuilder().name("other-system").build()).build()).build());
        assertNotEquals(partitionResource, partitionResource.toBuilder().schema(schema().toBuilder().schemaJson("other schema").build()).build());
    }

    @Test
    public void shouldHashCorrectly() {
        PartitionResource partitionResource = partitionResourceBuilder().build();

        Set<PartitionResource> setA = ImmutableSet.of(partitionResource);
        assertThat(setA).contains(partitionResource);
        assertThat(setA).contains(partitionResource.toBuilder().build());
        assertThat(setA).contains(partitionResource.toBuilder().build());
        assertThat(setA).doesNotContain(
                partitionResource.toBuilder().systemSpec(systemSpec().toBuilder().name("other-system").build()).build());

        Set<PartitionResource> setB = ImmutableSet.of(partitionResource, partitionResource.toBuilder().systemSpec(systemSpec().toBuilder().name("yet-another-system").build()).build());
        assertThat(setB).contains(partitionResource);
        assertThat(setB).contains(partitionResource.toBuilder().build());
        assertThat(setB).contains(partitionResource.toBuilder().build());
        assertThat(setB).doesNotContain(
                partitionResource.toBuilder().systemSpec(systemSpec().toBuilder().name("other-system").build()).build());

        Set<PartitionResource> setC = ImmutableSet.of(partitionResource, partitionResource.toBuilder().build());
        assertEquals(1, setC.size());
    }

    @Test
    public void shouldSerializeRaw() throws IOException {
        PartitionResource partitionResourceA = partitionResourceBuilder()
                .systemSpec(systemSpec())
                .partition(partition())
                .schema(schema())
                .build();

        String s = ob.writeValueAsString(partitionResourceA);
        PartitionResource partitionResourceB = ob.readValue(s, PartitionResource.class);

        assertEqual(partitionResourceA, partitionResourceB);
        assertEqual(partitionResourceA.toBuilder().build(), partitionResourceB);
        assertEqual(partitionResourceA, partitionResourceB.toBuilder().build());
    }

    @Test
    public void shouldSerializeSnapshot() throws IOException {
        PartitionResource partitionResourceA = partitionResourceBuilder()
                .systemSpec(systemSpec())
                .partition(partition())
                .schema(schema())
                .build();

        String s = ob.writeValueAsString(partitionResourceA);

        PartitionResource partitionResourceB = ob.readValue(s, PartitionResource.class);

        assertEqual(partitionResourceA, partitionResourceB);
        assertEqual(partitionResourceA.toBuilder().build(), partitionResourceB);
        assertEqual(partitionResourceA, partitionResourceB.toBuilder().build());
    }

    @Test
    public void shouldSerializePartitionResource() throws IOException {
        PartitionResource partitionResourceA = PartitionResource.builder()
                .systemSpec(systemSpec())
                .partition(partition())
                .schema(schema())
                .build();

        String s = ob.writeValueAsString(partitionResourceA);

        PartitionResource partitionResourceB = ob.readValue(s, PartitionResource.class);

        assertEqual(partitionResourceA, partitionResourceB);
        assertEqual(partitionResourceA.toBuilder().build(), partitionResourceB);
        assertEqual(partitionResourceA, partitionResourceB.toBuilder().build());
    }

    @Test
    public void shouldCopyAllDataOnToBuilder() {
        PartitionResource partitionResourceA = partitionResourceBuilder()
                .systemSpec(systemSpec())
                .partition(partition())
                .schema(schema())
                .build();

        PartitionResource partitionResourceB = partitionResourceA.toBuilder().build();

        assertEqual(partitionResourceA, partitionResourceB);
        assertEqual(partitionResourceA.toBuilder().build(), partitionResourceB);
        assertEqual(partitionResourceA, partitionResourceB.toBuilder().build());
    }

    private PartitionResource.Builder partitionResourceBuilder() {
        return ReflectionUtils.builderInstance(PartitionResource.InnerBuilder.class).id(10L)
                .recVersion(20L).systemSpec(systemSpec())
                .partition(partition()).schema(schema());
    }
    

    private SystemSpec systemSpec() {
        return SystemSpec.builder().name("name").partitionKeyDefinitions("").timeKeyDefinitions("").entityKeyDefinitions("").recordVersionKeyDefinitions("").build();
    }

    private Partition partition() {
        return Partition.builder().keyValues(singletonMap("a", "b")).systemSpec(systemSpec()).build();
    }

    private EntitySchema schema() {
        return EntitySchema.builder().schemaJson("").build();
    }

    private void assertEqual(PartitionResource partitionResourceA, PartitionResource partitionResourceB) {
        assertEquals(partitionResourceA, partitionResourceB);
        assertEquals(partitionResourceA.getId(), partitionResourceB.getId());
        assertEquals(partitionResourceA.getRecVersion(), partitionResourceB.getRecVersion());
        assertEquals(partitionResourceA.getSystemSpec(), partitionResourceB.getSystemSpec());
        assertEquals(partitionResourceA.getPartition(), partitionResourceB.getPartition());
        assertEquals(partitionResourceA.getSchema(), partitionResourceB.getSchema());
    }

}
