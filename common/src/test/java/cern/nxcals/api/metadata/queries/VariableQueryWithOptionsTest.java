package cern.nxcals.api.metadata.queries;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class VariableQueryWithOptionsTest {
    private static final Condition<Variables> condition = Variables.suchThat().id().eq(1L);

    public static Object[] variableQueries() {
        return new Object[] {
                new VariableQueryWithOptions(condition).limit(10),
                new VariableQueryWithOptions(condition).limit(10),
                new VariableQueryWithOptions(condition),
                new VariableQueryWithOptions(condition).noConfigs(),
                new VariableQueryWithOptions(condition).noConfigs().orderBy().variableName().desc(),
                new VariableQueryWithOptions(condition).noConfigs().orderBy().variableName().asc(),
                new VariableQueryWithOptions(condition).noConfigs().orderBy().variableName().asc().limit(10),
                new VariableQueryWithOptions(condition).noConfigs().limit(10).orderBy().variableName().asc(),
        };
    }

    @ParameterizedTest
    @MethodSource("variableQueries")
    void shouldSerializeAndDeserializeQueryOptions(VariableQueryWithOptions queryOptions)
            throws JsonProcessingException {
        //given
        //when
        String json = queryOptions.toJson();
        VariableQueryWithOptions decodedQueryOptions = VariableQueryWithOptions.fromJson(json);
        //then
        assertEquals(queryOptions, decodedQueryOptions);
    }

    @Test
    void shouldSetFieldToOrderBy() {
        VariableQueryWithOptions options = new VariableQueryWithOptions(condition).orderBy().variableName()
                .desc();

        assertEquals("variableName", options.getOrderByField());
        assertFalse(options.isAscending());
    }
}
