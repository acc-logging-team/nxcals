package cern.nxcals.api.metadata.queries;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.Instant;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class EntityQueryWithOptionsTest {
    private static Condition<Entities> condition = Entities.suchThat().id().eq(1L);

    public static Object[] EntityQueries() {
        return new Object[] {
                new EntityQueryWithOptions(condition).limit(10),
                new EntityQueryWithOptions(condition).limit(10).withHistory(100, 1000),
                new EntityQueryWithOptions(condition).withHistory(100, 1000),
                new EntityQueryWithOptions(condition).withHistory("1970-10-10 10:00:00", "1970-10-10 10:01:00"),
                new EntityQueryWithOptions(condition).withHistory(Instant.MIN, Instant.MAX),
                new EntityQueryWithOptions(condition).orderBy().systemId().asc(),
                new EntityQueryWithOptions(condition).orderBy().systemId().asc().limit(10),
                new EntityQueryWithOptions(condition).noHistory(),
        };
    }

    @ParameterizedTest
    @MethodSource("EntityQueries")
    void shouldSerializeAndDeserializeQueryOptions(EntityQueryWithOptions queryOptions) throws JsonProcessingException {
        //given
        //when
        String json = queryOptions.toJson();
        EntityQueryWithOptions decodedQueryOptions = EntityQueryWithOptions.fromJson(json);
        //then
        assertEquals(queryOptions, decodedQueryOptions);
    }

    @Test
    void shouldSetFieldToOrderBy() {
        EntityQueryWithOptions options = new EntityQueryWithOptions(condition).orderBy().systemId().desc();

        assertEquals("partition.system.id", options.getOrderByField());
        assertFalse(options.isAscending());
    }
}
