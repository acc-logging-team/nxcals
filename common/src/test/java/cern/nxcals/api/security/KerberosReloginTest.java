package cern.nxcals.api.security;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class KerberosReloginTest {

    @Test
    public void shouldThrowOnWrongKeytab() {
        assertThrows(IllegalArgumentException.class, () -> new KerberosRelogin("wrong", "keytab", false));
    }

}