package cern.nxcals.api.converters;

import cern.cmw.data.DiscreteFunction;
import cern.cmw.data.DiscreteFunctionList;
import cern.cmw.datax.EntryType;
import cern.cmw.datax.ImmutableData;
import cern.cmw.datax.ImmutableEntry;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.lang.reflect.Array;

import static cern.nxcals.api.converters.SparkRowToImmutableDataConverter.createArraySchema;
import static cern.nxcals.api.converters.SparkRowToImmutableDataConverter.createDiscreteFunctionListSchema;
import static cern.nxcals.api.converters.SparkRowToImmutableDataConverter.createDiscreteFunctionSchema;
import static cern.nxcals.api.converters.SparkRowToImmutableDataConverter.isArray;
import static cern.nxcals.api.converters.SparkRowToImmutableDataConverter.isDiscreteFunction;
import static cern.nxcals.api.converters.SparkRowToImmutableDataConverter.isDiscreteFunctionList;
import static cern.nxcals.common.avro.SchemaConstants.ARRAY_DIMENSIONS_FIELD_NAME;
import static cern.nxcals.common.avro.SchemaConstants.ARRAY_ELEMENTS_FIELD_NAME;
import static cern.nxcals.common.avro.SchemaConstants.DDF_X_ARRAY_FIELD_NAME;
import static cern.nxcals.common.avro.SchemaConstants.DDF_Y_ARRAY_FIELD_NAME;
import static cern.nxcals.common.avro.SchemaConstants.DF_LIST_FIELD_NAME;
import static org.apache.spark.sql.types.DataTypes.BooleanType;
import static org.apache.spark.sql.types.DataTypes.ByteType;
import static org.apache.spark.sql.types.DataTypes.DoubleType;
import static org.apache.spark.sql.types.DataTypes.FloatType;
import static org.apache.spark.sql.types.DataTypes.IntegerType;
import static org.apache.spark.sql.types.DataTypes.LongType;
import static org.apache.spark.sql.types.DataTypes.StringType;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;

@TestInstance(PER_CLASS)
public class SparkRowToImmutableDataConverterTest {
    private static final String TEST_FIELD_NAME = "TEST_NAME";

    @Test
    public void testConvertNull() {
        assertThrows(NullPointerException.class, () -> SparkRowToImmutableDataConverter.convert(null));
    }

    @Test
    public void testEmptyRow() {
        //given
        Row row = new GenericRowWithSchema(new Object[0], new StructType(new StructField[0]));
        //when
        ImmutableData immutableData = SparkRowToImmutableDataConverter.convert(row);
        //then
        assertThat(immutableData.getEntries()).isEmpty();
    }

    @Test
    public void shouldThrowOnNullElementsInArray() {
        //given
        StructType arraySchema = createArraySchema(IntegerType, true);
        Row array = new GenericRowWithSchema(new Object[] { null, new int[] { 0 } }, arraySchema);
        Row row = new GenericRowWithSchema(new Object[] { array },
                createStructType(createArraySchema(IntegerType, true)));
        //when
        assertThrows(IllegalArgumentException.class, () -> SparkRowToImmutableDataConverter.convert(row));

        //then exception
    }

    @ParameterizedTest
    @MethodSource("allTypes")
    public void testDifferentDataTypes(Object value, StructType type, EntryType entryType) {
        //given
        Row row = new GenericRowWithSchema(new Object[]{value}, type);
        //when
        ImmutableData immutableData = SparkRowToImmutableDataConverter.convert(row);
        //then
        verify(value, entryType, immutableData.getEntry(TEST_FIELD_NAME));
    }

    @ParameterizedTest
    @MethodSource("allTypes")
    public void testDifferentDataTypesNested(Object value, StructType nestedType, EntryType entryType) {
        //given nested objects
        Row rowNested = new GenericRowWithSchema(new Object[]{value}, nestedType);
        StructType type = new StructType(new StructField[]{structField(nestedType, TEST_FIELD_NAME)});
        Row row = new GenericRowWithSchema(new Object[]{rowNested}, type);

        //when
        //Getting nested field here
        ImmutableData immutableData = (ImmutableData) SparkRowToImmutableDataConverter.convert(row).getEntry(TEST_FIELD_NAME).get();
        //then
        verify(value, entryType, immutableData.getEntry(TEST_FIELD_NAME));
    }


    private void verify(Object value, EntryType entryType, ImmutableEntry entry) {
        if (entryType.equals(EntryType.DISCRETE_FUNCTION)) {
            checkDiscreteFunction((Row) value, entry.getAs(EntryType.DISCRETE_FUNCTION));
        } else if (entryType.equals(EntryType.DISCRETE_FUNCTION_LIST)) {
            Row row = (Row) value;
            Row[] rows = row.getAs(DF_LIST_FIELD_NAME);
            DiscreteFunctionList discreteFunctionList = entry.getAs(EntryType.DISCRETE_FUNCTION_LIST);
            for (int i = 0; i < rows.length; ++i) {
                checkDiscreteFunction(rows[i], discreteFunctionList.getFunction(i));
            }
        } else if (value instanceof Row && isArray(((Row) value).schema())) {
            Row row = (Row) value;
            checkArray(row, entry);
        } else {
            assertThat(entry.get()).isEqualTo(value);
        }
        assertThat(entry.getType()).isEqualTo(entryType);
    }

    private void checkDiscreteFunction(Row row, DiscreteFunction df) {
        double[] xArray = (double[]) row.getAs(DDF_X_ARRAY_FIELD_NAME);
        double[] yArray = (double[]) row.getAs(DDF_Y_ARRAY_FIELD_NAME);
        assertThat(df.getXArray()).isEqualTo(xArray);
        assertThat(df.getYArray()).isEqualTo(yArray);
    }

    private void checkArray(Row row, ImmutableEntry entry) {
        Object elements = row.getAs(ARRAY_ELEMENTS_FIELD_NAME);
        int[] dims = (int[]) row.getAs(ARRAY_DIMENSIONS_FIELD_NAME);

        if (elements instanceof Row[]) {
            //DFArray or DFLArray
            Row[] dfs = (Row[]) elements;
            for (int i = 0; i < dfs.length; i++) {
                if (isDiscreteFunction(dfs[i].schema())) {
                    checkDiscreteFunction(dfs[i], entry.getAs(EntryType.DISCRETE_FUNCTION_ARRAY)[i]);
                } else if (isDiscreteFunctionList(dfs[i].schema())) {

                } else {
                    throw new IllegalArgumentException("Unknown type for row " + row.json());
                }
            }
        } else {
            assertThat(elements).isEqualTo(entry.get());
            assertThat(dims).isEqualTo(entry.getDims());
        }
    }




    private StructType structType(StructField structField) {
        return new StructType(new StructField[]{structField});
    }

    private StructField structField(DataType dataType, String testName) {
        return new StructField(testName, dataType, true, null);
    }

    public Object[][] allTypes() {
        return new Object[][]{
                new Object[]{2, createStructType(IntegerType), EntryType.INT32},
                new Object[]{2D, createStructType(DoubleType), EntryType.DOUBLE},
                new Object[]{2L, createStructType(LongType), EntryType.INT64},
                new Object[]{2F, createStructType(FloatType), EntryType.FLOAT},
                new Object[]{true, createStructType(BooleanType), EntryType.BOOL},
                new Object[]{(byte) 'a', createStructType(ByteType), EntryType.INT8},
                new Object[]{"string", createStructType(StringType), EntryType.STRING},
                //DF
                new Object[] { createDiscreteFunctionAsRow(), createStructType(createDiscreteFunctionSchema(true)),
                        EntryType.DISCRETE_FUNCTION },
                //DFL
                new Object[] { createDiscreteFunctionListAsRow(),
                        createStructType(createDiscreteFunctionListSchema(true)), EntryType.DISCRETE_FUNCTION_LIST },

                //null are ok as elements in array
                new Object[] { createArray(new Integer[] { 2, 3, null }, IntegerType),
                        createStructType(createArraySchema(IntegerType, true)), EntryType.INT32_WRAPPER_ARRAY },
                new Object[] { createArray(new int[] { 2, 3 }, IntegerType),
                        createStructType(createArraySchema(IntegerType, true)), EntryType.INT32_ARRAY },
                new Object[] { createArray(new Long[] { 2L, 3L }, LongType),
                        createStructType(createArraySchema(LongType, true)), EntryType.INT64_WRAPPER_ARRAY },
                new Object[] { createArray(new long[] { 2L, 3L }, LongType),
                        createStructType(createArraySchema(LongType, true)), EntryType.INT64_ARRAY },
                new Object[] { createArray(new Float[] { 2F, 3F }, FloatType),
                        createStructType(createArraySchema(FloatType, true)), EntryType.FLOAT_WRAPPER_ARRAY },
                new Object[] { createArray(new float[] { 2F, 3F }, FloatType),
                        createStructType(createArraySchema(FloatType, true)), EntryType.FLOAT_ARRAY },
                new Object[] { createArray(new Boolean[] { true, false }, BooleanType),
                        createStructType(createArraySchema(BooleanType, true)), EntryType.BOOL_WRAPPER_ARRAY },
                new Object[] { createArray(new boolean[] { true, false }, BooleanType),
                        createStructType(createArraySchema(BooleanType, true)), EntryType.BOOL_ARRAY },
                new Object[] { createArray(new Byte[] { 'a', 'b' }, ByteType),
                        createStructType(createArraySchema(ByteType, true)), EntryType.INT8_WRAPPER_ARRAY },
                new Object[] { createArray(new byte[] { 'a', 'b' }, ByteType),
                        createStructType(createArraySchema(ByteType, true)), EntryType.INT8_ARRAY },
                new Object[] { createArray(new String[] { "1", "2" }, StringType),
                        createStructType(createArraySchema(StringType, true)), EntryType.STRING_ARRAY },
                //DFArray
                new Object[] { createArray(new Row[] { createDiscreteFunctionAsRow(), createDiscreteFunctionAsRow() }
                        , createDiscreteFunctionSchema(true)),
                        createStructType(createArraySchema(createDiscreteFunctionSchema(true), true)),
                        EntryType.DISCRETE_FUNCTION_ARRAY },
                //DFLArray
                new Object[] {
                        createArray(new Row[] { createDiscreteFunctionListAsRow(), createDiscreteFunctionListAsRow() }
                                , createDiscreteFunctionListSchema(true)),
                        createStructType(createArraySchema(createDiscreteFunctionListSchema(true), true)),
                        EntryType.DISCRETE_FUNCTION_LIST_ARRAY }

        };
    }

    private StructType createStructType(DataType integerType) {
        return structType(structField(integerType, TEST_FIELD_NAME));
    }

    private Row createArray(Object array, DataType type) {
        StructType arraySchema = createArraySchema(type, true);
        Row row = new GenericRowWithSchema(new Object[] { array, new int[] { Array.getLength(array) } }, arraySchema);
        return row;
    }

    private Row createDiscreteFunctionListAsRow() {
        StructType dflType = createDiscreteFunctionListSchema(true);
        Row row = new GenericRowWithSchema(
                new Object[] { new Row[] { createDiscreteFunctionAsRow(), createDiscreteFunctionAsRow() } }, dflType);
        return row;
    }

    private Row createDiscreteFunctionAsRow() {
        StructType dfType = createDiscreteFunctionSchema(true);
        Row row = new GenericRowWithSchema(new Object[] { new double[] { 1, 2 }, new double[] { 3, 4 } }, dfType);
        return row;
    }
}
