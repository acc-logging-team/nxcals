package cern.nxcals.api.converters;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class Py4jLongConverterTest {

    @Test
    void shouldConvertSetOfIntegers() {
        Set<Integer> integers = new HashSet<>();
        integers.add(1);
        integers.add(2);

        Set<Long> expected = new HashSet<>();
        expected.add(1L);
        expected.add(2L);

        Set<Long> result = Py4jLongConverter.convert(integers);
        assertEquals(expected, result);
    }

    @Test
    void shouldConvertSetOfLongs() {
        Set<Long> input = new HashSet<>();
        input.add(1L);
        input.add(2L);
        Set<Long> expected = new HashSet<>(input);

        Set<Long> result = Py4jLongConverter.convert(input);
        assertEquals(expected, result);
    }

    @Test
    void shouldRaiseException() {
        Set<Double> input = new HashSet<>();
        input.add(1.0d);
        input.add(2.0d);

        Exception exception = assertThrows(RuntimeException.class, () -> {
            Py4jLongConverter.convert(input);
        });

        String expectedMessage = "not supported as an argument";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void shouldConvertMap() {
        String KEY = "ABC";

        Set<Integer> integers = new HashSet<>();
        integers.add(1);
        integers.add(2);

        Set<Long> longs = new HashSet<>();
        longs.add(1L);
        longs.add(2L);

        Map<String, Set<Integer>> input = new HashMap<>();
        input.put(KEY, integers);

        Map<String, Set<Long>> expected = new HashMap<>();
        expected.put(KEY, longs);

        Map<String, Set<Long>> result = Py4jLongConverter.convertMap(new HashMap<>(input));
        assertEquals (expected, result);
    }
}