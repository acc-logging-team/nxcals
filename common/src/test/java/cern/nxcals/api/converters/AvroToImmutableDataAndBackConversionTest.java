package cern.nxcals.api.converters;

import cern.cmw.data.DataFactory;
import cern.cmw.data.DiscreteFunction;
import cern.cmw.data.DiscreteFunctionList;
import cern.cmw.datax.EntryType;
import cern.cmw.datax.ImmutableData;
import cern.cmw.datax.ImmutableEntry;
import cern.nxcals.common.avro.DataEncoderImpl;
import cern.nxcals.common.utils.IllegalCharacterConverter;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericRecord;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import static cern.cmw.datax.EntryType.DISCRETE_FUNCTION;
import static cern.cmw.datax.EntryType.FLOAT_ARRAY;
import static org.apache.avro.SchemaBuilder.record;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class AvroToImmutableDataAndBackConversionTest {

    private final DataEncoderImpl encoder = new DataEncoderImpl();
    private final IllegalCharacterConverter converter = IllegalCharacterConverter.get();

    private final Schema.Parser parser = new Schema.Parser();

    @ParameterizedTest 
    @MethodSource("allNullTypes")
    public void shouldConvertPrimitiveTypesWithNulls(EntryType type) {
        //given
        ImmutableData dataBefore = ImmutableData.builder().add(ImmutableEntry.ofNull("VALUE", type)).build();
        String schemaStr = encoder.encodeRecordFieldDefinitions(dataBefore);
        Schema schema = parser.parse(schemaStr);
        GenericRecord avro = ImmutableDataToAvroConverter.createDataGenericRecord(dataBefore, schema);

        //when
        ImmutableData dataAfter = AvroToImmutableDataConverter.convert(avro);

        //then
        assertEquals(dataBefore, dataAfter);
    }

    @ParameterizedTest
    @MethodSource("allTypes")
    public void shouldConvertPrimitiveTypes(Object value) {
        //given
        ImmutableData dataBefore = ImmutableData.builder().add(ImmutableEntry.of("VALUE", value)).build();
        String schemaStr = encoder.encodeRecordFieldDefinitions(dataBefore);
        Schema schema = parser.parse(schemaStr);
        GenericRecord avro = ImmutableDataToAvroConverter.createDataGenericRecord(dataBefore, schema);

        //when
        ImmutableData dataAfter = AvroToImmutableDataConverter.convert(avro);

        //then
        assertEquals(dataBefore, dataAfter);
    }


    public Schema recordSchema(String name, Schema type) {
        return record("TEST").fields().name(name).type(type).noDefault().endRecord();
    }


    @Test
    public void shouldConvertComplexArraysTypes() {
        //given
        String schemaStr =
                "{\"type\":\"record\",\"name\":\"nxcals_record\",\"namespace\":\"nxcals\",\"fields\":[" +
                "{\"name\":\"data1\",\"type\":[" +
                        "{\"name\":\"data1\",\"type\":\"record\"," +
                        "\"fields\":[" +
                        "{\"name\":\"elements\",\"type\":[{\"type\":\"array\",\"items\":[\"float\",\"null\"]},\"null\"]},"+
                        "{\"name\":\"dimensions\",\"type\":[{\"type\":\"array\",\"items\":[\"int\",\"null\"]},\"null\"]}]}"+
                                ",\"null\"]}," +
                "{\"name\":\"data2\",\"type\":[" +
                        "{\"name\":\"data2\",\"type\":\"record\"," +
                        "\"fields\":[" +
                        "{\"name\":\"elements\",\"type\":[{\"type\":\"array\",\"items\":[\"float\",\"null\"]},\"null\"]},"+
                        "{\"name\":\"dimensions\",\"type\":[{\"type\":\"array\",\"items\":[\"int\",\"null\"]},\"null\"]}]}"+
                        ",\"null\"]}" +
                "]}";

       ImmutableData dataBefore = ImmutableData.builder()
               .add(ImmutableEntry.of("data1", new float[] { 2F, 3F }, 2))
               .add(ImmutableEntry.ofNull("data2", FLOAT_ARRAY))
               .build();

        Schema schema = parser.parse(schemaStr);
        GenericRecord avro = ImmutableDataToAvroConverter.createDataGenericRecord(dataBefore, schema);

        //when
        ImmutableData dataAfter = AvroToImmutableDataConverter.convert(avro);

        //then
        assertEquals(Schema.Type.RECORD, schema.getType());
        assertEquals(2, schema.getFields().size());
        assertEquals(dataBefore, dataAfter);
    }

    @Test
    public void shouldConvertComplexDiscreetFunctionsTypes() {
        //given
        String schemaStr =
                "{\"type\":\"record\",\"name\":\"nxcals_record\",\"namespace\":\"nxcals\",\"fields\":[" +
                        "{\"name\":\"discrete_function1\",\"type\":[" +
                        "{\"type\":\"record\",\"name\":\"discrete_function1\"," +
                        "\"fields\":[" +
                        "{\"name\":\"xArray\",\"type\":[{\"type\":\"array\",\"items\":\"double\"},\"null\"]}," +
                        "{\"name\":\"yArray\",\"type\":[{\"type\":\"array\",\"items\":\"double\"},\"null\"]}]}" +
                        ",\"null\"]}," +
                "{\"name\":\"discrete_function2\",\"type\":[" +
                        "{\"type\":\"record\",\"name\":\"discrete_function2\"," +
                        "\"fields\":[" +
                        "{\"name\":\"xArray\",\"type\":[{\"type\":\"array\",\"items\":\"double\"},\"null\"]}," +
                        "{\"name\":\"yArray\",\"type\":[{\"type\":\"array\",\"items\":\"double\"},\"null\"]}]}" +
                        ",\"null\"]}" +
                "]}";

        ImmutableData dataBefore = ImmutableData.builder()
                .add(ImmutableEntry.of("discrete_function1",  DataFactory.createDiscreteFunction(new double[] { 1.0, 2.0 }, new double[] { 3.0, 4.0 })))
                .add(ImmutableEntry.ofNull("discrete_function2", DISCRETE_FUNCTION))
                .build();

        Schema schema = parser.parse(schemaStr);
        GenericRecord avro = ImmutableDataToAvroConverter.createDataGenericRecord(dataBefore, schema);

        //when
        ImmutableData dataAfter = AvroToImmutableDataConverter.convert(avro);

        //then
        assertEquals(Schema.Type.RECORD, schema.getType());
        assertEquals(2, schema.getFields().size());
        assertEquals(dataBefore, dataAfter);
    }

    public static Object[][] allTypes() {
        return new Object[][] {
                new Object[] { new Integer(2) },
                new Object[] { 2 },
                new Object[] { new Long(2) },
                new Object[] { 2L, },
                new Object[] { new Float(2) },
                new Object[] { 2F },
                new Object[] { new Double(2) },
                new Object[] { 2D },
                new Object[] { new Boolean(true) },
                new Object[] { true },
                new Object[] { "string" },
                new Object[] {
                        DataFactory.createDiscreteFunction(new double[] { 1.0, 2.0 }, new double[] { 3.0, 4.0 }) },
                new Object[] { new int[] { 2, 3 } },
                //                new Object[] { new Long[] { 2L, 3L } }, //We currently do not support big types arrays in ImmutableData
                new Object[] { new long[] { 2L, 3L } },
                //                new Object[] { new Float[] { 2F, 3F } },
                new Object[] { new float[] { 2F, 3F } },
                //                new Object[] { new Boolean[] { true, false } },
                new Object[] { new boolean[] { true, false } },
                //                new Object[] { new Byte[] { 'a', 'b' } },
                //                new Object[] { new byte[] { 'a', 'b' } }, //bytes are also always converted to ints
                new Object[] { new String[] { "str1", "str2" } },
                new Object[] { new DiscreteFunction[] {
                        DataFactory.createDiscreteFunction(new double[] { 1.0, 2.0 }, new double[] { 3.0, 4.0 }),
                        DataFactory.createDiscreteFunction(new double[] { 10.0, 20.0 }, new double[] { 30.0, 40.0 }) } },
                new Object[] { DataFactory.createDiscreteFunctionList(
                        DataFactory.createDiscreteFunction(new double[] { 1.0, 2.0 }, new double[] { 3.0, 4.0 }),
                        DataFactory.createDiscreteFunction(new double[] { 10.0, 20.0 }, new double[] { 30.0, 40.0 }) ) },

                //DF_LIST
                new Object[] {
                        DataFactory.createDiscreteFunctionList(
                                DataFactory.createDiscreteFunction(new double[] { 1.0, 2.0 }, new double[] { 3.0, 4.0 }),
                                DataFactory.createDiscreteFunction(new double[] { 10.0, 20.0 }, new double[] { 30.0, 40.0 }) ),
                        },


                //DF_LIST_ARRAY
                new Object[] { new DiscreteFunctionList[] {
                        //DF_LIST_1
                        DataFactory.createDiscreteFunctionList(
                        DataFactory.createDiscreteFunction(new double[] { 1.0, 2.0 }, new double[] { 3.0, 4.0 }),
                        DataFactory.createDiscreteFunction(new double[] { 10.0, 20.0 }, new double[] { 30.0, 40.0 }) ),
                        //DF_LIST_2
                        DataFactory.createDiscreteFunctionList(
                                DataFactory.createDiscreteFunction(new double[] { 1.0, 2.0 }, new double[] { 3.0, 4.0 }),
                                DataFactory.createDiscreteFunction(new double[] { 10.0, 20.0 }, new double[] { 30.0, 40.0 }) )}}
        };
    }

    public static Object[][] allNullTypes() {
        return new Object[][] {
                new Object[] { EntryType.INT32 },
                new Object[] { EntryType.INT64 },
                new Object[] { EntryType.FLOAT },
                new Object[] { EntryType.DOUBLE },
                new Object[] { EntryType.BOOL },
                new Object[] { EntryType.STRING },
                new Object[] { EntryType.INT32_ARRAY },
                new Object[] { EntryType.INT64_ARRAY },
                new Object[] { EntryType.FLOAT_ARRAY },
                new Object[] { EntryType.DOUBLE_ARRAY },
                new Object[] { EntryType.BOOL_ARRAY },
                new Object[] { EntryType.STRING_ARRAY }

        };
    }

}