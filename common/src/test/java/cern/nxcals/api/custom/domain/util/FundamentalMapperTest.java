package cern.nxcals.api.custom.domain.util;

import cern.nxcals.api.custom.domain.FundamentalFilter;
import cern.nxcals.common.utils.ReflectionUtils;
import com.google.common.collect.Sets;
import org.junit.jupiter.api.Test;

import java.io.UncheckedIOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FundamentalMapperTest {
    private static final String FUNDAMENTAL_PROP_PREFIX = FundamentalMapper.FUNDAMENTAL_GROUP_PROPERTY_KEY_PREFIX;

    @Test
    public void shouldReturnEmptySetOnToFundamentalsWhenMapIsEmpty() {
        Set<FundamentalFilter> fundamentalFilters = FundamentalMapper.toFundamentals(Collections.emptyMap());

        assertTrue(fundamentalFilters.isEmpty());
    }

    @Test
    public void shouldReturnEmptySetOnToFundamentalsWhenMapHasNoFundamentalKeys() {
        Map<String, String> groupProperties = new HashMap<>();
        groupProperties.put("definetelyNotFundamentalKey", "NotEvenAJson");
        groupProperties.put("nullProperty", null);
        Set<FundamentalFilter> fundamentalFilters = FundamentalMapper.toFundamentals(groupProperties);
        assertTrue(fundamentalFilters.isEmpty());
    }

    @Test
    public void shouldThrowExceptionOnToFundamentalsWhenFundamentalContentNull() {
        Map<String, String> groupProperties = new HashMap<>();
        groupProperties.put(FUNDAMENTAL_PROP_PREFIX + "1", null);
        assertThrows(IllegalArgumentException.class, () -> FundamentalMapper.toFundamentals(groupProperties));
    }

    @Test
    public void shouldThrowOnToFundamentalsWhenFundamentalContentIsInvalid() {
        Map<String, String> groupProperties = new HashMap<>();
        groupProperties.put("definetelyNotFundamentalKey", "NotEvenAJson");
        groupProperties.put(FUNDAMENTAL_PROP_PREFIX + "1", "");
        groupProperties.put(FUNDAMENTAL_PROP_PREFIX + "2", "no_json_should not parse");
        groupProperties.put("fundamentalFilterX" + "1-wrong-prefix-ending", "{}");


        assertThrows(UncheckedIOException.class, () -> FundamentalMapper.toFundamentals(groupProperties));
    }

    @Test
    public void shouldThrowWhenFundamentalPropertyNameHasNoId() {
        Map<String, String> groupProperties = new HashMap<>();
        groupProperties.put(FUNDAMENTAL_PROP_PREFIX, "{}"); //no id suffix

        assertThrows(IllegalStateException.class, () -> FundamentalMapper.toFundamentals(groupProperties));
    }

    @Test
    public void shouldReturnEmptyInstanceOnToFundamentalsWhenFundamentalContentIsEmptyJson() {
        String filterId = "my-test-id-1";
        Map<String, String> groupProperties = new HashMap<>();
        groupProperties.put(FUNDAMENTAL_PROP_PREFIX + filterId, "{}");
        Set<FundamentalFilter> fundamentalFilters = FundamentalMapper.toFundamentals(groupProperties);
        assertEquals(1, fundamentalFilters.size());
        FundamentalFilter filter = fundamentalFilters.iterator().next();
        assertEquals(filterId, filter.getId());
        assertNull(filter.getAccelerator());
        assertNull(filter.getDestination());
        assertNull(filter.getLsaCycle());
        assertNull(filter.getTimingUser());
    }

    @Test
    public void shouldReturnFiltersOnToFundamentals() {
        Map<String, String> groupProperties = new HashMap<>();
        String filterId1 = "01";
        String filterContent1 = "{\"destination\":\"aDest\",\"lsaCycle\":\"aCycle\",\"timingUser\":\"aUser\",\"accelerator\":\"anAccelerator\"}";
        String filterId2 = "02";
        String filterContent2 = "{\"destination\":\"otherDest\",\"lsaCycle\":\"aCycle\"}";
        groupProperties.put(FUNDAMENTAL_PROP_PREFIX + filterId1, filterContent1);
        groupProperties.put(FUNDAMENTAL_PROP_PREFIX + filterId2, filterContent2);
        Set<FundamentalFilter> fundamentalFilters = FundamentalMapper.toFundamentals(groupProperties);
        assertEquals(2, fundamentalFilters.size());

        Map<String, FundamentalFilter> fundamentalFilterById = fundamentalFilters.stream()
                .collect(Collectors.toMap(FundamentalFilter::getId, Function.identity()));

        FundamentalFilter filter1 = fundamentalFilterById.get(filterId1);
        assertNotNull(filter1);
        assertEquals(filterId1, filter1.getId()); //explicit check for injected id value
        FundamentalFilter expectedFilter1 = buildFilterWithPredefinedId(filterId1, "aDest", "aCycle", "aUser",
                "anAccelerator");
        assertEquals(expectedFilter1, filter1);

        FundamentalFilter filter2 = fundamentalFilterById.get(filterId2);
        assertNotNull(filter2);
        assertEquals(filterId2, filter2.getId()); //explicit check for injected id value
        FundamentalFilter expectedFilter2 = buildFilterWithPredefinedId(filterId2, "otherDest", "aCycle", null, null);
        assertEquals(expectedFilter2, filter2);
    }

    // test: toGroupProperties

    @Test
    public void shouldReturnEmptyMapOnToGroupPropertyWhenFiltersSetIsNull() {
        Map<String, String> groupProperties = FundamentalMapper.toGroupProperties(null);
        assertNotNull(groupProperties);
        assertTrue(groupProperties.isEmpty());
    }

    @Test
    public void shouldReturnEmptyMapOnToGroupPropertyWhenFiltersSetIsEmpty() {
        Map<String, String> groupProperties = FundamentalMapper.toGroupProperties(Collections.emptySet());
        assertNotNull(groupProperties);
        assertTrue(groupProperties.isEmpty());
    }

    @Test
    public void shouldThrowOnToGroupPropertyWhenFilterSetHasNullEntry() {
        assertThrows(NullPointerException.class, () -> FundamentalMapper.toGroupProperties(Collections.singleton(null)));
    }

    @Test
    public void shouldReturnEmptyMapOnToGroupPropertyWhenFiltersHasNoFieldProperties() {
        FundamentalFilter emptyFilter = FundamentalFilter.builder().build();
        Map<String, String> groupProperties = FundamentalMapper.toGroupProperties(Collections.singleton(emptyFilter));
        assertNotNull(groupProperties);
        assertEquals(1, groupProperties.size());

        String expectedFilterJson = "{\"destination\":null,\"lsaCycle\":null,\"timingUser\":null,\"accelerator\":null}";

        String filterJson = groupProperties.get(FUNDAMENTAL_PROP_PREFIX + emptyFilter.getId());
        assertNotNull(filterJson);
        assertEquals(expectedFilterJson, filterJson);
    }

    @Test
    public void shouldReturnGroupPropertiesMapOnToGroupProperty() {
        FundamentalFilter filter1 = FundamentalFilter.builder().accelerator("accelerator1")
                .destination("dest1").lsaCycle("cycle1").timingUser("user1").build();
        FundamentalFilter filter2 = FundamentalFilter.builder().accelerator("accelerator2")
                .destination("dest2").build();

        Map<String, String> groupProperties = FundamentalMapper.toGroupProperties(Sets.newHashSet(filter1, filter2));
        assertNotNull(groupProperties);
        assertEquals(2, groupProperties.size());
        String expectedJsonFilter1 = "{\"destination\":\"dest1\",\"lsaCycle\":\"cycle1\",\"timingUser\":\"user1\",\"accelerator\":\"accelerator1\"}";
        String expectedJsonFilter2 = "{\"destination\":\"dest2\",\"lsaCycle\":null,\"timingUser\":null,\"accelerator\":\"accelerator2\"}";

        String filterJson1 = groupProperties.get(FUNDAMENTAL_PROP_PREFIX + filter1.getId());
        assertNotNull(filterJson1);
        assertEquals(expectedJsonFilter1, filterJson1);

        String filterJson2 = groupProperties.get(FUNDAMENTAL_PROP_PREFIX + filter2.getId());
        assertNotNull(filterJson2);
        assertEquals(expectedJsonFilter2, filterJson2);
    }

    private FundamentalFilter buildFilterWithPredefinedId(String id, String destination, String lsaCycle,
            String timingUser,
            String accelerator) {
        return ReflectionUtils.builderInstance(FundamentalFilter.InnerBuilder.class)
                .id(id)
                .destination(destination)
                .lsaCycle(lsaCycle)
                .timingUser(timingUser)
                .accelerator(accelerator)
                .build();
    }
}
