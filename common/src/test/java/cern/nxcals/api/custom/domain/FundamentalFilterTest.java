package cern.nxcals.api.custom.domain;

import cern.nxcals.common.utils.GlobalObjectMapperProvider;
import cern.nxcals.common.utils.ReflectionUtils;
import com.fasterxml.jackson.databind.InjectableValues;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.StringUtils;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

public class FundamentalFilterTest {

    private static final ObjectMapper MAPPER = GlobalObjectMapperProvider.get();

    @Test
    public void shouldGenerateIdOnBuild() {
        FundamentalFilter filter = FundamentalFilter.builder().build();
        assertTrue(StringUtils.isNotBlank(filter.getId()));
    }

    @Test
    public void innerBuilderShouldThrowWhenIdIsNull() {
        FundamentalFilter filter = FundamentalFilter.builder().build();
        ReflectionUtils.enhanceWithId(filter, null);
        assertNull(filter.getId());
        try {
            filter.toBuilder().build();
        } catch (NullPointerException e) {
            return;
        }
        fail("Build method did not throw exception on missing id");
    }


    @Test
    public void shouldSerialize() throws IOException {
        FundamentalFilter filter = FundamentalFilter.builder()
                .timingUser("test")
                .destination("somewhere")
                .accelerator("TBD")
                .lsaCycle("testCycle")
                .build();
        assertTrue(StringUtils.isNotBlank(filter.getId()));

        String filterJson = MAPPER.writeValueAsString(filter);
        FundamentalFilter deserializedFilter = MAPPER.reader(new InjectableValues.Std().addValue("id", filter.getId()))
                .forType(FundamentalFilter.class).readValue(filterJson);

        assertEquals(filter, deserializedFilter);
        assertEquals(filter.getId(), deserializedFilter.getId());
        assertEquals(filter.toBuilder().build(), deserializedFilter);
        assertEquals(filter, deserializedFilter.toBuilder().build());
    }

}
