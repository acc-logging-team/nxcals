package cern.nxcals.api.custom.domain;

import cern.nxcals.api.custom.domain.constants.BeamModeData;
import cern.nxcals.api.custom.domain.constants.BeamModeValidity;
import cern.nxcals.api.custom.domain.constants.DynamicTimeUnit;
import cern.nxcals.api.custom.domain.constants.LoggingTimeZone;
import cern.nxcals.api.custom.domain.snapshot.PriorTime;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Visibility;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.TimeZone;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SnapshotTest {

    private static final String SNAPSHOT_NAME = "snapshot";
    private static final String OWNER = "owner";
    private static final String SYSTEM = "cmw";
    private static final String DESCRIPTION = "desc";
    private static final Visibility VISIBILITY = Visibility.PUBLIC;

    private static final BeamModeData BEAM_MODE_END = BeamModeData.builder()
            .beamModeValue("END OF FILL")
            .validity(BeamModeValidity.builder()
                    .startTime(Instant.parse("2023-05-16T05:15:03.533738525Z"))
                    .endTime(Instant.parse("2023-05-16T05:15:03.533738525Z")).build())
            .build();

    private static final BeamModeData BEAM_MODE_START = BeamModeData.builder()
            .beamModeValue("START OF FILL")
            .validity(BeamModeValidity.builder()
                    .startTime(Instant.parse("2023-05-15T20:17:30.151238525Z"))
                    .endTime(Instant.parse("2023-05-15T20:17:30.151238525Z")).build())
            .build();

    private static final int FILL_NUMBER = 8775;

    static Stream<Arguments> dataForDynamicTimeWindowTest() {


        return Stream.of(
                Arguments.of(LoggingTimeZone.UTC, DynamicTimeUnit.SECONDS, 2, ChronoUnit.SECONDS),
                Arguments.of(LoggingTimeZone.LOCAL, DynamicTimeUnit.MINUTES, 3, ChronoUnit.MINUTES),
                Arguments.of(LoggingTimeZone.UTC, DynamicTimeUnit.HOURS, 5, ChronoUnit.HOURS),
                Arguments.of(LoggingTimeZone.LOCAL, DynamicTimeUnit.DAYS, 7, ChronoUnit.DAYS),
                Arguments.of(LoggingTimeZone.UTC, DynamicTimeUnit.WEEKS, 8, ChronoUnit.WEEKS),
                Arguments.of(LoggingTimeZone.LOCAL, DynamicTimeUnit.MONTHS, 9, ChronoUnit.MONTHS),
                Arguments.of(LoggingTimeZone.UTC, DynamicTimeUnit.YEARS, 1, ChronoUnit.YEARS)
        );
    }

    @ParameterizedTest
    @MethodSource("dataForDynamicTimeWindowTest")
    void shouldCalculateDynamicTimeWindow(LoggingTimeZone loggingTimeZone, DynamicTimeUnit dynamicTimeUnit,
                                          Integer dynamicDuration, ChronoUnit resultUnit) {
        withMockedLocalDateTimeNow(loggingTimeZone.getTimeZone().toZoneId(), () -> {
            Snapshot.TimeDefinition timeDefinition = Snapshot.TimeDefinition.dynamic(
                    dynamicDuration, loggingTimeZone, PriorTime.from("Start of day"), dynamicTimeUnit
            );
            Snapshot snapshot = Snapshot.builder()
                    .name(SNAPSHOT_NAME)
                    .owner(OWNER)
                    .description(DESCRIPTION)
                    .system(SYSTEM)
                    .visibility(VISIBILITY)
                    .timeDefinition(timeDefinition)
                    .build();

            TimeWindow timeWindow = snapshot.calculateTimeWindow();

            Instant startOfDay = LocalDateTime.now(loggingTimeZone.getTimeZone().toZoneId())
                    .truncatedTo(ChronoUnit.DAYS).atZone(loggingTimeZone.getTimeZone().toZoneId()).toInstant();
            assertEquals(startOfDay, timeWindow.getEndTime());

            ZoneId zoneId = ZoneId.of("UTC");
            if (loggingTimeZone == LoggingTimeZone.LOCAL) {
                zoneId = TimeZone.getDefault().toZoneId();
            }
            assertEquals(timeWindow.getStartTime().atZone(zoneId),
                    timeWindow.getEndTime().atZone(zoneId).minus(dynamicDuration, resultUnit));
        });
    }

    /**
     * Mock LocalDateTime::now calls to return the same date during executing test
     *
     * @param zoneId   - to mock call LocalDateTime::now(ZoneId)
     * @param runnable - test to be run
     */
    private void withMockedLocalDateTimeNow(ZoneId zoneId, Runnable runnable) {
        LocalDateTime now = LocalDateTime.now();
        ZonedDateTime zoned = now.atZone(zoneId);
        try (MockedStatic<LocalDateTime> localDateTimeMockedStatic = Mockito.mockStatic(LocalDateTime.class, Mockito.CALLS_REAL_METHODS)) {
            localDateTimeMockedStatic.when(LocalDateTime::now).thenReturn(now);
            localDateTimeMockedStatic.when(() -> LocalDateTime.now(zoneId)).thenReturn(zoned.toLocalDateTime());
            runnable.run();
        }
    }

    @Test
    void shouldCalculateTimeWindowForFill() {
        Instant startTime = Instant.parse("2023-05-15T20:17:30.151238525Z");
        Instant endTime = Instant.parse("2023-05-16T05:15:03.533738525Z");

        Snapshot.TimeDefinition timeDefinition = Snapshot.TimeDefinition.forFill(FILL_NUMBER,
                BEAM_MODE_START, BEAM_MODE_END);

        Snapshot snapshot = Snapshot.builder()
                .name(SNAPSHOT_NAME)
                .owner(OWNER)
                .description(DESCRIPTION)
                .system(SYSTEM)
                .visibility(VISIBILITY)
                .timeDefinition(timeDefinition).build();

        TimeWindow timeWindow = snapshot.calculateTimeWindow();

        assertEquals(startTime, timeWindow.getStartTime());
        assertEquals(endTime, timeWindow.getEndTime());
    }

    @Test
    void shouldCalculateFixedDatesTimeWindow() {
        Instant startTime = Instant.parse("2023-05-15T20:17:30.151238525Z");
        Instant endTime = Instant.parse("2023-05-16T05:15:03.533738525Z");

        Snapshot snapshot = Snapshot.builder()
                .name(SNAPSHOT_NAME)
                .owner(OWNER)
                .description(DESCRIPTION)
                .system(SYSTEM)
                .visibility(VISIBILITY)
                .timeDefinition(Snapshot.TimeDefinition.fixedDates(startTime, endTime)).build();

        TimeWindow timeWindow = snapshot.calculateTimeWindow();

        assertEquals(startTime, timeWindow.getStartTime());
        assertEquals(endTime, timeWindow.getEndTime());
    }
}
