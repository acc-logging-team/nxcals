package cern.nxcals.api.custom.extraction.metadata.queries;

import org.junit.jupiter.api.Test;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TagsTest {

    @Test
    void variableName() {
        //when
        String stringCond = toRSQL(Tags.suchThat().variableName().eq("v1"));
        //then
        assertTrue(stringCond.contains("v1"));
    }

    @Test
    void entityKeyValues() {
        //when
        String stringCond = toRSQL(Tags.suchThat().entityKeyValues().eq("k1"));
        //then
        assertTrue(stringCond.contains("k1"));
    }
}