package cern.nxcals.common.qbuilders.delegates;

import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.extraction.metadata.queries.BaseBuilder;
import cern.nxcals.api.extraction.metadata.queries.KeyValuesProperty;
import cern.nxcals.common.rsql.Operators;
import com.github.rutledgepaulv.qbuilders.structures.FieldPath;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.Map;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class GenericKeyValuesPropertyDelegateTest {
    private static final String SCHEMA = "{\"type\":\"record\",\"name\":\"device\",\"fields\":[{\"name\":\"device\",\"type\":\"string\"}]}";

    @Mock
    private SystemSpec systemSpec;

    public static class TestQuery extends BaseBuilder<TestQuery> {
    }

    private Map<String, Object> keyValues;

    @BeforeEach
    public void setup() {
        keyValues = new HashMap<>();
        keyValues.put("device", "test_device");
    }

    @Test
    public void testParitionAvroSchema() {
        when(systemSpec.getPartitionKeyDefinitions()).thenReturn(SCHEMA);

        GenericKeyValuesPropertyDelegate delegate = GenericKeyValuesPropertyDelegate.of(
                SystemSpec::getPartitionKeyDefinitions,
                new FieldPath("field"), new TestQuery());

        toRSQL(delegate.eq(systemSpec, keyValues));

        verify(systemSpec).getPartitionKeyDefinitions();
        verify(systemSpec, never()).getEntityKeyDefinitions();
    }

    @Test
    public void testEntityAvroSchema() {
        when(systemSpec.getEntityKeyDefinitions()).thenReturn(SCHEMA);

        GenericKeyValuesPropertyDelegate delegate = GenericKeyValuesPropertyDelegate.of(
                SystemSpec::getEntityKeyDefinitions,
                new FieldPath("field"), new TestQuery());

        toRSQL(delegate.eq(systemSpec, keyValues));

        verify(systemSpec, never()).getPartitionKeyDefinitions();
        verify(systemSpec).getEntityKeyDefinitions();
    }

    @Test
    public void eq() {
        when(systemSpec.getEntityKeyDefinitions()).thenReturn(SCHEMA);
        String rsql = toRSQL(property().eq(systemSpec,keyValues));
        assertTrue(rsql.contains(Operators.STRING_EQUALS));

    }

    @Test
    public void ne() {
        when(systemSpec.getEntityKeyDefinitions()).thenReturn(SCHEMA);
        String rsql = toRSQL(property().ne(systemSpec,keyValues));
        assertTrue(rsql.contains(Operators.STRING_NOT_EQUALS));
    }

    @Test
    public void in() {
        when(systemSpec.getEntityKeyDefinitions()).thenReturn(SCHEMA);
        String rsql = toRSQL(property().in(systemSpec,keyValues));
        assertTrue(rsql.contains("=in="));
    }

    @Test
    public void nin() {
        when(systemSpec.getEntityKeyDefinitions()).thenReturn(SCHEMA);
        String rsql = toRSQL(property().nin(systemSpec,keyValues));
        assertTrue(rsql.contains("=out="));
    }

    @Test
    public void like() {
        when(systemSpec.getEntityKeyDefinitions()).thenReturn(SCHEMA);
        String rsql = toRSQL(property().like(systemSpec,keyValues));
        assertTrue(rsql.contains(Operators.LIKE));
    }

    @Test
    public void notLike() {
        when(systemSpec.getEntityKeyDefinitions()).thenReturn(SCHEMA);
        String rsql = toRSQL(property().notLike(systemSpec,keyValues));
        assertTrue(rsql.contains(Operators.NOT_LIKE));
    }

    @Test
    public void eqString() {
        String rsql = toRSQL(property().eq("value"));
        assertTrue(rsql.contains(Operators.STRING_EQUALS));

    }

    @Test
    public void neString() {
        String rsql = toRSQL(property().ne("value"));
        assertTrue(rsql.contains(Operators.STRING_NOT_EQUALS));
    }

    @Test
    public void inString() {
        String rsql = toRSQL(property().in("value"));
        assertTrue(rsql.contains("=in="));
    }

    @Test
    public void ninString() {
        String rsql = toRSQL(property().nin("value"));
        assertTrue(rsql.contains("=out="));
    }

    @Test
    public void likeString() {
        String rsql = toRSQL(property().like("value"));
        assertTrue(rsql.contains(Operators.LIKE));
    }

    @Test
    public void notLikeString() {
        String rsql = toRSQL(property().notLike("value"));
        assertTrue(rsql.contains(Operators.NOT_LIKE));
    }

    @Test
    public void eqCaseInsensitive() {
        String rsql = toRSQL(property().caseInsensitive().eq("value"));
        assertTrue(rsql.contains(Operators.CASE_INSENSITIVE_STRING_EQUALS));

    }

    @Test
    public void neCaseInsensitive() {
        String rsql = toRSQL(property().caseInsensitive().ne("value"));
        assertTrue(rsql.contains(Operators.CASE_INSENSITIVE_STRING_NOT_EQUALS));
    }

    @Test
    public void inCaseInsensitive() {
        String rsql = toRSQL(property().caseInsensitive().in("value"));
        assertTrue(rsql.contains(Operators.CASE_INSENSITIVE_IN));
    }

    @Test
    public void ninCaseInsensitive() {
        String rsql = toRSQL(property().caseInsensitive().nin("value"));
        assertTrue(rsql.contains(Operators.CASE_INSENSITIVE_NOT_IN));
    }

    @Test
    public void likeCaseInsensitive() {
        String rsql = toRSQL(property().caseInsensitive().like("value"));
        assertTrue(rsql.contains(Operators.CASE_INSENSITIVE_LIKE));
    }

    @Test
    public void notLikeCaseInsensitive() {
        String rsql = toRSQL(property().caseInsensitive().notLike("value"));
        assertTrue(rsql.contains(Operators.CASE_INSENSITIVE_NOT_LIKE));
    }

    private KeyValuesProperty<TestQuery> property() {
        return GenericKeyValuesPropertyDelegate.of(SystemSpec::getEntityKeyDefinitions, new FieldPath("field"),
                new TestQuery());
    }
}
