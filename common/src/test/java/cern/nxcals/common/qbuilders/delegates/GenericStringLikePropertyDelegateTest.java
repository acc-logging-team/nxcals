package cern.nxcals.common.qbuilders.delegates;

import cern.nxcals.api.extraction.metadata.queries.StringLikeProperty;
import cern.nxcals.api.extraction.metadata.queries.BaseBuilder;
import cern.nxcals.common.rsql.Operators;
import com.github.rutledgepaulv.qbuilders.structures.FieldPath;
import org.junit.jupiter.api.Test;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class GenericStringLikePropertyDelegateTest {

    public static class TestQuery extends BaseBuilder<TestQuery> {
        public StringLikeProperty<TestQuery> testProp() {
            return GenericStringLikePropertyDelegate.of(new FieldPath("testProp"), self());
        }
    }

    @Test
    public void eq() {
        String rsql = toRSQL(new TestQuery().testProp().eq("test"));
        assertTrue(rsql.contains(Operators.STRING_EQUALS));
    }

    @Test
    public void ne() {
        String rsql = toRSQL(new TestQuery().testProp().ne("test"));
        assertTrue(rsql.contains(Operators.STRING_NOT_EQUALS));
    }

    @Test
    public void like() {
        String rsql = toRSQL(new TestQuery().testProp().like("test"));
        assertTrue(rsql.contains(Operators.LIKE));
    }

    @Test
    public void notLike() {
        String rsql = toRSQL(new TestQuery().testProp().notLike("test"));
        assertTrue(rsql.contains(Operators.NOT_LIKE));
    }

    @Test
    public void eqCaseInsensitive() {
        String rsql = toRSQL(new TestQuery().testProp().caseInsensitive().eq("test"));
        assertTrue(rsql.contains(Operators.CASE_INSENSITIVE_STRING_EQUALS));
    }

    @Test
    public void neCaseInsensitive() {
        String rsql = toRSQL(new TestQuery().testProp().caseInsensitive().ne("test"));
        assertTrue(rsql.contains(Operators.CASE_INSENSITIVE_STRING_NOT_EQUALS));
    }

    @Test
    public void likeCaseInsensitive() {
        String rsql = toRSQL(new TestQuery().testProp().caseInsensitive().like("test"));
        assertTrue(rsql.contains(Operators.CASE_INSENSITIVE_LIKE));
    }

    @Test
    public void notLikeCaseInsensitive() {
        String rsql = toRSQL(new TestQuery().testProp().caseInsensitive().notLike("test"));
        assertTrue(rsql.contains(Operators.CASE_INSENSITIVE_NOT_LIKE));
    }

    @Test
    public void lexicallyAfter() {
        String rsql = toRSQL(new TestQuery().testProp().lexicallyAfter("test"));
        assertTrue(rsql.contains("=gt="));
    }

    @Test
    public void lexicallyBefore() {
        String rsql = toRSQL(new TestQuery().testProp().lexicallyBefore("test"));
        assertTrue(rsql.contains("=lt="));
    }

    @Test
    public void lexicallyNotAfter() {
        String rsql = toRSQL(new TestQuery().testProp().lexicallyNotAfter("test"));
        assertTrue(rsql.contains("=le="));
    }

    @Test
    public void lexicallyNotBefore() {
        String rsql = toRSQL(new TestQuery().testProp().lexicallyNotBefore("test"));
        assertTrue(rsql.contains("=ge="));
    }

    @Test
    public void pattern() {
        String rsql = toRSQL(new TestQuery().testProp().pattern("test"));
        assertTrue(rsql.contains("=re="));
    }

    @Test
    public void exists() {
        String rsql = toRSQL(new TestQuery().testProp().exists());
        assertTrue(rsql.contains("=ex=\"true\""));
    }

    @Test
    public void doesNotExist() {
        String rsql = toRSQL(new TestQuery().testProp().doesNotExist());
        assertTrue(rsql.contains("=ex=\"false\""));
    }

    @Test
    public void in() {
        String rsql = toRSQL(new TestQuery().testProp().in("test"));
        assertTrue(rsql.contains("=in="));
    }

    @Test
    public void nin() {
        String rsql = toRSQL(new TestQuery().testProp().nin("test"));
        assertTrue(rsql.contains("=out="));
    }

    @Test
    public void inCaseInsensitive() {
        String rsql = toRSQL(new TestQuery().testProp().caseInsensitive().in("test"));
        assertTrue(rsql.contains(Operators.CASE_INSENSITIVE_IN));
    }

    @Test
    public void ninCaseInsensitive() {
        String rsql = toRSQL(new TestQuery().testProp().caseInsensitive().nin("test"));
        assertTrue(rsql.contains(Operators.CASE_INSENSITIVE_NOT_IN));
    }
}
