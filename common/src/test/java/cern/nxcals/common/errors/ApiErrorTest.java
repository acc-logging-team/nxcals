package cern.nxcals.common.errors;

import cern.nxcals.common.utils.GlobalObjectMapperProvider;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ApiErrorTest {
    @Test
    public void shouldNotAcceptMessageNull() throws IOException {
        assertThrows(NullPointerException.class, () -> ApiError.builder().message(null).build());
    }

    @Test
    public void shouldAssumeDefaults() throws IOException {
        ApiError error = ApiError.builder().message("B").build();
        assertNull(error.getCode());
        assertNotNull(error.getMessage());
        assertNull(error.getStacktrace());
        assertTrue(error.isRepeatable());
    }

    @Test
    public void shouldSerialize() throws IOException {
        ApiError error = ApiError.builder().code("A").message("B").stacktrace("C").build();
        ObjectMapper ob = GlobalObjectMapperProvider.get();
        String s = ob.writeValueAsString(error);
        ApiError newError = ob.readValue(s, ApiError.class);

        assertEquals(newError.getCode(), error.getCode());
        assertEquals(newError.getMessage(), error.getMessage());
        assertEquals(newError.getStacktrace(), error.getStacktrace());
        assertEquals(newError.isRepeatable(), error.isRepeatable());
    }
}