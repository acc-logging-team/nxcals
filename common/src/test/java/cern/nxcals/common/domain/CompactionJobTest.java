package cern.nxcals.common.domain;

import cern.nxcals.common.utils.GlobalObjectMapperProvider;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URI;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CompactionJobTest {

    private static final String ID = "id";
    private static final String TMP_F1_AVRO = "/tmp/f1.avro";
    private static final String DEST = "/dest";

    @Test
    public void shouldConstructObject() {
        DataProcessingJob data = CompactionJob.builder()
                .files(Collections.singletonList(URI.create(TMP_F1_AVRO))).destinationDir(URI.create(DEST))
                .sourceDir(URI.create("/source/1/2/3/2024-01-01"))
                .build();

        DataProcessingJob data1 = CompactionJob.builder()
                .files(Collections.singletonList(URI.create(TMP_F1_AVRO))).destinationDir(URI.create(DEST))
                .sourceDir(URI.create("/source/1/2/3/2024-01-01"))
                .build();

        assertEquals(DEST, data.getDestinationDir().toString());
        assertEquals(1, data.getFiles().size());
        assertEquals(TMP_F1_AVRO, data.getFiles().get(0).toString());
        assertEquals(data, data1);
        //        assertTrue(data.toString().contains("Files=[/tmp/f1.avro]"));
    }

    @Test
    public void shouldSerializeAndDeserialize() throws IOException {
        CompactionJob data = CompactionJob.builder()
                .files(Collections.singletonList(URI.create(TMP_F1_AVRO)))
                .sortEnabled(true)
                .destinationDir(URI.create(DEST))
                .sourceDir(URI.create("/source/1/2/3/2024-01-01"))
                .build();

        ObjectMapper ob = GlobalObjectMapperProvider.get();

        String s = ob.writerWithDefaultPrettyPrinter().writeValueAsString(data);

        CompactionJob dataProcessJobData = ob.readValue(s, CompactionJob.class);

        assertEquals(data, dataProcessJobData);
        assertEquals(data.isSortEnabled(), dataProcessJobData.isSortEnabled());
    }
}
