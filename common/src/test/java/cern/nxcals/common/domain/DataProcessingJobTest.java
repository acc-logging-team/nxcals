package cern.nxcals.common.domain;

import cern.nxcals.api.domain.TimeEntityPartitionType;
import cern.nxcals.common.utils.GlobalObjectMapperProvider;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.hadoop.fs.Path;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DataProcessingJobTest {
    private static final ObjectMapper MAPPER = GlobalObjectMapperProvider.get();


    private DataProcessingJob[] getJobs() {
        return new DataProcessingJob[]{
                CompactionJob.builder().destinationDir(new Path("/test").toUri())
                        .filePrefix("")
                        .files(List.of(new Path("/file").toUri()))
                        .partitionCount(1)
                        .sortEnabled(false)
                        .sourceDir(new Path("/source/1/2/3/2024-01-01").toUri())
                        .systemId(2)
                        .totalSize(100)
                        .build(),
                AdaptiveCompactionJob.builder().destinationDir(new Path("/test").toUri())
                        .filePrefix("")
                        .files(List.of(new Path("/file").toUri()))
                        .parquetRowGroupSize(100)
                        .hdfsBlockSize(200)
                        .schemaId(2)
                        .partitionId(3)
                        .sortEnabled(false)
                        .sourceDir(new Path("/source/1/2/3/2024-01-01").toUri())
                        .systemId(2)
                        .totalSize(100)
                        .partitionType(TimeEntityPartitionType.of(2, 2))
                        .build(),
                MergeCompactionJob.builder().destinationDir(new Path("/test").toUri())
                        .filePrefix("")
                        .files(List.of(new Path("/file").toUri()))
                        .partitionCount(1)
                        .sortEnabled(false)
                        .sourceDir(new Path("/source/1/2/3/2024-01-01").toUri())
                        .systemId(2)
                        .totalSize(100)
                        .restagingReportFile(new Path("/tst").toUri())
                        .build(),
                RestageJob.builder().destinationDir(new Path("/test").toUri())
                        .filePrefix("")
                        .files(List.of(new Path("/file").toUri()))
                        .sourceDir(new Path("/source/1/2/3/2024-01-01").toUri())
                        .systemId(2)
                        .totalSize(100)
                        .build(),
                AdaptiveMergeCompactionJob.builder()
                        .destinationDir(new Path("/test").toUri())
                        .filePrefix("")
                        .files(List.of(new Path("/file").toUri()))
                        .partitionType(TimeEntityPartitionType.of(2, 2))
                        .sortEnabled(false)
                        .sourceDir(new Path("/source/1/2/3/2024-01-01").toUri())
                        .systemId(2)
                        .totalSize(100)
                        .restagingReportFile(new Path("/tst").toUri())
                        .build()
        };


    }

    @Test
    void shouldSerializeToJson() throws JsonProcessingException {
        DataProcessingJob[] jobs = getJobs();
        String json = MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(jobs);
        DataProcessingJob[] read = MAPPER.readValue(json, DataProcessingJob[].class);
        for (int i = 0; i < read.length; ++i) {
            assertEquals(jobs[i], read[i]);
        }
    }

}
