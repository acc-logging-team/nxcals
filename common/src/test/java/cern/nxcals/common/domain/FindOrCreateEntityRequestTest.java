package cern.nxcals.common.domain;

import cern.nxcals.common.utils.GlobalObjectMapperProvider;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class FindOrCreateEntityRequestTest {

    static final Map<String, Object> PARTITION_KEY_VALUES = ImmutableMap.of("name", "3");
    private static final Map<String, Object> ENTITY_KEY_VALUES = ImmutableMap
            .of("entity_string", "string", "entity_double", 1d);
    static final Schema SCHEMA = SchemaBuilder.record("schema").fields()
            .name("schema").type().stringType().noDefault().endRecord();

    @Test
    public void shouldNotCreateWithoutEntityKeyValues() {
        assertThrows(NullPointerException.class, () -> FindOrCreateEntityRequest.builder()
                .partitionKeyValues(PARTITION_KEY_VALUES).schema(SCHEMA.toString()).build());
    }

    @Test
    public void shouldNotCreateWithoutPartitionKeyValues() {
        assertThrows(NullPointerException.class, () -> FindOrCreateEntityRequest.builder()
                .entityKeyValues(ENTITY_KEY_VALUES)
                .schema(SCHEMA.toString()).build());
    }

    @Test
    public void shouldSerialize() throws IOException {
        FindOrCreateEntityRequest findOrCreateEntityRequest1 = FindOrCreateEntityRequest.builder()
                .entityKeyValues(ENTITY_KEY_VALUES)
                .partitionKeyValues(PARTITION_KEY_VALUES).schema(SCHEMA.toString()).build();
        ObjectMapper ob = GlobalObjectMapperProvider.get();
        String s = ob.writeValueAsString(findOrCreateEntityRequest1);
        FindOrCreateEntityRequest findOrCreateEntityRequest2 = ob.readValue(s, FindOrCreateEntityRequest.class);
        assertEquals(findOrCreateEntityRequest1.getEntityKeyValues(), findOrCreateEntityRequest2.getEntityKeyValues());
        assertEquals(findOrCreateEntityRequest1.getSchema(), findOrCreateEntityRequest2.getSchema());
        assertEquals(findOrCreateEntityRequest1.getPartitionKeyValues(),
                findOrCreateEntityRequest2.getPartitionKeyValues());
        assertEquals(findOrCreateEntityRequest1, findOrCreateEntityRequest2);
    }
}
