package cern.nxcals.common.utils;

import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.concurrent.Executors;
import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class LazyTest {

    @Test
    public void onlyCalculateOnce() {
        Integer value = 10;

        Supplier<Integer> source = mock(Supplier.class);
        when(source.get()).thenReturn(value);

        Lazy<Integer> integerLazy = new Lazy<>(source);

        verify(source, times(0)).get();

        assertEquals(value, integerLazy.get());
        assertEquals(value, integerLazy.get());
        assertEquals(value, integerLazy.get());
        assertEquals(value, integerLazy.get());

        verify(source, times(1)).get();
    }

    @Test
    public void shouldCalculateOnlyOnceMultithreaded() throws InterruptedException {
        Integer value = 10;

        Supplier<Integer> source = mock(Supplier.class);
        when(source.get()).thenReturn(value);

        Lazy<Integer> integerLazy = new Lazy<>(source);

        Executors.newFixedThreadPool(10)
                .invokeAll(Collections.nCopies(10, integerLazy::get));

        verify(source, times(1)).get();
    }

    @Test
    public void onlyCalculateOnceForNull() {
        Supplier<Integer> source = mock(Supplier.class);
        when(source.get()).thenReturn(null);

        Lazy<Integer> integerLazy = new Lazy<>(source);

        verify(source, times(0)).get();

        assertNull(integerLazy.get());
        assertNull(integerLazy.get());
        assertNull(integerLazy.get());
        assertNull(integerLazy.get());

        verify(source, times(1)).get();
    }

    @Test
    public void shouldPropagateExceptions() {
        Supplier<Integer> source = mock(Supplier.class);
        when(source.get()).thenThrow(new NullPointerException());

        Lazy<Integer> integerLazy = new Lazy<>(source);

        assertThrows(NullPointerException.class, () -> integerLazy.get());
    }

}