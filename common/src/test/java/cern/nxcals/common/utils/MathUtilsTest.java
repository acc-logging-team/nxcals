package cern.nxcals.common.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MathUtilsTest {

    @Test
    void shouldExecutePositiveModulo() {
        //the output should be positive
        assertEquals(8, MathUtils.pmod(-100L, 9));
        assertEquals(1, MathUtils.pmod(-7, 4));
        assertEquals(3, MathUtils.pmod(-1, 4));
        assertEquals(0, MathUtils.pmod(-8, 4));
        assertEquals(0, MathUtils.pmod(8, 4));

    }
}