package cern.nxcals.common.utils;

import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class HdfsFileUtilsTest {
    @Mock
    private FileSystem fileSystem;

    @Mock
    private RemoteIterator<LocatedFileStatus> iterator;
    @Mock
    private LocatedFileStatus fileStatus;

    @Test
    public void shouldCreateNestedDirectoriesOutOfProvidedDate() {
        String dateToNestedPaths = HdfsFileUtils.expandDateToNestedPaths("2018-04-10");

        assertEquals("2018/4/10", dateToNestedPaths);
    }

    @Test
    public void shouldFindFilesInPath() throws IOException {
        //given
        Path path = new Path("/test");
        when(fileSystem.exists(path)).thenReturn(true, false);
        when(fileSystem.listFiles(path, true)).thenReturn(iterator);
        when(iterator.hasNext()).thenReturn(true, false);
        when(iterator.next()).thenReturn(fileStatus);
        //when
        List<FileStatus> filesInPath = HdfsFileUtils
                .findFilesInPath(fileSystem, path, HdfsFileUtils.SearchType.RECURSIVE, status -> true);
        //then
        assertEquals(1, filesInPath.size());

    }

    private void prepareForHasDataFiles(Path path, Path file) throws IOException {
        when(fileSystem.exists(path)).thenReturn(true, false);
        when(fileSystem.listFiles(path, true)).thenReturn(iterator);
        when(iterator.hasNext()).thenReturn(true, false);
        when(iterator.next()).thenReturn(fileStatus);
        when(fileStatus.isFile()).thenReturn(true);
        when(fileStatus.getPath()).thenReturn(file);
    }

    @Test
    public void shouldHaveDataFiles() throws IOException {
        //given
        Path path = new Path("/test");
        Path file = new Path("/test/file.parquet");

        prepareForHasDataFiles(path, file);
        //when
        boolean value = HdfsFileUtils.hasDataFiles(fileSystem, path);

        //then
        assertTrue(value);
    }


    @Test
    public void shouldNotHaveDataFiles() throws IOException {
        //given
        Path path = new Path("/test");
        Path file = new Path("/test/file.not_data");

        prepareForHasDataFiles(path, file);
        //when
        boolean value = HdfsFileUtils.hasDataFiles(fileSystem, path);

        //then
        assertFalse(value);
    }

    @Test
    public void shouldReturnTimePartition() {
        assertEquals(2, HdfsFileUtils.getTimePartition(new Path("/2/some.file")).orElseThrow());
        assertEquals(3, HdfsFileUtils.getTimePartition(new Path("/3/some.file")).orElseThrow());
    }

    @Test
    public void shouldNotReturnTimePartition() {
        assertFalse(HdfsFileUtils.getTimePartition(new Path("/not_int/some.file")).isPresent());
    }
}
