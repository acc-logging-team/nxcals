package cern.nxcals.common.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StringUtilsTest {

    private final static String WITH_UNDERSCORES = "value_with_underscores";
    private final static String WITH_UNDERSCORES_RESULT = "value\\_with\\_underscores";
    private final static String WITH_PERCENTAGES = "value%with%percentages";
    private final static String WITH_PERCENTAGES_RESULT = "value\\%with\\%percentages";
    private final static String WITH_PERCENTAGES_AND_UNDERSCORES = "value_with%percentages%and_Underscores";
    private final static String WITH_PERCENTAGES_AND_UNDERSCORES_RESULT = "value\\_with\\%percentages\\%and\\_Underscores";

    @Test
    public void performSQLEscapingTestUnderscores() {
        // given
        String stringToEscape = WITH_UNDERSCORES;
        String escapedString = WITH_UNDERSCORES_RESULT;
        // when
        String result = StringUtils.escapeWildcards(stringToEscape);
        // then
        assertEquals(escapedString, result);
    }

    @Test
    public void performSQLEscapingTestPercentages() {
        // given
        String stringToEscape = WITH_PERCENTAGES;
        String escapedString = WITH_PERCENTAGES_RESULT;
        // when
        String result = StringUtils.escapeWildcards(stringToEscape);
        // then
        assertEquals(escapedString, result);
    }

    @Test
    public void performSQLEscapingTestPercentagesAndUnderscores() {
        // given
        String stringToEscape = WITH_PERCENTAGES_AND_UNDERSCORES;
        String escapedString = WITH_PERCENTAGES_AND_UNDERSCORES_RESULT;
        // when
        String result = StringUtils.escapeWildcards(stringToEscape);
        // then
        assertEquals(escapedString, result);
    }


}
