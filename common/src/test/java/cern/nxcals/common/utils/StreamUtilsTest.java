package cern.nxcals.common.utils;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StreamUtilsTest {

    public static Object[][] params() {
        return new Object[][] { new Object[] { Arrays.asList(1, 2), 3, false },
                new Object[] { Arrays.asList(1, 2, 3, 4), 3, true },
                new Object[] { Arrays.asList(1, 2), 0, true },
                new Object[] { Arrays.asList(1, 2), -1, true },
                new Object[] { Arrays.asList(1, 2), Integer.MAX_VALUE, false }
        };
    }

    @ParameterizedTest
    @MethodSource("params")
    void shouldTestStreamUtils(List<Integer> collection, int limit, boolean isParallel) {
        //given
        //when
        Stream<Integer> result = StreamUtils.of(collection, limit);
        //then
        assertEquals(isParallel, result.isParallel());
    }
}