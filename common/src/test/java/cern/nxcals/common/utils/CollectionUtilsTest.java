package cern.nxcals.common.utils;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Test;

import java.util.List;

import static cern.nxcals.common.utils.CollectionUtils.splitIntoParts;
import static org.junit.jupiter.api.Assertions.assertEquals;

class CollectionUtilsTest {
    @Getter
    @RequiredArgsConstructor
    private static class Record {
        final long size;
    }

    private static final Record smallRecord = new Record(100);
    private static final Record bigRecord = new Record(100_000_000);

    @Test
    void splitIntoPartsShouldCorrectlySplitListWithRecordsBySizeWhenSmallRecordsAreAtBeginning() {
        // given
        List<Record> records = List.of(smallRecord, smallRecord, bigRecord, smallRecord, smallRecord);

        // when
        List<List<Record>> partitionedList = splitIntoParts(records, 1000, 100, Record::getSize);

        // then
        assertEquals(2, partitionedList.size());
        assertEquals(3, partitionedList.get(0).size());
        assertEquals(2, partitionedList.get(1).size());
    }

    @Test
    void splitIntoPartsShouldReturnEmptyListIfNoRecordsPassed() {
        // given

        List<Record> records = List.of();

        // when
        List<List<Record>> partitionedList = splitIntoParts(records, 1000, 100, Record::getSize);

        // then
        assertEquals(0, partitionedList.size());
    }

    @Test
    void splitIntoPartsShouldCorrectlyNotSplitIfPartitionSizeIsSmall() {
        // given
        List<Record> records = List.of(smallRecord, smallRecord, smallRecord);

        // when
        List<List<Record>> partitionedList = splitIntoParts(records, 1000, 100, Record::getSize);

        // then
        assertEquals(1, partitionedList.size());
        assertEquals(3, partitionedList.get(0).size());
    }

    @Test
    void splitIntoPartsShouldCorrectlySplitBySize() {
        // given

        List<Record> records = List.of(bigRecord, bigRecord, bigRecord);

        // when
        List<List<Record>> partitionedList = splitIntoParts(records, 1000, 100, Record::getSize);

        // then
        assertEquals(3, partitionedList.size());
        assertEquals(1, partitionedList.get(0).size());
        assertEquals(1, partitionedList.get(1).size());
        assertEquals(1, partitionedList.get(2).size());
    }

    @Test
    void splitIntoPartsShouldCorrectlySplitByNumberOfRecords() {
        // given
        List<Record> records = List.of(smallRecord, smallRecord, smallRecord, smallRecord,
                smallRecord);

        // when
        List<List<Record>> partitionedList = CollectionUtils.splitIntoParts(records, 1000, 2, Record::getSize);

        // then
        assertEquals(3, partitionedList.size());
        assertEquals(2, partitionedList.get(0).size());
        assertEquals(2, partitionedList.get(1).size());
        assertEquals(1, partitionedList.get(2).size());
    }
}
