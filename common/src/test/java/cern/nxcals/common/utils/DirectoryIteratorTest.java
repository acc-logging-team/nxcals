package cern.nxcals.common.utils;

import lombok.AllArgsConstructor;
import lombok.Value;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DirectoryIteratorTest {

    @Test
    public void shouldFailOnNullFileSystem() throws IOException {
        assertThrows(NullPointerException.class, () -> new DirectoryIterator(null, new Path("root")));
    }

    @Test
    public void shouldFailOnNullRoot() throws IOException {
        assertThrows(NullPointerException.class, () -> new DirectoryIterator(mockFileSystem(file("file")), null));
    }

    @Test
    public void shouldDoNextWithoutHasNext() throws IOException {
        FileSystem fs = mockFileSystem(directory("root"));

        DirectoryIterator i = new DirectoryIterator(fs, new Path("root"));

        assertEquals("root", i.next().getName());
    }

    @Test
    public void shouldNotDoRemoveWithoutHasNext() throws IOException {
        FileSystem fs = mockFileSystem(directory("root"));

        DirectoryIterator i = new DirectoryIterator(fs, new Path("root"));

        assertThrows(IllegalStateException.class, () -> i.remove());
    }

    @Test
    public void shouldThrowNoSuchElementOnTermination() throws IOException {
        FileSystem fs = mockFileSystem(directory("root"));

        DirectoryIterator i = new DirectoryIterator(fs, new Path("root"));

        i.next();
        assertThrows(NoSuchElementException.class, () -> i.next());
    }

    @Test
    public void shouldFindEmptyRootDirectory() throws IOException {
        FileSystem fs = mockFileSystem(directory("root"));

        DirectoryIterator i = new DirectoryIterator(fs, new Path("root"));

        Set<String> visited = collectVisited(i);

        assertVisited(visited, "root");
    }

    @Test
    public void shouldFindDeepDirectories() throws IOException {
        FileSystem fs = mockFileSystem(
                directory("root",
                        file("file"),
                        directory("subdirectory",
                                directory("subdirectoryA"))));

        DirectoryIterator i = new DirectoryIterator(fs, new Path("root"));

        Set<String> visited = collectVisited(i);

        assertVisited(visited, "subdirectoryA", "subdirectory", "root");
    }

    @Test
    public void shouldFindEmptyDeepDirectory() throws IOException {
        FileSystem fs = mockFileSystem(
                directory("root",
                        file("file"),
                        directory("subdirectory",
                                directory("subdirectoryA"))));

        DirectoryIterator i = new DirectoryIterator(fs, new Path("root"), FileStatus::isDirectory);

        Set<String> visited = collectVisited(i);

        assertVisited(visited, "subdirectoryA");
    }

    @Test
    public void shouldFindMultipleEmptyDeepDirectories() throws IOException {
        FileSystem fs = mockFileSystem(
                directory("root",
                        file("file"),
                        directory("subdirectory",
                                directory("subdirectoryA"),
                                directory("subdirectoryB"))));

        DirectoryIterator i = new DirectoryIterator(fs, new Path("root"), FileStatus::isDirectory);

        Set<String> visited = collectVisited(i);

        assertVisited(visited, "subdirectoryA", "subdirectoryB");
    }

    @Test
    public void shouldSkipDirectoryIfContainsForbiddenFiles() throws IOException {
        FileSystem fs = mockFileSystem(
                directory("root",
                        file("forbidden")));

        DirectoryIterator i = new DirectoryIterator(fs, new Path("root"),
                child -> child.getPath().getName().equals("forbidden"));

        Set<String> visited = collectVisited(i);

        assertVisited(visited);
    }

    @Test
    public void shouldSkipDirectoryIfContainsForbiddenDirectories() throws IOException {
        FileSystem fs = mockFileSystem(
                directory("root",
                        directory("forbidden")));

        DirectoryIterator i = new DirectoryIterator(fs, new Path("root"),
                child -> child.getPath().getName().equals("forbidden"));

        Set<String> visited = collectVisited(i);

        assertVisited(visited, "forbidden");
    }

    @Test
    public void shouldReturnLeafDirectories() throws IOException {
        FileSystem fs = mockFileSystem(
                directory("root",
                        directory("subDir1"),
                        directory("subDir2"),
                        directory("subDir3")));

        DirectoryIterator i = new DirectoryIterator(fs, new Path("root"), FileStatus::isDirectory);

        Set<String> visited = collectVisited(i);

        assertVisited(visited, "subDir1", "subDir2", "subDir3");
    }

    @Test
    public void shouldReturnDirectoriesWithFiles() throws IOException {
        FileSystem fs = mockFileSystem(
                directory("root",
                        directory("subDir1",
                                file("a.file")),
                        directory("subDir2",
                                file("a.file")),
                        directory("subDir3",
                                file("b.file"))));

        DirectoryIterator i = new DirectoryIterator(fs, new Path("root"),
                child -> !child.getPath().getName().equals("a.file"));

        Set<String> visited = collectVisited(i);

        assertVisited(visited, "subDir1", "subDir2");
    }

    @Test
    public void shouldReturnLeafDirectoriesWithoutFiles() throws IOException {
        FileSystem fs = mockFileSystem(
                directory("root",
                        directory("subDir1",
                                file("a.file")),
                        directory("subDir2",
                                file("a.file")),
                        directory("subDir3",
                                file("b.file"))));

        DirectoryIterator i = new DirectoryIterator(fs, new Path("root"),
                t -> t.isDirectory() || t.getPath().getName().equals("a.file"));

        Set<String> visited = collectVisited(i);

        assertVisited(visited, "subDir3");
    }

    @Test
    public void shouldReturnDirectoriesWithoutFiles() throws IOException {
        FileSystem fs = mockFileSystem(
                directory("root",
                        directory("subDir1",
                                file("a.file")),
                        directory("subDir2",
                                file("a.file")),
                        directory("subDir3",
                                file("b.file"))));

        DirectoryIterator i = new DirectoryIterator(fs, new Path("root"),
                child -> child.getPath().getName().equals("a.file"));

        Set<String> visited = collectVisited(i);

        assertVisited(visited, "root", "subDir3");
    }

    @Test
    public void shouldReturnLeafDirectoriesWithFiles() throws IOException {
        FileSystem fs = mockFileSystem(
                directory("root",
                        directory("subDir1",
                                file("a.file")),
                        directory("subDir2",
                                file("a.file")),
                        directory("subDir3",
                                file("b.file"))));

        DirectoryIterator i = new DirectoryIterator(fs, new Path("root"),
                child -> child.isDirectory() || !child.getPath().getName().equals("a.file"));

        Set<String> visited = collectVisited(i);

        assertVisited(visited, "subDir1", "subDir2");
    }

    @Test
    public void shouldReturnDirectoriesWithoutAvroOrParquet() throws IOException {
        FileSystem fs = mockFileSystem(
                directory("root",
                        directory("withAvro",
                                file("a.avro"),
                                file("a.other")),
                        directory("withParquet",
                                file("a.parquet"),
                                file("a.other")),
                        directory("withBoth",
                                file("a.parquet"),
                                file("a.avro")),
                        directory("withOther",
                                file("a.other"),
                                file("b.other")),
                        directory("empty")));

        DirectoryIterator i = new DirectoryIterator(fs, new Path("root"),
                child -> child.isDirectory() || child.getPath().getName().endsWith(".avro") || child.getPath().getName()
                        .endsWith(".parquet"));

        Set<String> visited = collectVisited(i);

        assertVisited(visited, "withOther", "empty");
    }

    private void assertVisited(Set<String> visited, String... required) {
        assertEquals(new HashSet<>(Arrays.asList(required)), visited);
    }

    private Set<String> collectVisited(DirectoryIterator iterator) throws IOException {
        Set<String> visited = new HashSet<>();
        iterator.forEachRemaining(v -> visited.add(v.getName()));
        return visited;
    }

    private FileSystem mockFileSystem(MockFile root) throws IOException {
        FileSystem fs = mock(FileSystem.class);

        when(fs.listStatusIterator(any(Path.class))).thenReturn(new ReusableMockRemoteIterator());
        return mockFileSystem(fs, root);
    }

    private FileSystem mockFileSystem(FileSystem fs, MockFile root) throws IOException {
        when(fs.listStatusIterator(new Path(root.getPath())))
                .thenReturn(new ReusableMockRemoteIterator(root.getChildren()));

        for (MockFile child : root.children) {
            mockFileSystem(fs, child);
        }

        return fs;
    }

    private MockFile file(String name) {
        return new MockFile(name, true, new MockFile[0]);
    }

    private MockFile directory(String name, MockFile... children) {
        return new MockFile(name, false, children);
    }

    @Value
    @AllArgsConstructor
    private class MockFile {
        protected String path;
        protected boolean isFile;
        protected MockFile[] children;
    }

    private class ReusableMockRemoteIterator implements RemoteIterator<FileStatus> {
        private List<MockFile> values;
        private int index;

        ReusableMockRemoteIterator(MockFile... values) {
            this.values = new LinkedList<>(Arrays.asList(values));
            this.index = 0;
        }

        @Override
        public boolean hasNext() throws IOException {
            if (index >= values.size()) {
                index = 0;
                return false;
            } else {
                return true;
            }
        }

        @Override
        public FileStatus next() throws IOException {
            return fileStatusMock(values.get(index++));
        }

        private FileStatus fileStatusMock(MockFile value) {
            LocatedFileStatus result = mock(LocatedFileStatus.class);
            when(result.getPath()).thenReturn(new Path(value.getPath()));
            when(result.isFile()).thenReturn(value.isFile());
            when(result.isDirectory()).thenReturn(!value.isFile());
            return result;
        }
    }
}