package cern.nxcals.common.utils;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MapUtilsTest {

    @Test
    void mapKeys() {
        // given
        HashMap<String, String> map = new HashMap<>();
        map.put("1", "1");

        // when
        Map<Long, String> newMap = MapUtils.mapKeys(map, Long::valueOf);

        // then
        assertTrue(newMap.containsKey(1L));
        assertEquals("1", newMap.get(1L));
    }

    @Test
    void mapCollector() {
        // given
        String key1 = "a";
        String key2 = "b";
        Set<Pair<String, Long>> pairs = Set.of(
                Pair.of(key1, 1L),
                Pair.of(key2, 2L)
        );

        // when
        Map<String, Long> map = pairs.stream().collect(MapUtils.mapCollector());

        // then
        assertEquals(Set.of(key1, key2), map.keySet());
        assertEquals(pairs, map.entrySet());
    }

    @Test
    void revertMap() {
        // given
        String key1 = "1";
        long val11 = 1L;
        long val12 = 2L;

        String key2 = "2";
        long val2 = 3L;
        Map<String, Set<Long>> map = Map.of(key1, Set.of(val11, val12), key2, Set.of(val2));

        // when
        Map<Long, String> newMap = MapUtils.revertMap(map);

        // then
        assertEquals(Set.of(val11, val12, val2), newMap.keySet());
        assertEquals(key1, newMap.get(val11));
        assertEquals(key1, newMap.get(val12));
        assertEquals(key2, newMap.get(val2));
    }
}