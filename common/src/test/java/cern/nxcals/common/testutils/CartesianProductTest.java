package cern.nxcals.common.testutils;

import lombok.val;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CartesianProductTest {

    static final String firstString = "first";
    static final String secondString = "second";

    static Stream<Arguments> cartesianProductOfTwoArrays() {
        return Stream.of(
                Arguments.of(0, firstString, 1),
                Arguments.of(1, firstString, 2),
                Arguments.of(2, secondString, 1),
                Arguments.of(3, secondString, 2)
        );
    }

    @ParameterizedTest
    @MethodSource("cartesianProductOfTwoArrays")
    void shouldCorrectlyCreateCartesianProductForTwoArrays(int index, String firstElement, int secondElement) {
        String[] firstArray = { firstString, secondString };
        Integer[] secondArray = { 1, 2 };
        Stream<Arguments> resultStream = CartesianProduct.compose(firstArray, secondArray);
        val resultStreamAsList = resultStream.map(Arguments::get).collect(Collectors.toList());
        assertTrue(Arrays.deepEquals(resultStreamAsList.get(index), Arguments.of(firstElement, secondElement).get()));
    }

    @Test
    void shouldCorrectlyCreateCartesianProductForThreeArrays() {
        String[] array = { "first", "second" };
        Stream<Arguments> resultStream = CartesianProduct.compose(array, array, array);
        assertEquals(8, resultStream.count());
    }

    @Test
    void shouldCorrectlyCreateCartesianProductForFourArrays() {
        String[] array = { "first", "second" };
        Stream<Arguments> resultStream = CartesianProduct.compose(array, array, array, array);
        assertEquals(16, resultStream.count());
    }

    @Test
    void shouldCorrectlyCreateCartesianProductForFiveArrays() {
        String[] array = { "first", "second" };
        Stream<Arguments> resultStream = CartesianProduct.compose(array, array, array, array, array);
        assertEquals(32, resultStream.count());
    }

    @Test
    void shouldCorrectlyCreateCartesianProductForSixArrays() {
        String[] array = { "first", "second" };
        Stream<Arguments> resultStream = CartesianProduct.compose(array, array, array, array, array, array);
        assertEquals(64, resultStream.count());
    }

}