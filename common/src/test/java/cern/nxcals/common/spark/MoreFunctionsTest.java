package cern.nxcals.common.spark;

import cern.nxcals.common.utils.IllegalCharacterConverter;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.api.java.UDF1;
import org.apache.spark.sql.types.ArrayType;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static cern.nxcals.common.avro.SchemaConstants.ARRAY_ELEMENTS_FIELD_NAME;
import static cern.nxcals.common.spark.MoreFunctions.asRow;
import static cern.nxcals.common.spark.MoreFunctions.createNullableField;
import static cern.nxcals.common.spark.MoreFunctions.createTypeFromFields;
import static cern.nxcals.common.spark.UDFFilterOperand.BETWEEN;
import static cern.nxcals.common.spark.UDFFilterOperand.EQUALS;
import static cern.nxcals.common.spark.UDFFilterOperand.GREATER;
import static cern.nxcals.common.spark.UDFFilterOperand.GREATER_OR_EQUALS;
import static cern.nxcals.common.spark.UDFFilterOperand.IN;
import static cern.nxcals.common.spark.UDFFilterOperand.LESS;
import static cern.nxcals.common.spark.UDFFilterOperand.LESS_OR_EQUALS;
import static cern.nxcals.common.spark.UDFFilterOperand.NOT_IN;
import static cern.nxcals.common.spark.UDFVectorRestriction.ALL;
import static cern.nxcals.common.spark.UDFVectorRestriction.ANY;
import static com.google.common.collect.Lists.newArrayList;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.apache.spark.sql.types.DataTypes.DoubleType;
import static org.apache.spark.sql.types.DataTypes.LongType;
import static org.apache.spark.sql.types.DataTypes.StringType;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MoreFunctionsTest {
    private Row row = mock(Row.class);
    private StructType schema = mock(StructType.class);
    private StructField field = mock(StructField.class);
    private ArrayType type = mock(ArrayType.class);

    private final Broadcast<long[]> startRangesBroadcast = mock(Broadcast.class);

    public static Collection<Object[]> data() {
        //@formatter:off
        Object[][] data = new Object[][] {
                { ALL, newArrayList(1L, 2L), LongType, GREATER, newArrayList(1.0), FALSE },
                { ALL, newArrayList(10L, 20L, 30L), LongType, GREATER, newArrayList(10.0), FALSE },
                { ALL, newArrayList(10L, 20L, 30L), LongType, GREATER_OR_EQUALS, newArrayList(10.0), TRUE },
                { ALL, newArrayList(10L, 20L, 30L), LongType, LESS, newArrayList(10.0), FALSE },
                { ALL, newArrayList(10L, 20L, 30L), LongType, LESS_OR_EQUALS, newArrayList(100.0), TRUE },
                { ALL, newArrayList(10L, 20L, 30L), LongType, EQUALS, newArrayList(10.0), FALSE },
                {ALL, newArrayList(10L, 20L, 30L ), LongType, IN, newArrayList(10, 20), FALSE},
                {ALL, newArrayList(10L, 20L, 30L ), LongType, NOT_IN, newArrayList(10, 20), FALSE},
                {ALL, newArrayList(10L, 20L, 30L ), LongType, NOT_IN, newArrayList(50, 60), TRUE},
                {ALL, newArrayList(1L, 2L ), LongType, BETWEEN, newArrayList(1.0,2.0), TRUE},
                {ALL, newArrayList(1L, 2L, 3L ), LongType, BETWEEN, newArrayList(1.0,2.0), FALSE},


                {ANY, newArrayList(1L, 2L ), LongType, GREATER, newArrayList(1.0), TRUE},
                {ANY, newArrayList(10L, 20L, 30L ), LongType, GREATER, newArrayList(10.0), TRUE},
                {ANY, newArrayList(10L, 20L, 30L ), LongType, GREATER_OR_EQUALS, newArrayList(10.0), TRUE},
                {ANY, newArrayList(10L, 20L, 30L ), LongType, LESS, newArrayList(10.0), FALSE},
                {ANY, newArrayList(10L, 20L, 30L ), LongType, LESS_OR_EQUALS, newArrayList(100.0), TRUE},
                {ANY, newArrayList(10L, 20L, 30L ), LongType, EQUALS, newArrayList(10.0), TRUE},
                {ANY, newArrayList(10L, 20L, 30L ), LongType, IN, newArrayList(10, 20), TRUE},
                {ANY, newArrayList(10L, 20L, 30L ), LongType, NOT_IN, newArrayList(10, 20), TRUE},
                {ANY, newArrayList(10L, 20L, 30L ), LongType, NOT_IN, newArrayList(50, 60), TRUE},
                {ANY, newArrayList(1L, 2L ), LongType, BETWEEN, newArrayList(1.0,2.0), TRUE},
                {ANY, newArrayList(1L, 2L, 3L ), LongType, BETWEEN, newArrayList(1.0,2.0), TRUE},


                {ALL, newArrayList(1L, 2L ), DoubleType, GREATER, newArrayList(1.0,2.0), FALSE},
                {ALL, newArrayList(10L, 20L, 30L ), DoubleType, GREATER, newArrayList(10.0), FALSE},
                {ALL, newArrayList(10L, 20L, 30L ), DoubleType, GREATER_OR_EQUALS, newArrayList(10.0), TRUE},
                {ALL, newArrayList(10L, 20L, 30L ), DoubleType, LESS, newArrayList(10.0), FALSE},
                {ALL, newArrayList(10L, 20L, 30L ), DoubleType, LESS_OR_EQUALS, newArrayList(100.0), TRUE},
                {ALL, newArrayList(10L, 20L, 30L ), DoubleType, EQUALS, newArrayList(10.0), FALSE},
                {ALL, newArrayList(10L, 20L, 30L ), DoubleType, IN, newArrayList(10, 20), FALSE},
                {ALL, newArrayList(10L, 20L, 30L ), DoubleType, NOT_IN, newArrayList(10, 20), FALSE},
                {ALL, newArrayList(10L, 20L, 30L ), DoubleType, NOT_IN, newArrayList(50, 60), TRUE},
                {ALL, newArrayList(1L, 2L ), DoubleType, BETWEEN, newArrayList(1.0,2.0), TRUE},
                {ALL, newArrayList(1L, 2L, 3L ), DoubleType, BETWEEN, newArrayList(1.0,2.0), FALSE},


                {ANY, newArrayList(1L, 2L ), DoubleType, GREATER, newArrayList(1.0), TRUE},
                {ANY, newArrayList(10L, 20L, 30L ), DoubleType, GREATER, newArrayList(10.0), TRUE},
                {ANY, newArrayList(10L, 20L, 30L ), DoubleType, GREATER_OR_EQUALS, newArrayList(10.0), TRUE},
                {ANY, newArrayList(10L, 20L, 30L ), DoubleType, LESS, newArrayList(10.0), FALSE},
                {ANY, newArrayList(10L, 20L, 30L ), DoubleType, LESS_OR_EQUALS, newArrayList(100.0), TRUE},
                {ANY, newArrayList(10L, 20L, 30L ), DoubleType, EQUALS, newArrayList(10.0), TRUE},
                {ANY, newArrayList(10L, 20L, 30L ), DoubleType, IN, newArrayList(10, 20), TRUE},
                {ANY, newArrayList(10L, 20L, 30L ), DoubleType, NOT_IN, newArrayList(10, 20), TRUE},
                {ANY, newArrayList(10L, 20L, 30L ), DoubleType, NOT_IN, newArrayList(50, 60), TRUE},
                {ANY, newArrayList(1L, 2L ), DoubleType, BETWEEN, newArrayList(1.0,2.0), TRUE},
                {ANY, newArrayList(1L, 2L, 3L ), DoubleType, BETWEEN, newArrayList(1.0,2.0), TRUE},


                {ALL, newArrayList("a", "b", "c" ), StringType, EQUALS, newArrayList("a"), FALSE},
                {ALL, newArrayList("a","b","c" ), StringType, IN, newArrayList("a", "b"), FALSE},
                {ALL, newArrayList("a","b","c" ), StringType, NOT_IN, newArrayList("a", "b"), FALSE},
                {ALL, newArrayList("a","b","c" ), StringType, NOT_IN, newArrayList("d", "f", "g"), TRUE},

                {ANY, newArrayList("a", "b", "c" ), StringType, EQUALS, newArrayList("a"), TRUE},
                { ANY, newArrayList("a", "b", "c"), StringType, IN, newArrayList("a", "b"), TRUE },
                { ANY, newArrayList("a", "b", "c"), StringType, NOT_IN, newArrayList("a", "b"), TRUE },
                { ANY, newArrayList("a", "b", "c"), StringType, NOT_IN, newArrayList("d", "f", "g"), TRUE },

        };
        //@formatter:on
        return Arrays.asList(data);

    }

    public static Collection<Object[]> findFirstLessThanOrEqualTestData() {
        //@formatter:off
        Object[][] data = new Object[][] {
                //long[] startRanges, Long value, Long expectedStartRange
                { new long[] {}, null, null },
                { new long[] {}, 4L, null },
                { new long[] { 2L }, 1L, null },
                { new long[] { 2L }, 2L, 2L },
                { new long[] { 2L }, 3L, 2L },
                { new long[] { 1L, 2L, 5L, 7L, 13L, 20L }, -10L, null },
                { new long[] { 1L, 2L, 5L, 7L, 13L, 20L }, 1L, 1L },
                { new long[] { 1L, 2L, 5L, 7L, 13L, 20L }, 2L, 2L },
                { new long[] { 1L, 2L, 5L, 7L, 13L, 20L }, 3L, 2L },
                { new long[] { 1L, 2L, 5L, 7L, 13L, 20L }, 19L, 13L },
                { new long[] { 1L, 2L, 5L, 7L, 13L, 20L }, 20L, 20L },
                { new long[] { 1L, 2L, 5L, 7L, 13L, 20L }, 25L, 20L },
        };
        //@formatter:on
        return Arrays.asList(data);

    }

    // creates the test data

    @BeforeEach
    public void setup() {
        StructField[] fields = new StructField[] { field };
        when(row.schema()).thenReturn(schema);
        when(schema.fields()).thenReturn(fields);
        when(schema.fieldIndex(ARRAY_ELEMENTS_FIELD_NAME)).thenReturn(0);
        when(field.dataType()).thenReturn(type);
    }

    @ParameterizedTest
    @MethodSource("data")
    public void vectorFilterTest(UDFVectorRestriction restriction, List<Object> values, DataType dataType,
                                 UDFFilterOperand operand, List<Object> predicateValues, Boolean result) throws Exception {
        //given
        when(type.elementType()).thenReturn(dataType);
        when(row.getList(0)).thenReturn(values);
        UDF1<Row, Boolean> udf1 = MoreFunctions.vectorFilter(predicateValues, operand, restriction);

        //when
        Boolean output = udf1.call(row);

        //then
        assertEquals(result, output);

    }

    @ParameterizedTest
    @MethodSource("findFirstLessThanOrEqualTestData")
    public void findFistLessThanOrEqualTest(long[] startRanges, Long value, Long expectedStartRange) throws Exception {
        //given
        when(startRangesBroadcast.value()).thenReturn(startRanges);
        UDF1<Long, Long> udf = MoreFunctions.findFirstLessThanOrEqual(startRangesBroadcast);

        //when
        Long startRange = udf.call(value);

        //then
        assertEquals(expectedStartRange, startRange);
    }

    @Test
    void asRowShouldReturnCorrectSparkRow() {
        Row row = asRow(1, "str", 0.11);
        assertAll(
                () -> assertEquals(3, row.size()),
                () -> assertEquals(1, row.getInt(0)),
                () -> assertEquals("str", row.getString(1)),
                () -> assertEquals(0.11, row.getDouble(2))
        );
    }

    @Test
    void decodeColumnNames() {
        final String FIELD_NAME = "FIELD";
        final String NESTED_FIELD_NAME_1 = "NESTED_FIELD.1";
        final String FIELD_NAME_2 = "FIELD.2";
        final String FIELD_NAME_3 = "FIELD.3";
        final String NESTED_FIELD_NAME_3 = "NESTED_FIELD.3";

        IllegalCharacterConverter converter = IllegalCharacterConverter.get();

        StructType nestedData2WithEncodedFields = createTypeFromFields(
                createNullableField(converter.convertToLegal(FIELD_NAME_2), DataTypes.LongType),
                createNullableField(converter.convertToLegal(FIELD_NAME_3), DataTypes.StringType));

        StructType nestedData1WithEncodedFields = createTypeFromFields(
                createNullableField(converter.convertToLegal(FIELD_NAME_2), DataTypes.LongType),
                createNullableField(converter.convertToLegal(FIELD_NAME_3), DataTypes.StringType),
                createNullableField(converter.convertToLegal(NESTED_FIELD_NAME_3), nestedData2WithEncodedFields));

        StructType sparkSchemaWithEncodedFields = createTypeFromFields(
                createNullableField(FIELD_NAME, DataTypes.LongType),
                createNullableField(converter.convertToLegal(NESTED_FIELD_NAME_1), nestedData1WithEncodedFields));

        StructType nestedData2Fields = createTypeFromFields(
                createNullableField(FIELD_NAME_2, DataTypes.LongType),
                createNullableField(FIELD_NAME_3, DataTypes.StringType));

        StructType nestedData1Fields = createTypeFromFields(
                createNullableField(FIELD_NAME_2, DataTypes.LongType),
                createNullableField(FIELD_NAME_3, DataTypes.StringType),
                createNullableField(NESTED_FIELD_NAME_3, nestedData2Fields));

        StructType sparkSchema = createTypeFromFields(
                createNullableField(FIELD_NAME, DataTypes.LongType),
                createNullableField(NESTED_FIELD_NAME_1, nestedData1Fields));

        assertEquals(sparkSchema, MoreFunctions.decodeColumnNames(sparkSchemaWithEncodedFields));
    }
}