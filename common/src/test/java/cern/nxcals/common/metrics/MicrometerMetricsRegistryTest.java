package cern.nxcals.common.metrics;

import com.google.common.util.concurrent.AtomicDouble;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mockito;

import java.util.concurrent.atomic.AtomicLong;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MicrometerMetricsRegistryTest {

    private MeterRegistry meterRegistry;

    private MetricsRegistry instance;

    final static double delta = 0.00001;

    @BeforeEach
    public void setUp() throws Exception {
        meterRegistry = new SimpleMeterRegistry();
    }

    @Test
    public void testThrowExceptionOnCreateWhenMeterRegistryIsNull() {
        assertThrows(NullPointerException.class, () -> new MicrometerMetricsRegistry(null),
                "MicrometerMetricsRegistry should deny creation of instance with null MeterRegistry!");
    }

    @Test
    public void testThrowExceptionOnGaugeWhenNameIsNull() {
        instance = new MicrometerMetricsRegistry(meterRegistry);
        assertThrows(IllegalArgumentException.class,
                () -> instance.updateGaugeTo(null, 0),
                "MicrometerMetricsRegistry should not allow exporting metrics when name is null!"
        );
    }

    @Test
    public void testThrowExceptionOnGaugeWhenNameIsEmptyString() {
        instance = new MicrometerMetricsRegistry(meterRegistry);
        assertThrows(IllegalArgumentException.class,
                () -> instance.updateGaugeTo("", 0),
                "MicrometerMetricsRegistry should not allow exporting metrics when name is empty string!"
        );
    }

    @Test
    public void testThrowExceptionOnGaugeWhenNameIsWhitespaceString() {
        instance = new MicrometerMetricsRegistry(meterRegistry);
        assertThrows(IllegalArgumentException.class,
                () -> instance.updateGaugeTo("  ", 0),
                "MicrometerMetricsRegistry should not allow exporting metrics when name is whitespace string!"
        );
    }

    @Test
    public void testInitExportGaugeMetric() {
        String fileSizeMetricName = "cern.nxcals.metric.test";

        instance = new MicrometerMetricsRegistry(meterRegistry);

        instance.updateGaugeTo(fileSizeMetricName, 0d);

        assertEquals(0d, meterRegistry.get(fileSizeMetricName).gauge().value(), delta);
    }

    @Test
    public void testExportAndUpdateGaugeDoubleMetricMeterRegistryShouldBeCalledOnce() {
        String fileSizeMetricName = "cern.nxcals.metric.test";

        meterRegistry = Mockito.mock(MeterRegistry.class);
        when(meterRegistry.gauge(eq(fileSizeMetricName), any(AtomicDouble.class)))
                .thenAnswer(i -> i.getArgument(1));

        instance = new MicrometerMetricsRegistry(meterRegistry);

        instance.updateGaugeTo(fileSizeMetricName, 0d);
        instance.updateGaugeTo(fileSizeMetricName, 10d);
        instance.updateGaugeTo(fileSizeMetricName, 4d);

        verify(meterRegistry, times(1)).gauge(eq(fileSizeMetricName), any(AtomicDouble.class));
    }

    @Test
    public void testExportAndUpdateGaugeLongMetricMeterRegistryShouldBeCalledOnce() {
        String fileSizeMetricName = "cern.nxcals.metric.test";

        meterRegistry = Mockito.mock(MeterRegistry.class);
        when(meterRegistry.gauge(eq(fileSizeMetricName), any(AtomicLong.class)))
                .thenAnswer(i -> i.getArgument(1));

        instance = new MicrometerMetricsRegistry(meterRegistry);

        instance.updateGaugeTo(fileSizeMetricName, 0l);
        instance.updateGaugeTo(fileSizeMetricName, 10l);
        instance.updateGaugeTo(fileSizeMetricName, 4l);

        verify(meterRegistry, times(1)).gauge(eq(fileSizeMetricName), any(AtomicLong.class));
    }

    @Test
    public void testExportAndUpdateGaugeDoubleMetric() {
        String fileSizeMetricName = "cern.nxcals.metric.test";

        instance = new MicrometerMetricsRegistry(meterRegistry);

        instance.updateGaugeTo(fileSizeMetricName, 0);
        instance.updateGaugeTo(fileSizeMetricName, 4);
        instance.updateGaugeTo(fileSizeMetricName, 10);

        assertEquals(0.0, meterRegistry.get(fileSizeMetricName).gauge().value(), 10 + delta);
    }

    @Test
    public void testExportAndUpdateGaugeLongMetric() {
        String fileSizeMetricName = "cern.nxcals.metric.test";

        instance = new MicrometerMetricsRegistry(meterRegistry);

        instance.updateGaugeTo(fileSizeMetricName, 0l);
        instance.updateGaugeTo(fileSizeMetricName, 4l);
        instance.updateGaugeTo(fileSizeMetricName, 10l);

        assert(meterRegistry.get(fileSizeMetricName).gauge().value() == 10l);
    }

    @Test
    public void testThrowExceptionOnCounterWhenNameIsNull() {
        instance = new MicrometerMetricsRegistry(meterRegistry);

        assertThrows(IllegalArgumentException.class,
                () -> instance.incrementCounterBy(null, 2),
                "MicrometerMetricsRegistry should not allow exporting metrics when name is null!"
        );
    }

    @Test
    public void testThrowExceptionOnCounterWhenNameIsEmptyString() {
        instance = new MicrometerMetricsRegistry(meterRegistry);

        assertThrows(IllegalArgumentException.class,
                () -> instance.incrementCounterBy("", 2),
                "MicrometerMetricsRegistry should not allow exporting metrics when name is empty string!"
        );
    }

    @Test
    public void testThrowExceptionOnCounterWhenNameIsWhitespaceString() {
        instance = new MicrometerMetricsRegistry(meterRegistry);

        assertThrows(IllegalArgumentException.class,
                () -> instance.incrementCounterBy("  ", 2),
                "MicrometerMetricsRegistry should not allow exporting metrics when name is whitespace string!"
        );
    }

    @Test
    public void testExportCountMetricWithDefaultZeroValue() {
        String fileSizeMetricName = "cern.nxcals.metric.test.count";

        instance = new MicrometerMetricsRegistry(meterRegistry);

        instance.incrementCounterBy(fileSizeMetricName, 0);

        assertEquals(0.0, meterRegistry.get(fileSizeMetricName).counter().count(), 0);
    }

    @Test
    public void testExportCountMetric() {
        String fileSizeMetricName = "cern.nxcals.metric.test.count";

        instance = new MicrometerMetricsRegistry(meterRegistry);

        instance.incrementCounterBy(fileSizeMetricName, 2);

        assertEquals(2.0, meterRegistry.get(fileSizeMetricName).counter().count(), 0);
    }

}
