package cern.nxcals.common.metrics;

import io.micrometer.core.instrument.MeterRegistry;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class MetricsPulseTest {

    @Mock
    private MeterRegistry meterRegistry;

    @Mock
    private ScheduledExecutorService executorService;

    private MetricsRegistry metricsRegistry;

    @BeforeEach
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
        metricsRegistry = new MicrometerMetricsRegistry(meterRegistry);
    }

    @Test
    public void shouldRegister() {
        MetricsPulse metricsPulse = new MetricsPulse(executorService, metricsRegistry);

        metricsPulse.register("A", () -> 10L, 10, TimeUnit.SECONDS);

        Mockito.verify(executorService, Mockito.times(1)).scheduleAtFixedRate(
                ArgumentMatchers.any(), ArgumentMatchers.eq(1L), ArgumentMatchers.eq(10L),
                ArgumentMatchers.eq(TimeUnit.SECONDS));
    }
}
