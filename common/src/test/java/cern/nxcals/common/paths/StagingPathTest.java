package cern.nxcals.common.paths;

import cern.nxcals.api.utils.TimeUtils;
import org.apache.hadoop.fs.Path;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class StagingPathTest {

    @Test
    public void shouldFailForInvalidDateInThePath() {
        Path path = new Path("1/2/3/2017_01_01");

        assertThrows(IllegalArgumentException.class, () -> new StagingPath(path, true));
    }

    @Test
    public void shouldFailForAbsentSchemaData() {
        Path path = new Path("2017-01-01");

        assertThrows(IllegalArgumentException.class, () -> new StagingPath(path, true));
    }

    @Test
    public void shouldFailForWrongSchemaData() {
        Path path = new Path("1/2/xx/2017-01-01");

        assertThrows(IllegalArgumentException.class, () -> new StagingPath(path, true));
    }

    @Test
    public void shouldFailForAbsentPartitionData() {
        Path path = new Path("10/2017-01-01");

        assertThrows(IllegalArgumentException.class, () -> new StagingPath(path, true));
    }

    @Test
    public void shouldFailForWrongPartitionData() {
        Path path = new Path("1/xx/10/2017-01-01");

        assertThrows(IllegalArgumentException.class, () -> new StagingPath(path, true));
    }

    @Test
    public void shouldFailForAbsentSystemData() {
        Path path = new Path("10/10/2017-01-01");

        assertThrows(IllegalArgumentException.class, () -> new StagingPath(path, true));
    }

    @Test
    public void shouldFailForWrongSystemData() {
        Path path = new Path("xx/10/10/2017-01-01");

        assertThrows(IllegalArgumentException.class, () -> new StagingPath(path, true));
    }

    @Test
    public void shouldFailForExcessiveData() {
        Path path = new Path("10/10/2017-01-01/ANY_DIR");

        assertThrows(IllegalArgumentException.class, () -> new StagingPath(path, true));
    }

    @Test
    public void shouldParseCorrectly() {
        Path path = new Path("/ANY_DIR/ANY_DIR/1/2/3/2017-03-02");

        StagingPath stagingPath = new StagingPath(path, true);

        assertEquals(path, stagingPath.toPath());
        assertEquals(path.toUri(), stagingPath.toPath().toUri());
        assertEquals(path.toString(), stagingPath.toString());

        assertEquals(1, stagingPath.getSystemId());
        assertEquals(new Path("/ANY_DIR/ANY_DIR/1/"), stagingPath.getSystemPath());

        assertEquals(2, stagingPath.getPartitionId());
        assertEquals(new Path("/ANY_DIR/ANY_DIR/1/2/"), stagingPath.getPartitionPath());

        assertEquals(3, stagingPath.getSchemaId());
        assertEquals(new Path("/ANY_DIR/ANY_DIR/1/2/3/"), stagingPath.getSchemaPath());

        assertEquals("2017-03-02", stagingPath.getDatePath().getName());
        ZonedDateTime date = stagingPath.getDate();
        assertEquals(2017, date.getYear());
        assertEquals(3, date.getMonthValue());
        assertEquals(2, date.getDayOfMonth());

        assertEquals(new Path("/ANY_DIR/ANY_DIR/"), stagingPath.getRoot());
    }

    @Test
    public void shouldSortByDate() {
        Path path1 = new Path("/ANY_DIR_1/ANY_DIR/1/2/3/2017-03-02");
        Path path2 = new Path("/ANY_DIR_2/ANY_DIR/1/2/3/2018-03-02");

        TreeSet<StagingPath> paths = new TreeSet<>();

        paths.add(new StagingPath(path1));
        paths.add(new StagingPath(path2));
        assertEquals(paths.pollFirst().toPath(), path2);
        assertEquals(paths.pollFirst().toPath(), path1);
    }

    @Test
    public void shouldSortByPath() {
        Path path1 = new Path("/ANY_DIR/ANY_DIR/1/2/3/2017-03-02");
        Path path2 = new Path("/ANY_DIR/ANY_DIR/2/2/3/2017-03-02");

        TreeSet<StagingPath> paths = new TreeSet<>();

        paths.add(new StagingPath(path1));
        paths.add(new StagingPath(path2));

        assertEquals(path2, paths.pollFirst().toPath());
        assertEquals(path1, paths.pollFirst().toPath());
    }

    @Test
    public void shouldHashCorrectly() {
        Path path1 = new Path("/ANY_DIR_1/ANY_DIR_1/1/2/3/2017-03-02");
        Path path2 = new Path("/ANY_DIR_2/ANY_DIR_2/1/2/3/2017-03-02");

        Set<StagingPath> paths = new HashSet<>();

        paths.add(new StagingPath(path1));
        paths.add(new StagingPath(path2));

        assertEquals(1, paths.size());
    }

    @Test
    public void shouldGetInstant() {
        Path path = new Path("/ANY_DIR/ANY_DIR/1/2/3/2017-03-02");
        Instant instant = StagingPath.toInstant(path.toUri());
        assertEquals("2017-03-02", TimeUtils.getStringDateFromInstant(instant));
    }

    @Test
    public void shouldEquals() {
        Path path1 = new Path("/ANY_DIR_1/ANY_DIR_1/1/2/3/2017-03-02");
        Path path2 = new Path("/ANY_DIR_2/ANY_DIR_2/1/2/3/2017-03-02");
        StagingPath stagingPath1 = new StagingPath(path1);
        StagingPath stagingPath2 = new StagingPath(path2);

        assertEquals(stagingPath1, stagingPath2);

    }

    @Test
    public void shouldNotEquals() {
        Path path1 = new Path("/ANY_DIR_1/ANY_DIR_1/1/2/4/2017-03-02");
        Path path2 = new Path("/ANY_DIR_2/ANY_DIR_2/1/2/3/2017-03-02");
        StagingPath stagingPath1 = new StagingPath(path1);
        StagingPath stagingPath2 = new StagingPath(path2);

        assertNotEquals(stagingPath1, stagingPath2);

    }
}
