package cern.nxcals.common.grpc;

import io.grpc.CallOptions;
import io.grpc.Channel;
import io.grpc.ClientCall;
import io.grpc.Metadata;
import io.grpc.MethodDescriptor;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class RbacTokenInterceptorTest {

    @Mock
    private MethodDescriptor.Marshaller<Object> marshaller;

    @Mock
    private Channel channelNext;
    @Mock
    private ClientCall.Listener<Object> responseListener;

    @Mock
    private ClientCall<Object, Object> clientCall;

    @Test
    public void shouldAddTokenBytesToMetadata() throws Exception {
        //given
        when(channelNext.newCall(any(), any())).thenReturn(clientCall);

        MethodDescriptor<Object, Object> methodDescriptor = MethodDescriptor.newBuilder().setFullMethodName("Test")
                .setType(
                        MethodDescriptor.MethodType.UNARY).setRequestMarshaller(marshaller)
                .setResponseMarshaller(marshaller).build();

        RbacTokenInterceptor rbacTokenInterceptor = RbacTokenInterceptor.builder().token(() -> new byte[] { 1, 2, 3 })
                .build();
        Metadata metadata = new Metadata();
        //when
        ClientCall<Object, Object> clientCall = rbacTokenInterceptor
                .interceptCall(methodDescriptor, CallOptions.DEFAULT, channelNext);

        clientCall.start(responseListener, metadata);

        //then
        assertTrue(metadata.containsKey(RbacTokenInterceptor.TOKEN_KEY));
        byte[] bytes = metadata.get(RbacTokenInterceptor.TOKEN_KEY);
        assertEquals(3, bytes.length);

    }

}
