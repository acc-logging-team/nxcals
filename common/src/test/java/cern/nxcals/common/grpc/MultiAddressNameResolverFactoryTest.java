package cern.nxcals.common.grpc;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static cern.nxcals.common.grpc.MultiAddressNameResolverFactory.MultiAddressNameResolver.HOST_PORT_SEPARATOR;
import static cern.nxcals.common.grpc.MultiAddressNameResolverFactory.MultiAddressNameResolver.MACHINE_DELIMS;
import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;

@TestInstance(PER_CLASS)
class MultiAddressNameResolverFactoryTest {
    private static final String DEV = "cs-ccr-dev";
    private static final String PORT = "500";
    private static final MultiAddressNameResolverFactory factory = new MultiAddressNameResolverFactory();

    @ParameterizedTest
    @MethodSource("invalidAddress")
    void noInstanceWithInvalidAddress(URI address) {
        assertThrows(IllegalArgumentException.class, () -> factory.newNameResolver(address, null));
    }

    @ParameterizedTest
    @MethodSource("validAddress")
    void instanceWithValidAddress(TestAddress tuple) {
        assertEquals(tuple.getMachinesCount(), factory.newNameResolver(tuple.getAddress(), null).getAddresses().size());
    }

    @SuppressWarnings("squid:UnusedPrivateMethod")
    private Object[] invalidAddress() {
        return Stream.of(null, DEV, DEV + HOST_PORT_SEPARATOR, PORT).map(
                MultiAddressNameResolverFactoryTest::toUri).toArray();
    }

    @SuppressWarnings("squid:UnusedPrivateMethod")
    private Object[] validAddress() {
        int count = ThreadLocalRandom.current().nextInt(10) + 10;
        List<TestAddress> data = new ArrayList<>();
        for (String delim : MACHINE_DELIMS) {
            data.addAll(IntStream.rangeClosed(1, count).mapToObj(toMachines(count)).map(toTestAddress(delim))
                    .collect(toList()));
        }
        return data.toArray();
    }

    private static Function<List<String>, TestAddress> toTestAddress(String delim) {
        return addresses -> new TestAddress(String.join(delim, addresses), addresses.size());
    }

    private static URI toUri(String address) {
        try {
            return new URI(MultiAddressNameResolverFactory.SCHEME + "://" + address);
        } catch (URISyntaxException e) {
            throw new RuntimeException("Incorrect address: " + address);
        }
    }

    private static IntFunction<List<String>> toMachines(int count) {
        return i -> IntStream.rangeClosed(i, count).mapToObj(k -> DEV + k + HOST_PORT_SEPARATOR + k).collect(toList());
    }

    @AllArgsConstructor
    @ToString
    private static class TestAddress {
        @NonNull
        private final String address;
        @Getter
        private final int machinesCount;

        public URI getAddress() {
            return toUri(address);
        }
    }

}
