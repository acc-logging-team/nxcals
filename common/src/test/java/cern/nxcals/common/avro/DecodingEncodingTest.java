package cern.nxcals.common.avro;

import cern.cmw.datax.ImmutableData;
import cern.nxcals.api.converters.ImmutableDataToAvroConverter;
import cern.nxcals.api.domain.EntitySchema;
import cern.nxcals.common.utils.AvroUtils;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.BinaryDecoder;
import org.apache.avro.io.DecoderFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.ByteArrayInputStream;
import java.util.function.Function;

import static org.apache.avro.Schema.Type.STRING;
import static org.apache.avro.Schema.create;
import static org.apache.avro.SchemaBuilder.record;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DecodingEncodingTest {
    private static final DataEncoderImpl encoder = new DataEncoderImpl();
    private static final Schema.Parser parser = new Schema.Parser();
    private static final String CLASS = "class";
    private static final String PROPERTY = "property";
    private static final String TEST = "test";
    private static final String V = "prop";
    private static final String VALUE = "value";

    private Schema schema = record("TEST").namespace("ns").fields().name(CLASS).type(create(STRING)).noDefault()
            .name(PROPERTY).type(create(STRING)).noDefault().endRecord();

    @Mock
    private Function<Long, EntitySchema> schemaProvider;

    @Test
    public void shouldGetGenericRecord() {
        //given
        byte[] bytes = createBytes();
        DefaultBytesToGenericRecordDecoder decoder = new DefaultBytesToGenericRecordDecoder(schemaProvider);
        when(schemaProvider.apply(1L)).thenReturn(EntitySchema.builder().schemaJson(schema.toString()).build());
        //when

        GenericRecord record = decoder.apply(1L, bytes);

        //then
        assertEquals(TEST, record.get(CLASS).toString());
        assertEquals(V, record.get(PROPERTY).toString());
    }

    @Test
    void shouldGetGenericRecordWithBigArray() { // can be used for profiling decoder
        //given
        int size = 10 * 1024 * 1024;

        int[] array = new int[size];
        for (int i = 0; i < size; i++) {
            array[i] = i;
        }
        ImmutableData data = ImmutableData.builder()
                .add(CLASS, TEST)
                .add(PROPERTY, V)
                .add(VALUE, array).build();

        String schemaString = encoder.encodeRecordFieldDefinitions(data);
        Schema schema = parser.parse(schemaString);

        GenericRecord recordBefore = ImmutableDataToAvroConverter.createDataGenericRecord(data, schema);

        byte[] bytes = DefaultGenericRecordToBytesEncoder.convertToBytes(recordBefore);

        GenericDatumReader<GenericRecord> reader = new GenericDatumReader<>(schema);
        BinaryDecoder decoder = emptyDecoder();
        //when

        GenericRecord recordAfter = null;

        for (int i = 0; i < 1; i++) { // warm-up, for real test increase number of repetitions
            recordAfter = AvroUtils.deserializeFromAvro(bytes, reader, decoder, null);
        }

        long startTime = System.currentTimeMillis();
        for (int i = 0; i < 1; i++) { // test
            recordAfter = AvroUtils.deserializeFromAvro(bytes, reader, decoder, null);
        }
        long endTime = System.currentTimeMillis();

        System.out.println(endTime - startTime);

        //then
        assertEquals(TEST, recordAfter.get(CLASS).toString());
        assertEquals(V, recordAfter.get(PROPERTY).toString());
        assertNotNull(recordAfter.get(VALUE));
    }

    private byte[] createBytes() {
        GenericRecord record = new GenericData.Record(schema);
        record.put(CLASS, TEST);
        record.put(PROPERTY, V);

        return DefaultGenericRecordToBytesEncoder.convertToBytes(record);
    }

    private BinaryDecoder emptyDecoder() {
        return DecoderFactory.get().binaryDecoder(new ByteArrayInputStream(new byte[0]), null);
    }
}
