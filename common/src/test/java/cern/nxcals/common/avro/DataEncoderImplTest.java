/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.common.avro;

import cern.cmw.data.DataFactory;
import cern.cmw.data.DiscreteFunction;
import cern.cmw.datax.DataBuilder;
import cern.cmw.datax.ImmutableData;
import cern.cmw.datax.ImmutableEntry;
import cern.nxcals.api.domain.EntitySchema;
import cern.nxcals.api.domain.KeyValues;
import cern.nxcals.common.SystemFields;
import cern.nxcals.common.converters.TimeConverter;
import cern.nxcals.common.converters.TimeConverterImpl;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static cern.cmw.datax.EntryType.STRING;
import static cern.nxcals.common.Schemas.ENTITY_ID;
import static cern.nxcals.common.Schemas.PARTITION_ID;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class DataEncoderImplTest {
    public static final Schema entityKeyDefSchema = new Schema.Parser().parse(
            "{\"type\":\"record\",\"name\":\"CmwKey\",\"namespace\":\"cern.cals\",\"fields\":[{\"name\":\"device\",\"type\":\"string\"},{\"name\":\"property\",\"type\":\"string\"}]}");

    public static final Schema partitionKeyDefSchema = new Schema.Parser().parse(
            "{\"type\":\"record\",\"name\":\"CmwPartition\",\"namespace\":\"cern.cals\",\"fields\":[{\"name\":\"class\",\"type\":\"string\"},{\"name\":\"property\",\"type\":\"string\"}]}");

    public static final Schema recordVersionKeyDefSchemaNullable = new Schema.Parser()
            .parse("{\"type\":\"record\",\"name\":\"RecordVersion\",\"namespace\":\"cern.cals\",\"fields\":\n"
                    + "[{\"name\":\"record_version1\",\"type\":[\"string\",\"null\"]},{\"name\":\"record_version2\",\"type\":[\"int\",\"null\"]}]}");

    public static final Schema recordVersionKeyDefSchema = new Schema.Parser().parse(
            "{\"type\":\"record\",\"name\":\"RecordVersion\",\"namespace\":\"cern.cals\",\"fields\":[{\"name\":\"record_version1\",\"type\":\"string\"},{\"name\":\"record_version2\",\"type\":\"int\"}]}");

    public static final Schema timeKeyDefSchema = new Schema.Parser().parse(
            "{\"type\":\"record\",\"name\":\"CmwPT\",\"namespace\":\"cern.cals\",\"fields\":[{\"name\":\"_p_t_\",\"type\":\"long\"}]}");

    private final TimeConverter timeConverter = new TimeConverterImpl();

    private static boolean checkType(Schema schema, String field, String type) {
        return schema.getField(field).schema().getTypes().stream().map(Schema::getName).anyMatch(type::equals);
    }

    @BeforeEach
    public void setUp() {

    }

    @Test
    public void testEncodeEntityKeyValues() {
        // given
        DataEncoderImpl serializer = new DataEncoderImpl(
                entityKeyDefSchema, partitionKeyDefSchema,
                timeKeyDefSchema, recordVersionKeyDefSchemaNullable, timeConverter);
        ImmutableData data = createTestCmwData(10);

        // when
        KeyValues cachedRequest = serializer.encodeEntityKeyValues(data);

        // then
        assertNotNull(cachedRequest);
        assertNotNull(cachedRequest.getId());
        assertNotNull(cachedRequest.getKeyValues());
    }

    @Test
    public void testEncodePartitionKeyValues() {
        // given
        DataEncoderImpl serializer = new DataEncoderImpl(
                entityKeyDefSchema, partitionKeyDefSchema,
                timeKeyDefSchema, recordVersionKeyDefSchemaNullable, timeConverter);
        ImmutableData data = createTestCmwData(10);

        // when
        KeyValues partitionKeyValues = serializer.encodePartitionKeyValues(data);

        // then
        assertNotNull(partitionKeyValues);
        assertNotNull(partitionKeyValues.getKeyValues());
    }

    private void verifyFields(ImmutableData data, Schema schema) {
        for (ImmutableEntry entry : data.getEntries()) {
            assertNotNull(schema.getField(entry.getName()));
        }
    }

    @Test
    public void testEncodeSchemaWithNullRecordVersion() {
        // given
        DataEncoderImpl encoder = new DataEncoderImpl(entityKeyDefSchema, partitionKeyDefSchema,
                timeKeyDefSchema, null, timeConverter);
        ImmutableData data = createTestCmwData(null);

        // when
        String recordSchemaStr = encoder.encodeRecordFieldDefinitions(data);
        assertNotNull(recordSchemaStr);
        Schema recordSchema = new Schema.Parser().parse(recordSchemaStr);

        // then
        assertTrue(checkType(recordSchema, "boolField", "boolean"));
        assertTrue(checkType(recordSchema, "longField1", "long"));
        assertTrue(checkType(recordSchema, "shortField1", "int")); // short maps to int in Avro
        assertTrue(checkType(recordSchema, "doubleField", "double"));

        assertTrue(checkType(recordSchema, "boolField", "boolean"));
        assertTrue(checkType(recordSchema, "longField1", "long"));
        assertTrue(checkType(recordSchema, "shortField1", "int")); // short maps to int in Avro
        assertTrue(checkType(recordSchema, "doubleField", "double"));
        //check for all data fields
        verifyFields(data, recordSchema);
    }

    @Test
    public void testEncodeSchemaWithNullableRecordVersion() {
        // given
        DataEncoderImpl encoder = new DataEncoderImpl(entityKeyDefSchema, partitionKeyDefSchema,
                timeKeyDefSchema, recordVersionKeyDefSchemaNullable, timeConverter);
        ImmutableData data = createTestCmwData(1);
        DataBuilder builder = ImmutableData.builder();
        builder.addAll(data.getEntries());
        builder.add(ImmutableEntry.ofNull("record_version1", STRING));

        // when
        String recordSchemaStr = encoder.encodeRecordFieldDefinitions(data);
        assertNotNull(recordSchemaStr);
        Schema recordSchema = new Schema.Parser().parse(recordSchemaStr);

        // then
        assertTrue(checkType(recordSchema, "boolField", "boolean"));
        assertTrue(checkType(recordSchema, "longField1", "long"));
        assertTrue(checkType(recordSchema, "shortField1", "int")); // short maps to int in Avro
        assertTrue(checkType(recordSchema, "doubleField", "double"));

        assertTrue(checkType(recordSchema, "boolField", "boolean"));
        assertTrue(checkType(recordSchema, "longField1", "long"));
        assertTrue(checkType(recordSchema, "shortField1", "int")); // short maps to int in Avro
        assertTrue(checkType(recordSchema, "doubleField", "double"));

        //check for all data fields
        verifyFields(data, recordSchema);
    }

    @Test
    public void testEncodeSchemaWithMissingRecordVersionField() {
        // given
        DataEncoderImpl encoder = new DataEncoderImpl(entityKeyDefSchema, partitionKeyDefSchema,
                timeKeyDefSchema, recordVersionKeyDefSchemaNullable, timeConverter);
        ImmutableData data = createTestCmwData(null);

        // when
        assertThrows(IllegalArgumentException.class, () -> encoder.encodeRecordFieldDefinitions(data));

    }

    @Test
    public void testEncodeSchemaWithRecordVersion() {
        // given
        DataEncoderImpl encoder = new DataEncoderImpl(entityKeyDefSchema, partitionKeyDefSchema,
                timeKeyDefSchema, recordVersionKeyDefSchemaNullable, timeConverter);
        ImmutableData data = createTestCmwData(20);

        // when
        String recordSchemaStr = encoder.encodeRecordFieldDefinitions(data);
        assertNotNull(recordSchemaStr);
        Schema recordSchema = new Schema.Parser().parse(recordSchemaStr);

        // then
        assertTrue(checkType(recordSchema, "boolField", "boolean"));
        assertTrue(checkType(recordSchema, "longField1", "long"));
        assertTrue(checkType(recordSchema, "shortField1", "int")); // short maps to int in Avro
        assertTrue(checkType(recordSchema, "doubleField", "double"));
        assertTrue(checkType(recordSchema, "record_version1", "string"));
        assertTrue(checkType(recordSchema, "record_version2", "int"));

        //check for all data fields
        verifyFields(data, recordSchema);

    }

    @Test
    public void testUnsupportedMultiArrayOfDifferentData() {
        // given
        DataEncoderImpl encoder = new DataEncoderImpl(entityKeyDefSchema, partitionKeyDefSchema,
                timeKeyDefSchema, recordVersionKeyDefSchemaNullable, timeConverter);
        DataBuilder builder1 = ImmutableData.builder();
        builder1.add("field1", true);
        DataBuilder builder2 = ImmutableData.builder();
        builder2.add("field2", 1);

        DataBuilder builder = ImmutableData.builder();
        builder.add("multiarrayOfData",
                new ImmutableData[] { builder1.build(), builder2.build(), builder1.build(), builder2.build() }, 2, 2);

        assertThrows(IllegalArgumentException.class, () -> encoder.encodeRecordFieldDefinitions(builder.build()));
        // then
        // exception

    }

    @Test
    public void testEncodeSchemaWithNoRecordVersionSchema() {
        // given
        DataEncoderImpl encoder = new DataEncoderImpl(entityKeyDefSchema, partitionKeyDefSchema,
                timeKeyDefSchema, null, timeConverter);
        ImmutableData data = createTestCmwData(null);

        // when
        String recordSchemaStr = encoder.encodeRecordFieldDefinitions(data);
        assertNotNull(recordSchemaStr);
        Schema recordSchema = new Schema.Parser().parse(recordSchemaStr);

        // then
        assertTrue(checkType(recordSchema, "boolField", "boolean"));
        assertTrue(checkType(recordSchema, "longField1", "long"));
        assertTrue(checkType(recordSchema, "shortField1", "int")); // short maps to int in Avro
        assertTrue(checkType(recordSchema, "doubleField", "double"));
        //check for all data fields
        verifyFields(data, recordSchema);

    }

    @Test
    public void testEntityIdAndPartitionIdPassedFromRecord() {
        // given
        DataEncoderImpl encoder = new DataEncoderImpl(entityKeyDefSchema, partitionKeyDefSchema,
                timeKeyDefSchema, null, timeConverter);
        ImmutableData data = createTestCmwData(null, 1L, 2L);

        // when
        String recordSchemaStr = encoder.encodeRecordFieldDefinitions(data);
        assertNotNull(recordSchemaStr);
        Schema recordSchema = new Schema.Parser().parse(recordSchemaStr);

        // then
        assertEquals(ENTITY_ID.getSchema(), recordSchema.getField(ENTITY_ID.getFieldName()).schema());
        assertEquals(PARTITION_ID.getSchema(), recordSchema.getField(PARTITION_ID.getFieldName()).schema());
        //check for all data fields
        verifyFields(data, recordSchema);

    }

    @Test
    public void testNoFieldsInNestedRecord() {
        // given
        DataEncoderImpl encoder = new DataEncoderImpl(entityKeyDefSchema, partitionKeyDefSchema,
                timeKeyDefSchema, null, timeConverter);
        ImmutableData data = createTestCmwData(null, null, null, false);

        // when
        assertThrows(IllegalArgumentException.class, () -> encoder.encodeRecordFieldDefinitions(data));
    }

    @Test
    public void testUnsupportedArrayOfDifferentData() {
        // given
        DataEncoderImpl encoder = new DataEncoderImpl(entityKeyDefSchema, partitionKeyDefSchema,
                timeKeyDefSchema, recordVersionKeyDefSchemaNullable, timeConverter);
        DataBuilder builder1 = ImmutableData.builder();
        builder1.add("field1", true);
        DataBuilder builder2 = ImmutableData.builder();
        builder2.add("field2", 1);

        DataBuilder builder = ImmutableData.builder();
        builder.add("arrayOfData",
                new ImmutableData[] { builder1.build(), builder2.build(), builder1.build(), builder2.build() });

        // when
        assertThrows(IllegalArgumentException.class, () -> encoder.encodeRecordFieldDefinitions(builder.build()));

        // then
        // exception

    }

    @Test
    public void testUnsupportedArray2DOfDifferentData() {
        // given
        DataEncoderImpl encoder = new DataEncoderImpl(entityKeyDefSchema, partitionKeyDefSchema,
                timeKeyDefSchema, recordVersionKeyDefSchemaNullable, timeConverter);
        DataBuilder builder1 = ImmutableData.builder();
        builder1.add("field1", true);
        DataBuilder builder2 = ImmutableData.builder();
        builder2.add("field2", 1);

        DataBuilder builder = ImmutableData.builder();
        builder.add("arrayOfData",
                new ImmutableData[] { builder1.build(), builder2.build(), builder1.build(), builder2.build() }, 1, 2);
        // when
        assertThrows(IllegalArgumentException.class, () -> encoder.encodeRecordFieldDefinitions(builder.build()));

        // then
        // exception

    }

    @Test
    public void testSerializeNoSystemFields() {
        DataEncoderImpl serializer = new DataEncoderImpl(
                entityKeyDefSchema, partitionKeyDefSchema,
                timeKeyDefSchema, recordVersionKeyDefSchema, timeConverter);
        DataBuilder builder = ImmutableData.builder();
        builder.add("field", 1);
        assertThrows(RuntimeException.class, () -> serializer.encodeEntityKeyValues(builder.build()));
    }

    @Test
    public void testEncodeTimeValue() {
        // given
        DataEncoderImpl serializer = new DataEncoderImpl(
                entityKeyDefSchema, partitionKeyDefSchema,
                timeKeyDefSchema, recordVersionKeyDefSchemaNullable, timeConverter);
        ImmutableData data = createTestCmwData(10);
        // when
        Long timeKeyValue = serializer.encodeTimeKeyValues(data);

        // then
        assertNotNull(timeKeyValue);
        assertEquals(123456789L, timeKeyValue, 0);
    }

    private ImmutableData createTestCmwData(Integer recordVersion) {
        return createTestCmwData(recordVersion, null, null, true);
    }

    private ImmutableData createTestCmwData(Integer recordVersion, Long entityId, Long partitionId) {
        return createTestCmwData(recordVersion, entityId, partitionId, true);
    }

    private ImmutableData createTestCmwData(Integer recordVersion, Long entityId, Long partitionId, boolean addFields) {

        DataBuilder builder = ImmutableData.builder();

        if (entityId != null) {
            builder.add(SystemFields.NXC_ENTITY_ID.getValue(), entityId);
        }

        if (partitionId != null) {
            builder.add(SystemFields.NXC_PARTITION_ID.getValue(), entityId);
        }

        builder.add("device", "dev1");
        builder.add("property", "prop1");
        builder.add("class", "class1");
        builder.add("_p_t_", 123456789L);

        builder.add("boolField", true);
        builder.add("byteField1", (byte) -2);
        builder.add("shortField1", (short) -1);
        builder.add("intField1", 1);
        builder.add("longField1", 1L);
        builder.add("longField2", 1L);
        builder.add("floatField1", 1.0f);
        builder.add("doubleField", 1.0d);
        builder.add("stringField1", "string value");

        builder.add("boolArrayField", new boolean[] { true, false, true });
        builder.add("byteArrayField", new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
        builder.add("shortArrayField", new short[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
        builder.add("intArrayField", new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
        builder.add("longArrayField", new long[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
        builder.add("floatArrayField", new float[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
        builder.add("doubleArrayField", new double[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
        builder.add("stingArrayField", new String[] { "aaa", "bbb" });

        builder.add("boolArray2DField1", new boolean[] { true, false }, 1, 2);
        builder.add("byteArray2DField2", new byte[] { 1, 2 }, 1, 2);
        builder.add("shortArray2DField2", new short[] { 1, 2 }, 1, 2);
        builder.add("intArray2DField2", new int[] { 1, 2 }, 1, 2);
        builder.add("longArray2DField2", new long[] { 1, 2 }, 1, 2);
        builder.add("doubleArray2DField2", new double[] { 1, 2 }, 1, 2);
        builder.add("floatArray2DField2", new float[] { 1, 2 }, 1, 2);
        builder.add("stringArray2DField2", new String[] { "aaaa", "bbbb" }, 1, 2);

        builder.add("boolMultiArrayField1", new boolean[] { true, false, true, false }, 2, 2);
        builder.add("byteMultiArrayField1", new byte[] { 1, 2, 3, 4 }, 2, 2);
        builder.add("shortMultiArrayField1", new short[] { 1, 2, 3, 4 }, 2, 2);
        builder.add("intMultiArrayField1", new int[] { 1, 2, 3, 4 }, 2, 2);
        builder.add("longMultiArrayField1", new long[] { 1, 2, 3, 4 }, 2, 2);
        builder.add("doubleMultiArrayField1", new double[] { 1, 2, 3, 4 }, 2, 2);
        builder.add("floatMultiArrayField1", new float[] { 1, 2, 3, 4 }, 2, 2);
        builder.add("stringMultiArrayField1", new String[] { "aaaa", "bbbb", "cccc", "dddd" }, 2, 2);
        builder.add("stringMultiArrayField2", new String[] { "aaaa", "bbbb", "cccc", "dddd" }, 2, 2);

        builder.add("ddfField1",
                DataFactory.createDiscreteFunction(new double[] { 1.0, 2.0 }, new double[] { 1.0, 2.0 }));
        builder.add("ddfField2",
                DataFactory.createDiscreteFunction(new double[] { 1.0, 2.0 }, new double[] { 1.0, 2.0 }));

        builder.add("ddfArrayField1", new DiscreteFunction[] {
                DataFactory.createDiscreteFunction(new double[] { 1.0, 2.0 }, new double[] { 1.0, 2.0 }),
                DataFactory.createDiscreteFunction(new double[] { 1.0, 2.0, 3.0 }, new double[] { 1.0, 2.0, 3.0 }) });

        builder.add("ddfArrayField2", new DiscreteFunction[] {
                DataFactory.createDiscreteFunction(new double[] { 1.0, 2.0 }, new double[] { 1.0, 2.0 }),
                DataFactory.createDiscreteFunction(new double[] { 1.0, 2.0, 3.0 }, new double[] { 1.0, 2.0, 3.0 }) });

        builder.add("ddfMultiArrayField", new DiscreteFunction[] {
                        DataFactory.createDiscreteFunction(new double[] { 1.0, 2.0 }, new double[] { 1.0, 2.0 }),
                        DataFactory.createDiscreteFunction(new double[] { 1.0, 2.0, 3.0 }, new double[] { 1.0, 2.0, 3.0 }),
                        DataFactory.createDiscreteFunction(new double[] { 1.1, 2.1, 3.1 }, new double[] { 1.2, 2.2, 3.2 }) }, 1,
                1, 1);

        DataBuilder secondNestedBuilder = ImmutableData.builder();
        secondNestedBuilder.add("longField1", 1L);
        secondNestedBuilder.add("discreteFunctionField",
                DataFactory.createDiscreteFunction(new double[] { 1.0, 2.0 }, new double[] { 1.0, 2.0 }));
        secondNestedBuilder.add("multiArrayLong", new short[] { 1, 2, 3, 4 }, 2, 2);

        DataBuilder nestedBuilder = ImmutableData.builder();
        if (addFields) {
            nestedBuilder.add("boolField", true);
            nestedBuilder.add("longField", 1L);
            // here nothing happens, the record version is ignored.
            nestedBuilder.add("record_version1", "ver1");
            nestedBuilder.add("record_version2", 12);
            nestedBuilder.add("boolArray2DField2", new boolean[] { true, false }, 1, 2);
            nestedBuilder.add("byteArrayField", new byte[] { 1, 2, 3, 4 });
            nestedBuilder.add("dataField", secondNestedBuilder.build());
        }
        builder.add("dataField", nestedBuilder.build());

        if (recordVersion != null) {
            builder.add("record_version1", "ver1");
            builder.add("record_version2", recordVersion);
        }
        return builder.build();
    }

    private static GenericRecord deserialize(byte[] bytes, EntitySchema schemaData) {
        DefaultBytesToGenericRecordDecoder decoder = new DefaultBytesToGenericRecordDecoder(schemaId -> schemaData);
        return decoder.apply(schemaData.getId(), bytes);
    }
}
