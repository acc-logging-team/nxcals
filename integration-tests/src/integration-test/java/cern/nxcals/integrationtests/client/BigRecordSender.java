package cern.nxcals.integrationtests.client;

import cern.cmw.datax.DataBuilder;
import cern.cmw.datax.ImmutableData;
import cern.nxcals.api.ingestion.Publisher;
import cern.nxcals.api.ingestion.PublisherFactory;
import cern.nxcals.api.ingestion.Result;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

@Slf4j
public class BigRecordSender {
    static {
        System.setProperty("org.apache.logging.log4j.simplelog.StatusLogger.level", "TRACE");
        System.setProperty("logging.config", "classpath:log4j2.yml");

        String user = System.getProperty("user.name");
        System.setProperty("service.url", "https://nxcals-" + user + "1.cern.ch:19093,"
                + "https://nxcals-" + user + "2.cern.ch:19093,"
                + "https://nxcals-" + user + "3.cern.ch:19093");
        System.setProperty("kafka.producer.bootstrap.servers", "https://nxcals-" + user + "3.cern.ch:9092,"
                + "https://nxcals-" + user + "4.cern.ch:9092");

        System.setProperty("kafka.producer.batch.size", "500000000");
        System.setProperty("kafka.producer.max.request.size", "600000000");
        System.setProperty("kafka.producer.buffer.memory", "30000000");
        System.setProperty("kafka.producer.batch.size", "1638400");

    }

    private final int[] sizes = { 1209, 3474, 9683, 879, 5052, 6248, 5790, 7816, 5647, 4121, 7836, 3254, 5636, 9930,
            2214,
            1022, 9023, 7197, 7128, 3260, 1903, 4510, 899, 674, 5768, 4972, 911, 1121, 8544, 4828, 9841, 723, 3944,
            4584, 5550, 4677, 3962, 3752, 6059, 6537, 1404, 8040, 3177, 7565, 6894, 599, 235, 6142, 1945, 3213, 8066,
            9200, 898, 3168, 7427, 8663, 7653, 80, 2680, 1569, 1584, 6603, 6850, 6590, 8062, 2368, 2048, 2989, 6241,
            7994, 5488, 8467, 4434, 2814, 5125, 6896, 2712, 50, 4682, 2452, 5841, 9296, 3838, 7668, 6173, 3268, 5543,
            5859, 3171, 2945, 4571, 4605, 6591, 3217, 8200, 9075, 1370, 8490, 1144, 2468, 787, 3073, 8579, 6078, 7051,
            195, 1042, 2815, 530, 5151, 5277, 2574, 6316, 8234, 5853, 4245, 7012, 338, 6774, 2093, 5038, 296, 8083,
            8461, 3144, 1190, 5391, 9382, 6905, 7940, 10, 8659, 7343, 6156, 2389, 3922, 962, 8493, 1621, 8817, 8757,
            6264, 8009, 6198, 2730, 3624, 3097, 6051, 7004, 3276, 5043, 1256, 2826, 8574, 2624, 1605, 4346, 8892, 9720,
            5047, 1182, 7061, 7323, 6012, 343, 5896, 2715, 9834, 90, 8786, 9578, 1764, 2829, 6206, 8554, 6824, 7410,
            2455, 9795, 8801, 9388, 7018, 9571, 8612, 9737, 875, 5714, 4733, 9663, 9595, 6248, 9179, 9633, 9306, 6616,
            4468, 8065, 4138, 1658, 8981, 29272, 92585, 70401, 30573, 91670, 89306, 76449, 84603, 81571, 83670, 28398,
            43834, 19186, 33117, 17204, 38200, 33341, 79089, 59830, 44439, 342323, 516292, 261163, 648315, 942322 };

    private final List<Publisher<ImmutableData>> publishers = new ArrayList<>();
    private final RandomByteDataGenerator dataGenerator = new RandomByteDataGenerator();
    private final ArrayBlockingQueue<Runnable> queue = new ArrayBlockingQueue<>(100);

    private ExecutorService createExecutor() {
        ThreadFactoryBuilder threadFactoryBuilder = new ThreadFactoryBuilder();
        threadFactoryBuilder.setNameFormat("NXCALS-Ingestion-Executor-%d");
        return new ThreadPoolExecutor(1, 10, 1L, TimeUnit.MINUTES, queue,
                threadFactoryBuilder.build());
    }

    private ImmutableData getImmutableData(int messageSize, int dimension) {
        DataBuilder builder = ImmutableData.builder();
        builder.add("device", "bigRecordDevice");
        builder.add("property", "bigRecordProperty");
        builder.add("class", "bigRecordClass");
        builder.add("__record_version__", 0L);
        builder.add("__record_timestamp__", System.currentTimeMillis() * 1000_000);

        if (dimension == 1) {
            builder.add("data", dataGenerator.generateOneDimensionData(messageSize), 1);
        } else {
            builder.add("data", dataGenerator.generateTwoDimensionData(messageSize), 2);
        }

        return builder.build();
    }

    public static void main(String[] args) {
        BigRecordSender bigRecordSender = new BigRecordSender();
        ExecutorService executorService = bigRecordSender.createExecutor();
        bigRecordSender.publishers.add(PublisherFactory.newInstance().createPublisher("TEST-CMW", Function.identity()));

        ScheduledExecutorService publishingExecutorService = Executors.newSingleThreadScheduledExecutor();
        publishingExecutorService.scheduleWithFixedDelay(() -> {
            ImmutableData data = bigRecordSender.getImmutableData(
                    bigRecordSender.sizes[ThreadLocalRandom.current().nextInt(bigRecordSender.sizes.length)],
                    1);
            try {
                CompletableFuture<Result> future = bigRecordSender.publishers
                        .get(ThreadLocalRandom.current().nextInt(bigRecordSender.publishers.size()))
                        .publishAsync(data, executorService);
                future.whenComplete((Result n, Throwable ex) -> {
                    if (ex == null) {
                        log.info("Record sent");
                    } else {
                        log.error("Error sending record", ex);
                    }
                });
            } catch (Exception ex) {
                log.error("Cannot publish record", ex);
            }
            log.info("Record added for sending, queue size={}", bigRecordSender.queue.size());
        }, 0, 500, TimeUnit.MILLISECONDS);

    }
}
