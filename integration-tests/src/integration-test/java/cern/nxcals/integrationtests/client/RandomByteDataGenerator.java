package cern.nxcals.integrationtests.client;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by wjurasz on 30.03.17.
 */
public class RandomByteDataGenerator {
    private Map<Integer, byte[]> oneDimensionRandomData;
    private Map<Integer, byte[][]> twoDimensionRandomData;

    public RandomByteDataGenerator() {
        oneDimensionRandomData = new HashMap<>();
        twoDimensionRandomData = new HashMap<>();
    }

    public byte[] generateOneDimensionData(int dataSize) {
        byte[] bytes = oneDimensionRandomData.get(dataSize);
        if (bytes == null) {
            bytes = new byte[dataSize];
            ThreadLocalRandom.current().nextBytes(bytes);
            oneDimensionRandomData.put(dataSize, bytes);
        }
        return bytes;
    }

    public byte[][] generateTwoDimensionData(int dataSize) {
        byte[][] bytes = twoDimensionRandomData.get(dataSize);
        if (bytes == null) {
            int dim = Math.toIntExact(Math.round(Math.sqrt(dataSize)));
            bytes = new byte[dim][dim];
            for (int i = 0; i < dim; i++) {
                ThreadLocalRandom.current().nextBytes(bytes[i]);
            }
            twoDimensionRandomData.put(dataSize, bytes);
        }
        return bytes;
    }
}
