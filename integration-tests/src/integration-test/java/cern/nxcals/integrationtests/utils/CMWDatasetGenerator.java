package cern.nxcals.integrationtests.utils;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.utils.TimeUtils;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import scala.collection.JavaConverters;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

import static cern.nxcals.common.Schemas.ENTITY_ID;
import static cern.nxcals.common.Schemas.PARTITION_ID;
import static cern.nxcals.common.Schemas.SCHEMA_ID;
import static cern.nxcals.common.Schemas.SYSTEM_ID;
import static cern.nxcals.common.Schemas.TIMESTAMP;
import static cern.nxcals.integrationtests.service.AbstractTest.getRandomName;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.CLASS_KEY;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.DEVICE_KEY;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.PROPERTY_KEY;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.RECORD_TIMESTAMP_KEY;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.RECORD_VERSION_KEY;

@RequiredArgsConstructor
public class CMWDatasetGenerator extends CMWDataGenerator {
    // These fields must be present in dataset
    private static final List<StructField> obligatoryFieldsInCMWEntity = List.of(
            DataTypes.createStructField(SYSTEM_ID.getFieldName(), DataTypes.LongType, true),
            DataTypes.createStructField(ENTITY_ID.getFieldName(), DataTypes.LongType, true),
            DataTypes.createStructField(PARTITION_ID.getFieldName(), DataTypes.LongType, true),
            DataTypes.createStructField(SCHEMA_ID.getFieldName(), DataTypes.LongType, true),
            DataTypes.createStructField(TIMESTAMP.getFieldName(), DataTypes.LongType, true),
            DataTypes.createStructField(RECORD_VERSION_KEY, DataTypes.LongType, true),
            DataTypes.createStructField(RECORD_TIMESTAMP_KEY, DataTypes.LongType, true),
            DataTypes.createStructField(DEVICE_KEY, DataTypes.StringType, true),
            DataTypes.createStructField(PROPERTY_KEY, DataTypes.StringType, true),
            DataTypes.createStructField(CLASS_KEY, DataTypes.StringType, true));
    private final Function<Long, Row> rowGenerator;
    private final List<StructField> userFieldsSchema;

    private final String deviceName = getRandomName("device_");
    private final String propertyName = getRandomName("property_");
    private final String className = getRandomName("class_");
    @Getter
    private final Map<String, Object> partitionKeyValues = Map.of(CLASS_KEY, className, PROPERTY_KEY, propertyName);
    @Getter
    private final Map<String, Object> entityKeyValues = Map.of(DEVICE_KEY, deviceName, PROPERTY_KEY, propertyName);
    @Getter
    Map<String, Object> partitionKeys = Map.of(CLASS_KEY, className, PROPERTY_KEY, propertyName);
    @Getter
    private final String systemName = "CMW";
    @Getter
    private final Instant dataGenerationStart = Instant.now().minus(3, ChronoUnit.DAYS).truncatedTo(ChronoUnit.DAYS);
    @Getter
    private final long numberOfPoints = 10;

    public StructType getSchema() {
        List<StructField> fields = new ArrayList<>();
        fields.addAll(obligatoryFieldsInCMWEntity);
        fields.addAll(userFieldsSchema);
        return DataTypes.createStructType(fields);
    }

    public Dataset<Row> createDataset(Entity entity, SparkSession sparkSession) {
        StructType datasetSchema = getSchema();
        Function<Long, Row> generator =
                datapointNumber -> createRow(entity, dataGenerationStart, datapointNumber);
        return sparkSession.createDataFrame(
                LongStream.range(0, numberOfPoints).boxed().map(generator).collect(Collectors.toList()),
                datasetSchema);
    }

    private Row createRow(Entity entity, Instant dataStartingPoint, long pointNumber) {
        List<Object> rowValues = new ArrayList<>();
        // Add all obligatory fields
        rowValues.addAll(List.of(
                entity.getSystemSpec().getId(),
                entity.getId(),
                entity.getPartition().getId(),
                entity.getFirstEntityHistory().getEntitySchema().getId(),
                TimeUtils.getNanosFromInstant(dataStartingPoint.plusSeconds(pointNumber)),
                0L,
                TimeUtils.getNanosFromInstant(dataStartingPoint.plusSeconds(pointNumber)),
                deviceName,
                propertyName,
                className
        ));
        // Add user fields
        rowValues.addAll(JavaConverters.asJavaCollection(rowGenerator.apply(pointNumber).toSeq()));
        return Row.fromSeq(JavaConverters.asScalaBuffer(rowValues));
    }

    public Instant getDataGenerationEnd() {
        return dataGenerationStart.plusSeconds(numberOfPoints);
    }

    public TimeWindow getQueryTimeWindow() {
        return TimeWindow.between(getDataGenerationStart(), getDataGenerationEnd());
    }
}
