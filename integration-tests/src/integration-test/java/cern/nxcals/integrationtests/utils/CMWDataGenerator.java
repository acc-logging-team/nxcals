package cern.nxcals.integrationtests.utils;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.TimeWindow;

import java.util.Map;

import static cern.nxcals.integrationtests.utils.ServicesHelper.findEntityOrThrow;

public abstract class CMWDataGenerator {
    protected abstract Map<String, Object> getEntityKeyValues();

    protected abstract String getSystemName();

    /**
     * Be careful with usage, can be used after sending data, what will cause creating entity, before will throw
     * RuntimeException
     *
     * @return entity (if exist) which was created basing on generated data
     */

    public Entity getCreatedEntity() {
        return findEntityOrThrow(getSystemName(), getEntityKeyValues());
    }

    public abstract long getNumberOfPoints();

    public abstract TimeWindow getQueryTimeWindow();
}
