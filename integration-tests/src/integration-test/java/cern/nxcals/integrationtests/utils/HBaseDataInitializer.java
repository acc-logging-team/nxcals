package cern.nxcals.integrationtests.utils;

import cern.cmw.datax.ImmutableData;
import cern.nxcals.api.ingestion.Publisher;
import cern.nxcals.api.ingestion.PublisherFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.sql.SparkSession;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.fail;

@Slf4j
@RequiredArgsConstructor
public class HBaseDataInitializer extends DataInitializer {

    private static final int MAX_WAIT_FOR_DATA_MINUTES = 2;

    private final CMWTestDataGenerator generator;

    private final AtomicBoolean dataVisible = new AtomicBoolean(false);
    private final AtomicBoolean waitingForDataFailed = new AtomicBoolean(false);

    public void setUpData() {
        doOnce(this::insertData);
    }

    protected void insertData() {
        PublisherFactory publisherFactory = PublisherFactory.newInstance();
        List<ImmutableData> dataToPublish = generator.generateData();
        log.info("Sending fills and beam mode data to NXCALS");
        try (Publisher<ImmutableData> publisher = publisherFactory
                .createPublisher(generator.getSystemName(), Function.identity())) {
            for (ImmutableData data : dataToPublish) {
                publisher.publish(data);
            }
        } catch (Exception e) {
            log.error("Error sending data to NXCALS", e);
            fail("Error sending data to NXCALS! Cause:\n" + e.getCause());
        }
    }

    public void waitForData(SparkSession sparkSession) throws InterruptedException {
        if (!dataVisible.get()) {
            //Have to wait 30 seconds in order for the data to be processed and visible in NXCALS extraction
            log.info("Waiting max {} minutes for the data to be visible in NXCALS...", MAX_WAIT_FOR_DATA_MINUTES);
            waitForData(TimeUnit.MINUTES.toMillis(MAX_WAIT_FOR_DATA_MINUTES), sparkSession);
            log.info("Ok, data should be processed now, proceeding with tests");
            dataVisible.set(true);
        } else {
            log.info("Data already checked");
        }
    }

    private synchronized void waitForData(long maxTime, SparkSession sparkSession) throws InterruptedException {
        long start = System.currentTimeMillis();
        while (true) {
            if (waitingForDataFailed.get() || (System.currentTimeMillis() - start > maxTime)) {
                waitingForDataFailed.set(true);
                fail("Data was not processed in the required maxTime=" + maxTime + " ms");
            }
            if (generator.isDatasetReady(sparkSession)) {
                return;
            }
            log.info("Still no data visible, waiting more...");
            TimeUnit.SECONDS.sleep(4);
        }
    }
}
