package cern.nxcals.integrationtests.backport;

import cern.cmw.datax.DataBuilder;
import cern.cmw.datax.ImmutableData;
import cern.cmw.datax.ImmutableEntry;
import cern.nxcals.api.backport.client.service.MetaDataService;
import cern.nxcals.api.backport.client.service.QuerySnapshotDataService;
import cern.nxcals.api.backport.client.service.ServiceBuilder;
import cern.nxcals.api.backport.client.service.TimeseriesDataService;
import cern.nxcals.api.backport.domain.core.constants.FilterOperand;
import cern.nxcals.api.backport.domain.core.constants.LoggingTimeInterval;
import cern.nxcals.api.backport.domain.core.constants.ScaleAlgorithm;
import cern.nxcals.api.backport.domain.core.constants.TimescalingProperties;
import cern.nxcals.api.backport.domain.core.constants.VariableDataType;
import cern.nxcals.api.backport.domain.core.constants.VectorRestriction;
import cern.nxcals.api.backport.domain.core.metadata.FundamentalData;
import cern.nxcals.api.backport.domain.core.metadata.VariableSet;
import cern.nxcals.api.backport.domain.core.metadata.VectornumericElements;
import cern.nxcals.api.backport.domain.core.metadata.VectornumericElementsSet;
import cern.nxcals.api.backport.domain.core.snapshot.Snapshot;
import cern.nxcals.api.backport.domain.core.snapshot.SnapshotCriteria;
import cern.nxcals.api.backport.domain.core.timeseriesdata.SparkTimeseriesData;
import cern.nxcals.api.backport.domain.core.timeseriesdata.SparkTimeseriesDataSet;
import cern.nxcals.api.backport.domain.core.timeseriesdata.TimeseriesData;
import cern.nxcals.api.backport.domain.core.timeseriesdata.TimeseriesDataSet;
import cern.nxcals.api.backport.domain.core.timeseriesdata.VariableStatistics;
import cern.nxcals.api.backport.domain.core.timeseriesdata.VariableStatisticsSet;
import cern.nxcals.api.backport.domain.core.timeseriesdata.filter.value.Filter;
import cern.nxcals.api.backport.domain.core.timeseriesdata.filter.value.FilterFactory;
import cern.nxcals.api.backport.domain.core.timeseriesdata.filter.value.Filters;
import cern.nxcals.api.backport.domain.util.FundamentalUtils;
import cern.nxcals.api.config.SparkContext;
import cern.nxcals.api.custom.domain.GroupType;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.Group;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.api.domain.VariableDeclaredType;
import cern.nxcals.api.domain.Visibility;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.extraction.metadata.EntityService;
import cern.nxcals.api.extraction.metadata.GroupService;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.extraction.metadata.queries.Entities;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import cern.nxcals.api.ingestion.Publisher;
import cern.nxcals.api.ingestion.PublisherFactory;
import cern.nxcals.api.utils.TimeUtils;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.DependsOn;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.IntStream;

import static cern.nxcals.api.backport.domain.core.constants.ScaleAlgorithm.AVG;
import static cern.nxcals.api.backport.domain.core.constants.ScaleAlgorithm.COUNT;
import static cern.nxcals.api.backport.domain.core.constants.ScaleAlgorithm.MAX;
import static cern.nxcals.api.backport.domain.core.constants.ScaleAlgorithm.MIN;
import static cern.nxcals.api.backport.domain.core.constants.ScaleAlgorithm.SUM;
import static cern.nxcals.api.custom.domain.CernSystemConstants.CERN_SYSTEM;
import static cern.nxcals.api.custom.domain.GroupPropertyName.getSelectedVariables;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VALUE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = SparkContext.class)
@SpringBootApplication
@Slf4j
@DependsOn("kerberos")
public class BackportTimeseriesDataServiceTest {

    private static final String USER_NAME = "acclog";

    static {
        //Please leave this one just in case you want to run those test from IDE for debugging
        //Don't forget to check if hadoop-dev jar is included in build.gradle!!!

        //        System.setProperty("service.url", "https://nxcals-<user>1.cern.ch:19093".replace("<user>",USER_NAME)); //Do not use localhost here.
        //        System.setProperty("kafka.producer.bootstrap.servers", "https://nxcals-<user>3.cern.ch:9092,https://nxcals-<user>4.cern.ch:9092,https://nxcals-<user>5.cern.ch:9092".replace("<user>", USER_NAME));
        //        System.setProperty("kerberos.principal", USER_NAME);
        //        System.setProperty("kerberos.keytab", "/opt/<user>/.keytab".replace("<user>", USER_NAME));
    }

    private static final Instant FROM = Instant.now();
    private static final String RANDOM_DEVICE = UUID.randomUUID().toString();
    private static final String RANDOM_ACCELERATOR = UUID.randomUUID().toString();
    private static final String FUNDAMENTAL_VARIABLE_NAME = RANDOM_ACCELERATOR + FundamentalData.FUNDAMENTAL_SEPARATOR
            + FundamentalData.FUNDAMENTAL_SUFFIX;
    private static final String VECTORNUMERIC_VARIABLE_NAME = UUID.randomUUID().toString() + "-vectornumeric";
    private static final String CONTEXT_VARIABLE_NAME_PREFIX = "NXCALS_CONTEXT:";
    private static final String RANDOM_VARIABLE = UUID.randomUUID().toString();
    private static final String DEVICE1 = "test_device_1_" + RANDOM_DEVICE;
    private static final String DEVICE2 = "test_device_2_" + RANDOM_DEVICE;
    private static final String DEVICE3 = "test_device_3_" + RANDOM_DEVICE;

    private static final String RANDOM_STRING = UUID.randomUUID().toString();
    private static final String PROPERTY_PREFIX = "test_property_";
    private static final String PROPERTY1 = PROPERTY_PREFIX + RANDOM_STRING;
    private static final String PROPERTY2 = "test1property2" + RANDOM_STRING;
    private static final String RANDOM_PROPERTY3 = UUID.randomUUID().toString();
    private static final String PROPERTY3 = "test1property3" + RANDOM_PROPERTY3;

    private static final String RANDOM_PROPERTY4 = UUID.randomUUID().toString();
    private static final String PROPERTY4 = PROPERTY_PREFIX + RANDOM_PROPERTY4;
    private static final String PROPERTY4_VALUE_FIELD = "value";

    private static final String PROPERTY5 = "test1property5" + RANDOM_STRING;
    private static final String PROPERTY6 = "test1property6" + RANDOM_STRING;
    private static final String PROPERTY7 = "test1property7" + RANDOM_STRING;

    private static final String RANDOM_PROPERTY8 = UUID.randomUUID().toString();
    private static final String PROPERTY8 = PROPERTY_PREFIX + RANDOM_PROPERTY8;
    private static final String PROPERTY8_VALUE_FIELD = "vectnumValue";

    private static final String RANDOM_PROPERTY9 = UUID.randomUUID().toString();
    private static final String PROPERTY9 = PROPERTY_PREFIX + RANDOM_PROPERTY9;
    private static final String PROPERTY9_VALUE_FIELD = "contextValue";
    private static final String RANDOM_PROPERTY10 = UUID.randomUUID().toString();
    private static final String PROPERTY10 = PROPERTY_PREFIX + RANDOM_PROPERTY10;
    private static final String SYSTEM = "CMW";
    private static final String CLASS1 = "test_class";
    private static final String DEVICE_KEY = "device";
    private static final String PROPERTY_KEY = "property";
    private static final String CLASS_KEY = "class";
    private static final String RECORD_VERSION_KEY = "__record_version__";
    private static final String RECORD_TIMESTAMP_KEY = "__record_timestamp__";
    private static final String VARIABLE1 = RANDOM_VARIABLE + "-var1";
    private static final String VARIABLE2 = RANDOM_VARIABLE + "-var2";

    private static final String VARIABLE5 = RANDOM_VARIABLE + "-var5";
    private static final String VARIABLE6 = RANDOM_VARIABLE + "-var6";
    private static final String VARIABLE7 = RANDOM_VARIABLE + "-var7";

    private static final String VARIABLE5intArray = RANDOM_VARIABLE + "-var5a";
    private static final String VARIABLE6intArray = RANDOM_VARIABLE + "-var6a";
    private static final String VARIABLE7intArray = RANDOM_VARIABLE + "-var7a";

    private static final String VARIABLE8boolArray = RANDOM_VARIABLE + "-var8a";
    private static final String VARIABLE9shortArray = RANDOM_VARIABLE + "-var9a";
    private static final String VARIABLE10doubleArray = RANDOM_VARIABLE + "-var10a";
    private static final String VARIABLE11stringArray = RANDOM_VARIABLE + "-var11a";
    private static final String VARIABLE12 = RANDOM_VARIABLE + "-var12";

    private static final String SNAPSHOT_NAME1 = "snapshot1-" + UUID.randomUUID().toString();
    private static final String SNAPSHOT_NAME2 = "snapshot2-" + UUID.randomUUID().toString();

    private static final int MAX_VALUES = 100;
    private static final int MAX_WAIT_TIME = 10;

    private static final Instant FIXED_INTERVALS_START = Instant.now();
    private static final Instant FIXED_INTERVALS_END = FIXED_INTERVALS_START.plus(3, ChronoUnit.MINUTES)
            .minusNanos(1);
    private static final long NR_OF_INTERVALS = 12L;
    private static final long NR_INTERVAL_DATAPOINTS = (NR_OF_INTERVALS + 1) * 3;
    private static final short INTERVAL_IN_SECONDS = 15;

    private static final long NR_CONTEXT_DATAPOINTS = 3;

    static {
        System.setProperty("kafka.producer.linger.ms", "0");
    }

    @Autowired
    private SparkSession sparkSession;
    private static boolean dataVisible = false;

    private TimeseriesDataService timeseriesService;
    private MetaDataService metaService;
    private QuerySnapshotDataService snapshotDataService;

    @BeforeEach
    public void setUp() throws InterruptedException {
        timeseriesService = ServiceBuilder.getInstance(sparkSession).createTimeseriesService();
        metaService = ServiceBuilder.getInstance(sparkSession).createMetaService();
        snapshotDataService = ServiceBuilder.getInstance(sparkSession).createQuerySnapshotDataService();
        waitForData();
    }

    @BeforeAll
    public static void setUpData() throws InterruptedException {
        log.info("Sending data to NXCALS");
        try (Publisher<ImmutableData> publisher = PublisherFactory.newInstance()
                .createPublisher(SYSTEM, Function.identity())) {

            for (int i = 0; i < MAX_VALUES; ++i) {
                long timestamp = TimeUtils.getNanosFromInstant(FROM.plus(i, ChronoUnit.MILLIS));
                //We send 100 each but the Fundamental variable is taking 50 from each (100 in total).
                publisher.publish(
                        createFundamentalDataType(i, PROPERTY1, timestamp, FundamentalData.TGM_LSA_CYCLE_FIELD,
                                "CYCLE" + (i % 10),
                                "LHC1", FundamentalData.DESTINATION_FIELD, "TT2_D3"));
                publisher.publish(
                        createFundamentalDataType(i, PROPERTY2, timestamp, FundamentalData.XTIM_LSA_CYCLE_FIELD,
                                "CYCLE" + (i % 10),
                                "LHC2", FundamentalData.XTIM_DESTINATION_FIELD, "TT2_D3"));
                publisher.publish(createDataType(i, PROPERTY3, timestamp));
                publisher.publish(createDataType(i + 5000, PROPERTY5, timestamp));
                publisher.publish(createDataType(i + 6000, PROPERTY6, timestamp));
                if (i % 2 == 0) {
                    publisher.publish(createDataType(i + 7000, PROPERTY7, timestamp));
                }
            }

            // Test datapoints in NXCALS will have 3 values located around each interval of 15 seconds:
            // 59s: 0.0    00s: 1.0    01s: 2.0
            // 14s: 3.0    15s: 4.0    16s: 5.0
            // 29s: 6.0    30s: 7.0    31s: 8.0
            // ...
            //
            // 14s: 27.0   15s: 28.0   16s: 29.0
            Map<String, List<ImmutableData>> dataPointsForAggregation = new HashMap<>();
            for (int i = 0; i <= NR_OF_INTERVALS; i++) {
                dataPointsForAggregation.computeIfAbsent(VARIABLE2, k -> new ArrayList<>())
                        .addAll(buildDatapointsForFixedIntervals(i, PROPERTY4));
                dataPointsForAggregation.computeIfAbsent(VARIABLE12, k -> new ArrayList<>())
                        .addAll(buildDatapointsForFixedIntervals(i, PROPERTY10));
            }
            dataPointsForAggregation.values().parallelStream()
                    .flatMap(Collection::stream).forEach(publisher::publish);

            for (ImmutableData dataPoint : buildDatapointsForContext()) {
                publisher.publish((dataPoint));
            }

        } catch (Exception e) {
            log.error("Error sending  values to NXCALS", e);
        }
        log.info("Sending finished!");

        createFundamentalVariable();
        createContextVariable(createVectornumVariableForContext());
        createVariables();
        createSnapshots();
    }

    private static void createSnapshots() {
        SystemSpecService systemSpecService = ServiceClientFactory.createSystemSpecService();

        SystemSpec systemSpec = systemSpecService.findByName(CERN_SYSTEM)
                .orElseThrow(() -> new IllegalArgumentException("No such system"));

        GroupService groupService = ServiceClientFactory.createGroupService();
        VariableService variableService = ServiceClientFactory.createVariableService();

        Variable var1 = variableService.findOne(Variables.suchThat().variableName().eq(VARIABLE1))
                .orElseThrow(() -> new IllegalArgumentException("No such variable " + VARIABLE1));

        Variable var2 = variableService.findOne(Variables.suchThat().variableName().eq(VARIABLE2))
                .orElseThrow(() -> new IllegalArgumentException("No such variable " + VARIABLE2));

        Group snapshot = Group.builder()
                .name(SNAPSHOT_NAME1)
                .label(GroupType.SNAPSHOT.toString())
                .visibility(Visibility.PUBLIC)
                .systemSpec(systemSpec)
                .property("getDynamicDuration", "1")
                .property("getPriorTime", "Now")
                .property("getTime", "DAYS")
                .property("getTimeZone", "LOCAL_TIME")
                .property("isEndTimeDynamic", "true")
                .build();
        Group group = groupService.create(snapshot);

        Map<String, Set<Long>> snapshotVariables = new HashMap<>();
        snapshotVariables.put(getSelectedVariables.name(), Sets.newHashSet(var1.getId(), var2.getId()));
        groupService.addVariables(group.getId(), snapshotVariables);

        log.info("Snapshot created {}", group);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.000").withZone(ZoneId.of("UTC"));
        Group snapshot2 = Group.builder()
                .name(SNAPSHOT_NAME2)
                .label(GroupType.SNAPSHOT.toString())
                .visibility(Visibility.PUBLIC)
                .systemSpec(systemSpec)
                .property("getTimeZone", "UTC_TIME")
                .property("getStartTime", formatter.format(FROM.minusSeconds(1)))
                .property("getEndTime", formatter.format(FROM.plusSeconds(MAX_VALUES)))
                .build();
        Group group2 = groupService.create(snapshot2);

        Map<String, Set<Long>> snapshot2Variables = new HashMap<>();
        snapshot2Variables.put(getSelectedVariables.name(), Collections.singleton(var1.getId()));
        groupService.addVariables(group2.getId(), snapshot2Variables);

        log.info("Snapshot created {}", group2);

        Map<String, Set<Variable>> fetchedGroupVariables = groupService.getVariables(group.getId());
        assertNotNull(fetchedGroupVariables);
        assertEquals(1, fetchedGroupVariables.size());
        Set<Variable> fetchedSnapshotVariables = fetchedGroupVariables.get(getSelectedVariables.name());
        assertEquals(2, fetchedSnapshotVariables.size());
        assertEquals(Sets.newHashSet(var1, var2), fetchedSnapshotVariables);

        Map<String, Set<Variable>> fetchedGroup2Variables = groupService.getVariables(group2.getId());
        assertNotNull(fetchedGroup2Variables);
        assertEquals(1, fetchedGroup2Variables.size());
        Set<Variable> fetchedSnapshot2Variables = fetchedGroup2Variables.get(getSelectedVariables.name());
        assertEquals(1, fetchedSnapshot2Variables.size());
        assertEquals(Sets.newHashSet(var1), fetchedSnapshot2Variables);
    }

    private static Variable buildCMWVariable(String variableName, String device, String property, String field,
            String description, VariableDeclaredType type) {
        EntityService entityService = ServiceClientFactory.createEntityService();
        SystemSpecService systemSpecService = ServiceClientFactory.createSystemSpecService();

        SystemSpec systemSpec = systemSpecService.findByName(SYSTEM)
                .orElseThrow(() -> new IllegalArgumentException("No such system"));

        Entity entity = entityService.findOne(
                Entities.suchThat().systemName().eq(systemSpec.getName()).and()
                        .keyValues().eq(systemSpec, ImmutableMap.of(DEVICE_KEY, device, PROPERTY_KEY, property)))
                .orElseThrow(() -> new IllegalArgumentException("No such entity"));

        VariableConfig config = VariableConfig.builder().entityId(entity.getId()).validity(
                TimeWindow.between(null, null)).fieldName(field).build();

        return Variable.builder().variableName(variableName).systemSpec(systemSpec)
                .description(description)
                .declaredType(type)
                .configs(new TreeSet<>(ImmutableSet.of(config))).build();
    }

    private static void createVariables() {
        VariableService variableService = ServiceClientFactory.createVariableService();

        Variable registered1 = variableService.create(buildCMWVariable(VARIABLE1, DEVICE1, PROPERTY3,
                "field", "Some test variable...", VariableDeclaredType.NUMERIC));
        log.info("Variable1 created {}", registered1);

        Variable registered2 = variableService.create(buildCMWVariable(VARIABLE2, DEVICE2, PROPERTY4,
                PROPERTY4_VALUE_FIELD, "Test variable for fixed intervals", VariableDeclaredType.NUMERIC));
        log.info("Variable2 created {}", registered2);

        Variable registered5 = variableService.create(buildCMWVariable(VARIABLE5, DEVICE1, PROPERTY5,
                "field", "Some test variable...", VariableDeclaredType.NUMERIC));
        log.info("Variable5 created {}", registered5);
        Variable registered6 = variableService.create(buildCMWVariable(VARIABLE6, DEVICE1, PROPERTY6,
                "field", "Some test variable...", VariableDeclaredType.NUMERIC));
        log.info("Variable6 created {}", registered6);
        Variable registered7 = variableService.create(buildCMWVariable(VARIABLE7, DEVICE1, PROPERTY7,
                "field", "Some test variable...", VariableDeclaredType.NUMERIC));
        log.info("Variable7 created {}", registered7);

        Variable registered5a = variableService.create(buildCMWVariable(VARIABLE5intArray, DEVICE1, PROPERTY5,
                "intArrayField", "Some test variable...", VariableDeclaredType.VECTOR_NUMERIC));
        log.info("Variable5a created {}", registered5a);
        Variable registered6a = variableService.create(buildCMWVariable(VARIABLE6intArray, DEVICE1, PROPERTY6,
                "intArrayField", "Some test variable...", VariableDeclaredType.VECTOR_NUMERIC));
        log.info("Variable6a created {}", registered6a);
        Variable registered7a = variableService.create(buildCMWVariable(VARIABLE7intArray, DEVICE1, PROPERTY7,
                "intArrayField", "Some test variable...", VariableDeclaredType.VECTOR_NUMERIC));
        log.info("Variable7a created {}", registered7a);

        Variable registered8 = variableService.create(buildCMWVariable(VARIABLE9shortArray, DEVICE1, PROPERTY5,
                "shortArrayField", "Some test variable...", VariableDeclaredType.VECTOR_NUMERIC));
        log.info("Variable created {}", registered8);

        Variable registered9 = variableService.create(buildCMWVariable(VARIABLE10doubleArray, DEVICE1, PROPERTY5,
                "doubleArrayField", "Some test variable...", VariableDeclaredType.VECTOR_NUMERIC));
        log.info("Variable created {}", registered9);

        Variable registered10 = variableService.create(buildCMWVariable(VARIABLE11stringArray, DEVICE1, PROPERTY5,
                "stringArrayField", "Some test variable...", VariableDeclaredType.VECTOR_STRING));
        log.info("Variable created {}", registered10);
        Variable registered11 = variableService.create(buildCMWVariable(VARIABLE8boolArray, DEVICE1, PROPERTY5,
                "boolArrayField", "Some test variable...", VariableDeclaredType.VECTOR_NUMERIC));
        log.info("Variable created {}", registered11);
        Variable registered12 = variableService.create(buildCMWVariable(VARIABLE12, DEVICE2, PROPERTY10,
                PROPERTY4_VALUE_FIELD, "Test variable for fixed intervals", VariableDeclaredType.NUMERIC));
        log.info("Variable12 created {}", registered12);
    }

    private static ImmutableData createDataType(int i, String property, long timestamp) {
        DataBuilder builder = ImmutableData.builder();
        builder.add(DEVICE_KEY, DEVICE1);
        builder.add(PROPERTY_KEY, property);
        builder.add(CLASS_KEY, CLASS1);
        builder.add(RECORD_VERSION_KEY, 0L);
        builder.add(RECORD_TIMESTAMP_KEY, timestamp);
        builder.add("field", i);

        int[] intArray = new int[1000];
        for (int idx = 0; idx < intArray.length; ++idx) {
            intArray[idx] = i + idx;
        }

        builder.add("intArrayField", intArray);

        short[] shortArray = new short[1000];
        for (int idx = 0; idx < shortArray.length; ++idx) {
            shortArray[idx] = (short) (idx + i);
        }

        builder.add("shortArrayField", shortArray); //will be converted to ints in the storage

        double[] doubleArray = new double[1000];
        for (int idx = 0; idx < doubleArray.length; ++idx) {
            doubleArray[idx] = idx + (double) i * 0.0000001;
        }

        builder.add("doubleArrayField", doubleArray);

        String[] stringArray = new String[1000];
        for (int idx = 0; idx < stringArray.length; ++idx) {
            stringArray[idx] = "string" + (idx + i);
        }

        builder.add("stringArrayField", stringArray);

        boolean[] boolArray = new boolean[1000];
        for (int idx = 0; idx < boolArray.length; ++idx) {
            boolArray[idx] = (i + idx) % 2 == 0;
        }

        builder.add("booleanArrayField", boolArray);

        return builder.build();

    }

    private static ImmutableData createFundamentalDataType(int i, String property, long timestamp,
            String lsaCycleProperty, String lsaCycle, String user, String destinationPropertyName, String dest) {
        DataBuilder builder = ImmutableData.builder();
        builder.add(DEVICE_KEY, DEVICE1);
        builder.add(PROPERTY_KEY, property);
        builder.add(CLASS_KEY, CLASS1);
        builder.add(RECORD_VERSION_KEY, 0L);
        builder.add(RECORD_TIMESTAMP_KEY, timestamp);
        builder.add(lsaCycleProperty, lsaCycle);
        builder.add("USER", user);
        builder.add("BP_NUM", i);
        builder.add(destinationPropertyName, dest);
        return builder.build();

    }

    /**
     * In the real system CMW the fundamental is composed from TGM & XTIM, we simulate it here.
     */
    private static void createFundamentalVariable() {
        VariableService variableService = ServiceClientFactory.createVariableService();
        EntityService entityService = ServiceClientFactory.createEntityService();
        SystemSpecService systemSpecService = ServiceClientFactory.createSystemSpecService();

        SystemSpec systemSpec = systemSpecService.findByName(SYSTEM)
                .orElseThrow(() -> new IllegalArgumentException("No such system"));

        //simulates tgm
        Entity cpsTgm = entityService.findOne(
                Entities.suchThat().systemName().eq(systemSpec.getName()).and()
                        .keyValues().eq(systemSpec, ImmutableMap.of(DEVICE_KEY, DEVICE1, PROPERTY_KEY, PROPERTY1)))
                .orElseThrow(() -> new IllegalArgumentException("No such entity"));
        //simulates xtim
        Entity xtim = entityService.findOne(
                Entities.suchThat().systemName().eq(systemSpec.getName()).and()
                        .keyValues().eq(systemSpec, ImmutableMap.of(DEVICE_KEY, DEVICE1, PROPERTY_KEY, PROPERTY2)))
                .orElseThrow(() -> new IllegalArgumentException("No such entity"));

        Instant splitTime = FROM.plusMillis(50);
        VariableConfig cpsTgmData = VariableConfig.builder().entityId(cpsTgm.getId()).validity(
                TimeWindow.between(null, splitTime)).build();
        VariableConfig xtimData = VariableConfig.builder().entityId(xtim.getId())
                .validity(TimeWindow.between(splitTime, null)).build();

        Variable variable = Variable.builder().variableName(FUNDAMENTAL_VARIABLE_NAME)
                .systemSpec(systemSpec)
                .description("Fundamental linking TGM and XTIM data under one record")
                .declaredType(VariableDeclaredType.FUNDAMENTAL).configs(
                        ImmutableSortedSet.of(cpsTgmData, xtimData)).build();

        Variable registered = variableService.create(variable);

        log.info("Fundamental Variable created {}", registered);

    }

    private static long createVectornumVariableForContext() {
        VariableService variableService = ServiceClientFactory.createVariableService();

        return variableService.create(buildCMWVariable(VECTORNUMERIC_VARIABLE_NAME, DEVICE3, PROPERTY8,
                PROPERTY8_VALUE_FIELD, "Some test vectornumeric variable for context...",
                VariableDeclaredType.VECTOR_NUMERIC)).getId();
    }

    private static void createContextVariable(long variableId) {
        VariableService variableService = ServiceClientFactory.createVariableService();

        Variable contextVariable = variableService
                .create(buildCMWVariable(CONTEXT_VARIABLE_NAME_PREFIX + variableId, DEVICE3, PROPERTY9,
                        PROPERTY9_VALUE_FIELD, "Some test context variable...", VariableDeclaredType.VECTOR_STRING));
        log.info("Context variable created {}", contextVariable);
    }

    private static List<ImmutableData> buildDatapointsForFixedIntervals(long intervalNr, String property) {
        List<ImmutableData> dataPoints = new ArrayList<>();
        for (short i = 0; i < 3; i++) {
            DataBuilder builder = ImmutableData.builder();

            builder.add(DEVICE_KEY, DEVICE2);
            builder.add(PROPERTY_KEY, property);
            builder.add(CLASS_KEY, CLASS1);
            builder.add(RECORD_VERSION_KEY, 0L);
            builder.add(RECORD_TIMESTAMP_KEY,
                    TimeUtils.getNanosFromInstant(
                            FIXED_INTERVALS_START.plusSeconds((intervalNr * INTERVAL_IN_SECONDS) + (i - 1))));
            builder.add(PROPERTY4_VALUE_FIELD, (double) (intervalNr * 3 + i));
            dataPoints.add(builder.build());
        }

        return dataPoints;
    }

    private static List<ImmutableData> buildDatapointsForContext() {
        short ARRAY_SIZE = 10;
        List<ImmutableData> dataPoints = new ArrayList<>();

        for (short i = 0; i < MAX_VALUES; i++) {
            DataBuilder data1 = ImmutableData.builder();
            data1.add(DEVICE_KEY, DEVICE3);
            data1.add(PROPERTY_KEY, PROPERTY8);
            data1.add(CLASS_KEY, CLASS1);
            data1.add(RECORD_VERSION_KEY, 0L);
            data1.add(RECORD_TIMESTAMP_KEY, TimeUtils.getNanosFromInstant(FROM.plusMillis(i)));

            double[] doubleArray = new double[ARRAY_SIZE];
            for (int idx = 0; idx < doubleArray.length; idx++) {
                doubleArray[idx] = idx + (double) i;
            }
            data1.add(PROPERTY8_VALUE_FIELD, doubleArray);

            dataPoints.add(data1.build());
        }

        for (short i = 0; i < NR_CONTEXT_DATAPOINTS; i++) {
            DataBuilder data2 = ImmutableData.builder();
            data2.add(DEVICE_KEY, DEVICE3);
            data2.add(PROPERTY_KEY, PROPERTY9);
            data2.add(CLASS_KEY, CLASS1);
            data2.add(RECORD_VERSION_KEY, 0L);
            data2.add(RECORD_TIMESTAMP_KEY, TimeUtils.getNanosFromInstant(FROM.plusMillis(i)));

            String[] stringArray = new String[ARRAY_SIZE];
            for (int idx = 0; idx < stringArray.length; idx++) {
                stringArray[idx] = "STR_" + i + "_" + idx;
            }
            data2.add(PROPERTY9_VALUE_FIELD, stringArray);
            dataPoints.add(data2.build());
        }

        return dataPoints;
    }

    public void waitForData() throws InterruptedException {
        if (!dataVisible) {
            //Have to wait 30 seconds in order for the data to be processed and visible in NXCALS extraction
            log.info("Waiting max {} minutes for the data to be visible in NXCALS...", MAX_WAIT_TIME);
            waitForData(TimeUnit.MINUTES.toMillis(MAX_WAIT_TIME));
            log.info("Ok, data should be processed now, proceeding with tests");
            dataVisible = true;
        } else {
            log.info("Data already checked");
        }
    }

    private void waitForData(long maxTime) throws InterruptedException {
        long start = System.currentTimeMillis();

        while (true) {
            if (System.currentTimeMillis() - start > maxTime) {
                fail("Data was not processed in the required maxTime=" + maxTime + " ms");
            }

            if (getCount(DEVICE1, PROPERTY1, FROM, FROM.plusMillis(MAX_VALUES)) == MAX_VALUES
                    && getCount(DEVICE1, PROPERTY2, FROM, FROM.plusMillis(MAX_VALUES)) == MAX_VALUES
                    && getCount(DEVICE1, PROPERTY3, FROM, FROM.plusMillis(MAX_VALUES)) == MAX_VALUES
                    && getCount(DEVICE1, PROPERTY5, FROM, FROM.plusMillis(MAX_VALUES)) == MAX_VALUES
                    && getCount(DEVICE1, PROPERTY6, FROM, FROM.plusMillis(MAX_VALUES)) == MAX_VALUES
                    && getCount(DEVICE1, PROPERTY7, FROM, FROM.plusMillis(MAX_VALUES)) == MAX_VALUES / 2
                    && getCount(DEVICE3, PROPERTY8, FROM, FROM.plusMillis(MAX_VALUES - 1)) == MAX_VALUES
                    && getCount(DEVICE3, PROPERTY9, FROM, FROM.plusMillis(NR_CONTEXT_DATAPOINTS - 1))
                    == NR_CONTEXT_DATAPOINTS
                    && getCountForIntervals() == NR_INTERVAL_DATAPOINTS) {
                return;
            }

            log.info("Still no data visible, waiting more...");
            TimeUnit.SECONDS.sleep(1);
        }
    }

    private long getCount(String deviceName, String propertyName, Instant from, Instant to) {
        //@formatter:off
        return DataQuery.builder(sparkSession).byEntities().system(SYSTEM)
                .startTime(from)
                .endTime(to)
                .entity().keyValueLike(DEVICE_KEY, deviceName).keyValue(PROPERTY_KEY, propertyName)
                .build()
                .count();
        //@formatter:on

    }

    private long getCountForVariable(String variableName) {
        //@formatter:off
        return DataQuery.builder(sparkSession).byVariables().system(SYSTEM)
                .startTime(FROM)
                .endTime(FROM.plusMillis(MAX_VALUES))
                .variable(variableName)
                .build()
                .count();
        //@formatter:on

    }

    private long getCountForIntervals() {
        //@formatter:off
        return DataQuery.builder(sparkSession).byEntities().system(SYSTEM)
                .startTime(FIXED_INTERVALS_START.plusSeconds(-1))
                .endTime(FIXED_INTERVALS_START.plusSeconds(NR_OF_INTERVALS * INTERVAL_IN_SECONDS + 1))
                .entity()
                .keyValue(DEVICE_KEY, DEVICE2)
                .keyValue(PROPERTY_KEY, PROPERTY4)
                .build()
                .count();
        //@formatter:on
    }

    @Test
    public void shouldFindAllBaseData() {
        assertEquals(100, getCount(DEVICE1, PROPERTY1, FROM, FROM.plusMillis(MAX_VALUES)));
        assertEquals(100, getCount(DEVICE1, PROPERTY2, FROM, FROM.plusMillis(MAX_VALUES)));
        assertEquals(100, getCount(DEVICE1, PROPERTY3, FROM, FROM.plusMillis(MAX_VALUES)));
        assertEquals(100, getCount(DEVICE1, PROPERTY5, FROM, FROM.plusMillis(MAX_VALUES)));
        assertEquals(100, getCount(DEVICE1, PROPERTY6, FROM, FROM.plusMillis(MAX_VALUES)));
        assertEquals(50, getCount(DEVICE1, PROPERTY7, FROM, FROM.plusMillis(MAX_VALUES)));

        assertEquals(100, getCountForVariable(VARIABLE1));
        assertEquals(100, getCountForVariable(FUNDAMENTAL_VARIABLE_NAME));
        assertEquals(NR_INTERVAL_DATAPOINTS, getCountForIntervals());
    }

    @Test
    public void shouldFindFundamentalVariables() {
        VariableSet fundamentals = metaService
                .getFundamentalsInTimeWindowWithNameLikePattern(
                        Timestamp.from(FROM),
                        Timestamp.from(FROM.plusMillis(MAX_VALUES)), RANDOM_ACCELERATOR + "%:C%:%");

        assertEquals(20, fundamentals.getVariableNames().size());

    }

    @Test
    public void shouldFind2FundamentalVariables() {
        VariableSet fundamentals = metaService
                .getFundamentalsInTimeWindowWithNameLikePattern(
                        Timestamp.from(FROM),
                        Timestamp.from(FROM.plusMillis(MAX_VALUES)), RANDOM_ACCELERATOR + "%:CYCLE1%:%LHC%");
        assertEquals(2, fundamentals.size());
    }

    @Test
    public void shouldCreateFundamentalDataset() {
        VariableSet fundamentals = VariableSet.fromCalsVars(ImmutableSet.of(
                cern.nxcals.api.backport.domain.core.metadata.Variable.builder()
                        .variableName(RANDOM_ACCELERATOR + ":CYCLE1:LHC1").build(),
                cern.nxcals.api.backport.domain.core.metadata.Variable.builder()
                        .variableName(RANDOM_ACCELERATOR + ":CYCLE1:LHC2").build(),
                cern.nxcals.api.backport.domain.core.metadata.Variable.builder()
                        .variableName(RANDOM_ACCELERATOR + ":CYCLE1:NOT_EXISTS_3").build()//does not exists
        ));

        Dataset<Row> dataset = FundamentalUtils
                .createDatasetFromFundamentals(Timestamp.from(FROM),
                        Timestamp.from(FROM.plusMillis(MAX_VALUES)),
                        fundamentals, DataQuery.builder(sparkSession));

        assertEquals(10, dataset.count());

    }

    @Test
    public void shouldGetFundamentalDataInTimeWindowWithFundamentalNamesLikePattern() {
        TimeseriesDataSet bothFundamentalsData = timeseriesService
                .getFundamentalDataInTimeWindowWithFundamentalNamesLikePattern(RANDOM_ACCELERATOR + "%:%CYCLE%:%LHC%",
                        Timestamp.from(FROM),
                        Timestamp.from(FROM.plusMillis(MAX_VALUES)));
        TimeseriesDataSet oneFundamentalData = timeseriesService
                .getFundamentalDataInTimeWindowWithFundamentalNamesLikePattern(RANDOM_ACCELERATOR + "%:%CYCLE%:%LHC1",
                        Timestamp.from(FROM),
                        Timestamp.from(FROM.plusMillis(MAX_VALUES)));
        assertEquals(100, bothFundamentalsData.size());
        assertEquals(50, oneFundamentalData.size());
    }

    @Test
    public void shouldFindDataForVariable() {
        Dataset<Row> rowDataset = DataQuery.builder(sparkSession).byVariables()
                .system(SYSTEM)
                .startTime(FROM)
                .endTime(FROM.plusMillis(MAX_VALUES))
                .variable(VARIABLE1)
                .build();

        assertEquals(100, rowDataset.count());

    }

    @Test
    public void shouldFilterByFundamentals() {
        VariableSet fundamentals = metaService
                .getFundamentalsInTimeWindowWithNameLikePattern(
                        Timestamp.from(FROM),
                        Timestamp.from(FROM.plusMillis(MAX_VALUES)), RANDOM_ACCELERATOR + "%:CYCLE1:%LHC%");
        assertEquals(2, fundamentals.size());

        TimeseriesDataSet data = timeseriesService
                .getDataInTimeWindowFilteredByFundamentals(retrieveBackportVariableByName(VARIABLE1),
                        Timestamp.from(FROM),
                        Timestamp.from(FROM.plusMillis(MAX_VALUES)), fundamentals);

        assertEquals(10, data.size());

    }

    @Test
    public void shouldFilterByFundamentalsWithTimeLimit() {
        VariableSet fundamentals = metaService
                .getFundamentalsInTimeWindowWithNameLikePattern(
                        Timestamp.from(FROM),
                        Timestamp.from(FROM.plusMillis(1)), RANDOM_ACCELERATOR + "%:%CYCLE%:%LHC%");
        assertEquals(2, fundamentals.size());

        TimeseriesDataSet data = timeseriesService
                .getDataInTimeWindowFilteredByFundamentals(retrieveBackportVariableByName(VARIABLE1),
                        Timestamp.from(FROM),
                        Timestamp.from(FROM.plusMillis(1)), fundamentals);

        assertEquals(2, data.size());

    }

    @ParameterizedTest
    @MethodSource("filterByTimestamps")
    public void shouldFilterByTimestamps(List<Timestamp> timestamps, int expectedCount) {
        TimeseriesDataSet data = timeseriesService
                .getDataFilteredByTimestamps(retrieveBackportVariableByName(VARIABLE1), timestamps);
        assertEquals(expectedCount, data.size());
    }

    @Test
    public void shouldThrowForEmptyTimestampList() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> timeseriesService.getDataFilteredByTimestamps(retrieveBackportVariableByName(VARIABLE1), new ArrayList<>()));
    }

    @Test
    public void shouldFilterByFundamentalsAndReturnEmptyDataForNonExistingFundamental() {
        TimeseriesDataSet data = timeseriesService
                .getDataInTimeWindowFilteredByFundamentals(retrieveBackportVariableByName(VARIABLE1),
                        Timestamp.from(FROM),
                        Timestamp.from(FROM.plusMillis(MAX_VALUES)),
                        VariableSet.fromCalsVars(ImmutableSet
                                .of(cern.nxcals.api.backport.domain.core.metadata.Variable.builder().variableName(
                                        RANDOM_ACCELERATOR + FundamentalData.FUNDAMENTAL_SEPARATOR + "DOES_NOT_EXITST"
                                                + FundamentalData.FUNDAMENTAL_SEPARATOR + "LHC").build())));

        assertEquals(0, data.size());
    }

    @Test
    public void shouldFilterByFundamentalsWithAllFundamentals() {
        VariableSet fundamentals = metaService
                .getFundamentalsInTimeWindowWithNameLikePattern(
                        Timestamp.from(FROM),
                        Timestamp.from(FROM.plusMillis(MAX_VALUES)), RANDOM_ACCELERATOR + "%:%CYCLE%:%LHC%");

        System.out.println(fundamentals.getVariableNames());

        assertEquals(20, fundamentals.size());

        TimeseriesDataSet data = timeseriesService
                .getDataInTimeWindowFilteredByFundamentals(retrieveBackportVariableByName(VARIABLE1),
                        Timestamp.from(FROM),
                        Timestamp.from(FROM.plusMillis(MAX_VALUES)), fundamentals);

        assertEquals(100, data.size());
    }

    @ParameterizedTest
    @MethodSource("variableStatisticsFilteredByFundamentals")
    public void shouldGetVariableStatisticsFilteredByFundamentals(String variableName, BigDecimal expectedMinValue,
            BigDecimal expectedMaxValue, long expectedValueCount) {
        VariableSet fundamentals = metaService
                .getFundamentalsInTimeWindowWithNameLikePattern(
                        Timestamp.from(FROM),
                        Timestamp.from(FROM.plusMillis(MAX_VALUES)), RANDOM_ACCELERATOR + "%:CYCLE1:%LHC%");

        VariableSet variables = new VariableSet();
        variables.addVariable(retrieveBackportVariableByName(variableName));
        VariableStatisticsSet variableStatistics = timeseriesService
                .getVariableStatisticsOverTimeWindowFilteredByFundamentals(variables, Timestamp.from(FROM),
                        Timestamp.from(FROM.plusMillis(MAX_VALUES)), fundamentals);
        VariableStatistics result = variableStatistics.getVariableStatistics(variableName);

        assertEquals(expectedMinValue, result.getMinValue());
        assertEquals(expectedMaxValue, result.getMaxValue());
        assertEquals(expectedValueCount, result.getValueCount());

    }

    public static Object[][] variableStatisticsFilteredByFundamentals() {
        return new Object[][] {
                new Object[] { VARIABLE1, BigDecimal.valueOf(1), BigDecimal.valueOf(91), 10L },
                new Object[] { VARIABLE5, BigDecimal.valueOf(5001), BigDecimal.valueOf(5091), 10L },
                new Object[] { VARIABLE6, BigDecimal.valueOf(6001), BigDecimal.valueOf(6091), 10L }
        };
    }

    @ParameterizedTest
    @MethodSource("variableStatistics")
    public void shouldGetVariableStatistics(String variableName, BigDecimal expectedMinValue,
            BigDecimal expectedMaxValue, long expectedValueCount) {
        VariableSet variables = new VariableSet();
        variables.addVariable(retrieveBackportVariableByName(variableName));
        VariableStatisticsSet variableStatistics = timeseriesService
                .getVariableStatisticsOverMultipleVariablesInTimeWindow(variables, Timestamp.from(FROM),
                        Timestamp.from(FROM.plusMillis(MAX_VALUES)));
        VariableStatistics result = variableStatistics.getVariableStatistics(variableName);

        assertEquals(expectedMinValue, result.getMinValue());
        assertEquals(expectedMaxValue, result.getMaxValue());
        assertEquals(expectedValueCount, result.getValueCount());

    }

    public static Object[][] variableStatistics() {
        return new Object[][] {
                new Object[] { VARIABLE1, BigDecimal.valueOf(0), BigDecimal.valueOf(99), 100L },
                new Object[] { VARIABLE5, BigDecimal.valueOf(5000), BigDecimal.valueOf(5099), 100L },
                new Object[] { VARIABLE6, BigDecimal.valueOf(6000), BigDecimal.valueOf(6099), 100L }
        };
    }

    @Test
    public void shouldGetDataInFixedIntervalsForAgg() {
        for (ScaleAlgorithm algorithm : EnumSet.of(MIN, MAX, SUM, AVG, COUNT)) {
            // when
            TimeseriesDataSet actual = retrieveActualFixedIntervalsDataset(algorithm);
            TimeseriesDataSet generated = generateExpectedFixedIntervalsDatasetForAgg(algorithm);

            // then
            assertEquals(generated, actual, String.format("Checking assertions for algorithm [%s]", algorithm));
        }
    }

    @Test
    public void shouldGetDataInFixedIntervalsForAggMultipleVars() {
        for (ScaleAlgorithm algorithm : EnumSet.of(MIN, MAX, SUM, AVG, COUNT)) {
            // when
            TimeseriesDataSet actual = retrieveActualFixedIntervalsDatasetForMultipleVars(algorithm);
            TimeseriesDataSet generated = generateExpectedFixedIntervalsDatasetForAggMultipleVars(algorithm);

            // then
            for (int i = 0; i < actual.size(); i++) {
                assertEquals(generated.getTimeseriesData(i), actual.getTimeseriesData(i),
                        String.format("Checking assertions for algorithm [%s] on index [%s]", algorithm, i));
            }
        }
    }

    @Test
    public void shouldGetDataInFixedIntervalsForAvgWithLastWindowSmallerThanInterval() {
        Instant rangeEndNotAlignedOnInterval = FIXED_INTERVALS_END.minusSeconds(10);
        // when
        TimeseriesDataSet actual = retrieveActualFixedIntervalsDataset(AVG, FIXED_INTERVALS_START,
                rangeEndNotAlignedOnInterval);
        TimeseriesDataSet expected = generateExpectedFixedIntervalsDatasetForAvgWithSmallerLastWindow();
        // then
        assertEquals(expected, actual);
    }

    private TimeseriesDataSet generateExpectedFixedIntervalsDatasetForAvgWithSmallerLastWindow() {
        List<TimeseriesData> list = new ArrayList<>();
        for (long i = 0; i < NR_OF_INTERVALS; i++) {
            boolean lastWindow = (i == NR_OF_INTERVALS - 1);
            double value = (i * 3) + (lastWindow ? 1.5d : 2d);
            TimeseriesData timeseriesData = SparkTimeseriesData.of(
                    Timestamp.from(FIXED_INTERVALS_START.plusSeconds(INTERVAL_IN_SECONDS * i)), ImmutableEntry.of(
                            NXC_EXTR_VALUE.getValue(), value));
            list.add(timeseriesData);
        }
        return SparkTimeseriesDataSet.of(retrieveBackportVariableByName(VARIABLE2), list);
    }

    private TimeseriesDataSet generateExpectedFixedIntervalsDatasetForAgg(ScaleAlgorithm scaleAlgorithm) {
        List<TimeseriesData> list = new ArrayList<>();
        for (long i = 0; i < NR_OF_INTERVALS; i++) {
            Number value = 0;
            switch (scaleAlgorithm) {
            case MIN:
                value = (i * 3) + 1d;
                break;
            case MAX:
                value = (i * 3) + 3d;
                break;
            case SUM:
                value = (i * 9) + 6d;
                break;
            case AVG:
                value = (i * 3) + 2d;
                break;
            case COUNT:
                value = 3L;
                break;
            default:
                break;
            }

            TimeseriesData timeseriesData = SparkTimeseriesData.of(
                    Timestamp.from(FIXED_INTERVALS_START.plusSeconds(INTERVAL_IN_SECONDS * i)), ImmutableEntry.of(
                            NXC_EXTR_VALUE.getValue(), value));
            list.add(timeseriesData);
        }
        return SparkTimeseriesDataSet.of(retrieveBackportVariableByName(VARIABLE2), list);
    }

    private TimeseriesDataSet generateExpectedFixedIntervalsDatasetForAggMultipleVars(ScaleAlgorithm scaleAlgorithm) {
        List<TimeseriesData> list = new ArrayList<>();
        for (long i = 0; i < NR_OF_INTERVALS; i++) {
            Number value = 0;
            switch (scaleAlgorithm) {
            case MIN:
                value = (i * 3) + 1d;
                break;
            case MAX:
                value = (i * 3) + 3d;
                break;
            case SUM:
                value = 2 * ((i * 9) + 6d);
                break;
            case AVG:
                value = (i * 3) + 2d;
                break;
            case COUNT:
                value = 6d;
                break;
            default:
                break;
            }

            TimeseriesData timeseriesData = SparkTimeseriesData
                    .of(Timestamp.from(FIXED_INTERVALS_START.plusSeconds(INTERVAL_IN_SECONDS * i)), ImmutableEntry
                            .of(NXC_EXTR_VALUE.getValue(), value));
            list.add(timeseriesData);
        }
        return SparkTimeseriesDataSet.of(retrieveBackportVariableByName(VARIABLE2), list);
    }

    @Test
    public void shouldGetDataInFixedIntervalsForRepeat() {
        TimeseriesDataSet actual = retrieveActualFixedIntervalsDataset(ScaleAlgorithm.REPEAT);
        TimeseriesDataSet generated = generateExpectedFixedIntervalsDatasetForRepeat();

        assertEquals(generated, actual);
    }

    @Test
    public void shouldGetDataInFixedIntervalsForInterpolation() {
        TimeseriesDataSet actual = retrieveActualFixedIntervalsDataset(ScaleAlgorithm.INTERPOLATE);
        TimeseriesDataSet generated = generateExpectedDatasetForInterpolate();

        assertEquals(generated, actual);
    }

    @Test
    public void shoulGetVectorElements() {
        VectornumericElementsSet elementsSet =
                metaService.getVectorElements(retrieveBackportVariableByName(VECTORNUMERIC_VARIABLE_NAME));

        assertEquals(NR_CONTEXT_DATAPOINTS, elementsSet.size());
    }

    @Test
    public void shouldGetVectorElementsInTimeWindow() {
        cern.nxcals.api.backport.domain.core.metadata.Variable vectorNumericVar =
                retrieveBackportVariableByName(VECTORNUMERIC_VARIABLE_NAME);

        VectornumericElementsSet elementsSet =
                metaService.getVectorElementsInTimeWindow(vectorNumericVar,
                        Timestamp.from(FROM.plusMillis(1)), Timestamp.from(FROM.plusMillis(2)));

        VectornumericElements current =
                elementsSet.getVectornumericElements(TimeUtils.getTimestampFromInstant(Instant.now()));

        assertEquals(3, elementsSet.size());
        assertEquals("STR_2_0", current.getElementName(0));
    }

    @Test
    public void shouldGetVectorElementsInExtendedTimeWindow() {
        cern.nxcals.api.backport.domain.core.metadata.Variable vectorNumericVar =
                retrieveBackportVariableByName(VECTORNUMERIC_VARIABLE_NAME);

        VectornumericElementsSet elementsSet =
                metaService.getVectorElementsInTimeWindow(vectorNumericVar,
                        Timestamp.from(FROM.plusMillis(NR_CONTEXT_DATAPOINTS)),
                        Timestamp.from(FROM.plusMillis(NR_CONTEXT_DATAPOINTS + 1)));

        VectornumericElements current =
                elementsSet.getVectornumericElements(
                        TimeUtils.getTimestampFromInstant(FROM.plusMillis(NR_CONTEXT_DATAPOINTS - 1)));

        assertEquals(1, elementsSet.size());
        assertEquals("STR_2_0", current.getElementName(0));
    }

    @Test
    public void shouldGetDataInTimeWindowFilteredByValuesByItself() {
        cern.nxcals.api.backport.domain.core.metadata.Variable drivingVariable = retrieveBackportVariableByName(
                VARIABLE1);
        Filter filter1 = FilterFactory.getInstanceForNumerics()
                .withSingleValuePredicate(drivingVariable, 50.0, FilterOperand.EQUALS);

        Filter filter2 = FilterFactory.getInstanceForNumerics()
                .withSingleValuePredicate(drivingVariable, 60.0, FilterOperand.EQUALS);

        Filter filter = Filters.or(filter1, filter2);

        TimeseriesDataSet values = timeseriesService
                .getDataInTimeWindowFilteredByValues(drivingVariable, Timestamp.from(FROM),
                        Timestamp.from(FROM.plusMillis(MAX_VALUES)), filter);

        assertEquals(2, values.size());

    }

    @Test
    public void shouldGetDataInTimeWindowFilteredByValuesByOtherVariable() {
        cern.nxcals.api.backport.domain.core.metadata.Variable drivingVariable = retrieveBackportVariableByName(
                VARIABLE1);
        cern.nxcals.api.backport.domain.core.metadata.Variable filterVariable5 = retrieveBackportVariableByName(
                VARIABLE5);
        cern.nxcals.api.backport.domain.core.metadata.Variable filterVariable6 = retrieveBackportVariableByName(
                VARIABLE6);

        Filter filter1 = FilterFactory.getInstanceForNumerics()
                .withSingleValuePredicate(drivingVariable, 50.0, FilterOperand.EQUALS); //1 from this

        Filter filter2 = FilterFactory.getInstanceForNumerics()
                .withSingleValuePredicate(filterVariable5, 5060.0, FilterOperand.GREATER_OR_EQUALS); //40 from this

        Filter filter3 = FilterFactory.getInstanceForNumerics()
                .withMultipleValuesPredicate(filterVariable6, new Double[] { 6010.0, 6011.0 },
                        FilterOperand.IN); //2 from this

        Filter filter = Filters.or(Filters.or(filter1, filter2), filter3);

        TimeseriesDataSet values = timeseriesService
                .getDataInTimeWindowFilteredByValues(drivingVariable, Timestamp.from(FROM),
                        Timestamp.from(FROM.plusMillis(MAX_VALUES)), filter);

        assertEquals(43, values.size());

    }

    @Test
    public void shouldGetDataInTimeWindowFilteredByValuesByOtherVariableBetween() {
        cern.nxcals.api.backport.domain.core.metadata.Variable drivingVariable = retrieveBackportVariableByName(
                VARIABLE1);
        cern.nxcals.api.backport.domain.core.metadata.Variable filterVariable5 = retrieveBackportVariableByName(
                VARIABLE5);

        Filter filter = FilterFactory.getInstanceForNumerics()
                .withMultipleValuesPredicate(filterVariable5, new Double[] { 5010.0, 5012.0 },
                        FilterOperand.BETWEEN); //3 from this

        TimeseriesDataSet values = timeseriesService
                .getDataInTimeWindowFilteredByValues(drivingVariable, Timestamp.from(FROM),
                        Timestamp.from(FROM.plusMillis(MAX_VALUES)), filter);

        assertEquals(3, values.size());

    }

    @Test
    public void shouldGetDataInTimeWindowFilteredByValuesByOtherVariableByTimestamps() {
        cern.nxcals.api.backport.domain.core.metadata.Variable drivingVariable = retrieveBackportVariableByName(
                VARIABLE1);
        cern.nxcals.api.backport.domain.core.metadata.Variable filterVariable6 = retrieveBackportVariableByName(
                VARIABLE6);
        cern.nxcals.api.backport.domain.core.metadata.Variable filterVariable7 = retrieveBackportVariableByName(
                VARIABLE7);

        Filter filter1 = FilterFactory.getInstanceForNumerics().withTimePredicate(filterVariable7); //50 from this
        Filter filter2 = FilterFactory.getInstanceForNumerics()
                .withSingleValuePredicate(filterVariable6, 6010.0, FilterOperand.LESS); // but only 10 from this

        Filter filter = Filters.and(filter1, filter2); //should only get 5 because of the null from variable7
        TimeseriesDataSet values = timeseriesService
                .getDataInTimeWindowFilteredByValues(drivingVariable, Timestamp.from(FROM),
                        Timestamp.from(FROM.plusMillis(MAX_VALUES)), filter);

        assertEquals(5, values.size());

    }

    @Test
    public void shouldGetDataInTimeWindowFilteredByValuesAndVectorsLess() {

        cern.nxcals.api.backport.domain.core.metadata.Variable drivingVariable = retrieveBackportVariableByName(
                VARIABLE1);
        cern.nxcals.api.backport.domain.core.metadata.Variable filterVariable6a = retrieveBackportVariableByName(
                VARIABLE6intArray);

        Filter filter1 = FilterFactory.getInstanceForNumerics()
                .withSingleValuePredicate(filterVariable6a, 6010.0, FilterOperand.LESS,
                        VectorRestriction.SINGLE_ELEMENT); // 10 from this

        TimeseriesDataSet values = timeseriesService
                .getDataInTimeWindowFilteredByValues(drivingVariable, Timestamp.from(FROM),
                        Timestamp.from(FROM.plusMillis(MAX_VALUES)), filter1);

        assertEquals(10, values.size());

    }

    @Test
    public void shouldGetDataInTimeWindowFilteredByValuesAndVectorsLessOrEquals() {

        cern.nxcals.api.backport.domain.core.metadata.Variable drivingVariable = retrieveBackportVariableByName(
                VARIABLE1);
        cern.nxcals.api.backport.domain.core.metadata.Variable filterVariable6a = retrieveBackportVariableByName(
                VARIABLE6intArray);

        Filter filter1 = FilterFactory.getInstanceForNumerics()
                .withSingleValuePredicate(filterVariable6a, 6010.0, FilterOperand.LESS_OR_EQUALS,
                        VectorRestriction.SINGLE_ELEMENT); // 10 from this

        TimeseriesDataSet values = timeseriesService
                .getDataInTimeWindowFilteredByValues(drivingVariable, Timestamp.from(FROM),
                        Timestamp.from(FROM.plusMillis(MAX_VALUES)), filter1);

        assertEquals(11, values.size());

    }

    @Test
    public void shouldGetDataInTimeWindowFilteredByValuesAndVectorsVectorAndScalarConditions() {

        cern.nxcals.api.backport.domain.core.metadata.Variable drivingVariable = retrieveBackportVariableByName(
                VARIABLE1);
        cern.nxcals.api.backport.domain.core.metadata.Variable filterVariable6a = retrieveBackportVariableByName(
                VARIABLE6intArray);
        cern.nxcals.api.backport.domain.core.metadata.Variable filterVariable5 = retrieveBackportVariableByName(
                VARIABLE5);

        Filter filter1 = FilterFactory.getInstanceForNumerics()
                .withMultipleValuesPredicate(filterVariable5, new Double[] { 5010.0, 5012.0 },
                        FilterOperand.BETWEEN); //3 from this

        Filter filter2 = FilterFactory.getInstanceForNumerics()
                .withMultipleValuesPredicate(filterVariable6a, new Double[] { 1.0, 1000000.0 }, FilterOperand.BETWEEN,
                        VectorRestriction.ALL_ELEMENTS); // all from this

        Filter filter = Filters.and(filter1, filter2);
        TimeseriesDataSet values = timeseriesService
                .getDataInTimeWindowFilteredByValues(drivingVariable, Timestamp.from(FROM),
                        Timestamp.from(FROM.plusMillis(MAX_VALUES)), filter);

        assertEquals(3, values.size());

    }

    @Test
    public void shouldGetDataInTimeWindowFilteredByValuesAndVectorsShorts() {
        //short are converted to ints at ingestion

        cern.nxcals.api.backport.domain.core.metadata.Variable drivingVariable = retrieveBackportVariableByName(
                VARIABLE1);
        cern.nxcals.api.backport.domain.core.metadata.Variable filterVariable = retrieveBackportVariableByName(
                VARIABLE9shortArray);

        Filter filter = FilterFactory.getInstanceForNumerics()
                .withSingleValuePredicate(filterVariable, 5020.0, FilterOperand.LESS_OR_EQUALS,
                        VectorRestriction.SINGLE_ELEMENT);

        TimeseriesDataSet values = timeseriesService
                .getDataInTimeWindowFilteredByValues(drivingVariable, Timestamp.from(FROM),
                        Timestamp.from(FROM.plusMillis(MAX_VALUES)), filter);

        assertEquals(21, values.size());

    }

    @Test
    public void shouldGetDataInTimeWindowFilteredByValuesAndVectorsStrings() {
        //short are converted to ints at ingestion

        cern.nxcals.api.backport.domain.core.metadata.Variable drivingVariable = retrieveBackportVariableByName(
                VARIABLE1);
        cern.nxcals.api.backport.domain.core.metadata.Variable filterVariable = retrieveBackportVariableByName(
                VARIABLE11stringArray);

        Filter filter = FilterFactory.getInstanceForTextuals()
                .withMultipleValuesPredicate(filterVariable, new String[] { "string5015", "string5016", "string5017" },
                        FilterOperand.IN, VectorRestriction.SINGLE_ELEMENT);

        TimeseriesDataSet values = timeseriesService
                .getDataInTimeWindowFilteredByValues(drivingVariable, Timestamp.from(FROM),
                        Timestamp.from(FROM.plusMillis(MAX_VALUES)), filter);

        assertEquals(18, values.size());

    }

    @Test
    public void shouldGetDataInTimeWindowFilteredByValuesAndVectorsDoubles() {
        //short are converted to ints at ingestion

        cern.nxcals.api.backport.domain.core.metadata.Variable drivingVariable = retrieveBackportVariableByName(
                VARIABLE1);
        cern.nxcals.api.backport.domain.core.metadata.Variable filterVariable = retrieveBackportVariableByName(
                VARIABLE10doubleArray);

        Filter filter = FilterFactory.getInstanceForNumerics()
                .withSingleValuePredicate(filterVariable, 1.0005001, FilterOperand.EQUALS,
                        VectorRestriction.SINGLE_ELEMENT);

        TimeseriesDataSet values = timeseriesService
                .getDataInTimeWindowFilteredByValues(drivingVariable, Timestamp.from(FROM),
                        Timestamp.from(FROM.plusMillis(MAX_VALUES)), filter);

        assertEquals(1, values.size());

    }

    @Test
    public void shouldFindSnapshots() {
        SnapshotCriteria criteria = new SnapshotCriteria.SnapshotCriteriaBuilder(SNAPSHOT_NAME1, USER_NAME).withPublic()
                .build();
        List<Snapshot> snapshots = this.snapshotDataService.getSnapshotsFor(criteria);
        assertEquals(1, snapshots.size());

        SnapshotCriteria criteria2 = new SnapshotCriteria.SnapshotCriteriaBuilder(SNAPSHOT_NAME2, USER_NAME)
                .withPublic().build();
        List<Snapshot> snapshots2 = this.snapshotDataService.getSnapshotsFor(criteria2);
        assertEquals(1, snapshots2.size());

    }

    @Test
    public void shouldFindDataInSnapshotWithFixedIntervals() {
        List<TimeseriesDataSet> dataForSnapshot = this.timeseriesService.getDataForSnapshot(SNAPSHOT_NAME2, USER_NAME);
        assertEquals(1, dataForSnapshot.size());
        assertEquals(MAX_VALUES, dataForSnapshot.get(0).size());
    }

    @Test
    public void shouldGetDataForSnapshot() {
        List<TimeseriesDataSet> dataForSnapshot = this.timeseriesService.getDataForSnapshot(SNAPSHOT_NAME1, USER_NAME);
        assertEquals(2, dataForSnapshot.size());
        int dsSize1 = dataForSnapshot.get(0).size();
        assertTrue(dsSize1 > 0);
    }

    @ParameterizedTest
    @MethodSource("aggregatedData")
    public void getDataForVariableAggregated(ScaleAlgorithm algorithm, double expectedValue) {
        cern.nxcals.api.backport.domain.core.metadata.Variable variable = retrieveBackportVariableByName(VARIABLE1);
        try {
            TimeseriesData result = this.timeseriesService
                    .getDataForVariableAggregated(variable, Timestamp.from(FROM),
                            Timestamp.from(FROM.plusMillis(MAX_VALUES)), algorithm);
            assertEquals(expectedValue, result.getDoubleValue(), 0.01d);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    public static Object[][] aggregatedData() {
        return new Object[][] {
                new Object[] { MIN, 0d },
                new Object[] { MAX, 99d },
                new Object[] { COUNT, 100d },
                new Object[] { SUM, IntStream.range(0, 100).sum() },
                new Object[] { AVG, IntStream.range(0, 100).average().getAsDouble() }
        };
    }

    public static Object[][] filterByTimestamps() {
        return new Object[][] {
                new Object[] { Arrays.asList(Timestamp.from(FROM), Timestamp.from(FROM.plusMillis(20)),
                        Timestamp.from(FROM.plusMillis(25)), Timestamp.from(FROM.plusMillis(200))), 3 },
                new Object[] { Arrays.asList(Timestamp.from(FROM), Timestamp.from(FROM.minusMillis(100))), 1 },
                new Object[] { Arrays.asList(Timestamp.from(FROM), Timestamp.from(FROM.plusMillis(1))), 2 },
                new Object[] { Arrays.asList(Timestamp.from(FROM), Timestamp.from(FROM.plusMillis(20)),
                        Timestamp.from(FROM.plusMillis(10))), 3 },

        };
    }

    public void shouldReturnNullWhenThereIsNoDataForVariableAggregated() {
        cern.nxcals.api.backport.domain.core.metadata.Variable variable = retrieveBackportVariableByName(VARIABLE1);
        TimeseriesData result = this.timeseriesService
                .getDataForVariableAggregated(variable, Timestamp.from(Instant.now()),
                        Timestamp.from(Instant.now().minusMillis(MAX_VALUES)), COUNT);
        assertNull(result);
    }

    private TimeseriesDataSet generateExpectedDatasetForInterpolate() {
        List<TimeseriesData> list = new ArrayList<>();

        for (long i = 0; i < NR_OF_INTERVALS; i++) {
            TimeseriesData timeseriesData = SparkTimeseriesData
                    .of(Timestamp.from(FIXED_INTERVALS_START.plusSeconds(INTERVAL_IN_SECONDS * i)),
                            ImmutableEntry.of(NXC_EXTR_VALUE.getValue(), (i * 3) + 1d));
            list.add(timeseriesData);
        }
        return SparkTimeseriesDataSet.of(retrieveBackportVariableByName(VARIABLE2), list);
    }

    private TimeseriesDataSet generateExpectedFixedIntervalsDatasetForRepeat() {
        List<TimeseriesData> list = new ArrayList<>();

        for (long i = 0; i < NR_OF_INTERVALS; i++) {
            TimeseriesData timeseriesData = SparkTimeseriesData
                    .of(Timestamp.from(FIXED_INTERVALS_START.plusSeconds(INTERVAL_IN_SECONDS * i)),
                            ImmutableEntry.of(NXC_EXTR_VALUE.getValue(), (i * 3) + 1d));
            list.add(timeseriesData);
        }
        return SparkTimeseriesDataSet.of(retrieveBackportVariableByName(VARIABLE2), list);
    }

    private cern.nxcals.api.backport.domain.core.metadata.Variable retrieveBackportVariableByName(String variableName) {
        VariableSet variableSet = this.metaService
                .getVariablesOfDataTypeWithNameLikePattern(variableName, VariableDataType.ALL);

        assertEquals(1, variableSet.size());

        return variableSet.getVariable(variableName);
    }

    private TimeseriesDataSet retrieveActualFixedIntervalsDataset(ScaleAlgorithm scaleAlgorithm) {
        return retrieveActualFixedIntervalsDataset(scaleAlgorithm, FIXED_INTERVALS_START, FIXED_INTERVALS_END);
    }

    private TimeseriesDataSet retrieveActualFixedIntervalsDataset(ScaleAlgorithm scaleAlgorithm, Instant start,
            Instant end) {
        TimescalingProperties timescalingProperties = new TimescalingProperties(scaleAlgorithm, INTERVAL_IN_SECONDS,
                LoggingTimeInterval.SECOND);

        return timeseriesService.getDataInFixedIntervals(
                retrieveBackportVariableByName(VARIABLE2),
                Timestamp.from(start), Timestamp.from(end), timescalingProperties);
    }

    private TimeseriesDataSet retrieveActualFixedIntervalsDatasetForMultipleVars(ScaleAlgorithm scaleAlgorithm) {
        TimescalingProperties timescalingProperties = new TimescalingProperties(scaleAlgorithm, INTERVAL_IN_SECONDS,
                LoggingTimeInterval.SECOND);
        VariableSet variables = new VariableSet();
        variables.addVariable(retrieveBackportVariableByName(VARIABLE2));
        variables.addVariable(retrieveBackportVariableByName(VARIABLE12));
        return timeseriesService.getDataForMultipleVariablesAggregatedInFixedIntervals(variables,
                Timestamp.from(FIXED_INTERVALS_START), Timestamp.from(FIXED_INTERVALS_END), timescalingProperties);
    }
}
