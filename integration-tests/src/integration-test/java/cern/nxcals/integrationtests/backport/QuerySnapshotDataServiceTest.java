package cern.nxcals.integrationtests.backport;

import cern.nxcals.api.backport.client.service.QuerySnapshotDataService;
import cern.nxcals.api.backport.client.service.ServiceBuilder;
import cern.nxcals.api.backport.domain.core.metadata.FundamentalData;
import cern.nxcals.api.backport.domain.core.metadata.Variable;
import cern.nxcals.api.backport.domain.core.metadata.VariableSet;
import cern.nxcals.api.backport.domain.core.snapshot.Snapshot;
import cern.nxcals.api.backport.domain.core.snapshot.SnapshotCriteria;
import cern.nxcals.api.custom.domain.FundamentalFilter;
import cern.nxcals.api.custom.domain.GroupType;
import cern.nxcals.api.custom.domain.util.FundamentalMapper;
import cern.nxcals.api.domain.Group;
import cern.nxcals.api.domain.Identifiable;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.Visibility;
import cern.nxcals.api.extraction.metadata.GroupService;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import com.google.common.collect.Sets;
import lombok.NonNull;
import org.apache.commons.lang.StringUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
public class QuerySnapshotDataServiceTest {
    static {
        //Please use this one just in case you want to run those test from IDE for debugging
        // normally should stay commented-out
        //        String username = System.getProperty("user.name");
        //        System.setProperty("service.url", "https://nxcals-<user>1.cern.ch:19093".replace("<user>", username));
    }

    private static String FUNDAMENTAL_VARIABLE_NAME_SEPARATOR = FundamentalData.FUNDAMENTAL_SEPARATOR;

    private static SystemSpec SYSTEM = ServiceClientFactory.createSystemSpecService().
            findByName("MOCK-SYSTEM").orElseThrow(() -> new IllegalArgumentException("No such system"));

    private final GroupService groupService = ServiceClientFactory.createGroupService();

    private final QuerySnapshotDataService snapshotService = ServiceBuilder.getInstance()
            .createQuerySnapshotDataService();

    @Test
    public void shouldGetSnapshotWithEmptyFundamentalVariablesWhenNoFiltersDefined() {
        Group group = createSnapshotGroup(FundamentalMapper.toGroupProperties(Collections.emptySet()));
        Snapshot snapshot = fetchSnapshotBasedOn(group);

        VariableSet fundamentalVariables = snapshot.getProperties().getFundamentalFilters();
        assertNotNull(fundamentalVariables);
        assertTrue(fundamentalVariables.isEmpty());
    }

    @Test
    public void shouldGetSnapshotWithGeneratedFundamentalVariables() {
        FundamentalFilter filter1 = FundamentalFilter.builder()
                .accelerator("TEST").lsaCycle("super_cycle-test").timingUser("tester").build();
        FundamentalFilter filter2 = FundamentalFilter.builder()
                .accelerator("TEST").lsaCycle("super_cycle-test2").timingUser("tester").build();
        Set<FundamentalFilter> fundamentalFilters = Sets.newHashSet(filter1, filter2);

        Group group = createSnapshotGroup(FundamentalMapper.toGroupProperties(fundamentalFilters));
        Snapshot snapshot = fetchSnapshotBasedOn(group);

        VariableSet fundamentalVariables = snapshot.getProperties().getFundamentalFilters();
        assertNotNull(fundamentalVariables);
        assertEquals(fundamentalFilters.size(), fundamentalVariables.size());

        Set<String> expectedVariableNames = getExpectedVariableNamesFrom(fundamentalFilters);
        Set<String> actualVariableNames = getVariableNamesFrom(fundamentalVariables);

        assertEquals(expectedVariableNames.size(), actualVariableNames.size());
        assertTrue(actualVariableNames.containsAll(expectedVariableNames));
    }

    @Test
    public void shouldUpdateAndGetSnapshotWithGeneratedFundamentalVariables() {
        FundamentalFilter filter1 = FundamentalFilter.builder()
                .accelerator("TEST").lsaCycle("super_cycle-test").timingUser("tester").build();
        FundamentalFilter filter2 = FundamentalFilter.builder()
                .accelerator("TEST").lsaCycle("super_cycle-test2").timingUser("tester").build();
        FundamentalFilter filter3 = FundamentalFilter.builder()
                .accelerator("TEST-WILL-BE-REMOVED").lsaCycle("super_cycle-test2").timingUser("tester").build();

        Set<FundamentalFilter> fundamentalFilters = Sets.newHashSet(filter1, filter2, filter3);

        Group group = createSnapshotGroup(FundamentalMapper.toGroupProperties(fundamentalFilters));
        Snapshot snapshot = fetchSnapshotBasedOn(group);

        VariableSet fundamentalVariables = snapshot.getProperties().getFundamentalFilters();
        assertEquals(fundamentalFilters.size(), fundamentalVariables.size());

        // update group properties - filters

        FundamentalFilter updatedFilter1 = filter1.toBuilder().lsaCycle("super_cycle-updated-test").build();
        Set<FundamentalFilter> updatedFundamentalFilters = Sets.newHashSet(updatedFilter1, filter2);

        Group updatedGroup = group.toBuilder().clearProperties()
                .properties(FundamentalMapper.toGroupProperties(updatedFundamentalFilters))
                .properties(getSnapshotSpecificProperties()).build();

        Snapshot updatedSnapshot = fetchSnapshotBasedOn(groupService.update(updatedGroup));
        VariableSet updatedFundamentalVariables = updatedSnapshot.getProperties().getFundamentalFilters();

        Set<String> expectedVariableNames = getExpectedVariableNamesFrom(updatedFundamentalFilters);
        Set<String> actualVariableNames = getVariableNamesFrom(updatedFundamentalVariables);

        assertEquals(expectedVariableNames.size(), actualVariableNames.size());
        assertTrue(actualVariableNames.containsAll(expectedVariableNames));

    }

    private Snapshot fetchSnapshotBasedOn(@NonNull Group group) {
        List<Snapshot> snapshots = snapshotService.getSnapshotsFor(
                new SnapshotCriteria.SnapshotCriteriaBuilder(group.getName(), group.getOwner()).withPublic().build());
        assertNotNull(snapshots);
        assertEquals(1, snapshots.size());
        return snapshotService.getSnapshotWithAttributes(snapshots.iterator().next());
    }

    private Group createSnapshotGroup(@NonNull Map<String, String> properties) {
        Group snapshotGroup = Group.builder().systemSpec(SYSTEM)
                .name("snapshot_" + UUID.randomUUID().toString())
                .label(GroupType.SNAPSHOT.toString())
                .visibility(Visibility.PUBLIC)
                .properties(properties)
                .properties(getSnapshotSpecificProperties())
                .build();
        Group persistedGroup = groupService.create(snapshotGroup);
        assertNotEquals(Identifiable.NOT_SET, persistedGroup.getId());
        return persistedGroup;
    }

    private Map<String, String> getSnapshotSpecificProperties() {
        Map<String, String> snapshotProperties = new HashMap<>();
        snapshotProperties.put("getDynamicDuration", "1");
        snapshotProperties.put("getPriorTime", "Now");
        snapshotProperties.put("getTime", "DAYS");
        snapshotProperties.put("getTimeZone", "LOCAL_TIME");
        snapshotProperties.put("isEndTimeDynamic", "true");
        return snapshotProperties;
    }

    private Set<String> getVariableNamesFrom(VariableSet fundamentalVariables) {
        return StreamSupport.stream(fundamentalVariables.spliterator(), false)
                .map(Variable::getVariableName).collect(Collectors.toSet());
    }

    private Set<String> getExpectedVariableNamesFrom(Set<FundamentalFilter> filters) {
        return filters.stream()
                .map(f -> buildFundamentalVariableName(f.getAccelerator(), f.getLsaCycle(), f.getTimingUser()))
                .collect(Collectors.toSet());
    }

    private String buildFundamentalVariableName(String accelerator, String lsaCycle, String timingUser) {
        return String.join(FUNDAMENTAL_VARIABLE_NAME_SEPARATOR, StringUtils.defaultString(accelerator, ""),
                StringUtils.defaultString(lsaCycle, ""), StringUtils.defaultString(timingUser, ""));
    }
}
