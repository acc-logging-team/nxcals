package cern.nxcals.integrationtests.backport;

import cern.nxcals.api.backport.client.service.MetaDataService;
import cern.nxcals.api.backport.client.service.ServiceBuilder;
import cern.nxcals.api.backport.domain.core.constants.VariableDataType;
import cern.nxcals.api.backport.domain.core.metadata.HierarchySet;
import cern.nxcals.api.backport.domain.core.metadata.SimpleJapcParameter;
import cern.nxcals.api.backport.domain.core.metadata.VariableList;
import cern.nxcals.api.backport.domain.core.metadata.VariableSet;
import cern.nxcals.api.config.SparkContext;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.Group;
import cern.nxcals.api.domain.Hierarchy;
import cern.nxcals.api.domain.KeyValues;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.api.domain.Visibility;
import cern.nxcals.api.exceptions.NotFoundRuntimeException;
import cern.nxcals.api.extraction.metadata.queries.Groups;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import cern.nxcals.integrationtests.service.AbstractTest;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static cern.nxcals.api.custom.domain.CmwSystemConstants.CLASS_KEY_NAME;
import static cern.nxcals.api.custom.domain.CmwSystemConstants.CMW_SYSTEM;
import static cern.nxcals.api.custom.domain.CmwSystemConstants.DEVICE_KEY_NAME;
import static cern.nxcals.api.custom.domain.CmwSystemConstants.PROPERTY_KEY_NAME;
import static cern.nxcals.api.custom.domain.GroupType.GROUP;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;

@ExtendWith(SpringExtension.class)
@TestInstance(PER_CLASS)
@SpringBootTest(classes = SparkContext.class)
@SpringBootApplication
@Slf4j
public class BackportMetaDataServiceTest extends AbstractTest {

    //    private static final String USER_NAME = System.getProperty("user.name");

    static {
        //Please leave this one just in case you want to run those test from IDE for debugging
        //Don't forget to check if hadoop-dev jar is included in build.gradle!!!

        //        System.setProperty("service.url", "https://nxcals-<user>1.cern.ch:19093".replace("<user>",USER_NAME)); //Do not use localhost here.
        //        System.setProperty("kafka.producer.bootstrap.servers", "https://nxcals-<user>3.cern.ch:9092,https://nxcals-<user>4.cern.ch:9092,https://nxcals-<user>5.cern.ch:9092".replace("<user>", USER_NAME));
        //        System.setProperty("kerberos.principal", USER_NAME);
        //        System.setProperty("kerberos.keytab", "/opt/<user>/.keytab".replace("<user>", USER_NAME));
    }

    @Autowired
    private SparkSession sparkSession;

    private MetaDataService metaService;
    private SystemSpec CMW;
    private static final String GROUP_NAME = "my-group" + UUID.randomUUID().toString();

    @BeforeAll
    public void setUp() throws InterruptedException {
        metaService = ServiceBuilder.getInstance(sparkSession).createMetaService();

        CMW = systemService.findByName(CMW_SYSTEM).orElseThrow(() -> new NotFoundRuntimeException(""));
    }

    @Test
    public void getJapcDefinitionFor() {
        Entity e1 = entity("dev1", "prop1");
        Entity e2 = entity("dev1", "prop2");
        Entity e3 = entity("dev2", "prop3");

        Variable v1 = variable("v-name-1", e1);
        Variable v2 = variable("v-name-2", e2);
        Variable v3 = variable("v-name-3", e3);
        Variable v4 = variable("v-name-4", e3);
        Variable v5 = variable("v-name-5", e3);

        Group group = group(GROUP_NAME);
        Set<Long> variableIds = Arrays.asList(v1, v2, v3, v4, v5).stream().map(v -> v.getId())
                .collect(Collectors.toSet());
        Map<String, Set<Long>> variables = new HashMap<>();
        variables.put(null, variableIds);
        internalGroupService.setVariables(group.getId(), variables);

        Collection<SimpleJapcParameter> result = metaService.getJapcDefinitionFor(VariableList.from(group));

        assertThat(result).hasSize(5);
        assertThat(result).extracting(SimpleJapcParameter::getFieldName).containsOnly("field");
        assertThat(result).extracting(SimpleJapcParameter::getPropertyName).containsOnly("prop1", "prop2", "prop3");
        assertThat(result).extracting(SimpleJapcParameter::getDeviceName).containsOnly("dev1", "dev2");
        assertThat(result).extracting(SimpleJapcParameter::getVariableName)
                .containsOnly("v-name-1", "v-name-2", "v-name-3", "v-name-4", "v-name-5");
    }

    @Test
    public void shouldGetHierarchiesForVariable() {
        Hierarchy nodeA = hierarchyService.create(Hierarchy.builder().name(getRandomString()).systemSpec(CMW).build());
        Hierarchy nodeB = hierarchyService.create(Hierarchy.builder().name(getRandomString()).systemSpec(CMW).build());
        String variableName = "v-name-1";
        Variable v1 = variableService.findOne(
                        Variables.suchThat().systemName().eq(CMW.getName()).and().variableName().eq(variableName))
                .orElseGet(() -> variableService.create(
                        Variable.builder().variableName(variableName).systemSpec(CMW).build()));

        Set<Long> variableIds = Sets.newHashSet(v1.getId());
        cern.nxcals.api.backport.domain.core.metadata.Variable backportVariable = retrieveBackportVariableByName(
                variableName);
        cern.nxcals.api.backport.domain.core.metadata.Hierarchy backportHierarchyA = cern.nxcals.api.backport.domain.core.metadata.Hierarchy
                .from(nodeA);
        cern.nxcals.api.backport.domain.core.metadata.Hierarchy backportHierarchyB = cern.nxcals.api.backport.domain.core.metadata.Hierarchy
                .from(nodeB);

        hierarchyService.addVariables(nodeA.getId(), variableIds);
        hierarchyService.addVariables(nodeB.getId(), variableIds);

        HierarchySet result = metaService.getHierarchiesForVariable(backportVariable);
        assertTrue(result.getAllHierachies().contains(backportHierarchyA));
        assertTrue(result.getAllHierachies().contains(backportHierarchyB));
    }

    private Variable variable(String varName, Entity entity) {
        return variableService.findOne(
                        Variables.suchThat().systemName().eq(CMW.getName()).and().variableName().eq(varName))
                .orElseGet(() -> createVariable(varName, entity));
    }

    private Variable createVariable(String varName, Entity entity) {
        Variable variable = Variable.builder()
                .variableName(varName)
                .systemSpec(CMW)
                .description("Description")
                .declaredType(null)
                .configs(ImmutableSortedSet.of(VariableConfig.builder()
                        .entityId(entity.getId())
                        .fieldName("field")
                        .build()))
                .build();
        return variableService.create(variable);
    }

    private Entity entity(String device, String property) {
        return internalEntityService.findOrCreateEntityFor(CMW.getId(),
                new KeyValues(0, ImmutableMap.of(DEVICE_KEY_NAME, device, PROPERTY_KEY_NAME, property)),
                new KeyValues(0, ImmutableMap.of(CLASS_KEY_NAME, device, PROPERTY_KEY_NAME, property)),
                "brokenSchema1",
                1476789831111222334L);
    }

    private Group group(String name) {
        return internalGroupService.findOne(Groups.suchThat().systemName().eq(CMW.getName()).and().name().eq(name))
                .orElseGet(() -> createGroup(name));
    }

    private Group createGroup(String name) {
        Group group = Group.builder().label(GROUP.toString()).visibility(Visibility.PUBLIC).name(name).systemSpec(CMW)
                .build();
        return internalGroupService.create(group);
    }

    private cern.nxcals.api.backport.domain.core.metadata.Variable retrieveBackportVariableByName(String variableName) {
        VariableSet variableSet = this.metaService
                .getVariablesOfDataTypeWithNameLikePattern(variableName, VariableDataType.ALL);

        assertEquals(1, variableSet.size());

        return variableSet.getVariable(variableName);
    }
}
