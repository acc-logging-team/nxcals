package cern.nxcals.integrationtests.spark;

import cern.nxcals.api.config.AuthenticationContext;
import cern.nxcals.api.config.SparkProperties;
import cern.nxcals.api.custom.config.SparkSessionFlavor;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.utils.SparkUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

@SpringBootTest(classes = AuthenticationContext.class)
@SpringBootApplication
@Slf4j
public class SparkYarnSessionTest {

    @Test
    public void shouldCreateSparkSession() {
        SparkProperties properties = SparkProperties.remoteSessionProperties("testApp", SparkSessionFlavor.SMALL,
                "test");
        properties.setProperty("spark.kerberos.principal", System.getProperty("kerberos.principal"));
        properties.setProperty("spark.kerberos.keytab", System.getProperty("kerberos.keytab"));
        SparkSession sparkSession = SparkUtils.createSparkSession(properties);
        DataQuery.builder(sparkSession).byEntities().system("MOCK-SYSTEM")
                .startTime(Instant.now().minus(1, ChronoUnit.HOURS))
                .endTime(Instant.now())
                .entity().keyValueLike("device", "NXCALS_MONITORING_DEV").build();
    }
}
