package cern.nxcals.integrationtests.ingestion;

import cern.cmw.datax.DataBuilder;
import cern.cmw.datax.ImmutableData;
import cern.nxcals.api.config.SparkContext;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.EntityHistory;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.extraction.metadata.queries.Entities;
import cern.nxcals.api.ingestion.Publisher;
import cern.nxcals.api.ingestion.PublisherFactory;
import cern.nxcals.api.ingestion.Result;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.common.SystemFields;
import cern.nxcals.integrationtests.ServiceProvider;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.DependsOn;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;

import static cern.cmw.datax.EntryType.INT64;
import static cern.nxcals.api.domain.TimeWindow.between;
import static java.time.Duration.ofSeconds;
import static java.time.temporal.ChronoUnit.DAYS;
import static java.util.Collections.singletonMap;
import static java.util.Comparator.comparing;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = SparkContext.class)
@SpringBootApplication
@Slf4j
@DependsOn("kerberos")
public class BufferedPublisherTest extends ServiceProvider {
    //    static {
    //        String username = System.getProperty("user.name");
    //        System.setProperty("kerberos.principal", username);
    //        System.setProperty("kerberos.keytab", "/opt/<user>/.keytab".replace("<user>", username));
    //        System.setProperty("service.url",
    //                "https://nxcals-<user>1.cern.ch:19093".replace("<user>", username)); //Do not use localhost here.
    //        System.setProperty("kafka.producer.bootstrap.servers",
    //                "https://nxcals-<user>3.cern.ch:9092,https://nxcals-<user>4.cern.ch:9092,https://nxcals-<user>5.cern.ch:9092".replace(
    //                        "<user>", username));
    //    }

    @Autowired
    private SparkSession sparkSession;
    private static final String SYSTEM_NAME = "MOCK-SYSTEM";
    private static final String TIMESTAMP = "timestamp";
    private static final String ENTITY_KEY = "device";
    private static final String PARTITION_KEY = "specification";

    private final Publisher<ImmutableData> publisher = PublisherFactory.newInstance()
            .createBufferedPublisher(SYSTEM_NAME, Function.identity(), ofSeconds(1), ofSeconds(1));
    private static final ExecutorService EXECUTOR = Executors.newFixedThreadPool(10);
    private static final String FIELD_PREFIX = "f_";
    private static final String FIELD_ZERO = FIELD_PREFIX + "0";
    private static final String PARTITION_PREFIX = "p_";
    private static final String PARTITION_ZERO = PARTITION_PREFIX + "0";
    private static final Instant yesterday = Instant.now().minus(1, DAYS);
    private static final int MAX_WAITING_TIME_IN_SECONDS = 60;

    @AfterAll
    public static void afterClass() throws Exception {
        EXECUTOR.shutdown();
    }

    @Test
    public void shouldPreserveHistoryForChangingSchema() {
        shouldPreserveHistoryWith(mmd -> createMessages(mmd.getCount(), mmd.getEntityName(),
                i -> PARTITION_PREFIX + i / mmd.pointsPerLogicalPartition(), i -> FIELD_ZERO,
                mmd.getFirstHistoryTime()::plusSeconds));
    }

    @Test
    public void shouldPreserveHistoryForChangingPartition() {
        shouldPreserveHistoryWith(mmd -> createMessages(mmd.getCount(), mmd.getEntityName(), i -> PARTITION_ZERO,
                i -> FIELD_PREFIX + i / mmd.pointsPerLogicalPartition(), mmd.getFirstHistoryTime()::plusSeconds));
    }

    @Test
    public void shouldPreserveHistoryForChangingSchemaAndPartition() {
        shouldPreserveHistoryWith(mmd -> createMessages(mmd.getCount(), mmd.getEntityName(),
                i -> PARTITION_PREFIX + i / mmd.pointsPerLogicalPartition(),
                i -> FIELD_PREFIX + i / mmd.pointsPerLogicalPartition(), mmd.getFirstHistoryTime()::plusSeconds));
    }

    @Test
    public void shouldCorrectlyPublishUsingEntityAndPartitionId() throws InterruptedException {
        // create entity
        String entityName = initEntity();

        // make sure it was properly created
        Entity entity = findWithHistory(entityName, yesterday.minusSeconds(1), yesterday);
        assertNotNull(entity);
        assertEquals(between(yesterday, null), entity.getFirstEntityHistory().getValidity());

        ImmutableData nextRecord = dataWithIds(entity, TimeUtils.getNanosFromInstant(yesterday) + 1);
        this.publisher.publish(nextRecord);

        // wait for data to be visible
        long recordCount = 0;
        long desiredRecordCount = 2;
        for (int i = 0; i < MAX_WAITING_TIME_IN_SECONDS && recordCount < desiredRecordCount; i++) {
            Dataset<Row> dataset = DataQuery.builder(sparkSession).entities().system(SYSTEM_NAME).idEq(entity.getId())
                    .timeWindow(yesterday.minusSeconds(1), yesterday.plusSeconds(1)).build();
            recordCount = dataset.count();
            Thread.sleep(1000);
        }

        assertEquals(desiredRecordCount, recordCount);
    }

    private String initEntity() {
        String entityName = UUID.randomUUID().toString();
        ImmutableData data = data(entityName, PARTITION_ZERO, FIELD_ZERO, TimeUtils.getNanosFromInstant(yesterday));
        this.publisher.publish(data);
        return entityName;
    }

    private ImmutableData dataWithIds(Entity entity, long timestamp) {
        return ImmutableData.builder()
                .add(SystemFields.NXC_ENTITY_ID.getValue(), entity.getId())
                .add(SystemFields.NXC_PARTITION_ID.getValue(), entity.getPartition().getId())
                .add(ENTITY_KEY, "Wrong-Key")
                .add(PARTITION_KEY, "Wrong-part")
                .add(FIELD_ZERO, 0L)
                .add(TIMESTAMP, timestamp)
                .build();
    }

    private void shouldPreserveHistoryWith(Function<MessageMakerData, List<ImmutableData>> messagesMaker) {
        // given
        // create entity
        String entityName = initEntity();
        // make sure it was properly created
        Entity entity = findWithHistory(entityName, yesterday.minusSeconds(1), yesterday);
        assertNotNull(entity);
        assertEquals(between(yesterday, null), entity.getFirstEntityHistory().getValidity());

        // create a bunch of messages
        MessageMakerData mmd = new MessageMakerData(12, entityName, yesterday, 3);
        List<ImmutableData> messages = messagesMaker.apply(mmd);
        messages.sort(comparing(this::byTimestamp).reversed());
        // when
        // send and wait
        CompletableFuture.allOf(messages.stream().map(msg -> this.publisher.publishAsync(msg, EXECUTOR))
                .map(this::assertPublished).toArray(CompletableFuture[]::new)).join();

        // then
        entity = findWithHistory(entityName, mmd.getFirstHistoryTime(), Instant.now());
        verifyHistory(entity, mmd);
    }

    private Long byTimestamp(ImmutableData d) {
        return d.getEntry(TIMESTAMP).getAs(INT64);
    }

    private void verifyHistory(Entity entity, MessageMakerData mmd) {
        assertEquals(mmd.getLogicalPartitionCount(), entity.getEntityHistory().size());
        Instant fhT = mmd.getFirstHistoryTime();
        // histories start from the youngest
        int index = entity.getEntityHistory().size() - 1;
        for (EntityHistory eh : entity.getEntityHistory()) {
            if (eh.getValidity().isRightInfinite()) {
                assertEquals(between(fhT.plusSeconds(index * mmd.pointsPerLogicalPartition()), null), eh.getValidity());
            } else {
                assertEquals(between(fhT.plusSeconds(index * mmd.pointsPerLogicalPartition()),
                        fhT.plusSeconds((index + 1) * mmd.pointsPerLogicalPartition())), eh.getValidity());
            }
            index--;
        }
    }

    private CompletableFuture<Result> assertPublished(CompletableFuture<Result> result) {
        return result.whenCompleteAsync((o, e) -> assertNotNull(o), EXECUTOR);
    }

    private Entity findWithHistory(String entityName, Instant from, Instant to) {
        return internalEntityService
                .findOneWithHistory(conditionFor(entityName), TimeUtils.getNanosFromInstant(from),
                        TimeUtils.getNanosFromInstant(to))
                .orElseThrow(() -> new IllegalStateException("must not happen"));
    }

    private SystemSpec getSpec(String systemName) {
        return internalSystemService.findByName(systemName)
                .orElseThrow(() -> new IllegalStateException("No system " + "found for " + systemName));
    }

    private ImmutableData data(String eKey, String partitionKey, String field, long stampInNanos) {
        DataBuilder builder = ImmutableData.builder();
        builder.add(ENTITY_KEY, eKey);
        builder.add(PARTITION_KEY, partitionKey);
        builder.add(TIMESTAMP, stampInNanos);
        builder.add(field, 1L);
        return builder.build();
    }

    private Condition<Entities> conditionFor(String entityName) {
        return Entities.suchThat().systemName().eq(SYSTEM_NAME).and().keyValues()
                .eq(getSpec(SYSTEM_NAME), singletonMap("device", entityName));
    }

    private List<ImmutableData> createMessages(int count, String entityName, Function<Integer, String> partitionMaker,
            Function<Integer, String> schemaMaker, Function<Integer, Instant> timeMaker) {
        LinkedList<ImmutableData> messages = new LinkedList<>();
        for (int i = 0; i < count; i++) {
            ImmutableData msg = data(entityName, partitionMaker.apply(i), schemaMaker.apply(i),
                    TimeUtils.getNanosFromInstant(timeMaker.apply(i)));
            messages.add(msg);
        }
        return messages;
    }

    @Data
    private static class MessageMakerData {
        private final int count;
        private final String entityName;
        private final Instant firstHistoryTime;
        private final int logicalPartitionCount;

        public int pointsPerLogicalPartition() {
            return count / logicalPartitionCount;
        }
    }
}
