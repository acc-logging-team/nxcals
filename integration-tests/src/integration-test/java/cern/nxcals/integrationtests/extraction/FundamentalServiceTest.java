package cern.nxcals.integrationtests.extraction;

import cern.cmw.datax.ImmutableData;
import cern.nxcals.api.config.SparkContext;
import cern.nxcals.api.custom.domain.FundamentalFilter;
import cern.nxcals.api.custom.service.FundamentalService;
import cern.nxcals.api.custom.service.Services;
import cern.nxcals.api.custom.service.fundamental.FundamentalContext;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.api.domain.VariableDeclaredType;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.extraction.metadata.EntityService;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.extraction.metadata.queries.Entities;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.integrationtests.testutils.ExtractionTestHelper;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.DependsOn;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import static cern.nxcals.api.backport.domain.core.metadata.FundamentalData.TGM_LSA_CYCLE_FIELD;
import static cern.nxcals.api.backport.domain.core.metadata.FundamentalData.XTIM_DESTINATION_FIELD;
import static cern.nxcals.api.backport.domain.core.metadata.FundamentalData.XTIM_LSA_CYCLE_FIELD;
import static cern.nxcals.api.custom.domain.CmwSystemConstants.DEVICE_KEY_NAME;
import static cern.nxcals.api.custom.domain.CmwSystemConstants.PROPERTY_KEY_NAME;
import static cern.nxcals.api.custom.service.fundamental.FundamentalContext.DESTINATION_FIELD;
import static cern.nxcals.api.custom.service.fundamental.FundamentalContext.USER_FIELD;
import static cern.nxcals.api.custom.service.fundamental.FundamentalContext.VIRTUAL_LSA_CYCLE_FIELD;
import static cern.nxcals.common.SystemFields.NXC_EXTR_ENTITY_ID;
import static cern.nxcals.common.SystemFields.NXC_EXTR_TIMESTAMP;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VARIABLE_NAME;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.CLASS_KEY;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.DEVICE_KEY;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.PROPERTY_KEY;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.RECORD_TIMESTAMP_KEY;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.RECORD_VERSION_KEY;
import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Integration tests for {@link cern.nxcals.api.custom.service.FundamentalService}
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = SparkContext.class)
@SpringBootApplication
@Slf4j
@DependsOn("kerberos")
public class FundamentalServiceTest {
    static {
        //        Please leave this one just in case you want to run those test from IDE for debugging
        //        Don't forget to check if hadoop-dev jar is included in build.gradle!!!

        //        String username = System.getProperty("user.name");
        //        System.setProperty("kerberos.principal", username);
        //        System.setProperty("kerberos.keytab", "/opt/" + username + "/.keytab");
        //        System.setProperty("service.url",
        //                "https://nxcals-<user>-1.cern.ch:19093".replace("<user>", username)); //Do not use localhost here.
        //        System.setProperty("kafka.producer.bootstrap.servers",
        //                "https://nxcals-<user>-3.cern.ch:9092,https://nxcals-<user>-4.cern.ch:9092,https://nxcals-<user>-5.cern.ch:9092"
        //                        .replace("<user>", username));
    }

    private static final long TOTAL_NUM_OF_PRODUCED_DATA = 6 * 2; // 2 fundamentals, each has 6 data point

    private static final VariableService VARIABLE_SERVICE = ServiceClientFactory.createVariableService();
    private static final EntityService ENTITY_SERVICE = ServiceClientFactory.createEntityService();
    private static final SystemSpecService SYSTEM_SERVICE = ServiceClientFactory.createSystemSpecService();

    public static final SystemSpec FUNDAMENTAL_SYSTEM_SPEC = SYSTEM_SERVICE.findByName(FundamentalContext.SYSTEM)
            .orElseThrow(() -> new IllegalArgumentException("No such system"));
    private static final Instant DATA_PRODUCE_START = Instant.now();
    private static final Instant DATA_PRODUCE_END = DATA_PRODUCE_START.plusSeconds(6);

    private static final int MAX_WAIT_FOR_DATA_MINUTES = 2;
    private static final AtomicBoolean DATA_VISIBLE = new AtomicBoolean(false);
    private static final AtomicBoolean WAITING_FOR_DATA_FAILED = new AtomicBoolean(false);

    private static final String RANDOM_SUFFIX = UUID.randomUUID().toString();

    private static final String FUNDAMENTAL_DEVICE_NAME_1 = "FUNDAMENTAL.DEVICE_1_" + RANDOM_SUFFIX;
    private static final String FUNDAMENTAL_DEVICE_NAME_2 = "FUNDAMENTAL.DEVICE_2_" + RANDOM_SUFFIX;
    private static final String FUNDAMENTAL_PROPERTY_NAME = "FUNDAMENTAL_PROPERTY";
    private static final String FUNDAMENTAL_CLASS_NAME = "TEST_CLASS";

    private static final Map<String, Object> FUNDAMENTAL_ENTITY_KEYS_1 = ImmutableMap
            .of(DEVICE_KEY_NAME, FUNDAMENTAL_DEVICE_NAME_1, PROPERTY_KEY_NAME, FUNDAMENTAL_PROPERTY_NAME);
    private static final Map<String, Object> FUNDAMENTAL_ENTITY_KEYS_2 = ImmutableMap
            .of(DEVICE_KEY_NAME, FUNDAMENTAL_DEVICE_NAME_2, PROPERTY_KEY_NAME, FUNDAMENTAL_PROPERTY_NAME);

    private static final String ACCELERATOR_1 = "ACCELERATOR_1_" + RANDOM_SUFFIX;
    private static final String ACCELERATOR_2 = "ACCELERATOR_2_" + RANDOM_SUFFIX;
    private static final String FUNDAMENTAL_VARIABLE_NAME_1 =
            ACCELERATOR_1.toUpperCase() + FundamentalContext.VARIABLE_NAME_SUFFIX;
    private static final String FUNDAMENTAL_VARIABLE_NAME_2 =
            ACCELERATOR_2.toUpperCase() + FundamentalContext.VARIABLE_NAME_SUFFIX;

    @Autowired
    private SparkSession sparkSession;
    private FundamentalService fundamentalService;

    @BeforeAll
    public static void setUpData() {
        List<ImmutableData> dataToPublish = generateFundamentalData();
        log.info("Sending fundamental data to NXCALS");
        ExtractionTestHelper.publishData(FUNDAMENTAL_SYSTEM_SPEC.getName(), dataToPublish);

        registerFundamentalVariable(FUNDAMENTAL_VARIABLE_NAME_1, FUNDAMENTAL_ENTITY_KEYS_1);
        registerFundamentalVariable(FUNDAMENTAL_VARIABLE_NAME_2, FUNDAMENTAL_ENTITY_KEYS_2);
    }

    private static List<ImmutableData> generateFundamentalData() {
        List<ImmutableData> dataPoints = new ArrayList<>();
        dataPoints.addAll(generateDifferentFundamentalData(FUNDAMENTAL_DEVICE_NAME_1));
        dataPoints.addAll(generateDifferentFundamentalData(FUNDAMENTAL_DEVICE_NAME_2));
        return dataPoints;
    }

    private static List<ImmutableData> generateDifferentFundamentalData(String device) {
        Instant start = DATA_PRODUCE_START;
        List<ImmutableData> dataPoints = new ArrayList<>();

        dataPoints.add(buildDataWithOldFields(start.plusSeconds(1), device, "TEST_CYCLE_1", "TEST_USER_1",
                "TEST_DEST_1"));
        dataPoints.add(buildDataWithOldFields(start.plusSeconds(2), device, "TEST_CYCLE_1", "TEST_USER_2",
                "TEST_DEST_2"));
        dataPoints.add(buildDataWithOldFields(start.plusSeconds(3), device, "TEST_CYCLE_3", "TEST_USER_3",
                "TEST_DEST_3"));

        dataPoints.add(buildDataWithNewFields(start.plusSeconds(4), device, "TEST_CYCLE_1", "TEST_USER_1",
                "TEST_DEST_1"));
        dataPoints.add(buildDataWithNewFields(start.plusSeconds(5), device, "TEST_CYCLE_1", "TEST_USER_2",
                "TEST_DEST_2"));
        dataPoints.add(buildDataWithNewFields(start.plusSeconds(6), device, "TEST_CYCLE_3", "TEST_USER_3",
                "TEST_DEST_3"));

        return dataPoints;
    }

    private static ImmutableData buildDataWithOldFields(Instant timestamp, String device, String cycle, String user,
            String destination) {
        return ImmutableData.builder()
                .add(DEVICE_KEY, device)
                .add(PROPERTY_KEY, FUNDAMENTAL_PROPERTY_NAME)
                .add(CLASS_KEY, FUNDAMENTAL_CLASS_NAME)
                .add(RECORD_VERSION_KEY, 0L)
                .add(RECORD_TIMESTAMP_KEY, TimeUtils.getNanosFromInstant(timestamp))
                .add(TGM_LSA_CYCLE_FIELD, cycle)
                .add(USER_FIELD, user)
                .add(DESTINATION_FIELD, destination)
                .build();
    }

    private static ImmutableData buildDataWithNewFields(Instant timestamp, String device, String cycle, String user,
            String destination) {
        return ImmutableData.builder()
                .add(DEVICE_KEY, device)
                .add(PROPERTY_KEY, FUNDAMENTAL_PROPERTY_NAME)
                .add(CLASS_KEY, FUNDAMENTAL_CLASS_NAME)
                .add(RECORD_VERSION_KEY, 0L)
                .add(RECORD_TIMESTAMP_KEY, TimeUtils.getNanosFromInstant(timestamp))
                .add(XTIM_LSA_CYCLE_FIELD, cycle)
                .add(USER_FIELD, user)
                .add(XTIM_DESTINATION_FIELD, destination)
                .build();
    }

    public static void registerFundamentalVariable(String variableName,
            Map<String, Object> entityKeyValues) {
        VARIABLE_SERVICE.findOne(Variables.suchThat().systemName().eq(FUNDAMENTAL_SYSTEM_SPEC.getName()).and()
                .variableName().eq(variableName)).ifPresent(v -> {
            throw new IllegalArgumentException("Variable with this name already exists");
        });
        Entity entity = findEntityOrThrow(entityKeyValues);
        Variable variable = VARIABLE_SERVICE
                .create(buildFundamentalVariable(FUNDAMENTAL_SYSTEM_SPEC, entity, variableName));
        assertNotNull(variable);
    }

    private static Variable buildFundamentalVariable(SystemSpec system, Entity entity, String variableName) {
        VariableConfig config = VariableConfig.builder()
                .entityId(entity.getId())
                .variableName(variableName)
                .build();
        return Variable.builder().systemSpec(system).variableName(variableName)
                .declaredType(VariableDeclaredType.FUNDAMENTAL).configs(ImmutableSortedSet.of(config)).build();
    }

    private static Entity findEntityOrThrow(Map<String, Object> keyValues) {
        return ENTITY_SERVICE.findOne(
                        Entities.suchThat().keyValues().eq(FUNDAMENTAL_SYSTEM_SPEC, keyValues))
                .orElseThrow(() -> new IllegalArgumentException(
                        format("Could not find entity [ %s ]! Make sure that data were sent and metadata created for this target",
                                keyValues.get(DEVICE_KEY_NAME))));
    }

    @BeforeEach
    public void waitForData() throws InterruptedException {
        this.fundamentalService = Services.newInstance(sparkSession).fundamentalService();

        if (!DATA_VISIBLE.get()) {
            //Have to wait 30 seconds in order for the data to be processed and visible in NXCALS extraction
            log.info("Waiting max {} minutes for the data to be visible in NXCALS...", MAX_WAIT_FOR_DATA_MINUTES);
            waitForData(TimeUnit.MINUTES.toMillis(MAX_WAIT_FOR_DATA_MINUTES));
            log.info("Ok, data should be processed now, proceeding with tests");
            DATA_VISIBLE.set(true);
        } else {
            log.info("Data already checked");
        }
    }

    private synchronized void waitForData(long maxTime) throws InterruptedException {
        long start = System.currentTimeMillis();
        while (true) {
            if (WAITING_FOR_DATA_FAILED.get() || (System.currentTimeMillis() - start > maxTime)) {
                WAITING_FOR_DATA_FAILED.set(true);
                fail("Data was not processed in the required maxTime=" + maxTime + " ms");
            }
            if (getDataCount() == TOTAL_NUM_OF_PRODUCED_DATA) {
                return;
            }
            log.info("Still no data visible, waiting more...");
            TimeUnit.SECONDS.sleep(4);
        }
    }

    private long getDataCount() {
        long total = getDataCount(FUNDAMENTAL_DEVICE_NAME_1) +
                getDataCount(FUNDAMENTAL_DEVICE_NAME_2);
        log.info("Last checked data count: " + total);
        return total;
    }

    private long getDataCount(String device) {
        return DataQuery.builder(sparkSession).byEntities().system(FUNDAMENTAL_SYSTEM_SPEC.getName())
                .startTime(DATA_PRODUCE_START)
                .endTime(DATA_PRODUCE_END)
                .entity()
                .keyValue(DEVICE_KEY, device)
                .keyValue(PROPERTY_KEY, FUNDAMENTAL_PROPERTY_NAME)
                .build().orderBy("__record_timestamp__").count();
    }

    // test cases

    @Test
    public void shouldGetFundamentalDataForOneVariableFilter() {
        //given
        FundamentalFilter acceleratorFilter = FundamentalFilter.builder().accelerator(ACCELERATOR_1).build();
        Set<FundamentalFilter> filters = Set.of(acceleratorFilter);

        Set<String> expectedVariables = Set.of(FUNDAMENTAL_VARIABLE_NAME_1);
        List<Instant> expectedInstants = convertToInstants(List.of(1, 2, 3, 4, 5, 6));

        //when
        Dataset<Row> result = fundamentalService
                .getAll(TimeWindow.between(DATA_PRODUCE_START, DATA_PRODUCE_END), filters)
                .sort(NXC_EXTR_VARIABLE_NAME.getValue(), NXC_EXTR_TIMESTAMP.getValue());

        //then
        verifyResult(expectedVariables, expectedInstants, result);
    }

    @Test
    public void shouldGetFundamentalDataForOneVariableFilterWithWildcard() {
        //given
        FundamentalFilter acceleratorFilter = FundamentalFilter.builder().accelerator(ACCELERATOR_1)
                .lsaCycle("TEST_CYCLE_1").timingUser("TEST_USER_%").build();
        Set<FundamentalFilter> filters = Set.of(acceleratorFilter);

        Set<String> expectedVariables = Set.of(FUNDAMENTAL_VARIABLE_NAME_1);
        List<Instant> expectedInstants = convertToInstants(List.of(1, 2, 4, 5));

        //when
        Dataset<Row> result = fundamentalService
                .getAll(TimeWindow.between(DATA_PRODUCE_START, DATA_PRODUCE_END), filters)
                .sort(NXC_EXTR_VARIABLE_NAME.getValue(), NXC_EXTR_TIMESTAMP.getValue());

        //then
        verifyResult(expectedVariables, expectedInstants, result);
    }

    @Test
    public void shouldGetFundamentalDataForTwoVariableFilters() {
        //given
        FundamentalFilter acceleratorFilter1 = FundamentalFilter.builder().accelerator(ACCELERATOR_1).build();
        FundamentalFilter acceleratorFilter2 = FundamentalFilter.builder().accelerator(ACCELERATOR_2).build();
        Set<FundamentalFilter> filters = Set.of(acceleratorFilter1, acceleratorFilter2);

        Set<String> expectedVariables = Set.of(FUNDAMENTAL_VARIABLE_NAME_1, FUNDAMENTAL_VARIABLE_NAME_2);
        List<Instant> expectedInstants = convertToInstants(
                List.of(1, 2, 3, 4, 5, 6, 1, 2, 3, 4, 5, 6)); // 2 variables combined

        //when
        Dataset<Row> result = fundamentalService
                .getAll(TimeWindow.between(DATA_PRODUCE_START, DATA_PRODUCE_END), filters)
                .sort(NXC_EXTR_VARIABLE_NAME.getValue(), NXC_EXTR_TIMESTAMP.getValue());

        //then
        verifyResult(expectedVariables, expectedInstants, result);
    }

    @Test
    public void shouldGetFundamentalDataForTwoSpecificFilters() {
        //given
        FundamentalFilter acceleratorFilter1 = FundamentalFilter.builder().accelerator(ACCELERATOR_1)
                .lsaCycle("TEST_CYCLE_1").timingUser("TEST_USER_2").destination("TEST_DEST_2").build(); // row 2 and 5
        FundamentalFilter acceleratorFilter2 = FundamentalFilter.builder().accelerator(ACCELERATOR_2)
                .lsaCycle("TEST_CYCLE_3").timingUser("TEST_USER_3").destination("TEST_DEST_3").build(); // row 3 and 6
        Set<FundamentalFilter> filters = Set.of(acceleratorFilter1, acceleratorFilter2);

        Set<String> expectedVariables = Set.of(FUNDAMENTAL_VARIABLE_NAME_1, FUNDAMENTAL_VARIABLE_NAME_2);
        List<Instant> expectedInstants = convertToInstants(List.of(2, 5, 3, 6));

        //when
        Dataset<Row> result = fundamentalService
                .getAll(TimeWindow.between(DATA_PRODUCE_START, DATA_PRODUCE_END), filters)
                .sort(NXC_EXTR_VARIABLE_NAME.getValue(), NXC_EXTR_TIMESTAMP.getValue());

        //then
        verifyResult(expectedVariables, expectedInstants, result);
    }

    @Test
    public void shouldGetFundamentalDataForSmallerTimeWindowAndSpecificFilter() {
        //given
        FundamentalFilter acceleratorFilter1 = FundamentalFilter.builder().accelerator(ACCELERATOR_1)
                .lsaCycle("TEST_CYCLE_1").timingUser("TEST_USER_2").destination("TEST_DEST_2")
                .build(); // 2 (filtered out by tw) and 5
        FundamentalFilter acceleratorFilter2 = FundamentalFilter.builder().accelerator(ACCELERATOR_2)
                .lsaCycle("TEST_CYCLE_3").timingUser("TEST_USER_3").destination("TEST_DEST_3")
                .build(); // 3 and 6 ((filtered out by tw))

        Set<FundamentalFilter> filters = Set.of(acceleratorFilter1, acceleratorFilter2);

        Set<String> expectedVariables = Set.of(FUNDAMENTAL_VARIABLE_NAME_1, FUNDAMENTAL_VARIABLE_NAME_2);
        List<Instant> expectedInstants = convertToInstants(List.of(5, 3));

        //when
        Dataset<Row> result = fundamentalService
                .getAll(TimeWindow.between(DATA_PRODUCE_START.plusSeconds(3), DATA_PRODUCE_START.plusSeconds(5)),
                        filters)
                .sort(NXC_EXTR_VARIABLE_NAME.getValue(), NXC_EXTR_TIMESTAMP.getValue());

        //then
        verifyResult(expectedVariables, expectedInstants, result);
    }

    @Test
    public void shouldReturnEmptyFundamentalDatasetIfFilterDoesNotMatchData() {
        //given
        FundamentalFilter acceleratorFilter1 = FundamentalFilter.builder().accelerator(ACCELERATOR_1)
                .lsaCycle("TEST_CYCLE_WRONG").timingUser("TEST_USER_2").destination("TEST_DEST_2")
                .build();

        Set<FundamentalFilter> filters = Set.of(acceleratorFilter1);

        Set<String> expectedVariables = Collections.emptySet();
        List<Instant> expectedInstants = Collections.emptyList();

        //when
        Dataset<Row> result = fundamentalService
                .getAll(TimeWindow.between(DATA_PRODUCE_START, DATA_PRODUCE_END), filters)
                .sort(NXC_EXTR_VARIABLE_NAME.getValue(), NXC_EXTR_TIMESTAMP.getValue());

        //then
        verifyResult(expectedVariables, expectedInstants, result);
    }

    @Test
    public void shouldGetFundamentalDataForVariables() {
        //given
        TimeWindow timeWindow = TimeWindow.between(DATA_PRODUCE_START, DATA_PRODUCE_END);
        List<String> accelerators = List.of(ACCELERATOR_2, ACCELERATOR_1);

        //when
        Dataset<Row> result = fundamentalService.getFor(timeWindow, accelerators);

        //then
        assertEquals(getExpectedColumns(), new HashSet<>(List.of(result.columns())));
    }

    private void verifyResult(Set<String> expectedVariables,
            List<Instant> expectedInstants, Dataset<Row> result) {

        assertEquals(expectedInstants.size(), result.count());

        Set<String> datasetColumns = Sets.newHashSet(result.columns());
        assertEquals(getExpectedColumns(), datasetColumns);

        Set<String> variableNames = result.select(NXC_EXTR_VARIABLE_NAME.getValue()).collectAsList().stream()
                .map(r -> r.getString(0)).collect(Collectors.toSet());
        assertEquals(expectedVariables, variableNames);

        List<Instant> instants = result.select(NXC_EXTR_TIMESTAMP.getValue()).collectAsList().stream()
                .map(r -> TimeUtils.getInstantFromNanos(r.getLong(0))).collect(Collectors.toList());
        assertEquals(expectedInstants, instants);
    }

    private List<Instant> convertToInstants(List<Integer> addedSeconds) {
        return addedSeconds.stream().map(DATA_PRODUCE_START::plusSeconds).collect(Collectors.toList());
    }

    private Set<String> getExpectedColumns() {
        return Sets.newHashSet(
                NXC_EXTR_ENTITY_ID.getValue(),
                DEVICE_KEY,
                PROPERTY_KEY,
                CLASS_KEY,
                RECORD_VERSION_KEY,
                NXC_EXTR_TIMESTAMP.getValue(),
                NXC_EXTR_VARIABLE_NAME.getValue(),
                VIRTUAL_LSA_CYCLE_FIELD,
                USER_FIELD,
                DESTINATION_FIELD);
    }

}
