/*
 * Copyright (c) 2024 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.integrationtests.extraction.custom;

import cern.nxcals.api.custom.domain.FundamentalFilter;
import cern.nxcals.api.custom.domain.Snapshot;
import cern.nxcals.api.custom.domain.constants.BeamModeData;
import cern.nxcals.api.custom.domain.constants.BeamModeValidity;
import cern.nxcals.api.custom.domain.constants.DynamicTimeUnit;
import cern.nxcals.api.custom.domain.constants.LoggingTimeZone;
import cern.nxcals.api.custom.domain.snapshot.PriorTime;
import cern.nxcals.api.custom.extraction.metadata.SnapshotService;
import cern.nxcals.api.custom.extraction.metadata.queries.Snapshots;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.Visibility;
import cern.nxcals.integrationtests.service.AbstractTest;
import com.google.common.collect.ImmutableSet;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static cern.nxcals.integrationtests.utils.ServicesHelper.SNAPSHOT_SERVICE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

public class SnapshotServiceTest extends AbstractTest {
    private static final String USER = System.getenv("USER");

    private static final String VAR_NAME = "TEST_VARIABLE";
    private static final String X_AXIS_VAR_NAME = "TEST_X_AXIS_VARIABLE";
    private static final String FUND_FILTERS_VAR_NAME = "TEST_FUND_FILTERS_VARIABLE";
    private static final String DRIVING_VAR_NAME = "TEST_DRIVING_VARIABLE";

    private static final String SNAPSHOT_NAME = "TEST_SNAPSHOT_NAME";
    private static final String SNAPSHOT_DESCRIPTION = "TEST_SNAPSHOT_DESCRIPTION" + Instant.now().toEpochMilli();
    private static final String SNAPSHOT_SYSTEM = "CMW";
    private static final String SNAPSHOT_OWNER = USER;
    private static final Visibility SNAPSHOT_VISIBILITY = Visibility.PUBLIC;
    private static final String SNAPSHOT_START_TIME = "2023-05-15T20:17:30.151238525Z";
    private static final String SNAPSHOT_END_TIME = "2023-05-16T05:15:03.533738525Z";

    private static final String ENTITY_NAME_PREFIX = "SNAPSHOTS";

    private final Entity entity = entity(-100, ENTITY_NAME_PREFIX);
    private final ImmutableSet<Entity> entities = ImmutableSet.of(entity);
    private final ImmutableSet<Variable> snapshotVariables = ImmutableSet.of(variable(entity, VAR_NAME));
    private final ImmutableSet<Variable> xAxisVariables = ImmutableSet.of(variable(entity, X_AXIS_VAR_NAME));
    private final ImmutableSet<Variable> fundamentalFiltersVariables = ImmutableSet.of(
            variable(entity, FUND_FILTERS_VAR_NAME));
    private final ImmutableSet<Variable> drivingVariables = ImmutableSet.of(variable(entity, DRIVING_VAR_NAME));

    @Test
    public void shouldCreateDynamicRangeSnapshot() {
        String name = getRandomName(SNAPSHOT_NAME);
        Snapshot snapshot = createDynamicRangeSnapshot(name);

        try {
            assertNotNull(snapshot);
            assertEquals(name, snapshot.getName());
        } finally {
            SNAPSHOT_SERVICE.delete(snapshot.getId());
        }
    }

    @Test
    public void shouldCreateFillRangeSnapshot() {
        String name = getRandomName(SNAPSHOT_NAME);
        Snapshot snapshot = createFillRangeSnapshot(name);

        try {
            assertNotNull(snapshot);
            assertEquals(name, snapshot.getName());
            assertTimeWindow(snapshot.calculateTimeWindow());
        } finally {
            SNAPSHOT_SERVICE.delete(snapshot.getId());
        }
    }

    @Test
    public void shouldCreateFixedRangeSnapshot() {
        String name = getRandomName(SNAPSHOT_NAME);
        Snapshot snapshot = createFixedRangeSnapshot(name);
        try {
            assertNotNull(snapshot);
            assertEquals(name, snapshot.getName());
            assertTimeWindow(snapshot.calculateTimeWindow());
        } finally {
            SNAPSHOT_SERVICE.delete(snapshot.getId());
        }
    }

    @Test
    public void shouldFindAndUpdateSnapshot() {
        String name = getRandomName(SNAPSHOT_NAME);
        createFillRangeSnapshot(name);

        //Find a SNAPSHOT
        Snapshot snapshot = SNAPSHOT_SERVICE.findOne(Snapshots.suchThat().name().eq(name))
                .orElseThrow(() -> new RuntimeException("No such snapshot: " + name));

        //Update SNAPSHOT information
        Snapshot newSnapshot = snapshot.toBuilder().description(SNAPSHOT_DESCRIPTION)
                .timeDefinition(Snapshot.TimeDefinition.forFill(1,
                        BeamModeData.builder()
                                .validity(BeamModeValidity.builder().startTime(Instant.parse(SNAPSHOT_START_TIME))
                                        .build()).build()
                        ,
                        BeamModeData.builder()
                                .validity(BeamModeValidity.builder().endTime(Instant.parse(SNAPSHOT_END_TIME))
                                        .build()).build()))
                .build();

        SNAPSHOT_SERVICE.update(newSnapshot);

        snapshot = SNAPSHOT_SERVICE.findAll(Snapshots.suchThat().name().eq(name)).iterator().next();

        assertEquals(SNAPSHOT_DESCRIPTION, snapshot.getDescription());
        assertEquals(1, snapshot.getFillNumber());

        assertEquals(SNAPSHOT_START_TIME, snapshot.getBeamModeStart().getValidity().getStartTime().toString());
        assertEquals(SNAPSHOT_END_TIME, snapshot.getBeamModeEnd().getValidity().getEndTime().toString());
    }

    @Test
    public void shouldFindByVariableNameLike() {
        String name = getRandomName(SNAPSHOT_NAME);
        Snapshot snapshot = createFixedRangeSnapshot(name);

        Map<String, Set<Variable>> variables = SNAPSHOT_SERVICE.getVariables(snapshot.getId());
        assertEquals(4, variables.size());
        assertEquals(1, variables.get(SnapshotService.AssociationType.DEFAULT.getValue()).size());
        assertEquals(1, variables.get(SnapshotService.AssociationType.X_AXIS_VARIABLE.getValue()).size());
        assertEquals(1, variables.get(SnapshotService.AssociationType.FUNDAMENTAL_FILTERS.getValue()).size());
        assertEquals(1, variables.get(SnapshotService.AssociationType.DRIVING_VARIABLE.getValue()).size());

        //Find a SNAPSHOT
        snapshot = findSnapshotWithName(name);

        assertNotNull(snapshot);
    }

    @Test
    public void shouldRetrieveFilters() {
        String name = getRandomName(SNAPSHOT_NAME);
        Snapshot snapshot = createDynamicRangeSnapshot(name);

        Set<FundamentalFilter> fundamentalFilterSet = new HashSet<>();

        FundamentalFilter fundamentalFilter1 = FundamentalFilter.builder()
                .destination("DEST_1").lsaCycle("LSA_CYCLE_1'").timingUser("TIMING_USER_1").accelerator("LHC").build();
        FundamentalFilter fundamentalFilter2 = FundamentalFilter.builder()
                .destination("DEST_2").lsaCycle("LSA_CYCLE_2'").timingUser("TIMING_USER_2").accelerator("LHC").build();
        fundamentalFilterSet.add(fundamentalFilter1);
        fundamentalFilterSet.add(fundamentalFilter2);

        Snapshot newSnapshot = snapshot.toBuilder().fundamentalFilterSet(fundamentalFilterSet).build();
        Set<FundamentalFilter> fundamentalFilters = newSnapshot.getFundamentalFilterSet();

        assertEquals(2, fundamentalFilters.size());
    }

    @Test
    public void shouldAssureCorrectDynamicTimeWindowCalculation() {
        String name = getRandomName(SNAPSHOT_NAME);
        Snapshot snapshot = createFillRangeSnapshot(name);

        Snapshot modified = snapshot.toBuilder()
                .timeDefinition(dynamicTimeDefinition())
                .build();

        assertNull(modified.getFillNumber());
    }

    @Test
    public void shouldAssureCorrectTimeWindowCalculationForFill() {
        String name = getRandomName(SNAPSHOT_NAME);
        Snapshot snapshot = createDynamicRangeSnapshot(name);

        Snapshot modified = snapshot.toBuilder()
                .timeDefinition(forFillTimeDefinition())
                .build();

        assertNull(modified.isEndTimeDynamic());
    }

    @Test
    public void shouldCalculateFixedRangeTimeWindow() {
        String name = getRandomName(SNAPSHOT_NAME);
        Snapshot snapshot = createDynamicRangeSnapshot(name);

        Snapshot modified = snapshot.toBuilder()
                .timeDefinition(forFillTimeDefinition())
                .build();

        Snapshot updated = modified.toBuilder()
                .timeDefinition(fixedRangeTimeDefinition())
                .build();

        assertNull(updated.isEndTimeDynamic());
        assertNull(updated.getFillNumber());
    }

    @AfterAll
    public static void cleanUp() {
        Set<Snapshot> snapshots = SNAPSHOT_SERVICE.findAll(
                Snapshots.suchThat().variableName().like(SNAPSHOT_NAME + "%"));
        snapshots.forEach(snapshot -> SNAPSHOT_SERVICE.delete(snapshot.getId()));
    }

    private Snapshot createDynamicRangeSnapshot(String name) {
        return SNAPSHOT_SERVICE.create(Snapshot.builder()
                        .name(name)
                        .description(SNAPSHOT_DESCRIPTION)
                        .system(SNAPSHOT_SYSTEM)
                        .owner(SNAPSHOT_OWNER)
                        .visibility(SNAPSHOT_VISIBILITY)
                        .timeDefinition(dynamicTimeDefinition()).build(),
                entities, snapshotVariables, xAxisVariables,
                fundamentalFiltersVariables, drivingVariables);
    }

    private Snapshot createFillRangeSnapshot(String name) {
        return SNAPSHOT_SERVICE.create(Snapshot.builder()
                        .name(name)
                        .description(SNAPSHOT_DESCRIPTION)
                        .system(SNAPSHOT_SYSTEM)
                        .owner(SNAPSHOT_OWNER)
                        .visibility(SNAPSHOT_VISIBILITY)
                        .timeDefinition(forFillTimeDefinition())
                        .build(),
                entities, snapshotVariables, xAxisVariables,
                fundamentalFiltersVariables, drivingVariables);
    }

    private Snapshot createFixedRangeSnapshot(String name) {
        return SNAPSHOT_SERVICE.create(Snapshot.builder()
                        .name(name)
                        .description(SNAPSHOT_DESCRIPTION)
                        .system(SNAPSHOT_SYSTEM)
                        .owner(SNAPSHOT_OWNER)
                        .visibility(SNAPSHOT_VISIBILITY)
                        .timeDefinition(fixedRangeTimeDefinition())
                        .build(),
                entities, snapshotVariables, xAxisVariables,
                fundamentalFiltersVariables, drivingVariables);
    }

    private Snapshot.TimeDefinition forFillTimeDefinition() {
        return Snapshot.TimeDefinition.forFill(1,
                BeamModeData.builder()
                        .beamModeValue("START OF FILL")
                        .validity(BeamModeValidity.builder()
                                .startTime(Instant.parse(SNAPSHOT_START_TIME))
                                .endTime(Instant.parse(SNAPSHOT_START_TIME)).build())
                        .build(),
                BeamModeData.builder()
                        .beamModeValue("END OF FILL")
                        .validity(BeamModeValidity.builder()
                                .startTime(Instant.parse(SNAPSHOT_END_TIME))
                                .endTime(Instant.parse(SNAPSHOT_END_TIME)).build())
                        .build());
    }

    private Snapshot.TimeDefinition dynamicTimeDefinition() {
        return Snapshot.TimeDefinition.dynamic(1, LoggingTimeZone.UTC,
                PriorTime.PREVIOUS_MONDAY, DynamicTimeUnit.DAYS);
    }

    private Snapshot.TimeDefinition fixedRangeTimeDefinition() {
        return Snapshot.TimeDefinition.fixedDates(
                Instant.parse(SNAPSHOT_START_TIME), Instant.parse(SNAPSHOT_END_TIME));
    }

    private Snapshot findSnapshotWithName(String name) {
        return SNAPSHOT_SERVICE.findOne(Snapshots.suchThat().name().eq(name))
                .orElseThrow(() -> new RuntimeException("No such snapshot: " + name));
    }

    private void assertTimeWindow(TimeWindow timeWindow) {
        assertEquals(SNAPSHOT_START_TIME, timeWindow.getStartTime().toString());
        assertEquals(SNAPSHOT_END_TIME, timeWindow.getEndTime().toString());
    }
}
