/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.integrationtests.extraction.custom;

import cern.nxcals.api.custom.domain.Tag;
import cern.nxcals.api.custom.extraction.metadata.TagService;
import cern.nxcals.api.custom.extraction.metadata.TagServiceFactory;
import cern.nxcals.api.custom.extraction.metadata.queries.Tags;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.Visibility;
import cern.nxcals.integrationtests.service.AbstractTest;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TagServiceTest extends AbstractTest {
    private static final String NEW_KEY = "new Key";
    private static final String VARIABLE_NAME = "VARIABLE_TEST";
    private static final String TAG_NAME = "TEST_TAG_";
    private static final String TAG_DESCRIPTION = "TEST_TAG_DESCRIPTION" + Instant.now().toEpochMilli();
    private final TagService tagService = TagServiceFactory.createTagService();

    @Test
    public void shouldCreateTag() {
        String name = TAG_NAME + getRandomString();
        Tag tag = createTag(name);
        try {
            assertNotNull(tag);
            assertEquals(name, tag.getName());
        } finally {
            tagService.delete(tag.getId());
        }
    }

    private Tag createTag(String name) {
        Instant tagTimestamp = Instant.now();
        Entity entity = entity(-100, "TAGS");
        ImmutableSet<Entity> entities = ImmutableSet.of(entity);
        ImmutableSet<Variable> variables = ImmutableSet.of(variable(entity, VARIABLE_NAME));

        return tagService.create(name,
                "description", Visibility.PUBLIC,
                "acclog", "CMW",
                tagTimestamp,
                entities, variables);
    }

    @Test
    public void shouldFindAndUpdateTag() {
        String name = TAG_NAME + getRandomString();
        Tag tag = createTag(name);
        try {
            //Find a TAG
            tag = tagService.findOne(Tags.suchThat().name().eq(name))
                    .orElseThrow(() -> new RuntimeException("No such tag: " + name));

            //Update TAG information
            Tag newTag = tag.toBuilder().description(TAG_DESCRIPTION).properties(ImmutableMap.of(NEW_KEY, "value"))
                    .build();

            tagService.update(newTag);

            tag = tagService.findAll(Tags.suchThat().name().eq(name)).iterator().next();

            assertEquals(TAG_DESCRIPTION, tag.getDescription());
            assertTrue(tag.getProperties().containsKey(NEW_KEY));

        } finally {
            tagService.delete(tag.getId());

        }
    }

    @Test
    public void shouldFindByVariableNameLike() {
        String name = TAG_NAME + getRandomString();
        Tag tag = createTag(name);
        try {
            Map<String, Set<Variable>> variables = tagService.getVariables(tag.getId());
            assertEquals(1, variables.size());
            assertEquals(1, variables.get(TagService.DEFAULT_RELATION).size());

            //Find a TAG
            tag = tagService.findOne(Tags.suchThat().name().eq(name).and().variableName().like(VARIABLE_NAME + "%"))
                    .orElseThrow(() -> new RuntimeException("No such tag: " + name));

            //Update TAG information
            assertNotNull(tag);

        } finally {
            tagService.delete(tag.getId());

        }
    }

}


