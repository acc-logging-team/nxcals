package cern.nxcals.integrationtests.extraction;

import cern.cmw.datax.DataBuilder;
import cern.cmw.datax.ImmutableData;
import cern.nxcals.api.config.SparkContext;
import cern.nxcals.api.domain.Partition;
import cern.nxcals.api.domain.PartitionProperties;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.extraction.metadata.InternalServiceClientFactory;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.api.ingestion.Publisher;
import cern.nxcals.api.ingestion.PublisherFactory;
import cern.nxcals.internal.extraction.metadata.InternalPartitionService;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.getDefaultFieldsForCMWEntityQueryWith;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.getFieldData;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = SparkContext.class)
@SpringBootApplication
@Slf4j
@Disabled
public class UpdatableDataTest {
    public static final int PUBLISHED_RECORDS_NUMBER = 100;
    public static final int UPDATED_RECORDS_NUMBER = 101;
    protected static final InternalPartitionService internalPartitionService = InternalServiceClientFactory
            .createPartitionService();
    private static final int MAX_WAIT_TIME = 3;
    private static final SystemSpecService systemService = ServiceClientFactory.createSystemSpecService();
    private static final String RANDOM_DEVICE = UUID.randomUUID().toString();
    private static final Instant FROM = Instant.now().minus(10, ChronoUnit.DAYS);
    public static final Instant END_TIME = FROM.plus(UPDATED_RECORDS_NUMBER, ChronoUnit.MILLIS);
    private static final String SYSTEM = "TEST-CMW";
    private static final String CLASS = "test_class";
    private static final String DEVICE_KEY = "device";
    private static final String PROPERTY_KEY = "property";
    private static final String CLASS_KEY = "class";
    private static final String RECORD_VERSION_KEY = "__record_version__";
    private static final String RECORD_TIMESTAMP_KEY = "__record_timestamp__";
    private static final String DEVICE = "test_device_" + RANDOM_DEVICE;
    private static final String FIELD1 = "field1";
    private static final String FIELD2 = "field2";
    private static final String PROPERTY = "test_property_" + UUID.randomUUID().toString();
    static final Map<String, Object> PARTITION_KEY_VALUES = ImmutableMap
            .of(CLASS_KEY, CLASS, PROPERTY_KEY, PROPERTY);
    private static final SystemSpec SYSTEM_SPEC = systemService.findByName(SYSTEM)
            .orElseThrow(() -> new IllegalArgumentException("No such system"));
    private static final List<Integer> EXPECTED_SIMPLE_VALUES = IntStream.range(0, PUBLISHED_RECORDS_NUMBER).boxed()
            .collect(Collectors.toList());
    private static final List<Integer> EXPECTED_UPDATED_VALUES = IntStream.range(0, (UPDATED_RECORDS_NUMBER) * 2)
            .filter(i -> i % 2 == 0).boxed()
            .collect(Collectors.toList());
    private static volatile boolean dataVisible = false;
    private static final Publisher<ImmutableData> publisher = PublisherFactory.newInstance()
            .createPublisher(SYSTEM, Function.identity());

    static {
        System.setProperty("kafka.producer.linger.ms", "0");
    }

    @Autowired
    private SparkSession sparkSession;
    private boolean waitingForDataFailed = false;

    @BeforeAll
    public static void setUpData() {
        //create partition
        Partition partition = Partition.builder()
                .properties(new PartitionProperties(true, null, TimeWindow.between(null, null)))
                .systemSpec(SYSTEM_SPEC)
                .keyValues(PARTITION_KEY_VALUES)
                .build();
        Partition savedPartition = internalPartitionService.create(partition);
        assertNotNull(savedPartition);
        //publish data
        log.info("Sending data to NXCALS");
        try {
            for (int i = 0; i < PUBLISHED_RECORDS_NUMBER; ++i) {
                long timestamp = FROM.plus(i, ChronoUnit.MILLIS).toEpochMilli() * 1000_000;
                publisher.publish(createSimpleData(i, PROPERTY, timestamp));
            }
        } catch (Exception e) {
            log.error("Error sending values to NXCALS", e);
            fail("Error sending values to NXCALS");
        }

    }

    private static ImmutableData createSimpleData(int i, String property, long timestamp) {
        DataBuilder builder = ImmutableData.builder();
        builder.add(DEVICE_KEY, DEVICE);
        builder.add(PROPERTY_KEY, property);
        builder.add(CLASS_KEY, CLASS);
        builder.add(RECORD_VERSION_KEY, 0L);
        builder.add(RECORD_TIMESTAMP_KEY, timestamp);
        builder.add(FIELD1, i);
        builder.add(FIELD2, -i);
        return builder.build();
    }

    public void waitForData(boolean isUpdated) throws InterruptedException {
        if (!dataVisible) {
            //Have to wait 30 seconds in order for the data to be processed and visible in NXCALS extraction
            log.info("Waiting max " + MAX_WAIT_TIME + " minutes for the data to be visible in NXCALS...");
            waitForData(TimeUnit.MINUTES.toMillis(MAX_WAIT_TIME), isUpdated);
            log.info("Ok, data should be processed now, proceeding with tests");
            dataVisible = true;
        } else {
            log.info("Data already checked");
        }
    }

    private synchronized void waitForData(long maxTime, boolean isUpdated) throws InterruptedException {
        long start = System.currentTimeMillis();

        while (true) {
            if (waitingForDataFailed || (System.currentTimeMillis() - start > maxTime)) {
                waitingForDataFailed = true;
                fail("Data was not processed in the required maxTime=" + maxTime + " ms");
            }
            if (isDataPresent(isUpdated)) {
                return;
            }

            log.info("Still no data visible, waiting more...");
            TimeUnit.SECONDS.sleep(1);
        }
    }

    private boolean isDataPresent(boolean isUpdated) {
        if (isUpdated) {
            return getCount(DEVICE, PROPERTY) == UPDATED_RECORDS_NUMBER;
        } else {
            return getCount(DEVICE, PROPERTY) == PUBLISHED_RECORDS_NUMBER;
        }
    }

    private long getCount(String device, String property) {
        return DataQuery.builder(sparkSession).byEntities().system(SYSTEM).startTime(FROM)
                .endTime(END_TIME)
                .entity().keyValueLike(DEVICE_KEY, device).keyValue(PROPERTY_KEY, property).build().count();
    }

    public void updateData() {
        //sending data with the same timestamp and properties, only different values
        dataVisible = false;
        log.info("Sending new data to NXCALS");
        try {
            for (int i = 0; i < PUBLISHED_RECORDS_NUMBER + 1; ++i) {
                long timestamp = FROM.plus(i, ChronoUnit.MILLIS).toEpochMilli() * 1000_000;
                publisher.publish(createSimpleData(2 * i, PROPERTY, timestamp));
            }
        } catch (Exception e) {
            log.error("Error sending values to NXCALS", e);
            fail("Error sending values to NXCALS");
        }
    }

    @Test
    public void shouldExtractUpdatedData() {
        try {
            waitForData(false);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Set<String> expectedColumns = getDefaultFieldsForCMWEntityQueryWith();
        expectedColumns.add(FIELD1);
        expectedColumns.add(FIELD2);
        List<Integer> expected = EXPECTED_SIMPLE_VALUES;
        Dataset<Row> dataset = DataQuery.builder(sparkSession).byEntities().system(SYSTEM).startTime(FROM)
                .endTime(END_TIME)
                .entity().keyValueLike(DEVICE_KEY, DEVICE).keyValue(PROPERTY_KEY, PROPERTY).build();
        // then
        assertEquals(PUBLISHED_RECORDS_NUMBER, dataset.count());

        Set<String> datasetColumns = Sets.newHashSet(dataset.columns());
        assertEquals(expectedColumns, datasetColumns);
        List<Object> retrieved = getFieldData(dataset, DEVICE, PROPERTY, FIELD1);
        assertEquals(expected, retrieved);
        log.info("Published data was correctly retrieved");
        //send new data
        updateData();
        dataVisible = false;
        //wait for the data to be processed
        try {
            waitForData(true);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        expected = EXPECTED_UPDATED_VALUES;
        // when
        dataset = DataQuery.builder(sparkSession).byEntities().system(SYSTEM).startTime(FROM)
                .endTime(END_TIME)
                .entity().keyValueLike(DEVICE_KEY, DEVICE).keyValue(PROPERTY_KEY, PROPERTY).build();
        // then
        assertEquals(UPDATED_RECORDS_NUMBER, dataset.count());
        assertEquals(expectedColumns, datasetColumns);
        retrieved = getFieldData(dataset, DEVICE, PROPERTY, FIELD1);
        assertEquals(expected, retrieved);
    }

}
