package cern.nxcals.integrationtests.extraction.client;

import cern.cmw.datax.ImmutableData;
import cern.nxcals.api.config.SparkContext;
import cern.nxcals.api.custom.service.ExtractionService;
import cern.nxcals.api.custom.service.Services;
import cern.nxcals.api.custom.service.extraction.ExtractionProperties;
import cern.nxcals.api.custom.service.extraction.LookupStrategy;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableDeclaredType;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.common.utils.IllegalCharacterConverter;
import cern.nxcals.common.utils.Lazy;
import cern.nxcals.integrationtests.utils.CMWDataGenerator;
import cern.nxcals.integrationtests.utils.CMWDatasetGenerator;
import cern.nxcals.integrationtests.utils.CMWEntityDataGenerator;
import cern.nxcals.integrationtests.utils.HBaseDataInitializer;
import cern.nxcals.integrationtests.utils.HdfsDataInitializer;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.DependsOn;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.function.Function;

import static cern.nxcals.common.SystemFields.NXC_EXTR_VALUE;
import static cern.nxcals.common.spark.MoreFunctions.asRow;
import static cern.nxcals.common.spark.MoreFunctions.createNullableField;
import static cern.nxcals.common.spark.MoreFunctions.createTypeFromFields;
import static cern.nxcals.integrationtests.service.AbstractTest.getRandomName;
import static cern.nxcals.integrationtests.utils.ServicesHelper.deleteVariable;
import static cern.nxcals.integrationtests.utils.ServicesHelper.registerSimpleVariable;
import static cern.nxcals.integrationtests.utils.ServicesHelper.registerVariableWithPredicate;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = SparkContext.class)
@SpringBootApplication
@Slf4j
@DependsOn("kerberos")
public class ExtractionOfNestedStructTest {
    //    static {
    //        String username = System.getProperty("user.name");
    //        System.setProperty("kerberos.principal", "acclog");
    //        System.setProperty("kerberos.keytab", "/opt/<username>/.keytab-acclog".replace("<username>", username));
    //        System.setProperty("service.url",
    //                "https://nxcals-<user>-1.cern.ch:19093".replace("<user>", username)); //Do not use localhost here.
    //        System.setProperty("kafka.producer.bootstrap.servers",
    //                "https://nxcals-<user>-3.cern.ch:9092,https://nxcals-<user>-4.cern.ch:9092,https://nxcals-<user>-5.cern.ch:9092".replace(
    //                        "<user>", username));
    //    }

    private static final String FIELD_NAME = "FIELD";
    private static final String NESTED_FIELD_NAME_1 = "NESTED_FIELD1";
    private static final String FIELD_NAME_TO_ENCODE_1 = "FIELD.1";
    private static final String FIELD_NAME_TO_ENCODE_2 = "FIELD.2";
    private static final String NESTED_FIELD_TO_ENCODE_NAME_3 = "NESTED_FIELD.3";

    private static final String NESTED_FIELD_1_ENCODED_FIELD_1_NAME =
            NESTED_FIELD_NAME_1 + ".`" + FIELD_NAME_TO_ENCODE_1 + "`";
    private static final String NESTED_FIELD_1_ENCODED_FIELD_2_NAME =
            NESTED_FIELD_NAME_1 + ".`" + FIELD_NAME_TO_ENCODE_2 + "`";
    private static final String NESTED_FIELD_1_NESTED_ENCODED_FIELD_3_NAME =
            NESTED_FIELD_NAME_1 + ".`" + NESTED_FIELD_TO_ENCODE_NAME_3 + "`";
    private static final String NESTED_FIELD_1_NESTED_ENCODED_FIELD_3_ENCODED_FIELD_1_NAME =
            NESTED_FIELD_1_NESTED_ENCODED_FIELD_3_NAME + ".`" + FIELD_NAME_TO_ENCODE_1 + "`";
    private static final String NESTED_FIELD_1_NESTED_ENCODED_FIELD_3_ENCODED_FIELD_2_NAME =
            NESTED_FIELD_NAME_1 + ".`" + NESTED_FIELD_TO_ENCODE_NAME_3 + "`.`" + FIELD_NAME_TO_ENCODE_2 + "`";
    private static final String NESTED_FIELD_2_ENCODED_FIELD_1_NAME =
            NESTED_FIELD_NAME_1 + ".`" + FIELD_NAME_TO_ENCODE_1 + "`";

    // HBase part

    private static final Function<Integer, ImmutableData> rowGenerator = datapointNumber -> {
        ImmutableData nestedData2 = ImmutableData.builder()
                .add(FIELD_NAME_TO_ENCODE_1, -datapointNumber)
                .add(FIELD_NAME_TO_ENCODE_2, "-VALUE")
                .build();
        ImmutableData nestedData1 = ImmutableData.builder()
                .add(FIELD_NAME_TO_ENCODE_1, datapointNumber)
                .add(FIELD_NAME_TO_ENCODE_2, "VALUE")
                .add(NESTED_FIELD_TO_ENCODE_NAME_3, nestedData2)
                .build();
        return ImmutableData.builder()
                .add(FIELD_NAME, datapointNumber)
                .add(FIELD_NAME_TO_ENCODE_1, -datapointNumber)
                .add(NESTED_FIELD_NAME_1, nestedData1)
                .build();
    };
    private static final CMWEntityDataGenerator hbaseDataGenerator = new CMWEntityDataGenerator(rowGenerator);

    private static final HBaseDataInitializer hbaseDataInitializer = new HBaseDataInitializer(hbaseDataGenerator);

    // Hdfs part

    private static List<StructField> createSchema() {
        IllegalCharacterConverter converter = IllegalCharacterConverter.get();

        StructType nestedData2Fields = createTypeFromFields(
                createNullableField(converter.convertToLegal(FIELD_NAME_TO_ENCODE_1), DataTypes.LongType),
                createNullableField(converter.convertToLegal(FIELD_NAME_TO_ENCODE_2), DataTypes.StringType));

        StructType nestedData1Fields = createTypeFromFields(
                createNullableField(converter.convertToLegal(FIELD_NAME_TO_ENCODE_1), DataTypes.LongType),
                createNullableField(converter.convertToLegal(FIELD_NAME_TO_ENCODE_2), DataTypes.StringType),
                createNullableField(converter.convertToLegal(NESTED_FIELD_TO_ENCODE_NAME_3), nestedData2Fields));

        return List.of(
                createNullableField(FIELD_NAME, DataTypes.LongType),
                createNullableField(converter.convertToLegal(FIELD_NAME_TO_ENCODE_1), DataTypes.LongType),
                createNullableField(NESTED_FIELD_NAME_1, nestedData1Fields));
    }

    private static final CMWDatasetGenerator hdfsDatasetGenerator = new CMWDatasetGenerator(datapointNumber -> {
        Row nestedData2 = asRow(-datapointNumber, "-VALUE");
        Row nestedData1 = asRow(datapointNumber, "VALUE", nestedData2);
        return asRow(datapointNumber, -datapointNumber, nestedData1);
    }, createSchema());

    @Autowired
    private SparkSession sparkSession;

    @Value("${hdfs.data.paths}")
    private String hdfsDataDir;

    private static final HdfsDataInitializer hdfsDataInitializer = new HdfsDataInitializer(hdfsDatasetGenerator);

    private final Lazy<ExtractionService> extractionService = new Lazy<>(
            () -> Services.newInstance(sparkSession).extractionService());

    @BeforeAll
    static void beforeAll() {
        hbaseDataInitializer.setUpData();
    }

    @BeforeEach
    void beforeEach() throws InterruptedException {
        hbaseDataInitializer.waitForData(sparkSession);
        hdfsDataInitializer.setUpData(sparkSession, hdfsDataDir);
    }

    static Object[] dataGenerators() {
        return new Object[] {
                new Object[] { hbaseDataGenerator, "HBase data" },
                new Object[] { hdfsDatasetGenerator, "Hdfs data" }
        };
    }

    @ParameterizedTest(name = "should correctly extract data end decode schema - {1}")
    @MethodSource("dataGenerators")
    void shouldCorrectlyExtractEntityWithNestedStructsAndEncodedFieldNames(CMWDataGenerator dataGenerator,
            String comment) {
        Entity entity = dataGenerator.getCreatedEntity();

        ExtractionProperties extractionProperties = getForTimeWindow(dataGenerator.getQueryTimeWindow());

        Dataset<Row> dataset = extractionService.get().getData(entity, extractionProperties);

        assertAll(
                () -> assertEquals(dataGenerator.getNumberOfPoints(),
                        dataset.filter(NESTED_FIELD_1_ENCODED_FIELD_1_NAME + " >= 0").count()),
                () -> assertEquals(dataGenerator.getNumberOfPoints(),
                        dataset.filter(NESTED_FIELD_1_NESTED_ENCODED_FIELD_3_ENCODED_FIELD_1_NAME + " <= 0").count()),
                () -> assertEquals(dataGenerator.getNumberOfPoints(),
                        dataset.filter(NESTED_FIELD_1_ENCODED_FIELD_2_NAME + " == 'VALUE'").count()),
                () -> assertEquals(dataGenerator.getNumberOfPoints(),
                        dataset.filter(NESTED_FIELD_1_NESTED_ENCODED_FIELD_3_ENCODED_FIELD_2_NAME + " == '-VALUE'")
                                .count()),
                () -> assertEquals(dataGenerator.getNumberOfPoints(),
                        dataset.filter(FIELD_NAME + " >= 0").count())
        );
    }

    @ParameterizedTest(name = "should correctly extract data using entity and alias of encoded field - {1}")
    @MethodSource("dataGenerators")
    void shouldCorrectlyExtractEntityWithAliasOfEncodedFieldOld(CMWDataGenerator dataGenerator, String comment) {
        // this is a test case, which should work with old version - but was not without change on server side
        Entity entity = dataGenerator.getCreatedEntity();

        Dataset<Row> dataset = DataQuery.builder(sparkSession).entities().system(entity.getSystemSpec().getName())
                .idEq(entity.getId()).timeWindow(dataGenerator.getQueryTimeWindow())
                .fieldAliases("alias", FIELD_NAME_TO_ENCODE_1).build();

        assertEquals(dataGenerator.getNumberOfPoints(),
                dataset.filter("alias <= 0").count());
    }

    @ParameterizedTest(name = "should correctly extract data using variable pointing to field with encoded name - {1}")
    @MethodSource("dataGenerators")
    void shouldCorrectlyExtractVariablePointingToFieldWithEncodedName(
            CMWDataGenerator dataGenerator,
            String comment) {
        Entity entity = dataGenerator.getCreatedEntity();
        ExtractionProperties extractionProperties = getForTimeWindow(dataGenerator.getQueryTimeWindow());

        Variable variable = registerSimpleVariable(getRandomName("VAR_"), entity, FIELD_NAME_TO_ENCODE_1,
                VariableDeclaredType.NUMERIC);
        try {
            Dataset<Row> dataset = extractionService.get().getData(variable, extractionProperties);
            assertEquals(dataGenerator.getNumberOfPoints(),
                    dataset.filter(NXC_EXTR_VALUE.getValue() + " <= 0").count());
        } finally {
            deleteVariable(variable);
        }
    }

    @Disabled("Aliases won't work without change on server side")
    @ParameterizedTest(name = "should correctly extract data using entity and alias of nested field - {1}")
    @MethodSource("dataGenerators")
    void shouldCorrectlyExtractEntityWithAliasOfNestedField(CMWDataGenerator dataGenerator, String comment) {
        Entity entity = dataGenerator.getCreatedEntity();

        Dataset<Row> dataset = DataQuery.builder(sparkSession).entities().system(entity.getSystemSpec().getName())
                .idEq(entity.getId()).timeWindow(dataGenerator.getQueryTimeWindow())
                .fieldAliases("alias", NESTED_FIELD_1_NESTED_ENCODED_FIELD_3_ENCODED_FIELD_1_NAME).build();

        assertEquals(dataGenerator.getNumberOfPoints(),
                dataset.filter("alias <= 0").count());
    }

    @Disabled("Wait for change with backquotes")
    @ParameterizedTest(name = "should correctly extract data using entity and alias of nested record field - {1}")
    @MethodSource("dataGenerators")
    void shouldCorrectlyExtractEntityWithAliasOfNestedRecordField(CMWDataGenerator dataGenerator, String comment) {
        Entity entity = dataGenerator.getCreatedEntity();

        Dataset<Row> dataset = DataQuery.builder(sparkSession).entities().system(entity.getSystemSpec().getName())
                .idEq(entity.getId()).timeWindow(dataGenerator.getQueryTimeWindow())
                .fieldAliases("alias", NESTED_FIELD_1_NESTED_ENCODED_FIELD_3_NAME).build();

        assertEquals(dataGenerator.getNumberOfPoints(),
                dataset.filter("alias.`" + FIELD_NAME_TO_ENCODE_1 + "` <= 0").count());
    }

    @Disabled("Wait for change with backquotes")
    @ParameterizedTest(name = "should correctly extract data using entity and alias of encoded field - {1}")
    @MethodSource("dataGenerators")
    void shouldCorrectlyExtractEntityWithAliasOfEncodedField(CMWDataGenerator dataGenerator, String comment) {
        Entity entity = dataGenerator.getCreatedEntity();

        Dataset<Row> dataset = DataQuery.builder(sparkSession).entities().system(entity.getSystemSpec().getName())
                .idEq(entity.getId()).timeWindow(dataGenerator.getQueryTimeWindow())
                .fieldAliases("alias", "`" + FIELD_NAME_TO_ENCODE_1 + "`").build();

        assertEquals(dataGenerator.getNumberOfPoints(),
                dataset.filter("alias <= 0").count());
    }

    @Disabled("Wait for change with backquotes")
    @ParameterizedTest(name = "should correctly extract data using variable pointing to nested field with encoded name - {1}")
    @MethodSource("dataGenerators")
    void shouldCorrectlyExtractVariablePointingToFieldInNestedStructureWithEncodedName(
            CMWDataGenerator dataGenerator,
            String comment) {
        Entity entity = dataGenerator.getCreatedEntity();
        ExtractionProperties extractionProperties = getForTimeWindow(dataGenerator.getQueryTimeWindow());

        Variable variable = registerSimpleVariable(getRandomName("VAR_"), entity, NESTED_FIELD_2_ENCODED_FIELD_1_NAME,
                VariableDeclaredType.NUMERIC);
        try {
            Dataset<Row> dataset = extractionService.get().getData(variable, extractionProperties);
            assertEquals(dataGenerator.getNumberOfPoints(),
                    dataset.filter(NXC_EXTR_VALUE.getValue() + " >= 0").count());
        } finally {
            deleteVariable(variable);
        }
    }

    @Disabled("Wait for change with backquotes")
    @ParameterizedTest(name = "should correctly extract data using variable with predicate pointing to nested field with encoded name - {1}")
    @MethodSource("dataGenerators")
    void shouldCorrectlyExtractVariablePointingToFieldInNestedStructureWithEncodedNameWithPredicate(
            CMWDataGenerator dataGenerator,
            String comment) {
        Entity entity = dataGenerator.getCreatedEntity();
        ExtractionProperties extractionProperties = getForTimeWindow(dataGenerator.getQueryTimeWindow());

        String predicate = FIELD_NAME + " > 5 AND " + NESTED_FIELD_2_ENCODED_FIELD_1_NAME + " % 3 > 0";
        Variable variable = registerVariableWithPredicate(getRandomName("VAR_"), entity,
                NESTED_FIELD_2_ENCODED_FIELD_1_NAME,
                VariableDeclaredType.NUMERIC, predicate);
        try {
            Dataset<Row> dataset = extractionService.get().getData(variable, extractionProperties);
            assertTrue(dataset.filter(NXC_EXTR_VALUE.getValue() + " >= 0").count() > 0);
        } finally {
            deleteVariable(variable);
        }
    }

    private static ExtractionProperties getForTimeWindow(TimeWindow window) {
        return ExtractionProperties.builder()
                .timeWindow(window.getStartTime(), window.getEndTime())
                .lookupStrategy(LookupStrategy.LAST_BEFORE_START_IF_EMPTY).build();
    }

}
