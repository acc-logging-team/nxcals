package cern.nxcals.integrationtests.extraction;

import cern.cmw.datax.DataBuilder;
import cern.cmw.datax.ImmutableData;
import cern.nxcals.api.config.SparkContext;
import cern.nxcals.api.custom.domain.BeamMode;
import cern.nxcals.api.custom.domain.Fill;
import cern.nxcals.api.custom.service.FillService;
import cern.nxcals.api.custom.service.Services;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.api.domain.VariableDeclaredType;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.extraction.metadata.EntityService;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.extraction.metadata.queries.Entities;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import cern.nxcals.api.ingestion.Publisher;
import cern.nxcals.api.ingestion.PublisherFactory;
import cern.nxcals.api.utils.TimeUtils;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSortedSet;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.DependsOn;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;
import java.util.stream.Collectors;

import static cern.nxcals.api.custom.domain.CmwSystemConstants.CLASS_KEY_NAME;
import static cern.nxcals.api.custom.domain.CmwSystemConstants.CMW_SYSTEM;
import static cern.nxcals.api.custom.domain.CmwSystemConstants.DEVICE_KEY_NAME;
import static cern.nxcals.api.custom.domain.CmwSystemConstants.PROPERTY_KEY_NAME;
import static cern.nxcals.api.custom.domain.CmwSystemConstants.RECORD_TIMESTAMP;
import static cern.nxcals.api.custom.domain.CmwSystemConstants.RECORD_VERSION;
import static java.lang.String.format;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Integration tests suite for {@link cern.nxcals.api.custom.service.FillService}
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = SparkContext.class)
@SpringBootApplication
@Slf4j
@DependsOn("kerberos")
public class FillExtractionTest {
    static {
        System.setProperty("kafka.producer.linger.ms", "0");

        //        Please leave this one just in case you want to run those test from IDE for debugging
        //        Don't forget to check if hadoop-dev jar is included in build.gradle!!!

        //        String username = System.getProperty("user.name");
        //        System.setProperty("service.url",
        //                "https://nxcals-<user>1.cern.ch:19093".replace("<user>", username)); //Do not use localhost here.
        //        System.setProperty("kafka.producer.bootstrap.servers",
        //                "https://nxcals-<user>3.cern.ch:9092,https://nxcals-<user>4.cern.ch:9092,https://nxcals-<user>5.cern.ch:9092"
        //                        .replace("<user>", username));
    }

    private static final int MAX_WAIT_FOR_DATA_MINUTES = 2;
    private static final String RANDOM_SUFFIX = UUID.randomUUID().toString();

    private static final String FILL_DEVICE_NAME = "XTIM.HX.FILLN-CT_" + RANDOM_SUFFIX;
    private static final String BMODE_DEVICE_NAME = "XTIM.HX.BMODE-CT_" + RANDOM_SUFFIX;

    private static final String XTIM_PROPERTY_NAME = "Acquisition";
    private static final String XTIM_CLASS_NAME = "XTIM_LHC_TEST";

    private static final String FILL_VARIABLE_NAME = "HX:FILLN";
    private static final String BMODE_VARIABLE_NAME = "HX:BMODE";

    private static final String FILL_FIELD_NAME = "FILL_NB";
    private static final String BMODE_FIELD_NAME = "MACHINE_STATE";

    private static final SystemSpecService systemService = ServiceClientFactory.createSystemSpecService();
    private static final EntityService entityService = ServiceClientFactory.createEntityService();
    private static final VariableService variableService = ServiceClientFactory.createVariableService();

    // Need to always use CMW system here as fills and beam modes solely belong to that concept
    private static final SystemSpec CMW_SYSTEM_SPEC = systemService.findByName(CMW_SYSTEM)
            .orElseThrow(() -> new IllegalArgumentException("No such system"));

    // FILL DATA BREAKDOWN:
    // Sent values with a frequency of 6 messages per minute
    private static final int FILL_DATA_SIZE = 200;
    private static final Instant FILL_DATA_PUBLISH_START = Instant.now().minus(3, ChronoUnit.HOURS);
    private static final Duration FILL_DATA_PUBLISH_INTERVAL = Duration.ofSeconds(10);

    private static final int DUPLICATE_DATA_FILL_NUMBER = 100;
    private static final int DUPLICATE_DATA_FILLS_SIZE = 5;

    // BEAM MODE DATA BREAKDOWN:
    // Sent values twice as frequently as fill data (12 messages per minute)
    // Begin data publication with an offset of 10 intervals from the first produced fill (skip 10 cycles).
    private static final int BMODE_DATA_SIZE = 400;
    private static final Instant BMODE_DATA_PUBLISH_START = FILL_DATA_PUBLISH_START
            .plus(FILL_DATA_PUBLISH_INTERVAL.multipliedBy(10));
    private static final Duration BMODE_DATA_PUBLISH_INTERVAL = FILL_DATA_PUBLISH_INTERVAL.dividedBy(2);

    private static final AtomicBoolean DATA_VISIBLE = new AtomicBoolean(false);
    private static final AtomicBoolean WAITING_FOR_DATA_FAILED = new AtomicBoolean(false);

    @Autowired
    private SparkSession sparkSession;
    private FillService fillService;

    @BeforeAll
    public static void setUpData() {
        PublisherFactory publisherFactory = PublisherFactory.newInstance();
        List<ImmutableData> dataToPublish = new ArrayList<>();
        dataToPublish.addAll(generateFillData());
        dataToPublish.addAll(generateBeamModeData());
        log.info("Sending fills and beam mode data to NXCALS");
        try (Publisher<ImmutableData> publisher = publisherFactory
                .createPublisher(CMW_SYSTEM_SPEC.getName(), Function.identity())) {
            for (ImmutableData data : dataToPublish) {
                publisher.publish(data);
            }
        } catch (Exception e) {
            log.error("Error sending data to NXCALS", e);
            fail("Error sending data to NXCALS! Cause:\n" + e.getCause());
        }
        configureFillVariable();
        configureBeamModeVariable();
    }

    @BeforeEach
    public void waitForData() throws InterruptedException {
        this.fillService = Services.newInstance(sparkSession).fillService();
        if (!DATA_VISIBLE.get()) {
            //Have to wait 30 seconds in order for the data to be processed and visible in NXCALS extraction
            log.info("Waiting max " + MAX_WAIT_FOR_DATA_MINUTES + " minutes for the data to be visible in NXCALS...");
            waitForData(TimeUnit.MINUTES.toMillis(MAX_WAIT_FOR_DATA_MINUTES));
            log.info("Ok, data should be processed now, proceeding with tests");
            DATA_VISIBLE.set(true);
        } else {
            log.info("Data already checked");
        }
    }

    @Test
    public void shouldWorkForEmptyData() {
        assertTrue(fillService.findFills(Instant.ofEpochMilli(100L), Instant.ofEpochMilli(200L)).isEmpty());
        assertFalse(fillService.findFill(1).isPresent()); // should not get fill numbers < 2 (test fills)
    }

    @Test
    public void shouldNotFetchTestFills() {
        // should not get fill numbers < 2 (test fills)
        assertFalse(fillService.findFill(1).isPresent());
        assertFalse(fillService.findFill(0).isPresent());
    }

    @Test
    public void shouldGetFillsAndBeamModesInTimeWindow() {
        Instant from = FILL_DATA_PUBLISH_START.plus(FILL_DATA_PUBLISH_INTERVAL.multipliedBy(10));
        Instant to = from.plus(FILL_DATA_PUBLISH_INTERVAL.multipliedBy(4));

        List<Fill> fetchedFills = fillService.findFills(from, to);

        assertEquals(4, fetchedFills.size());
        int[] expectedFillNumber = { 10, 11, 12, 13 };
        assertArrayEquals(expectedFillNumber, fetchedFills.stream().mapToInt(Fill::getNumber).toArray());
        for (Fill fill : fetchedFills) {
            assertFalse(fill.getBeamModes().isEmpty());
            String[] expectedModes = { "NO BEAM", "BEAM" };
            String[] modes = fill.getBeamModes().stream().map(BeamMode::getBeamModeValue).toArray(String[]::new);
            assertArrayEquals(expectedModes, modes);
        }
    }

    @Test
    public void shouldFindFillByNumber() {
        Optional<Fill> foundFill = fillService.findFill(3);
        assertNotNull(foundFill);
        assertTrue(foundFill.isPresent());

        Fill fill = foundFill.get();
        assertEquals(3L, fill.getNumber());

        TimeWindow expectedTimeWindow = TimeWindow.between(
                FILL_DATA_PUBLISH_START.plus(FILL_DATA_PUBLISH_INTERVAL.multipliedBy(3)),
                FILL_DATA_PUBLISH_START.plus(FILL_DATA_PUBLISH_INTERVAL.multipliedBy(4)));
        assertEquals(expectedTimeWindow, fill.getValidity());
        assertTrue(fill.getBeamModes().isEmpty());
    }

    @Test
    public void shouldMergeSequentialFillsOfSameNumber() {
        Optional<Fill> foundFill = fillService.findFill(DUPLICATE_DATA_FILL_NUMBER);
        assertNotNull(foundFill);
        assertTrue(foundFill.isPresent());

        Fill fill = foundFill.get();
        assertEquals(DUPLICATE_DATA_FILL_NUMBER, fill.getNumber());

        Instant fillStart = FILL_DATA_PUBLISH_START
                .plus(FILL_DATA_PUBLISH_INTERVAL.multipliedBy(DUPLICATE_DATA_FILL_NUMBER));
        TimeWindow expectedTimeWindow = TimeWindow.between(fillStart, fillStart.plus(FILL_DATA_PUBLISH_INTERVAL));
        assertEquals(expectedTimeWindow, fill.getValidity());

        List<BeamMode> beamModes = fill.getBeamModes();
        assertNotNull(beamModes);
        assertEquals(2, beamModes.size());
        assertEquals(Arrays.asList("NO BEAM", "BEAM"), beamModes.stream().map(BeamMode::getBeamModeValue).collect(
                Collectors.toList()));
    }

    @Test
    public void shouldGetLastCompletedFill() {
        Instant start = FILL_DATA_PUBLISH_START.plus(FILL_DATA_PUBLISH_INTERVAL.multipliedBy(198));
        TimeWindow expectedLastFillTImeWindow = TimeWindow.between(start, start.plus(FILL_DATA_PUBLISH_INTERVAL));

        Optional<Fill> fetchedFill = fillService.getLastCompleted();
        assertNotNull(fetchedFill);
        assertTrue(fetchedFill.isPresent());
        Fill lastFill = fetchedFill.get();
        assertEquals(198, lastFill.getNumber());
        assertEquals(expectedLastFillTImeWindow, lastFill.getValidity());

        List<BeamMode> beamModes = lastFill.getBeamModes();
        assertFalse(beamModes.isEmpty());
        assertEquals(2, beamModes.size());
        assertEquals(Arrays.asList("NO BEAM", "BEAM"), beamModes.stream().map(BeamMode::getBeamModeValue).collect(
                Collectors.toList()));
    }

    @Test
    public void shouldProvideConsistentResultsOnMultipleCallsToFindFills() {
        Instant from = FILL_DATA_PUBLISH_START.plus(FILL_DATA_PUBLISH_INTERVAL.multipliedBy(100));
        Instant to = from.plus(FILL_DATA_PUBLISH_INTERVAL.multipliedBy(50));

        Comparator<Fill> fillComparator = Comparator.comparingInt(Fill::getNumber);
        List<Fill> fetchedFills = fillService.findFills(from, to);
        assertEquals(50, fetchedFills.size());

        fetchedFills.sort(fillComparator);
        for (int i = 0; i < 3; i++) {
            List<Fill> fills = fillService.findFills(from, to);
            fills.sort(fillComparator);
            assertEquals(fetchedFills.size(), fills.size());
            for (int j = 0; j < fetchedFills.size(); j++) {
                Fill expected = fetchedFills.get(j);
                Fill test = fills.get(j);

                assertEquals(expected.getNumber(), test.getNumber());
                assertEquals(expected.getValidity(), test.getValidity());
                assertEquals(expected.getBeamModes().size(), test.getBeamModes().size());
            }
        }
    }

    // helper methods

    public static List<ImmutableData> generateFillData() {
        List<ImmutableData> dataToPublish = new ArrayList<>();
        for (int i = 0; i < FILL_DATA_SIZE; i++) {
            Instant timestamp = FILL_DATA_PUBLISH_START.plus(FILL_DATA_PUBLISH_INTERVAL.multipliedBy(i));

            DataBuilder dataBuilder = createBasicData(FILL_DEVICE_NAME, TimeUtils.getNanosFromInstant(timestamp));
            dataBuilder.add(FILL_FIELD_NAME, i);
            dataToPublish.add(dataBuilder.build());

            if (i == DUPLICATE_DATA_FILL_NUMBER) { // create some extra fills to test merging algorithm
                for (int j = 0; j < DUPLICATE_DATA_FILLS_SIZE; j++) {
                    DataBuilder duplicateDataBuilder = createBasicData(FILL_DEVICE_NAME,
                            TimeUtils.getNanosFromInstant(timestamp.plusMillis(10L * (j + 1))));
                    duplicateDataBuilder.add(FILL_FIELD_NAME, i);
                    dataToPublish.add(duplicateDataBuilder.build());
                }
            }
        }
        return dataToPublish;
    }

    public static List<ImmutableData> generateBeamModeData() {
        List<String> availableModes = Arrays.asList("NO BEAM", "BEAM");

        List<ImmutableData> dataToPublish = new ArrayList<>();
        for (int i = 0; i < BMODE_DATA_SIZE; i++) {
            DataBuilder dataBuilder = createBasicData(BMODE_DEVICE_NAME,
                    TimeUtils.getNanosFromInstant(
                            BMODE_DATA_PUBLISH_START.plus(BMODE_DATA_PUBLISH_INTERVAL.multipliedBy(i))));
            dataBuilder.add(BMODE_FIELD_NAME, availableModes.get(i % availableModes.size()));
            dataToPublish.add(dataBuilder.build());
        }
        return dataToPublish;
    }

    private static DataBuilder createBasicData(String deviceName, long timestamp) {
        DataBuilder builder = ImmutableData.builder();
        builder.add(DEVICE_KEY_NAME, deviceName);
        builder.add(PROPERTY_KEY_NAME, XTIM_PROPERTY_NAME);
        builder.add(CLASS_KEY_NAME, XTIM_CLASS_NAME);
        builder.add(RECORD_TIMESTAMP, timestamp);
        builder.add(RECORD_VERSION, 0L);
        return builder;
    }

    private static void configureFillVariable() {
        Map<String, Object> entityKeys = ImmutableMap
                .of(DEVICE_KEY_NAME, FILL_DEVICE_NAME, PROPERTY_KEY_NAME, XTIM_PROPERTY_NAME);
        Entity entity = findEntityOrThrow(entityKeys);
        Variable variable = buildVariable(entity, FILL_VARIABLE_NAME, FILL_FIELD_NAME, VariableDeclaredType.NUMERIC);
        createOrConfigureVariable(variable);
    }

    private static void configureBeamModeVariable() {
        Map<String, Object> entityKeys = ImmutableMap
                .of(DEVICE_KEY_NAME, BMODE_DEVICE_NAME, PROPERTY_KEY_NAME, XTIM_PROPERTY_NAME);
        Entity entity = findEntityOrThrow(entityKeys);
        Variable variable = buildVariable(entity, BMODE_VARIABLE_NAME, BMODE_FIELD_NAME, VariableDeclaredType.TEXT);
        createOrConfigureVariable(variable);
    }

    private static Entity findEntityOrThrow(Map<String, Object> keyValues) {
        return entityService.findOne(
                Entities.suchThat().keyValues().eq(CMW_SYSTEM_SPEC, keyValues))
                .orElseThrow(() -> new IllegalArgumentException(
                        format("Could not find entity [ %s ]! Make sure that data were sent and metadata created for this target",
                                keyValues.get(DEVICE_KEY_NAME))));
    }

    private static Variable buildVariable(Entity entity, String variableName, String fieldName,
            VariableDeclaredType type) {
        VariableConfig config = VariableConfig.builder()
                .entityId(entity.getId())
                .variableName(variableName)
                .fieldName(fieldName).build();
        return Variable.builder().systemSpec(CMW_SYSTEM_SPEC).variableName(variableName)
                .declaredType(type).configs(ImmutableSortedSet.of(config)).build();
    }

    private static void createOrConfigureVariable(Variable variable) {
        Optional<Variable> fetchedVariable = variableService.findOne(Variables.suchThat()
                .systemId().eq(variable.getSystemSpec().getId()).and().variableName().eq(variable.getVariableName()));
        if (fetchedVariable.isPresent()) {
            //reconfigure existing to target currently used entity
            Variable updatedVariable = fetchedVariable.get().toBuilder().configs(variable.getConfigs()).build();
            variableService.update(updatedVariable);
        } else {
            variableService.create(variable);
        }
    }

    private synchronized void waitForData(long maxTime) throws InterruptedException {
        long start = System.currentTimeMillis();
        while (true) {
            if (WAITING_FOR_DATA_FAILED.get() || (System.currentTimeMillis() - start > maxTime)) {
                WAITING_FOR_DATA_FAILED.set(true);
                fail("Data was not processed in the required maxTime=" + maxTime + " ms");
            }
            if (isDataPresent()) {
                return;
            }
            log.info("Still no data visible, waiting more...");
            TimeUnit.SECONDS.sleep(4);
        }
    }

    private boolean isDataPresent() {
        Instant endTime = Instant.now();
        long fillsCount = getCount(FILL_DEVICE_NAME, FILL_DATA_PUBLISH_START, endTime);
        long modesCount = getCount(BMODE_DEVICE_NAME, BMODE_DATA_PUBLISH_START, endTime);

        return fillsCount == (FILL_DATA_SIZE + DUPLICATE_DATA_FILLS_SIZE) && modesCount == BMODE_DATA_SIZE;
    }

    private long getCount(String device, Instant startTime, Instant endTime) {
        return DataQuery.builder(sparkSession).byEntities().system(CMW_SYSTEM_SPEC.getName())
                .startTime(startTime).endTime(endTime)
                .entity()
                .keyValue(DEVICE_KEY_NAME, device)
                .keyValue(PROPERTY_KEY_NAME, XTIM_PROPERTY_NAME).build().count();
    }

}
