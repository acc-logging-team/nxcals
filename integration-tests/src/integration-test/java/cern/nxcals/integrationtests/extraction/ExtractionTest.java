package cern.nxcals.integrationtests.extraction;

import cern.cmw.data.DataFactory;
import cern.cmw.data.DiscreteFunctionList;
import cern.cmw.datax.DataBuilder;
import cern.cmw.datax.EntryType;
import cern.cmw.datax.ImmutableData;
import cern.cmw.datax.ImmutableEntry;
import cern.nxcals.api.config.SparkContext;
import cern.nxcals.api.converters.SparkRowToImmutableDataConverter;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.EntityQuery;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.extraction.data.builders.DevicePropertyDataQuery;
import cern.nxcals.api.extraction.data.builders.ParameterDataQuery;
import cern.nxcals.api.extraction.metadata.EntityService;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import cern.nxcals.api.ingestion.Publisher;
import cern.nxcals.api.ingestion.PublisherFactory;
import cern.nxcals.api.utils.TimeUtils;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.DependsOn;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static cern.nxcals.common.SystemFields.NXC_EXTR_TIMESTAMP;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VARIABLE_NAME;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.CLASS_KEY;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.DEVICE_KEY;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.PROPERTY_KEY;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.RECORD_TIMESTAMP_KEY;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.RECORD_VERSION_KEY;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.SYSTEM;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.SYSTEM_2;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.SYSTEM_2_ENTITY_KEY;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.SYSTEM_2_PARTITION_KEY;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.SYSTEM_2_TIME_KEY;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.checkColumns;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.checkColumnsInVariableQuery;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.findEntityOrThrow;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.findVariableOrThrow;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.getDefaultFieldsForCMWEntityQueryWith;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.getDefaultFieldsForVariableQuery;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.getFieldData;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.getFieldsForEntityQueryIn;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.getVariableData;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.registerVariable;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.registerVariableToOneFieldWithOneConfig;
import static java.time.temporal.ChronoUnit.HOURS;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = SparkContext.class)
@SpringBootApplication
@Slf4j
@DependsOn("kerberos")
public class ExtractionTest {
    static {
        System.setProperty("kafka.producer.linger.ms", "0");
        //        Please leave this one just in case you want to run those test from IDE for debugging
        //        Don't forget to check if hadoop-dev jar is included in build.gradle!!!

        //        String username = System.getProperty("user.name");
        //        System.setProperty("kerberos.principal", username);
        //        System.setProperty("kerberos.keytab", "/opt/<user>/.keytab".replace("<user>", username));
        //        System.setProperty("service.url",
        //                "https://nxcals-<user>-1.cern.ch:19093".replace("<user>", username)); //Do not use localhost here.
        //        System.setProperty("kafka.producer.bootstrap.servers",
        //                "https://nxcals-<user>-3.cern.ch:9092,https://nxcals-<user>-4.cern.ch:9092,https://nxcals-<user>-5.cern.ch:9092"
        //                        .replace("<user>", username));
    }

    private static final int MAX_WAIT_TIME = 10;
    public static final int RECORDS_NUMBER = 100;

    private static final Instant FROM_TIME = Instant.now();
    private static final Instant END_TIME = FROM_TIME.plus(RECORDS_NUMBER, ChronoUnit.MILLIS);

    private static final String RANDOM_SUFFIX = UUID.randomUUID().toString();

    private static final String DEVICE1 = "test_device_1_" + RANDOM_SUFFIX;
    private static final String DEVICE2 = "test_device_2_" + RANDOM_SUFFIX;
    private static final String DEVICE3 = "test_device_3_" + RANDOM_SUFFIX;
    private static final String DEVICE_OLD = "test_old_device_" + RANDOM_SUFFIX;
    private static final String DEVICE_ACTIVE = "test_active_device_" + RANDOM_SUFFIX;
    private static final String DEVICE5 = "test_device_5_" + RANDOM_SUFFIX;

    private static final String DEVICE_MULTI_VARIABLES_1 = "test_device_multi_variables_1";
    private static final String DEVICE_MULTI_VARIABLES_2 = "test_device_multi_variables_2";

    private static final String PROPERTY1 = "test_property_1_" + RANDOM_SUFFIX;
    private static final String PROPERTY2 = "test_property_2_" + RANDOM_SUFFIX;
    private static final String PROPERTY3 = "test_property_3_" + RANDOM_SUFFIX; // for switching schema (diff fieldName)
    private static final String PROPERTY4 = "test_property_4_"
            + RANDOM_SUFFIX; // for testing variable config bounds, will contain only 1 value (to generate meta-info)
    private static final String PROPERTY5 = "test_property_5_" + RANDOM_SUFFIX;
    private static final String PROPERTY_PROMOTABLE =
            "promotable_test_property" + RANDOM_SUFFIX; // for promotable field types
    private static final String PROPERTY_LIKE_PATTERN = "test_property%" + RANDOM_SUFFIX;

    private static final String PROPERTY_TEST = "static_property";

    private static final String CLASS1 = "test_class";
    private static final String SPECIFICATION1 = "test_specification";

    private static final Map<String, Object> ENTITY_KEY_VALUES1 = ImmutableMap.of(DEVICE_KEY, DEVICE1, PROPERTY_KEY,
            PROPERTY1);
    private static final Map<String, Object> ENTITY_KEY_VALUES2 = ImmutableMap.of(DEVICE_KEY, DEVICE1, PROPERTY_KEY,
            PROPERTY2);
    private static final Map<String, Object> ENTITY_KEY_VALUES3 = ImmutableMap.of(DEVICE_KEY, DEVICE2, PROPERTY_KEY,
            PROPERTY3);
    private static final Map<String, Object> ENTITY_KEY_VALUES4 = ImmutableMap.of(DEVICE_KEY, DEVICE2, PROPERTY_KEY,
            PROPERTY4);
    private static final Map<String, Object> ENTITY_KEY_VALUES_PROMOTABLE = ImmutableMap.of(DEVICE_KEY, DEVICE1,
            PROPERTY_KEY, PROPERTY_PROMOTABLE);

    private static final Map<String, Object> ENTITY_KEY_VALUES_MULTI_VARIABLES_TEST_DEVICE_1 = ImmutableMap.of(
            DEVICE_KEY, DEVICE_MULTI_VARIABLES_1, PROPERTY_KEY, PROPERTY_TEST);
    private static final Map<String, Object> ENTITY_KEY_VALUES_MULTI_VARIABLES_TEST_DEVICE_2 = ImmutableMap.of(
            DEVICE_KEY, DEVICE_MULTI_VARIABLES_2, PROPERTY_KEY, PROPERTY_TEST);

    // device for TEST-CMW
    private static final Map<String, Object> ENTITY_KEY_VALUES_OLD = ImmutableMap.of(DEVICE_KEY, DEVICE_OLD,
            PROPERTY_KEY, PROPERTY1);
    // device for MOCK-SYSTEM
    private static final Map<String, Object> ENTITY_KEY_VALUES_ACTIVE = ImmutableMap.of(DEVICE_KEY, DEVICE_ACTIVE);

    private static final String FIELD1 = "field1";
    private static final String FIELD2 = "field2";
    private static final String FIELD3_GENERAL = "field3";
    private static final String FIELD3_OLD = "field3_old";
    private static final String FIELD3_NEW = "field3_new";
    private static final String FIELD3_ALIAS = "field3_alias";
    private static final String FIELD4 = "field4";
    private static final String FIELD_NULLABLE = "fieldNullable";
    private static final String FIELD_PROMOTABLE = "field_promotable";
    private static final String FIELD_NON_EXISTING = "field_non_existing";
    private static final String FIELD_TEST_PREFIX = "field_test";
    private static final String FIELD_WITH_ILLEGAL_CHARS = "illegal:field";
    private static final String DFL_FIELD = "DFL_FIELD";
    private static final String DFLA_FIELD = "DFLA_FIELD";

    private static final String VARIABLE1 = String.join(":", DEVICE1, PROPERTY1, FIELD1);
    private static final String VARIABLE2 = String.join(":", DEVICE1, PROPERTY2, FIELD1);
    private static final String VARIABLE3 = String.join(":", DEVICE2, PROPERTY3, FIELD3_GENERAL);
    // for testing variable configs bounds (field1 + field4)
    private static final String VARIABLE1_4 = String.join(":", DEVICE2, PROPERTY4, "field1_4");
    private static final String SYSTEM_2_VARIABLE = String.join(":", DEVICE_ACTIVE, FIELD2);
    private static final String VARIABLE_PROMOTABLE = String.join(":", DEVICE1, PROPERTY_PROMOTABLE,
            FIELD_PROMOTABLE);
    private static final String VARIABLE_NULLABLE = String.join(":", "NON:NULL", DEVICE1, PROPERTY1, FIELD_NULLABLE);
    private static final String VARIABLE_ON_OLD_FIELD = String.join(":", DEVICE2, PROPERTY3, FIELD3_OLD);
    private static final String VARIABLE_ON_NON_EXISTING_FIELD = String.join(":", DEVICE2, PROPERTY3,
            FIELD_NON_EXISTING);
    private static final String VARIABLE_ON_MULTIPLE_SYSTEMS = String.join(":", "TEST", "VARIABLE_" + RANDOM_SUFFIX,
            "MULTI_SYSTEM");
    private static final String VARIABLE_NAME_MULTI_VARIABLE_PREFIX = "TEST:MULTI:VARIABLE";
    private static final String VARIABLE_WITH_ONE_CONFIG_HAVING_ILLEGAL_FIELD = String.join(":", "TEST",
            "VARIABLE1_" + RANDOM_SUFFIX, "ILLEGAL_FIELD");
    private static final String VARIABLE_WITH_MULTIPLE_CONFIGS_HAVING_ILLEGAL_FIELD = String.join(":", "TEST",
            "VARIABLE2_" + RANDOM_SUFFIX, "ILLEGAL_FIELD");
    private static final String NOT_NULL_FIELD_ONE_VARIABLE = "NN_F_1_VARIABLE_" + RANDOM_SUFFIX;
    private static final String NOT_NULL_FIELD_TWO_VARIABLE = "NN_F_2_VARIABLE_" + RANDOM_SUFFIX;

    private static final String STRING_FIELD_PREFIX = "string_";
    private static final String FIELD_LIKE_PATTERN = "field%";
    private static final String VARIABLE_LIKE_PATTERN = String.join(":", DEVICE1, PROPERTY_LIKE_PATTERN,
            FIELD_LIKE_PATTERN);

    private static final List<Integer> EXPECTED_SIMPLE_VALUES = IntStream.range(0, RECORDS_NUMBER).boxed()
            .collect(Collectors.toList());
    private static final List<Integer> EXPECTED_SIMPLE_NEG_VALUES = IntStream.range(0, RECORDS_NUMBER).boxed()
            .map(i -> -i).collect(Collectors.toList());
    private static final List<Integer> EXPECTED_NON_NULL_VALUES = IntStream.range(0, RECORDS_NUMBER).boxed()
            .filter(i -> i % 2 == 0).collect(Collectors.toList());
    private static final List<Integer> EXPECTED_OLD_FIELD_VALUES = IntStream.range(0, RECORDS_NUMBER / 2).boxed()
            .collect(Collectors.toList());

    private static final Map<String, String> MULTI_VARIABLE_DEFINITIONS_DEVICE_1 = generateMultiVariableDefinitionsByRange(
            0, 505, VARIABLE_NAME_MULTI_VARIABLE_PREFIX, FIELD_TEST_PREFIX);
    private static final Map<String, String> MULTI_VARIABLE_DEFINITIONS_DEVICE_2 = generateMultiVariableDefinitionsByRange(
            505, 1010, VARIABLE_NAME_MULTI_VARIABLE_PREFIX, FIELD_TEST_PREFIX);

    private static final Set<String> MULTI_VARIABLE_DEFINITION_KEYS = Sets.union(
            MULTI_VARIABLE_DEFINITIONS_DEVICE_1.keySet(), MULTI_VARIABLE_DEFINITIONS_DEVICE_2.keySet());

    public static final String TEST_VALUE_OLD_SYSTEM = "test_value_old_system";
    public static final String TEST_VALUE_ACTIVE_SYSTEM = "test_value_active_system";
    private static final Set<String> EXPECTED_MULTI_SYSTEM_VALUES = Sets.newHashSet(TEST_VALUE_OLD_SYSTEM,
            TEST_VALUE_ACTIVE_SYSTEM);

    private static final VariableService variableService = ServiceClientFactory.createVariableService();
    private static final EntityService entityService = ServiceClientFactory.createEntityService();
    private static final SystemSpecService systemService = ServiceClientFactory.createSystemSpecService();

    private static final SystemSpec SYSTEM_SPEC = systemService.findByName(SYSTEM)
            .orElseThrow(() -> new IllegalArgumentException("No such system"));
    private static final SystemSpec SYSTEM_SPEC_2 = systemService.findByName(SYSTEM_2)
            .orElseThrow(() -> new IllegalArgumentException("No such system"));

    private static final String ALIAS = "ALIAS";
    private static final String ALIAS1 = "ALIAS1";
    private static final String ALIAS2 = "ALIAS2";
    private static final String NON_EXISTING_DEVICE = "NON_EXISTING_DEVICE";
    private static final String NON_EXISTING_PROPERTY = "NON_EXISTING_PROPERTY";
    private static final String NON_EXISTING_VARIABLE = "NON_EXISTING_VARIABLE";

    @Autowired
    private SparkSession sparkSession;
    private static volatile boolean dataVisible = false;
    private boolean waitingForDataFailed = false;

    //Beware of Java11 - micros are now present, which makes those tests fail if we don't round.
    static long roundNanos(Instant time) {
        return time.toEpochMilli() * 1000_000;
    }

    private static final TimeWindow timeWindow = TimeWindow.between(roundNanos(FROM_TIME),
            TimeUtils.getNanosFromInstant(END_TIME));

    @BeforeAll
    public static void setUpData() {
        Publisher<ImmutableData> publisher = PublisherFactory.newInstance()
                .createPublisher(SYSTEM, Function.identity());
        Publisher<ImmutableData> publisherMockSystem = PublisherFactory.newInstance()
                .createPublisher(SYSTEM_2, Function.identity());
        log.info("Sending data to NXCALS");
        try {
            for (int i = 0; i < RECORDS_NUMBER; ++i) {
                long timestamp = roundNanos(FROM_TIME.plus(i, ChronoUnit.MILLIS));
                publisher.publish(createSimpleData(i, DEVICE1, PROPERTY1, timestamp));
                publisher.publish(createSimpleData(i, DEVICE1, PROPERTY2, timestamp));
                publisher.publish(createSimpleData(i, DEVICE3, PROPERTY1, timestamp));
                publisher.publish(createDataWithChangedFieldName(i, timestamp));
                publisher.publish(createDataWithPromotableFieldTypes(i, timestamp));
                if (i < RECORDS_NUMBER / 2) {
                    publisher.publish(createDataForOldSystem(timestamp));
                } else {
                    publisherMockSystem.publish(createDataForActiveSystem(i, timestamp));
                }
                publisher.publish(createDataWithIllegalField(i, DEVICE5, PROPERTY5, timestamp));
            }
            // publish only one value
            long timestamp = roundNanos(FROM_TIME.plus(1, ChronoUnit.MILLIS));
            publisher.publish(
                    buildSingleIntDataRecord(DEVICE2, PROPERTY4, CLASS1, Collections.singleton(FIELD4), 1, timestamp));

            publisher.publish(buildSingleIntDataRecord(DEVICE_MULTI_VARIABLES_1, PROPERTY_TEST, CLASS1,
                    MULTI_VARIABLE_DEFINITIONS_DEVICE_1.values(), 2, timestamp));
            publisher.publish(buildSingleIntDataRecord(DEVICE_MULTI_VARIABLES_2, PROPERTY_TEST, CLASS1,
                    MULTI_VARIABLE_DEFINITIONS_DEVICE_2.values(), 2, timestamp));

        } catch (Exception e) {
            log.error("Error sending values to NXCALS", e);
            fail("Error sending values to NXCALS");
        }
        registerVariableToOneFieldWithOneConfig(ENTITY_KEY_VALUES1, FIELD1, VARIABLE1, entityService, variableService,
                SYSTEM_SPEC);
        registerVariableToOneFieldWithOneConfig(ENTITY_KEY_VALUES2, FIELD1, VARIABLE2, entityService, variableService,
                SYSTEM_SPEC);
        registerVariableToOneFieldWithOneConfig(ENTITY_KEY_VALUES3, FIELD3_OLD, VARIABLE_ON_OLD_FIELD, entityService,
                variableService, SYSTEM_SPEC);
        registerVariableToOneFieldWithOneConfig(ENTITY_KEY_VALUES3, FIELD_NON_EXISTING, VARIABLE_ON_NON_EXISTING_FIELD,
                entityService, variableService, SYSTEM_SPEC);
        registerVariableToOneFieldWithOneConfig(ENTITY_KEY_VALUES_PROMOTABLE, FIELD_PROMOTABLE, VARIABLE_PROMOTABLE,
                entityService, variableService, SYSTEM_SPEC);
        registerVariableToOneFieldWithOneConfig(ENTITY_KEY_VALUES1, FIELD_NULLABLE, VARIABLE_NULLABLE, entityService,
                variableService, SYSTEM_SPEC);
        registerVariableWithTwoConfigs(ENTITY_KEY_VALUES3, FIELD3_OLD, FIELD3_NEW, VARIABLE3);
        registerVariableForVariableConfigTesting(ENTITY_KEY_VALUES1, ENTITY_KEY_VALUES4, FIELD1, FIELD4, VARIABLE1_4);
        registerVariableWithMultiSystemConfig(FIELD1, VARIABLE_ON_MULTIPLE_SYSTEMS);

        assertTrue(MULTI_VARIABLE_DEFINITION_KEYS.size() > 1000); //ensure variables more than 1K
        registerMultipleVariablesOnEntity(MULTI_VARIABLE_DEFINITIONS_DEVICE_1,
                ENTITY_KEY_VALUES_MULTI_VARIABLES_TEST_DEVICE_1);
        registerMultipleVariablesOnEntity(MULTI_VARIABLE_DEFINITIONS_DEVICE_2,
                ENTITY_KEY_VALUES_MULTI_VARIABLES_TEST_DEVICE_2);

        // not null variable names case
        registerVariableToOneFieldWithOneConfig(ENTITY_KEY_VALUES1, FIELD1, NOT_NULL_FIELD_ONE_VARIABLE, entityService,
                variableService, SYSTEM_SPEC);
        registerVariableToOneFieldWithOneConfig(ImmutableMap.of(DEVICE_KEY, DEVICE3, PROPERTY_KEY, PROPERTY1), FIELD2,
                NOT_NULL_FIELD_TWO_VARIABLE, entityService, variableService, SYSTEM_SPEC);

        registerVariableToOneFieldWithOneConfig(ENTITY_KEY_VALUES1, FIELD_WITH_ILLEGAL_CHARS,
                VARIABLE_WITH_ONE_CONFIG_HAVING_ILLEGAL_FIELD, entityService, variableService, SYSTEM_SPEC);

        registerVariableWithTwoConfigs(ENTITY_KEY_VALUES1, FIELD_WITH_ILLEGAL_CHARS, FIELD1,
                VARIABLE_WITH_MULTIPLE_CONFIGS_HAVING_ILLEGAL_FIELD);
        registerVariableToOneFieldWithOneConfig(ENTITY_KEY_VALUES_ACTIVE, FIELD2, SYSTEM_2_VARIABLE, entityService,
                variableService, SYSTEM_SPEC_2);
    }

    private static ImmutableData createSimpleData(int i, String device, String property, long timestamp) {
        DataBuilder builder = ImmutableData.builder();
        builder.add(DEVICE_KEY, device);
        builder.add(PROPERTY_KEY, property);
        builder.add(CLASS_KEY, CLASS1);
        builder.add(RECORD_VERSION_KEY, 0L);
        builder.add(RECORD_TIMESTAMP_KEY, timestamp);
        builder.add(FIELD1, i);
        builder.add(FIELD2, -i);
        builder.add(DFL_FIELD, createDFL());
        builder.add(DFLA_FIELD, createDFLArray());

        if (i % 2 == 0) {
            builder.add(FIELD_NULLABLE, i);
        } else {
            builder.add(ImmutableEntry.ofNull(FIELD_NULLABLE, EntryType.INT32));
        }
        return builder.build();
    }

    private static DiscreteFunctionList[] createDFLArray() {
        return new DiscreteFunctionList[] { createDFL(), createDFL() };
    }

    private static DiscreteFunctionList createDFL() {
        return DataFactory.createDiscreteFunctionList(
                DataFactory.createDiscreteFunction(new double[] { 1, 2 }, new double[] { 3, 4 }),
                DataFactory.createDiscreteFunction(new double[] { 1, 2 }, new double[] { 3, 4 }));
    }

    private static ImmutableData createDataWithIllegalField(int i, String device, String property, long timestamp) {
        DataBuilder builder = ImmutableData.builder();
        builder.add(DEVICE_KEY, device);
        builder.add(PROPERTY_KEY, property);
        builder.add(CLASS_KEY, CLASS1);
        builder.add(RECORD_VERSION_KEY, 0L);
        builder.add(RECORD_TIMESTAMP_KEY, timestamp);
        builder.add(FIELD_WITH_ILLEGAL_CHARS, -i);
        return builder.build();
    }

    private static ImmutableData createDataWithChangedFieldName(int i, long timestamp) {
        DataBuilder builder = ImmutableData.builder();
        builder.add(DEVICE_KEY, DEVICE2);
        builder.add(PROPERTY_KEY, PROPERTY3);
        builder.add(CLASS_KEY, CLASS1);
        builder.add(RECORD_VERSION_KEY, 0L);
        builder.add(RECORD_TIMESTAMP_KEY, timestamp);
        if (i < RECORDS_NUMBER / 2) {
            builder.add(FIELD3_OLD, i);
        } else {
            builder.add(FIELD3_NEW, i);
        }
        return builder.build();
    }

    private static ImmutableData buildSingleIntDataRecord(String deviceName, String propertyName, String className,
            Collection<String> fieldNames, int data, long timestamp) {
        DataBuilder builder = ImmutableData.builder();
        builder.add(DEVICE_KEY, deviceName);
        builder.add(PROPERTY_KEY, propertyName);
        builder.add(CLASS_KEY, className);
        builder.add(RECORD_VERSION_KEY, 0L);
        builder.add(RECORD_TIMESTAMP_KEY, timestamp);
        for (String fieldName : fieldNames) {
            builder.add(fieldName, data);
        }
        return builder.build();
    }

    private static ImmutableData createDataWithPromotableFieldTypes(int i, long timestamp) {
        DataBuilder builder = ImmutableData.builder();
        builder.add(DEVICE_KEY, DEVICE1);
        builder.add(PROPERTY_KEY, PROPERTY_PROMOTABLE);
        builder.add(CLASS_KEY, CLASS1);
        builder.add(RECORD_VERSION_KEY, 0L);
        builder.add(RECORD_TIMESTAMP_KEY, timestamp);
        if (i < RECORDS_NUMBER / 2) {
            builder.add(FIELD_PROMOTABLE, i);
        } else {
            builder.add(FIELD_PROMOTABLE, STRING_FIELD_PREFIX + i);
        }
        return builder.build();
    }

    private static ImmutableData createDataForOldSystem(long timestamp) {
        DataBuilder builder = ImmutableData.builder();
        builder.add(DEVICE_KEY, DEVICE_OLD);
        builder.add(PROPERTY_KEY, PROPERTY1);
        builder.add(CLASS_KEY, CLASS1);
        builder.add(RECORD_VERSION_KEY, 0L);
        builder.add(RECORD_TIMESTAMP_KEY, timestamp);
        builder.add(FIELD1, TEST_VALUE_OLD_SYSTEM);
        return builder.build();
    }

    private static ImmutableData createDataForActiveSystem(int i, long timestamp) {
        DataBuilder builder = ImmutableData.builder();
        builder.add(SYSTEM_2_ENTITY_KEY, DEVICE_ACTIVE);
        builder.add(SYSTEM_2_PARTITION_KEY, SPECIFICATION1);
        builder.add(SYSTEM_2_TIME_KEY, timestamp);
        builder.add(FIELD1, TEST_VALUE_ACTIVE_SYSTEM);
        builder.add(FIELD2, i);
        return builder.build();
    }

    private static Map<String, String> generateMultiVariableDefinitionsByRange(int startInclusive, int endExclusive,
            String variableNamePrefix, String fieldNamePrefix) {
        return IntStream.range(startInclusive, endExclusive).mapToObj(String::valueOf).collect(
                Collectors.toMap(i -> String.join(":", variableNamePrefix, i),
                        i -> String.join("_", fieldNamePrefix, i)));
    }

    private static void registerVariableForVariableConfigTesting(Map<String, Object> keyValues1,
            Map<String, Object> keyValues2, String fieldName1, String fieldName2, String variableName) {
        Entity entity1 = findEntity(keyValues1);
        Entity entity2 = findEntity(keyValues2);
        // will have half of the field1 values
        VariableConfig varConfData1 = VariableConfig.builder().entityId(entity1.getId()).fieldName(fieldName1).validity(
                        TimeWindow.before(FROM_TIME.plus(RECORDS_NUMBER / 2, ChronoUnit.MILLIS).toEpochMilli() * 1000_000))
                .build();
        // will have 0 values for the specified TimeWindow
        VariableConfig varConfData2 = VariableConfig.builder().entityId(entity2.getId()).fieldName(fieldName2).validity(
                        TimeWindow.after(FROM_TIME.plus(RECORDS_NUMBER / 2, ChronoUnit.MILLIS).toEpochMilli() * 1000_000))
                .build();
        SortedSet<VariableConfig> varConfSet = ImmutableSortedSet.of(varConfData1, varConfData2);

        registerVariable(variableName, varConfSet, variableService, SYSTEM_SPEC);
    }

    private static Entity findEntity(Map<String, Object> keyValues, SystemSpec system) {
        return findEntityOrThrow(keyValues, entityService, system);
    }

    private static Entity findEntity(Map<String, Object> keyValues) {
        return findEntity(keyValues, SYSTEM_SPEC);
    }

    private static Variable findVariable(String systemName, String variableName) {
        return findVariableOrThrow(variableName, variableService, systemName);
    }

    private static void registerVariableWithTwoConfigs(Map<String, Object> keyValues, String fieldName1,
            String fieldName2, String variableName) {
        Entity entity = findEntity(keyValues);

        long timestamp = FROM_TIME.plus(RECORDS_NUMBER / 2, ChronoUnit.MILLIS).toEpochMilli() * 1000_000;

        VariableConfig varConfData1 = VariableConfig.builder().entityId(entity.getId()).fieldName(fieldName1)
                .validity(TimeWindow.before(timestamp)).build();
        VariableConfig varConfData2 = VariableConfig.builder().entityId(entity.getId()).fieldName(fieldName2)
                .validity(TimeWindow.after(timestamp)).build();
        SortedSet<VariableConfig> varConfSet = new TreeSet<>();
        varConfSet.add(varConfData1);
        varConfSet.add(varConfData2);

        registerVariable(variableName, varConfSet, variableService, SYSTEM_SPEC);
    }

    private static void registerVariableWithMultiSystemConfig(String fieldName, String variableName) {
        long timestamp = FROM_TIME.plus(RECORDS_NUMBER / 2, ChronoUnit.MILLIS).toEpochMilli() * 1000_000;

        Entity oldEntity = findEntity(ENTITY_KEY_VALUES_OLD, SYSTEM_SPEC);
        Entity activeEntity = findEntity(ENTITY_KEY_VALUES_ACTIVE, SYSTEM_SPEC_2);

        VariableConfig oldVarConfData = VariableConfig.builder().entityId(oldEntity.getId()).fieldName(fieldName)
                .validity(TimeWindow.before(timestamp)).build();
        VariableConfig activeVarConfData = VariableConfig.builder().entityId(activeEntity.getId()).fieldName(fieldName)
                .validity(TimeWindow.after(timestamp)).build();
        SortedSet<VariableConfig> varConfSet = new TreeSet<>();
        varConfSet.add(oldVarConfData);
        varConfSet.add(activeVarConfData);

        registerVariable(variableName, varConfSet, variableService, SYSTEM_SPEC_2);
    }

    private static void registerMultipleVariablesOnEntity(Map<String, String> variableDefinitions,
            Map<String, Object> keyValues) {
        Set<Variable> fetchedVariables = new HashSet<>(
                variableService.findAll(Variables.suchThat().variableName().in(variableDefinitions.keySet())));
        Set<String> fetchedVariableNames = fetchedVariables.stream().map(Variable::getVariableName)
                .collect(Collectors.toSet());
        for (String variableName : Sets.difference(variableDefinitions.keySet(), fetchedVariableNames)) {
            fetchedVariables.add(
                    registerVariableToOneFieldWithOneConfig(keyValues, variableDefinitions.get(variableName),
                            variableName,
                            entityService, variableService, SYSTEM_SPEC));
        }
        assertEquals(variableDefinitions.size(), fetchedVariables.size());
    }

    @BeforeEach
    public void waitForData() throws InterruptedException {
        if (!dataVisible) {
            //Have to wait 30 seconds in order for the data to be processed and visible in NXCALS extraction
            log.info("Waiting max " + MAX_WAIT_TIME + " minutes for the data to be visible in NXCALS...");
            waitForData(TimeUnit.MINUTES.toMillis(MAX_WAIT_TIME));
            log.info("Ok, data should be processed now, proceeding with tests");
            dataVisible = true;
        } else {
            log.info("Data already checked");
        }
    }

    private synchronized void waitForData(long maxTime) throws InterruptedException {
        long start = System.currentTimeMillis();

        while (true) {
            if (waitingForDataFailed || (System.currentTimeMillis() - start > maxTime)) {
                waitingForDataFailed = true;
                fail("Data was not processed in the required maxTime=" + maxTime + " ms");
            }
            if (isDataPresent()) {
                return;
            }

            log.info("Still no data visible, waiting more...");
            TimeUnit.SECONDS.sleep(1);
        }
    }

    private boolean isDataPresent() {
        return getCount(DEVICE1, PROPERTY1) == RECORDS_NUMBER && getCount(DEVICE1, PROPERTY2) == RECORDS_NUMBER
                && getCount(DEVICE2, PROPERTY3) == RECORDS_NUMBER
                && (getCount(DEVICE_OLD, PROPERTY1) + getCountForSystem2(DEVICE_ACTIVE)) == RECORDS_NUMBER
                && getCount(DEVICE2, PROPERTY4) == 1 && (getCount(DEVICE_MULTI_VARIABLES_1, PROPERTY_TEST) > 0) && (
                getCount(DEVICE_MULTI_VARIABLES_2, PROPERTY_TEST) > 0);
    }

    private long getCount(String device, String property) {
        long start = roundNanos(FROM_TIME);
        long end = roundNanos(FROM_TIME.plus(RECORDS_NUMBER - 1L, ChronoUnit.MILLIS));
        long count = DataQuery.builder(sparkSession).byEntities().system(SYSTEM).startTime(start).endTime(end).entity()
                .keyValueLike(DEVICE_KEY, device).keyValue(PROPERTY_KEY, property).build().count();
        log.info("Count for sytem {}, {}/{} from {} to {} : {}", SYSTEM, device, property, start, end, count);
        return count;
    }

    private long getCountForSystem2(String device) {
        long start = roundNanos(FROM_TIME);
        long end = roundNanos(FROM_TIME.plus(RECORDS_NUMBER - 1L, ChronoUnit.MILLIS));
        long count = DataQuery.builder(sparkSession).byEntities().system(SYSTEM_2).startTime(start).endTime(end)
                .entity().keyValueLike(DEVICE_KEY, device).build().count();
        log.info("Count for sytem {}, {} from {} to {} : {}", SYSTEM_2, device, start, end, count);
        return count;
    }

    static Stream<Function<SparkSession, Dataset<Row>>> queriesForSystemDevicePropertyIn(String system, String device,
            String property, TimeWindow timeWindow) {
        Map<String, Object> deviceProperty = Map.of(DEVICE_KEY, device, PROPERTY_KEY, property);
        return Stream.of(
                session -> DataQuery.builder(session).byEntities().system(system).startTime(timeWindow.getStartTime())
                        .endTime(timeWindow.getEndTime()).entity().keyValue(DEVICE_KEY, device)
                        .keyValue(PROPERTY_KEY, property).build(),
                session -> DataQuery.builder(session).entities().system(system).keyValuesEq(deviceProperty)
                        .timeWindow(timeWindow).build(),
                session -> DataQuery.builder(session).entities().system(system).idEq(findEntity(deviceProperty).getId())
                        .timeWindow(timeWindow).build(),
                session -> DataQuery.builder(session).entities().idEq(findEntity(deviceProperty).getId())
                        .timeWindow(timeWindow).build(),
                session -> DataQuery.builder(session).entities().system(system)
                        .idIn(Set.of(findEntity(deviceProperty).getId())).timeWindow(timeWindow).build(),
                session -> DataQuery.builder(session).entities()
                        .idIn(Set.of(findEntity(deviceProperty).getId())).timeWindow(timeWindow).build(),
                session -> DataQuery.builder(session).entities().system(system).idIn(findEntity(deviceProperty).getId())
                        .timeWindow(timeWindow).build(),
                session -> DataQuery.getFor(session, timeWindow, findEntity(deviceProperty)),
                session -> DataQuery.getFor(session, timeWindow, SYSTEM, deviceProperty),
                session -> DevicePropertyDataQuery.getFor(session, timeWindow, SYSTEM, device, property),
                session -> ParameterDataQuery.builder(session).system(system).deviceEq(device).propertyEq(property)
                        .timeWindow(timeWindow).build(),
                session -> ParameterDataQuery.getFor(session, system, device, property, timeWindow)
        );
    }

    static Stream<Function<SparkSession, Dataset<Row>>> queriesForSystemDevice1Property1In(TimeWindow timeWindow) {
        return queriesForSystemDevicePropertyIn(SYSTEM, DEVICE1, PROPERTY1, timeWindow);
    }

    static Stream<Object[]> queriesForSystemDevice1Property1() {
        return withSourceName(queriesForSystemDevice1Property1In(timeWindow), "queriesForSystemDevice1Property1");
    }

    static Stream<Object[]> queriesLikeForExistingDevice1Property1AndNonExistingDevice() {
        String device = NON_EXISTING_DEVICE;
        String property = NON_EXISTING_PROPERTY;

        Entity existingEntity = findEntity(ENTITY_KEY_VALUES1);
        Stream<Function<SparkSession, Dataset<Row>>> functionStream = Stream.of(
                session -> DataQuery.builder(session).byEntities().system(SYSTEM).startTime(timeWindow.getStartTime())
                        .endTime(timeWindow.getEndTime()).entity().keyValue(DEVICE_KEY, device)
                        .keyValueLike(PROPERTY_KEY, property).entity().keyValue(DEVICE_KEY, DEVICE1)
                        .keyValue(PROPERTY_KEY, PROPERTY1).build(),
                session -> DevicePropertyDataQuery.builder(session).system(SYSTEM).startTime(timeWindow.getStartTime())
                        .endTime(timeWindow.getEndTime()).entity().device(device).propertyLike(property).entity()
                        .device(DEVICE1).property(PROPERTY1).build(),
                session -> DataQuery.builder(session).entities().system(SYSTEM)
                        .keyValuesLike(Map.of(DEVICE_KEY, device, PROPERTY_KEY, property))
                        .keyValuesEq(ENTITY_KEY_VALUES1).timeWindow(timeWindow).build(),
                session -> DataQuery.builder(session).entities().system(SYSTEM)
                        .keyValuesLike(Map.of(DEVICE_KEY, device, PROPERTY_KEY, property))
                        .idEq(existingEntity.getId()).timeWindow(timeWindow).build(),
                session -> DataQuery.builder(session).entities().idEq(existingEntity.getId())
                        .system(SYSTEM).keyValuesLike(Map.of(DEVICE_KEY, device, PROPERTY_KEY, property))
                        .timeWindow(timeWindow).build(),
                session -> DataQuery.builder(session).entities().system(SYSTEM_2).idEq(existingEntity.getId())
                        .keyValuesLike(Map.of(DEVICE_KEY, device)).timeWindow(timeWindow).build(),
                session -> ParameterDataQuery.builder(session).system(SYSTEM).deviceEq(device).propertyLike(property)
                        .deviceEq(DEVICE1).propertyEq(PROPERTY1).timeWindow(timeWindow).build(),
                session -> DataQuery.getFor(session, timeWindow, SYSTEM,
                        new EntityQuery(Map.of(), Map.of(DEVICE_KEY, device, PROPERTY_KEY, property)),
                        new EntityQuery(ENTITY_KEY_VALUES1))
        );

        return withSourceName(functionStream, "queriesLikeForExistingDevice1Property1AndNonExistingDevice");
    }

    @ParameterizedTest
    @MethodSource("queriesForSystemDevice1Property1")
    public void shouldExtractAndConvertDiscreteFunctionListAndDiscreteFunctionListArray(
            Function<SparkSession, Dataset<Row>> query, String description) {
        //given data published

        //when - passed from generator
        Row row = query.apply(sparkSession).first();
        //then
        ImmutableData immutableData = SparkRowToImmutableDataConverter.convert(row);

        DiscreteFunctionList discreteFunctionList = immutableData.getEntry(DFL_FIELD)
                .getAs(EntryType.DISCRETE_FUNCTION_LIST);
        assertEquals(2, discreteFunctionList.getSize());
        DiscreteFunctionList[] discreteFunctionListArray = immutableData.getEntry(DFLA_FIELD)
                .getAs(EntryType.DISCRETE_FUNCTION_LIST_ARRAY);
        assertEquals(2, discreteFunctionListArray.length);
    }

    @ParameterizedTest
    @MethodSource({ "queriesForSystemDevice1Property1", "queriesLikeForExistingDevice1Property1AndNonExistingDevice" })
    public void shouldExtractPublishedDataWithLikeForOneDeviceProp(Function<SparkSession, Dataset<Row>> query,
            String description) {
        // given
        //  [__record_version__, field1, property, __record_timestamp__, field2, device, class, nxcals_entity_id]
        Set<String> expectedColumns = getEntity1QueryColumns(FIELD1, FIELD2, FIELD_NULLABLE);
        // when
        Dataset<Row> dataset = query.apply(sparkSession);
        // then
        assertEquals(RECORDS_NUMBER, dataset.count());

        checkColumns(expectedColumns, dataset);

        List<Object> retrieved = getFieldData(dataset, DEVICE1, PROPERTY1, FIELD1);
        assertEquals(EXPECTED_SIMPLE_VALUES, retrieved);
    }

    @Test
    public void shouldHaveFieldsFromExistingEntitiesWhenNonEmptyDatasets() {
        // given
        Set<String> fields = getEntity1QueryColumns(FIELD1, FIELD2, FIELD_NULLABLE);

        // when
        Dataset<Row> dataset = DataQuery.builder(sparkSession).entities().system(SYSTEM_2)
                .idEq(findEntity(ENTITY_KEY_VALUES1).getId()).timeWindow(timeWindow).build();

        // then
        checkColumns(fields, dataset);
    }

    static Stream<Function<SparkSession, Dataset<Row>>> queriesOfEmptyDatasetFromTwoSystems() {
        TimeWindow emptyDatasetTimeWindow = TimeWindow.between(0, 1000);
        Stream<Function<SparkSession, Dataset<Row>>> functionStream = Stream.of(
                spark -> DataQuery.builder(spark).entities().system(SYSTEM_2)
                        .idEq(findEntity(ENTITY_KEY_VALUES1).getId()).timeWindow(emptyDatasetTimeWindow).build(),
                spark -> DataQuery.builder(spark).entities()
                        .idEq(findEntity(ENTITY_KEY_VALUES_ACTIVE, SYSTEM_SPEC_2).getId())
                        .idEq(findEntity(ENTITY_KEY_VALUES1).getId()).timeWindow(emptyDatasetTimeWindow).build()
        );
        return functionStream;
    }

    @ParameterizedTest
    @MethodSource("queriesOfEmptyDatasetFromTwoSystems")
    public void shouldHaveFieldsFromAllSystemsWhenEmptyDatasets(Function<SparkSession, Dataset<Row>> query) {
        // given
        Set<String> fields = Sets.union(getFieldsForEntityQueryIn(SYSTEM_2), getFieldsForEntityQueryIn(SYSTEM));

        // when
        Dataset<Row> dataset = query.apply(sparkSession);

        // then
        checkColumns(fields, dataset);
    }

    private static Set<String> getEntity1QueryColumns(String... fields) {
        Set<String> expectedColumns = getDefaultFieldsForCMWEntityQueryWith(DFL_FIELD, DFLA_FIELD);
        expectedColumns.addAll(asList(fields));
        return expectedColumns;
    }

    static Stream<Function<SparkSession, Dataset<Row>>> device1Property1WithAliasQueries() {
        String fieldAlias = ALIAS;
        Entity entity = findEntity(ENTITY_KEY_VALUES1);
        return Stream.of(
                session -> DataQuery.builder(session).byEntities().system(SYSTEM)
                        .startTime(timeWindow.getStartTime()).endTime(timeWindow.getEndTime())
                        .fieldAliases(fieldAlias, asList(FIELD1, FIELD2, FIELD_NULLABLE)).entity()
                        .keyValue(DEVICE_KEY, DEVICE1).keyValue(PROPERTY_KEY, PROPERTY1).build(),
                session -> DevicePropertyDataQuery.builder(session).system(SYSTEM)
                        .startTime(timeWindow.getStartTime()).endTime(timeWindow.getEndTime())
                        .fieldAliases(fieldAlias, asList(FIELD1, FIELD2, FIELD_NULLABLE)).entity()
                        .device(DEVICE1).property(PROPERTY1).build(),
                session -> DataQuery.builder(session).entities().system(SYSTEM).keyValuesEq(ENTITY_KEY_VALUES1)
                        .timeWindow(timeWindow)
                        .fieldAliases(fieldAlias, new LinkedHashSet<>(asList(FIELD1, FIELD2, FIELD_NULLABLE))).build(),
                session -> DataQuery.builder(session).entities().system(SYSTEM).idEq(entity.getId())
                        .timeWindow(timeWindow)
                        .fieldAliases(fieldAlias, new LinkedHashSet<>(asList(FIELD1, FIELD2, FIELD_NULLABLE))).build(),
                session -> DataQuery.builder(session).entities().idEq(entity.getId())
                        .timeWindow(timeWindow)
                        .fieldAliases(fieldAlias, new LinkedHashSet<>(asList(FIELD1, FIELD2, FIELD_NULLABLE))).build(),
                session -> DataQuery.builder(session).entities().system(SYSTEM).idIn(Set.of(entity.getId()))
                        .timeWindow(timeWindow)
                        .fieldAliases(fieldAlias, new LinkedHashSet<>(asList(FIELD1, FIELD2, FIELD_NULLABLE))).build(),
                session -> DataQuery.builder(session).entities().system(SYSTEM).idIn(entity.getId())
                        .timeWindow(timeWindow)
                        .fieldAliases(fieldAlias, new LinkedHashSet<>(asList(FIELD1, FIELD2, FIELD_NULLABLE))).build(),
                session -> ParameterDataQuery.builder(session).system(SYSTEM).deviceEq(DEVICE1).propertyEq(PROPERTY1)
                        .timeWindow(timeWindow)
                        .fieldAliases(fieldAlias, new LinkedHashSet<>(asList(FIELD1, FIELD2, FIELD_NULLABLE))).build()
        );

    }

    @ParameterizedTest
    @MethodSource("device1Property1WithAliasQueries")
    public void shouldGetOnlyFirstColumnForAlias(Function<SparkSession, Dataset<Row>> query) {
        // given
        String fieldAlias = ALIAS;
        Set<String> expectedColumns = getEntity1QueryColumns(fieldAlias);
        // when
        Dataset<Row> dataset = query.apply(sparkSession);
        // then
        assertEquals(RECORDS_NUMBER, dataset.count());

        checkColumns(expectedColumns, dataset);

        List<Object> retrieved = getFieldData(dataset, DEVICE1, PROPERTY1, fieldAlias);
        assertEquals(EXPECTED_SIMPLE_VALUES, retrieved);
    }

    static Stream<Function<SparkSession, Dataset<Row>>> device1Property1WithAliasesQueries() {
        String fieldAlias1 = ALIAS1;
        String fieldAlias2 = ALIAS2;

        Entity entity = findEntity(ENTITY_KEY_VALUES1);
        return Stream.of(
                session -> DataQuery.builder(session).byEntities().system(SYSTEM)
                        .startTime(timeWindow.getStartTime()).endTime(timeWindow.getEndTime())
                        .fieldAliases(fieldAlias1, asList(FIELD1, FIELD2))
                        .fieldAliases(fieldAlias2, asList(FIELD2, FIELD_NULLABLE)).entity()
                        .keyValueLike(DEVICE_KEY, DEVICE1)
                        .keyValue(PROPERTY_KEY, PROPERTY1).build(),
                session -> DevicePropertyDataQuery.builder(session).system(SYSTEM)
                        .startTime(timeWindow.getStartTime()).endTime(timeWindow.getEndTime())
                        .fieldAliases(fieldAlias1, asList(FIELD1, FIELD2))
                        .fieldAliases(fieldAlias2, asList(FIELD2, FIELD_NULLABLE)).entity()
                        .device(DEVICE1).property(PROPERTY1).build(),
                session -> DataQuery.builder(session).entities().system(SYSTEM).keyValuesEq(ENTITY_KEY_VALUES1)
                        .timeWindow(timeWindow).fieldAliases(fieldAlias1, new LinkedHashSet<>(asList(FIELD1, FIELD2)))
                        .fieldAliases(fieldAlias2, new LinkedHashSet<>(asList(FIELD2, FIELD_NULLABLE))).build(),
                session -> DataQuery.builder(session).entities().system(SYSTEM).idEq(entity.getId())
                        .timeWindow(timeWindow).fieldAliases(fieldAlias1, new LinkedHashSet<>(asList(FIELD1, FIELD2)))
                        .fieldAliases(fieldAlias2, new LinkedHashSet<>(asList(FIELD2, FIELD_NULLABLE))).build(),
                session -> ParameterDataQuery.builder(session).system(SYSTEM).deviceEq(DEVICE1).propertyEq(PROPERTY1)
                        .timeWindow(timeWindow).fieldAliases(fieldAlias1, new LinkedHashSet<>(asList(FIELD1, FIELD2)))
                        .fieldAliases(fieldAlias2, new LinkedHashSet<>(asList(FIELD2, FIELD_NULLABLE))).build()
        );
    }

    @ParameterizedTest
    @MethodSource("device1Property1WithAliasesQueries")
    public void shouldGetFirstExistingColumnForEachAlias(Function<SparkSession, Dataset<Row>> query) {
        String fieldAlias1 = ALIAS1;
        String fieldAlias2 = ALIAS2;
        Set<String> expectedColumns = getEntity1QueryColumns(fieldAlias1, fieldAlias2);

        // when
        Dataset<Row> dataset = query.apply(sparkSession);
        // then
        assertEquals(RECORDS_NUMBER, dataset.count());

        checkColumns(expectedColumns, dataset);

        List<Object> retrieved1 = getFieldData(dataset, DEVICE1, PROPERTY1, fieldAlias1);
        assertEquals(EXPECTED_SIMPLE_VALUES, retrieved1);

        List<Object> retrieved2 = getFieldData(dataset, DEVICE1, PROPERTY1, fieldAlias2);
        assertEquals(EXPECTED_SIMPLE_NEG_VALUES, retrieved2);
    }

    static Stream<Function<SparkSession, Dataset<Row>>> device1Property1WithSimpleAliasesQueries() {
        String fieldAlias1 = ALIAS1;
        String fieldAlias2 = ALIAS2;

        Entity entity = findEntity(ENTITY_KEY_VALUES1);
        return Stream.of(
                session -> DataQuery.builder(session).byEntities().system(SYSTEM)
                        .startTime(timeWindow.getStartTime()).endTime(timeWindow.getEndTime())
                        .fieldAlias(fieldAlias1, FIELD1)
                        .fieldAliases(fieldAlias2, asList(FIELD1, FIELD_NULLABLE)).entity()
                        .keyValue(DEVICE_KEY, DEVICE1)
                        .keyValue(PROPERTY_KEY, PROPERTY1).build(),
                session -> DevicePropertyDataQuery.builder(session).system(SYSTEM)
                        .startTime(timeWindow.getStartTime()).endTime(timeWindow.getEndTime())
                        .fieldAlias(fieldAlias1, FIELD1)
                        .fieldAliases(fieldAlias2, asList(FIELD1, FIELD_NULLABLE)).entity()
                        .device(DEVICE1).property(PROPERTY1).build(),
                session -> DataQuery.builder(session).entities().system(SYSTEM).keyValuesEq(ENTITY_KEY_VALUES1)
                        .timeWindow(timeWindow).fieldAliases(fieldAlias1, new LinkedHashSet<>(List.of(FIELD1)))
                        .fieldAliases(fieldAlias2, new LinkedHashSet<>(asList(FIELD1, FIELD_NULLABLE))).build(),
                session -> DataQuery.builder(session).entities().system(SYSTEM).idEq(entity.getId())
                        .timeWindow(timeWindow).fieldAliases(fieldAlias1, new LinkedHashSet<>(List.of(FIELD1)))
                        .fieldAliases(fieldAlias2, new LinkedHashSet<>(asList(FIELD1, FIELD_NULLABLE))).build(),
                session -> ParameterDataQuery.builder(session).system(SYSTEM).deviceEq(DEVICE1).propertyEq(PROPERTY1)
                        .timeWindow(timeWindow).fieldAliases(fieldAlias1, new LinkedHashSet<>(List.of(FIELD1)))
                        .fieldAliases(fieldAlias2, new LinkedHashSet<>(asList(FIELD1, FIELD_NULLABLE))).build()
        );
    }

    @ParameterizedTest
    @MethodSource("device1Property1WithSimpleAliasesQueries")
    public void shouldGetTwoColumnsForTwoAliases(Function<SparkSession, Dataset<Row>> query) {
        String fieldAlias1 = ALIAS1;
        String fieldAlias2 = ALIAS2;
        Set<String> expectedColumns = getEntity1QueryColumns(fieldAlias1, fieldAlias2, FIELD2);

        Dataset<Row> dataset = query.apply(sparkSession);
        // then
        assertEquals(RECORDS_NUMBER, dataset.count());

        checkColumns(expectedColumns, dataset);

        List<Object> retrieved1 = getFieldData(dataset, DEVICE1, PROPERTY1, fieldAlias1);
        assertEquals(EXPECTED_SIMPLE_VALUES, retrieved1);

        List<Object> retrieved2 = getFieldData(dataset, DEVICE1, PROPERTY1, fieldAlias2);
        assertEquals(EXPECTED_SIMPLE_VALUES, retrieved2);

    }

    static Stream<Function<SparkSession, Dataset<Row>>> queriesForDevice1PropertyLike1And2() {
        return Stream.of(
                session -> DataQuery.builder(session).byEntities().system(SYSTEM)
                        .startTime(timeWindow.getStartTime()).endTime(timeWindow.getEndTime())
                        .entity()
                        .keyValue(DEVICE_KEY, DEVICE1)
                        .keyValueLike(PROPERTY_KEY, PROPERTY_LIKE_PATTERN).build(),
                session -> DataQuery.builder(session).entities().system(SYSTEM).keyValuesLike(Map.of(
                        DEVICE_KEY, DEVICE1, PROPERTY_KEY, PROPERTY_LIKE_PATTERN
                )).timeWindow(timeWindow).build(),
                session -> DevicePropertyDataQuery.builder(session).system(SYSTEM).startTime(timeWindow.getStartTime())
                        .endTime(timeWindow.getEndTime()).entity().device(DEVICE1).propertyLike(PROPERTY_LIKE_PATTERN)
                        .build(),
                session -> ParameterDataQuery.builder(session).system(SYSTEM).deviceEq(DEVICE1)
                        .propertyLike(PROPERTY_LIKE_PATTERN).timeWindow(timeWindow).build()
        );
    }

    @ParameterizedTest
    @MethodSource("queriesForDevice1PropertyLike1And2")
    public void shouldExtractPublishedDataWithLikeForTwoDeviceProp(Function<SparkSession, Dataset<Row>> query) {
        // given
        //  [__record_version__, property, __record_timestamp__, field1, field2, device, class, nxcals_entity_id]
        Set<String> expectedColumns = getEntity1QueryColumns(FIELD1, FIELD2, FIELD_NULLABLE);

        // when
        Dataset<Row> dataset = query.apply(sparkSession);
        //then
        assertEquals(RECORDS_NUMBER * 2, dataset.count());

        checkColumns(expectedColumns, dataset);

        List<Object> retrieved1 = getFieldData(dataset, DEVICE1, PROPERTY1, FIELD1);
        assertEquals(EXPECTED_SIMPLE_VALUES, retrieved1);

        List<Object> retrieved2 = getFieldData(dataset, DEVICE1, PROPERTY1, FIELD2);
        assertEquals(EXPECTED_SIMPLE_NEG_VALUES, retrieved2);
    }

    static Stream<Function<SparkSession, Dataset<Row>>> queriesForSystemVariableIn(String system, String variableName,
            TimeWindow timeWindow) {
        return Stream.of(
                session -> DataQuery.builder(session).byVariables().system(system)
                        .startTime(timeWindow.getStartTime()).endTime(timeWindow.getEndTime()).variable(variableName)
                        .build(),
                session -> DataQuery.builder(session).variables().system(system).nameEq(variableName)
                        .timeWindow(timeWindow).build(),
                session -> DataQuery.builder(session).variables().system(system)
                        .idEq(findVariable(system, variableName).getId())
                        .timeWindow(timeWindow).build(),
                session -> DataQuery.builder(session).variables()
                        .idEq(findVariable(system, variableName).getId())
                        .timeWindow(timeWindow).build(),
                session -> DataQuery.builder(session).variables().system(system)
                        .idIn(findVariable(system, variableName).getId())
                        .timeWindow(timeWindow).build(),
                session -> DataQuery.builder(session).variables().system(system.equals(SYSTEM) ? SYSTEM_2 : SYSTEM)
                        .idIn(findVariable(system, variableName).getId()).timeWindow(timeWindow).build(),
                session -> DataQuery.builder(session).variables().system(system)
                        .idIn(Set.of(findVariable(system, variableName).getId()))
                        .timeWindow(timeWindow).build(),
                session -> DataQuery.getFor(session, timeWindow, findVariable(system, variableName)),
                session -> DataQuery.getFor(session, timeWindow, system, variableName)
        );
    }

    static Stream<Function<SparkSession, Dataset<Row>>> queriesForSystemVariableIn(String variableName,
            TimeWindow timeWindow) {
        return queriesForSystemVariableIn(SYSTEM, variableName, timeWindow);
    }

    static Stream<Function<SparkSession, Dataset<Row>>> queriesForSystemVariable(String variableName) {
        return queriesForSystemVariableIn(variableName, timeWindow);
    }

    static Stream<Object[]> queriesForSystemVariable1() {
        return withSourceName(queriesForSystemVariable(VARIABLE1), "queriesForSystemVariable1");
    }

    static Stream<Function<SparkSession, Dataset<Row>>> queriesLikeForSystemVariableIn(String variableName,
            TimeWindow timeWindow) {
        return Stream.of(
                session -> DataQuery.builder(session).byVariables().system(SYSTEM)
                        .startTime(timeWindow.getStartTime()).endTime(timeWindow.getEndTime())
                        .variableLike(variableName)
                        .build(),
                session -> DataQuery.builder(session).variables().system(SYSTEM).nameLike(variableName)
                        .timeWindow(timeWindow).build(),
                session -> DataQuery.getFor(session, timeWindow, SYSTEM, List.of(), List.of(variableName))
        );
    }

    static Stream<Function<SparkSession, Dataset<Row>>> queriesLikeForSystemVariable(String variableName) {
        return queriesLikeForSystemVariableIn(variableName, timeWindow);
    }

    static Stream<Object[]> queriesLikeForExistingVariable1AndNonExistingVariable() {
        String existingVariable = VARIABLE1;
        String notExistingVariable = NON_EXISTING_VARIABLE;
        long existingVariableId = findVariable(SYSTEM, existingVariable).getId();
        Stream<Function<SparkSession, Dataset<Row>>> functionStream = Stream.of(
                session -> DataQuery.builder(session).byVariables().system(SYSTEM)
                        .startTime(timeWindow.getStartTime()).endTime(timeWindow.getEndTime())
                        .variableLike(notExistingVariable)
                        .variable(existingVariable).build(),
                session -> DataQuery.builder(session).variables().system(SYSTEM)
                        .nameEq(existingVariable).nameLike(notExistingVariable)
                        .timeWindow(timeWindow).build(),
                session -> DataQuery.builder(session).variables().system(SYSTEM)
                        .idEq(existingVariableId).nameLike(notExistingVariable)
                        .timeWindow(timeWindow).build(),
                session -> DataQuery.builder(session).variables().idEq(existingVariableId)
                        .system(SYSTEM).nameLike(notExistingVariable)
                        .timeWindow(timeWindow).build(),
                session -> DataQuery.builder(session).variables().system(SYSTEM)
                        .idIn(existingVariableId).nameLike(notExistingVariable)
                        .timeWindow(timeWindow).build(),
                session -> DataQuery.getFor(session, timeWindow, SYSTEM, List.of(existingVariable),
                        List.of(notExistingVariable))
        );

        return withSourceName(functionStream, "queriesLikeForExistingVariable1AndNonExistingVariable");
    }

    @ParameterizedTest(name = "{index} - from source: {1}")
    @MethodSource({ "queriesForSystemVariable1", "queriesLikeForExistingVariable1AndNonExistingVariable" })
    public void shouldExtractPublishedDataForOneVariable(Function<SparkSession, Dataset<Row>> query,
            String description) {
        //given

        //when
        Dataset<Row> dataset = query.apply(sparkSession);
        //then
        assertEquals(RECORDS_NUMBER, dataset.count());

        checkColumnsInVariableQuery(dataset);

        List<Object> retrieved = getVariableData(dataset, VARIABLE1);
        assertEquals(EXPECTED_SIMPLE_VALUES, retrieved);
    }

    static Stream<Function<SparkSession, Dataset<Row>>> queriesForSystemVariableNullable() {
        return queriesForSystemVariable(VARIABLE_NULLABLE);
    }

    @ParameterizedTest
    @MethodSource("queriesForSystemVariableNullable")
    public void shouldExtractDataWithoutNullsForNullableFieldVariable(Function<SparkSession, Dataset<Row>> query) {
        //given

        //when
        Dataset<Row> dataset = query.apply(sparkSession);
        //then
        assertEquals(RECORDS_NUMBER / 2, dataset.count());

        checkColumnsInVariableQuery(dataset);

        List<Object> retrieved = getVariableData(dataset, VARIABLE_NULLABLE);
        assertEquals(EXPECTED_NON_NULL_VALUES, retrieved);
    }

    static Stream<Function<SparkSession, Dataset<Row>>> queriesForSystemVariableOnOldField() {
        return queriesForSystemVariable(VARIABLE_ON_OLD_FIELD);
    }

    @ParameterizedTest
    @MethodSource("queriesForSystemVariableOnOldField")
    public void shouldExtractDataOnlyFromEntityResourcesContainVariableFieldInSchema(
            Function<SparkSession, Dataset<Row>> query) {
        //given

        //when
        Dataset<Row> dataset = query.apply(sparkSession);
        //then
        assertEquals(RECORDS_NUMBER / 2, dataset.count());

        checkColumnsInVariableQuery(dataset);

        List<Object> retrieved = getVariableData(dataset, VARIABLE_ON_OLD_FIELD);
        assertEquals(EXPECTED_OLD_FIELD_VALUES, retrieved);
    }

    static Stream<Function<SparkSession, Dataset<Row>>> queriesForSystemAndTwoVariables(String variable1Name,
            String variable2Name) {
        return Stream.of(
                session -> DataQuery.builder(session).byVariables().system(SYSTEM)
                        .startTime(timeWindow.getStartTime()).endTime(timeWindow.getEndTime()).variable(variable1Name)
                        .variable(variable2Name)
                        .build(),
                session -> DataQuery.builder(session).byVariables().system(SYSTEM)
                        .startTime(timeWindow.getStartTime()).endTime(timeWindow.getEndTime())
                        .variables(asList(variable1Name, variable2Name))
                        .build(),
                session -> DataQuery.builder(session).variables().system(SYSTEM).nameEq(variable1Name)
                        .nameEq(variable2Name)
                        .timeWindow(timeWindow).build(),
                session -> DataQuery.builder(session).variables().system(SYSTEM).nameIn(variable1Name, variable2Name)
                        .timeWindow(timeWindow).build(),
                session -> DataQuery.builder(session).variables().system(SYSTEM)
                        .nameIn(asList(variable1Name, variable2Name))
                        .timeWindow(timeWindow).build(),
                session -> DataQuery.builder(session).variables().system(SYSTEM).nameEq(variable1Name)
                        .idEq(findVariable(SYSTEM, variable2Name).getId())
                        .timeWindow(timeWindow).build(),
                session -> DataQuery.builder(session).variables().system(SYSTEM)
                        .idEq(findVariable(SYSTEM, variable1Name).getId())
                        .idEq(findVariable(SYSTEM, variable2Name).getId())
                        .timeWindow(timeWindow).build(),
                session -> DataQuery.builder(session).variables()
                        .idEq(findVariable(SYSTEM, variable1Name).getId())
                        .system(SYSTEM)
                        .idEq(findVariable(SYSTEM, variable2Name).getId())
                        .timeWindow(timeWindow).build(),
                session -> DataQuery.builder(session).variables().system(SYSTEM)
                        .idIn(Set.of(findVariable(SYSTEM, variable1Name).getId(),
                                findVariable(SYSTEM, variable2Name).getId()))
                        .timeWindow(timeWindow).build(),
                session -> DataQuery.getFor(session, timeWindow, SYSTEM, variable1Name, variable2Name),
                session -> DataQuery.getFor(session, timeWindow, SYSTEM, asList(variable1Name, variable2Name))
        );
    }

    static Stream<Function<SparkSession, Dataset<Row>>> queriesForSystemVariable1And2() {
        return queriesForSystemAndTwoVariables(VARIABLE1, VARIABLE2);
    }

    @ParameterizedTest
    @MethodSource("queriesForSystemVariable1And2")
    public void shouldExtractPublishedDataWithLikeForTwoVariables(Function<SparkSession, Dataset<Row>> query) {
        //given
        List<Integer> expected = EXPECTED_SIMPLE_VALUES;
        //when
        Dataset<Row> dataset = query.apply(sparkSession);
        //then
        assertEquals(RECORDS_NUMBER * 2, dataset.count());

        checkColumnsInVariableQuery(dataset);

        List<Object> retrieved1 = getVariableData(dataset, VARIABLE1);
        assertEquals(expected, retrieved1);

        List<Object> retrieved2 = getVariableData(dataset, VARIABLE2);
        assertEquals(expected, retrieved2);
    }

    @Test
    public void shouldExtractPublishedDataTwoVariablesFromDifferentSystems() {
        //given
        List<Integer> expected = EXPECTED_SIMPLE_VALUES;
        //when
        Dataset<Row> dataset = DataQuery.builder(sparkSession).variables()
                .idEq(findVariable(SYSTEM, VARIABLE1).getId())
                .system(SYSTEM).idEq(findVariable(SYSTEM_2, SYSTEM_2_VARIABLE).getId())
                .timeWindow(timeWindow).build();
        //then
        assertEquals(RECORDS_NUMBER * 1.5, dataset.count());

        checkColumnsInVariableQuery(dataset);

        List<Object> retrieved1 = getVariableData(dataset, VARIABLE1);
        assertEquals(expected, retrieved1);

        List<Object> retrieved2 = getVariableData(dataset, SYSTEM_2_VARIABLE);
        assertEquals(expected.subList(50, 100), retrieved2);
    }

    static Stream<Object[]> queriesLikeForNonExistingDevice() {
        String notExistingDevice = NON_EXISTING_DEVICE;
        String wildcard = "%";
        Stream<Function<SparkSession, Dataset<Row>>> functionStream = Stream.of(
                session -> DataQuery.builder(session).byEntities().system(SYSTEM)
                        .startTime(timeWindow.getStartTime()).endTime(timeWindow.getEndTime()).entity()
                        .keyValue(DEVICE_KEY, notExistingDevice)
                        .keyValueLike(PROPERTY_KEY, wildcard).build(),
                session -> DataQuery.builder(session).entities().system(SYSTEM)
                        .keyValuesLike(Map.of(DEVICE_KEY, notExistingDevice, PROPERTY_KEY, wildcard))
                        .timeWindow(timeWindow).build(),
                session -> DataQuery.getFor(session, timeWindow, SYSTEM,
                        new EntityQuery(Map.of(DEVICE_KEY, notExistingDevice), Map.of(PROPERTY_KEY, wildcard)))
        );
        return withSourceName(functionStream, "queriesLikeForNonExistingDevice");
    }

    static Stream<Object[]> device1Property1QueriesWithTimeWindowBeforeData() {
        return withSourceName(queriesForSystemDevice1Property1In(
                        TimeWindow.between(FROM_TIME.minus(3, HOURS), FROM_TIME.minus(2, HOURS))),
                "device1Property1QueriesWithTimeWindowBeforeData");
    }

    static <T> Function<T, Object[]> addSourceName(String sourceName) {
        return f -> new Object[] { f, sourceName };
    }

    static <T> Stream<Object[]> withSourceName(Stream<T> stream, String sourceName) {
        return stream.map(addSourceName(sourceName));
    }

    @ParameterizedTest(name = "{index} - from source: {1}")
    @MethodSource({ "queriesLikeForNonExistingDevice", "device1Property1QueriesWithTimeWindowBeforeData" })
    public void shouldGetEmptyKeyValuesDataset(
            Function<SparkSession, Dataset<Row>> query, String description) {
        // given
        Set<String> expectedColumns = getDefaultFieldsForCMWEntityQueryWith();
        // when
        Dataset<Row> dataset = query.apply(sparkSession);
        // then
        assertEquals(0, dataset.count());
        checkColumns(expectedColumns, dataset);
    }

    static Stream<Object[]> device1Property1QueriesWithTimeWindowAfterData() {
        return withSourceName(queriesForSystemDevice1Property1In(
                        TimeWindow.between(FROM_TIME.plus(2, HOURS), FROM_TIME.plus(3, HOURS))),
                "device1Property1QueriesWithTimeWindowAfterData");
    }

    @ParameterizedTest()
    @MethodSource("device1Property1QueriesWithTimeWindowAfterData")
    public void shouldGetEmptyKeyValuesDataset(
            Function<SparkSession, Dataset<Row>> query) {
        // given
        Set<String> expectedColumns = getEntity1QueryColumns(FIELD1, FIELD2, FIELD_NULLABLE);
        // when
        Dataset<Row> dataset = query.apply(sparkSession);
        // then
        assertEquals(0, dataset.count());
        checkColumns(expectedColumns, dataset);
    }

    static Stream<Function<SparkSession, Dataset<Row>>> queriesForDeviceProperty(String device, String property) {
        return queriesForSystemDevicePropertyIn(SYSTEM, device, property, timeWindow);
    }

    static Stream<Object[]> queriesForNonExistingDeviceAndProperty() {
        return withSourceName(queriesForDeviceProperty(NON_EXISTING_DEVICE, NON_EXISTING_PROPERTY),
                "queriesForNonExistingDeviceAndProperty");
    }

    static Stream<Object[]> queriesForOneExistingAndOneNonExistingDevice() {
        String device = NON_EXISTING_DEVICE;
        String property = NON_EXISTING_PROPERTY;
        Map<String, Object> deviceProperty = Map.of(DEVICE_KEY, device, PROPERTY_KEY, property);
        Entity existingEntity = findEntity(ENTITY_KEY_VALUES1);
        Stream<Function<SparkSession, Dataset<Row>>> streamOfFunctions = Stream.of(
                session -> DataQuery.builder(session).byEntities().system(SYSTEM).startTime(timeWindow.getStartTime())
                        .endTime(timeWindow.getEndTime()).entity().keyValue(DEVICE_KEY, device)
                        .keyValue(PROPERTY_KEY, property).entity().keyValue(DEVICE_KEY, DEVICE1)
                        .keyValue(PROPERTY_KEY, PROPERTY1).build(),
                session -> DataQuery.builder(session).entities().system(SYSTEM)
                        .keyValuesEq(deviceProperty)
                        .keyValuesEq(ENTITY_KEY_VALUES1)
                        .timeWindow(timeWindow).build(),
                session -> DataQuery.builder(session).entities().system(SYSTEM)
                        .keyValuesEq(deviceProperty)
                        .idEq(existingEntity.getId())
                        .timeWindow(timeWindow).build(),
                session -> DataQuery.builder(session).entities().system(SYSTEM)
                        .keyValuesEq(deviceProperty)
                        .idIn(existingEntity.getId())
                        .timeWindow(timeWindow).build(),
                session -> DataQuery.getFor(session, timeWindow, SYSTEM,
                        new EntityQuery(deviceProperty),
                        new EntityQuery(ENTITY_KEY_VALUES1)),
                session -> DataQuery.getFor(session, timeWindow, SYSTEM, deviceProperty, ENTITY_KEY_VALUES1),
                session -> DevicePropertyDataQuery.builder(session).system(SYSTEM).startTime(timeWindow.getStartTime())
                        .endTime(timeWindow.getEndTime()).entity().device(device).property(property).entity()
                        .device(DEVICE1).property(PROPERTY1).build(),
                session -> ParameterDataQuery.builder(session).system(SYSTEM).deviceEq(device).propertyEq(property)
                        .deviceEq(DEVICE1).propertyEq(PROPERTY1).timeWindow(timeWindow).build()
        );
        return withSourceName(streamOfFunctions, "queriesForOneExistingAndOneNonExistingDevice");
    }

    static Stream<Object[]> queriesForNonExistingProperty() {
        return withSourceName(queriesForDeviceProperty(DEVICE1, NON_EXISTING_PROPERTY),
                "queriesForNonExistingProperty");
    }

    static Stream<Object[]> queriesForNonExistingDevice() {
        return withSourceName(queriesForDeviceProperty(NON_EXISTING_DEVICE, PROPERTY1),
                "queriesForNonExistingDevice");
    }

    static Stream<Object[]> queriesForNonExistingVariable() {
        return withSourceName(queriesForSystemVariable(NON_EXISTING_VARIABLE), "queriesForNonExistingVariable");
    }

    static Stream<Object[]> queriesForExistingAndNonExistingVariable() {
        return withSourceName(queriesForSystemAndTwoVariables(VARIABLE1, NON_EXISTING_VARIABLE),
                "queriesForExistingAndNonExistingVariable");
    }

    @ParameterizedTest(name = "{index} - from source: {1}")
    @MethodSource({ "queriesForExistingAndNonExistingVariable", "queriesForNonExistingVariable",
            "queriesForNonExistingDevice", "queriesForNonExistingProperty",
            "queriesForOneExistingAndOneNonExistingDevice", "queriesForNonExistingDeviceAndProperty" })
    public void shouldThrowWhenOneOfTheVariablesOrEntitiesIsNotFound(
            Function<SparkSession, Dataset<Row>> query, String description) {
        assertThrows(IllegalArgumentException.class,
                () -> query.apply(sparkSession));
    }

    static Stream<Object[]> queriesLikeForNonExistingVariable() {
        return withSourceName(queriesLikeForSystemVariable(NON_EXISTING_VARIABLE),
                "queriesLikeForNonExistingVariable");
    }

    static Stream<Object[]> variable1QueriesBeforeData() {
        return withSourceName(queriesForSystemVariableIn(VARIABLE1,
                        TimeWindow.between(FROM_TIME.minus(3, HOURS), FROM_TIME.minus(2, HOURS))),
                "variable1QueriesBeforeData");
    }

    static Stream<Object[]> variable1QueriesAfterData() {
        return withSourceName(queriesForSystemVariableIn(VARIABLE1,
                        TimeWindow.between(FROM_TIME.minus(3, HOURS), FROM_TIME.minus(2, HOURS))),
                "variable1QueriesAfterData");
    }

    static Stream<Object[]> queriesForSystemVariableOnNonExistingField() {
        return withSourceName(queriesForSystemVariable(VARIABLE_ON_NON_EXISTING_FIELD),
                "queriesForSystemVariableOnNonExistingField");
    }

    @ParameterizedTest(name = "{index} - from source: {1}")
    @MethodSource({ "variable1QueriesBeforeData", "variable1QueriesAfterData", "queriesLikeForNonExistingVariable",
            "queriesForSystemVariableOnNonExistingField" })
    public void shouldExtractEmptyVariableDataset(
            Function<SparkSession, Dataset<Row>> query, String description) {
        // given
        // when
        Dataset<Row> dataset = query.apply(sparkSession);
        // then
        assertEquals(0, dataset.count());
        checkColumnsInVariableQuery(dataset);
    }

    static Stream<Function<SparkSession, Dataset<Row>>> queriesForDevice2Property3() {
        return queriesForDeviceProperty(DEVICE2, PROPERTY3);
    }

    @ParameterizedTest
    @MethodSource("queriesForDevice2Property3")
    public void shouldExtractPublishedDataWithChangedFieldNameByEntities(Function<SparkSession, Dataset<Row>> query) {
        //given
        //  [__record_version__, property, __record_timestamp__, field3_old, field3_new, device, class, nxcals_entity_id]
        Set<String> expectedColumns = getDefaultFieldsForCMWEntityQueryWith(FIELD3_OLD, FIELD3_NEW);

        List<Integer> expectedFieldOld = IntStream.range(0, RECORDS_NUMBER / 2).boxed().collect(Collectors.toList());
        List<Integer> expectedFieldNew = IntStream.range(RECORDS_NUMBER / 2, RECORDS_NUMBER).boxed()
                .collect(Collectors.toList());
        //when
        Dataset<Row> dataset = query.apply(sparkSession);
        //then
        assertEquals(RECORDS_NUMBER, dataset.count());

        checkColumns(expectedColumns, dataset);

        List<Object> retrievedFieldOld = getFieldData(dataset, DEVICE2, PROPERTY3, FIELD3_OLD);
        List<Object> retrievedFieldNew = getFieldData(dataset, DEVICE2, PROPERTY3, FIELD3_NEW);

        assertEquals(expectedFieldOld, retrievedFieldOld);
        assertEquals(expectedFieldNew, retrievedFieldNew);
    }

    static Stream<Function<SparkSession, Dataset<Row>>> queriesForSystemVariable3() {
        return queriesForSystemVariable(VARIABLE3);
    }

    @ParameterizedTest
    @MethodSource("queriesForSystemVariable3")
    public void shouldExtractPublishedDataWithChangedFieldNameByVariables(Function<SparkSession, Dataset<Row>> query) {
        //given
        //when
        Dataset<Row> dataset = query.apply(sparkSession);
        //then
        assertEquals(RECORDS_NUMBER, dataset.count());

        checkColumnsInVariableQuery(dataset);

        List<Object> retrieved = getVariableData(dataset, VARIABLE3);
        assertEquals(EXPECTED_SIMPLE_VALUES, retrieved);
    }

    static Stream<Function<SparkSession, Dataset<Row>>> queriesForSystemVariable14() {
        return queriesForSystemVariable(VARIABLE1_4);
    }

    @ParameterizedTest
    @MethodSource("queriesForSystemVariable14")
    public void shouldTreatVariableConfigDateToAsOpen(Function<SparkSession, Dataset<Row>> query) {
        //given
        List<Integer> expected = IntStream.range(0, RECORDS_NUMBER / 2).boxed().collect(Collectors.toList());
        //when
        Dataset<Row> dataset = query.apply(sparkSession);
        //then
        assertEquals(RECORDS_NUMBER / 2, dataset.count());

        checkColumnsInVariableQuery(dataset);

        List<Object> retrieved = getVariableData(dataset, VARIABLE1_4);
        assertEquals(expected, retrieved);
    }

    static Stream<Object[]> queriesForSystemEntityPromotable() {
        return withSourceName(queriesForDeviceProperty(DEVICE1, PROPERTY_PROMOTABLE),
                "queriesForSystemEntityPromotable");
    }

    static Stream<Object[]> queriesForSystemVariablePromotable() {
        return withSourceName(queriesForSystemVariable(VARIABLE_PROMOTABLE), "queriesForSystemVariablePromotable");
    }

    @ParameterizedTest(name = "{index} - from source: {1}")
    @MethodSource({ "queriesForSystemEntityPromotable", "queriesForSystemVariablePromotable" })
    public void shouldExtractWhenFieldTypesPromotable(Function<SparkSession, Dataset<Row>> query, String description) {
        Dataset<Row> ds = query.apply(sparkSession);
        assertEquals(100, ds.count());
    }

    static Stream<Function<SparkSession, Dataset<Row>>> queriesForSystemVariable3WithAlias() {
        String variableName = VARIABLE3;
        String alias = FIELD3_ALIAS;
        List<String> aliasedFields = ImmutableList.of(FIELD3_OLD, FIELD3_NEW);
        long variableId = findVariable(SYSTEM, variableName).getId();
        return Stream.of(
                session -> DataQuery.builder(session).byVariables().system(SYSTEM)
                        .startTime(timeWindow.getStartTime()).endTime(timeWindow.getEndTime())
                        .fieldAliases(alias, aliasedFields).variable(variableName)
                        .build(),
                session -> DataQuery.builder(session).variables().system(SYSTEM).nameEq(variableName)
                        .timeWindow(timeWindow).fieldAliases(alias, new LinkedHashSet<>(aliasedFields)).build(),
                session -> DataQuery.builder(session).variables().system(SYSTEM).idEq(variableId)
                        .timeWindow(timeWindow).fieldAliases(alias, new LinkedHashSet<>(aliasedFields)).build()
        );
    }

    @ParameterizedTest
    @MethodSource("queriesForSystemVariable3WithAlias")
    public void shouldExtractWithAliasesByVariables(Function<SparkSession, Dataset<Row>> query) {
        //given
        //  [nxcals_value, nxcals_entity_id, nxcals_timestamp, nxcals_variable_name]
        Set<String> expectedColumns = getDefaultFieldsForVariableQuery();
        expectedColumns.add(FIELD3_ALIAS);
        //when
        Dataset<Row> dataset = query.apply(sparkSession);
        //then
        assertEquals(RECORDS_NUMBER, dataset.count());

        checkColumns(expectedColumns, dataset);

        List<Object> retrieved = getVariableData(dataset, VARIABLE3);
        assertEquals(EXPECTED_SIMPLE_VALUES, retrieved);
    }

    static Stream<Function<SparkSession, Dataset<Row>>> queriesForSystemDevice2Property3WithAlias() {
        String device = DEVICE2;
        String property = PROPERTY3;
        Map<String, Object> deviceProperty = Map.of(DEVICE_KEY, device, PROPERTY_KEY, property);
        long entityId = findEntity(deviceProperty).getId();
        String alias = FIELD3_ALIAS;
        List<String> aliasedFields = ImmutableList.of(FIELD3_OLD, FIELD3_NEW);
        return Stream.of(
                session -> DataQuery.builder(session).byEntities().system(SYSTEM)
                        .startTime(timeWindow.getStartTime()).endTime(timeWindow.getEndTime())
                        .fieldAliases(alias, aliasedFields).entity().keyValue(DEVICE_KEY, device)
                        .keyValue(PROPERTY_KEY, property)
                        .build(),
                session -> DevicePropertyDataQuery.builder(session).system(SYSTEM)
                        .startTime(timeWindow.getStartTime()).endTime(timeWindow.getEndTime())
                        .fieldAliases(alias, aliasedFields).entity().device(device)
                        .property(property).build(),
                session -> DataQuery.builder(session).entities().system(SYSTEM).idEq(entityId)
                        .timeWindow(timeWindow).fieldAliases(alias, new LinkedHashSet<>(aliasedFields)).build(),
                session -> ParameterDataQuery.builder(session).system(SYSTEM).deviceEq(device).propertyEq(property)
                        .timeWindow(timeWindow).fieldAliases(alias, new LinkedHashSet<>(aliasedFields)).build()
        );
    }

    @ParameterizedTest
    @MethodSource("queriesForSystemDevice2Property3WithAlias")
    public void shouldExtractWithAliasesByEntities(Function<SparkSession, Dataset<Row>> query) {
        //given
        //  [__record_version__, field3_old, property, __record_timestamp__, field3_alias, device, class, nxcals_entity_id]
        Set<String> expectedColumns = getDefaultFieldsForCMWEntityQueryWith(FIELD3_ALIAS);

        //when
        Dataset<Row> dataset = query.apply(sparkSession);
        //then
        assertEquals(RECORDS_NUMBER, dataset.count());

        checkColumns(expectedColumns, dataset);

        List<Object> retrieved = getFieldData(dataset, DEVICE2, PROPERTY3, FIELD3_ALIAS);
        assertEquals(EXPECTED_SIMPLE_VALUES, retrieved);
    }

    static Stream<Function<SparkSession, Dataset<Row>>> queriesForSystemDevice2Property3WithAliases() {
        String device = DEVICE2;
        String property = PROPERTY3;
        Map<String, Object> deviceProperty = Map.of(DEVICE_KEY, device, PROPERTY_KEY, property);
        long entityId = findEntity(deviceProperty).getId();
        String alias = FIELD3_ALIAS;
        String alias2 = "first_field_alias";
        String alias3 = "second_field_alias";
        String alias2Field = FIELD_NON_EXISTING;
        String alias3Field = FIELD_NON_EXISTING + "2";
        List<String> aliasedFields = ImmutableList.of(FIELD3_OLD, FIELD3_NEW);
        return Stream.of(
                session -> DataQuery.builder(session).byEntities().system(SYSTEM)
                        .startTime(timeWindow.getStartTime()).endTime(timeWindow.getEndTime())
                        .fieldAliases(alias, aliasedFields).fieldAlias(alias2, alias2Field)
                        .fieldAlias(alias3, alias3Field).entity()
                        .keyValue(DEVICE_KEY, device).keyValue(PROPERTY_KEY, property)
                        .build(),
                session -> DevicePropertyDataQuery.builder(session).system(SYSTEM)
                        .startTime(timeWindow.getStartTime()).endTime(timeWindow.getEndTime())
                        .fieldAliases(alias, aliasedFields).fieldAlias(alias2, alias2Field)
                        .fieldAlias(alias3, alias3Field).entity().device(device)
                        .property(property).build(),
                session -> DataQuery.builder(session).entities().system(SYSTEM).keyValuesEq(deviceProperty)
                        .timeWindow(timeWindow).fieldAliases(alias, new LinkedHashSet<>(aliasedFields))
                        .fieldAliases(alias2, alias2Field).fieldAliases(alias3, alias3Field).build(),
                session -> DataQuery.builder(session).entities().system(SYSTEM).idEq(entityId)
                        .timeWindow(timeWindow).fieldAliases(alias, new LinkedHashSet<>(aliasedFields))
                        .fieldAliases(alias2, alias2Field).fieldAliases(alias3, alias3Field).build(),
                session -> ParameterDataQuery.builder(session).system(SYSTEM).deviceEq(device).propertyEq(property)
                        .timeWindow(timeWindow).fieldAliases(alias, new LinkedHashSet<>(aliasedFields))
                        .fieldAliases(alias2, alias2Field).fieldAliases(alias3, alias3Field).build()
        );
    }

    @ParameterizedTest
    @MethodSource("queriesForSystemDevice2Property3WithAliases")
    public void shouldAddColumnsWithAliasIfItDoesntExist(Function<SparkSession, Dataset<Row>> query) {
        //given
        //  [__record_version__, field3_old, property, __record_timestamp__, field3_alias, device, class, nxcals_entity_id]
        String firstAlias = "first_field_alias";
        String secondAlias = "second_field_alias";
        Set<String> expectedColumns = getDefaultFieldsForCMWEntityQueryWith(FIELD3_ALIAS, firstAlias, secondAlias);

        //when
        Dataset<Row> dataset = query.apply(sparkSession);
        //then
        assertEquals(RECORDS_NUMBER, dataset.count());

        checkColumns(expectedColumns, dataset);

        List<Object> retrieved = getFieldData(dataset, DEVICE2, PROPERTY3, FIELD3_ALIAS);
        assertEquals(EXPECTED_SIMPLE_VALUES, retrieved);
    }

    static Stream<Function<SparkSession, Dataset<Row>>> queriesForVariableOnMultipleSystems() {
        return queriesForSystemVariableIn(SYSTEM_2, VARIABLE_ON_MULTIPLE_SYSTEMS, timeWindow);
    }

    @ParameterizedTest
    @MethodSource("queriesForVariableOnMultipleSystems")
    public void shouldExtractDataForOneVariableWithMultiSystemConfiguration(
            Function<SparkSession, Dataset<Row>> query) {
        //given
        //when
        Dataset<Row> dataset = query.apply(sparkSession);
        //then
        assertEquals(RECORDS_NUMBER, dataset.count());

        checkColumnsInVariableQuery(dataset);

        Set<Object> retrievedValues = new HashSet<>(getVariableData(dataset, VARIABLE_ON_MULTIPLE_SYSTEMS));
        assertEquals(EXPECTED_MULTI_SYSTEM_VALUES, retrievedValues);
    }

    static Stream<Function<SparkSession, Dataset<Row>>> queriesForNotNullVariables() {
        return queriesForSystemAndTwoVariables(NOT_NULL_FIELD_ONE_VARIABLE, NOT_NULL_FIELD_TWO_VARIABLE);
    }

    @ParameterizedTest
    @MethodSource("queriesForNotNullVariables")
    public void shouldNotCreateNotNullVariableNames(Function<SparkSession, Dataset<Row>> query) {
        Dataset<Row> dataset = query.apply(sparkSession);
        dataset = dataset.where(dataset.col(NXC_EXTR_VARIABLE_NAME.getValue()).isNull());
        assertEquals(0, dataset.count());
    }

    static Stream<Function<SparkSession, Dataset<Row>>> queriesForVariableWithMultipleConfigHavingIllegalField() {
        return queriesForSystemVariable(VARIABLE_WITH_MULTIPLE_CONFIGS_HAVING_ILLEGAL_FIELD);
    }

    @ParameterizedTest
    @MethodSource("queriesForVariableWithMultipleConfigHavingIllegalField")
    public void shouldSupportIllegalCharactersInVariableField(Function<SparkSession, Dataset<Row>> query) {
        // @formatter:off
        Dataset<Row> dataset = query.apply(sparkSession);
        // @formatter:on

        assertEquals(50, dataset.count());  // how does it support? it just skips this field
    }

    static Stream<Function<SparkSession, Dataset<Row>>> queriesForDevice5Property5() {
        return queriesForDeviceProperty(DEVICE5, PROPERTY5);
    }

    @ParameterizedTest
    @MethodSource("queriesForDevice5Property5")
    void shouldGetDatasetForEntityWithFieldWithIllegalCharacters(Function<SparkSession, Dataset<Row>> query) {
        Set<String> expectedColumns = getDefaultFieldsForCMWEntityQueryWith(FIELD_WITH_ILLEGAL_CHARS);

        Dataset<Row> dataset = query.apply(sparkSession);
        checkColumns(expectedColumns, dataset);
    }

    static Stream<Function<SparkSession, Dataset<Row>>> queriesForMonitoringEntityBeforeData() {
        return queriesForSystemDevice1Property1In(TimeWindow.between(Instant.ofEpochMilli(1), Instant.ofEpochMilli(2)));
    }

    @ParameterizedTest
    @MethodSource("queriesForMonitoringEntityBeforeData")
    void shouldGetEmptyDataSetForEntityWithNonExistingHdfsResources(Function<SparkSession, Dataset<Row>> query) {
        Dataset<Row> dataset = query.apply(sparkSession);

        assertNotNull(dataset);
        assertTrue(dataset.isEmpty());

        Set<String> expected = getFieldsForEntityQueryIn(SYSTEM);
        Set<String> datasetColumns = new HashSet<>(asList(dataset.columns()));
        assertEquals(expected, datasetColumns);
    }

    static Stream<Function<SparkSession, Dataset<Row>>> queriesForMonitoringVariableBeforeData() {
        return queriesForSystemVariableIn(SYSTEM, VARIABLE1,
                TimeWindow.between(Instant.ofEpochMilli(1), Instant.ofEpochMilli(2)));
    }

    @ParameterizedTest
    @MethodSource("queriesForMonitoringVariableBeforeData")
    void shouldGetEmptyDataSetForVariableWithNonExistingHdfsResources(Function<SparkSession, Dataset<Row>> query) {
        Dataset<Row> dataset = query.apply(sparkSession);

        assertNotNull(dataset);
        assertTrue(dataset.isEmpty());

        checkColumnsInVariableQuery(dataset);
    }

    @Test
    void shouldDoPivot() {
        // given
        Set<String> variableNames = Set.of(VARIABLE1, VARIABLE2, VARIABLE_NULLABLE, VARIABLE_PROMOTABLE);

        // when
        Dataset<Row> dataset1 = DataQuery.getAsPivot(sparkSession, timeWindow, SYSTEM, variableNames);
        Set<String> columns = Sets.newHashSet(dataset1.columns());
        assertEquals(Sets.union(Set.of(NXC_EXTR_TIMESTAMP.getValue()), variableNames), columns);
        assertEquals(RECORDS_NUMBER, dataset1.count());
    }

    @Test
    void shouldDoPivotByVariableNamePattern() {
        // given
        Set<String> variableNames = Set.of(VARIABLE1, VARIABLE2, VARIABLE_NULLABLE, VARIABLE_PROMOTABLE);

        // when
        Dataset<Row> dataset = DataQuery.getAsPivot(sparkSession, timeWindow, SYSTEM, emptyList(), variableNames);

        Set<String> columns = Sets.newHashSet(dataset.columns());
        assertEquals(Sets.union(Set.of(NXC_EXTR_TIMESTAMP.getValue()), variableNames), columns);
        assertEquals(RECORDS_NUMBER, dataset.count());
    }
}
