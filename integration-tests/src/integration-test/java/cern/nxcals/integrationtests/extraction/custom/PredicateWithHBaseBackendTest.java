package cern.nxcals.integrationtests.extraction.custom;

import cern.cmw.datax.ImmutableData;
import cern.nxcals.api.config.SparkContext;
import cern.nxcals.api.custom.service.ExtractionService;
import cern.nxcals.api.custom.service.Services;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.common.utils.Lazy;
import cern.nxcals.integrationtests.utils.CMWEntityDataGenerator;
import cern.nxcals.integrationtests.utils.HBaseDataInitializer;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.DependsOn;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;
import java.util.List;

import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.RECORD_TIMESTAMP_KEY;
import static cern.nxcals.integrationtests.utils.ServicesHelper.VARIABLE_SERVICE;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = SparkContext.class)
@SpringBootApplication
@Slf4j
@DependsOn("kerberos")
public class PredicateWithHBaseBackendTest implements PredicateAbstractTest {
    //    static {
    //        String username = System.getProperty("user.name");
    //        System.setProperty("kerberos.principal", username);
    //        System.setProperty("kerberos.keytab", "/opt/<user>/.keytab".replace("<user>", username));
    //        System.setProperty("service.url",
    //                "https://nxcals-<user>-1.cern.ch:19093".replace("<user>", username)); //Do not use localhost here.
    //        System.setProperty("kafka.producer.bootstrap.servers",
    //                "https://nxcals-<user>-3.cern.ch:9092,https://nxcals-<user>-4.cern.ch:9092,https://nxcals-<user>-5.cern.ch:9092".replace(
    //                        "<user>", username));
    //    }

    private static final String FIELD_NAME = "FIELD";
    private static final String SELECTOR_FIELD_NAME = "selector";
    private static final List<String> selectorValues = List.of("CPS", "SPS");
    private static final CMWEntityDataGenerator generator = new CMWEntityDataGenerator(
            datapointNumber -> ImmutableData.builder()
                    .add(FIELD_NAME, datapointNumber)
                    .add(SELECTOR_FIELD_NAME, selectorValues.get(datapointNumber % selectorValues.size()))
                    .build()
    );
    private static final HBaseDataInitializer dataInitializer = new HBaseDataInitializer(generator);

    @Autowired
    private SparkSession sparkSession;
    private final Lazy<ExtractionService> extractionServiceLazy = new Lazy<>(
            () -> Services.newInstance(sparkSession).extractionService());

    @Override
    public Entity getEntity() {
        return generator.getCreatedEntity();
    }

    @Override
    public Instant getStartTime() {
        return generator.getDataProduceStart();
    }

    @Override
    public Instant getEndTime() {
        return generator.getDataProduceEnd();
    }

    @Override
    public String getFieldName() {
        return FIELD_NAME;
    }

    @Override
    public String getAdditionalFieldNameForPredicate() {
        return SELECTOR_FIELD_NAME;
    }

    @Override
    public SparkSession getSparkSession() {
        return sparkSession;
    }

    @Override
    public ExtractionService getExtractionService() {
        return extractionServiceLazy.get();
    }

    @Override
    public VariableService getVariableService() {
        return VARIABLE_SERVICE;
    }

    @Override
    public String getTimeFieldName() {
        return RECORD_TIMESTAMP_KEY;
    }

    @BeforeAll
    static void beforeAll() {
        dataInitializer.setUpData();
    }

    @BeforeEach
    void beforeEach() throws InterruptedException {
        dataInitializer.waitForData(sparkSession);
    }
}
