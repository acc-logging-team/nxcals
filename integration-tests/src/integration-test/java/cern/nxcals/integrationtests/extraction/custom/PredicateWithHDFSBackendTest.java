package cern.nxcals.integrationtests.extraction.custom;

import cern.nxcals.api.config.SparkContext;
import cern.nxcals.api.custom.service.ExtractionService;
import cern.nxcals.api.custom.service.Services;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.extraction.metadata.EntityService;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.common.utils.Lazy;
import com.google.common.collect.ImmutableMap;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.DependsOn;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Map;

import static cern.nxcals.api.custom.domain.CmwSystemConstants.DEVICE_KEY_NAME;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.SYSTEM_2;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.findEntityOrThrow;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = SparkContext.class)
@SpringBootApplication
@Slf4j
@DependsOn("kerberos")
public class PredicateWithHDFSBackendTest implements PredicateAbstractTest {

    //    static {
    //        String username = System.getProperty("user.name");
    //        System.setProperty("kerberos.principal", username);
    //        System.setProperty("kerberos.keytab", "/opt/<user>/.keytab".replace("<user>", username));
    //        System.setProperty("service.url",
    //                "https://nxcals-<user>1.cern.ch:19093".replace("<user>", username)); //Do not use localhost here.
    //        System.setProperty("kafka.producer.bootstrap.servers",
    //                "https://nxcals-<user>3.cern.ch:9092,https://nxcals-<user>4.cern.ch:9092,https://nxcals-<user>5.cern.ch:9092".replace(
    //                        "<user>", username));
    //    }

    @Autowired
    @Getter
    protected SparkSession sparkSession;

    private final Lazy<ExtractionService> extractionServiceLazy = new Lazy<>(
            () -> Services.newInstance(sparkSession).extractionService());

    protected static VariableService VARIABLE_SERVICE = ServiceClientFactory.createVariableService();
    protected static EntityService ENTITY_SERVICE = ServiceClientFactory.createEntityService();
    protected static SystemSpecService SYSTEM_SERVICE = ServiceClientFactory.createSystemSpecService();
    private static final String MONITORING_SYSTEM = SYSTEM_2;

    private static final SystemSpec MONITORING_SYSTEM_SPEC = SYSTEM_SERVICE.findByName(MONITORING_SYSTEM)
            .orElseThrow(() -> new IllegalArgumentException("No such system"));

    private static final String MONITORING_DEVICE_1 = "NXCALS_MONITORING_DEV1";
    private static final Map<String, Object> MONITORING_DEVICE_KEY1 = ImmutableMap
            .of(DEVICE_KEY_NAME, MONITORING_DEVICE_1);
    private static final String MONITORING_ENTITY_INT_FIELD = "intField1";
    private static final String MONITORING_ENTITY_STRING_FIELD = "stringField1";
    private static final Instant queryStart = TimeUtils.getInstantFromString("2022-04-02 00:00:00.000");
    private static final Instant queryEnd = queryStart.plus(1, ChronoUnit.HOURS);

    @Override
    public ExtractionService getExtractionService() {
        return extractionServiceLazy.get();
    }

    @Override
    public VariableService getVariableService() {
        return VARIABLE_SERVICE;
    }

    @Override
    public Entity getEntity() {
        return findEntityOrThrow(MONITORING_DEVICE_KEY1, ENTITY_SERVICE, MONITORING_SYSTEM_SPEC);
    }

    @Override
    public Instant getStartTime() {
        return queryStart;
    }

    @Override
    public Instant getEndTime() {
        return queryEnd;
    }

    @Override
    public String getFieldName() {
        return MONITORING_ENTITY_INT_FIELD;
    }

    @Override
    public String getAdditionalFieldNameForPredicate() {
        return MONITORING_ENTITY_STRING_FIELD;
    }

    @Override
    public String getTimeFieldName() {
        return "timestamp";
    }
}
