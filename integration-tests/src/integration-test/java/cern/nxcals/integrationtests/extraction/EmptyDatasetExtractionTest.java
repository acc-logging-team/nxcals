package cern.nxcals.integrationtests.extraction;

import cern.nxcals.api.config.SparkContext;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableDeclaredType;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.extraction.metadata.InternalServiceClientFactory;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.integrationtests.testutils.ExtractionTestHelper;
import cern.nxcals.internal.extraction.metadata.InternalEntityService;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.DependsOn;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static cern.nxcals.common.SystemFields.NXC_EXTR_ENTITY_ID;
import static cern.nxcals.common.SystemFields.NXC_EXTR_TIMESTAMP;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VALUE;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VARIABLE_NAME;
import static cern.nxcals.common.SystemFields.NXC_KAFKA_KEY_TIMESTAMP;
import static cern.nxcals.integrationtests.service.AbstractTest.getRandomName;
import static cern.nxcals.integrationtests.service.AbstractTest.toKeyValues;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.SYSTEM_2;
import static cern.nxcals.integrationtests.utils.ServicesHelper.SYSTEM_SERVICE;
import static cern.nxcals.integrationtests.utils.ServicesHelper.VARIABLE_SERVICE;
import static cern.nxcals.integrationtests.utils.ServicesHelper.registerSimpleVariable;
import static cern.nxcals.integrationtests.utils.ServicesHelper.registerVariableToEntity;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = SparkContext.class)
@SpringBootApplication
@Slf4j
@DependsOn("kerberos")
class EmptyDatasetExtractionTest {

    static {
        System.setProperty("kafka.producer.linger.ms", "0");
        //        Please leave this one just in case you want to run those test from IDE for debugging
        //        Don't forget to check if hadoop-dev jar is included in build.gradle!!!
        //        String username = System.getProperty("user.name");
        //        System.setProperty("kerberos.principal", username);
        //        System.setProperty("kerberos.keytab", "/opt/<user>/.keytab".replace("<user>", username));
        //        System.setProperty("service.url",
        //                "https://nxcals-<user>-1.cern.ch:19093".replace("<user>", username)); //Do not use localhost here.
        //        System.setProperty("kafka.producer.bootstrap.servers",
        //                "https://nxcals-<user>-3.cern.ch:9092"
        //                        .replace("<user>", username));
    }

    @Autowired
    private SparkSession sparkSession;

    protected static final InternalEntityService internalEntityService = InternalServiceClientFactory
            .createEntityService();

    private static final String MONITORING_SYSTEM = SYSTEM_2;
    private static final String NAME_PREFIX = "EmptyDatasetTests-";
    private static final String DEVICE_KEY = "device";
    private static final String PARTITION_KEY = "specification";

    static final String VALID_SCHEMA = "{\"type\":\"record\",\"name\":\"data0\",\"namespace\":\"cern.nxcals\",\"fields\":[{\"name\": \"__sys_nxcals_system_id__\", \"type\": \"long\"},{\"name\": \"__sys_nxcals_entity_id__\", \"type\": \"long\"},{\"name\": \"__sys_nxcals_partition_id__\", \"type\": \"long\"},{\"name\": \"__sys_nxcals_schema_id__\", \"type\": \"long\"},{\"name\": \"__sys_nxcals_timestamp__\", \"type\": \"long\"},{\"name\":\"no_data_field\",\"type\":[\"long\",\"null\"]}]}";

    private static final Map<String, Object> NO_DATA_PARTITION_KEYS = ImmutableMap.of(PARTITION_KEY,
            "no_data_device_specification");
    private static final String NO_DATA_FIELD = "no_data_field";

    private static final Instant HDFS_QUERY_START = TimeUtils.getInstantFromString("2010-04-02 00:00:00.001");

    private static final Entity ENTITY_WITHOUT_HISTORY = createMonitoringEntityWithoutHistory();

    static Entity createMonitoringEntityWithoutHistory() {
        SystemSpec systemData = SYSTEM_SERVICE.findByName(MONITORING_SYSTEM)
                .orElseThrow(() -> new IllegalArgumentException("No such system"));

        Map<String, Object> entityKeyValues = ImmutableMap.of(DEVICE_KEY, getRandomName(NAME_PREFIX));

        return internalEntityService.createEntity(systemData.getId(), entityKeyValues,
                NO_DATA_PARTITION_KEYS);
    }

    static Entity createMonitoringEntityWithHistory(Instant historySince) {
        SystemSpec systemData = SYSTEM_SERVICE.findByName(MONITORING_SYSTEM)
                .orElseThrow(() -> new IllegalArgumentException("No such system"));

        Map<String, Object> entityKeyValues = ImmutableMap.of(DEVICE_KEY, getRandomName(NAME_PREFIX));

        return internalEntityService.findOrCreateEntityFor(
                systemData.getId(),
                toKeyValues(entityKeyValues),
                toKeyValues(NO_DATA_PARTITION_KEYS),
                VALID_SCHEMA,
                TimeUtils.getNanosFromInstant(historySince));
    }

    static class HdfsAndHbaseTests {
        Object[][] hbaseAndHdfsTimestamps() {
            return new Object[][] {
                    new Object[] { HDFS_QUERY_START, HDFS_QUERY_START.plusSeconds(10), "hdfs" },
                    new Object[] { Instant.now().minusSeconds(10), Instant.now(), "hbase" }
            };
        }
    }

    @Nested
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class EntityTests extends HdfsAndHbaseTests {
        @MethodSource("hbaseAndHdfsTimestamps")
        @ParameterizedTest(name = "{index} should return correct empty set if entity doesn't have schema, when extracting from {2}")
        void shouldGetDefaultSetOfColumnsForEntityWithAbsentSchema(Instant queryStart, Instant queryEnd,
                String description) {
            checkEntityDatasetWithoutSchema(ENTITY_WITHOUT_HISTORY, TimeWindow.between(queryStart, queryEnd));
        }

        private void checkEntityDatasetWithoutSchema(Entity entity, TimeWindow timeWindow) {
            Dataset<Row> result = DataQuery.builder(sparkSession).entities()
                    .system(ENTITY_WITHOUT_HISTORY.getSystemSpec().getName())
                    .idEq(entity.getId())
                    .timeWindow(timeWindow)
                    .build();

            Set<String> expectedResult = Sets.newHashSet(
                    NXC_EXTR_ENTITY_ID.getValue(),
                    NXC_KAFKA_KEY_TIMESTAMP.getValue(),
                    DEVICE_KEY,
                    PARTITION_KEY);
            Set<String> actualResult = Sets.newHashSet(result.columns());
            assertEquals(expectedResult, actualResult);
            assertEquals(0, result.count());
        }

        @MethodSource("hbaseAndHdfsTimestamps")
        @ParameterizedTest(name = "{index} should return correct empty set if entity doesn't have valid schema, when extracting from {2}")
        void shouldGetDefaultSetOfColumnsForEntityWithoutValidSchema(Instant queryStart, Instant queryEnd,
                String description) {
            Entity entity = createMonitoringEntityWithHistory(queryEnd.plusSeconds(1));

            checkEntityDatasetWithoutSchema(entity, TimeWindow.between(queryStart, queryEnd));
        }

        @MethodSource("hbaseAndHdfsTimestamps")
        @ParameterizedTest(name = "{index} should return correct empty set if entity has valid schema, when extracting from {2}")
        void shouldReturnCorrectSetOfColumnsForEntityWithValidSchema(Instant queryStart, Instant queryEnd,
                String description) {
            Entity entity = createMonitoringEntityWithHistory(queryEnd.minusSeconds(1));

            Dataset<Row> result = DataQuery.builder(sparkSession).entities()
                    .system(entity.getSystemSpec().getName())
                    .idEq(entity.getId())
                    .timeWindow(queryStart, queryEnd)
                    .build();

            Set<String> expectedFields = Sets.newHashSet(
                    NXC_EXTR_ENTITY_ID.getValue(),
                    NXC_KAFKA_KEY_TIMESTAMP.getValue(),
                    PARTITION_KEY,
                    DEVICE_KEY,
                    NO_DATA_FIELD
            );

            ExtractionTestHelper.checkColumns(expectedFields, result);

            assertEquals(0, result.count());

            StructField fieldType = result.schema().apply(NO_DATA_FIELD);
            assertEquals(DataTypes.LongType, fieldType.dataType());
        }

    }

    @Nested
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class VariableToFieldTests extends HdfsAndHbaseTests {
        @MethodSource("hbaseAndHdfsTimestamps")
        @ParameterizedTest(name = "{index} should return correct empty set if variable points to entity without schema, when extracting from {2}")
        void shouldGetDefaultSetOfColumnsForVariablePointingToEntityWithAbsentSchema(Instant queryStart,
                Instant queryEnd,
                String description) {
            Variable variable = registerSimpleVariable(getRandomName(NAME_PREFIX),
                    ENTITY_WITHOUT_HISTORY,
                    "field1", VariableDeclaredType.NUMERIC);

            checkDefaultVariableDataset(variable, TimeWindow.between(queryStart, queryEnd));
        }

        private void checkDefaultVariableDataset(Variable variable, TimeWindow timeWindow) {
            Dataset<Row> result = DataQuery.builder(sparkSession).variables()
                    .system(variable.getSystemSpec().getName())
                    .idEq(variable.getId())
                    .timeWindow(timeWindow)
                    .build();

            ExtractionTestHelper.checkColumnsInVariableQuery(result);
            assertEquals(0, result.count());

            StructField valueField = result.schema().apply(NXC_EXTR_VALUE.getValue());
            assertEquals(DataTypes.StringType, valueField.dataType()); // we don't use declared type
        }

        @MethodSource("hbaseAndHdfsTimestamps")
        @ParameterizedTest(name = "{index} should return correct empty set if variable points to entity without valid schema, when extracting from {2}")
        void shouldGetDefaultSetOfColumnsForVariablePointingToEntityWithoutValidSchema(Instant queryStart,
                Instant queryEnd,
                String description) {
            Entity entity = createMonitoringEntityWithHistory(queryEnd.plusSeconds(1));

            Variable variable = registerSimpleVariable(getRandomName(NAME_PREFIX), entity,
                    NO_DATA_FIELD, VariableDeclaredType.NUMERIC);

            checkDefaultVariableDataset(variable, TimeWindow.between(queryStart, queryEnd));

        }

        @MethodSource("hbaseAndHdfsTimestamps")
        @ParameterizedTest(name = "{index} should return correct empty set if variable doesn't have config, when extracting from {2}")
        void shouldGetDefaultSetOfColumnsForVariableWithoutConfig(Instant queryStart,
                Instant queryEnd,
                String description) {
            Variable variable = VARIABLE_SERVICE.create(
                    Variable.builder()
                            .variableName(getRandomName(NAME_PREFIX))
                            .systemSpec(SYSTEM_SERVICE.findByName(MONITORING_SYSTEM).get())
                            .build()
            );

            checkDefaultVariableDataset(variable, TimeWindow.between(queryStart, queryEnd));
        }

        @MethodSource("hbaseAndHdfsTimestamps")
        @ParameterizedTest(name = "{index} should return correct empty set if variable points to entity with valid schema, but without data, when extracting from {2}")
        void shouldGetDefaultSetOfColumnsForVariablePointingToEntityWithValidSchema(Instant queryStart,
                Instant queryEnd,
                String description) {
            Entity entity = createMonitoringEntityWithHistory(queryStart);

            Variable variable = registerSimpleVariable(getRandomName(NAME_PREFIX), entity,
                    NO_DATA_FIELD, VariableDeclaredType.NUMERIC);

            Dataset<Row> result = DataQuery.builder(sparkSession).variables()
                    .system(variable.getSystemSpec().getName())
                    .idEq(variable.getId())
                    .timeWindow(queryStart, queryEnd)
                    .build();

            ExtractionTestHelper.checkColumnsInVariableQuery(result);
            assertEquals(0, result.count());

            StructField valueField = result.schema().apply(NXC_EXTR_VALUE.getValue());
            assertEquals(DataTypes.LongType, valueField.dataType()); // we don't use declared type
        }

        @MethodSource("hbaseAndHdfsTimestamps")
        @ParameterizedTest(name = "{index} should return correct empty set if extraction two variables - one pointing to entity with valid schema (no data), second pointing to entity without valid schema, when extracting from {2}")
        void shouldGetDefaultSetOfColumnsForVariablePointingToEntityWithValidSchemaWhenSecondPointsToEntityWithoutSchema(
                Instant queryStart, Instant queryEnd,
                String description) {
            Entity entityWithSchema = createMonitoringEntityWithHistory(queryStart);

            Variable variableWithSchema = registerSimpleVariable(getRandomName(NAME_PREFIX),
                    entityWithSchema, NO_DATA_FIELD, VariableDeclaredType.NUMERIC);

            Variable variableWithoutSchema = registerSimpleVariable(getRandomName(NAME_PREFIX),
                    ENTITY_WITHOUT_HISTORY, NO_DATA_FIELD, VariableDeclaredType.NUMERIC);

            Dataset<Row> result = DataQuery.builder(sparkSession).variables()
                    .system(variableWithSchema.getSystemSpec().getName())
                    .idIn(variableWithSchema.getId(), variableWithoutSchema.getId())
                    .timeWindow(queryStart, queryEnd)
                    .build();

            ExtractionTestHelper.checkColumnsInVariableQuery(result);
            assertEquals(0, result.count());

            StructField valueField = result.schema().apply(NXC_EXTR_VALUE.getValue());
            assertEquals(DataTypes.LongType, valueField.dataType()); // we don't use declared type
        }
    }

    @Nested
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class VariableToWholeEntityTest extends HdfsAndHbaseTests {
        @MethodSource("hbaseAndHdfsTimestamps")
        @ParameterizedTest(name = "{index} should return correct empty set if variable points to entity with valid schema, but without data, when extracting from {2}")
        void shouldGetDefaultSetOfColumnsForVariablePointingToWholeEntityWithValidSchema(
                Instant queryStart, Instant queryEnd,
                String description) {
            Entity entityWithSchema = createMonitoringEntityWithHistory(queryStart);

            Variable variableWithSchema = registerVariableToEntity(getRandomName(NAME_PREFIX), entityWithSchema);

            Dataset<Row> result = DataQuery.builder(sparkSession).variables()
                    .system(variableWithSchema.getSystemSpec().getName())
                    .idEq(variableWithSchema.getId())
                    .timeWindow(queryStart, queryEnd)
                    .build();

            Set<String> expectedFields = Sets.newHashSet(
                    NXC_EXTR_ENTITY_ID.getValue(),
                    NXC_EXTR_TIMESTAMP.getValue(),
                    NXC_EXTR_VARIABLE_NAME.getValue(),
                    NO_DATA_FIELD
            );
            ExtractionTestHelper.checkColumns(expectedFields, result);
            assertEquals(0, result.count());

            StructField valueField = result.schema().apply(NO_DATA_FIELD);
            assertEquals(DataTypes.LongType, valueField.dataType()); // we don't use declared type
        }

        @MethodSource("hbaseAndHdfsTimestamps")
        @ParameterizedTest(name = "{index} should return correct empty set if variable points to entity without valid schema, when extracting from {2}")
        void shouldGetDefaultSetOfColumnsForVariablePointingToWholeEntityWithoutValidSchema(
                Instant queryStart, Instant queryEnd,
                String description) {
            Entity entityWithSchema = createMonitoringEntityWithHistory(queryEnd.plusSeconds(1));

            Variable variableWithoutSchema = registerVariableToEntity(getRandomName(NAME_PREFIX), entityWithSchema);

            checkDefaultVariableDataset(variableWithoutSchema, TimeWindow.between(queryStart, queryEnd));
        }

        private void checkDefaultVariableDataset(Variable variable, TimeWindow timeWindow) {
            Dataset<Row> result = DataQuery.builder(sparkSession).variables()
                    .system(variable.getSystemSpec().getName())
                    .idEq(variable.getId())
                    .timeWindow(timeWindow)
                    .build();

            Set<String> expectedFields = Sets.newHashSet(
                    NXC_EXTR_ENTITY_ID.getValue(),
                    NXC_EXTR_TIMESTAMP.getValue(),
                    NXC_EXTR_VARIABLE_NAME.getValue(),
                    NXC_EXTR_VALUE.getValue() // is it correct?
            );
            ExtractionTestHelper.checkColumns(expectedFields, result);
            assertEquals(0, result.count());
        }

        @MethodSource("hbaseAndHdfsTimestamps")
        @ParameterizedTest(name = "{index} should return correct empty set if variable points to entity without any schema, when extracting from {2}")
        void shouldGetDefaultSetOfColumnsForVariablePointingToWholeEntityWithoutHistory(
                Instant queryStart, Instant queryEnd,
                String description) {
            Variable variableWithoutSchema = registerVariableToEntity(getRandomName(NAME_PREFIX),
                    ENTITY_WITHOUT_HISTORY);

            checkDefaultVariableDataset(variableWithoutSchema, TimeWindow.between(queryStart, queryEnd));
        }
    }

    @AfterAll
    public static void cleanUp() {
        Set<Variable> variables = VARIABLE_SERVICE.findAll(Variables.suchThat().variableName().like(NAME_PREFIX + "%"));
        VARIABLE_SERVICE.deleteAll(variables.stream().map(Variable::getId).collect(Collectors.toSet()));
    }
}
