package cern.nxcals.integrationtests.extraction;

import cern.cmw.datax.DataBuilder;
import cern.cmw.datax.ImmutableData;
import cern.nxcals.api.config.SparkContext;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.extraction.metadata.EntityService;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.ingestion.Publisher;
import cern.nxcals.api.ingestion.PublisherFactory;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.DependsOn;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Stream;

import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.getCount;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.getDefaultFieldsForCMWEntityQueryWith;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.getDefaultFieldsForVariableQuery;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.getFieldData;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.getVariableData;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.registerVariableToOneFieldWithOneConfig;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = SparkContext.class)
@TestPropertySource(locations = "classpath:/application.yml")
@SpringBootApplication
@Slf4j
@DependsOn("kerberos")
public class PromotionTest {
    static {
        //        Please leave this one just in case you want to run those test from IDE for debugging
        //        Don't forget to check if hadoop-dev jar is included in build.gradle!!!
        //
        //        System.setProperty("service.url", "https://nxcals-<user>1.cern.ch:19093".replace("<user>","achelaru")); //Do not use localhost here.
        //        System.setProperty("kafka.producer.bootstrap.servers", "https://nxcals-<user>3.cern.ch:9092,https://nxcals-<user>4.cern.ch:9092,https://nxcals-<user>5.cern.ch:9092".replace("<user>", "achelaru"));
        //        System.setProperty("kerberos.principal", "achelaru");
        //        System.setProperty("kerberos.keytab", "/opt/<user>/.keytab".replace("<user>", "achelaru"));
    }

    private static final Instant FROM = Instant.now();
    private static final String RANDOM_DEVICE = UUID.randomUUID().toString();
    private static final String DEVICE = "test_device_promotion_" + RANDOM_DEVICE;
    private static final String PROPERTY_PREFIX = "test_property_";
    private static final String FIELD_PREFIX = "field_";
    private static final String SYSTEM = "TEST-CMW";
    private static final String CLASS = "test_class";
    private static final String DEVICE_KEY = "device";
    private static final String PROPERTY_KEY = "property";
    private static final String CLASS_KEY = "class";
    private static final String RECORD_VERSION_KEY = "__record_version__";
    private static final String RECORD_TIMESTAMP_KEY = "__record_timestamp__";
    private static final int RECORDS_NUMBER = 10;
    private static final Instant END_TIME = FROM.plus(RECORDS_NUMBER + 1, ChronoUnit.MILLIS);

    private static final VariableService variableService = ServiceClientFactory.createVariableService();
    private static final EntityService entityService = ServiceClientFactory.createEntityService();
    private static final SystemSpecService systemService = ServiceClientFactory.createSystemSpecService();

    private static final SystemSpec SYSTEM_SPEC = systemService.findByName(SYSTEM)
            .orElseThrow(() -> new IllegalArgumentException("No such system"));

    private static final Map<String, Class<?>> MAP_TYPE_TO_ARRAY = ImmutableMap.of("java.lang.Integer", int[].class,
            "java.lang.Long", long[].class, "java.lang.Float", float[].class, "java.lang.Double", double[].class);

    private static Set<PromotionTestSample<?, ?, ?>> testSamples;

    @Autowired
    private SparkSession sparkSession;
    private static volatile boolean dataVisible = false;
    private static final Map<Class<?>, Function<String, ?>> typesWithConverters;

    static {
        typesWithConverters = new HashMap<>();
        typesWithConverters.put(Integer.class, Integer::parseInt);
        typesWithConverters.put(Long.class, Long::parseLong);
        typesWithConverters.put(Float.class, Float::parseFloat);
        typesWithConverters.put(Double.class, Double::parseDouble);
    }

    //    RECORD, ENUM, ARRAY, MAP, UNION, FIXED, STRING, BYTES,
    //    INT, LONG, FLOAT, DOUBLE, BOOLEAN, NULL;

    private static Set<PromotionTestSample<?, ?, ?>> getTestSamples() {
        return Sets.newHashSet(
                getTestSample(1, Integer.class, Integer.class, Integer::valueOf, Integer::valueOf, Integer.class),
                getTestSample(2, Integer.class, Long.class, Integer::valueOf, Long::valueOf, Long.class),
                getTestSample(3, Integer.class, Float.class, Integer::valueOf, i -> i + 0.5f, Float.class),
                getTestSample(4, Integer.class, Double.class, Integer::valueOf, i -> i + 0.5d, Double.class),

                getTestSample(5, Long.class, Integer.class, Long::valueOf, Integer::valueOf, Long.class),
                getTestSample(6, Long.class, Long.class, Long::valueOf, Long::valueOf, Long.class),
                getTestSample(7, Long.class, Float.class, Long::valueOf, i -> i + 0.5f, Float.class),
                getTestSample(8, Long.class, Double.class, Long::valueOf, i -> i + 0.5d, Double.class),

                getTestSample(9, Float.class, Integer.class, i -> i + 0.5f, Integer::valueOf, Float.class),
                getTestSample(10, Float.class, Long.class, i -> i + 0.5f, Long::valueOf, Float.class),
                getTestSample(11, Float.class, Float.class, i -> i + 0.5f, i -> i + 0.5f, Float.class),
                getTestSample(12, Float.class, Double.class, i -> i + 0.5f, i -> i + 0.5d, Double.class),

                getTestSample(13, Double.class, Integer.class, i -> i + 0.5d, Integer::valueOf, Double.class),
                getTestSample(14, Double.class, Long.class, i -> i + 0.5d, Long::valueOf, Double.class),
                getTestSample(15, Double.class, Float.class, i -> i + 0.5d, i -> i + 0.5f, Double.class),
                getTestSample(16, Double.class, Double.class, i -> i + 0.5d, i -> i + 0.5d, Double.class),

                getTestSample(17, int[].class, int[].class, i -> new int[] { i }, i -> new int[] { i }, int[].class),
                getTestSample(18, int[].class, long[].class, i -> new int[] { i }, i -> new long[] { Long.valueOf(i) },
                        long[].class),
                getTestSample(19, int[].class, float[].class, i -> new int[] { i }, i -> new float[] { i + 0.5f },
                        float[].class),
                getTestSample(20, int[].class, double[].class, i -> new int[] { i }, i -> new double[] { i + 0.5d },
                        double[].class),

                getTestSample(21, long[].class, int[].class, i -> new long[] { Long.valueOf(i) }, i -> new int[] { i },
                        long[].class),
                getTestSample(22, long[].class, long[].class, i -> new long[] { Long.valueOf(i) },
                        i -> new long[] { Long.valueOf(i) }, long[].class),
                getTestSample(23, long[].class, float[].class, i -> new long[] { Long.valueOf(i) },
                        i -> new float[] { i + 0.5f }, float[].class),
                getTestSample(24, long[].class, double[].class, i -> new long[] { Long.valueOf(i) },
                        i -> new double[] { i + 0.5d }, double[].class),

                getTestSample(25, float[].class, int[].class, i -> new float[] { i + 0.5f }, i -> new int[] { i },
                        float[].class),
                getTestSample(26, float[].class, long[].class, i -> new float[] { i + 0.5f },
                        i -> new long[] { Long.valueOf(i) }, float[].class),
                getTestSample(27, float[].class, float[].class, i -> new float[] { i + 0.5f },
                        i -> new float[] { i + 0.5f }, float[].class),
                getTestSample(28, float[].class, double[].class, i -> new float[] { i + 0.5f },
                        i -> new double[] { i + 0.5d }, double[].class),

                getTestSample(29, double[].class, int[].class, i -> new double[] { i + 0.5d }, i -> new int[] { i },
                        double[].class),
                getTestSample(30, double[].class, long[].class, i -> new double[] { i + 0.5d },
                        i -> new long[] { Long.valueOf(i) }, double[].class),
                getTestSample(31, double[].class, float[].class, i -> new double[] { i + 0.5d },
                        i -> new float[] { i + 0.5f }, double[].class),
                getTestSample(32, double[].class, double[].class, i -> new double[] { i + 0.5d },
                        i -> new double[] { i + 0.5d }, double[].class)
        );
    }

    @BeforeAll
    public static void setUpData() throws InterruptedException {
        testSamples = getTestSamples();

        Publisher<ImmutableData> publisher = PublisherFactory.newInstance()
                .createPublisher(SYSTEM, Function.identity());
        log.info("Sending data to NXCALS");
        for (int i = 1; i <= RECORDS_NUMBER; ++i) {
            try {
                long timestamp = FROM.plus(i, ChronoUnit.MILLIS).toEpochMilli() * 1000_000;
                for (PromotionTestSample<?, ?, ?> testSample : testSamples) {
                    publisher.publish(createDataWithChangedFieldType(i, timestamp, testSample));
                }
            } catch (Exception e) {
                log.error("Error sending values to NXCALS", e);
                fail("Error sending values to NXCALS");
            }
        }

        for (PromotionTestSample testSample : testSamples) {
            registerVariableForTestSample(testSample);
        }
    }

    @BeforeEach
    public void waitForData() throws InterruptedException {
        if (!dataVisible) {
            //Have to wait 30 seconds in order for the data to be processed and visible in NXCALS extraction
            log.info("Waiting max 2 minutes for the data to be visible in NXCALS...");
            waitForData(TimeUnit.MINUTES.toMillis(2));
            log.info("Ok, data should be processed now, proceeding with tests");
            dataVisible = true;
        } else {
            log.info("Data already checked");
        }
    }

    private void waitForData(long maxTime) throws InterruptedException {
        long start = System.currentTimeMillis();

        while (true) {
            if (System.currentTimeMillis() - start > maxTime) {
                fail("Data was not processed in the required maxTime=" + maxTime + " ms");
            }
            if (isDataPresent()) {
                return;
            }
            log.info("Still no data visible, waiting more...");
            TimeUnit.SECONDS.sleep(1);
        }
    }

    private boolean isDataPresent() {
        for (PromotionTestSample testSample : testSamples) {
            if (getCount(testSample.getDevice(), testSample.getProperty(), sparkSession, FROM) != RECORDS_NUMBER) {
                return false;
            }
        }
        return true;
    }

    public static void registerVariableForTestSample(PromotionTestSample<?, ?, ?> testSample) {
        registerVariableToOneFieldWithOneConfig(testSample.getEntityKeyValues(), testSample.getField(),
                testSample.getVariable(), entityService, variableService, SYSTEM_SPEC);
    }

    private static <O, N, E> ImmutableData createDataWithChangedFieldType(int i, long timestamp,
            PromotionTestSample<O, N, E> testSample) {
        DataBuilder builder = ImmutableData.builder();
        builder.add(DEVICE_KEY, testSample.getDevice());
        builder.add(PROPERTY_KEY, testSample.getProperty());
        builder.add(CLASS_KEY, CLASS);
        builder.add(RECORD_VERSION_KEY, 0L);
        builder.add(RECORD_TIMESTAMP_KEY, timestamp);
        if (i <= 5) {
            builder.add(testSample.getField(), testSample.getOldGenerator().apply(i));
        } else {
            builder.add(testSample.getField(), testSample.getNewGenerator().apply(i));
        }
        return builder.build();
    }

    private static <O, N, E> PromotionTestSample<O, N, E> getTestSample(int id, Class<O> oldType, Class<N> newType,
            Function<Integer, O> oldGenerator, Function<Integer, N> newGenerator, Class<E> expectedType) {

        String device = DEVICE;
        String property = PROPERTY_PREFIX + id;
        String field = FIELD_PREFIX + id;

        PromotionTestSample<O, N, E> promotionTestSample = new PromotionTestSample<>();
        promotionTestSample.setId(id);
        promotionTestSample.setDevice(device);
        promotionTestSample.setProperty(property);
        promotionTestSample.setEntityKeyValues(ImmutableMap.of(DEVICE_KEY, device, PROPERTY_KEY, property));
        promotionTestSample.setField(FIELD_PREFIX + id);
        promotionTestSample.setVariable(String.join(":", device, property, field));
        promotionTestSample.setOldType(oldType);
        promotionTestSample.setNewType(newType);
        promotionTestSample.setExpectedType(expectedType);
        promotionTestSample.setOldGenerator(oldGenerator);
        promotionTestSample.setNewGenerator(newGenerator);

        log.info("Created TestSample with the next info: " + promotionTestSample.printInfo());
        return promotionTestSample;
    }

    // passing test type string in order to have it in the generated test name (useful for detection of the failed test)
    public static Stream<Arguments> provideTestCasesByEntities() {
        return testSamples.stream().map(testSample -> Arguments.of("byEntities", testSample));
    }

    public static Stream<Arguments> provideTestCasesByVariables() {
        return testSamples.stream().map(testSample -> Arguments.of("byVariables", testSample));
    }

    @ParameterizedTest
    @MethodSource("provideTestCasesByEntities")
    public void shouldExtractPublishedDataWithPromotableFieldTypesByEntities(String type,
            PromotionTestSample<?, ?, ?> testSample) {
        log.info("TestType: " + type);
        //given
        Set<String> expectedColumns = getDefaultFieldsForCMWEntityQueryWith();
        expectedColumns.add(testSample.getField());

        Class<?> expected = testSample.getExpectedType();
        //when
        Dataset<Row> dataset = DataQuery.builder(sparkSession).byEntities().system(SYSTEM)
                .startTime(FROM)
                .endTime(END_TIME)
                .entity().keyValueLike(DEVICE_KEY, testSample.getDevice())
                .keyValue(PROPERTY_KEY, testSample.getProperty())
                .build();
        //then
        assertEquals(RECORDS_NUMBER, dataset.count());

        Set<String> datasetColumns = Sets.newHashSet(dataset.columns());
        assertEquals(expectedColumns, datasetColumns);

        List<Object> retrieved = getFieldData(dataset, testSample.getDevice(), testSample.getProperty(),
                testSample.getField());
        for (Object retrievedElement : retrieved) {
            if (isGenericRowWithSchema(retrievedElement)) {
                assertEquals(expected.getName(), getArrayTypeFromGenericRowWithSchema(
                        (GenericRowWithSchema) retrievedElement));
            } else {
                assertEquals(expected.getName(), retrievedElement.getClass().getName());
            }
        }

    }

    @ParameterizedTest
    @MethodSource("provideTestCasesByVariables")
    public void shouldExtractPublishedDataWithPromotableFieldTypesByVariables(String type,
            PromotionTestSample<?, ?, ?> testSample) {
        log.info("TestType: " + type);
        //given
        Set<String> expectedColumns = getDefaultFieldsForVariableQuery();

        Class<?> expected = testSample.getExpectedType();
        //when
        Dataset<Row> dataset = DataQuery.builder(sparkSession).byVariables().system(SYSTEM)
                .startTime(FROM)
                .endTime(END_TIME)
                .variable(testSample.getVariable())
                .build();
        //then
        assertEquals(RECORDS_NUMBER, dataset.count());

        Set<String> datasetColumns = Sets.newHashSet(dataset.columns());
        assertEquals(expectedColumns, datasetColumns);

        List<Object> retrieved = getVariableData(dataset, testSample.getVariable());
        for (Object retrievedElement : retrieved) {
            if (isGenericRowWithSchema(retrievedElement)) {
                assertEquals(expected.getName(), getArrayTypeFromGenericRowWithSchema(
                        (GenericRowWithSchema) retrievedElement));
            } else {
                assertEquals(expected.getName(), retrievedElement.getClass().getName());
            }
        }

    }

    private static boolean isGenericRowWithSchema(Object elem) {
        return elem.getClass().getName().equals("org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema");
    }

    private static String getArrayTypeFromGenericRowWithSchema(GenericRowWithSchema genericRowWithSchema) {
        String type = genericRowWithSchema.getList(0).get(0).getClass().getName();
        return MAP_TYPE_TO_ARRAY.get(type).getName();
    }

    @Data
    private static class PromotionTestSample<O, N, E> {
        private int id;
        private String device;
        private String property;
        private Map<String, Object> entityKeyValues;
        private String field;
        private String variable;

        private Class<O> oldType;
        private Class<N> newType;
        private Class<E> expectedType;

        private Function<Integer, O> oldGenerator;
        private Function<Integer, N> newGenerator;

        @Override
        public String toString() {
            return "PromotionTestSample{ " +
                    "id=" + id +
                    ", oldType=" + oldType.getSimpleName() +
                    ", newType=" + newType.getSimpleName() +
                    ", expectedType=" + expectedType.getSimpleName() +
                    '}';
        }

        public String printInfo() {
            return "{ id=" + id +
                    ", device='" + device + '\'' +
                    ", property='" + property + '\'' +
                    ", entityKeyValues=" + entityKeyValues +
                    ", field='" + field + '\'' +
                    ", variable='" + variable + '\'' +
                    ", oldType=" + oldType +
                    ", newType=" + newType +
                    ", expectedType=" + expectedType +
                    ", oldGenerator=" + oldGenerator +
                    ", newGenerator=" + newGenerator +
                    '}';
        }
    }
}
