package cern.nxcals.integrationtests.extraction.custom.data.builders.expanded;

import cern.nxcals.api.config.SparkContext;
import cern.nxcals.api.custom.extraction.data.builders.expanded.DataQuery;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.DependsOn;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = SparkContext.class)
@SpringBootApplication
@Slf4j
@DependsOn("kerberos")
public class DataQueryExpandedTest {

    @Autowired
    private SparkSession sparkSession;

    @Test
    public void shouldGetListOfDatasets() {

        Map<String, Object> keyValuesLike = new HashMap<>();
        keyValuesLike.put("device", "NXCALS_MONITORING_DEV%");

        List<Dataset<Row>> datasets = DataQuery.builder(sparkSession).entities().system("MOCK-SYSTEM")
                .keyValuesLike(keyValuesLike).timeWindow(
                        Instant.now().minus(1, ChronoUnit.DAYS), Instant.now()).build();

        assertTrue(datasets.size() > 1);

    }

}
