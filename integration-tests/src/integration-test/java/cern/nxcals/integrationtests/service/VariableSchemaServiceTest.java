package cern.nxcals.integrationtests.service;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableDeclaredType;
import cern.nxcals.api.extraction.metadata.queries.Entities;
import cern.nxcals.api.metadata.queries.Variables;
import cern.nxcals.common.utils.AvroUtils;
import org.apache.avro.Schema;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static cern.nxcals.integrationtests.utils.ServicesHelper.registerSimpleVariable;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class VariableSchemaServiceTest extends AbstractTest {
    private static final String NAME_PREFIX = VariableSchemaServiceTest.class.getName();
    private static final Instant NOW = Instant.now();
    private static final int DAYS_IN_MONTH = 30;
    private static final TimeWindow LAST_MONTH = TimeWindow.between(NOW.minus(DAYS_IN_MONTH, ChronoUnit.DAYS), NOW);

    private static final Entity monitoringEntity = entityService.findOne(
            Entities.suchThat().keyValues().eq(SYSTEM, Map.of(DEVICE_KEY, "NXCALS_MONITORING_DEV1"))
    ).get();

    private static final String FIELD_IN_MONITORING_ENTITY_1 = "boolField";
    private static final String FIELD_IN_MONITORING_ENTITY_2 = "longField2";

    @Test
    void shouldObtainSchemaForVariableToFieldInMonitoringEntity() {
        // given
        Variable variable = registerSimpleVariable(getRandomName(NAME_PREFIX), monitoringEntity,
                FIELD_IN_MONITORING_ENTITY_1, VariableDeclaredType.NUMERIC);
        // when
        Map<TimeWindow, Schema> schemas = variableService.findSchemas(variable, LAST_MONTH);

        assertEquals(1, schemas.size());
        assertTrue(schemas.containsKey(LAST_MONTH));

        Schema schema = schemas.get(LAST_MONTH);

        assertEquals(Schema.Type.UNION, schema.getType());
        assertEquals(Schema.Type.BOOLEAN, AvroUtils.getSchemaTypeFor(schema));
    }

    @Test
    void shouldObtainSchemaForVariableToMonitoringEntity() {
        // given
        Variable variable = registerSimpleVariable(getRandomName(NAME_PREFIX), monitoringEntity,
                null, VariableDeclaredType.NUMERIC);

        // when
        Map<TimeWindow, Schema> schemas = variableService.findSchemas(variable, LAST_MONTH);

        assertEquals(DAYS_IN_MONTH + 1, schemas.size());
    }

    @Test
    void shouldObtainSchemaForVariableToMonitoringEntityForNotClosedTimeWindow() {
        // given
        Variable variable = registerSimpleVariable(getRandomName(NAME_PREFIX), monitoringEntity,
                null, VariableDeclaredType.NUMERIC);

        Instant monthBefore = NOW.minus(DAYS_IN_MONTH, ChronoUnit.DAYS);
        TimeWindow timeWindow = TimeWindow.after(monthBefore);
        // when
        Map<TimeWindow, Schema> schemas = variableService.findSchemas(variable, timeWindow);

        assertEquals(DAYS_IN_MONTH + 1, schemas.size());
    }

    @Test
    void shouldReturnEmptyIfVariableDoesntHaveConfig() {
        // given
        Variable variableToRegister = Variable.builder()
                .variableName(getRandomName(NAME_PREFIX))
                .systemSpec(SYSTEM)
                .declaredType(VariableDeclaredType.FUNDAMENTAL)
                .build();

        Variable variable = variableService.create(variableToRegister);

        // when
        Map<TimeWindow, Schema> schemas = variableService.findSchemas(variable, LAST_MONTH);

        assertEquals(0, schemas.size());
    }

    @Test
    void shouldReturnEmptyIfVariableDoesntHaveValidConfig() {
        // given
        Variable variable = variable(monitoringEntity, NAME_PREFIX);

        // when
        Map<TimeWindow, Schema> schemas = variableService.findSchemas(variable, LAST_MONTH);

        assertEquals(0, schemas.size());
    }

    @Test
    void shouldReturnConfigForTwoVariablesById() {
        // given
        Variable variable1 = registerSimpleVariable(getRandomName(NAME_PREFIX), monitoringEntity,
                FIELD_IN_MONITORING_ENTITY_1, VariableDeclaredType.NUMERIC);

        Variable variable2 = registerSimpleVariable(getRandomName(NAME_PREFIX), monitoringEntity,
                FIELD_IN_MONITORING_ENTITY_2, VariableDeclaredType.NUMERIC);

        Set<Long> variableIds = Set.of(variable1.getId(), variable2.getId());
        // when
        Map<Long, Map<TimeWindow, Schema>> schemas = variableService.findSchemasById(variableIds, LAST_MONTH);

        assertEquals(2, schemas.size());
        assertEquals(variableIds, schemas.keySet());

        Schema schemaVar1 = schemas.get(variable1.getId()).get(LAST_MONTH);
        assertEquals(Schema.Type.UNION, schemaVar1.getType());
        assertEquals(Schema.Type.BOOLEAN, AvroUtils.getSchemaTypeFor(schemaVar1));

        Schema schemaVar2 = schemas.get(variable2.getId()).get(LAST_MONTH);
        assertEquals(Schema.Type.UNION, schemaVar2.getType());
        assertEquals(Schema.Type.LONG, AvroUtils.getSchemaTypeFor(schemaVar2));
    }

    @Test
    void shouldReturnConfigForTwoVariables() {
        // given
        Variable variable1 = registerSimpleVariable(getRandomName(NAME_PREFIX), monitoringEntity,
                FIELD_IN_MONITORING_ENTITY_1, VariableDeclaredType.NUMERIC);

        Variable variable2 = registerSimpleVariable(getRandomName(NAME_PREFIX), monitoringEntity,
                FIELD_IN_MONITORING_ENTITY_2, VariableDeclaredType.NUMERIC);

        Set<Variable> variables = Set.of(variable1, variable2);
        // when
        Map<Variable, Map<TimeWindow, Schema>> schemas = variableService.findSchemas(variables, LAST_MONTH);

        assertEquals(2, schemas.size());
        assertEquals(variables, schemas.keySet());

        Schema schemaVar1 = schemas.get(variable1).get(LAST_MONTH);
        assertEquals(Schema.Type.UNION, schemaVar1.getType());
        assertEquals(Schema.Type.BOOLEAN, AvroUtils.getSchemaTypeFor(schemaVar1));

        Schema schemaVar2 = schemas.get(variable2).get(LAST_MONTH);
        assertEquals(Schema.Type.UNION, schemaVar2.getType());
        assertEquals(Schema.Type.LONG, AvroUtils.getSchemaTypeFor(schemaVar2));
    }

    @AfterAll
    public static void cleanUp() {
        Set<Variable> variables = variableService.findAll(Variables.suchThat().variableName().like(NAME_PREFIX + "%"));
        Set<Long> variableIds = variables.stream().map(Variable::getId).collect(Collectors.toSet());
        variableService.deleteAll(variableIds);
    }
}
