/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.integrationtests.service;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.Group;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.Visibility;
import cern.nxcals.api.exceptions.FatalDataConflictRuntimeException;
import cern.nxcals.api.exceptions.NotFoundRuntimeException;
import cern.nxcals.api.extraction.metadata.queries.Groups;
import cern.nxcals.common.utils.ReflectionUtils;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class GroupServiceTest extends AbstractTest {
    @Test
    public void shouldCreateGroup() {
        Group groupOld = group().build();
        Group groupNew = internalGroupService.create(groupOld);

        assertEquals(groupNew, groupOld);

        assertTrue(groupNew.hasId());
        assertTrue(groupNew.hasVersion());

        assertFalse(groupOld.hasId());
        assertFalse(groupOld.hasVersion());
    }

    @Test
    public void shouldFailOnRepeatedNames() {
        Group groupOld = group().build();
        internalGroupService.create(groupOld);
        assertThrows(FatalDataConflictRuntimeException.class, () -> internalGroupService.create(groupOld));
    }

    @Test
    public void shouldUpdateGroup() {
        Group groupOld = group().build();
        Group groupNew = internalGroupService.create(groupOld);

        Group groupEdited = internalGroupService
                .update(groupNew.toBuilder().name(groupOld.getName() + "_edited").build());

        assertEquals(groupEdited.getName(), groupOld.getName() + "_edited");
    }

    @Test
    public void shouldThrowExceptionOnDeleteWhenGroupDoesNotExist() {
        long nonExistingGroupId = Long.MIN_VALUE + 200;
        assertThrows(RuntimeException.class, () -> internalGroupService.delete(nonExistingGroupId));
    }

    @Test
    public void shouldDeleteGroup() {
        Group group = internalGroupService.create(group().build());

        internalGroupService.delete(group.getId());

        Group fetchedGroup = internalGroupService.findById(group.getId()).orElse(null);
        assertNull(fetchedGroup);
    }

    @Test
    public void shouldFindGroup() {
        assertGroupExists("myGroupName");
        assertGroupExists("MyGroupName");

        assertThat(internalGroupService.findAll(Groups.suchThat().name().eq("myGroupName"))).hasSize(1);
        assertThat(internalGroupService.findAll(Groups.suchThat().name().eq("MyGroupName"))).hasSize(1);
        assertThat(internalGroupService.findAll(Groups.suchThat().name().caseInsensitive().eq("MYGROUPNAME")).size())
                .isGreaterThanOrEqualTo(2);
    }

    private void assertGroupExists(String name) {
        if (!internalGroupService.findOne(Groups.suchThat().name().eq(name)).isPresent()) {
            internalGroupService.create(group().name(name).build());
        }
    }

    @Test
    public void shouldCreateGroupWithVariables() {
        Group group = internalGroupService.create(group().build());
        assertNotNull(group);

        Entity entity = entity(10);
        final int variableCount = 3;
        Map<String, Set<Long>> associations = new HashMap<>();
        for (int i = 0; i < variableCount; i++) {
            Variable variable = variable(entity);
            associations.computeIfAbsent(null, k -> new HashSet<>()).add(variable.getId());
            associations.computeIfAbsent("a", k -> new HashSet<>()).add(variable.getId());
        }
        internalGroupService.addVariables(group.getId(), associations);

        Map<String, Set<Variable>> fetchedVariables = internalGroupService.getVariables(group.getId());
        assertNotNull(fetchedVariables);
        assertThat(fetchedVariables).containsKeys("a", null);
        assertThat(fetchedVariables.get(null)).hasSize(variableCount);
        assertThat(fetchedVariables.get("a")).hasSize(variableCount);

        Set<Variable> allVariables = fetchedVariables.values().stream().flatMap(Collection::stream)
                .collect(Collectors.toSet());
        assertThat(allVariables).hasSize(variableCount);

        internalGroupService.removeVariables(group.getId(), associations);

        Map<String, Set<Variable>> resultVariables = internalGroupService.getVariables(group.getId());
        assertNotNull(resultVariables);
        assertThat(resultVariables).isEmpty();
    }

    @Test
    public void shouldCreateGroupWithVariablesAndOverwriteAssociations() {
        Group group = internalGroupService.create(group().build());
        assertNotNull(group);

        Entity entity = entity(10);
        final int variableCount = 3;
        Map<String, Set<Long>> associations = new HashMap<>();
        for (int i = 0; i < variableCount; i++) {
            Variable variable = variable(entity);
            associations.computeIfAbsent(null, k -> new HashSet<>()).add(variable.getId());
            associations.computeIfAbsent("a", k -> new HashSet<>()).add(variable.getId());
        }
        internalGroupService.addVariables(group.getId(), associations);

        Map<String, Set<Variable>> fetchedVariables = internalGroupService.getVariables(group.getId());
        assertNotNull(fetchedVariables);
        assertThat(fetchedVariables).containsKeys("a", null);
        assertThat(fetchedVariables.get(null)).hasSize(variableCount);
        assertThat(fetchedVariables.get("a")).hasSize(variableCount);

        Set<Variable> allVariables = fetchedVariables.values().stream().flatMap(Collection::stream)
                .collect(Collectors.toSet());
        assertThat(allVariables).hasSize(variableCount);

        // now lets's overwrite
        String targetAssociation = "should-overwrite-other";
        Variable overwriteVariable = variable(entity, "overwrite-previous");
        Map<String, Set<Long>> overwriteAssociations = new HashMap<>();
        overwriteAssociations.put(targetAssociation, Collections.singleton(overwriteVariable.getId()));
        internalGroupService.setVariables(group.getId(), overwriteAssociations);

        Map<String, Set<Variable>> resultVariables = internalGroupService.getVariables(group.getId());
        assertNotNull(resultVariables);
        assertThat(resultVariables).size().isOne();
        assertThat(resultVariables.get(targetAssociation)).isNotNull();
        assertThat(resultVariables.get(targetAssociation).iterator().next()).isEqualTo(overwriteVariable);

        Set<Variable> allResultVariables = resultVariables.values().stream()
                .flatMap(Collection::stream).collect(Collectors.toSet());
        assertThat(allResultVariables).size().isOne();
    }

    @Test
    public void shouldCreateGroupWithEntities() {
        Group group = internalGroupService.create(group().build());
        assertNotNull(group);

        int entityCount = 3;
        Map<String, Set<Long>> associations = new HashMap<>();
        for (int i = 0; i < entityCount; i++) {
            Entity entity = entity(i);
            associations.computeIfAbsent(null, k -> new HashSet<>()).add(entity.getId());
            associations.computeIfAbsent("b", k -> new HashSet<>()).add(entity.getId());
        }
        internalGroupService.addEntities(group.getId(), associations);

        Map<String, Set<Entity>> fetchedEntities = internalGroupService.getEntities(group.getId());
        assertNotNull(fetchedEntities);
        assertThat(fetchedEntities).containsKeys("b", null);
        assertThat(fetchedEntities.get(null)).hasSize(entityCount);
        assertThat(fetchedEntities.get("b")).hasSize(entityCount);

        Set<Entity> allEntities = fetchedEntities.values().stream().flatMap(Collection::stream)
                .collect(Collectors.toSet());
        assertThat(allEntities).hasSize(entityCount);

        internalGroupService.removeEntities(group.getId(), associations);

        Map<String, Set<Entity>> resultEntities = internalGroupService.getEntities(group.getId());
        assertNotNull(resultEntities);
        assertThat(resultEntities).isEmpty();
    }

    @Test
    public void shouldCreateGroupWithEntitiesAndOverwriteAssociations() {
        Group group = internalGroupService.create(group().build());
        assertNotNull(group);

        int entityCount = 3;
        Map<String, Set<Long>> associations = new HashMap<>();
        for (int i = 0; i < entityCount; i++) {
            Entity entity = entity(i);
            associations.computeIfAbsent(null, k -> new HashSet<>()).add(entity.getId());
            associations.computeIfAbsent("b", k -> new HashSet<>()).add(entity.getId());
        }
        internalGroupService.addEntities(group.getId(), associations);

        Map<String, Set<Entity>> fetchedEntities = internalGroupService.getEntities(group.getId());
        assertNotNull(fetchedEntities);
        assertThat(fetchedEntities).containsKeys("b", null);
        assertThat(fetchedEntities.get(null)).hasSize(entityCount);
        assertThat(fetchedEntities.get("b")).hasSize(entityCount);

        Set<Entity> allEntities = fetchedEntities.values().stream().flatMap(Collection::stream)
                .collect(Collectors.toSet());
        assertThat(allEntities).hasSize(entityCount);

        // now lets's overwrite
        String targetAssociation = "should-overwrite-other";
        Entity overwriteEntity = entity(10, "overwrite-previous");
        Map<String, Set<Long>> overwriteAssociations = new HashMap<>();
        overwriteAssociations.put(targetAssociation, Collections.singleton(overwriteEntity.getId()));
        internalGroupService.setEntities(group.getId(), overwriteAssociations);

        Map<String, Set<Entity>> resultEntities = internalGroupService.getEntities(group.getId());
        assertNotNull(resultEntities);
        assertThat(resultEntities).size().isOne();
        assertThat(resultEntities.get(targetAssociation)).isNotNull();
        assertThat(resultEntities.get(targetAssociation).iterator().next()).isEqualTo(overwriteEntity);

        Set<Entity> allResultVariables = resultEntities.values().stream()
                .flatMap(Collection::stream).collect(Collectors.toSet());
        assertThat(allResultVariables).size().isOne();
    }

    @Test
    public void shouldNotAllowCreateWithId() {
        Group groupOld = group().build();
        ReflectionUtils.enhanceWithId(groupOld, 1L);
        assertThrows(FatalDataConflictRuntimeException.class, () -> internalGroupService.create(groupOld));
    }

    @Test
    public void shouldNotAllowUpdateWithoutId() {
        Group groupOld = group().build();
        assertThrows(IllegalArgumentException.class, () -> internalGroupService.update(groupOld));
    }

    @Test
    public void shouldNotAllowManualIdOverride() {
        Group groupOld = group().build();
        Group groupNew = internalGroupService.create(groupOld);
        ReflectionUtils.enhanceWithId(groupNew, -1L);
        assertThrows(NotFoundRuntimeException.class, () -> internalGroupService.update(groupNew));
    }


    private Group.Builder group() {
        return Group.builder().label("my-group-label").visibility(Visibility.PUBLIC).name("group_" + getRandomString())
                .systemSpec(SYSTEM);
    }
}
