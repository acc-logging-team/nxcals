package cern.nxcals.integrationtests.service;

import cern.nxcals.api.custom.domain.FundamentalFilter;
import cern.nxcals.api.custom.domain.util.FundamentalMapper;
import cern.nxcals.api.domain.Group;
import cern.nxcals.api.domain.Visibility;
import cern.nxcals.common.utils.ReflectionUtils;
import com.google.common.collect.Sets;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FundamentalFilterGroupServiceTest extends AbstractTest {
    private static final String FUNDAMENTAL_PROP_PREFIX = FundamentalMapper.FUNDAMENTAL_GROUP_PROPERTY_KEY_PREFIX;

    @Test
    public void shouldReturnEmptyMapOnToGroupPropertyWhenFiltersHasNoFieldProperties() {
        FundamentalFilter emptyFilter = FundamentalFilter.builder().build();

        Map<String, String> groupProperties = FundamentalMapper.toGroupProperties(Collections.singleton(emptyFilter));
        assertNotNull(groupProperties);
        assertEquals(1, groupProperties.size());

        Group group = groupBuilder()
                .properties(groupProperties)
                .property("extraProperty", "test value")
                .build();

        Group persistedGroup = internalGroupService.create(group);
        assertNotNull(persistedGroup);

        Group fetchedGroup = internalGroupService.findById(persistedGroup.getId()).orElseThrow(() ->
                new IllegalStateException("should be able to find group with id: " + persistedGroup.getId()));
        assertNotNull(fetchedGroup);
        Map<String, String> fetchedGroupProperties = fetchedGroup.getProperties();
        assertEquals(2, fetchedGroupProperties.size());

        String expectedFilterJson = "{\"destination\":null,\"lsaCycle\":null,\"timingUser\":null,\"accelerator\":null}";

        String filterJson = fetchedGroupProperties.get(FUNDAMENTAL_PROP_PREFIX + emptyFilter.getId());
        assertNotNull(filterJson);
        assertEquals(expectedFilterJson, filterJson);

        Set<FundamentalFilter> fetchedFundamentalFilters = FundamentalMapper.toFundamentals(fetchedGroupProperties);
        assertNotNull(fetchedFundamentalFilters);
        assertEquals(1, fetchedFundamentalFilters.size());
        FundamentalFilter fetchedFilter = fetchedFundamentalFilters.iterator().next();
        assertNotNull(fetchedFilter);
        assertEquals(emptyFilter.getId(), fetchedFilter.getId());
        assertEquals(emptyFilter, fetchedFilter);

    }

    @Test
    public void shouldMapFundamentalFiltersAndCreateGroupWithProperties() {
        FundamentalFilter filter1 = FundamentalFilter.builder().accelerator("accelerator1")
                .destination("dest1").lsaCycle("cycle1").timingUser("user1").build();
        FundamentalFilter filter2 = FundamentalFilter.builder().accelerator("accelerator2")
                .destination("dest2").build();

        Map<String, String> groupProperties = FundamentalMapper.toGroupProperties(Sets.newHashSet(filter1, filter2));
        assertNotNull(groupProperties);

        Group group = groupBuilder().properties(groupProperties).build();
        Group persistedGroup = internalGroupService.create(group);
        assertEquals(persistedGroup, group);

        Map<String, String> persistedProperties = persistedGroup.getProperties();
        assertNotNull(persistedProperties);
        assertEquals(2, persistedProperties.size());

        Map<String, FundamentalFilter> persistedFilters = FundamentalMapper.toFundamentals(persistedProperties).stream()
                .collect(Collectors.toMap(FundamentalFilter::getId, Function.identity()));

        FundamentalFilter persistedFilter1 = persistedFilters.get(filter1.getId());
        assertNotNull(persistedFilter1);
        assertEquals(filter1.getId(), persistedFilter1.getId());
        assertEquals(filter1, persistedFilter1);

        FundamentalFilter persistedFilter2 = persistedFilters.get(filter2.getId());
        assertNotNull(persistedFilter2);
        assertEquals(filter2.getId(), persistedFilter2.getId());
        assertEquals(filter2, persistedFilter2);
    }

    @Test
    public void shouldMapFundamentalFiltersAndUpdateGroupWithProperties() {
        FundamentalFilter filter1 = FundamentalFilter.builder().accelerator("accelerator1")
                .destination("dest1").lsaCycle("cycle1").timingUser("user1").build();
        FundamentalFilter filter2 = FundamentalFilter.builder().accelerator("accelerator2")
                .destination("dest2").build();

        Map<String, String> groupProperties = FundamentalMapper.toGroupProperties(Sets.newHashSet(filter1, filter2));
        assertNotNull(groupProperties);
        Group group = groupBuilder()
                .properties(groupProperties)
                .build();

        Group persistedGroup = internalGroupService.create(group);

        assertEquals(persistedGroup, group);
        assertEquals(2, persistedGroup.getProperties().size());
        assertTrue(persistedGroup.hasId());
        assertTrue(persistedGroup.hasVersion());

        FundamentalFilter filter3 = FundamentalFilter.builder().accelerator("accelerator3")
                .destination("dest3").lsaCycle("cycle3").timingUser("user3").build();
        Map<String, String> moreGroupProperties = FundamentalMapper.toGroupProperties(Collections.singleton(filter3));

        Group groupUpdated = internalGroupService.update(persistedGroup.toBuilder()
                .properties(moreGroupProperties).build());

        Map<String, String> updatedProperties = groupUpdated.getProperties();
        assertEquals(3, updatedProperties.size());

        Map<String, FundamentalFilter> fetchedFilters = FundamentalMapper.toFundamentals(updatedProperties).stream()
                .collect(Collectors.toMap(FundamentalFilter::getId, Function.identity()));

        FundamentalFilter fetchedFilter1 = fetchedFilters.get(filter1.getId());
        assertNotNull(fetchedFilter1);
        assertEquals(fetchedFilter1.getId(), filter1.getId());
        assertEquals(fetchedFilter1, filter1);

        FundamentalFilter fetchedFilter2 = fetchedFilters.get(filter2.getId());
        assertNotNull(fetchedFilter2);
        assertEquals(fetchedFilter2.getId(), filter2.getId());
        assertEquals(fetchedFilter2, filter2);

        FundamentalFilter fetchedFilter3 = fetchedFilters.get(filter3.getId());
        assertNotNull(fetchedFilter3);
        assertEquals(fetchedFilter3.getId(), filter3.getId());
        assertEquals(fetchedFilter3, filter3);
    }

    // test: toFundamentals

    @Test
    public void shouldReturnEmptyInstanceOnToFundamentalsWhenFundamentalContentIsEmpty() {
        String filterId = "my-test-id-1";

        Group group = groupBuilder()
                .property(FUNDAMENTAL_PROP_PREFIX + filterId, "{}")
                .property("testProperty", "testValue")
                .build();

        Group persistedGroup = internalGroupService.create(group);

        assertEquals(persistedGroup, group);
        assertEquals(2, persistedGroup.getProperties().size());
        assertTrue(persistedGroup.hasId());
        assertTrue(persistedGroup.hasVersion());

        Group fetchedGroup = internalGroupService.findById(persistedGroup.getId()).orElseThrow(() ->
                new IllegalStateException("should be able to find group with id: " + persistedGroup.getId()));

        Set<FundamentalFilter> fundamentalFilters = FundamentalMapper.toFundamentals(fetchedGroup.getProperties());
        assertNotNull(fundamentalFilters);
        assertEquals(1, fundamentalFilters.size());

        FundamentalFilter filter = fundamentalFilters.iterator().next();
        assertEquals(filterId, filter.getId());
        assertNull(filter.getAccelerator());
        assertNull(filter.getDestination());
        assertNull(filter.getLsaCycle());
        assertNull(filter.getTimingUser());
    }

    @Test
    public void shouldReturnFiltersOnToFundamentals() {
        String filterId1 = "01";
        String filterId2 = "02";
        FundamentalFilter filter1 = buildFilterWithPredefinedId(filterId1, "aDest", "aCycle", "aUser",
                "anAccelerator");
        FundamentalFilter filter2 = buildFilterWithPredefinedId(filterId2, "otherDest", "aCycle", null, null);

        Map<String, String> groupProperties = FundamentalMapper.toGroupProperties(Sets.newHashSet(filter1, filter2));
        assertNotNull(groupProperties);

        Group group = groupBuilder()
                .properties(groupProperties)
                .property("extraKey", "test value")
                .build();

        Group persistedGroup = internalGroupService.create(group);
        assertEquals(persistedGroup, group);
        assertEquals(3, persistedGroup.getProperties().size());
        assertTrue(persistedGroup.hasId());
        assertTrue(persistedGroup.hasVersion());

        Group fetchedGroup = internalGroupService.findById(persistedGroup.getId()).orElseThrow(() ->
                new IllegalStateException("should be able to find group with id: " + persistedGroup.getId()));

        Set<FundamentalFilter> fundamentalFilters = FundamentalMapper.toFundamentals(fetchedGroup.getProperties());
        assertEquals(2, fundamentalFilters.size());

        Map<String, FundamentalFilter> fundamentalFilterById = fundamentalFilters.stream()
                .collect(Collectors.toMap(FundamentalFilter::getId, Function.identity()));

        FundamentalFilter fetchedFilter1 = fundamentalFilterById.get(filterId1);
        assertNotNull(fetchedFilter1);
        assertEquals(filterId1, fetchedFilter1.getId()); //explicit check for injected id value
        assertEquals(filter1, fetchedFilter1);

        FundamentalFilter fetchedFilter2 = fundamentalFilterById.get(filterId2);
        assertNotNull(fetchedFilter2);
        assertEquals(filterId2, fetchedFilter2.getId()); //explicit check for injected id value
        assertEquals(filter2, fetchedFilter2);
    }

    private FundamentalFilter buildFilterWithPredefinedId(String id, String destination, String lsaCycle,
            String timingUser,
            String accelerator) {
        return ReflectionUtils.builderInstance(FundamentalFilter.InnerBuilder.class)
                .id(id)
                .destination(destination)
                .lsaCycle(lsaCycle)
                .timingUser(timingUser)
                .accelerator(accelerator)
                .build();
    }

    private Group.Builder groupBuilder() {
        return Group.builder().label("my-group-label").visibility(Visibility.PUBLIC).name("group_" + getRandomString())
                .systemSpec(SYSTEM);
    }

}
