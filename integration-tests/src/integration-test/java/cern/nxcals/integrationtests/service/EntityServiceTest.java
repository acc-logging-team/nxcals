/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.integrationtests.service;

import cern.nxcals.api.domain.CreateEntityRequest;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.EntityHistory;
import cern.nxcals.api.domain.KeyValues;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.exceptions.FatalDataConflictRuntimeException;
import cern.nxcals.api.extraction.metadata.InternalServiceClientFactory;
import cern.nxcals.api.extraction.metadata.queries.Entities;
import cern.nxcals.api.metadata.queries.ConditionWithOptions;
import cern.nxcals.api.metadata.queries.EntityQueryWithOptions;
import cern.nxcals.internal.extraction.metadata.InternalEntityService;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.LongConsumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Created by jwozniak on 02/07/17.
 */
public class EntityServiceTest extends AbstractTest {
    private static cern.nxcals.api.metadata.queries.Entities newEntities() {
        return cern.nxcals.api.metadata.queries.Entities.suchThat();
    }

    @Test
    public void shouldCreateAndFindEntity() {

        KeyValues entityKeyValues = new KeyValues(ENTITY_KEY_VALUES.get(DEVICE_KEY).hashCode(), ENTITY_KEY_VALUES);
        KeyValues partitionKeyValues = new KeyValues(0, PARTITION_KEY_VALUES);

        Entity entityData1 = internalEntityService
                .findOrCreateEntityFor(SYSTEM.getId(), entityKeyValues, partitionKeyValues, "brokenSchema1",
                        RECORD_TIMESTAMP);

        Entity entityData2 = internalEntityService
                .findOrCreateEntityFor(SYSTEM.getId(), entityKeyValues, partitionKeyValues, "brokenSchema1",
                        RECORD_TIMESTAMP);

        assertEquals(1, entityData1.getEntityHistory().size());
        assertEquals(1, entityData2.getEntityHistory().size());

        Entity entityData4 = internalEntityService.findOne(
                        Entities.suchThat().systemId().eq(SYSTEM.getId()).and().keyValues().eq(SYSTEM, ENTITY_KEY_VALUES))
                .orElseThrow(() -> new IllegalArgumentException("No such entity"));
        assertNotNull(entityData4, "Entity " + ENTITY_KEY_VALUES + " not found");

        assertEquals(entityData1.getEntityKeyValues(), entityData4.getEntityKeyValues(),
                "Entities created and found not equal");

        Entity entityData5 = internalEntityService
                .findOneWithHistory(Entities.suchThat().id().eq(entityData4.getId()), 0, RECORD_TIMESTAMP)
                .orElseThrow(() -> new IllegalArgumentException("No such entity"));

        assertNotNull(entityData5, "Entity " + ENTITY_KEY_VALUES + " not found between 0 and " + RECORD_TIMESTAMP);

        Entity entityData6 = internalEntityService
                .findOne(newEntities().id().eq(entityData4.getId()).withOptions().noHistory())
                .orElseThrow(() -> new IllegalArgumentException("No such entity"));

        assertAll(() -> assertNotNull(entityData6,
                        "Entity " + ENTITY_KEY_VALUES + " not found between 0 and " + RECORD_TIMESTAMP),
                () -> assertEquals(0, entityData6.getEntityHistory().size()),
                () -> assertNotEquals(0, entityData5.getEntityHistory().size())
        );
    }

    @Test
    public void shouldCreateAndFindEntityUsingIds() {

        KeyValues entityKeyValues = new KeyValues(ENTITY_KEY_VALUES.get(DEVICE_KEY).hashCode(), ENTITY_KEY_VALUES);
        KeyValues partitionKeyValues = new KeyValues(0, PARTITION_KEY_VALUES);

        long nextTimestamp = RECORD_TIMESTAMP + 100;
        String nextSchema = "brokenSchema2";
        Entity entityData1 = internalEntityService
                .findOrCreateEntityFor(SYSTEM.getId(), entityKeyValues, partitionKeyValues, "brokenSchema1",
                        RECORD_TIMESTAMP);

        assertEquals(1, entityData1.getEntityHistory().size());
        assertEquals(RECORD_TIMESTAMP, entityData1.getFirstEntityHistory().getValidity().getStartTimeNanos());

        Entity entityData2 = internalEntityService
                .findOrCreateEntityFor(SYSTEM.getId(), entityData1.getId(), entityData1.getPartition().getId(),
                        nextSchema, nextTimestamp);

        assertEquals(1, entityData2.getEntityHistory().size());
        assertEquals(nextTimestamp, entityData2.getFirstEntityHistory().getValidity().getStartTimeNanos());
        assertEquals(nextSchema, entityData2.getFirstEntityHistory().getEntitySchema().getSchemaJson());

    }

    @Test
    public void throwExceptionWhenCreatingEntityWithIncorrectPartitionId() {
        assertThrows(IllegalArgumentException.class, () -> internalEntityService
                .findOrCreateEntityFor(SYSTEM.getId(), Long.MIN_VALUE + 1, Long.MIN_VALUE + 1, "",
                        RECORD_TIMESTAMP + 50)); // WARN: hibernate cannot parse Long.MIN_VALUE xD don't remove "+ 1"
    }

    @Test
    public void shouldNotFindEntity() {
        Map<String, Object> keyValues = ImmutableMap.of(DEVICE_KEY, "%$#SHOULD_NOT_BE_FOUND");
        Entity entityData3 = entityService
                .findOne(Entities.suchThat().systemId().eq(SYSTEM.getId()).and().keyValues().eq(SYSTEM, keyValues))
                .orElse(null);
        assertNull(entityData3);
    }

    @Test
    public void shouldLockUntil() {
        KeyValues entityKeyValues = new KeyValues(ENTITY_KEY_VALUES.get(DEVICE_KEY).hashCode(), ENTITY_KEY_VALUES);
        KeyValues partitionKeyValues = new KeyValues(0, PARTITION_KEY_VALUES);

        Entity entityData1 = internalEntityService
                .findOrCreateEntityFor(SYSTEM.getId(), entityKeyValues, partitionKeyValues, "brokenSchema1",
                        RECORD_TIMESTAMP);
        Instant now = Instant.now();
        Entity newEntityData = entityData1.toBuilder().lockedUntilStamp(now).build();

        Set<Entity> updatedEntityData = internalEntityService.updateEntities(Sets.newHashSet(newEntityData));

        assertEquals(1, updatedEntityData.size());
        assertEquals(now, updatedEntityData.iterator().next().getLockedUntilStamp());
    }

    @Test
    public void shouldUpdateEntityWithRecVersionBiggerThanZero() {
        KeyValues entityKeyValues = new KeyValues(ENTITY_KEY_VALUES.get(DEVICE_KEY).hashCode(), ENTITY_KEY_VALUES);
        KeyValues partitionKeyValues = new KeyValues(0, PARTITION_KEY_VALUES);

        Entity newEntity = internalEntityService
                .findOrCreateEntityFor(SYSTEM.getId(), entityKeyValues, partitionKeyValues, "brokenSchema1",
                        RECORD_TIMESTAMP);
        Instant now = Instant.now();

        Entity lockedEntity = newEntity.toBuilder().lockedUntilStamp(now).build();

        Set<Entity> updateEntitiesResponse = internalEntityService.updateEntities(Sets.newHashSet(lockedEntity));

        assertEquals(1, updateEntitiesResponse.size());

        Entity initiallyUpdatedEntity = updateEntitiesResponse.iterator().next();
        assertEquals(now, initiallyUpdatedEntity.getLockedUntilStamp());
        assertTrue(initiallyUpdatedEntity.getRecVersion() > 0);

        Set<Entity> unlockedEntities = new HashSet<>();
        Entity unlockedEntity = initiallyUpdatedEntity.toBuilder().unlock().build();
        unlockedEntities.add(unlockedEntity);
        Set<Entity> lastUpdatedEntitiesResponse = internalEntityService.updateEntities(unlockedEntities);

        assertEquals(1, lastUpdatedEntitiesResponse.size());
        assertNull(lastUpdatedEntitiesResponse.iterator().next().getLockedUntilStamp());
    }

    /**
     * This test is done for the case where we have had problems with entity history optimistic locking exceptions.
     * The service client code should repeat the action N times and it should eventually succeed.
     *
     * @throws InterruptedException
     */
    @Test
    public void shouldNotFailWithOptimisticLockingWithManyThreads() throws InterruptedException {
        InternalEntityService internalEntityService = InternalServiceClientFactory.createEntityService();

        AtomicInteger count = new AtomicInteger(0);

        for (int j = 0; j < 50; ++j) {
            //This is created here to avoid interference with other tests. We must have new values here.
            String randomEntityKeySuffix = RandomStringUtils.randomAlphabetic(20);
            String randomPartitionKeySuffix = RandomStringUtils.randomAlphabetic(20);
            int size = 100;
            CountDownLatch latch = new CountDownLatch(size);
            for (int i = 0; i < size; ++i) {
                Thread th = new Thread(() -> {

                    ImmutableMap<String, Object> entityKeys = ImmutableMap.of(DEVICE_KEY,
                            "device_" + randomEntityKeySuffix + "___" + ThreadLocalRandom.current().nextInt(5));
                    ImmutableMap<String, Object> partitionKeys = ImmutableMap
                            .of("specification", "devClass1_" + randomPartitionKeySuffix);
                    KeyValues entityKeyValues = new KeyValues(0, entityKeys);
                    KeyValues partitionKeyValues = new KeyValues(0, partitionKeys);
                    try {
                        internalEntityService.findOrCreateEntityFor(SYSTEM.getId(), entityKeyValues, partitionKeyValues,
                                "some-schema", ThreadLocalRandom.current().nextInt(10000));
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        count.getAndIncrement();
                    } finally {
                        latch.countDown();
                    }
                });
                th.start();
            }

            //wait for the thread to finish
            latch.await();

            assertEquals(0, count.get());
        }
    }

    /**
     * This test is done for the case where we have had problems with entity history not creating the history with the earliest point (NXCALS-1907)
     *
     * @throws InterruptedException
     */
    @Test
    public void shouldCreateCorrectEntityHistoryWithManyThreads() throws InterruptedException {
        InternalEntityService internalEntityService = InternalServiceClientFactory.createEntityService();

        int max = 100000;
        for (int j = 0; j < 50; ++j) {
            //This is created here to avoid interference with other tests. We must have new values here.
            String randomPartitionKeySuffix = RandomStringUtils.randomAlphanumeric(20);
            ImmutableMap<String, Object> entityKeys = getRandomEntityKeys();
            ImmutableMap<String, Object> partitionKeys = ImmutableMap
                    .of("specification", "devClass1_" + randomPartitionKeySuffix);
            KeyValues entityKeyValues = new KeyValues(0, entityKeys);
            KeyValues partitionKeyValues = new KeyValues(0, partitionKeys);
            int size = 100;
            CountDownLatch latch = new CountDownLatch(size);
            //collection of all timestamps here to help analysing the problem
            List<Long> timestamps = Collections.synchronizedList(new ArrayList<>(size));
            for (int i = 0; i < size; ++i) {
                Thread th = new Thread(() -> {
                    try {
                        long timestamp = ThreadLocalRandom.current().nextInt(max);
                        timestamps.add(timestamp);
                        internalEntityService.findOrCreateEntityFor(SYSTEM.getId(), entityKeyValues, partitionKeyValues,
                                "some-schema", timestamp);
                    } finally {
                        latch.countDown();
                    }
                });
                th.start();
            }

            //wait for the thread to finish
            latch.await();

            //Get the entity with all history (this one gets it from the server and not cache).
            //The cached one might have some old history (depending on the ordering of threads)
            Entity entity = internalEntityService.findOneWithHistory(
                            Entities.suchThat().systemId().eq(SYSTEM.getId()).and().keyValues()
                                    .eq(SYSTEM, entityKeyValues.getKeyValues()), 0, max)
                    .orElseThrow(() -> new IllegalArgumentException("No such entity"));

            //get the smallest timestamp that should be the one in the history
            Collections.sort(timestamps);
            assertEquals((long) timestamps.get(0),
                    entity.getEntityHistory().first().getValidity().getStartTimeNanos(),
                    "Incoherent timestamps for " + entityKeys);
        }
    }

    @Test
    public void shouldFindEntitiesByIds() {
        //given
        ImmutableMap<String, Object> partitionKeys = ImmutableMap.of("specification", "devClass1_id_test");
        KeyValues partitionKeyValues = new KeyValues(partitionKeys.get("specification").hashCode(), partitionKeys);
        int size = 2000;
        //has to run this loop in parallel for speed...
        List<Long> entityDataList = IntStream.range(0, size).parallel().mapToObj(i -> {
            ImmutableMap<String, Object> entityKeys = ImmutableMap.of(DEVICE_KEY, "device_id_test" + i);
            KeyValues entityKeyValues = new KeyValues(entityKeys.get(DEVICE_KEY).hashCode(), entityKeys);
            Entity entityData = internalEntityService
                    .findOrCreateEntityFor(SYSTEM.getId(), entityKeyValues, partitionKeyValues, "some-schema", i);
            return entityData.getId();

        }).collect(Collectors.toList());

        //when (must partition by 1000).
        for (List<Long> partition : Lists.partition(entityDataList, 1000)) {
            Set<Entity> allById = internalEntityService.findAll(Entities.suchThat().id().in(partition));
            //then
            assertEquals(partition.size(), allById.size());
        }

    }

    @Test
    public void shouldFindEntitiesByPartition() {
        //given
        KeyValues entityKeyValues = new KeyValues(ENTITY_KEY_VALUES_PARTITION_TEST.get(DEVICE_KEY).hashCode(),
                ENTITY_KEY_VALUES_PARTITION_TEST);
        ImmutableMap<String, Object> partitionKeys = ImmutableMap.of("specification", "devClass1_partition_test");
        KeyValues partitionKeyValues = new KeyValues(partitionKeys.get("specification").hashCode(), partitionKeys);

        Entity entityData = internalEntityService
                .findOrCreateEntityFor(SYSTEM.getId(), entityKeyValues, partitionKeyValues, "some-schema",
                        RECORD_TIMESTAMP);

        assertEquals(1, entityData.getEntityHistory().size());

        long partitionId = entityData.getPartition().getId();

        //when
        Set<Entity> allBySystemIdAndPartitionKeyValuesLike = internalEntityService.findAll(
                Entities.suchThat().systemName().eq(entityData.getSystemSpec().getName()).and().partitionKeyValues()
                        .eq(SYSTEM, entityData.getPartition().getKeyValues()));

        //then
        assertThat(allBySystemIdAndPartitionKeyValuesLike).isNotEmpty();
        allBySystemIdAndPartitionKeyValuesLike.forEach(fetchedEntityData -> {
            assertEquals(partitionId, fetchedEntityData.getPartition().getId());
        });
    }

    @Test
    public void shouldCreateEntityWithoutHistory() {
        // given
        Map<String, Object> entityKey = getRandomEntityKeys();

        // when
        Entity entity = entityService.createEntity(SYSTEM.getId(), entityKey, PARTITION_KEY_VALUES);
        Optional<Entity> optionalEntity = entityService.findById(entity.getId());

        // then
        assertThat(optionalEntity).isPresent();
        Entity foundEntity = optionalEntity.get();
        assertThat(foundEntity).isNotNull();
        assertThat(foundEntity.getEntityHistory()).isEmpty();
        assertThat(foundEntity.getPartition().getKeyValues()).isEqualTo(PARTITION_KEY_VALUES);
    }

    @Test
    public void shouldNotCreateEntityWithoutHistoryTwice() {
        // given
        Map<String, Object> entityKey = getRandomEntityKeys();

        // when
        Entity entity = entityService.createEntity(SYSTEM.getId(), entityKey, PARTITION_KEY_VALUES);
        Optional<Entity> optionalEntity = entityService.findById(entity.getId());

        // then
        assertThat(optionalEntity).isPresent();
        assertThrows(FatalDataConflictRuntimeException.class,
                () -> entityService.createEntity(SYSTEM.getId(), entityKey, PARTITION_KEY_VALUES));
    }

    @Test
    public void shouldNotCreateEntityWithNonExistingSystem() {
        assertThrows(IllegalArgumentException.class,
                () -> entityService.createEntity(-1L, ENTITY_KEY_VALUES, PARTITION_KEY_VALUES));
    }

    @Test
    public void shouldNotCreateEntityWithInvalidEntityKey() {
        assertThrows(IllegalArgumentException.class, () ->
                entityService.createEntity(SYSTEM.getId(), ImmutableMap.of(getRandomString(), getRandomString()),
                        PARTITION_KEY_VALUES));
    }

    @Test
    public void shouldNotCreateEntityWithInvalidPartitionKey() {
        assertThrows(IllegalArgumentException.class, () -> entityService
                .createEntity(SYSTEM.getId(), ENTITY_KEY_VALUES,
                        ImmutableMap.of(getRandomString(), getRandomString())));
    }

    @Test
    public void shouldCreateEntityWithoutHistoryAndAddNewOneOnData() {
        Map<String, Object> entityKey = getRandomEntityKeys();
        Supplier<Entity> entityMaker = () -> entityService
                .createEntity(SYSTEM.getId(), entityKey, PARTITION_KEY_VALUES);
        this.shouldCreateEntityWithoutHistoryAndModifyItBy(entityMaker, entityId -> internalEntityService
                .findOrCreateEntityFor(SYSTEM.getId(), new KeyValues(0, entityKey),
                        new KeyValues(0, PARTITION_KEY_VALUES), SCHEMA, RECORD_TIMESTAMP));
    }

    @Test
    public void shouldCreateEntityWithoutHistoryAndExtendIt() {
        Map<String, Object> entityKey = getRandomEntityKeys();
        Supplier<Entity> entityMaker = () -> entityService
                .createEntity(SYSTEM.getId(), entityKey, PARTITION_KEY_VALUES);
        this.shouldCreateEntityWithoutHistoryAndModifyItBy(entityMaker,
                entityId -> internalEntityService.extendEntityFirstHistoryDataFor(entityId, SCHEMA, RECORD_TIMESTAMP));
    }

    @Test
    public void shouldNotAllowToExceedMaxNumberOfHistoryEntriesPerPeriodByKeyValues() {
        Map<String, Object> entityKey = getRandomEntityKeys();
        assertThrows(IllegalArgumentException.class, () -> {
            for (int i = 0; i < 10000; ++i) {
                //after a max number of new histories (usually we should set this to 100 at most) this should throw an exception.
                internalEntityService
                        .findOrCreateEntityFor(SYSTEM.getId(), new KeyValues(0, entityKey),
                                new KeyValues(0, PARTITION_KEY_VALUES), SCHEMA + i, RECORD_TIMESTAMP + i);
            }
        });
    }

    @Test
    public void shouldNotAllowToExceedMaxNumberOfHistoryEntriesPerPeriodByIds() {
        Map<String, Object> entityKey = getRandomEntityKeys();
        Entity entity = entityService
                .createEntity(SYSTEM.getId(), entityKey, PARTITION_KEY_VALUES);

        assertThrows(IllegalArgumentException.class, () -> {
            for (int i = 0; i < 10000; ++i) {
                //after a max number of new histories (usually we should set this to 100 at most) this should throw an exception.
                internalEntityService
                        .findOrCreateEntityFor(SYSTEM.getId(), entity.getId(), entity.getPartition().getId(),
                                SCHEMA + i, RECORD_TIMESTAMP + i);
            }
        });
    }

    @Test
    public void shouldCreateEntities() {
        // given
        Map<String, Object> entityKey1 = getRandomEntityKeys();
        Map<String, Object> entityKey2 = getRandomEntityKeys();
        CreateEntityRequest request1 = getRequest(entityKey1);
        CreateEntityRequest request2 = getRequest(entityKey2);
        // when
        Set<Entity> entities = entityService.createEntities(Sets.newHashSet(request1, request2));
        Set<Entity> foundEntities = entityService
                .findAll(Entities.suchThat().id().in(entities.stream().map(Entity::getId).collect(Collectors.toSet())));
        // then
        assertThat(foundEntities.size()).isEqualTo(2);
        assertThat(foundEntities).extracting(Entity::getEntityHistory).allMatch(Set::isEmpty);
        assertThat(foundEntities).extracting(Entity::getEntityKeyValues).containsAll(Sets.newHashSet(
                entityKey1, entityKey2));
        assertThat(foundEntities).extracting(entity -> entity.getPartition().getKeyValues())
                .allMatch(kv -> kv.equals(PARTITION_KEY_VALUES));
    }

    private CreateEntityRequest getRequest(Map<String, Object> entityKey1) {
        return CreateEntityRequest.builder().systemId(SYSTEM.getId())
                .entityKeyValues(entityKey1).partitionKeyValues(PARTITION_KEY_VALUES).build();
    }

    @Test
    public void shouldNotCreateEntitiesTwice() {
        // given
        Map<String, Object> entityKey1 = getRandomEntityKeys();
        Map<String, Object> entityKey2 = getRandomEntityKeys();
        CreateEntityRequest request1 = getRequest(entityKey1);
        CreateEntityRequest request2 = getRequest(entityKey2);

        // when
        Entity entity = entityService.createEntity(SYSTEM.getId(), entityKey1, PARTITION_KEY_VALUES);
        Optional<Entity> optionalEntity = entityService.findById(entity.getId());

        // then
        assertThat(optionalEntity).isPresent();
        assertThrows(FatalDataConflictRuntimeException.class, () ->
                entityService.createEntities(Sets.newHashSet(request1, request2)));
    }

    @Test
    public void shouldThrowExceptionOnCreateEntitiesWhenInputIsNull() {
        //when
        assertThrows(NullPointerException.class, () -> entityService.createEntities(null));
    }

    @Test
    public void shouldReturnEmptySetOnCreateEntitiesWhenInputIsEmpty() {
        //when
        Set<Entity> entities = entityService.updateEntities(Collections.emptySet());
        //then
        assertThat(entities).isNotNull();
        assertThat(entities).isEmpty();
    }

    @Test
    public void shouldNotCreateEntitiesWithNonExistingSystem() {
        //given
        Map<String, Object> entityKey1 = getRandomEntityKeys();
        Map<String, Object> entityKey2 = getRandomEntityKeys();
        CreateEntityRequest request1 = CreateEntityRequest.builder().systemId(-1L)
                .entityKeyValues(entityKey1).partitionKeyValues(PARTITION_KEY_VALUES).build();
        CreateEntityRequest request2 = getRequest(entityKey2);
        //when
        assertThrows(IllegalArgumentException.class,
                () -> entityService.createEntities(Sets.newHashSet(request1, request2)));
    }

    @Test
    public void shouldNotCreateEntitiesWithInvalidEntityKey() {
        //given
        Map<String, Object> entityKey1 = getRandomEntityKeys();
        CreateEntityRequest request1 = getRequest(entityKey1);
        CreateEntityRequest request2 = CreateEntityRequest.builder().systemId(SYSTEM.getId())
                .entityKeyValues(ImmutableMap.of(getRandomString(), getRandomString()))
                .partitionKeyValues(PARTITION_KEY_VALUES).build();
        //when
        assertThrows(IllegalArgumentException.class,
                () -> entityService.createEntities(Sets.newHashSet(request1, request2)));
    }

    @Test
    public void shouldNotCreateEntitiesWithInvalidPartitionKey() {
        //given
        Map<String, Object> entityKey1 = getRandomEntityKeys();
        Map<String, Object> entityKey2 = getRandomEntityKeys();
        CreateEntityRequest request1 = getRequest(entityKey1);
        CreateEntityRequest request2 = CreateEntityRequest.builder().systemId(SYSTEM.getId())
                .entityKeyValues(entityKey2).partitionKeyValues(ImmutableMap.of(getRandomString(), getRandomString()))
                .build();
        //when
        assertThrows(IllegalArgumentException.class,
                () -> entityService.createEntities(Sets.newHashSet(request1, request2))
        );
    }

    @Nested
    @DisplayName("Test queries withOptions")
    class QueryOptionsTests {
        @Nested
        public class TestsWithEntityWithMultipleEntityHistoryEntries {
            private final Map<String, Object> kvs = Map.of(DEVICE_KEY,
                    "device-with-more-than-one-history-entry" + UUID.randomUUID());
            private final KeyValues entityKeyValues = new KeyValues(kvs.get(DEVICE_KEY).hashCode(), kvs);
            private final KeyValues partitionKeyValues = new KeyValues(0, PARTITION_KEY_VALUES);
            Entity entityData = internalEntityService
                    .findOrCreateEntityFor(SYSTEM.getId(), entityKeyValues, partitionKeyValues, "brokenSchema1",
                            RECORD_TIMESTAMP);

            ConditionWithOptions<cern.nxcals.api.metadata.queries.Entities, EntityQueryWithOptions> baseQuery = newEntities().id()
                    .eq(entityData.getId());

            @BeforeEach
            public void createHistoryForTests() {
                internalEntityService.extendEntityFirstHistoryDataFor(entityData.getId(), VALID_SCHEMA, 2000);
                internalEntityService.extendEntityFirstHistoryDataFor(entityData.getId(), SCHEMA, 1000);
                internalEntityService.extendEntityFirstHistoryDataFor(entityData.getId(), VALID_SCHEMA, 0);
            }

            @Test
            public void shouldFindEntityWithLatestHistory() {
                // default, should return only latest history entry
                EntityQueryWithOptions defaultQuery = baseQuery.withOptions();
                Entity entity = internalEntityService.findOne(defaultQuery)
                        .orElseThrow(() -> new IllegalArgumentException("No such entity"));
                assertAll(
                        () -> assertEquals(1, entity.getEntityHistory().size()),
                        () -> assertEquals(TimeWindow.after(RECORD_TIMESTAMP),
                                entity.getFirstEntityHistory().getValidity())
                );
            }

            @Test
            public void shouldReturnEntityWithoutHistory() {
                // should return entity without history entries
                EntityQueryWithOptions noHistoryQuery = baseQuery.withOptions().noHistory();
                Entity entity = internalEntityService.findOne(noHistoryQuery)
                        .orElseThrow(() -> new IllegalArgumentException("No such entity"));
                assertEquals(0, entity.getEntityHistory().size());
            }

            @ParameterizedTest
            @CsvSource({ "1500,3000,2", "500,3000,3" })
            public void shouldReturnEntityWithHistoryEntriesMatchingSpecifiedScope(long historyStartTime,
                    long historyEndTime, int expectedHistoryEntries) {
                // should return entity with history entries valid in given range
                EntityQueryWithOptions withPartOfHistoryQuery = baseQuery.withOptions()
                        .withHistory(historyStartTime, historyEndTime);
                Entity entity = internalEntityService.findOne(withPartOfHistoryQuery)
                        .orElseThrow(() -> new IllegalArgumentException("No such entity"));
                assertEquals(expectedHistoryEntries, entity.getEntityHistory().size());
            }
        }

        @Nested
        public class TestsWithMultipleEntities {
            private final Map<String, Object> partitionKeys = Map.of("specification", "devClass1_id_test");
            private final KeyValues partitionKeyValues = new KeyValues(partitionKeys.get("specification").hashCode(),
                    partitionKeys);
            int numberOfEntities = 50;
            //has to run this loop in parallel for speed...
            List<Long> entityIds = IntStream.range(0, numberOfEntities).parallel().mapToObj(i -> {
                Map<String, Object> entityKeys = Map.of(DEVICE_KEY, "device_id_test" + i);
                KeyValues entityKeyValues = new KeyValues(entityKeys.get(DEVICE_KEY).hashCode(), entityKeys);
                Entity entityData = internalEntityService
                        .findOrCreateEntityFor(SYSTEM.getId(), entityKeyValues, partitionKeyValues, "some-schema", i);
                return entityData.getId();
            }).collect(Collectors.toList());

            @Test
            public void shouldReturnLimitedListOfEntities() {
                // given
                int expectedNumberOfEntities = 10;
                EntityQueryWithOptions query = newEntities().id().in(entityIds).withOptions()
                        .limit(expectedNumberOfEntities);

                // when
                List<Entity> entities = internalEntityService.findAll(query);

                // then
                assertEquals(expectedNumberOfEntities, entities.size());
            }

            @Test
            public void shouldReturnOffsetListOfEntities() {
                // given
                int expectedNumberOfEntities = 10;
                EntityQueryWithOptions query = newEntities().id().in(entityIds).withOptions()
                        .offset(numberOfEntities - expectedNumberOfEntities);

                // when
                List<Entity> entities = internalEntityService.findAll(query);

                // then
                assertEquals(expectedNumberOfEntities, entities.size());
            }

            @Test
            public void shouldFindEntitiesAndSortByKeyValueDesc() {
                //given
                EntityQueryWithOptions query = newEntities().id().in(entityIds).withOptions().orderBy()
                        .keyValues().desc();

                // when
                List<Entity> entities = internalEntityService.findAll(query);
                //then
                assertAll(
                        () -> assertEquals(50, entities.size()),
                        () -> assertEquals(Map.of(DEVICE_KEY, "device_id_test9"), entities.get(0).getEntityKeyValues()),
                        () -> assertEquals(Map.of(DEVICE_KEY, "device_id_test8"), entities.get(1).getEntityKeyValues()),
                        () -> assertEquals(Map.of(DEVICE_KEY, "device_id_test7"), entities.get(2).getEntityKeyValues())
                );
            }

            @Test
            public void shouldFindEntitiesAndSortByKeyValueAsc() {
                //given
                EntityQueryWithOptions query = newEntities().id().in(entityIds).withOptions().orderBy()
                        .keyValues().asc();

                // when
                List<Entity> entityServiceAll = internalEntityService.findAll(query);
                //then
                assertAll(
                        () -> assertEquals(50, entityServiceAll.size()),
                        // they are sorted alphabetically, so 40, 41... 49 was before
                        () -> assertEquals(Map.of(DEVICE_KEY, "device_id_test0"),
                                entityServiceAll.get(0).getEntityKeyValues()),
                        () -> assertEquals(Map.of(DEVICE_KEY, "device_id_test1"),
                                entityServiceAll.get(1).getEntityKeyValues()),
                        () -> assertEquals(Map.of(DEVICE_KEY, "device_id_test10"),
                                entityServiceAll.get(2).getEntityKeyValues())
                );
            }
        }
    }

    private void shouldCreateEntityWithoutHistoryAndModifyItBy(Supplier<Entity> entityMaker, LongConsumer extender) {
        Entity entity = entityMaker.get();
        Optional<Entity> optionalEntity = entityService.findById(entity.getId());

        assertThat(optionalEntity).isPresent();
        Entity foundEntity = optionalEntity.get();
        assertThat(foundEntity).isNotNull();
        assertThat(foundEntity.getEntityHistory()).isEmpty();
        assertThat(foundEntity.getPartition().getKeyValues()).isEqualTo(PARTITION_KEY_VALUES);

        extender.accept(foundEntity.getId());
        optionalEntity = entityService.findById(entity.getId());
        assertThat(optionalEntity).isPresent();
        foundEntity = optionalEntity.get();
        assertThat(foundEntity.getEntityHistory()).isNotEmpty();
        EntityHistory firstHistory = foundEntity.getFirstEntityHistory();
        assertThat(firstHistory.getEntitySchema().getSchemaJson()).isEqualTo(SCHEMA);
        assertThat(firstHistory.getValidity().getEndTime()).isNull();
        assertThat(firstHistory.getValidity().getStartTimeNanos()).isEqualTo(RECORD_TIMESTAMP);
    }

    private ImmutableMap<String, Object> getRandomEntityKeys() {
        return ImmutableMap.of(DEVICE_KEY, "device_value" + getRandomString());
    }
}
