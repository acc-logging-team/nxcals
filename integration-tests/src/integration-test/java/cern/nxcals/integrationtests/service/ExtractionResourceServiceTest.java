/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.integrationtests.service;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.KeyValues;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.api.domain.VariableDeclaredType;
import cern.nxcals.api.extraction.metadata.queries.Entities;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import cern.nxcals.common.domain.ColumnMapping;
import cern.nxcals.common.domain.EntityKeyValues;
import cern.nxcals.common.domain.ExtractionCriteria;
import cern.nxcals.common.domain.ExtractionTask;
import cern.nxcals.common.domain.ExtractionUnit;
import cern.nxcals.integrationtests.utils.ServicesHelper;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
public class ExtractionResourceServiceTest extends AbstractTest {
    private static final String NAME_PREFIX = ExtractionResourceServiceTest.class.getName();

    @Test
    public void shouldFindEntitiesResources() {
        Entity entity = createMonitoringEntityWithHistory();

        TimeWindow window = getLastTenDaysWindow();

        ExtractionCriteria criteria = ExtractionCriteria.builder().systemName(MOCK_SYSTEM_NAME)
                .entities(singletonList(
                        EntityKeyValues.builder().fromMap(false, entity.getEntityKeyValues()).build()))
                .timeWindow(window).build();
        ExtractionUnit resourceData = entityResourceService.findBy(criteria);
        assertEquals(1, resourceData.getTasks().size());

        checkEmptyDatasetColumns(resourceData.getEmptyDatasetMapping());
    }

    private Entity createMonitoringEntityWithHistory() {
        KeyValues entityKeyValues = toKeyValues(getRandomEntityKeyValues());
        KeyValues partitionKeyValues = toKeyValues(getRandomPartitionKeyValues());

        return internalEntityService
                .findOrCreateEntityFor(SYSTEM.getId(), entityKeyValues, partitionKeyValues, VALID_SCHEMA,
                        RECORD_TIMESTAMP);
    }

    private void checkEmptyDatasetColumns(List<ColumnMapping> columns) {
        List<String> columnsInEmptyDs = Lists.transform(columns, ColumnMapping::getFieldName);
        assertEquals(new HashSet<>(columnsInEmptyDs).size(), columnsInEmptyDs.size(),
                "Should have no duplicated columns");
        assertTrue(columns.stream().noneMatch(mapping -> mapping.getSchemaJson().isEmpty()),
                "All must have schema");
    }

    private TimeWindow getLastTenDaysWindow() {
        Instant now = Instant.now();
        return TimeWindow.between(now.minus(10, ChronoUnit.DAYS), now);
    }

    @Test
    public void shouldFindVariableResourcesPointingToField() {
        Entity entity = createMonitoringEntityWithHistory();

        Variable variable = ServicesHelper.registerSimpleVariable(getRandomName(NAME_PREFIX), entity, "test_field",
                VariableDeclaredType.NUMERIC);

        TimeWindow window = getLastTenDaysWindow();

        ExtractionCriteria criteria = ExtractionCriteria.builder().systemName(MOCK_SYSTEM_NAME)
                .variableId(variable.getId())
                .timeWindow(window).build();
        ExtractionUnit resourceData = entityResourceService.findBy(criteria);
        assertEquals(1, resourceData.getTasks().size());

        checkEmptyDatasetColumns(resourceData.getEmptyDatasetMapping());
    }

    @Test
    public void shouldFindVariableResourcesPointingToEntity() {
        Entity entity = createMonitoringEntityWithHistory();

        Variable variable = ServicesHelper.registerVariableToEntity(getRandomName(NAME_PREFIX), entity);

        TimeWindow window = getLastTenDaysWindow();

        ExtractionCriteria criteria = ExtractionCriteria.builder().systemName(MOCK_SYSTEM_NAME)
                .variableId(variable.getId())
                .timeWindow(window).build();
        ExtractionUnit resourceData = entityResourceService.findBy(criteria);
        assertEquals(1, resourceData.getTasks().size());

        checkEmptyDatasetColumns(resourceData.getEmptyDatasetMapping());
    }

    @Test
    public void shouldFindEntitiesResourcesCorrectColumnsNumberShouldBePresent() {
        //this is using existing monitoring device
        KeyValues entityKeyValues = toKeyValues(ENTITY_KEY_VALUES_RESOURCES_TEST_WITH_HBASE);

        Entity entityData1 = internalEntityService.findOne(
                        Entities.suchThat().keyValues().eq(SYSTEM, entityKeyValues.getKeyValues()))
                .orElseThrow(() -> new RuntimeException("No such entity " + entityKeyValues.getKeyValues()));

        Instant now = Instant.now();
        TimeWindow window = TimeWindow.between(now.minus(1, ChronoUnit.HOURS), now);
        ExtractionCriteria criteria = ExtractionCriteria.builder().systemName(MOCK_SYSTEM_NAME)
                .entities(singletonList(
                        EntityKeyValues.builder().fromMap(false, entityData1.getEntityKeyValues()).build()))
                .timeWindow(window).build();
        ExtractionUnit resourceData = entityResourceService.findBy(criteria);

        ExtractionTask entityResource = resourceData.getTasks().get(0);

        //Avoids problems with running tests at midnight where two different columns from two schemas are taken.
        assertTrue(entityResource.getColumns().size() >= 33);
    }

    @Test
    public void shouldFindBySystemIdKeyValuesLikeAndTimeWindow() {
        KeyValues entityKeyValues1 = toKeyValues(getRandomEntityKeyValues());
        KeyValues partitionKeyValues = toKeyValues(getRandomPartitionKeyValues());

        internalEntityService
                .findOrCreateEntityFor(SYSTEM.getId(), entityKeyValues1, partitionKeyValues, VALID_SCHEMA,
                        RECORD_TIMESTAMP);

        KeyValues entityKeyValues2 = new KeyValues(ENTITY_KEY_VALUES_WILDCARDS.get(DEVICE_KEY).hashCode(),
                ENTITY_KEY_VALUES_WILDCARDS);

        internalEntityService
                .findOrCreateEntityFor(SYSTEM.getId(), entityKeyValues2, partitionKeyValues, VALID_SCHEMA,
                        RECORD_TIMESTAMP);

        Instant now = Instant.now();
        TimeWindow window = TimeWindow.between(now.minus(10, ChronoUnit.DAYS), now);
        ExtractionCriteria criteria = ExtractionCriteria.builder().systemName(MOCK_SYSTEM_NAME)
                .entities(List.of(EntityKeyValues.builder().fromMap(false, entityKeyValues1.getKeyValues()).build(),
                        EntityKeyValues.builder().fromMap(true, ENTITY_KEY_VALUES_WILDCARDS_SEARCH).build()))
                .timeWindow(window).build();

        ExtractionUnit resources = entityResourceService.findBy(criteria);

        assertFalse(resources.getTasks().isEmpty());
    }

    @Test
    public void shouldAllowFindVariablesBySystemIdAndNameForMoreThan1kVariables() {
        int numOfVariables = 1010;

        Entity entity = ensureEntityExists();

        Set<Variable> variables = ensureVariablesExistForEntity(numOfVariables, entity);
        assertEquals(numOfVariables, variables.size());

        Map<String, Boolean> variablesSearchInput = variables.stream()
                .collect(Collectors.toMap(Variable::getVariableName, v -> false));

        ExtractionCriteria criteria = ExtractionCriteria.builder().systemName(SYSTEM.getName())
                .variables(variablesSearchInput)
                .timeWindow(TimeWindow.between(Instant.EPOCH, Instant.now())).build();

        ExtractionUnit extraction = entityResourceService.findBy(criteria);
        assertFalse(extraction.getTasks().isEmpty());
        assertEquals(numOfVariables, extraction.getCriteria().getVariables().size());
    }

    @AfterAll
    public static void cleanUp() {
        Set<Variable> variablesToRemove = variableService.findAll(
                Variables.suchThat().variableName().like(NAME_PREFIX + "%"));
        variableService.deleteAll(variablesToRemove.stream().map(Variable::getId).collect(Collectors.toSet()));
    }

    private Entity ensureEntityExists() {
        Map<String, Object> partitionKey = getRandomPartitionKeyValues();
        KeyValues partitionKeyValues = new KeyValues(partitionKey.get(PARTITION_KEY).hashCode(), partitionKey);
        Map<String, Object> entityKey = getRandomEntityKeyValues();
        KeyValues entityKeyValues = new KeyValues(entityKey.get(DEVICE_KEY).hashCode(), entityKey);
        Entity entity = internalEntityService.findOrCreateEntityFor(
                SYSTEM.getId(), entityKeyValues, partitionKeyValues, VALID_SCHEMA, RECORD_TIMESTAMP);
        assertNotNull(entity);
        return entity;
    }

    private Set<Variable> ensureVariablesExistForEntity(long numOfVariables, Entity entity) {
        Set<String> expectedVariableNames = new HashSet<>();
        for (int i = 0; i < numOfVariables; i++) {
            expectedVariableNames.add(String.format("TEST:RESOURCE:VARIABLE:%s:%s", entity.getId(), i));
        }
        Set<Variable> variables = new HashSet<>(variableService.findAll(Variables.suchThat().variableName()
                .in(expectedVariableNames)));

        Set<String> existingVariableNames = variables.stream().map(Variable::getVariableName)
                .collect(Collectors.toSet());
        for (String variableName : Sets.difference(expectedVariableNames, existingVariableNames)) {
            variables.add(variableService.create(buildNewVariableFor(variableName, entity.getId())));
        }
        assertEquals(numOfVariables, variables.size());
        return variables;
    }

    public Variable buildNewVariableFor(String variableName, long entityId) {
        VariableConfig config = VariableConfig.builder().entityId(entityId).fieldName("test_field").build();
        return Variable.builder().variableName(variableName).systemSpec(SYSTEM).declaredType(VariableDeclaredType.TEXT)
                .configs(new TreeSet<>(Collections.singleton(config))).build();
    }
}
