/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.integrationtests.service;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.KeyValues;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.api.domain.VariableDeclaredType;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import cern.nxcals.integrationtests.ServiceProvider;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSortedSet;

import java.util.Map;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Those tests need variable that will point to the service location -Dservice.url=xxx.
 */
public class AbstractTest extends ServiceProvider {
    static final String DEVICE_KEY = "device";
    static final String PARTITION_KEY = "specification";

    static final Map<String, Object> ENTITY_KEY_VALUES = ImmutableMap
            .of(DEVICE_KEY, "device_value" + getRandomString());
    static final Map<String, Object> ENTITY_KEY_VALUES_WILDCARDS = ImmutableMap
            .of(DEVICE_KEY, "device_value_wildcards" + getRandomString());
    static final Map<String, Object> ENTITY_KEY_VALUES_WILDCARDS_SEARCH = ImmutableMap
            .of(DEVICE_KEY, "device_value_w%cards" + getRandomString());

    static final Map<String, Object> ENTITY_KEY_VALUES_RESOURCES_TEST_WITH_HBASE = ImmutableMap
            .of(DEVICE_KEY, "NXCALS_MONITORING_DEV0");

    static final Map<String, Object> ENTITY_KEY_VALUES_SCHEMA_TEST = ImmutableMap
            .of(DEVICE_KEY, "device_value_schema_test" + getRandomString());
    static final Map<String, Object> ENTITY_KEY_VALUES_VARIABLE_TEST = ImmutableMap
            .of(DEVICE_KEY, "device_value_variable_test" + getRandomString());
    protected static final Map<String, Object> PARTITION_KEY_VALUES = ImmutableMap
            .of(PARTITION_KEY, "devClass1" + getRandomString());
    static final Map<String, Object> PARTITION_KEY_VALUES_1 = ImmutableMap
            .of(PARTITION_KEY, "devClass2" + getRandomString());

    static final Map<String, Object> PARTITION_KEY_VALUES_2 = ImmutableMap
            .of(PARTITION_KEY, "devClass3" + getRandomString());


    static final Map<String, Object> ENTITY_KEY_VALUES_PARTITION_TEST = ImmutableMap
            .of(DEVICE_KEY, "device_value_partition_test_1" + getRandomString());
    static final Map<String, Object> ENTITY_KEY_VALUES_CHANGELOG_TEST = ImmutableMap
            .of(DEVICE_KEY, "device_value_" + getRandomString());
    static final Map<String, Object> ENTITY_KEY_VALUES_CHANGELOG_TEST_1 = ImmutableMap
            .of(DEVICE_KEY, "device_value_" + getRandomString());
    static final String SCHEMA = "TEST_SCHEMA" + getRandomString();
    static final String VALID_SCHEMA = "{\"type\":\"record\",\"name\":\"data0\",\"namespace\":\"cern.nxcals\",\"fields\":[{\"name\":\"test_field\",\"type\":[\"string\",\"null\"]},{\"name\":\"timestamp\",\"type\":\"long\"},{\"name\":\"device\",\"type\":\"string\"},{\"name\":\"specification\",\"type\":\"string\"}]}";
    static final String MOCK_SYSTEM_NAME = "MOCK-SYSTEM";
    protected static final SystemSpec SYSTEM = systemService.findByName(MOCK_SYSTEM_NAME)
            .orElseThrow(() -> new IllegalArgumentException("No such system"));

    protected static final long RECORD_TIMESTAMP = 1476789831111222334L;
    static final long MOCK_SYSTEM_ID = 0; //This is no longer -100, we should use 0 for MOCK-SYSTEM

    static final String VARIABLE_NAME_PREFIX = "VARIABLE_NAME";
    static final String VARIABLE_NAME_2_PREFIX = "VARIABLE_NAME2";

    static final String VARIABLE_NAME_LIKE_PREFIX = "VAR%E_NAME";

    public static String getRandomString() {
        return UUID.randomUUID().toString();
    }

    public static String getRandomName(String namePrefix) {
        return namePrefix + getRandomString();
    }

    public Variable getVariableData(String varName) {
        return getVariableData(varName, null);
    }

    public static Map<String, Object> getRandomEntityKeyValues() {
        return Map.of(DEVICE_KEY, getRandomName("device_value"));
    }

    public static Map<String, Object> getRandomPartitionKeyValues() {
        return Map.of(PARTITION_KEY, getRandomName("devClass"));
    }

    public static KeyValues toKeyValues(Map<String, Object> map) {
        return new KeyValues(map.hashCode(), map);
    }

    public Variable getVariableData(String varName, VariableDeclaredType type) {
        KeyValues entityKeyValues = new KeyValues(ENTITY_KEY_VALUES_VARIABLE_TEST.get(DEVICE_KEY).hashCode(),
                ENTITY_KEY_VALUES_VARIABLE_TEST);
        KeyValues partitionKeyValues = new KeyValues(0, PARTITION_KEY_VALUES);
        Entity entityData = internalEntityService
                .findOrCreateEntityFor(SYSTEM.getId(), entityKeyValues, partitionKeyValues, "brokenSchema1",
                        RECORD_TIMESTAMP);

        assertNotNull(entityData);

        VariableConfig config = VariableConfig.builder().entityId(entityData.getId()).fieldName("field").build();
        return Variable.builder().variableName(varName).systemSpec(SYSTEM).description("Description").declaredType(type)
                .configs(ImmutableSortedSet.of(config)).build();
    }

    public Variable getVariable(String variableName) {
        return variableService
                .findOne(Variables.suchThat().systemName().eq(SYSTEM.getName()).and().variableName().eq(variableName))
                .orElseThrow(() -> new IllegalStateException("no data"));
    }

    public Variable variable(Entity entity) {
        return variable(entity, "");
    }

    public Variable variable(Entity entity, String variableNamePrefix) {
        Variable variable = Variable.builder()
                .variableName(variableNamePrefix + getRandomString())
                .systemSpec(SYSTEM)
                .description("Description")
                .declaredType(null)
                .configs(ImmutableSortedSet.of(VariableConfig.builder()
                        .entityId(entity.getId())
                        .fieldName("field")
                        .build()))
                .build();
        return variableService.create(variable);
    }

    public Entity entity(int id) {
        return entity(id, "");
    }

    public Entity entity(int id, String entityNamePrefix) {
        return internalEntityService.findOrCreateEntityFor(SYSTEM.getId(),
                new KeyValues(id, ImmutableMap.of("device", entityNamePrefix + getRandomString())),
                new KeyValues(0, PARTITION_KEY_VALUES),
                "brokenSchema1",
                RECORD_TIMESTAMP);
    }
}
