/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.integrationtests.service;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.EntityChangelog;
import cern.nxcals.api.domain.KeyValues;
import cern.nxcals.api.domain.OperationType;
import cern.nxcals.api.extraction.metadata.queries.EntityChangelogs;
import com.google.common.collect.ImmutableSet;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class EntityChangelogServiceTest extends AbstractTest {
    private static Entity entityData;

    @BeforeAll
    public static void createEntity() {
        entityData = createNewEntity();
    }

    private static Entity createNewEntity() {
        KeyValues entityKeyValues = new KeyValues(ENTITY_KEY_VALUES_CHANGELOG_TEST.get("device").hashCode(), ENTITY_KEY_VALUES_CHANGELOG_TEST);
        KeyValues partitionKeyValues = new KeyValues(0, PARTITION_KEY_VALUES);
        Entity entityData = internalEntityService
                .findOrCreateEntityFor(SYSTEM.getId(), entityKeyValues, partitionKeyValues, "brokenSchema1",
                        RECORD_TIMESTAMP);
        return entityData;
    }

    @Test
    public void shouldFindEntityChangelog() {
        assertNotNull(entityData);
        EntityChangelog foundEntityChangelog = getInsertedEntityChangelog(entityData.getId());
        assertEquals(entityData.getId(), foundEntityChangelog.getEntityId());
        assertEquals(entityData.getEntityKeyValues(), foundEntityChangelog.getNewKeyValues());
        assertEquals(SYSTEM.getId(), (long) foundEntityChangelog.getNewSystemId());
        assertNotNull(foundEntityChangelog.getClientInfo());
    }

    @Test
    public void shouldFindChangelogsByTime() {
        assertNotNull(entityData);
        Set<EntityChangelog> foundEntityChangelogs = entityChangelogService
                .findAll(EntityChangelogs.suchThat().creationTimeUtc().notOlderThan(Duration.ofHours(1)));
        assertFalse(foundEntityChangelogs.isEmpty());
        assertTrue(foundEntityChangelogs.stream().anyMatch(c -> c.getNewKeyValues().equals(ENTITY_KEY_VALUES_CHANGELOG_TEST)));
    }

    @Test
    public void shouldFindChangelogsByOperationType() {
        assertNotNull(entityData);
        Set<EntityChangelog> foundEntityChangelogs = entityChangelogService
                .findAll(EntityChangelogs.suchThat().opType().eq(OperationType.INSERT));
        assertFalse(foundEntityChangelogs.isEmpty());
        assertTrue(foundEntityChangelogs.stream().anyMatch(c -> c.getNewKeyValues().equals(ENTITY_KEY_VALUES_CHANGELOG_TEST)));
    }

    private EntityChangelog getInsertedEntityChangelog(Long entityId) {
        return entityChangelogService
                .findOne(EntityChangelogs.suchThat().entityId().eq(entityId).and().opType().eq(OperationType.INSERT))
                .orElseThrow(() -> new IllegalStateException("no data"));
    }

    private EntityChangelog getUpdatedEntityChangelog(Long entityId) {
        return entityChangelogService
                .findOne(EntityChangelogs.suchThat().entityId().eq(entityId).and()
                        .opType().eq(OperationType.UPDATE))
                .orElseThrow(() -> new IllegalStateException("no data"));
    }

    @Test
    public void shouldUpdateEntityAndFindEntityChangelog() {
        assertNotNull(entityData);

        //check for changelog
        EntityChangelog firstChangelog = getInsertedEntityChangelog(entityData.getId());
        assertNotNull(firstChangelog);
        //update Entity
        Entity changedEntity = entityData.toBuilder().entityKeyValues(ENTITY_KEY_VALUES_CHANGELOG_TEST_1).build();

        Set<Entity> updatedEntities = entityService.updateEntities(ImmutableSet.of(changedEntity));
        Entity updatedEntity = updatedEntities.iterator().next();
        EntityChangelog secondEntityChangelog = getUpdatedEntityChangelog(updatedEntity.getId());

        assertEquals(ENTITY_KEY_VALUES_CHANGELOG_TEST_1, secondEntityChangelog.getNewKeyValues());
        assertEquals(entityData.getEntityKeyValues(), secondEntityChangelog.getOldKeyValues());
        assertNotNull(secondEntityChangelog.getClientInfo());
    }
}