package cern.nxcals.integrationtests.service;

import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.extraction.metadata.queries.SystemSpecs;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

/**
 * Created by jwozniak on 02/07/17.
 */
public class SystemSpecServiceTest extends AbstractTest {

    @Test
    public void shouldFindSystemById() {
        SystemSpec systemData = internalSystemService.findOne(SystemSpecs.suchThat().id().eq(MOCK_SYSTEM_ID)).orElseThrow(() -> new IllegalArgumentException("No such system id"));

        assertNotNull(systemData);
        assertEquals(MOCK_SYSTEM_ID, systemData.getId());
    }

    @Test
    public void shouldFindSystemByName() {
        SystemSpec systemData = systemService.findByName(MOCK_SYSTEM_NAME).orElseThrow(() -> new IllegalArgumentException("No such system name"));
        assertNotNull(systemData);
        assertEquals(MOCK_SYSTEM_NAME, systemData.getName());
    }

    @Test
    public void shouldNotFindSystemById() {
        SystemSpec systemData = internalSystemService.findOne(SystemSpecs.suchThat().id().eq(MOCK_SYSTEM_ID - 10000)).orElse(null);
        assertNull(systemData);
    }

    @Test
    public void shouldNotFindSystemByName() {
        SystemSpec systemData = systemService.findByName(MOCK_SYSTEM_NAME + System.currentTimeMillis()).orElse(null);
        assertNull(systemData);
    }

    @Test
    public void shouldFindAll() {
        Set<SystemSpec> systems = systemService.findAll();
        assertNotNull(systems);
        assertFalse(systems.isEmpty());
    }
}
