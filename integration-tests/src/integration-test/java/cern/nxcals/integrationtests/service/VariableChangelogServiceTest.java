/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.integrationtests.service;

import cern.nxcals.api.domain.OperationType;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableChangelog;
import cern.nxcals.api.extraction.metadata.queries.VariableChangelogs;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class VariableChangelogServiceTest extends AbstractTest {

    @Test
    public void shouldCreateVariableAndFindVariableChangelog() {
        //lets first find some existing entity for which we will create variable
        String variableName = getRandomName(VARIABLE_NAME_PREFIX);
        Variable variableData = this.getVariableData(variableName);

        Variable variable = variableService.create(variableData);

        assertNotNull(variable);

        VariableChangelog foundVariableChangelog = getVariableChangelog(variableName);
        assertNotNull(foundVariableChangelog);
        assertEquals(variableName, foundVariableChangelog.getNewVariableName());
        assertEquals(variable.getId(), foundVariableChangelog.getVariableId());
        assertEquals(variable.getDescription(), foundVariableChangelog.getNewDescription());
        assertEquals(variable.getUnit(), foundVariableChangelog.getNewUnit());
        assertNotNull(foundVariableChangelog.getClientInfo());
    }

    @Test
    public void shouldFindChangelogsByTime() {
        String variableName = getRandomName(VARIABLE_NAME_PREFIX);
        Variable variableData = this.getVariableData(variableName);

        Variable variable = variableService.create(variableData);

        assertNotNull(variable);

        Set<VariableChangelog> foundVariableChangelogs = variableChangelogService
                .findAll(VariableChangelogs.suchThat().creationTimeUtc().notOlderThan(Duration.ofHours(1)));
        assertFalse(foundVariableChangelogs.isEmpty());
        assertTrue(foundVariableChangelogs.stream().filter(c -> Objects.nonNull(c.getNewVariableName()))
                .anyMatch(c -> c.getNewVariableName().equals(variableName)));

    }

    @Test
    public void shouldFindChangelogsByOperationType() {
        String variableName = getRandomName(VARIABLE_NAME_PREFIX);
        Variable variableData = this.getVariableData(variableName);

        Variable variable = variableService.create(variableData);

        assertNotNull(variable);

        Set<VariableChangelog> foundVariableChangelogs = variableChangelogService
                .findAll(VariableChangelogs.suchThat().opType().eq(OperationType.INSERT));
        assertFalse(foundVariableChangelogs.isEmpty());
        assertTrue(foundVariableChangelogs.stream().anyMatch(c -> c.getNewVariableName().equals(variableName)));
    }

    @Test
    public void shouldNotFindVariableChangelog() {
        String nonExistentVariable = "NoSuchVariable" + System.currentTimeMillis();
        Optional<VariableChangelog> foundVariableChangelog = variableChangelogService.findOne(
                VariableChangelogs.suchThat().newVariableName().eq(nonExistentVariable));
        assertNotNull(foundVariableChangelog);
        assertFalse(foundVariableChangelog.isPresent());
    }

    @Test
    public void shouldUpdateVariableAndFindVariableChangelog() {
        String testVariableName = getRandomName(VARIABLE_NAME_PREFIX);
        Variable variableData = this.getVariableData(testVariableName);

        //register
        Variable newVariable = variableService.create(variableData);
        assertNotEquals(0, newVariable.getId());

        //check for changelog
        VariableChangelog firstChangelog = getVariableChangelog(testVariableName);
        assertNotNull(firstChangelog);

        //update Variable
        String newDescription = "Oops! I forgot to initially update description! Let's do it now!";

        Variable changedVariable = newVariable.toBuilder().description(newDescription).unit("newUnit").build();

        Variable updatedVariable = variableService.update(changedVariable);

        VariableChangelog secondVariableChangelog = getUpdatedVariableChangelog(updatedVariable.getId());

        assertEquals(newDescription, secondVariableChangelog.getNewDescription());
        assertEquals(variableData.getDescription(), secondVariableChangelog.getOldDescription());
        assertEquals(variableData.getUnit(), secondVariableChangelog.getOldUnit());
        assertEquals(updatedVariable.getUnit(), secondVariableChangelog.getNewUnit());

    }

    private VariableChangelog getVariableChangelog(String variableName) {
        return variableChangelogService
                .findOne(VariableChangelogs.suchThat().newSystemId().eq(SYSTEM.getId()).and().newVariableName()
                        .eq(variableName))
                .orElseThrow(() -> new IllegalStateException("no data"));
    }

    private VariableChangelog getUpdatedVariableChangelog(Long variableId) {
        return variableChangelogService
                .findOne(
                        VariableChangelogs.suchThat().newSystemId().eq(SYSTEM.getId()).and().variableId().eq(variableId)
                                .and().opType().eq(OperationType.UPDATE))
                .orElseThrow(() -> new IllegalStateException("no data"));
    }

}
