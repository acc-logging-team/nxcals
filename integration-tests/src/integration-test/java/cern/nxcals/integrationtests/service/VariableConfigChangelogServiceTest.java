/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.integrationtests.service;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.KeyValues;
import cern.nxcals.api.domain.OperationType;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.api.domain.VariableConfigChangelog;
import cern.nxcals.api.extraction.metadata.queries.VariableConfigChangelogs;
import com.google.common.collect.ImmutableSortedSet;
import lombok.val;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class VariableConfigChangelogServiceTest extends AbstractTest {

    @Test
    public void shouldCreateVariableConfigsAndFindVariableConfigChangelog() {
        String variableName = getRandomName(VARIABLE_NAME_PREFIX);
        Variable variableData = this.getVariableData(variableName);
        Variable variable = variableService.create(variableData);

        SortedSet<VariableConfig> configData = variable.getConfigs();
        VariableConfig existingConfig = configData.first();

        VariableConfigChangelog foundChangelog = getVariableConfigChangelog(existingConfig.getId());

        assertEquals(existingConfig.getFieldName(), foundChangelog.getNewFieldName());
        assertEquals(variable.getId(), foundChangelog.getNewVariableId().longValue());
        assertEquals(existingConfig.getEntityId(), foundChangelog.getNewEntityId().longValue());
        assertEquals(existingConfig.getFieldName(), foundChangelog.getNewFieldName());
        assertNotNull(foundChangelog.getClientInfo());
    }

    @Test
    public void shouldFindVariableConfigChangelogsByTime() {
        String variableName = getRandomName(VARIABLE_NAME_PREFIX);
        Variable variableData = this.getVariableData(variableName);

        Variable variable = variableService.create(variableData);

        assertNotNull(variable);

        Set<VariableConfigChangelog> foundVariableConfigChangelogs = variableConfigChangelogService
                .findAll(VariableConfigChangelogs.suchThat().creationTimeUtc().notOlderThan(Duration.ofHours(1)));
        assertFalse(foundVariableConfigChangelogs.isEmpty());
        assertTrue(foundVariableConfigChangelogs.stream().anyMatch(c -> c.getVariableConfigId() ==
                variable.getConfigs().first().getId()));

    }

    @Test
    public void shouldFindVariableConfigChangelogsByOperationType() {
        String variableName = getRandomName(VARIABLE_NAME_PREFIX);
        Variable variableData = this.getVariableData(variableName);

        Variable variable = variableService.create(variableData);

        assertNotNull(variable);

        Set<VariableConfigChangelog> foundVariableConfigChangelogs = variableConfigChangelogService
                .findAll(VariableConfigChangelogs.suchThat().opType().eq(OperationType.INSERT));
        assertFalse(foundVariableConfigChangelogs.isEmpty());
        assertTrue(foundVariableConfigChangelogs.stream().anyMatch(c -> c.getNewVariableId().equals(variable.getId())));
    }

    @Test
    public void shouldNotFindVariableConfigChangelog() {
        Optional<VariableConfigChangelog> variableConfigChangelog = variableConfigChangelogService.findOne(
                VariableConfigChangelogs.suchThat().newFieldName().eq("non_existing_field"));
        assertNotNull(variableConfigChangelog);
        assertFalse(variableConfigChangelog.isPresent());
    }

    @Test
    public void shouldUpdateVariableAndFindVariableConfigChangelog() {
        String testVariableName = getRandomName(VARIABLE_NAME_PREFIX);
        Variable variableData = this.getVariableData(testVariableName);

        //register
        Variable newVariable = variableService.create(variableData);
        assertNotEquals(0, newVariable.getId());

        //check for changelog
        VariableConfigChangelog variableConfigChangelog = getVariableConfigChangelog(newVariable.getConfigs()
                .first().getId());
        assertNotNull(variableConfigChangelog);

        //update Variable config
        VariableConfig updatedConfig = newVariable.getConfigs().last().toBuilder().fieldName("updatedField").build();

        Variable changedVariable = newVariable.toBuilder().configs(new TreeSet<>(Collections.singleton(updatedConfig)))
                .build();

        Variable updatedVariable = variableService.update(changedVariable);

        VariableConfigChangelog updatedVariableConfigChangelog = getUpdatedVariableConfigChangelog(
                updatedConfig.getId());

        assertEquals(updatedConfig.getEntityId(), updatedVariableConfigChangelog.getNewEntityId().longValue());
        assertEquals(updatedConfig.getFieldName(), updatedVariableConfigChangelog.getNewFieldName());
        assertEquals(updatedVariable.getId(), updatedVariableConfigChangelog.getNewVariableId().longValue());
    }

    @Test
    public void shouldUpdateVariableConfigAndFindVariableConfigChangelogs() {
        String testVariableName = getRandomName(VARIABLE_NAME_PREFIX);
        Variable variableData = this.getVariableData(testVariableName);

        //register
        Variable newVariable = variableService.create(variableData);
        assertNotEquals(0, newVariable.getId());

        Variable variableWithNewConfigs = newVariable.toBuilder().configs(getSampleVariableConfigs()).build();
        Variable updatedVariable = variableService.update(variableWithNewConfigs);

        val foundVariableConfigChangelogs = variableConfigChangelogService
                .findAll(VariableConfigChangelogs.suchThat().newVariableId().eq(updatedVariable.getId()));

        for (VariableConfig config : updatedVariable.getConfigs()) {
            assertTrue(foundVariableConfigChangelogs.stream()
                    .anyMatch(foundConfig -> foundConfig.getNewFieldName().equals(config.getFieldName()) && foundConfig
                            .getNewVariableId().equals(updatedVariable.getId())));
        }

    }

    @Test
    public void shouldFindVariableConfigChangelogsWithMorethan1kEntities() {
        String testVariableName = getRandomName(VARIABLE_NAME_PREFIX);
        Variable variableData = this.getVariableData(testVariableName);

        //register
        Variable newVariable = variableService.create(variableData);
        assertNotEquals(0, newVariable.getId());

        List<Long> entityIds = new ArrayList<>();
        for (long i = 0; i < 1200; i++) {
            entityIds.add(i);
        }
        assertTrue(entityIds.size() > 1000);
        val foundVariableConfigChangelogs = variableConfigChangelogService.findAll(
                VariableConfigChangelogs.suchThat().newEntityId().in(entityIds));
        assertNotNull(foundVariableConfigChangelogs);
    }

    private VariableConfigChangelog getVariableConfigChangelog(long variableConfigId) {
        return variableConfigChangelogService
                .findOne(VariableConfigChangelogs.suchThat().variableConfigId().eq(variableConfigId))
                .orElseThrow(() -> new IllegalStateException("no data"));
    }

    private VariableConfigChangelog getUpdatedVariableConfigChangelog(Long variableConfigId) {
        return variableConfigChangelogService.findOne(VariableConfigChangelogs.suchThat()
                .variableConfigId().eq(variableConfigId).and().opType().eq(OperationType.UPDATE))
                .orElseThrow(() -> new IllegalStateException("no data"));
    }

    private SortedSet<VariableConfig> getSampleVariableConfigs() {
        KeyValues entityKeyValues = new KeyValues(ENTITY_KEY_VALUES_VARIABLE_TEST.get("device").hashCode(),
                ENTITY_KEY_VALUES_VARIABLE_TEST);
        KeyValues partitionKeyValues = new KeyValues(0, PARTITION_KEY_VALUES);
        Entity entityData = internalEntityService
                .findOrCreateEntityFor(SYSTEM.getId(), entityKeyValues, partitionKeyValues, "brokenSchema1",
                        RECORD_TIMESTAMP);

        VariableConfig firstConfig = VariableConfig.builder().entityId(entityData.getId()).fieldName("field1").build();
        VariableConfig secondConfig = VariableConfig.builder().entityId(entityData.getId()).fieldName("field2").build();

        return ImmutableSortedSet.of(firstConfig, secondConfig);
    }

}
