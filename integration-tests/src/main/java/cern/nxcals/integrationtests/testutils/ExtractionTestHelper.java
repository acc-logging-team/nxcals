package cern.nxcals.integrationtests.testutils;

import cern.cmw.datax.ImmutableData;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.api.domain.VariableDeclaredType;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.extraction.metadata.EntityService;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.extraction.metadata.queries.Entities;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import cern.nxcals.api.ingestion.Publisher;
import cern.nxcals.api.ingestion.PublisherFactory;
import cern.nxcals.common.SystemFields;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;

import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

@Slf4j
public abstract class ExtractionTestHelper {
    public static final String SYSTEM = "TEST-CMW";
    public static final String DEVICE_KEY = "device";
    public static final String PROPERTY_KEY = "property";
    public static final String CLASS_KEY = "class";
    public static final String RECORD_VERSION_KEY = "__record_version__";
    public static final String RECORD_TIMESTAMP_KEY = "__record_timestamp__";
    public static final String SYSTEM_2 = "MOCK-SYSTEM";
    public static final String SYSTEM_2_ENTITY_KEY = "device";
    public static final String SYSTEM_2_PARTITION_KEY = "specification";
    public static final String SYSTEM_2_TIME_KEY = "timestamp";

    public static Variable registerVariableToOneFieldWithOneConfig(Map<String, Object> keyValues, String fieldName,
            String variableName, EntityService entityService, VariableService variableService, SystemSpec systemSpec) {
        Entity entity = findEntityOrThrow(keyValues, entityService, systemSpec);
        VariableConfig varConfData = VariableConfig.builder().entityId(entity.getId()).fieldName(fieldName).build();
        SortedSet<VariableConfig> varConfSet = ImmutableSortedSet.of(varConfData);

        return registerVariable(variableName, varConfSet, variableService, systemSpec);
    }

    public static Variable registerVariableToWholeEntityWithOneConfig(Map<String, Object> keyValues,
            String variableName,
            EntityService entityService, VariableService variableService, SystemSpec systemSpec) {
        Entity entity = findEntityOrThrow(keyValues, entityService, systemSpec);
        VariableConfig varConfData = VariableConfig.builder().entityId(entity.getId()).build();
        SortedSet<VariableConfig> varConfSet = ImmutableSortedSet.of(varConfData);

        return registerVariable(variableName, varConfSet, variableService, systemSpec);
    }

    public static Entity findEntityOrThrow(Map<String, Object> keyValues, EntityService entityService,
            SystemSpec systemSpec) {
        return entityService.findOne(
                        Entities.suchThat().systemName().eq(systemSpec.getName()).and().keyValues().eq(systemSpec, keyValues))
                .orElseThrow(() -> new IllegalArgumentException("No such entity"));
    }

    public static Variable findVariableOrThrow(String variableName, VariableService variableService,
            String systemName) {
        return variableService.findOne(
                        Variables.suchThat().systemName().eq(systemName).and().variableName().eq(variableName))
                .orElseThrow(
                        () -> new IllegalArgumentException(
                                String.format("Variable [%s] was not found on system [%s]", variableName,
                                        systemName)));
    }

    public static Variable registerVariable(String variableName, SortedSet<VariableConfig> varConfSet,
            VariableService variableService, SystemSpec systemSpec) {
        variableService.findOne(Variables.suchThat().variableName().eq(variableName)).ifPresent(v -> {
            throw new IllegalArgumentException("Variable with this name already exists");
        });
        Variable variable = Variable.builder().variableName(variableName).systemSpec(systemSpec)
                .description("Description").declaredType(VariableDeclaredType.NUMERIC).configs(varConfSet).build();
        return variableService.create(variable);
    }

    public static long getCount(String device, String property, SparkSession sparkSession, Instant from) {
        return DataQuery.builder(sparkSession).byEntities().system(SYSTEM).startTime(from).endTime(Instant.now())
                .entity().keyValueLike(DEVICE_KEY, device).keyValue(PROPERTY_KEY, property).build().count();
    }

    public static List<Object> getFieldData(Dataset<Row> dataset, String deviceName, String propertyName,
            String fieldName) {
        return dataset.filter(functions.col(fieldName).isNotNull())
                .filter(functions.col(DEVICE_KEY).equalTo(deviceName))
                .filter(functions.col(PROPERTY_KEY).equalTo(propertyName)).sort(RECORD_TIMESTAMP_KEY).select(fieldName)
                .collectAsList().stream().map(r -> r.get(0)).collect(Collectors.toList());
    }

    public static List<Object> getVariableData(Dataset<Row> dataset, String variableName) {
        return dataset.filter(SystemFields.NXC_EXTR_VARIABLE_NAME.getValue() + " = '" + variableName + "'")
                .sort(SystemFields.NXC_EXTR_TIMESTAMP.getValue()).select(SystemFields.NXC_EXTR_VALUE.getValue())
                .collectAsList().stream()
                .map(r -> r.get(0)).collect(Collectors.toList());
    }

    public static void publishData(String systemName, List<ImmutableData> dataToPublish) {
        PublisherFactory publisherFactory = PublisherFactory.newInstance();
        try (Publisher<ImmutableData> publisher = publisherFactory
                .createPublisher(systemName, Function.identity())) {
            for (ImmutableData data : dataToPublish) {
                publisher.publish(data);
            }
        } catch (Exception e) {
            log.error("Error sending data to NXCALS", e);
            fail("Error sending data to NXCALS! Cause:\n" + e.getCause());
        }
    }

    public static Set<String> getSystemFieldsFor(String systemName) {
        switch (systemName) {
        case SYSTEM:
            return ImmutableSet.of(DEVICE_KEY, PROPERTY_KEY, CLASS_KEY, RECORD_VERSION_KEY, RECORD_TIMESTAMP_KEY);
        case SYSTEM_2:
            return ImmutableSet.of(SYSTEM_2_ENTITY_KEY, SYSTEM_2_PARTITION_KEY, SYSTEM_2_TIME_KEY);
        default:
            throw new IllegalArgumentException("System fields not defined here for " + systemName);
        }
    }

    public static Set<String> getDefaultFieldsForVariableQuery() {
        return Sets.newHashSet(SystemFields.NXC_EXTR_VALUE.getValue(), SystemFields.NXC_EXTR_ENTITY_ID.getValue(),
                SystemFields.NXC_EXTR_VARIABLE_NAME.getValue(), SystemFields.NXC_EXTR_TIMESTAMP.getValue());
    }

    public static Set<String> getFieldsForEntityQueryIn(String system) {
        Set<String> fields = new HashSet<>(getSystemFieldsFor(system));
        fields.add(SystemFields.NXC_EXTR_ENTITY_ID.getValue());
        return fields;
    }

    public static Set<String> getDefaultFieldsForCMWEntityQueryWith(String... fields) {
        Set<String> systemFields = getFieldsForEntityQueryIn(SYSTEM);
        systemFields.addAll(asList(fields));
        return systemFields;
    }

    public static void checkColumns(Set<String> expectedColumns, Dataset<Row> dataset) {
        Set<String> datasetColumns = Sets.newHashSet(dataset.columns());
        assertEquals(expectedColumns, datasetColumns);
    }

    public static void checkColumnsInVariableQuery(Dataset<Row> dataset) {
        //  [nxcals_value, nxcals_entity_id, nxcals_timestamp, nxcals_variable_name]
        Set<String> expectedColumns = getDefaultFieldsForVariableQuery();
        checkColumns(expectedColumns, dataset);
    }
}
