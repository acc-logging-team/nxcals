package cern.nxcals.integrationtests.prometheus.config;

import cern.nxcals.integrationtests.prometheus.prometheus.Prometheus;
import feign.Feign;
import feign.gson.GsonDecoder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by jwozniak on 26/03/17.
 */
@Configuration
public class PrometheusConfig {

    @Value("${prometheus.address}")
    private String prometheusAddress;

    @Bean
    public Prometheus createPrometheus() {

        return Feign.builder()
                .decoder(new GsonDecoder())
                .target(Prometheus.class, prometheusAddress);
    }
}
