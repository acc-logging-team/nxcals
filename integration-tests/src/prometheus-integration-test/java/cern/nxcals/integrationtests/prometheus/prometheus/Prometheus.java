package cern.nxcals.integrationtests.prometheus.prometheus;

import com.google.gson.JsonObject;
import feign.Param;
import feign.RequestLine;

/**
 * Created by jwozniak on 25/03/17.
 */
public interface Prometheus {
    @RequestLine("GET /api/v1/query?query=ALERTS{alertstate=\"firing\"}")
    JsonObject alertsFiring();

    @RequestLine("GET /api/v1/query?query={query}")
    JsonObject metrics(@Param("query") String query);
}

