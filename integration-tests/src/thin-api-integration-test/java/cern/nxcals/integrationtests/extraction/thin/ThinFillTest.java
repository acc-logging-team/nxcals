package cern.nxcals.integrationtests.extraction.thin;

import cern.cmw.datax.DataBuilder;
import cern.cmw.datax.ImmutableData;
import cern.nxcals.api.custom.converters.FillConverter;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.api.domain.VariableDeclaredType;
import cern.nxcals.api.extraction.metadata.EntityService;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.extraction.metadata.queries.Entities;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import cern.nxcals.api.extraction.thin.AvroQuery;
import cern.nxcals.api.extraction.thin.ExtractionServiceGrpc;
import cern.nxcals.api.extraction.thin.FillData;
import cern.nxcals.api.extraction.thin.FillQueryByNumber;
import cern.nxcals.api.extraction.thin.FillQueryByWindow;
import cern.nxcals.api.extraction.thin.FillServiceGrpc;
import cern.nxcals.api.extraction.thin.ServiceFactory;
import cern.nxcals.api.extraction.thin.TimeWindow;
import cern.nxcals.api.extraction.thin.data.builders.DevicePropertyDataQuery;
import cern.nxcals.api.ingestion.Publisher;
import cern.nxcals.api.ingestion.PublisherFactory;
import cern.nxcals.api.utils.TimeUtils;
import cern.rbac.client.authentication.AuthenticationClient;
import cern.rbac.client.authentication.AuthenticationException;
import cern.rbac.common.RbaToken;
import cern.rbac.util.holder.ClientTierTokenHolder;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSortedSet;
import com.google.protobuf.Empty;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.SortedSet;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

@SpringBootTest(classes = { cern.nxcals.integrationtests.extraction.thin.ThinExtractionTest.class })
@ExtendWith(SpringExtension.class)
@Slf4j
public class ThinFillTest {

    //uncomment for IDE testing...
    static {
        System.setProperty("spark.app.name", "IntTests");
        System.setProperty("kafka.producer.linger.ms", "0");

        //        Please leave this one just in case you want to run those test from IDE for debugging
        //        Don't forget to check if hadoop-dev jar is included in build.gradle!!!

        //        String username = System.getProperty("user.name");
        //
        //        System.setProperty("spark.servers.url",
        //                "nxcals-USER-1.cern.ch:15000,nxcals-USER-2.cern.ch:15000,nxcals-USER-3.cern.ch:15000,nxcals-USER-4.cern.ch:15000,nxcals-USER-5.cern.ch:15000".replace(
        //                        "USER", username));
        //        System.setProperty("kafka.producer.bootstrap.servers",
        //                "https://nxcals-<user>-3.cern.ch:9092,https://nxcals-<user>-4.cern.ch:9092,https://nxcals-<user>-5.cern.ch:9092"
        //                        .replace("<user>", username));
        //        System.setProperty("service.url",
        //                "https://nxcals-<user>-1.cern.ch:19093"
        //                        .replace("<user>", username));
        //
        //        System.setProperty("kerberos.principal", username);
        //        System.setProperty("kerberos.keytab", "/opt/<user>/.keytab".replace("<user>", username));
    }

    private static final String USER = "acclog";
    @Value("${acclog.password}")
    private String password;

    private FillServiceGrpc.FillServiceBlockingStub fillService = ServiceFactory
            .createFillService(System.getProperty("spark.servers.url"));

    private static void login(String password) {
        try {
            AuthenticationClient authenticationClient = AuthenticationClient.create();
            //Don't commit your password here!!!
            RbaToken token = authenticationClient.loginExplicit(ThinFillTest.USER, password);
            ClientTierTokenHolder.setRbaToken(token);
        } catch (AuthenticationException e) {
            throw new IllegalArgumentException("Cannot login", e);
        }
    }

    private static final SystemSpecService systemService = ServiceClientFactory.createSystemSpecService();
    private static final EntityService entityService = ServiceClientFactory.createEntityService();
    private static final VariableService variableService = ServiceClientFactory.createVariableService();
    private static final ExtractionServiceGrpc.ExtractionServiceBlockingStub extractionService = ServiceFactory
            .createExtractionService(System.getProperty("spark.servers.url"));

    private static final int FIRST_FILL = 100;
    private static final int FILLS_NUMBER = 3;
    private static final int BEAMS_PER_FILL_NUMBER = 5;
    private static final int MAX_WAIT_TIME = 4;
    // we round to seconds and adding 10 millisecond in order to
    // verify that instant-string-instant conversion works with milliseconds < 100 (NXCALS-6010)
    private static final long FROM_TIME =
            Instant.now().truncatedTo(ChronoUnit.SECONDS).plusMillis(10).toEpochMilli() * 1000_000;

    private static final String RANDOM_SUFFIX = UUID.randomUUID().toString();

    private static final String DEVICE1 = "test_device_1_" + RANDOM_SUFFIX;
    private static final String PROPERTY1 = "test_property_1_" + RANDOM_SUFFIX;

    private static final String DEVICE2 = "test_device_2_" + RANDOM_SUFFIX;
    private static final String PROPERTY2 = "test_property_2_" + RANDOM_SUFFIX;

    private static final String CLASS = "test_class";
    private static final String RECORD_VERSION_KEY = "__record_version__";
    private static final String RECORD_TIMESTAMP_KEY = "__record_timestamp__";

    private static final String DEVICE_KEY = "device";
    private static final String PROPERTY_KEY = "property";
    private static final String CLASS_KEY = "class";

    private static final String FIELD1 = "FILL_NB";
    private static final String VARIABLE1 = "HX:FILLN";
    private static final String FIELD2 = "MACHINE_STATE";
    private static final String VARIABLE2 = "HX:BMODE";

    private static String CMW_SYSTEM = "CMW";
    private static String DEVICE_KEY_NAME = "device";
    private static String PROPERTY_KEY_NAME = "property";

    private static final SystemSpec CMW_SYSTEM_SPEC = systemService.findByName(CMW_SYSTEM)
            .orElseThrow(() -> new IllegalArgumentException("No such system"));

    private static volatile boolean dataVisible = false;
    private boolean waitingForDataFailed = false;

    @BeforeAll
    public static void setUpData() {

        Publisher<ImmutableData> publisher = PublisherFactory.newInstance()
                .createPublisher(CMW_SYSTEM, Function.identity());
        log.info("Sending data to NXCALS");
        try {
            for (int i = 0; i < FILLS_NUMBER + 1; ++i) {
                long timestamp = TimeUtils.getNanosFromInstant(
                        TimeUtils.getInstantFromNanos(FROM_TIME).plus(i * 10, ChronoUnit.MILLIS));
                publisher.publish(createFillData(FIRST_FILL + i, timestamp));

                for (int k = 0; k < BEAMS_PER_FILL_NUMBER; ++k) {
                    long beamModeTimestamp = TimeUtils.getNanosFromInstant(
                            TimeUtils.getInstantFromNanos(FROM_TIME).plus(i * 10 + k, ChronoUnit.MILLIS));
                    publisher.publish(createBeamModeData("MODE_" + String.valueOf(k), beamModeTimestamp));
                }
            }
        } catch (Exception e) {
            log.error("Error sending values to NXCALS", e);
            fail("Error sending values to NXCALS");
        }
        registerVariableWithConfig(DEVICE1, PROPERTY1, FIELD1, VARIABLE1);
        registerVariableWithConfig(DEVICE2, PROPERTY2, FIELD2, VARIABLE2);
    }

    private static ImmutableData createFillData(int fillNumber, long timestamp) {
        DataBuilder builder = ImmutableData.builder();
        builder.add(DEVICE_KEY, DEVICE1);
        builder.add(PROPERTY_KEY, PROPERTY1);
        builder.add(CLASS_KEY, CLASS);
        builder.add(RECORD_VERSION_KEY, 0L);
        builder.add(RECORD_TIMESTAMP_KEY, timestamp);
        builder.add(FIELD1, fillNumber);
        return builder.build();
    }

    private static ImmutableData createBeamModeData(String machineState, long timestamp) {
        DataBuilder builder = ImmutableData.builder();
        builder.add(DEVICE_KEY, DEVICE2);
        builder.add(PROPERTY_KEY, PROPERTY2);
        builder.add(CLASS_KEY, CLASS);
        builder.add(RECORD_VERSION_KEY, 0L);
        builder.add(RECORD_TIMESTAMP_KEY, timestamp);
        builder.add(FIELD2, machineState);
        return builder.build();
    }

    @BeforeEach
    public void waitForData() throws InterruptedException {
        login(this.password);
        if (!dataVisible) {
            //Have to wait 30 seconds in order for the data to be processed and visible in NXCALS extraction
            log.info("Waiting max " + MAX_WAIT_TIME + " minutes for the data to be visible in NXCALS...");
            waitForData(TimeUnit.MINUTES.toMillis(MAX_WAIT_TIME));
            log.info("Ok, data should be processed now, proceeding with tests");
            dataVisible = true;
        } else {
            log.info("Data already checked");
        }
    }

    private synchronized void waitForData(long maxTime) throws InterruptedException {
        long start = System.currentTimeMillis();

        while (true) {
            if (waitingForDataFailed || (System.currentTimeMillis() - start > maxTime)) {
                waitingForDataFailed = true;
                fail("Data was not processed in the required maxTime=" + maxTime + " ms");
            }
            if (isDataPresent()) {
                return;
            }

            log.info("Still no data visible, waiting more...");
            TimeUnit.SECONDS.sleep(1);
        }
    }

    private boolean isDataPresent() {
        return getCount(DEVICE1, PROPERTY1) == FILLS_NUMBER + 1
                && getCount(DEVICE2, PROPERTY2) == (FILLS_NUMBER + 1) * BEAMS_PER_FILL_NUMBER;
    }

    private long getCount(String deviceName, String propertyName) {
        Instant startTime = TimeUtils.getInstantFromNanos(FROM_TIME);
        Instant endTime = startTime.plus((FILLS_NUMBER + 1) * 10, ChronoUnit.MILLIS);

        String script = DevicePropertyDataQuery.builder().system(CMW_SYSTEM)
                .startTime(startTime).endTime(endTime).entity()
                .parameter(deviceName + "/" + propertyName).build();

        AvroQuery avroQuery = AvroQuery.newBuilder()
                .setScript(script)
                .setCompression(AvroQuery.Codec.BZIP2) //optional bytes compression
                .build();

        long count = extractionService.query(avroQuery).getRecordCount();
        log.info("Count for sytem {}, {}/{} from {} to {} : {}", CMW_SYSTEM, DEVICE1, PROPERTY1, startTime, endTime,
                count);
        return count;
    }

    private static void registerVariableWithConfig(String deviceName, String propertyName, String fieldName,
            String variableName) {
        Entity entity = findEntityOrThrow(deviceName, propertyName);
        VariableConfig varConfData = VariableConfig.builder().entityId(entity.getId()).fieldName(fieldName).build();
        SortedSet<VariableConfig> varConfSet = ImmutableSortedSet.of(varConfData);

        registerVariable(variableName, varConfSet);
    }

    private static void registerVariable(String variableName, SortedSet<VariableConfig> varConfSet) {

        Optional<Variable> variable = ThinFillTest.variableService.findOne(Variables.suchThat()
                .systemName().eq(CMW_SYSTEM_SPEC.getName()).and().variableName().eq(variableName));

        if (variable.isPresent()) {
            Variable updatedVariable = variable.get().toBuilder().configs(varConfSet).build();
            variableService.update(updatedVariable);
        } else {

            Variable newVariable = Variable.builder().variableName(variableName).systemSpec(CMW_SYSTEM_SPEC)
                    .description("Description").declaredType(VariableDeclaredType.NUMERIC).configs(varConfSet).build();
            variableService.create(newVariable);
        }
    }

    private static Entity findEntityOrThrow(String deviceName, String propertyName) {
        Map<String, Object> entityKeys = ImmutableMap
                .of(DEVICE_KEY_NAME, deviceName, PROPERTY_KEY_NAME, propertyName);

        return ThinFillTest.entityService.findOne(
                        Entities.suchThat().keyValues().eq(CMW_SYSTEM_SPEC, entityKeys))
                .orElseThrow(() -> new IllegalArgumentException("No such entity"));
    }

    @Test
    public void shouldGetFillData() {
        // given
        login(this.password);
        int fillNumber = FIRST_FILL + 2;

        // when
        FillData fillData = fillService.findFill(FillQueryByNumber.newBuilder().setFillNr(fillNumber).build());

        // then
        assertEquals(fillNumber, FillConverter.toFill(fillData).getNumber());
    }

    @Test
    public void shouldGetFillsData() {
        // given
        login(this.password);

        long start = FROM_TIME;
        long stop = TimeUtils.getNanosFromInstant(
                TimeUtils.getInstantFromNanos(FROM_TIME).plus((FILLS_NUMBER - 1) * 10, ChronoUnit.MILLIS));

        // when
        Iterator<FillData> fillsData = fillService.findFills(FillQueryByWindow.newBuilder()
                .setTimeWindow(TimeWindow.newBuilder().setStartTime(start).setEndTime(stop).build()).build());

        // then
        List<FillData> fillsDataList = new ArrayList<>();
        fillsData.forEachRemaining(fillsDataList::add);
        assertTrue(FillConverter.toFillList(fillsDataList).size() > 0);
    }

    @Test
    public void shouldGetLastCompletedData() {
        // given
        login(this.password);

        // when
        FillData fillData = fillService.getLastCompleted(Empty.newBuilder().build());

        // then
        assertTrue(fillData.getBeamModesList().size() > 0);
    }
}
