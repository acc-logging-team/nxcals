package cern.nxcals.integrationtests.extraction.thin;

import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.extraction.data.Avro;
import cern.nxcals.api.extraction.data.Datax;
import cern.nxcals.api.extraction.metadata.EntityService;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.extraction.thin.AvroData;
import cern.nxcals.api.extraction.thin.AvroQuery;
import cern.nxcals.api.extraction.thin.ExtractionServiceGrpc;
import cern.nxcals.api.extraction.thin.ServiceFactory;
import cern.nxcals.api.extraction.thin.data.builders.DataQuery;
import cern.nxcals.api.utils.TimeUtils;
import cern.rbac.client.authentication.AuthenticationClient;
import cern.rbac.client.authentication.AuthenticationException;
import cern.rbac.common.RbaToken;
import cern.rbac.util.holder.ClientTierTokenHolder;
import com.google.common.collect.ImmutableMap;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Stream;

import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.DEVICE_KEY;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.SYSTEM_2;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.registerVariableToOneFieldWithOneConfig;
import static cern.nxcals.integrationtests.testutils.ExtractionTestHelper.registerVariableToWholeEntityWithOneConfig;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(classes = { ThinExtractionTest.class })
@Slf4j
public class ThinExtractionTest {
    //uncomment for IDE testing...
    static {
        //        System.setProperty("spark.servers.url",
        //                "nxcals-USER-1.cern.ch:15000,nxcals-USER-2.cern.ch:15000,nxcals-USER-3.cern.ch:15000,nxcals-USER-4.cern.ch:15000,nxcals-USER-5.cern.ch:15000"
        //                        .replace("USER", System.getProperty("user.name")));
        //        System.setProperty("service.url",
        //                "https://nxcals-USER-1.cern.ch:19093,https://nxcals-USER-1.cern.ch:19094,https://nxcals-USER-2.cern.ch:19093".replace(
        //                        "USER", System.getProperty("user.name")));
        //        System.setProperty("kafka.producer.bootstrap.servers",
        //                "https://nxcals-<user>-3.cern.ch:9092,https://nxcals-<user>-4.cern.ch:9092,https://nxcals-<user>-5.cern.ch:9092".replace(
        //                        "<user>", System.getProperty("user.name")));
        //        System.setProperty("rbac.env", "TEST");
    }

    private static final Instant NO_LOSSES_DAY = TimeUtils.getInstantFromString("2022-04-02 00:00:00.000");
    private static final Instant HBASE_EXTRACTION_START = Instant.now().minus(3, ChronoUnit.MINUTES);

    private static final String USER = "acclog";
    @Value("${acclog.password}")
    private String password;

    private static final String SYSTEM_NAME = SYSTEM_2;
    private static final String DEVICE1 = "NXCALS_MONITORING_DEV1";
    private static final Map<String, Object> ENTITY_KEY_VALUES1 = ImmutableMap.of(DEVICE_KEY, DEVICE1);

    private static final String FIELD1 = "stringField1";
    private static final String FIELD2 = "doubleField";
    private static final String RANDOM_SUFFIX = UUID.randomUUID().toString();
    private static final String VARIABLE1 = String.join(":", DEVICE1, RANDOM_SUFFIX, FIELD1);
    private static final String VARIABLE2 = String.join(":", DEVICE1, RANDOM_SUFFIX, FIELD2);
    private static final String VARIABLE_TO_WHOLE_ENTITY = String.join(":", DEVICE1, RANDOM_SUFFIX);

    private static final VariableService variableService = ServiceClientFactory.createVariableService();
    private static final EntityService entityService = ServiceClientFactory.createEntityService();
    private static final SystemSpecService systemService = ServiceClientFactory.createSystemSpecService();

    private static final ExtractionServiceGrpc.ExtractionServiceBlockingStub extractionService = ServiceFactory
            .createExtractionService(System.getProperty("spark.servers.url"));

    private static final SystemSpec SYSTEM_SPEC = systemService.findByName(SYSTEM_NAME)
            .orElseThrow(() -> new IllegalArgumentException("No such system"));

    private static Variable variable1;
    private static Variable variable2;
    private static Variable variableToWholeEntity;

    @BeforeAll
    public static void setUpVariables() {
        variable1 = registerVariableToOneFieldWithOneConfig(ENTITY_KEY_VALUES1, FIELD1, VARIABLE1, entityService,
                variableService,
                SYSTEM_SPEC);
        variable2 = registerVariableToOneFieldWithOneConfig(ENTITY_KEY_VALUES1, FIELD1, VARIABLE2, entityService,
                variableService,
                SYSTEM_SPEC);
        variableToWholeEntity = registerVariableToWholeEntityWithOneConfig(ENTITY_KEY_VALUES1, VARIABLE_TO_WHOLE_ENTITY,
                entityService,
                variableService, SYSTEM_SPEC);
        assert (variable1 != null);
        assert (variable2 != null);
        assert (variableToWholeEntity != null);
    }

    @AfterAll
    public static void removeVariables() {
        variableService.deleteAll(Set.of(variable1.getId(), variable2.getId()));
    }

    @BeforeEach
    void loginBeforeEachTest() {
        //must have RBAC here
        login(USER, this.password);
    }

    private static void login(String user, String password) {
        try {
            AuthenticationClient authenticationClient = AuthenticationClient.create();
            //Don't commit your password here!!!
            RbaToken token = authenticationClient.loginExplicit(user, password);
            ClientTierTokenHolder.setRbaToken(token);
        } catch (AuthenticationException e) {
            throw new IllegalArgumentException("Cannot login", e);
        }
    }

    @ParameterizedTest(name = "shouldExtractEntity from {1}")
    @MethodSource("extractionTimes")
    void shouldExtractEntity(Instant time, String sourceName) {
        extractData(time);
    }

    @ParameterizedTest(name = "shouldExtractEntityById from {1}")
    @MethodSource("extractionTimes")
    void shouldExtractEntityById(Instant time, String sourceName) {
        extractDataByKeyValuesAndIds(time);
    }

    @ParameterizedTest(name = "shouldExtractVariables from {1}")
    @MethodSource("extractionTimes")
    void shouldExtractVariables(Instant time, String sourceName) {
        extractVariables(time);
    }

    @ParameterizedTest(name = "shouldExtractVariableById from {1}")
    @MethodSource("extractionTimes")
    void shouldExtractVariableById(Instant time, String sourceName) {
        extractVariableByIds(time);
    }

    @ParameterizedTest(name = "shouldExtractVariableWithAlias from {1}")
    @MethodSource("extractionTimes")
    void shouldExtractVariableWithAlias(Instant time, String sourceName) {
        extractVariableWithAlias(time);
    }

    @ParameterizedTest(name = "shouldExtractEntityWithAlias from {1}")
    @MethodSource("extractionTimes")
    void shouldExtractEntityWithAlias(Instant time, String sourceName) {
        extractEntityWithAlias(time);
    }

    public static Stream<Object> extractionTimes() {
        return Stream.of(new Object[] { NO_LOSSES_DAY, "HDFS" }, new Object[] { HBASE_EXTRACTION_START, "Hbase" });
    }

    void extractData(Instant start) {
        // given
        Instant stop = start.plus(1, ChronoUnit.MINUTES);

        // when
        String query = DataQuery.builder().byEntities()
                .system(SYSTEM_NAME)
                .startTime(start)
                .endTime(stop)
                .entity().keyValue(DEVICE_KEY, DEVICE1).build();

        AvroData data = extractionService.query(AvroQuery.newBuilder().setScript(query).build());

        // then
        checkIfAvroDataIsNotEmpty(data);
    }

    void extractDataByKeyValuesAndIds(Instant start) {
        // given
        Instant stop = start.plus(1, ChronoUnit.MINUTES);

        // get any entity
        String query = DataQuery.builder().byEntities()
                .system(SYSTEM_NAME)
                .startTime(start)
                .endTime(stop)
                .entity().keyValue(DEVICE_KEY, DEVICE1).build();

        AvroData data = extractionService
                .query(AvroQuery.newBuilder().setScript(query).build());

        Long id = (Long) Datax.records(data).get(0).getEntry("nxcals_entity_id").get();
        // get entity by id
        String queryById = DataQuery.builder().entities()
                .system(SYSTEM_NAME).idEq(id)
                .timeWindow(start, stop).build();

        AvroData variableData = extractionService
                .query(AvroQuery.newBuilder().setScript(queryById).build());

        checkIfAvroDataIsNotEmpty(variableData);
    }

    void extractVariables(Instant start) {
        // given

        Instant stop = start.plus(1, ChronoUnit.MINUTES);
        AvroData data = getVariables(start, stop);

        checkIfAvroDataIsNotEmpty(data);
    }

    private AvroData getVariables(Instant start, Instant stop) {
        // get variable
        String query = DataQuery.builder().byVariables()
                .system(SYSTEM_NAME)
                .startTime(start)
                .endTime(stop)
                .variableLike("%" + DEVICE1 + ":" + RANDOM_SUFFIX + ":%").build();

        return ThinExtractionTest.extractionService.query(AvroQuery.newBuilder().setScript(query).build());
    }

    void extractVariableByIds(Instant start) {
        // given
        Instant stop = start.plus(1, ChronoUnit.MINUTES);

        // get entity by id
        String queryById = DataQuery.builder().variables()
                .system(SYSTEM_NAME).idEq(variable1.getId()).idIn(Set.of(variable2.getId()))
                .timeWindow(start, stop).build();

        AvroData data = extractionService.query(AvroQuery.newBuilder().setScript(queryById).build());

        checkIfAvroDataIsNotEmpty(data);
    }

    void extractVariableWithAlias(Instant start) {
        // given
        Instant stop = start.plus(1, ChronoUnit.MINUTES);

        // get entity by id
        String queryById = DataQuery.builder().variables()
                .system(SYSTEM_NAME)
                .nameEq(VARIABLE_TO_WHOLE_ENTITY)
                .timeWindow(start, stop)
                .fieldAliases("alias123", FIELD1)
                .build();

        AvroData data = extractionService.query(AvroQuery.newBuilder().setScript(queryById).build());

        checkIfAvroDataIsNotEmpty(data);
    }

    void extractEntityWithAlias(Instant start) {
        // given
        Instant stop = start.plus(1, ChronoUnit.MINUTES);

        // when
        String query = DataQuery.builder().entities()
                .system(SYSTEM_NAME)
                .keyValuesEq(Map.of(DEVICE_KEY, DEVICE1))
                .timeWindow(start, stop)
                .fieldAliases("alias", FIELD1, FIELD2).build();

        AvroData data = extractionService.query(AvroQuery.newBuilder().setScript(query).build());

        // then
        checkIfAvroDataIsNotEmpty(data);
    }

    private void checkIfAvroDataIsNotEmpty(AvroData data) {
        assertTrue(data.getRecordCount() > 0);
        assertFalse(Avro.records(data).isEmpty());
        assertFalse(Datax.records(data).isEmpty());
    }
}
