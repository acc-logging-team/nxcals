class TestUtils:
    @staticmethod
    def construct_var_name(random_suffix: str, field_name: str) -> str:
        return ":".join(["PYTIMBER", random_suffix, field_name])
