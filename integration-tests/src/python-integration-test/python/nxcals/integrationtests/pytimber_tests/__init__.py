import datetime
import random
import string

import numpy

from python.nxcals.integrationtests.pytimber_tests.test_utils import TestUtils

RANDOM_SUFFIX = "".join(random.choices(string.ascii_uppercase, k=10))

PYTIMBER_TEST_SYSTEM = "CMW"

PYTIMBER_TEST_DEVICE = f"PYTIMBER_TEST_DEVICE_{RANDOM_SUFFIX}"
PYTIMBER_TEST_PROPERTY = f"PYTIMBER_TEST_PROPERTY_{RANDOM_SUFFIX}"

PYTIMBER_TEST_DEVICE_2 = f"PYTIMBER_TEST_DEVICE_2_{RANDOM_SUFFIX}"
PYTIMBER_TEST_PROPERTY_2 = f"PYTIMBER_TEST_PROPERTY_2_{RANDOM_SUFFIX}"

PYTIMBER_TEST_FUND_DEVICE = f"PYTIMBER_TEST_FUND_DEVICE_{RANDOM_SUFFIX}"
PYTIMBER_TEST_FUND_PROPERTY = f"PYTIMBER_TEST_FUND_PROPERTY_{RANDOM_SUFFIX}"

PYTIMBER_TEST_FILL_DEVICE = f"PYTIMBER_TEST_FILL_DEVICE_{RANDOM_SUFFIX}"
PYTIMBER_TEST_FILL_PROPERTY = f"PYTIMBER_TEST_FILL_PROPERTY_{RANDOM_SUFFIX}"

PYTIMBER_TEST_BMODE_DEVICE = f"PYTIMBER_TEST_BMODE_DEVICE_{RANDOM_SUFFIX}"
PYTIMBER_TEST_BMODE_PROPERTY = f"PYTIMBER_TEST_BMODE_PROPERTY_{RANDOM_SUFFIX}"

PYTIMBER_TEST_NO_DATA_DEVICE = f"PYTIMBER_TEST_NO_DATA_DEVICE_{RANDOM_SUFFIX}"
PYTIMBER_TEST_NO_DATA_PROPERTY = f"PYTIMBER_TEST_NO_DATA_PROPERTY_{RANDOM_SUFFIX}"

PYTIMBER_TEST_CLASS = f"PYTIMBER_TEST_CLASS_{RANDOM_SUFFIX}"

ALIGNMENT_VARIABLE_NAME = f"PYTIMBER:{RANDOM_SUFFIX}:ALIGNMENT"

VAR_DESCRIPTION = f"Variable description {RANDOM_SUFFIX}"
VAR_UNIT = "Variable unit"

TIMESTAMPS_KEY = "timestamps"

INTERVALS_NUMBER = 10
RECORDS_NUMBER = 5

FILLS_NUMBER = 10
FIRST_FILL = 10
BEAM_MODES = [
    "NOBEAM",
    "CYCLING",
    "SETUP",
    "INJPROB",
    "INJPROB",
    "PRERAMP",
    "RAMP",
    "FLATTOP",
    "SQUEEZE",
    "ADJUST",
    "STABLE",
    "BEAMDUMP",
]
FILL_NR_FIELD = "FILL_NB"
BEAM_MODE_FIELD = "MACHINE_STATE"
NO_DATA_FIELD = "NO_DATA"

SECOND = 1000000000
MILLISECOND = 1000000
TIME_STEP = MILLISECOND * 10

START_TIME: int = (
    (numpy.datetime64("today") - numpy.timedelta64(2, "h"))
    .astype("datetime64[ns]")
    .item()
)
END_TIME: int = (
    (numpy.datetime64("today") - numpy.timedelta64(1, "h"))
    .astype("datetime64[ns]")
    .item()
)

HIERARCHY_BASE = "NXCALS_INTEGRATION_TESTS"

NODES_LIST = [
    (0, None, HIERARCHY_BASE),
    (1, f"/{HIERARCHY_BASE}", f"A_{RANDOM_SUFFIX}"),
    (2, f"/{HIERARCHY_BASE}/A_{RANDOM_SUFFIX}", f"A_A_{RANDOM_SUFFIX}"),
    (3, f"/{HIERARCHY_BASE}/A_{RANDOM_SUFFIX}", f"A_B_{RANDOM_SUFFIX}"),
    (4, f"/{HIERARCHY_BASE}", f"B_{RANDOM_SUFFIX}"),
    (5, f"/{HIERARCHY_BASE}/B_{RANDOM_SUFFIX}", f"B_A_{RANDOM_SUFFIX}"),
    (6, f"/{HIERARCHY_BASE}/B_{RANDOM_SUFFIX}", f"B_B_{RANDOM_SUFFIX}"),
]

SNAPSHOT_SYSTEM = "CERN"
SNAPSHOT_USER: str = "acclog"
DATE_FORMAT: str = "%Y-%m-%d %H:%M:%S.%f"

SNAPSHOTS_LIST = [
    (
        f"FILLS1{RANDOM_SUFFIX}",
        {
            "getDynamicDuration": "1",
            "getTimeZone": "LOCAL_TIME",
            "getTime": "WEEKS",
            "getPriorTime": "Now",
            "isEndTimeDynamic": "true",
        },
    ),
    (
        f"FILLS2{RANDOM_SUFFIX}",
        {
            "getDynamicDuration": "10",
            "getTimeZone": "UTC_TIME",
            "getTime": "HOURS",
            "getPriorTime": "Start of day",
            "isEndTimeDynamic": "true",
        },
    ),
    (
        f"FILLS3{RANDOM_SUFFIX}",
        {
            "getTimeZone": "UTC_TIME",
            "isEndTimeDynamic": "false",
            "getEndTime": "2050-01-01 00:00:00.000",
            "getStartTime": "2023-01-01 00:00:00.000",
        },
    ),
    (
        f"FILLS4{RANDOM_SUFFIX}",
        {
            "beamModeStart": (
                '{"validity":'
                '{"startTime":"2022-11-27T16:36:56.863488525Z","endTime":"2022-11-27T16:36:56.863488525Z"},'
                '"beamModeValue":"START OF FILL"}'
            ),
            "beamModeEnd": (
                '{"validity":'
                '{"startTime":"2022-11-28T10:01:20.781613525Z","endTime":"2022-11-28T10:01:20.781613525Z"},'
                '"beamModeValue":"END OF FILL"}'
            ),
            "fillNumber": "8496",
            "getTimeZone": "UTC_TIME",
            "isEndTimeDynamic": "false",
            "getEndTime": "2022-11-28 10:01:20.781613525",
            "getStartTime": "2022-11-27 16:36:56.863488525",
        },
    ),
    (
        f"FILLS5{RANDOM_SUFFIX}",
        {
            "getTimeZone": "UTC_TIME",
            "isEndTimeDynamic": "false",
            "getEndTime": datetime.datetime.utcfromtimestamp(END_TIME / 1e9).strftime(
                DATE_FORMAT
            )[:-3],
            "getStartTime": datetime.datetime.utcfromtimestamp(
                START_TIME / 1e9
            ).strftime(DATE_FORMAT)[:-3],
        },
    ),
    (
        f"FILLS6{RANDOM_SUFFIX}",
        {
            "getTimeZone": "LOCAL_TIME",
            "isEndTimeDynamic": "false",
            "getEndTime": datetime.datetime.fromtimestamp(END_TIME / 1e9).strftime(
                DATE_FORMAT
            )[:-3],
            "getStartTime": datetime.datetime.fromtimestamp(START_TIME / 1e9).strftime(
                DATE_FORMAT
            )[:-3],
        },
    ),
]

NUMERIC_VARIABLE_NAME = TestUtils.construct_var_name(RANDOM_SUFFIX, "FIELD0")
TEXT_VARIABLE_NAME = TestUtils.construct_var_name(RANDOM_SUFFIX, "FIELD1")
FLOAT_VARIABLE_NAME = TestUtils.construct_var_name(RANDOM_SUFFIX, "FIELD2")
VECTOR_STRING_VARIABLE_NAME = TestUtils.construct_var_name(RANDOM_SUFFIX, "FIELD4")
INT_VECTOR_NUMERIC_VARIABLE_NAME = TestUtils.construct_var_name(RANDOM_SUFFIX, "FIELD5")
DOUBLE_VECTOR_NUMERIC_VARIABLE_NAME = TestUtils.construct_var_name(
    RANDOM_SUFFIX, "FIELD6"
)
INT_MATRIX_VARIABLE_NAME = TestUtils.construct_var_name(RANDOM_SUFFIX, "FIELD7")
NO_DATA_VARIABLE_NAME = TestUtils.construct_var_name(RANDOM_SUFFIX, NO_DATA_FIELD)
