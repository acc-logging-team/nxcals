import math
import logging
from datetime import datetime, timezone
from typing import Any, Iterable, Optional, Dict, List

import numpy as np
import pytest
import time

from numpy.core._multiarray_umath import ndarray

from python.nxcals.integrationtests.pytimber_tests import (
    ALIGNMENT_VARIABLE_NAME,
    BEAM_MODES,
    END_TIME,
    FILLS_NUMBER,
    FIRST_FILL,
    HIERARCHY_BASE,
    INTERVALS_NUMBER,
    PYTIMBER_TEST_SYSTEM,
    RANDOM_SUFFIX,
    RECORDS_NUMBER,
    SECOND,
    SNAPSHOT_USER,
    START_TIME,
    TIME_STEP,
    TIMESTAMPS_KEY,
    VAR_DESCRIPTION,
    VAR_UNIT,
    NUMERIC_VARIABLE_NAME,
    TEXT_VARIABLE_NAME,
    DOUBLE_VECTOR_NUMERIC_VARIABLE_NAME,
    INT_VECTOR_NUMERIC_VARIABLE_NAME,
    FLOAT_VARIABLE_NAME,
    INT_MATRIX_VARIABLE_NAME,
    VECTOR_STRING_VARIABLE_NAME,
    NO_DATA_VARIABLE_NAME,
)
from python.nxcals.integrationtests.pytimber_tests.test_utils import TestUtils
from pytimber import (
    Fundamentals,
    LoggingDB,
    SparkLoggingDB,
    AGGREGATION_VALUE_FIELD,
)
from pytimber.stats import (
    Statistics,
    ScalarStatistics,
)
from pytimber.utils import (
    PatternOrList,
    TimeType,
    nanos_to_datetime,
    nanos_to_timestamp,
)

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)


class TestVariable:
    @pytest.mark.core
    @pytest.mark.parametrize(
        "pattern_or_list, var_count",
        [
            ("NON_EXISTENT_VARIABLE", 0),
            (TestUtils.construct_var_name(RANDOM_SUFFIX, "%FIELD%"), 8),
            ([NUMERIC_VARIABLE_NAME, TEXT_VARIABLE_NAME], 2),
        ],
    )
    def test_search(
        self, ldb: LoggingDB, pattern_or_list: PatternOrList, var_count: int
    ) -> None:
        variable_list = ldb.search(pattern_or_list)
        assert len(variable_list) == var_count

    @pytest.mark.core
    def test_get_description(self, ldb: LoggingDB) -> None:
        descriptions = ldb.getDescription([NUMERIC_VARIABLE_NAME])
        assert descriptions[NUMERIC_VARIABLE_NAME] == VAR_DESCRIPTION

    @pytest.mark.core
    def test_get_unit(self, ldb: LoggingDB) -> None:
        units = ldb.getUnit([NUMERIC_VARIABLE_NAME])
        assert units[NUMERIC_VARIABLE_NAME] == VAR_UNIT

    @pytest.mark.core
    def test_get_variable_origins(self, ldb: LoggingDB) -> None:
        origins = ldb.getVariablesOrigin(NUMERIC_VARIABLE_NAME)
        assert origins[NUMERIC_VARIABLE_NAME] == PYTIMBER_TEST_SYSTEM

    @pytest.mark.core
    def test_get_with_grouped_variables(self, ldb: LoggingDB) -> None:
        result = ldb.get(
            [TEXT_VARIABLE_NAME, NO_DATA_VARIABLE_NAME], START_TIME, END_TIME
        )

        t, v = result[TEXT_VARIABLE_NAME]
        assert len(t) == len(v) == INTERVALS_NUMBER * RECORDS_NUMBER


class TestFill:
    @pytest.mark.core
    @pytest.mark.parametrize(
        "fill_nr, fill_start_time, fill_last_mode, mode_start_time",
        [
            (
                FIRST_FILL,
                START_TIME,
                "BEAMDUMP",
                START_TIME + (len(BEAM_MODES) - 1) * TIME_STEP,
            ),
        ],
    )
    def test_get_lhc_fill_data(
        self,
        ldb: LoggingDB,
        fill_nr: int,
        fill_start_time: int,
        fill_last_mode: str,
        mode_start_time: int,
    ) -> None:
        fill_data = ldb.getLHCFillData(fill_nr)

        assert fill_data

        last = len(fill_data["beamModes"]) - 1

        assert datetime.utcfromtimestamp(fill_data["startTime"]) == nanos_to_datetime(
            fill_start_time, utc=False
        )
        assert datetime.utcfromtimestamp(
            fill_data["beamModes"][last]["startTime"],
        ) == nanos_to_datetime(mode_start_time, utc=False)

        assert fill_data["beamModes"][last]["mode"] == fill_last_mode
        assert datetime.utcfromtimestamp(
            fill_data["beamModes"][last]["startTime"],
        ) == nanos_to_datetime(mode_start_time, utc=False)

    @pytest.mark.core
    @pytest.mark.parametrize(
        "start_time, end_time, beam_modes, fills_cnt, first_fill",
        [
            (
                START_TIME,
                END_TIME,
                "STABLE",
                FILLS_NUMBER,
                FIRST_FILL,
            ),
            (
                START_TIME,
                END_TIME,
                None,
                FILLS_NUMBER,
                FIRST_FILL,
            ),
        ],
    )
    def test_get_lhc_fills_by_time(
        self,
        ldb: LoggingDB,
        start_time: TimeType,
        end_time: TimeType,
        beam_modes: Optional[PatternOrList],
        fills_cnt: int,
        first_fill: int,
    ) -> None:
        fills = ldb.getLHCFillsByTime(start_time, end_time, beam_modes)
        assert len(fills) == fills_cnt
        assert fills[0]["fillNumber"] == first_fill

    @pytest.mark.core
    @pytest.mark.parametrize(
        "start_time, end_time, mode1, mode2, int_cnt, first_fill",
        [
            (
                START_TIME,
                END_TIME,
                "SETUP",
                "STABLE",
                FILLS_NUMBER,
                FIRST_FILL,
            ),
        ],
    )
    def test_get_interval_by_lhc_modes(
        self,
        ldb: LoggingDB,
        start_time: TimeType,
        end_time: TimeType,
        mode1: str,
        mode2: str,
        int_cnt: int,
        first_fill: int,
    ) -> None:
        int_start = START_TIME + BEAM_MODES.index(mode1) * TIME_STEP
        int_end = START_TIME + (BEAM_MODES.index(mode2) + 1) * TIME_STEP

        intervals = ldb.getIntervalsByLHCModes(start_time, end_time, mode1, mode2)
        assert len(intervals) == int_cnt
        assert intervals[0][0] == first_fill
        assert datetime.utcfromtimestamp(intervals[0][1]) == nanos_to_datetime(
            int_start, utc=False
        )
        assert datetime.utcfromtimestamp(intervals[0][2]) == nanos_to_datetime(
            int_end, utc=False
        )


class TestAligned:
    def test_get_aligned_basic(self, ldb: LoggingDB, spark_ldb: SparkLoggingDB) -> None:
        master_var_name = NUMERIC_VARIABLE_NAME
        pattern_or_list = [master_var_name, TEXT_VARIABLE_NAME]

        self._get_aligned_basic_with_numpy_instance(
            ldb, master_var_name, pattern_or_list
        )
        self._get_aligned_basic_with_spark_instance(spark_ldb, pattern_or_list)

    def _get_aligned_basic_with_numpy_instance(
        self, ldb: LoggingDB, master_var_name: str, pattern_or_list: List[str]
    ) -> None:
        result = ldb.getAligned(pattern_or_list, START_TIME, END_TIME)
        master_data = ldb.getVariable(master_var_name, START_TIME, END_TIME)

        assert set(pattern_or_list + [TIMESTAMPS_KEY]) == set(list(result.keys()))
        assert result[TIMESTAMPS_KEY].tolist() == master_data[0].tolist()

    def _get_aligned_basic_with_spark_instance(
        self,
        spark_ldb: SparkLoggingDB,
        pattern_or_list: List[str],
    ) -> None:
        spark_result = spark_ldb.getAligned(pattern_or_list, START_TIME, END_TIME)

        assert len(spark_result) == len(pattern_or_list) + 1
        assert spark_result[TIMESTAMPS_KEY].count() == INTERVALS_NUMBER * RECORDS_NUMBER

    def test_get_aligned_with_master_specified(self, ldb: LoggingDB) -> None:
        master_var_name = ALIGNMENT_VARIABLE_NAME

        pattern_or_list = [NUMERIC_VARIABLE_NAME, TEXT_VARIABLE_NAME]

        result = ldb.getAligned(
            pattern_or_list, START_TIME, END_TIME, None, master_var_name
        )
        master_data = ldb.getVariable(master_var_name, START_TIME, END_TIME)

        assert result[TIMESTAMPS_KEY].tolist() == master_data[0].tolist()

    def test_get_aligned_with_fundamentals_and_master_specified(
        self, ldb: LoggingDB
    ) -> None:
        master_var_name = ALIGNMENT_VARIABLE_NAME

        pattern_or_list = [NUMERIC_VARIABLE_NAME, TEXT_VARIABLE_NAME]

        result = ldb.getAligned(
            pattern_or_list,
            START_TIME,
            END_TIME,
            f"ACC_{RANDOM_SUFFIX}:CYCLE%:USER%",
            master_var_name,
        )
        master_data = ldb.getVariable(master_var_name, START_TIME, END_TIME)
        assert result[TIMESTAMPS_KEY].tolist() == master_data[0].tolist()

    def test_get_aligned_vectors_aligned_to_scalar(self, ldb: LoggingDB) -> None:
        master_var_name = NUMERIC_VARIABLE_NAME
        pattern_or_list = [master_var_name, DOUBLE_VECTOR_NUMERIC_VARIABLE_NAME]

        result = ldb.getAligned(pattern_or_list, START_TIME, END_TIME)
        (_, master_data_values) = ldb.getVariable(master_var_name, START_TIME, END_TIME)

        assert np.array_equal(result[master_var_name], master_data_values)

    def test_get_aligned_vectors_aligned_to_vector(self, ldb: LoggingDB) -> None:
        master_var_name = INT_VECTOR_NUMERIC_VARIABLE_NAME
        pattern_or_list = [master_var_name, DOUBLE_VECTOR_NUMERIC_VARIABLE_NAME]

        result = ldb.getAligned(pattern_or_list, START_TIME, END_TIME)
        (_, master_data_values) = ldb.getVariable(master_var_name, START_TIME, END_TIME)

        assert all(np.array_equal(arr1, arr2) for arr1, arr2 in zip(result[master_var_name], master_data_values))

    @pytest.mark.skip(reason="no way of currently testing this")
    def test_on_real_data(self, ldb: LoggingDB) -> None:
        t1 = "2023-08-16 00:00:00.000"
        t2 = "2023-09-16 00:00:00.000"

        execution_times = []
        for i in range(10):
            start_time = time.time()

            # VECTOR_NUMERIC
            result = ldb.getAligned("MKP.BA1.CPUTEMP.5B:Settings:delayCpuDiff", t1, t2)

            # MATRIX_NUMERIC
            # result=ldb.getAligned('MKP.BA1.F3.PFN.2/ExpertAcquisitionDevice#arrIPSampling', t1, t2)

            end_time = time.time()
            elapsed_time = end_time - start_time
            print(i, elapsed_time)
            execution_times.append(elapsed_time)

        print(f"Execution time: {execution_times} seconds")


class TestScaled:
    @pytest.mark.parametrize(
        "scale_algorithm, expected_value",
        [
            ("MAX", RECORDS_NUMBER - 1),
            ("MIN", 0),
            ("AVG", sum(rec_nr for rec_nr in range(RECORDS_NUMBER)) / RECORDS_NUMBER),
            ("COUNT", RECORDS_NUMBER),
            ("SUM", sum(rec_nr for rec_nr in range(RECORDS_NUMBER))),
        ],
    )
    def test_get_scaled(
        self,
        ldb: LoggingDB,
        spark_ldb: SparkLoggingDB,
        scale_algorithm: str,
        expected_value: Any,
    ) -> None:
        self._get_scaled_with_numpy_instance(ldb, scale_algorithm, expected_value)
        self._get_scaled_with_spark_instance(spark_ldb, scale_algorithm, expected_value)

    def _get_scaled_with_numpy_instance(
        self,
        ldb: LoggingDB,
        scale_algorithm: str,
        expected_value: Any,
    ) -> None:
        data = ldb.getScaled(
            NUMERIC_VARIABLE_NAME,
            START_TIME,
            END_TIME,
            scaleInterval="SECOND",
            scaleAlgorithm=scale_algorithm,
            scaleSize=1,
        )
        _, v = data[NUMERIC_VARIABLE_NAME]

        result = np.repeat(expected_value, INTERVALS_NUMBER)
        assert (v[:INTERVALS_NUMBER] - result).sum() == 0

    def _get_scaled_with_spark_instance(
        self,
        spark_ldb: SparkLoggingDB,
        scale_algorithm: str,
        expected_value: Any,
    ) -> None:
        spark_data = spark_ldb.getScaled(
            NUMERIC_VARIABLE_NAME,
            START_TIME,
            END_TIME,
            scaleInterval="SECOND",
            scaleAlgorithm=scale_algorithm,
            scaleSize=1,
        )
        v = (
            spark_data[NUMERIC_VARIABLE_NAME]
            .toPandas()[AGGREGATION_VALUE_FIELD]
            .to_numpy()
        )

        result = np.repeat(expected_value, INTERVALS_NUMBER)
        assert (v[:INTERVALS_NUMBER] - result).sum() == 0

    def test_get_scaled_for_list(
        self, ldb: LoggingDB, spark_ldb: SparkLoggingDB
    ) -> None:
        variables = [NUMERIC_VARIABLE_NAME, FLOAT_VARIABLE_NAME]

        average_value1: float = 0
        average_value2: float = 0

        for rec_nr in range(RECORDS_NUMBER):
            average_value1 += rec_nr
            average_value2 += rec_nr + 0.2

        average_value1 /= RECORDS_NUMBER
        average_value2 /= RECORDS_NUMBER

        result1 = np.repeat(average_value1, INTERVALS_NUMBER)
        result2 = np.repeat(average_value2, INTERVALS_NUMBER)

        self._get_scaled_for_list_with_numpy_instance(ldb, variables, result1, result2)
        self._get_scaled_for_list_with_spark_instance(
            spark_ldb, variables, result1, result2
        )

    def _get_scaled_for_list_with_numpy_instance(
        self,
        ldb: LoggingDB,
        variables: List[str],
        expected_result1: ndarray[float],
        expected_result2: ndarray[float],
    ) -> None:
        data = ldb.getScaled(
            variables,
            START_TIME,
            END_TIME,
            scaleInterval="SECOND",
            scaleAlgorithm="AVG",
            scaleSize=1,
        )

        assert _sum_is_zero(data[variables[0]][1][:INTERVALS_NUMBER] - expected_result1)
        assert _sum_is_zero(data[variables[1]][1][:INTERVALS_NUMBER] - expected_result2)

    def _get_scaled_for_list_with_spark_instance(
        self,
        spark_ldb: SparkLoggingDB,
        variables: List[str],
        expected_result1: ndarray[float],
        expected_result2: ndarray[float],
    ) -> None:
        spark_data = spark_ldb.getScaled(
            variables,
            START_TIME,
            END_TIME,
            scaleInterval="SECOND",
            scaleAlgorithm="AVG",
            scaleSize=1,
        )

        assert _sum_is_zero(
            spark_data[variables[0]]
            .toPandas()[AGGREGATION_VALUE_FIELD]
            .to_numpy()[:INTERVALS_NUMBER]
            - expected_result1
        )
        assert _sum_is_zero(
            spark_data[variables[1]]
            .toPandas()[AGGREGATION_VALUE_FIELD]
            .to_numpy()[:INTERVALS_NUMBER]
            - expected_result2
        )


class TestPivot:
    @pytest.mark.core
    def test_get_pivot(self, ldb: LoggingDB) -> None:
        variable_name2 = TestUtils.construct_var_name(RANDOM_SUFFIX, "FIELD2")
        variable_name4 = TestUtils.construct_var_name(RANDOM_SUFFIX, "FIELD4")
        result = ldb.get_as_pivot(
            [variable_name2, variable_name4], START_TIME, END_TIME
        )

        assert result[START_TIME][variable_name2] == 0.2
        assert result[START_TIME][variable_name4].elements == ["A_0", "B_0", "C_0"]

    @pytest.mark.core
    def test_get_pivot_as_dataset(self, spark_ldb: SparkLoggingDB) -> None:
        variable_name2 = TestUtils.construct_var_name(RANDOM_SUFFIX, "FIELD2")
        variable_name4 = TestUtils.construct_var_name(RANDOM_SUFFIX, "FIELD4")
        result = spark_ldb.get_as_pivot(
            [variable_name2, variable_name4], START_TIME, END_TIME
        )

        assert len(result.collect()) == INTERVALS_NUMBER * RECORDS_NUMBER


class TestStats:
    @pytest.mark.core
    def test_get_stats_for_numeric(self, ldb: LoggingDB) -> None:
        stat = ldb.getStats(NUMERIC_VARIABLE_NAME, START_TIME, END_TIME)[
            NUMERIC_VARIABLE_NAME
        ]

        if isinstance(stat.MinTstamp, float):
            assert stat.MinTstamp * 1e9 == START_TIME
        if isinstance(stat.MaxTstamp, float):
            assert (
                stat.MaxTstamp * 1e9
                == START_TIME
                + (INTERVALS_NUMBER - 1) * SECOND
                + (RECORDS_NUMBER - 1) * TIME_STEP
            )
        assert stat.ValueCount == INTERVALS_NUMBER * RECORDS_NUMBER
        assert stat.MinValue == 0
        assert stat.MaxValue == RECORDS_NUMBER - 1

        nr_list = list(range(RECORDS_NUMBER))

        assert stat.AvgValue == sum(nr_list) / len(nr_list)
        assert math.isclose(stat.StandardDeviationValue, np.std(nr_list), abs_tol=1e-08)

    def test_get_stats_for_strings(self, ldb: LoggingDB) -> None:
        variable = TEXT_VARIABLE_NAME

        stat = ldb.getStats(variable, START_TIME, END_TIME)[variable]

        if isinstance(stat.MinTstamp, float):
            assert stat.MinTstamp * 1e9 == START_TIME
        if isinstance(stat.MaxTstamp, float):
            assert (
                stat.MaxTstamp * 1e9
                == START_TIME
                + (INTERVALS_NUMBER - 1) * SECOND
                + (RECORDS_NUMBER - 1) * TIME_STEP
            )
        assert stat.ValueCount == INTERVALS_NUMBER * RECORDS_NUMBER
        assert stat.MinValue == "str_0"
        assert stat.MaxValue == f"str_{RECORDS_NUMBER - 1}"

        assert stat.AvgValue is None
        assert stat.StandardDeviationValue is None

    def test_get_stats_for_scalars_and_vectors(self, ldb: LoggingDB) -> None:
        stat = ldb.getStats(
            [
                TEXT_VARIABLE_NAME,
                INT_VECTOR_NUMERIC_VARIABLE_NAME,
                DOUBLE_VECTOR_NUMERIC_VARIABLE_NAME,
            ],
            START_TIME,
            END_TIME,
        )

        assert isinstance(stat[TEXT_VARIABLE_NAME], ScalarStatistics)
        assert isinstance(stat[INT_VECTOR_NUMERIC_VARIABLE_NAME], Statistics)
        assert isinstance(stat[DOUBLE_VECTOR_NUMERIC_VARIABLE_NAME], Statistics)

    def test_get_stats_with_missing_type(self, ldb: LoggingDB) -> None:
        with pytest.raises(
            ValueError, match=f"[{FLOAT_VARIABLE_NAME}, {VECTOR_STRING_VARIABLE_NAME}]"
        ):
            ldb.getStats(
                [
                    FLOAT_VARIABLE_NAME,
                    VECTOR_STRING_VARIABLE_NAME,
                    DOUBLE_VECTOR_NUMERIC_VARIABLE_NAME,
                ],
                START_TIME,
                END_TIME,
            )


class TestFundamentals:
    def test_get_fundamentals(self, ldb: LoggingDB) -> None:
        result = ldb.getFundamentals(
            START_TIME, END_TIME, f"ACC_{RANDOM_SUFFIX}:CYCLE%:USER%"
        )
        unique_values_set = set(result)
        res_len = int((RECORDS_NUMBER + 1) / 2)
        assert res_len == len(result)
        assert f"ACC_{RANDOM_SUFFIX}:CYCLE0:USER0" in [
            fundamental.name for fundamental in result
        ]
        assert len(result) == len(unique_values_set)

    def test_search_fundamental(self, ldb: LoggingDB) -> None:
        result = ldb.searchFundamental(
            f"ACC_{RANDOM_SUFFIX}:CYCLE0:USER0", START_TIME, END_TIME
        )
        assert f"ACC_{RANDOM_SUFFIX}:CYCLE0:USER0" in result
        assert 1 == len(result)

    def test_search_fundamental_filtered_by_dest(self, ldb: LoggingDB) -> None:
        fund = Fundamentals(f"ACC_{RANDOM_SUFFIX}:CYCLE%:USER%", ["DEST0"])
        result = ldb.searchFundamental(fund, START_TIME, END_TIME)
        assert 1 == len(result)


class TestVectors:
    def test_get_meta_data(self, ldb: LoggingDB) -> None:
        result = ldb.getMetaData(DOUBLE_VECTOR_NUMERIC_VARIABLE_NAME)

        assert (
            len(result[DOUBLE_VECTOR_NUMERIC_VARIABLE_NAME][0])
            == len(result[DOUBLE_VECTOR_NUMERIC_VARIABLE_NAME][1])
            == INTERVALS_NUMBER * RECORDS_NUMBER
        )

        # Convert result to dictionary to facilitate testing
        d = {}
        for i in range(INTERVALS_NUMBER * RECORDS_NUMBER - 1):
            d[result[DOUBLE_VECTOR_NUMERIC_VARIABLE_NAME][0][i]] = result[
                DOUBLE_VECTOR_NUMERIC_VARIABLE_NAME
            ][1][i]

        assert d[nanos_to_timestamp(START_TIME)].elements == ["A_0", "B_0", "C_0"]


class TestExtractionMethods:
    @pytest.mark.core
    @pytest.mark.parametrize(
        "field_name, value",
        [
            ("FIELD0", 0),
            ("FIELD1", "str_0"),
            ("FIELD2", 0.2),
            ("FIELD3", "A"),
        ],
    )
    def test_get_scalars(self, ldb: LoggingDB, field_name: str, value: Any) -> None:
        variable_name = TestUtils.construct_var_name(RANDOM_SUFFIX, field_name)
        result = ldb.get(variable_name, START_TIME, END_TIME, unixtime=True)

        t, v = result[variable_name]
        assert len(t) == len(v) == INTERVALS_NUMBER * RECORDS_NUMBER

        assert t[0] == nanos_to_datetime(START_TIME).timestamp()
        assert v[0] == value

    def test_get_scalars_as_dataset(self, spark_ldb: SparkLoggingDB) -> None:
        variable_name1 = TestUtils.construct_var_name(RANDOM_SUFFIX, "FIELD1")
        variable_name3 = TestUtils.construct_var_name(RANDOM_SUFFIX, "FIELD3")
        result = spark_ldb.get([variable_name1, variable_name3], START_TIME, END_TIME)

        assert len(result.keys()) == 2
        assert (
            len(result[variable_name1].collect())
            == len(result[variable_name3].collect())
            == INTERVALS_NUMBER * RECORDS_NUMBER
        )

    @pytest.mark.core
    @pytest.mark.parametrize(
        "predefined_end_time, value, expected_time",
        [
            ("last", 0, START_TIME),
            ("next", 2, START_TIME + 2 * TIME_STEP),
            (None, 0, START_TIME),
        ],
    )
    def test_get_with_predefined_end_time(
        self,
        ldb: LoggingDB,
        predefined_end_time: TimeType,
        expected_time: TimeType,
        value: Any,
    ) -> None:
        result = ldb.get(
            NUMERIC_VARIABLE_NAME, START_TIME + TIME_STEP, predefined_end_time
        )

        assert result[NUMERIC_VARIABLE_NAME][0][0] * 1e9 == expected_time
        assert result[NUMERIC_VARIABLE_NAME][1][0] == value

    @pytest.mark.core
    def test_get_vars_with_empty_dataset(self, ldb: LoggingDB) -> None:
        data = ldb.get(
            [NUMERIC_VARIABLE_NAME, TEXT_VARIABLE_NAME, "NON_EXISTENT"],
            START_TIME,
            END_TIME,
        )
        assert len(data) == 2

    @pytest.mark.core
    @pytest.mark.parametrize(
        "field_name, value",
        [
            ("FIELD4", ["A_0", "B_0", "C_0"]),
            ("FIELD5", [1, 2, 3]),
            ("FIELD6", [1.0, 2.0, 3.0]),
        ],
    )
    def test_get_vectornumeric(
        self, ldb: LoggingDB, field_name: str, value: Any
    ) -> None:
        vectnum_variable_name = TestUtils.construct_var_name(RANDOM_SUFFIX, field_name)
        result = ldb.get(vectnum_variable_name, START_TIME, END_TIME)

        _, v = result[vectnum_variable_name]

        assert np.array_equal(v[0], value)

    def test_get_matrixnumeric(self, ldb: LoggingDB) -> None:
        result = ldb.get(INT_MATRIX_VARIABLE_NAME, START_TIME, END_TIME)
        t, v = result[INT_MATRIX_VARIABLE_NAME]

        assert len(t) == len(v) == RECORDS_NUMBER * INTERVALS_NUMBER

        for vv in v:
            assert len(vv) == 2
            assert len(vv[0]) == 3

        assert np.array_equal(v[0][0], [1, 2, 3])

    @pytest.mark.core
    @pytest.mark.parametrize(
        "fundamentals",
        [
            (Fundamentals(f"ACC_{RANDOM_SUFFIX}:CYCLE%:USER%")),
            (f"ACC_{RANDOM_SUFFIX}:CYCLE%:USER%"),
        ],
    )
    def test_get_variable_filtered_by_fundamentals(
        self, ldb: LoggingDB, fundamentals: [str, Fundamentals]
    ) -> None:
        t, v = ldb.get_variable(
            NUMERIC_VARIABLE_NAME,
            START_TIME,
            END_TIME,
            fundamental=fundamentals,
        )

        assert t[0] * 1e9 == START_TIME
        assert v[0] == 0
        assert INTERVALS_NUMBER * int((RECORDS_NUMBER + 1) / 2) == len(t) == len(v)

    def test_get_with_incorrect_fundamentals(self, ldb: LoggingDB) -> None:
        with pytest.raises(ValueError):
            ldb.get_variable(
                NUMERIC_VARIABLE_NAME,
                START_TIME,
                END_TIME,
                fundamentals=Fundamentals("Fundamental"),
                fundamental="Fundamental",
            )

    @pytest.mark.parametrize(
        "dest_in, dest_not_in, count_values",
        [
            (["DEST0"], None, INTERVALS_NUMBER),
            (None, ["DEST0"], INTERVALS_NUMBER * (int((RECORDS_NUMBER + 1) / 2) - 1)),
            (["DEST0"], "[DEST0]", INTERVALS_NUMBER),
        ],
    )
    def test_get_filter_by_fundamentals_with_destination(
        self,
        ldb: LoggingDB,
        dest_in: Optional[Iterable[str]],
        dest_not_in: Optional[Iterable[str]],
        count_values: int,
    ) -> None:
        fundamental = Fundamentals(
            f"ACC_{RANDOM_SUFFIX}:CYCLE%:USER%",
            dest_in=dest_in,
            dest_not_in=dest_not_in,
        )

        result = ldb.get(NUMERIC_VARIABLE_NAME, START_TIME, END_TIME, fundamental)
        assert len(result[NUMERIC_VARIABLE_NAME][0]) == count_values

    def test_get_filtered_by_empty_fundamentals(self, ldb: LoggingDB) -> None:
        fundamental = Fundamentals(f"ACC_{RANDOM_SUFFIX}:CYCLE%:USER%")

        assert (
            len(
                ldb.get(
                    NUMERIC_VARIABLE_NAME,
                    START_TIME + TIME_STEP,
                    START_TIME + TIME_STEP,
                )[NUMERIC_VARIABLE_NAME][0]
            )
            == 1
        )
        assert (
            ldb.get(
                NUMERIC_VARIABLE_NAME,
                START_TIME + TIME_STEP,
                START_TIME + TIME_STEP,
                fundamental,
            )
            == {}
        )

    def test_get_unixtime(self, ldb: LoggingDB) -> None:
        data = ldb.get(NUMERIC_VARIABLE_NAME, START_TIME, END_TIME, unixtime=False)
        t, v = data[NUMERIC_VARIABLE_NAME]

        assert t[0] == datetime.fromtimestamp(START_TIME / 1e9, tz=timezone.utc)

    def test_get_variables_with_different_types(self, ldb: LoggingDB):
        variable_name_pattern = TestUtils.construct_var_name(RANDOM_SUFFIX, "%")
        data = ldb.get(variable_name_pattern, START_TIME, END_TIME, unixtime=False)

        assert len(data) == 9
        for variable_name, (timestamps, values) in data.items():
            timestamps_1, values_1 = ldb.get(
                variable_name, START_TIME, END_TIME, unixtime=False
            )[variable_name]
            assert np.array_equal(timestamps, timestamps_1)
            assert all(np.array_equal(arr1, arr2) for arr1, arr2 in zip(values, values_1))


class TestTimestamps:
    STR_FORMAT = "%Y-%m-%d %H:%M:%S.%f"
    STR_FORMAT_WITH_ZONE = "%Y-%m-%d %H:%M:%S.%f%z"

    @pytest.mark.core
    @pytest.mark.parametrize(
        "start_time, end_time",
        [
            (START_TIME, END_TIME),
            (
                datetime.fromtimestamp(START_TIME / 1e9),
                datetime.fromtimestamp(END_TIME / 1e9),
            ),
            (
                datetime.fromtimestamp(START_TIME / 1e9, tz=timezone.utc),
                datetime.fromtimestamp(END_TIME / 1e9, tz=timezone.utc),
            ),
            (
                datetime.fromtimestamp(START_TIME / 1e9, tz=timezone.utc).timestamp(),
                datetime.fromtimestamp(END_TIME / 1e9, tz=timezone.utc).timestamp(),
            ),
            (
                datetime.fromtimestamp(START_TIME / 1e9).strftime(STR_FORMAT),
                datetime.fromtimestamp(END_TIME / 1e9).strftime(STR_FORMAT),
            ),
            (
                datetime.fromtimestamp(START_TIME / 1e9).strftime(STR_FORMAT_WITH_ZONE),
                datetime.fromtimestamp(END_TIME / 1e9).strftime(STR_FORMAT_WITH_ZONE),
            ),
            (np.datetime64(START_TIME, "ns"), np.datetime64(END_TIME, "ns")),
            (
                datetime.fromtimestamp(START_TIME / 1e9, tz=timezone.utc)
                - datetime(1970, 1, 1, 0, 0, 0, tzinfo=timezone.utc),
                datetime.fromtimestamp(END_TIME / 1e9, tz=timezone.utc)
                - datetime(1970, 1, 1, 0, 0, 0, tzinfo=timezone.utc),
            ),
        ],
    )
    def test_should_retrieve(
        self, ldb: LoggingDB, start_time: Any, end_time: Any
    ) -> None:
        result = ldb.get(NUMERIC_VARIABLE_NAME, start_time, end_time)
        t, _ = result[NUMERIC_VARIABLE_NAME]

        assert len(t) == INTERVALS_NUMBER * RECORDS_NUMBER


class TestHierarchies:
    @pytest.mark.core
    @pytest.mark.parametrize(
        "nodes_pattern, parent_node, child_node, count_children",
        [
            (
                f"/{HIERARCHY_BASE}%",
                f"/{HIERARCHY_BASE}/A_{RANDOM_SUFFIX}",
                f"/{HIERARCHY_BASE}/A_{RANDOM_SUFFIX}/A_A_{RANDOM_SUFFIX}",
                6,
            ),
        ],
    )
    def test_get_children_for_hierarchies_as_pattern(
        self,
        ldb: LoggingDB,
        nodes_pattern: PatternOrList,
        parent_node: str,
        child_node: str,
        count_children: int,
    ) -> None:
        result = ldb.getChildrenForHierarchies(nodes_pattern)

        assert child_node in result[parent_node]
        assert (
            len(
                dict(
                    (key, value)
                    for key, value in result.items()
                    if RANDOM_SUFFIX in key
                )
            )
            == count_children
        )

    @pytest.mark.core
    @pytest.mark.parametrize(
        "nodes_list, count_children",
        [
            (
                [
                    f"/{HIERARCHY_BASE}/A_{RANDOM_SUFFIX}",
                    f"/{HIERARCHY_BASE}/B_{RANDOM_SUFFIX}",
                ],
                4,
            ),
        ],
    )
    def test_get_children_for_hierarchies_as_list(
        self,
        ldb: LoggingDB,
        nodes_list: PatternOrList,
        count_children: int,
    ) -> None:
        result = ldb.getChildrenForHierarchies(nodes_list)

        total_children = sum((len(node) for node in result.values()))
        assert total_children == count_children

    @pytest.mark.core
    @pytest.mark.parametrize(
        "nodes_pattern, nodepath, variable, count",
        [
            (
                f"/{HIERARCHY_BASE}/A_{RANDOM_SUFFIX}%",
                f"/{HIERARCHY_BASE}/A_{RANDOM_SUFFIX}",
                f"PYTIMBER:{RANDOM_SUFFIX}:FIELD1",
                2,
            ),
            (
                f"/{HIERARCHY_BASE}/B_{RANDOM_SUFFIX}%",
                f"/{HIERARCHY_BASE}/B_{RANDOM_SUFFIX}",
                f"PYTIMBER:{RANDOM_SUFFIX}:FIELD2",
                2,
            ),
        ],
    )
    def test_get_vars_for_hierarchies_as_pattern(
        self,
        ldb: LoggingDB,
        nodes_pattern: PatternOrList,
        nodepath: str,
        variable: str,
        count: int,
    ) -> None:
        result = ldb.getVariablesForHierarchies(nodes_pattern)

        variables = result[nodepath]
        assert variable in variables
        assert len(variables) >= count

    @pytest.mark.core
    @pytest.mark.parametrize(
        "nodes_list, count_variables",
        [
            (
                [
                    f"/{HIERARCHY_BASE}/A_{RANDOM_SUFFIX}",
                    f"/{HIERARCHY_BASE}/B_{RANDOM_SUFFIX}",
                ],
                4,
            ),
        ],
    )
    def test_get_vars_for_hierarchies_as_list(
        self,
        ldb: LoggingDB,
        nodes_list: PatternOrList,
        count_variables: int,
    ) -> None:
        result = ldb.getVariablesForHierarchies(nodes_list)

        total_variables = 0
        for node in result:
            total_variables += len(result[node])

        assert total_variables == count_variables

    @pytest.mark.core
    @pytest.mark.parametrize(
        "variables_pattern, variable, nodepath, count",
        [
            (
                f"PYTIMBER:{RANDOM_SUFFIX}:%",
                f"PYTIMBER:{RANDOM_SUFFIX}:FIELD1",
                f"/{HIERARCHY_BASE}/A_{RANDOM_SUFFIX}",
                6,
            ),
        ],
    )
    def test_get_hierarchies_for_vars_as_pattern(
        self,
        ldb: LoggingDB,
        variables_pattern: PatternOrList,
        variable: str,
        nodepath: str,
        count: int,
    ) -> None:
        result = ldb.getHierarchiesForVariables(variables_pattern)

        hierarchies = result[variable]
        assert nodepath in hierarchies
        assert len(hierarchies) == count

    @pytest.mark.core
    @pytest.mark.parametrize(
        "variables_list, count_hierarchies",
        [
            (
                [
                    f"PYTIMBER:{RANDOM_SUFFIX}:FIELD1",
                    f"PYTIMBER:{RANDOM_SUFFIX}:FIELD2",
                ],
                12,
            ),
        ],
    )
    def test_get_hierarchies_for_vars_as_list(
        self,
        ldb: LoggingDB,
        variables_list: PatternOrList,
        count_hierarchies: int,
    ) -> None:
        result = ldb.getHierarchiesForVariables(variables_list)

        total_hierarchies = 0
        for node in result:
            total_hierarchies += len(result[node])

        assert total_hierarchies == count_hierarchies


class TestSnapshots:
    @pytest.mark.parametrize(
        "snapshot_pattern, owner_pattern, description_pattern, count_snapshots",
        [
            (f"FILLS%{RANDOM_SUFFIX}", f"{SNAPSHOT_USER}%", None, 6),
            ("%", SNAPSHOT_USER, f"FILLS1{RANDOM_SUFFIX}", 1),
        ],
    )
    def test_get_snapshot_names(
        self,
        ldb: LoggingDB,
        snapshot_pattern: PatternOrList,
        owner_pattern: str,
        description_pattern: str,
        count_snapshots: int,
    ) -> None:
        result = ldb.getSnapshotNames(
            snapshot_pattern, owner_pattern, description_pattern
        )
        assert len(result) == count_snapshots

    @pytest.mark.core
    @pytest.mark.parametrize(
        "snapshot_pattern, owner_pattern, snapshot, variable, count_snapshots, count_variables",
        [
            (
                f"FILLS%{RANDOM_SUFFIX}",
                SNAPSHOT_USER,
                f"FILLS2{RANDOM_SUFFIX}",
                f"PYTIMBER:{RANDOM_SUFFIX}:FIELD2",
                6,
                12,
            ),
        ],
    )
    def test_get_variables_for_snapshots_as_pattern(
        self,
        ldb: LoggingDB,
        snapshot_pattern: PatternOrList,
        owner_pattern: str,
        snapshot: str,
        variable: str,
        count_snapshots: int,
        count_variables: int,
    ) -> None:
        result = ldb.getVariablesForSnapshots(snapshot_pattern, owner_pattern)

        assert variable in result[snapshot]
        assert len(result) == count_snapshots

        total_variables = 0
        for snapshot in result:
            total_variables += len(result[snapshot])

        assert total_variables == count_variables
        assert variable in result[snapshot]

    @pytest.mark.parametrize(
        "snapshot_list, owner_pattern, snapshot, variable, count_variables",
        [
            (
                [f"FILLS1{RANDOM_SUFFIX}", f"FILLS2{RANDOM_SUFFIX}"],
                SNAPSHOT_USER,
                f"FILLS1{RANDOM_SUFFIX}",
                f"PYTIMBER:{RANDOM_SUFFIX}:FIELD1",
                4,
            ),
        ],
    )
    def test_get_variables_for_snapshots_as_list(
        self,
        ldb: LoggingDB,
        snapshot_list: PatternOrList,
        owner_pattern: str,
        snapshot: str,
        variable: str,
        count_variables: int,
    ) -> None:
        result = ldb.getVariablesForSnapshots(snapshot_list, owner_pattern)

        total_variables = 0
        for node in result:
            total_variables += len(result[node])

        assert total_variables == count_variables
        assert variable in result[snapshot]

    @pytest.mark.core
    @pytest.mark.parametrize(
        "snapshot_pattern_or_list, owner_pattern, snapshot_name, variable_name, values_count",
        [
            (
                [
                    f"FILLS1{RANDOM_SUFFIX}",
                    f"FILLS2{RANDOM_SUFFIX}",
                    f"FILLS4{RANDOM_SUFFIX}",
                ],
                SNAPSHOT_USER,
                f"FILLS1{RANDOM_SUFFIX}",
                f"PYTIMBER:{RANDOM_SUFFIX}:FIELD1",
                50,
            ),
            (
                f"FILLS2{RANDOM_SUFFIX}",
                SNAPSHOT_USER,
                f"FILLS2{RANDOM_SUFFIX}",
                f"PYTIMBER:{RANDOM_SUFFIX}:FIELD2",
                50,
            ),
            (
                f"FILLS3{RANDOM_SUFFIX}",
                SNAPSHOT_USER,
                f"FILLS3{RANDOM_SUFFIX}",
                f"PYTIMBER:{RANDOM_SUFFIX}:FIELD2",
                50,
            ),
            (
                f"FILLS5{RANDOM_SUFFIX}",
                SNAPSHOT_USER,
                f"FILLS5{RANDOM_SUFFIX}",
                f"PYTIMBER:{RANDOM_SUFFIX}:FIELD2",
                50,
            ),
            (
                f"FILLS6{RANDOM_SUFFIX}",
                SNAPSHOT_USER,
                f"FILLS6{RANDOM_SUFFIX}",
                f"PYTIMBER:{RANDOM_SUFFIX}:FIELD2",
                50,
            ),
        ],
    )
    def test_get_data_with_snapshots_using_definitions(
        self,
        ldb: LoggingDB,
        spark_ldb: SparkLoggingDB,
        snapshot_pattern_or_list: PatternOrList,
        owner_pattern: str,
        snapshot_name: str,
        variable_name: str,
        values_count: int,
    ) -> None:
        result1 = ldb.getDataUsingSnapshots(snapshot_pattern_or_list, owner_pattern)
        result2 = spark_ldb.getDataUsingSnapshots(
            snapshot_pattern_or_list, owner_pattern
        )

        assert (
            len(result1[snapshot_name][variable_name][1])
            == len(result2[snapshot_name][variable_name].collect())
            == values_count
        )

    @pytest.mark.core
    def test_get_data_with_snapshots_using_timestamps(
        self,
        ldb: LoggingDB,
        spark_ldb: SparkLoggingDB,
    ) -> None:
        snapshot_pattern_or_list: PatternOrList = f"FILLS1{RANDOM_SUFFIX}"
        owner_pattern: str = SNAPSHOT_USER
        variable_name: str = f"PYTIMBER:{RANDOM_SUFFIX}:FIELD1"

        for interval_nr in range(INTERVALS_NUMBER):
            for rec_nr in range(RECORDS_NUMBER):
                result = ldb.getDataUsingSnapshots(
                    f"FILLS1{RANDOM_SUFFIX}",
                    owner_pattern,
                    t1=START_TIME,
                    t2=START_TIME + interval_nr * SECOND + rec_nr * TIME_STEP,
                )
                assert (
                    len(result[snapshot_pattern_or_list][variable_name][0])
                    == interval_nr * RECORDS_NUMBER + rec_nr + 1
                )

    @pytest.mark.core
    def test_input_for_get_data_using_snapshots(
        self,
        ldb: LoggingDB,
    ) -> None:
        with pytest.raises(ValueError):
            ldb.get_data_using_snapshots(
                "SNAPSHOT_NAME",
                "OWNER",
                search_criteria={"A": "B"},
                t1=0,
                t2=1,
            )

        with pytest.raises(ValueError):
            ldb.get_data_using_snapshots(
                "SNAPSHOT_NAME",
                "OWNER",
                t2=1,
            )

    @pytest.mark.core
    @pytest.mark.parametrize(
        "snapshot_pattern_or_list, owner_pattern, search_criteria, snapshot_name, variable_name, values_count",
        [
            (
                [
                    f"FILLS1{RANDOM_SUFFIX}",
                    f"FILLS2{RANDOM_SUFFIX}",
                    f"FILLS4{RANDOM_SUFFIX}",
                ],
                SNAPSHOT_USER,
                {
                    "getDynamicDuration": "1",
                    "getTimeZone": "LOCAL_TIME",
                    "getTime": "WEEKS",
                    "getPriorTime": "Now",
                    "isEndTimeDynamic": "true",
                },
                f"FILLS1{RANDOM_SUFFIX}",
                f"PYTIMBER:{RANDOM_SUFFIX}:FIELD1",
                50,
            ),
            (
                f"FILLS3{RANDOM_SUFFIX}",
                SNAPSHOT_USER,
                {
                    "getTimeZone": "UTC_TIME",
                    "isEndTimeDynamic": "false",
                    "getEndTime": "2050-01-01 00:00:00.000",
                    "getStartTime": "2023-01-01 00:00:00.000",
                    "fundamentalFilter": f'{{"accelerator": "ACC_{RANDOM_SUFFIX}", "lsaCycle": "CYCLE%", "timingUser": "USER%"}}',
                },
                f"FILLS3{RANDOM_SUFFIX}",
                f"PYTIMBER:{RANDOM_SUFFIX}:FIELD1",
                30,
            ),
            (
                f"FILLS3{RANDOM_SUFFIX}",
                SNAPSHOT_USER,
                {
                    "getTimeZone": "UTC_TIME",
                    "isEndTimeDynamic": "false",
                    "getEndTime": "2050-01-01 00:00:00.000",
                    "getStartTime": "2023-01-01 00:00:00.000",
                    "fundamentalFilter": f'{{"accelerator": "ACC_{RANDOM_SUFFIX}", "lsaCycle": "CYCLE2", "timingUser": "USER2"}}',
                },
                f"FILLS3{RANDOM_SUFFIX}",
                f"PYTIMBER:{RANDOM_SUFFIX}:FIELD2",
                10,
            ),
        ],
    )
    def test_get_data_with_snapshots_using_snapshot_properties(
        self,
        ldb: LoggingDB,
        spark_ldb: SparkLoggingDB,
        snapshot_pattern_or_list: PatternOrList,
        owner_pattern: str,
        search_criteria: Optional[Dict[str, str]],
        snapshot_name: str,
        variable_name: str,
        values_count: int,
    ) -> None:
        result1 = ldb.getDataUsingSnapshots(
            snapshot_pattern_or_list, owner_pattern, search_criteria=search_criteria
        )
        result2 = spark_ldb.getDataUsingSnapshots(
            snapshot_pattern_or_list, owner_pattern, search_criteria=search_criteria
        )

        assert (
            len(result1[snapshot_name][variable_name][1])
            == len(result2[snapshot_name][variable_name].collect())
            == values_count
        )


class TestInit:
    def test_init_without_arguments(self) -> None:
        LoggingDB()

    def test_init_with_flavor(self) -> None:
        LoggingDB(sparkconf="small")


def _sum_is_zero(data_series: tuple[float]) -> bool:
    return math.isclose(data_series.sum(), 0, abs_tol=1e-06)
