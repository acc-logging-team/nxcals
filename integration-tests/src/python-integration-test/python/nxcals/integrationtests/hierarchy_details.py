import dataclasses
from typing import Any, List


@dataclasses.dataclass(frozen=True)
class HierarchyDetails:
    system: str
    base: str
    nodes_list: List[Any]
