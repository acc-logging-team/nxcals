from datetime import datetime, timedelta

import pkg_resources
from nxcals.api.extraction.data.builders import (
    DataQuery,
    EntityQuery,
    ParameterDataQuery,
)
from nxcals.api.extraction.data.builders_expanded import DataQuery as DataQueryExpanded
from pyspark.sql.functions import col
from pyspark.sql.utils import IllegalArgumentException

from nxcals.api.utils.extraction.array_utils import ArrayUtils
from . import (
    PySparkIntegrationTest,
    metadata_utils,
    spark_session,
    log,
)
from .. import MONITORING_SYSTEM_NAME, MONITORING_DEVICE_KEY
from pyspark.sql.types import ArrayType, IntegerType, FloatType


def no_data_losses_day():
    return datetime(2022, 4, 2, 00, 00, 00, 000, tzinfo=None)  # magic date


def end_of_no_data_losses_day():
    return no_data_losses_day() + timedelta(days=1) - timedelta(seconds=1)


class ShouldHaveKerberosPropertiesSetInJVM(PySparkIntegrationTest):
    def runTest(self):
        principal = spark_session._jvm.java.lang.System.getProperty(
            "kerberos.principal"
        )
        keytab = spark_session._jvm.java.lang.System.getProperty("kerberos.keytab")
        log.info(principal)
        log.info(keytab)
        spark_kerberos_principal = spark_session.sparkContext.getConf().get(
            "spark.kerberos.principal"
        )
        spark_kerberos_keytab = spark_session.sparkContext.getConf().get(
            "spark.kerberos.keytab"
        )
        self.assertTrue(len(principal) > 0)
        self.assertTrue(len(keytab) > 0)
        self.assertEqual(principal, spark_kerberos_principal)
        self.assertEqual(keytab, spark_kerberos_keytab)


class ShouldHaveKerberosReloginServicePresent(PySparkIntegrationTest):
    def runTest(self):
        from nxcals.spark_session_builder import _nxcals_relogin_service

        self.assertTrue(_nxcals_relogin_service is not None)


class ShouldGetEmptyDatasetWithSystemDefinedFieldsOnKeyValuesLikeQueryWhenNoResourcesFound(
    PySparkIntegrationTest
):
    def runTest(self):
        df = (
            DataQuery.builder(spark_session)
            .byEntities()
            .system(MONITORING_SYSTEM_NAME)
            .startTime(no_data_losses_day())
            .endTime(end_of_no_data_losses_day())
            .entity()
            .keyValueLike(MONITORING_DEVICE_KEY, "NON_EXISTING_DEVICE")
            .build()
        )

        self.assertEqual(df.count(), 0)
        self.assertEqual(len(df.columns), 4)


class ShouldThrowOnKeyValuesQueryWhenNoResourcesFound(PySparkIntegrationTest):
    def runTest(self):
        with self.assertRaises(IllegalArgumentException):
            DataQuery.builder(spark_session).byEntities().system(
                MONITORING_SYSTEM_NAME
            ).startTime(no_data_losses_day()).endTime(
                end_of_no_data_losses_day()
            ).entity().keyValue(MONITORING_DEVICE_KEY, "NON_EXISTING_DEVICE").build()


class ShouldThrowOnIdQueryWhenNoResourcesFound(PySparkIntegrationTest):
    def runTest(self):
        with self.assertRaises(IllegalArgumentException):
            DataQuery.builder(spark_session).entities().system(
                MONITORING_SYSTEM_NAME
            ).idEq(0).timeWindow(
                no_data_losses_day(), end_of_no_data_losses_day()
            ).build()


class ShouldThrowOnIdAndKeyValueQueryWhenNoResourcesFound(PySparkIntegrationTest):
    def runTest(self):
        with self.assertRaises(IllegalArgumentException):
            DataQuery.builder(spark_session).entities().system(
                MONITORING_SYSTEM_NAME
            ).idEq(0).keyValuesEq(
                {MONITORING_DEVICE_KEY: "NON_EXISTING_DEVICE"}
            ).timeWindow(no_data_losses_day(), end_of_no_data_losses_day()).build()


class ShouldThrowOnKeyValueAndIdQueryWhenNoResourcesFound(PySparkIntegrationTest):
    def runTest(self):
        with self.assertRaises(IllegalArgumentException):
            DataQuery.builder(spark_session).entities().system(
                MONITORING_SYSTEM_NAME
            ).keyValuesEq({MONITORING_DEVICE_KEY: "NON_EXISTING_DEVICE"}).idEq(
                0
            ).timeWindow(no_data_losses_day(), end_of_no_data_losses_day()).build()


class ShouldThrowOnQueryByNonExistingIds(PySparkIntegrationTest):
    def runTest(self):
        with self.assertRaises(IllegalArgumentException):
            DataQuery.builder(spark_session).entities().system(
                MONITORING_SYSTEM_NAME
            ).keyValuesEq({MONITORING_DEVICE_KEY: "NON_EXISTING_DEVICE"}).idIn(
                {0}
            ).timeWindow(no_data_losses_day(), end_of_no_data_losses_day()).build()


class ShouldExtractCorrectNumberOfRecordsFromOneDate(PySparkIntegrationTest):
    def runTest(self):
        df = (
            DataQuery.builder(spark_session)
            .byEntities()
            .system(MONITORING_SYSTEM_NAME)
            .startTime(no_data_losses_day())
            .endTime(end_of_no_data_losses_day())
            .fieldAlias("fieldAlias", "field1")
            .fieldAliases({"fieldAlias": ["field1", "field2"]})
            .entity()
            .keyValue(MONITORING_DEVICE_KEY, "NXCALS_MONITORING_DEV6")
            .build()
        )

        self.assertEqual(df.count(), 86400)


class ShouldExtractCorrectNumberOfRecordsFromOneDateUsingStaticFunction(
    PySparkIntegrationTest
):
    def runTest(self):
        df = DataQuery.getForEntities(
            spark_session,
            system=MONITORING_SYSTEM_NAME,
            start_time=no_data_losses_day(),
            end_time=end_of_no_data_losses_day(),
            entity_queries=[
                EntityQuery(
                    {MONITORING_DEVICE_KEY: "NXCALS_MONITORING_DEV6"},
                ),
            ],
            field_aliases={
                "fieldAlias": ["field1", "field2"],
            },
        )

        self.assertEqual(df.count(), 86400)


class ShouldExtractCorrectNumberOfRecordsFromOneDateAsString(PySparkIntegrationTest):
    def runTest(self):
        time_format = "%Y-%m-%d %H:%M:%S.%f"

        df = (
            DataQuery.builder(spark_session)
            .byEntities()
            .system(MONITORING_SYSTEM_NAME)
            .startTime(no_data_losses_day().strftime(time_format))
            .endTime(end_of_no_data_losses_day().strftime(time_format))
            .entity()
            .keyValue(MONITORING_DEVICE_KEY, "NXCALS_MONITORING_DEV6")
            .build()
        )

        self.assertEqual(df.count(), 86400)


# there is some serious problem in the configuration of the integration test spark context
# none of the three below commands (collect, foreach with simple lambda and foreach with complex lambda)
# work well, count() and join() however, work fine.
# they fail with "AttributeError: 'NoneType' object has no attribute 'setCallSite'"
#
# def assert_equals(obtained, expected):
#     if obtained != expected:
#         raise ValueError(
#             'Test failed. Expected size is: {}, actual is: {}'.format(expected, obtained))
#
# class ShouldProperlySerializeForeachAndDoSimpleFiltering(PySparkIntegrationTest):
#     def runTest(self):
#
#         df = KeyValuesQuery.builder(getSpark()) \
#             .system(MONITORING_SYSTEM_NAME) \
#             .startTime(no_data_losses_day()) \
#             .duration(timedelta(minutes=30)) \
#             .entity() \
#             .keyValue(MONITORING_DEVICE_KEY, 'NXCALS_MONITORING_DEV6') \
#             .build()
#
#         # df.rdd.collect()
#
#         # df.foreach(lambda x: x)
#
#         # df.foreach(lambda row: assert_equals(len(list(filter(lambda e: e % 2, row.byteArrayField.elements))), 5))


class ShouldCorrectlyExecuteSparkAggregationFunctions(PySparkIntegrationTest):
    def runTest(self):
        df1 = (
            DataQuery.builder(spark_session)
            .byEntities()
            .system(MONITORING_SYSTEM_NAME)
            .startTime(no_data_losses_day())
            .duration(timedelta(minutes=30))
            .entity()
            .keyValue(MONITORING_DEVICE_KEY, "NXCALS_MONITORING_DEV6")
            .build()
            .alias("df1")
        )

        df2 = (
            DataQuery.builder(spark_session)
            .byEntities()
            .system(MONITORING_SYSTEM_NAME)
            .startTime(no_data_losses_day())
            .duration(timedelta(minutes=30))
            .entity()
            .keyValue(MONITORING_DEVICE_KEY, "NXCALS_MONITORING_DEV7")
            .build()
            .alias("df2")
        )

        for res in (
            df2.join(df1, col("df1.shortField1") == col("df2.shortField1"))
            .groupBy(col("df2.byteField1"))
            .avg("df1.longField2")
            .collect()
        ):
            self.assertEqual(res["avg(longField2)"], 1)


class ShouldGetEmptyDatasetWithSystemDefinedFieldsOnVariablesLikeQueryWhenNoResourcesFound(
    PySparkIntegrationTest
):
    def runTest(self):
        df = (
            DataQuery.builder(spark_session)
            .byVariables()
            .system(MONITORING_SYSTEM_NAME)
            .startTime(no_data_losses_day())
            .endTime(end_of_no_data_losses_day())
            .variableLike("NON_EXISTING_DEVICE_NAME")
            .build()
        )

        self.assertEqual(df.count(), 0)
        self.assertEqual(len(df.columns), 4)


class ShouldGetEmptyDatasetWithSystemDefinedFieldsOnVariablesLikeQueryWhenNoResourcesFoundUsingStaticMethods(
    PySparkIntegrationTest,
):
    def runTest(self):
        df = DataQuery.getForVariables(
            spark=spark_session,
            system=MONITORING_SYSTEM_NAME,
            start_time=no_data_losses_day(),
            end_time=end_of_no_data_losses_day(),
            variables_like=["NON_EXISTING_DEVICE_NAME"],
            field_aliases={"fieldAlias": ["field1", "field2"]},
        )

        self.assertEqual(df.count(), 0)
        self.assertEqual(len(df.columns), 4)


class ShouldThrowOnVariablesQueryWhenNoResourcesFound(PySparkIntegrationTest):
    def runTest(self):
        with self.assertRaises(IllegalArgumentException):
            DataQuery.builder(spark_session).byVariables().system(
                MONITORING_SYSTEM_NAME
            ).startTime(no_data_losses_day()).endTime(
                end_of_no_data_losses_day()
            ).variable("NON_EXISTING_DEVICE_NAME").build()


class ShouldBeAbleToJoinRDDsInYarnModeUsingSpark3(PySparkIntegrationTest):
    def runTest(self):
        rdd1 = (
            DataQuery.builder(spark_session)
            .byEntities()
            .system(MONITORING_SYSTEM_NAME)
            .startTime(no_data_losses_day())
            .duration(timedelta(minutes=30))
            .entity()
            .keyValue(MONITORING_DEVICE_KEY, "NXCALS_MONITORING_DEV6")
            .build()
            .select("timestamp", "stringField1")
            .rdd
        )

        rdd2 = (
            DataQuery.builder(spark_session)
            .byEntities()
            .system(MONITORING_SYSTEM_NAME)
            .startTime(no_data_losses_day())
            .duration(timedelta(minutes=30))
            .entity()
            .keyValue(MONITORING_DEVICE_KEY, "NXCALS_MONITORING_DEV7")
            .build()
            .select("timestamp", "stringField1")
            .rdd
        )

        rdd3 = rdd1.join(rdd2)
        self.assertTrue(rdd3.count() > 0)


class ShouldHaveSameLibrariesInAllMachinesInYarnMode(PySparkIntegrationTest):
    def checkLibraries(self, master_libraries):
        import pkg_resources

        installed_packages = pkg_resources.working_set
        current_machine_libraries = sorted(
            ["%s==%s" % (i.key, i.version) for i in installed_packages],
        )
        assert current_machine_libraries == master_libraries

    def runTest(self):
        installed_packages = pkg_resources.working_set
        current_libraries = sorted(
            ["%s==%s" % (i.key, i.version) for i in installed_packages],
        )
        spark_session.sparkContext.parallelize(
            [current_libraries],
        ).map(self.checkLibraries)


class MockSystemTest(PySparkIntegrationTest):
    SYSTEM = MONITORING_SYSTEM_NAME
    DEVICE = "NXCALS_MONITORING_DEV1"
    KEY_VALUES_PATTERN = f'%"{DEVICE}"%'

    entity = None

    @classmethod
    def setUpClass(cls):
        maybe_entity = metadata_utils.find_entity(
            system=cls.SYSTEM,
            key_value_pattern=cls.KEY_VALUES_PATTERN,
        )
        if not maybe_entity.isPresent():
            raise RuntimeError(
                f"Cannot find device in system! SYSTEM: {cls.SYSTEM} DEVICE: {cls.DEVICE}",
            )
        cls.entity = maybe_entity.get()


class ShouldCorrectlySearchForEntityWithTimeWindowFromDataQueryEntities(MockSystemTest):
    def runTest(self):
        df = (
            DataQuery.builder(spark_session)
            .entities()
            .system(self.SYSTEM)
            .keyValuesEq({MONITORING_DEVICE_KEY: self.DEVICE})
            .timeWindow(no_data_losses_day(), end_of_no_data_losses_day())
            .build()
        )

        self.assertTrue(df.count() > 0)


class ShouldCorrectlySearchForListOfEntityWithTimeWindowFromDataQueryEntities(
    MockSystemTest
):
    def runTest(self):
        df = (
            DataQuery.builder(spark_session)
            .entities()
            .system(self.SYSTEM)
            .keyValuesIn([{MONITORING_DEVICE_KEY: self.DEVICE}])
            .timeWindow(no_data_losses_day(), end_of_no_data_losses_day())
            .build()
        )

        self.assertTrue(df.count() > 0)


class ShouldCorrectlySearchForEntityPatternWithTimeWindowFromDataQueryEntities(
    MockSystemTest
):
    def runTest(self):
        df = (
            DataQuery.builder(spark_session)
            .entities()
            .system(self.SYSTEM)
            .keyValuesLike({MONITORING_DEVICE_KEY: self.DEVICE})
            .timeWindow(no_data_losses_day(), end_of_no_data_losses_day())
            .build()
        )

        self.assertTrue(df.count() > 0)


class ShouldCorrectlySearchForEntityWithTimepointFromDataQueryEntities(MockSystemTest):
    def runTest(self):
        DataQuery.builder(spark_session).entities().system(self.SYSTEM).keyValuesEq(
            {MONITORING_DEVICE_KEY: self.DEVICE}
        ).atTime(no_data_losses_day()).build()


class ShouldCorrectlySearchForEntityById(MockSystemTest):
    def runTest(self):
        df = (
            DataQuery.builder(spark_session)
            .entities()
            .system(self.SYSTEM)
            .idEq(self.entity.getId())
            .timeWindow(no_data_losses_day(), end_of_no_data_losses_day())
            .build()
        )

        self.assertTrue(df.count() > 0)


class ShouldCorrectlySearchForEntityByIds(MockSystemTest):
    def runTest(self):
        df1 = (
            DataQuery.builder(spark_session)
            .entities()
            .system(self.SYSTEM)
            .idIn({self.entity.getId()})
            .timeWindow(no_data_losses_day(), end_of_no_data_losses_day())
            .build()
        )

        df2 = (
            DataQuery.builder(spark_session)
            .entities()
            .idIn({self.entity.getId()})
            .timeWindow(no_data_losses_day(), end_of_no_data_losses_day())
            .build()
        )

        df3 = (
            DataQuery.builder(spark_session)
            .entities()
            .idEq(self.entity.getId())
            .timeWindow(no_data_losses_day(), end_of_no_data_losses_day())
            .build()
        )

        df1count = df1.count()

        self.assertTrue(df1count == df2.count())
        self.assertTrue(df1count == df3.count())
        self.assertTrue(df1count > 0)


class CMWEntityTest(PySparkIntegrationTest):
    SYSTEM = "TEST-CMW"
    DEVICE = "test_device_python_integration_test"
    PROPERTY = "test_property_python_integration_test"
    PARAMETER = f"{DEVICE}/{PROPERTY}"
    KEY_VALUES_PATTERN = f"%{DEVICE}%{PROPERTY}%"

    @classmethod
    def setUpClass(cls):
        maybe_entity = metadata_utils.find_entity(
            system=cls.SYSTEM,
            key_value_pattern=cls.KEY_VALUES_PATTERN,
        )
        if not maybe_entity.isPresent():
            metadata_utils.create_cmw_entity(cls.SYSTEM, cls.DEVICE, cls.PROPERTY)


class ShouldCorrectlySearchForDevicePropertyWithTimeWindow(CMWEntityTest):
    def runTest(self):
        ParameterDataQuery.builder(spark_session).system(self.SYSTEM).deviceEq(
            self.DEVICE
        ).propertyEq(self.PROPERTY).timeWindow(
            no_data_losses_day(), end_of_no_data_losses_day()
        ).build()


class ShouldCorrectlySearchForDevicePropertyPatternsWithTimeWindow(CMWEntityTest):
    def runTest(self):
        ParameterDataQuery.builder(spark_session).system(self.SYSTEM).deviceLike(
            self.DEVICE
        ).propertyLike(self.PROPERTY).timeWindow(
            no_data_losses_day(), end_of_no_data_losses_day()
        ).build()


class ShouldCorrectlySearchForParameterWithTimeWindow(CMWEntityTest):
    def runTest(self):
        ParameterDataQuery.builder(spark_session).system(self.SYSTEM).parameterEq(
            self.PARAMETER
        ).timeWindow(no_data_losses_day(), end_of_no_data_losses_day()).build()


class ShouldCorrectlySearchForListOfParameterWithTimeWindow(CMWEntityTest):
    def runTest(self):
        ParameterDataQuery.builder(spark_session).system(self.SYSTEM).parameterIn(
            [self.PARAMETER]
        ).timeWindow(no_data_losses_day(), end_of_no_data_losses_day()).build()


class VariableTest(PySparkIntegrationTest):
    system = MONITORING_SYSTEM_NAME
    entity_name_pattern = '%"NXCALS_MONITORING_DEV1"%'
    variable_name = "PYTHON_INTEGRATION_TEST_VARIABLE"
    field = "specification"
    variable = None

    @classmethod
    def setUpClass(cls):
        maybe_variable = metadata_utils.find_variable(
            system=cls.system,
            name=cls.variable_name,
        )
        if not maybe_variable.isPresent():
            maybe_entity = metadata_utils.find_entity(
                system=cls.system,
                key_value_pattern=cls.entity_name_pattern,
            )
            if not maybe_entity.isPresent():
                raise RuntimeError(
                    f"Entity {cls.entity_name_pattern} not found in system: {cls.system}",
                )
            else:
                cls.variable = metadata_utils.create_variable(
                    name=cls.variable_name,
                    entity=maybe_entity.get(),
                    field=cls.field,
                )
        else:
            cls.variable = maybe_variable.get()


class ShouldCorrectlySearchForVariableWithTimeWindow(VariableTest):
    def runTest(self):
        df = (
            DataQuery.builder(spark_session)
            .variables()
            .system(self.system)
            .nameEq(self.variable_name)
            .timeWindow(no_data_losses_day(), end_of_no_data_losses_day())
            .build()
        )

        self.assertTrue(df.count() > 0)


class ShouldCorrectlySearchForListOfVariableWithTimeWindow(VariableTest):
    def runTest(self):
        df = (
            DataQuery.builder(spark_session)
            .variables()
            .system(self.system)
            .nameIn([self.variable_name])
            .timeWindow(no_data_losses_day(), end_of_no_data_losses_day())
            .build()
        )

        self.assertTrue(df.count() > 0)


class ShouldCorrectlySearchForVariablePatternWithTimeWindow(VariableTest):
    def runTest(self):
        df = (
            DataQuery.builder(spark_session)
            .variables()
            .system(self.system)
            .nameLike(self.variable_name)
            .timeWindow(no_data_losses_day(), end_of_no_data_losses_day())
            .build()
        )

        self.assertTrue(df.count() > 0)


class ShouldThrowOnVariablesQueryByIdWhenNoResourcesFound(VariableTest):
    def runTest(self):
        with self.assertRaises(IllegalArgumentException):
            DataQuery.builder(spark_session).variables().system(self.system).idEq(
                0
            ).timeWindow(no_data_losses_day(), end_of_no_data_losses_day()).build()


class ShouldThrowOnVariablesQueryByIdAndVariableNameWhenNoResourcesFound(VariableTest):
    def runTest(self):
        with self.assertRaises(IllegalArgumentException):
            DataQuery.builder(spark_session).variables().system(self.system).idEq(
                0
            ).nameEq("NON_EXISTING_DEVICE_NAME").timeWindow(
                no_data_losses_day(), end_of_no_data_losses_day()
            ).build()


class ShouldReturnDataWhenSearchByVariableId(VariableTest):
    def runTest(self):
        df1 = (
            DataQuery.builder(spark_session)
            .variables()
            .system(self.system)
            .idEq(self.variable.getId())
            .timeWindow(no_data_losses_day(), end_of_no_data_losses_day())
            .build()
        )

        df2 = (
            DataQuery.builder(spark_session)
            .variables()
            .idEq(self.variable.getId())
            .timeWindow(no_data_losses_day(), end_of_no_data_losses_day())
            .build()
        )

        df3 = (
            DataQuery.builder(spark_session)
            .variables()
            .system(self.system)
            .nameEq(self.variable_name)
            .timeWindow(no_data_losses_day(), end_of_no_data_losses_day())
            .build()
        )

        df1count = df1.count()

        self.assertTrue(df1count == df2.count())
        self.assertTrue(df1count == df3.count())
        self.assertTrue(df1count > 0)


class ShouldReturnDataWhenSearchByVariableIds(VariableTest):
    def runTest(self):
        df1 = (
            DataQuery.builder(spark_session)
            .variables()
            .system(self.system)
            .idIn({self.variable.getId()})
            .timeWindow(no_data_losses_day(), end_of_no_data_losses_day())
            .build()
        )

        df2 = (
            DataQuery.builder(spark_session)
            .variables()
            .idIn({self.variable.getId()})
            .timeWindow(no_data_losses_day(), end_of_no_data_losses_day())
            .build()
        )

        self.assertTrue(df1.count() == df2.count())
        self.assertTrue(df1.count() > 0)


class ShouldDoPivotByVariableNamePatterns(VariableTest):
    def runTest(self):
        df = DataQuery.getAsPivot(
            spark_session,
            no_data_losses_day(),
            end_of_no_data_losses_day(),
            self.system,
            [],
            [self.variable_name],
        )

        self.assertEqual(set(df.columns), {"nxcals_timestamp", self.variable_name})
        self.assertTrue(df.count() > 0)


class ShouldDoPivotByVariableNames(VariableTest):
    def runTest(self):
        df = DataQuery.getAsPivot(
            spark_session,
            no_data_losses_day(),
            end_of_no_data_losses_day(),
            system=self.system,
            variables=[self.variable_name],
        )

        self.assertEqual(set(df.columns), {"nxcals_timestamp", self.variable_name})
        self.assertTrue(df.count() > 0)


class ShouldExecuteAsExpanded(PySparkIntegrationTest):
    def runTest(self):
        list_of_dses = (
            DataQueryExpanded.builder(spark_session)
            .entities()
            .system(MONITORING_SYSTEM_NAME)
            .keyValuesEq({MONITORING_DEVICE_KEY: "NXCALS_MONITORING_DEV5"})
            .keyValuesEq({MONITORING_DEVICE_KEY: "NXCALS_MONITORING_DEV6"})
            .timeWindow(
                no_data_losses_day(), no_data_losses_day() + timedelta(hours=25)
            )
            .build()
        )
        self.assertTrue(isinstance(list_of_dses, list))
        self.assertTrue(len(list_of_dses) > 1)


class ShouldExtractArrayColumns(PySparkIntegrationTest):
    def runTest(self):
        df = DataQuery.getForEntities(
            spark_session,
            system=MONITORING_SYSTEM_NAME,
            start_time=no_data_losses_day(),
            end_time=end_of_no_data_losses_day(),
            entity_queries=[
                EntityQuery(
                    {MONITORING_DEVICE_KEY: "NXCALS_MONITORING_DEV6"},
                ),
            ],
        )

        field_name = "intArrayField"
        df2 = ArrayUtils.reshape(df, [field_name])
        self.assertEqual(df2.schema[field_name].dataType, ArrayType(IntegerType()))

        field_name = "floatArray2DField2"
        df3 = ArrayUtils.reshape(df, [field_name])
        self.assertEqual(
            df3.schema[field_name].dataType, ArrayType(ArrayType(FloatType()))
        )

        df4 = ArrayUtils.reshape(df)
        self.assertTrue(len(ArrayUtils._extract_array_fields(df4.schema)) == 0)