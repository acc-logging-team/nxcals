import logging
import os
import unittest
from nxcals.spark_session_builder import Flavor, get_or_create
from pyspark.sql import SparkSession

from python.nxcals.integrationtests.metadata_utils import MetadataUtils

log = logging.getLogger(__name__)

user = os.environ.get("USER")
assert isinstance(user, str)

# Please uncomment when running the tests in your local environment
# os.environ["SERVICE_URL"] = f"https://nxcals-{user}-1.cern.ch:19093"
# os.environ["KAFKA_SERVERS_URL"] = (
#     f"nxcals-{user}-3.cern.ch:9092,nxcals-{user}-4.cern.ch:9092,nxcals-{user}-5.cern.ch:9092"
# )
# os.environ["SPARK_MASTER"] = "local[*]"

# Global variable to be initialised according to the parameters, nxcals.* or spark bundle, local or yarn mode.
spark_session: SparkSession

log.info("SERVER_URL: ", os.environ.get("SERVICE_URL"))
log.info("KAFKA_SERVERS_URL: ", os.environ.get("KAFKA_SERVERS_URL"))

log.warning(
    f"Using nxcals package to create spark session: spark_master=={os.environ['SPARK_MASTER']}"
)

if os.environ["SPARK_MASTER"] == "yarn":
    log.warning("Getting spark session with yarn")

    # options needed to configure networking in container
    additional_ci_options = {}
    if "SPARK_DRIVER_HOST" in os.environ:
        additional_ci_options = {
            "spark.driver.host": os.environ["SPARK_DRIVER_HOST"],
        }

    spark_session = get_or_create(
        app_name="nxcals-python-integration-tests-builder-yarn",
        flavor=Flavor.YARN_SMALL,
        conf={"spark.executor.memory": "1g", **additional_ci_options},
    )
else:
    log.warning("Getting spark session with local[]")
    spark_session = get_or_create(
        app_name="nxcals-python-integration-tests-builder-local", flavor=None
    )


# # Check if 'SERVICE_URL' variable is set
# if 'SERVICE_URL' not in os.environ:
#     raise ValueError("Environment variable 'SERVICE_URL' is not set")
#
# # Check if 'KAFKA_SERVERS_URL' variable is set
# if 'KAFKA_SERVERS_URL' not in os.environ:
#     raise ValueError("Environment variable 'KAFKA_SERVERS_URL' is not set")


def set_java_system_property(property_name, value):
    spark_session._jvm.java.lang.System.setProperty(property_name, value)


set_java_system_property("service.url", os.environ["SERVICE_URL"])
set_java_system_property(
    "kafka.producer.bootstrap.servers", os.environ["KAFKA_SERVERS_URL"]
)
set_java_system_property("kafka.producer.linger.ms", "0")

metadata_utils = MetadataUtils(spark_session)


class PySparkIntegrationTest(unittest.TestCase):
    @classmethod
    def setUpClass(self) -> None:
        # Kept for reference in case we need it
        pass

    @classmethod
    def tearDownClass(self) -> None:
        pass
