from . import spark_session as spark, metadata_utils

from .. import (
    MONITORING_SYSTEM_NAME,
    CMW_SYSTEM_NAME,
    MONITORING_DEVICE_KEY,
    CMW_PROPERTY_KEY,
    CMW_DEVICE_KEY,
    get_random_name,
)


def test_variable_with_query_options():
    KEY_VALUES_PATTERN = f'%NXCALS_MONITORING_DEV1"%'
    variable_name_prefix = get_random_name("PYTHON_INTEGRATION_TEST")
    field = "specification"
    entity = metadata_utils.find_entity(
        MONITORING_SYSTEM_NAME, KEY_VALUES_PATTERN
    ).get()
    variables = []
    for i in range(10):
        variables.append(
            metadata_utils.create_variable(f"{variable_name_prefix}-{i}", entity, field)
        )

    try:
        _nxcals_api = spark._jvm.cern.nxcals.api
        ServiceClientFactory = _nxcals_api.extraction.metadata.ServiceClientFactory
        Variables = _nxcals_api.metadata.queries.Variables

        variable_service = ServiceClientFactory.createVariableService()

        query = (
            getattr(
                Variables.suchThat().systemName().eq(MONITORING_SYSTEM_NAME), "and"
            )()
            .variableName()
            .like(f"{variable_name_prefix}%")
            .withOptions()
            .noConfigs()
            .orderBy()
            .variableName()
            .asc()
            .limit(10)
        )

        variables = variable_service.findAll(query)

        assert len(variables) == 10
    finally:
        for variable in variables:
            metadata_utils.variable_service.delete(variable.getId())


def test_entities_with_query_options():
    _nxcals_api = spark._jvm.cern.nxcals.api
    ServiceClientFactory = _nxcals_api.extraction.metadata.ServiceClientFactory
    Entities = _nxcals_api.metadata.queries.Entities

    entity_service = ServiceClientFactory.createEntityService()
    system_service = ServiceClientFactory.createSystemSpecService()

    system_data = system_service.findByName(MONITORING_SYSTEM_NAME)

    if system_data.isEmpty():
        raise ValueError("No such system")

    key_values = {MONITORING_DEVICE_KEY: "NXCALS_MONITORING_DEV%"}

    query = (
        Entities.suchThat()
        .keyValues()
        .like(system_data.get(), key_values)
        .withOptions()
        .withHistory("2017-10-10 14:15:00.000000000", "2023-10-26 14:15:00.000000000")
        .limit(5)
        .orderBy()
        .keyValues()
        .desc()
    )

    entities = entity_service.findAll(query)

    assert len(entities) == 5


def test_update_entity():
    device_name = get_random_name("PYTHON_INTEGRATION_TEST")
    entity = metadata_utils.create_cmw_entity(CMW_SYSTEM_NAME, device_name, "property1")

    new_key_values = {CMW_DEVICE_KEY: device_name, CMW_PROPERTY_KEY: "property2"}

    new_entity = entity.toBuilder().entityKeyValues(new_key_values).build()

    updated_entities = metadata_utils.entity_service.updateEntities({new_entity})

    assert len(updated_entities) == 1
    assert set(list(updated_entities)[0].getEntityKeyValues().items()) == set(
        new_key_values.items()
    )
