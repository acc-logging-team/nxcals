import logging
import typing
from pathlib import PosixPath
from time import sleep, time

import pytest
from nxcals.api.extraction.data.builders import DataQuery
from pyspark.sql import SparkSession
from python.nxcals.integrationtests.hierarchy_details import HierarchyDetails
from python.nxcals.integrationtests.pytimber_tests import (
    END_TIME,
    HIERARCHY_BASE,
    INTERVALS_NUMBER,
    NODES_LIST,
    PYTIMBER_TEST_DEVICE,
    PYTIMBER_TEST_FUND_DEVICE,
    PYTIMBER_TEST_FUND_PROPERTY,
    PYTIMBER_TEST_PROPERTY,
    PYTIMBER_TEST_SYSTEM,
    RECORDS_NUMBER,
    SNAPSHOT_SYSTEM,
    SNAPSHOTS_LIST,
    START_TIME,
    VAR_DESCRIPTION,
    PYTIMBER_TEST_FILL_DEVICE,
    PYTIMBER_TEST_FILL_PROPERTY,
    PYTIMBER_TEST_BMODE_DEVICE,
    PYTIMBER_TEST_BMODE_PROPERTY,
    BEAM_MODES,
    PYTIMBER_TEST_DEVICE_2,
    PYTIMBER_TEST_PROPERTY_2,
)
from pytimber import LoggingDB, PageStore, SparkLoggingDB
from pytimber.utils import DEFAULT_HIERARCHY_NXCALS_SYSTEM

from python.nxcals.integrationtests import data_access
from python.nxcals.integrationtests.data_generation import DataGeneration

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

MAX_WAIT_TIME = 10


def pytest_addoption(parser: pytest.Parser) -> None:
    parser.addoption(
        "--runcore",
        action="store_true",
        default=False,
        help="run core tests only",
    )


def pytest_configure(config: pytest.Config) -> None:
    config.addinivalue_line(
        "markers",
        "core: mark test as essential for running",
    )
    config.addinivalue_line("markers", "unit: mark class for unit tests")
    config.addinivalue_line(
        "markers",
        "integration: mark class for integration tests",
    )


def pytest_collection_modifyitems(
    config: pytest.Config, items: typing.List[pytest.Item]
) -> None:
    if config.getoption("--runcore"):
        skip_not_core = pytest.mark.skip(reason="not marked as a core test")
        for item in items:
            if "core" not in item.keywords:
                item.add_marker(skip_not_core)


@pytest.fixture(scope="session")
def spark_session() -> SparkSession:
    return data_access.spark_session


@pytest.fixture(scope="session")
def data_initialized(
    spark_session: SparkSession,
) -> typing.Generator[DataGeneration, None, None]:
    data_generation = DataGeneration(
        spark_session,
        PYTIMBER_TEST_SYSTEM,
        VAR_DESCRIPTION,
        HierarchyDetails(DEFAULT_HIERARCHY_NXCALS_SYSTEM, HIERARCHY_BASE, NODES_LIST),
        SNAPSHOT_SYSTEM,
        SNAPSHOTS_LIST,
    )

    data_generation.generate_data()
    log.info(
        f"Waiting max {MAX_WAIT_TIME} minutes for the data to be visible in NXCALS..."
    )
    start = time()

    data_visible = False
    while not data_visible:
        if time() - start > 60 * MAX_WAIT_TIME:
            pytest.fail(
                f"Data was not processed in the required maxTime={MAX_WAIT_TIME} ms"
            )

        if (
            _get_count(spark_session, PYTIMBER_TEST_DEVICE, PYTIMBER_TEST_PROPERTY)
            == INTERVALS_NUMBER * RECORDS_NUMBER
            and _get_count(
                spark_session, PYTIMBER_TEST_DEVICE_2, PYTIMBER_TEST_PROPERTY_2
            )
            == INTERVALS_NUMBER
            and _get_count(
                spark_session,
                PYTIMBER_TEST_FUND_DEVICE,
                PYTIMBER_TEST_FUND_PROPERTY,
            )
            == INTERVALS_NUMBER * int((RECORDS_NUMBER + 1) / 2)
            and _get_count(
                spark_session,
                PYTIMBER_TEST_FILL_DEVICE,
                PYTIMBER_TEST_FILL_PROPERTY,
            )
            == INTERVALS_NUMBER
            and _get_count(
                spark_session,
                PYTIMBER_TEST_BMODE_DEVICE,
                PYTIMBER_TEST_BMODE_PROPERTY,
            )
            == INTERVALS_NUMBER * (len(BEAM_MODES) - 1)
        ):
            data_visible = True
        else:
            log.info("Still no data visible, waiting more...")

            sleep(1)
    log.info("Data has been published.")

    data_generation.register_variables()
    log.info("Variables are registered.")

    data_generation.generate_hierarchies()
    log.info("Hierarchies have been generated.")

    data_generation.generate_snapshots()
    log.info("Snapshots have been generated, proceeding with tests...")

    yield data_generation

    log.info("Deleting variables...")
    data_generation.delete_variables()

    log.info("Deleting hierarchies...")
    data_generation.delete_hierarchies()

    log.info("Deleting snapshots...")
    data_generation.delete_snapshots()


@pytest.fixture(scope="session")
def spark_ldb(
    spark_session: SparkSession, data_initialized: DataGeneration
) -> SparkLoggingDB:
    return SparkLoggingDB(spark_session=spark_session)


@pytest.fixture(scope="session")
def ldb(spark_session: SparkSession, data_initialized: DataGeneration) -> LoggingDB:
    return LoggingDB(spark_session=spark_session)


@pytest.fixture(scope="session")
def start_time() -> int:
    return START_TIME


@pytest.fixture(scope="session")
def end_time() -> int:
    return END_TIME


def _get_count(
    spark_session: SparkSession, device_name: str, property_name: str
) -> int:
    count: int = (
        DataQuery.builder(spark_session)
        .byEntities()
        .system(PYTIMBER_TEST_SYSTEM)
        .startTime(START_TIME)
        .endTime(END_TIME)
        .entity()
        .keyValue("device", device_name)
        .keyValue("property", property_name)
        .build()
        .count()
    )

    log.info(
        "Count for sytem %s, %s/%s from %d to %d : %d",
        PYTIMBER_TEST_SYSTEM,
        device_name,
        property_name,
        START_TIME,
        END_TIME,
        0,
    )
    return count


@pytest.fixture
def db(tmp_path: PosixPath) -> typing.Iterator[PageStore]:
    pagestore = PageStore("test.db", str(tmp_path))
    try:
        yield pagestore
    finally:
        pagestore.delete()
