from typing import Dict, List, Optional, Any

from py4j.java_gateway import JavaObject
from pyspark.sql import SparkSession


class MetadataUtils:
    def __init__(self, spark_session: SparkSession):
        nxcals_api = spark_session._jvm.cern.nxcals.api

        self.domain = nxcals_api.domain
        self.metadata = nxcals_api.extraction.metadata

        self.variable_service = (
            self.metadata.ServiceClientFactory.createVariableService()
        )
        self.entity_service = self.metadata.ServiceClientFactory.createEntityService()
        self.system_service = (
            self.metadata.ServiceClientFactory.createSystemSpecService()
        )
        self.hierarchy_service = (
            self.metadata.ServiceClientFactory.createHierarchyService()
        )
        self.group_service = self.metadata.ServiceClientFactory.createGroupService()

        self.immutable_sorted_set = (
            spark_session._jvm.com.google.common.collect.ImmutableSortedSet
        )

        self._hierarchies = self.metadata.queries.Hierarchies
        self._hierarchy = nxcals_api.domain.Hierarchy

        self._variables = self.metadata.queries.Variables

        self._groups = self.metadata.queries.Groups
        self._group_type = nxcals_api.custom.domain.GroupType
        self._group = nxcals_api.domain.Group
        self._visibility = nxcals_api.domain.Visibility

    def find_variable(self, system: str, name: str) -> JavaObject:
        query = (
            getattr(self._variables.suchThat().systemName().eq(system), "and")()
            .variableName()
            .eq(name)
        )
        return self.variable_service.findOne(query)

    def find_variables(self, system: str, variable_names: List[str]) -> JavaObject:
        return self.variable_service.findAll(
            getattr(
                getattr(
                    self._variables.suchThat().systemName().eq(system), "and"
                )().variableName(),
                "in",
            )(variable_names),
        )

    def find_variables_by_description(
        self, system: str, description: str
    ) -> JavaObject:
        query = (
            getattr(self._variables.suchThat().systemName().eq(system), "and")()
            .description()
            .eq(description)
        )
        return self.variable_service.findAll(query)

    def find_entity(self, system: str, key_value_pattern: str) -> JavaObject:
        entities = self.metadata.queries.Entities
        query = (
            getattr(entities.suchThat().systemName().eq(system), "and")()
            .keyValues()
            .like(key_value_pattern)
        )
        return self.entity_service.findOne(query)

    def create_variable(
        self,
        name: str,
        entity: JavaObject,
        field: Optional[str],
        description: Optional[str] = None,
        unit: Optional[str] = None,
        declared_type: Optional[Any] = None,
    ) -> JavaObject:
        config = (
            self.domain.VariableConfig.builder()
            .entityId(entity.getId())
            .fieldName(field)
            .validity(
                self.domain.TimeWindow.between(None, None),
            )
            .build()
        )
        variable = self.domain.Variable.builder().variableName(name)

        if description:
            variable = variable.description(description)
        if unit:
            variable = variable.unit(unit)
        if declared_type:
            variable = variable.declaredType(declared_type)

        variable = (
            variable.configs(self.immutable_sorted_set.copyOf({config}))
            .systemSpec(entity.getSystemSpec())
            .build()
        )
        return self.variable_service.create(variable)

    def recreate_variable(
        self,
        name: str,
        entity: JavaObject,
        field: Optional[str],
        description: Optional[str] = None,
        unit: Optional[str] = None,
        declared_type: Optional[str] = None,
    ) -> JavaObject:
        maybe_variable = self.find_variable(
            system=entity.getSystemSpec().getName(), name=name
        )
        if maybe_variable.isPresent():
            self.variable_service.delete(maybe_variable.get().getId())

        return self.create_variable(
            name, entity, field, description, unit, declared_type
        )

    def delete_variables_by_description(self, system: str, description: str) -> None:
        for var in self.find_variables_by_description(system, description):
            self.variable_service.delete(var.getId())

    def create_cmw_entity(
        self, system_name: str, device_name: str, property_name: str
    ) -> JavaObject:
        return self.create_entity(
            system_name,
            {"device": device_name, "property": property_name},
            {"class": "test_class", "property": property_name},
        )

    def create_entity(
        self, system_name: str, key_values: Dict[str, str], partition: Dict[str, str]
    ) -> JavaObject:
        system_spec = self._find_system_spec(system_name)
        return self.entity_service.createEntity(
            system_spec.getId(), key_values, partition
        )

    def create_hierarchy(
        self,
        system: str,
        hierarchy_base: str,
        parent_path: str,
        node_name: str,
    ) -> Optional[JavaObject]:
        system_spec = self._find_system_spec(system)

        if parent_path:
            parent = self.find_node_by_path(system, parent_path)
            if parent.isEmpty():
                raise ValueError("No such hierarchy")

            parent = parent.get()
        else:
            node = self.find_node_by_path(system, f"/{hierarchy_base}")
            if node.isEmpty():
                parent = None
            else:
                return None

        new_hierarchy = (
            self._hierarchy.builder()
            .name(node_name)
            .systemSpec(system_spec)
            .parent(parent)
            .build()
        )
        return self.hierarchy_service.create(new_hierarchy)

    def find_hierarchy_by_name(self, system: str, node_name: str) -> JavaObject:
        hierarchies = self.metadata.queries.Hierarchies
        query = (
            getattr(hierarchies.suchThat().systemName().eq(system), "and")()
            .name()
            .eq(node_name)
        )
        return self.hierarchy_service.findOne(query)

    def find_node_by_path(self, system: str, node_path: str) -> JavaObject:
        return self.hierarchy_service.findOne(
            getattr(self._hierarchies.suchThat().systemName().eq(system), "and")()
            .path()
            .eq(node_path),
        )

    def attach_variables_to_hierarchy(
        self,
        hierarchy_id: int,
        variables_system: str,
        variable_names: List[str],
    ) -> None:
        variables_list = self.find_variables(variables_system, variable_names)
        self.hierarchy_service.addVariables(
            hierarchy_id, {var.getId() for var in variables_list}
        )

    def delete_hierarchy(self, system: str, node_name: str) -> None:
        node = self.find_hierarchy_by_name(system, node_name)

        if not node.isEmpty():
            print(node.get().getName())
            self.hierarchy_service.deleteLeaf(node.get())

    def create_snapshot(
        self, system: str, snapshot_name: str, properties: Dict[str, str]
    ) -> JavaObject:
        system_spec = self._find_system_spec(system)

        snapshot = (
            self._group.builder()
            .systemSpec(system_spec)
            .name(snapshot_name)
            .label(self._group_type.SNAPSHOT.toString())
            .visibility(self._visibility.PUBLIC)
            .properties(properties)
            .description(snapshot_name)
            .build()
        )

        self.delete_snapshot(system, snapshot_name)
        return self.group_service.create(snapshot)

    def attach_variables_to_snapshot(
        self, group_id: int, variables_system: str, variable_names: List[str]
    ) -> None:
        variables_list = self.find_variables(variables_system, variable_names)
        variable_ids_per_association = {
            "getSelectedVariables": {var.getId() for var in variables_list}
        }

        self.group_service.addVariables(group_id, variable_ids_per_association)

    def delete_snapshot(self, system: str, snapshot_name: str) -> None:
        group = self.group_service.findOne(
            getattr(
                getattr(self._groups.suchThat().systemName().eq(system), "and")()
                .label()
                .eq(self._group_type.SNAPSHOT.toString()),
                "and",
            )()
            .name()
            .eq(snapshot_name),
        )

        if not group.isEmpty():
            self.group_service.delete(group.get().getId())

    def _find_system_spec(self, system_name: str) -> JavaObject:
        system_spec = self.system_service.findByName(system_name)

        if system_spec.isEmpty():
            raise ValueError(f"System {system_name} not found")
        return system_spec.get()
