import time
from dataclasses import dataclass
from typing import Dict, Any, Callable

import pytest
from py4jgw.cern.cmw.datax import ImmutableData
from py4jgw.cern.nxcals.api.domain import TimeWindow, Variable
from py4jgw.cern.nxcals.api.ingestion import PublisherFactory
from py4jgw.java.util.function import Function
from pyspark.sql import SparkSession

from python.nxcals.integrationtests import (
    data_access,
    CMW_SYSTEM_NAME,
    get_random_name,
    CMW_DEVICE_KEY,
    CMW_PROPERTY_KEY,
    CMW_CLASS_KEY,
    CMW_TIMESTAMP_KEY,
    CMW_VERSION_KEY,
)

from py4jgw.cern.nxcals.api.extraction.data.builders import DataQuery

from python.nxcals.integrationtests.metadata_utils import MetadataUtils

prefix = "TYPE_STUBS_TESTS"
INT_FIELD = "intField"
STRING_FIELD = "stringField"
VARIABLE_1_INT_FIELD = "variable_1"
VARIABLE_1_STRING_FIELD = "variable_2"
VARIABLE_2_INT_FIELD = "variable_3"
NUMBER_OF_POINTS = 20


@pytest.fixture(scope="session")
def spark_session() -> SparkSession:
    return data_access.spark_session


@pytest.fixture(scope="session")
def query_builder(spark_session) -> SparkSession:
    return DataQuery.builder(spark_session)


@pytest.fixture(scope="session")
def cmw_publisher(spark_session: SparkSession):
    return PublisherFactory.newInstance().createPublisher(
        CMW_SYSTEM_NAME, Function.identity()
    )


@dataclass
class GeneratedData:
    time_window: TimeWindow
    variables: Dict[str, Variable]


def create_cmw_record(
    device: str, prop: str, clazz: str, timestamp_nanos: int, fields: Dict[str, Any]
):
    builder = ImmutableData.builder()
    builder.add(CMW_DEVICE_KEY, device, None)
    builder.add(CMW_PROPERTY_KEY, prop, None)
    builder.add(CMW_CLASS_KEY, clazz, None)
    builder.add(CMW_TIMESTAMP_KEY, timestamp_nanos, None)
    builder.add(CMW_VERSION_KEY, 1_000_000_000_000_000_000, None)

    for key, value in fields.items():
        builder.add(key, value, None)

    return builder.build()


@pytest.fixture(scope="session")
def data_generated(cmw_publisher, spark_session: SparkSession) -> GeneratedData:
    # For pivot
    device_name = get_random_name(prefix)
    property_1_name = get_random_name(prefix)
    property_2_name = get_random_name(prefix)
    clazz = get_random_name(prefix)

    now = time.time_ns()

    for i in range(NUMBER_OF_POINTS):
        record_property_1 = create_cmw_record(
            device_name,
            property_1_name,
            clazz,
            now + i,
            {INT_FIELD: i, STRING_FIELD: str(i)},
        )

        cmw_publisher.publish(record_property_1)

        if i % 2 == 0:
            record_property_2 = create_cmw_record(
                device_name, property_2_name, clazz, now + i, {INT_FIELD: i}
            )
            cmw_publisher.publish(record_property_2)

    time_window = TimeWindow.between(now, now + NUMBER_OF_POINTS)

    metadata_utils = MetadataUtils(spark_session)

    entity_1_int_field_variable = create_variable(
        metadata_utils, property_1_name, INT_FIELD
    )

    entity_1_string_field_variable = create_variable(
        metadata_utils, property_1_name, STRING_FIELD
    )

    entity_2_int_field_variable = create_variable(
        metadata_utils, property_2_name, INT_FIELD
    )

    wait_until(
        lambda: DataQuery.getFor(
            spark_session, time_window, entity_1_int_field_variable
        ).count()
        == NUMBER_OF_POINTS
    )

    wait_until(
        lambda: DataQuery.getFor(
            spark_session, time_window, entity_2_int_field_variable
        ).count()
        == int(NUMBER_OF_POINTS / 2)
    )

    return GeneratedData(
        time_window,
        {
            VARIABLE_1_INT_FIELD: entity_1_int_field_variable,
            VARIABLE_1_STRING_FIELD: entity_1_string_field_variable,
            VARIABLE_2_INT_FIELD: entity_2_int_field_variable,
        },
    )


def create_variable(
    metadata_utils: MetadataUtils, key_or_value_pattern: str, field: str
):
    return metadata_utils.create_variable(
        get_random_name(prefix),
        metadata_utils.find_entity(CMW_SYSTEM_NAME, f"%{key_or_value_pattern}%").get(),
        field,
    )


def wait_until(provider: Callable[[], bool], timeout_after: int = 120):
    counter = 0
    while not provider():
        if counter > timeout_after:
            raise RuntimeError("No data")

        counter = counter + 1
        time.sleep(1)
