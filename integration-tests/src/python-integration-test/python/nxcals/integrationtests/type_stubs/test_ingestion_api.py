import time

import pytest
from py4jgw.cern.nxcals.api.extraction.data.builders import DataQuery
from pyspark.sql import SparkSession

from python.nxcals.integrationtests import (
    get_random_name,
    CMW_SYSTEM_NAME,
    CMW_DEVICE_KEY,
    CMW_PROPERTY_KEY,
)
from python.nxcals.integrationtests.type_stubs import NAME_PREFIX
from python.nxcals.integrationtests.type_stubs.conftest import create_cmw_record


def test_publishing_data(cmw_publisher):
    device = get_random_name(NAME_PREFIX)
    prop = get_random_name(NAME_PREFIX)
    clazz = get_random_name(NAME_PREFIX)

    record = create_cmw_record(device, prop, clazz, time.time_ns(), {"int_field": 1})

    result = cmw_publisher.publish(record)
    assert result is not None


@pytest.mark.timeout(120)
def test_publishing_data_and_getting_data(spark_session: SparkSession, cmw_publisher):
    device = get_random_name(NAME_PREFIX)
    prop = get_random_name(NAME_PREFIX)
    clazz = get_random_name(NAME_PREFIX)
    timestamp = time.time_ns()

    record = create_cmw_record(device, prop, clazz, timestamp, {"int_field": 1})
    cmw_publisher.publish(record)

    while True:
        dataset = (
            DataQuery.builder(spark_session)
            .entities()
            .system(CMW_SYSTEM_NAME)
            .keyValuesEq({CMW_DEVICE_KEY: device, CMW_PROPERTY_KEY: prop})
            .timeWindow(timestamp, timestamp)
            .build()
        )

        if dataset.count() >= 1:
            return

        time.sleep(1)
