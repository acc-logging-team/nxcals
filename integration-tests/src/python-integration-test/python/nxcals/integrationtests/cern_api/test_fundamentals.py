import pytest
from pyspark.sql import SparkSession

from nxcals.api.cern.data.services import ServiceFactory, FundamentalService
from python.nxcals.integrationtests.pytimber_tests import (
    RANDOM_SUFFIX,
    END_TIME,
    START_TIME,
)
from python.nxcals.integrationtests.data_generation import DataGeneration
import pyspark


class TestFundamentals:
    def test_creating_service(self, spark_session: SparkSession) -> None:
        ServiceFactory.fundamentals_service(spark_session)

    def test_extract_with_fundamentals_for_variables_list(
        self, spark_session: SparkSession, data_initialized: DataGeneration
    ):
        service: FundamentalService = ServiceFactory.fundamentals_service(spark_session)
        dataset = service.get_for(START_TIME, END_TIME, [f"ACC_{RANDOM_SUFFIX}"])

        data = dataset.toPandas()
        assert len(data) > 0
        expected_columns = {
            "DEST",
            "LSA_CYCLE",
            "USER",
            "nxcals_timestamp",
            "__record_version__",
            "class",
            "device",
            "property",
            "nxcals_entity_id",
            "nxcals_variable_name",
        }
        assert expected_columns == set(dataset.columns)

    def test_if_throw_incorrect_accelerator(
        self, spark_session: SparkSession, data_initialized: DataGeneration
    ):
        service: FundamentalService = ServiceFactory.fundamentals_service(spark_session)
        pyspark.errors.exceptions.captured.IllegalArgumentException
        with pytest.raises(ValueError):
            service.get_for(START_TIME, END_TIME, [f"ACC_Non_existing"])

    def test_if_throw_if_no_accelerator(
        self, spark_session: SparkSession, data_initialized: DataGeneration
    ):
        service: FundamentalService = ServiceFactory.fundamentals_service(spark_session)
        with pytest.raises(ValueError):
            service.get_for(START_TIME, END_TIME, [])

    def test_if_throw_if_one_accelerator_was_not_found(
        self, spark_session: SparkSession, data_initialized: DataGeneration
    ):
        service: FundamentalService = ServiceFactory.fundamentals_service(spark_session)
        with pytest.raises(ValueError):
            service.get_for(
                START_TIME, END_TIME, [f"ACC_{RANDOM_SUFFIX}", "non_existing"]
            )
