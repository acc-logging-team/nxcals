package cern.nxcals.integrationtests.auth;

import cern.nxcals.api.config.SparkProperties;
import cern.nxcals.api.utils.SparkUtils;
import cern.rbac.client.authentication.AuthenticationException;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;

public class AuthorizationTestLocal extends AuthorizationTest {

    static {
        // Uncomment this to run in the IDE
        // System.setProperty("NXCALS_RBAC_AUTH", "true");
        // String username = System.getProperty("user.name");
        // System.setProperty("service.url", "https://nxcals-" + username + "1.cern.ch:19093");

        // Uncomment this to run in the IDE with a user keytab
        // System.setProperty("kerberos.principal", username);
        // System.setProperty("kerberos.keytab", System.getProperty("user.home") + "/.keytab");

        // Uncomment this to run in the IDE with an acclog keytab
        // System.setProperty("kerberos.principal", "acclog");
        // System.setProperty("kerberos.keytab", "/opt/" + username + "/.keytab-acclog");
    }

    @BeforeAll
    private static void loginBefore() throws IOException, AuthenticationException {
        AuthorizationTest.loginRbac();
    }

    @Test
    public void shouldAccessHBaseInLocal() {
        SparkSession localSession = getLocalSparkSession();
        extractHBaseDataAndVerify(localSession);
    }

    @Test
    public void shouldAccessHdfsInLocal() {
        SparkSession localSession = getLocalSparkSession();
        extractHdfsDataAndVerify(localSession);
    }

    private SparkSession getLocalSparkSession() {
        SparkProperties properties = SparkProperties.defaults("testApp");
        return SparkUtils.createSparkSession(properties);
    }
}
