package cern.nxcals.api.extraction.metadata.security;

import feign.Request;

import java.util.Optional;
import java.util.function.Function;

public interface AuthTokenProvider extends Function<Request, Optional<String>> {
}
