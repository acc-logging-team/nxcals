package cern.nxcals.api.extraction.metadata.feign;

import cern.nxcals.api.domain.PartitionResource;
import feign.Body;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

import java.util.Set;

import static cern.nxcals.common.web.Endpoints.PARTITION_RESOURCES;
import static cern.nxcals.common.web.Endpoints.PARTITION_RESOURCES_FIND_ALL;
import static cern.nxcals.common.web.HttpHeaders.ACCEPT_APPLICATION_JSON;
import static cern.nxcals.common.web.HttpHeaders.CONTENT_TYPE_APPLICATION_JSON;
import static cern.nxcals.common.web.HttpHeaders.CONTENT_TYPE_TEXT_PLAIN;
import static cern.nxcals.common.web.HttpVerbs.DELETE;
import static cern.nxcals.common.web.HttpVerbs.POST;
import static cern.nxcals.common.web.HttpVerbs.PUT;

public interface PartitionResourceClient extends FeignQuerySupport<PartitionResource> {
    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_TEXT_PLAIN })
    @RequestLine(POST + PARTITION_RESOURCES_FIND_ALL)
    @Body("{condition}")
    Set<PartitionResource> findAll(@Param("condition") String condition);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(value = PUT + PARTITION_RESOURCES)
    PartitionResource update(PartitionResource partitionResource);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(POST + PARTITION_RESOURCES)
    PartitionResource create(PartitionResource partitionResource);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(DELETE + PARTITION_RESOURCES)
    void delete(Long partitionResourceId);

}
