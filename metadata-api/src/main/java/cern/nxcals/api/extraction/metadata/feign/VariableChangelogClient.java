/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.api.extraction.metadata.feign;

import cern.nxcals.api.domain.VariableChangelog;
import feign.Body;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

import java.util.Set;

import static cern.nxcals.common.web.Endpoints.VARIABLE_CHANGELOGS_FIND_ALL;
import static cern.nxcals.common.web.HttpHeaders.ACCEPT_APPLICATION_JSON;
import static cern.nxcals.common.web.HttpHeaders.CONTENT_TYPE_TEXT_PLAIN;
import static cern.nxcals.common.web.HttpVerbs.POST;

/**
 * Feign declarative service interface for consuming VariableChangelog service.
 */
@SuppressWarnings("squid:UndocumentedApi")
public interface VariableChangelogClient extends FeignQuerySupport<VariableChangelog> {

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_TEXT_PLAIN })
    @RequestLine(POST + VARIABLE_CHANGELOGS_FIND_ALL)
    @Body("{condition}")
    Set<VariableChangelog> findAll(@Param("condition") String condition);

}
