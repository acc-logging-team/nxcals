package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.domain.HierarchyVariablesChangelog;
import cern.nxcals.api.extraction.metadata.feign.HierarchyVariablesChangelogClient;
import cern.nxcals.api.extraction.metadata.queries.HierarchyVariablesChangelogs;
import cern.nxcals.internal.extraction.metadata.InternalHierarchyVariablesChangelogService;

import java.util.Optional;

/**
 * Implementation of {@link InternalHierarchyVariablesChangelogService} for getting {@link HierarchyVariablesChangelog} objects
 */
public class HierarchyVariablesChangelogProvider
        extends AbstractProvider<HierarchyVariablesChangelog, HierarchyVariablesChangelogClient, HierarchyVariablesChangelogs>
        implements InternalHierarchyVariablesChangelogService {

    HierarchyVariablesChangelogProvider(HierarchyVariablesChangelogClient httpClient) {
        super(httpClient);
    }

    @Override
    public Optional<HierarchyVariablesChangelog> findById(long id) {
        return findOne(HierarchyVariablesChangelogs.suchThat().id().eq(id));
    }

}
