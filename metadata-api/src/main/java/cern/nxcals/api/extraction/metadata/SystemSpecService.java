/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.extraction.metadata.queries.SystemSpecs;

import java.util.Optional;
import java.util.Set;

/**
 * SystemData provider Service interface.
 */
//Methods are self-descriptive
@SuppressWarnings("squid:UndocumentedApi")
public interface SystemSpecService extends Queryable<SystemSpec, SystemSpecs> {
    Optional<SystemSpec> findByName(String name);
    Set<SystemSpec> findAll();
}