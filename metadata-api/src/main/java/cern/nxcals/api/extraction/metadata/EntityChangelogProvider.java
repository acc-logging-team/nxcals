/*
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.domain.EntityChangelog;
import cern.nxcals.api.extraction.metadata.feign.EntityChangelogClient;
import cern.nxcals.api.extraction.metadata.queries.EntityChangelogs;
import cern.nxcals.internal.extraction.metadata.InternalEntityChangelogService;

import java.util.Optional;

/**
 * Implementation of {@link EntityChangelogService} for getting {@link EntityChangelog} objects
 */
class EntityChangelogProvider extends AbstractProvider<EntityChangelog, EntityChangelogClient, EntityChangelogs>
        implements InternalEntityChangelogService {

    EntityChangelogProvider(EntityChangelogClient httpClient) {
        super(httpClient);
    }

    @Override
    public Optional<EntityChangelog> findById(long id) {
        return findOne(EntityChangelogs.suchThat().id().eq(id));
    }
}