/*
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.domain.VariableConfigChangelog;
import cern.nxcals.api.extraction.metadata.feign.VariableConfigChangelogClient;
import cern.nxcals.api.extraction.metadata.queries.VariableConfigChangelogs;
import cern.nxcals.internal.extraction.metadata.InternalVariableConfigChangelogService;

import java.util.Optional;

/**
 * Implementation of {@link VariableConfigChangelogService} for getting {@link VariableConfigChangelog} objects
 */
class VariableConfigChangelogProvider extends AbstractProvider<VariableConfigChangelog, VariableConfigChangelogClient, VariableConfigChangelogs>
        implements InternalVariableConfigChangelogService {

        VariableConfigChangelogProvider(VariableConfigChangelogClient httpClient) {
        super(httpClient);
    }

    @Override
    public Optional<VariableConfigChangelog> findById(long id) {
        return findOne(VariableConfigChangelogs.suchThat().id().eq(id));
    }
}