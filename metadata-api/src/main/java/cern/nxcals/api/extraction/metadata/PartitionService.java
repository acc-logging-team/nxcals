/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.domain.Partition;
import cern.nxcals.api.extraction.metadata.queries.Partitions;

public interface PartitionService  extends Queryable<Partition, Partitions> {
}