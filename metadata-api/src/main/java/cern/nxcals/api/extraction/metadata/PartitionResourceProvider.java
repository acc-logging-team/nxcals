package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.domain.PartitionResource;
import cern.nxcals.api.extraction.metadata.feign.PartitionResourceClient;
import cern.nxcals.api.extraction.metadata.queries.PartitionResources;
import cern.nxcals.internal.extraction.metadata.InternalPartitionResourceService;

import java.util.Optional;

class PartitionResourceProvider extends AbstractProvider<PartitionResource, PartitionResourceClient, PartitionResources>
        implements InternalPartitionResourceService {

    PartitionResourceProvider(PartitionResourceClient httpClient) {
        super(httpClient);
    }

    @Override
    public Optional<PartitionResource> findById(long id) {
        return findOne(PartitionResources.suchThat().id().eq(id));
    }

    @Override
    public PartitionResource create(PartitionResource partitionResource) {
        return getHttpClient().create(partitionResource);
    }

    @Override
    public PartitionResource update(PartitionResource partitionResource) {
        return getHttpClient().update(partitionResource);
    }

    @Override
    public void delete(long partitionResourceId){
        getHttpClient().delete(partitionResourceId);
    }

}
