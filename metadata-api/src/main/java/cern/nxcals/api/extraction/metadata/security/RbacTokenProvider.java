package cern.nxcals.api.extraction.metadata.security;

import cern.rbac.common.RbaToken;
import cern.rbac.util.lookup.RbaTokenLookup;
import feign.Request;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;

import java.util.Base64;
import java.util.Optional;

@Slf4j
@Builder
public class RbacTokenProvider implements AuthTokenProvider {
    @Override
    public Optional<String> apply(Request request) {
        log.debug("Trying authenticate using RBAC...");
        try {
            RbaToken rbaToken = RbaTokenLookup.findRbaToken();
            if (rbaToken != null) {
                log.debug("Using RBAC, token found {}", rbaToken);
                if (rbaToken.isValid()) {
                    return Optional.of(Base64.getEncoder().encodeToString(rbaToken.getEncoded()));
                }
                log.warn("Provided RBAC token has expired! Please, renew the token if you intent to use RBAC.");
            }
            return Optional.empty();
        } catch (Exception ex) {
            log.debug("Cannot encode RBAC token", ex);
            throw new IllegalStateException("Cannot encode RBAC token", ex);
        }
    }

    @Override
    public String toString() {
        return "RBAC";
    }
}
