package cern.nxcals.api.extraction.metadata.security;

/**
 * Paths in configuration enclosed in enum.
 *
 * @author wjurasz
 */
public enum PropertiesKeys {
    SERVICE_URL_CONFIG("service.url"),
    SERVICE_CONNECT_TIMEOUT_CONFIG("service.connectTimeout"),
    SERVICE_READ_TIMEOUT_CONFIG("service.readTimeout"),
    USER_PRINCIPAL_PATH("kerberos.principal"),
    USER_KEYTAB_PATH_CONFIG("kerberos.keytab"),
    MAX_RETRIES("service.maxRetries");


    private String pathInProperties;

    PropertiesKeys(String pathInProperties) {
        this.pathInProperties = pathInProperties;
    }

    public String getPathInProperties() {
        return pathInProperties;
    }
}
