/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.domain.VariableConfigChangelog;
import cern.nxcals.api.extraction.metadata.queries.VariableConfigChangelogs;

public interface VariableConfigChangelogService extends Queryable<VariableConfigChangelog, VariableConfigChangelogs> {
}