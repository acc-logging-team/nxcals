/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.domain.CreateEntityRequest;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.exceptions.FatalDataConflictRuntimeException;
import cern.nxcals.api.extraction.metadata.queries.Entities;
import cern.nxcals.api.metadata.queries.ConditionWithOptions;
import cern.nxcals.api.metadata.queries.EntityQueryWithOptions;
import cern.nxcals.common.annotation.Experimental;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * Public access to NXCALS entities.
 */
public interface EntityService extends Queryable<Entity, Entities> {

    /**
     * Searching for multiple values.
     *
     * @param condition entity search criteria
     * @param historyStartTime in nanoseconds elapsed since 1970-01-01 00:00:00 in the UTC timezone
     * @param historyEndTime in nanoseconds elapsed since 1970-01-01 00:00:00 in the UTC timezone
     * @return a {@link Set} of found entities.
     */
    Set<Entity> findAllWithHistory(Condition<Entities> condition, long historyStartTime, long historyEndTime);

    /**
     * Searching for one value. If the metadata returns multiple values the exception is thrown (@see {@link IllegalStateException})
     *
     *
     * @param condition entity search criteria
     * @param historyStartTime in nanoseconds elapsed since 1970-01-01 00:00:00 in the UTC timezone
     * @param historyEndTime in nanoseconds elapsed since 1970-01-01 00:00:00 in the UTC timezone
     * @return Optional with value or empty.
     * @throws IllegalStateException if found multiple values.
     */
    Optional<Entity> findOneWithHistory(Condition<Entities> condition, long historyStartTime, long historyEndTime);

    /**
     * Updates a set of entities.
     * NOTE: this is a mutative method. It is not thread-safe or meant to be used in parallel
     *
     * @param entityDataList a {@link Set} of {@link Entity}s that identify the entities to be updated.
     *                       We only allow change of the keyValues and lock/unlock for operation.
     * @return a {@link Set} of updated entities.
     */
    Set<Entity> updateEntities(Set<Entity> entityDataList);

    /**
     * Creates an entity for the given input criteria.
     * NOTE: this is a mutative method. It is not thread-safe or meant to be used in parallel
     *
     * @param systemId
     * @param entityKey
     * @param partitionKey
     * @return created entity
     * @throws FatalDataConflictRuntimeException if such entity exists
     * @throws IllegalArgumentException if systemId does not exist or entityKey or partitionKey are invalid
     */
    Entity createEntity(long systemId, Map<String, Object> entityKey, Map<String, Object> partitionKey);

    /**
     * Creates entities for the given input requests.
     * NOTE: this is a mutative method. It is not thread-safe or meant to be used in parallel
     *
     * @param entities a set of {@link CreateEntityRequest} based on which new entities should be created
     * @return a {@link Set} of created entities
     * @throws FatalDataConflictRuntimeException if such entity exists
     * @throws IllegalArgumentException          if systemId does not exist or entityKeyValues or partitionKeyValues are invalid
     */
    Set<Entity> createEntities(Set<CreateEntityRequest> entities);

    /**
     * Find all entities which match condition. Can be configured with additional options.
     *
     * @param options query with options, like limit
     * @return a {@link List} of found entities.
     */
    @Experimental
    List<Entity> findAll(EntityQueryWithOptions options);

    /**
     * Find one entity which match condition. Can be configured with additional options.
     *
     * @param options query with options, like limit
     * @return an {@link Optional} with {@link Entity}
     */
    @Experimental
    Optional<Entity> findOne(EntityQueryWithOptions options);

    /**
     * Find all entities which match condition.
     *
     * @param condition query
     * @return a {@link List} of found entities.
     */
    @Experimental
    Set<Entity> findAll(
            ConditionWithOptions<cern.nxcals.api.metadata.queries.Entities, EntityQueryWithOptions> condition);

    /**
     * Find one entity which match condition.
     *
     * @param condition query, must match zero or one entity
     * @return an {@link Optional} with {@link Entity}
     */
    @Experimental
    Optional<Entity> findOne(
            ConditionWithOptions<cern.nxcals.api.metadata.queries.Entities, EntityQueryWithOptions> condition);
}
