package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.domain.Group;
import cern.nxcals.api.extraction.metadata.queries.Groups;

public interface GroupService extends AbstractGroupService<Group,Groups> {

}
