/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.api.extraction.metadata.feign;

import static cern.nxcals.common.web.Endpoints.AUTHORIZATION_TOKENS;
import static cern.nxcals.common.web.HttpVerbs.GET;

import cern.nxcals.api.custom.domain.DelegationToken;
import feign.RequestLine;

/**
 * Feign declarative service interface for consuming Authorization service.
 */
@SuppressWarnings("squid:UndocumentedApi")
public interface AuthorizationClient {

    @RequestLine(GET + AUTHORIZATION_TOKENS)
    DelegationToken createDelegationToken();

}
