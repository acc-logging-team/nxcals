package cern.nxcals.api.extraction.metadata.security.kerberos;

import cern.nxcals.api.extraction.metadata.security.KerberosAuthenticationException;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.ietf.jgss.GSSContext;
import org.ietf.jgss.GSSCredential;
import org.ietf.jgss.GSSException;
import org.ietf.jgss.GSSManager;
import org.ietf.jgss.GSSName;
import org.ietf.jgss.Oid;

import javax.security.auth.Subject;
import javax.security.auth.login.AppConfigurationEntry;
import javax.security.auth.login.Configuration;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.security.Principal;
import java.security.PrivilegedAction;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class GssKerberosContext implements KerberosContext {
    /*
        SPNEGO (Simple and Protected GSSAPI Negotiation Mechanism) is a mechanism used to negotiate security protocol.
        In our case we are using it to establish Kerberos based connection.
        This oid corresponds to SPNEGO mechanism.
    */
    private static final String SPNEGO_OID_VERSION = "1.3.6.1.5.5.2";
    private static final int RENEWAL_MARGIN_SECONDS = 1000;
    private final Oid oid;
    private final ClientLoginConfig loginConfig;
    private final GSSManager manager;

    private Subject subject;
    private GSSName clientName;
    private GSSCredential clientCred;

    public GssKerberosContext(String userPrincipal, String keytabLocation,
            Map<String, Object> loginOptions) {
        verifyKeytab(keytabLocation);

        this.manager = GSSManager.getInstance();
        this.oid = initOid();

        this.loginConfig = new ClientLoginConfig(userPrincipal, keytabLocation, loginOptions);

        logon();

        log.debug(" ClientName: {}, UserPrincipal: {}, KeytabLocation: {}", this.clientName, userPrincipal, keytabLocation);
    }

    private void verifyKeytab(String keytabLocation) {
        if (!Strings.isNullOrEmpty(keytabLocation) && !fileExists(keytabLocation)) {
            throw logAndWrap("Keytab file not found: " + keytabLocation);
        }
    }

    private boolean fileExists(String keytab) {
        return Paths.get(keytab).toFile().exists();
    }

    private Oid initOid() {
        try {
            return new Oid(SPNEGO_OID_VERSION);
        } catch (GSSException gssException) {
            throw logAndWrap("Error while creating Oid: " + SPNEGO_OID_VERSION, gssException);
        }
    }

    private void logon() {
        try {

            /*
            Note that we reuse the subject. The consequence is that if we change the user data via
            environment variables it will not be taken into account until the app relaunches
            */
            LoginContext lc = new LoginContext("", new Subject(), null, loginConfig);
            lc.login();
            this.subject = lc.getSubject();
            this.clientName = Subject.doAs(subject, (PrivilegedAction<GSSName>) this::initClientName);
            this.clientCred = Subject.doAs(subject, (PrivilegedAction<GSSCredential>) this::initClientCred);

        } catch (LoginException loginException) {
            throw logAndWrap("Couldn't log in to Kerberos", loginException);
        }
    }

    private void tryRelogon() {
        try {
            if (clientCred.getRemainingLifetime() < RENEWAL_MARGIN_SECONDS) {
                synchronized (this) {
                    if (clientCred.getRemainingLifetime() < RENEWAL_MARGIN_SECONDS) {
                        logon();
                    }
                }
            }
        } catch (GSSException e) {
            throw logAndWrap("Error renewing client credentials ", e);
        }
    }

    private GSSName initClientName() {
        try {
            Principal principal = subject.getPrincipals()
                    .stream()
                    .findFirst()
                    .orElseThrow(()-> new KerberosAuthenticationException("Error while acquiring ticket for service communication. No user principal found"));
            return manager.createName(principal.getName(), GSSName.NT_USER_NAME);
        } catch (GSSException e) {
            throw logAndWrap("Error creating client principal ", e);
        }
    }

    private GSSCredential initClientCred() {
        try {
            return manager
                    .createCredential(clientName, GSSCredential.DEFAULT_LIFETIME, oid, GSSCredential.INITIATE_ONLY);
        } catch (GSSException e) {
            throw logAndWrap("Error creating client credentials ", e);
        }
    }

    private KerberosAuthenticationException logAndWrap(String message) {
        log.error(message);
        return new KerberosAuthenticationException(message);
    }

    private KerberosAuthenticationException logAndWrap(String message, Exception ex) {
        log.error(message);
        return new KerberosAuthenticationException(message, ex);
    }

    @Override
    public String requestTokenFor(String servicePrincipal) {
        log.debug("Kerberos execute start");

        tryRelogon();

        return Subject.doAs(subject, (PrivilegedAction<String>) () -> {
                    try {
                        log.info("Initializing secure context with: {}", servicePrincipal);

                        GSSName serverName = manager.createName(servicePrincipal, GSSName.NT_HOSTBASED_SERVICE);
                        GSSContext context = establishContext(serverName);

                        byte[] outToken = context.initSecContext(new byte[0], 0, 0);

                        log.debug("Kerberos execution successful");

                        return convertToToken(outToken);

                    } catch (GSSException gssException) {
                        throw logAndWrap("Error while acquiring ticket for service communication", gssException);
                    }
                }
        );
    }

    private String convertToToken(byte[] outToken) {
        return new String(Base64.getEncoder().encode(outToken), StandardCharsets.UTF_8).replace("\n", "");
    }

    private GSSContext establishContext(GSSName serverName) throws GSSException {
        //Create context for client-service connection
        GSSContext context = manager.createContext(serverName, oid, clientCred, GSSContext.DEFAULT_LIFETIME);

        //Client validates service, service validates client. Both sides of connection checks if request is valid.
        context.requestMutualAuth(true);
        //We are not encrypting the message, SSL takes care of that.
        context.requestConf(false);
        //Create checksum for message
        context.requestInteg(true);

        return context;
    }

    private static class ClientLoginConfig extends Configuration {
        private static final String MODULE_NAME = "com.sun.security.auth.module.Krb5LoginModule";

        private final AppConfigurationEntry config;
        private final String userPrincipal;
        private final String keytabLocation;

        private ClientLoginConfig(String userPrincipal, String keytabLocation, Map<String, Object> loginOptions) {
            super();

            this.userPrincipal = userPrincipal;
            this.keytabLocation = keytabLocation;

            Map<String, Object> configMap = new HashMap<>();
            //We do not support password auth.
            configMap.put("doNotPrompt", "true");
            //If it is possible, use cached ticket.
            configMap.put("useTicketCache", "true");
            //In case there are some changes coming from System properties, we want to refresh config.
            configMap.put("refreshKrb5Config", "true");
            //If the ticket expires, renew it.
            configMap.put("renewTGT", "true");
            // LoginModule retrieves the username and password from the module's shared
            // state using "javax.security.auth.login.name" and "javax.security.auth.login.password" as the respective keys.
            // The retrieved values are used for authentication.
            configMap.put("useFirstPass", "true");

            //If the principal is provided use it, otherwise java will try to acquire one from cache.
            if (StringUtils.isNotEmpty(userPrincipal)) {
                configMap.put("principal", userPrincipal);
            }

            //If the cached ticket is not valid or it does not exist, new one will be acquired from KDC using keyTab.
            if (StringUtils.isNotEmpty(keytabLocation)) {
                configMap.put("useKeyTab", "true");
                configMap.put("keyTab", keytabLocation);
            }

            if (loginOptions != null) {
                configMap.putAll(loginOptions);
            }

            this.config = new AppConfigurationEntry(MODULE_NAME, AppConfigurationEntry.LoginModuleControlFlag.REQUIRED,
                    configMap);
        }

        String getUserPrincipal() {
            return userPrincipal;
        }

        String getKeytabLocation() {
            return keytabLocation;
        }

        @Override
        public AppConfigurationEntry[] getAppConfigurationEntry(String name) {
            return new AppConfigurationEntry[] { config };
        }
    }
}
