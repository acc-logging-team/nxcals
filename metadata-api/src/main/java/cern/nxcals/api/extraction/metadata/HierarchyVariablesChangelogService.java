package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.domain.HierarchyVariablesChangelog;
import cern.nxcals.api.extraction.metadata.queries.HierarchyVariablesChangelogs;

public interface HierarchyVariablesChangelogService extends Queryable<HierarchyVariablesChangelog, HierarchyVariablesChangelogs> {

}
