package cern.nxcals.api.extraction.metadata.feign;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.Group;
import cern.nxcals.api.domain.Variable;
import feign.Body;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

import java.util.Map;
import java.util.Set;

import static cern.nxcals.common.web.Endpoints.ENTITIES;
import static cern.nxcals.common.web.Endpoints.GROUPS;
import static cern.nxcals.common.web.Endpoints.GROUPS_FIND_ALL;
import static cern.nxcals.common.web.Endpoints.IDS_SUFFIX;
import static cern.nxcals.common.web.Endpoints.VARIABLES;
import static cern.nxcals.common.web.HttpHeaders.ACCEPT_APPLICATION_JSON;
import static cern.nxcals.common.web.HttpHeaders.CONTENT_TYPE_APPLICATION_JSON;
import static cern.nxcals.common.web.HttpHeaders.CONTENT_TYPE_TEXT_PLAIN;
import static cern.nxcals.common.web.HttpVerbs.DELETE;
import static cern.nxcals.common.web.HttpVerbs.GET;
import static cern.nxcals.common.web.HttpVerbs.POST;
import static cern.nxcals.common.web.HttpVerbs.PUT;

@SuppressWarnings("squid:UndocumentedApi")
public interface GroupClient extends FeignQuerySupport<Group> {
    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_TEXT_PLAIN })
    @RequestLine(POST + GROUPS_FIND_ALL)
    @Body("{condition}")
    @Override
    Set<Group> findAll(@Param("condition") String condition);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(POST + GROUPS)
    Group create(Group group);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(PUT + GROUPS)
    Group update(Group group);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(value = DELETE + GROUPS + "/{id}", decodeSlash = false)
    void delete(@Param("id") long groupId);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(value = GET + GROUPS + "/{id}" + VARIABLES, decodeSlash = false)
    Map<String, Set<Variable>> getVariables(@Param("id") long groupId);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(value = PUT + GROUPS + "/{id}" + VARIABLES + IDS_SUFFIX, decodeSlash = false)
    void setVariables(@Param("id") long groupId, Map<String, Set<Long>> variableIdsPerAssociation);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(value = POST + GROUPS + "/{id}" + VARIABLES + IDS_SUFFIX, decodeSlash = false)
    void addVariables(@Param("id") long groupId, Map<String, Set<Long>> variableIdsPerAssociation);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(value = DELETE + GROUPS + "/{id}" + VARIABLES + IDS_SUFFIX, decodeSlash = false)
    void removeVariables(@Param("id") long groupId, Map<String, Set<Long>> variableIdsPerAssociation);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(value = GET + GROUPS + "/{id}" + ENTITIES, decodeSlash = false)
    Map<String, Set<Entity>> getEntities(@Param("id") long groupId);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(value = PUT + GROUPS + "/{id}" + ENTITIES + IDS_SUFFIX, decodeSlash = false)
    void setEntities(@Param("id") long groupId, Map<String, Set<Long>> entityIdsPerAssociation);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(value = POST + GROUPS + "/{id}" + ENTITIES + IDS_SUFFIX, decodeSlash = false)
    void addEntities(@Param("id") long groupId, Map<String, Set<Long>> entityIdsPerAssociation);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(value = DELETE + GROUPS + "/{id}" + ENTITIES + IDS_SUFFIX, decodeSlash = false)
    void removeEntities(@Param("id") long groupId, Map<String, Set<Long>> entityIdsPerAssociation);

}
