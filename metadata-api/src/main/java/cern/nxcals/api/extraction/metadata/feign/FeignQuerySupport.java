package cern.nxcals.api.extraction.metadata.feign;

import java.util.Set;

public interface FeignQuerySupport<S> {
    /**
     * Searching for multiple values.
     *
     * @param condition
     *
     * @return
     */
    Set<S> findAll(String condition);


}