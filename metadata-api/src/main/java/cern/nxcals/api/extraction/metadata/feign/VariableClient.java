/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.api.extraction.metadata.feign;

import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.common.annotation.Experimental;
import feign.Body;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static cern.nxcals.common.web.Endpoints.BATCH;
import static cern.nxcals.common.web.Endpoints.VARIABLES;
import static cern.nxcals.common.web.Endpoints.VARIABLES_FIND_ALL;
import static cern.nxcals.common.web.Endpoints.VARIABLES_FIND_ALL_WITH_OPTIONS;
import static cern.nxcals.common.web.Endpoints.VARIABLE_FIND_SCHEMAS;
import static cern.nxcals.common.web.HttpHeaders.ACCEPT_APPLICATION_JSON;
import static cern.nxcals.common.web.HttpHeaders.CONTENT_TYPE_APPLICATION_JSON;
import static cern.nxcals.common.web.HttpHeaders.CONTENT_TYPE_TEXT_PLAIN;
import static cern.nxcals.common.web.HttpVerbs.DELETE;
import static cern.nxcals.common.web.HttpVerbs.POST;
import static cern.nxcals.common.web.HttpVerbs.PUT;

/**
 * Feign declarative service interface for consuming Variable service.
 */
@SuppressWarnings("squid:UndocumentedApi")
public interface VariableClient extends FeignQuerySupport<Variable> {

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_TEXT_PLAIN })
    @RequestLine(POST + VARIABLES_FIND_ALL)
    @Body("{condition}")
    @Override
    Set<Variable> findAll(@Param("condition") String condition);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(POST + VARIABLES)
    Variable create(Variable variable);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(PUT + VARIABLES)
    Variable update(Variable variable);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(PUT + VARIABLES + BATCH)
    Set<Variable> updateAll(Set<Variable> variable);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(POST + VARIABLES + BATCH)
    Set<Variable> createAll(Set<Variable> variable);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(value = DELETE + VARIABLES + "/{id}", decodeSlash = false)
    void delete(@Param("id") long variableId);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(DELETE + VARIABLES + BATCH)
    void deleteAll(Set<Long> variableIds);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_TEXT_PLAIN })
    @RequestLine(POST + VARIABLES_FIND_ALL_WITH_OPTIONS + "?queryOptions={queryOptions}")
    @Body("{condition}")
    @Experimental
    List<Variable> findAll(@Param("condition") String condition, @Param("queryOptions") String queryOptions);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(POST + VARIABLE_FIND_SCHEMAS + "?startTime={startTime}&endTime={endTime}")
    @Experimental
    Map<Long, Map<String, Set<TimeWindow>>> findSchemas(Set<Long> variableIds,
            @Param("startTime") Instant startTime,
            @Param("endTime") Instant endTime);
}
