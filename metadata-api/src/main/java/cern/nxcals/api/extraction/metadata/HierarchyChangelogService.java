package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.domain.HierarchyChangelog;
import cern.nxcals.api.extraction.metadata.queries.HierarchyChangelogs;

public interface HierarchyChangelogService extends Queryable<HierarchyChangelog, HierarchyChangelogs> {
}
