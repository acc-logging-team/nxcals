package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.domain.BaseGroup;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.Variable;
import com.github.rutledgepaulv.qbuilders.builders.QBuilder;

import java.util.Map;
import java.util.Set;

public interface AbstractGroupService<G extends BaseGroup, Q extends QBuilder<Q>> extends Queryable<G, Q> {

    /**
     * Creates group as in the parameter
     * NOTE: this is a mutative method. It is not thread-safe or meant to be used in parallel
     *
     * @param group to be created
     * @return Created group
     */
    G create(G group);

    /**
     * Updates an existing group to the new values as in the parameter
     * NOTE: this is a mutative method. It is not thread-safe or meant to be used in parallel
     *
     * @param group to be updated
     * @return Updated group or null if the group does not exist
     */
    G update(G group);

    /**
     * Deletes an existing group
     *
     * @param id the id of the group to be deleted
     */
    void delete(long id);

    /**
     * Returns the complete map of variables grouped by their association to a given group
     *
     * @param id of the group to query
     * @return {@link Map} of variables grouped by their association type to the group
     */
    Map<String, Set<Variable>> getVariables(long id);

    /**
     * Attaches the given {@link Map} of variables to the given group, <b>overwriting</b> any existing associations.
     * In case the association(s) do not exist, it creates and sets the variables as described by their ids.
     * The group and the variables must exist.
     * The map key should represent the association type of the enclosed variable {@link Set}.
     * NOTE: this action replaces any existing associations. The resulted associations would reflect the provided context.
     * NOTE: this is a mutative method. It is not thread-safe or meant to be used in parallel
     *
     * @param id                        group to attach variables to
     * @param variableIdsPerAssociation {@link Map} of variable ids to attach, grouped by the association type
     */
    void setVariables(long id, Map<String, Set<Long>> variableIdsPerAssociation);

    /**
     * Attaches the given {@link Map} of associations to the given group, by <b>adding</b> the provided variables
     * to the corresponding association. In case the association(s) do not exist, it creates and adds the variables
     * as described by their ids. The group and the variables to add must exist.
     * The map key should represent the association type of the enclosed variable {@link Set}.
     * NOTE: this action only adds to any existing attached {@link Set}. Does not fail if variable already present.
     * The resulted associations would reflect the merged product of the existing associations and the provided ones.
     * NOTE: this is a mutative method. It is not thread-safe or meant to be used in parallel
     *
     * @param id                        group to attach variables to
     * @param variableIdsPerAssociation {@link Map} of variable ids to attach, grouped by the association type
     */
    void addVariables(long id, Map<String, Set<Long>> variableIdsPerAssociation);

    /**
     * Detaches the given {@link Map} of variables from the given group. The map key should represent the association
     * type of the enclosed variable {@link Set}. The group and the variables must exist.
     * NOTE: it removes from any existing attached {@link Set}. Does not fail if variable already absent.
     * NOTE: this is a mutative method. It is not thread-safe or meant to be used in parallel
     *
     * @param id                        group to attach variables to
     * @param variableIdsPerAssociation {@link Map} of variable ids to detach, grouped by the association type
     */
    void removeVariables(long id, Map<String, Set<Long>> variableIdsPerAssociation);

    /**
     * Returns the complete map of entities grouped by their association to a given group
     *
     * @param id of the group to query
     * @return {@link Map} of entities grouped by their association type to the group
     */
    Map<String, Set<Entity>> getEntities(long id);

    /**
     * Attaches the given {@link Map} of entities to the given group, <b>overwriting</b> any existing associations.
     * In case the association(s) do not exist, it creates and sets the entities as described by their ids.
     * The group and the entities must exist.
     * The map key should represent the association type of the enclosed entity {@link Set}.
     * NOTE: this action replaces any existing associations. The resulted associations would reflect the provided context.
     * NOTE: this is a mutative method. It is not thread-safe or meant to be used in parallel
     *
     * @param id                      id of the group to attach entities to
     * @param entityIdsPerAssociation {@link Map} of entity ids to attach, grouped by the association type
     */
    void setEntities(long id, Map<String, Set<Long>> entityIdsPerAssociation);

    /**
     * Attaches the given {@link Map} of entities to the given group, by <b>adding</b> the provided entities
     * to the corresponding association. In case the association(s) do not exist, it creates and
     * adds the entities as described by their ids. The group and the entities to add must exist.
     * The map key should represent the association type of the enclosed entity {@link Set}.
     * NOTE: this action only adds to any existing attached {@link Set}. Does not fail if entity already present.
     * The resulted associations would reflect the merged product of the existing associations and the provided ones.
     * NOTE: this is a mutative method. It is not thread-safe or meant to be used in parallel
     *
     * @param id                      group to attach entities to
     * @param entityIdsPerAssociation {@link Map} of entity ids to attach, grouped by the association type
     */
    void addEntities(long id, Map<String, Set<Long>> entityIdsPerAssociation);

    /**
     * Detaches the given {@link Map} of entities from the given group. The map key should represent the association
     * type of the enclosed entity {@link Set}. The group and the entities must exist.
     * NOTE: it removes from any existing attached {@link Set}. Does not fail if entity already absent.
     * NOTE: this is a mutative method. It is not thread-safe or meant to be used in parallel
     *
     * @param id                      group to attach entities to
     * @param entityIdsPerAssociation {@link Map} of entity ids to detach, grouped by the association type
     */
    void removeEntities(long id, Map<String, Set<Long>> entityIdsPerAssociation);
}

