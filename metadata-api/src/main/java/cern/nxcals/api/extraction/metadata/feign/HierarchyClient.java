package cern.nxcals.api.extraction.metadata.feign;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.Hierarchy;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableHierarchies;
import cern.nxcals.api.domain.VariableHierarchyIds;
import feign.Body;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

import java.util.Set;

import static cern.nxcals.common.web.Endpoints.BATCH;
import static cern.nxcals.common.web.Endpoints.ENTITIES;
import static cern.nxcals.common.web.Endpoints.HIERARCHIES;
import static cern.nxcals.common.web.Endpoints.HIERARCHIES_FIND_ALL;
import static cern.nxcals.common.web.Endpoints.IDS_SUFFIX;
import static cern.nxcals.common.web.Endpoints.VARIABLES;
import static cern.nxcals.common.web.HttpHeaders.ACCEPT_APPLICATION_JSON;
import static cern.nxcals.common.web.HttpHeaders.CONTENT_TYPE_APPLICATION_JSON;
import static cern.nxcals.common.web.HttpHeaders.CONTENT_TYPE_TEXT_PLAIN;
import static cern.nxcals.common.web.HttpVerbs.DELETE;
import static cern.nxcals.common.web.HttpVerbs.POST;
import static cern.nxcals.common.web.HttpVerbs.PUT;

public interface HierarchyClient extends FeignQuerySupport<Hierarchy>{
    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_TEXT_PLAIN})
    @RequestLine(POST + HIERARCHIES_FIND_ALL)
    @Body("{condition}")
    @Override
    Set<Hierarchy> findAll(@Param("condition") String condition);

    //TODO: check why this is POST instead of GET
    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(value = POST + HIERARCHIES + "/{id}" + VARIABLES, decodeSlash = false)
    Set<Variable> getVariables(@Param("id") long hierarchyId);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(value = PUT + HIERARCHIES + "/{id}" + VARIABLES + IDS_SUFFIX, decodeSlash = false)
    void setVariables(@Param("id") long hierarchyId, Set<Long> variables);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(value = POST + HIERARCHIES + "/{id}" + VARIABLES + IDS_SUFFIX, decodeSlash = false)
    void addVariables(@Param("id") long hierarchyId, Set<Long> variables);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(value = DELETE + HIERARCHIES + "/{id}" + VARIABLES + IDS_SUFFIX, decodeSlash = false)
    void removeVariables(@Param("id") long hierarchyId, Set<Long> variables);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(value = POST + HIERARCHIES + "/{id}" + ENTITIES, decodeSlash = false)
    Set<Entity> getEntities(@Param("id") long hierarchyId);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(value = PUT + HIERARCHIES + "/{id}" + ENTITIES + IDS_SUFFIX, decodeSlash = false)
    void setEntities(@Param("id") long hierarchyId, Set<Long> entities);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(value = POST + HIERARCHIES + "/{id}" + ENTITIES + IDS_SUFFIX, decodeSlash = false)
    void addEntities(@Param("id") long hierarchyId, Set<Long> entities);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(value = DELETE + HIERARCHIES + "/{id}" + ENTITIES + IDS_SUFFIX, decodeSlash = false)
    void removeEntities(@Param("id") long hierarchyId, Set<Long> entities);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(value = POST + HIERARCHIES, decodeSlash = false)
    Hierarchy create(Hierarchy hierarchy);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(value = PUT + HIERARCHIES, decodeSlash = false)
    Hierarchy update(Hierarchy hierarchy);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(value = DELETE + HIERARCHIES + "/{id}", decodeSlash = false)
    void deleteLeaf(@Param("id") long hierarchyId);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(value = POST + HIERARCHIES + BATCH, decodeSlash = false)
    Set<Hierarchy> createAll(Set<Hierarchy> hierarchies);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(value = PUT + HIERARCHIES + BATCH, decodeSlash = false)
    Set<Hierarchy> updateAll(Set<Hierarchy> hierarchies);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(value = DELETE + HIERARCHIES + BATCH, decodeSlash = false)
    void deleteAllLeaves(Set<Long> hierarchyIds);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(value = POST + HIERARCHIES + BATCH + VARIABLES + IDS_SUFFIX, decodeSlash = false)
    Set<VariableHierarchies> getHierarchiesForVariables(Set<Long> variableIds);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(value = POST + HIERARCHIES + IDS_SUFFIX + BATCH + VARIABLES + IDS_SUFFIX, decodeSlash = false)
    Set<VariableHierarchyIds> getHierarchyIdsForVariables(Set<Long> variableIds);
}
