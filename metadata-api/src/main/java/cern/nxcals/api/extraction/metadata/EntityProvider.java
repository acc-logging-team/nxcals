/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.domain.CreateEntityRequest;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.EntityHistory;
import cern.nxcals.api.domain.KeyValues;
import cern.nxcals.api.domain.Partition;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.exceptions.FatalDataConflictRuntimeException;
import cern.nxcals.api.extraction.metadata.feign.EntityClient;
import cern.nxcals.api.extraction.metadata.queries.Entities;
import cern.nxcals.api.extraction.metadata.queries.SystemSpecs;
import cern.nxcals.api.extraction.metadata.security.PropertiesKeys;
import cern.nxcals.api.metadata.queries.ConditionWithOptions;
import cern.nxcals.api.metadata.queries.EntityQueryWithOptions;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.common.annotation.Experimental;
import cern.nxcals.common.domain.FindOrCreateEntityRequest;
import cern.nxcals.common.utils.CallRepeater;
import cern.nxcals.common.utils.ConfigHolder;
import cern.nxcals.internal.extraction.metadata.InternalEntityService;
import cern.nxcals.internal.extraction.metadata.InternalSystemSpecService;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.text.MessageFormat;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;

import static cern.nxcals.common.utils.KeyValuesUtils.convertMapIntoAvroSchemaString;
import static cern.nxcals.common.utils.OptionalUtils.toOptional;
import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static java.util.stream.Collectors.groupingBy;

/**
 * Implementation of the {@link AbstractProvider} for getting {@link Entity} objects
 */
@Slf4j
class EntityProvider extends AbstractProvider<Entity, EntityClient, Entities> implements InternalEntityService {
    private static final BiFunction<Map<String, Object>, Partition, Boolean> PARTITION_KEY_VALUE_MATCHER = (partitionKeyValues, partition) -> partition
            .getKeyValues().equals(partitionKeyValues);
    private static final BiFunction<Long, Partition, Boolean> PARTITION_ID_MATCHER = (partitionId, partition) ->
            partition.getId() == partitionId;

    private final InternalSystemSpecService systemService;

    EntityProvider(EntityClient httpService, InternalSystemSpecService systemService) {
        super(httpService);
        this.systemService = systemService;
    }

    @Override
    public Entity findOrCreateEntityFor(long systemId, @NonNull KeyValues entityKeyValues,
            @NonNull KeyValues partitionKeyValues, @NonNull String schema, long recordTimestamp) {
        return CallRepeater.call(
                () -> internalFindOrCreateEntityFor(systemId, entityKeyValues, partitionKeyValues, schema,
                        recordTimestamp),
                String.format(
                        "another client has already created an entity for system %d entity %s  partition %s schema %s",
                        systemId, entityKeyValues.getKeyValues(), partitionKeyValues.getKeyValues(), schema),
                ConfigHolder.getInt(PropertiesKeys.MAX_RETRIES.getPathInProperties(), 10),
                ex -> new FatalDataConflictRuntimeException(String.format(
                        "All repeated calls failed - cannot find or create entity=%s, partition=%s, timestamp=%d, schema=%s",
                        entityKeyValues.getKeyValues(), partitionKeyValues.getKeyValues(), recordTimestamp, schema),
                        ex),
                Lists.newArrayList(IllegalStateException.class, IllegalArgumentException.class,
                        FatalDataConflictRuntimeException.class)
        );

    }

    @Override
    public Entity findOrCreateEntityFor(long systemId, long entityId, long partitionId, @NonNull String schema,
            long recordTimestamp) {
        return CallRepeater.call(
                () -> internalFindOrCreateEntityFor(systemId, entityId, partitionId, schema, recordTimestamp),
                String.format(
                        "another client has already created an entity for system %d entity %d  partition %d schema %s",
                        systemId, entityId, partitionId, schema),
                ConfigHolder.getInt(PropertiesKeys.MAX_RETRIES.getPathInProperties(), 10),
                ex -> new FatalDataConflictRuntimeException(String.format(
                        "All repeated calls failed - cannot find or create entity=%d, partition=%d, timestamp=%d, schema=%s",
                        entityId, partitionId, recordTimestamp, schema), ex),
                Lists.newArrayList(IllegalStateException.class, IllegalArgumentException.class,
                        FatalDataConflictRuntimeException.class)
        );

    }

    private Entity internalFindOrCreateEntityFor(long systemId, long entityId, long partitionId, @NonNull String schema,
            long recordTimestamp) {
        //this is only finding
        Condition<Entities> condition = Entities.suchThat().id().eq(entityId);
        Entity entity = this.findOneWithCache(condition).orElseThrow(
                () -> new IllegalArgumentException(String.format("Entity with id=%1$d not found", entityId)));

        if (isRecordDefinitionFoundInCachedHistory(entity, partitionId, PARTITION_ID_MATCHER, schema,
                recordTimestamp)) {
            //the check method will throw exception on history rewrite attempt, so here all is fine
            return entity;
        }

        //The entity history is incomplete, we have to call the server.
        try {
            //call the service to change the history (add entry for this timestamp) and update the local cache (forcing call to the server)
            entity = serviceFindOrCreateEntityFor(systemId, entityId, partitionId, schema, recordTimestamp);
            return entity;
        } catch (FatalDataConflictRuntimeException exception) {
            //We have a data conflict for this entity.
            //We are using the given timestamp assuming that the next records will follow it.
            log.warn("Data conflict for system={}, entity={}, partition={}, schema={}, timestamp={}", systemId,
                    entityId, partitionId, schema, recordTimestamp);

            throw new FatalDataConflictRuntimeException(MessageFormat
                    .format("Data conflict detected, schema or partition history rewrite error, for systemId={0,number,#}, entityId={1,number,#}, partitionId={2,number,#},recordTimestamp={3}, schema={4}",
                            systemId, entityId, partitionId, TimeUtils.getInstantFromNanos(recordTimestamp), schema),
                    exception);
        } finally {
            if (entity != null) {
                //store in cache (to avoid hitting the service again for the same schema and further timestamps)
                putToCache(toRSQL(condition), entity);
            }
        }
    }

    private Entity serviceFindOrCreateEntityFor(long systemId, long entityId, long partitionId, @NonNull String schema,
            long timestamp) {
        log.debug(
                "Calling a remote service to updateEntityHistory for system={}, entityId={} partitionId={} timestamp={}",
                systemId, entityId, partitionId, TimeUtils.getInstantFromNanos(timestamp));
        return getHttpClient().findOrCreateEntityFor(systemId, entityId, partitionId, timestamp, schema);
    }

    private Entity internalFindOrCreateEntityFor(long systemId, KeyValues entityKeyValues, KeyValues partitionKeyValues,
            String schema, long recordTimestamp) {
        //We only cache by query condition from findOneWithCache (system, entityKeyValues).
        //If the cache does not contain the entity this first call always hits the remote service in order to put the entity into the cache.
        //It does not try to create the entity with the given input parameters as the data in this call might be wrong (like history rewrite),
        //that will lead to exception from the service
        //side and finally to a situation where we keep calling the service for always wrong data.
        //This might slow down the ingestion and happened after the restart of the datasources for empty cache and invalid first record (jwozniak)
        //Entity can be not found, so we create it.

        //this is only finding using cache (by default findOne is not cached)
        Condition<Entities> condition = conditionFor(systemId, entityKeyValues.getKeyValues());
        Entity entity = findOneWithCache(condition).orElse(null);

        if (entity != null && isRecordDefinitionFoundInCachedHistory(entity, partitionKeyValues.getKeyValues(),
                PARTITION_KEY_VALUE_MATCHER, schema, recordTimestamp)) {
            //entity found, data ok
            return entity;
        }
        //This is a new record (null) or history is incomplete (can still be wrong)
        //isRecordDefinitionFoundInCachedHistory() would have thrown an exception for history rewrite.
        try {
            //get from server (find or create)
            entity = serviceFindOrCreateEntityFor(systemId, entityKeyValues.getKeyValues(),
                    partitionKeyValues.getKeyValues(), schema, recordTimestamp);

            return entity;
        } catch (FatalDataConflictRuntimeException exception) {
            //We have a data conflict for this entity. The cache needs to be updated with the recent state of this entity to avoid hitting the service.
            //We are using the given timestamp assuming that the next records will follow it.
            log.warn("Data conflict for system={}, entity={}, partition={}, schema={}, timestamp={}", systemId,
                    entityKeyValues.getKeyValues(), partitionKeyValues.getKeyValues(), schema, recordTimestamp);

            throw new IllegalStateException(MessageFormat
                    .format("Data conflict detected, schema or partition history rewrite error, for systemId={0,"
                                    + "number,#}, entityKey={1}, partitionKey={2}, recordTimestamp={3}, schema={4}", systemId,
                            entityKeyValues.getKeyValues(), partitionKeyValues.getKeyValues(),
                            TimeUtils.getInstantFromNanos(recordTimestamp), schema), exception);
        } finally {
            if (entity != null) {
                //store in cache (to avoid hitting the service again for the same schema and further timestamps)
                putToCache(toRSQL(condition), entity);
            }
        }
    }

    private Entity serviceFindOrCreateEntityFor(long systemId, Map<String, Object> entityKeyValues,
            Map<String, Object> partitionKeyValues, String schema, long recordTimestamp) {
        log.debug(
                "Calling a remote service to findOrCreateEntityFor system={}, entityKey={}, partitionKey={}, schema={}, timestamp={}",
                systemId, entityKeyValues, partitionKeyValues, schema, recordTimestamp);
        FindOrCreateEntityRequest findOrCreateEntityRequest = FindOrCreateEntityRequest.builder()
                .entityKeyValues(entityKeyValues).partitionKeyValues(partitionKeyValues).schema(schema).build();
        return getHttpClient().findOrCreateEntityFor(systemId, recordTimestamp, findOrCreateEntityRequest);
    }

    @Override
    public Entity extendEntityFirstHistoryDataFor(long entityId, @NonNull String schema, long from) {
        return serviceExtendEntityHistoryDataFor(entityId, schema, from);
    }

    @Override
    public Set<Entity> findAllWithHistory(@NonNull Condition<Entities> condition, long historyStartTime,
            long historyEndTime) {
        return getHttpClient().findAll(toRSQL(condition), historyStartTime, historyEndTime);
    }

    //Currently this one is not cached. To be seen if this is needed.
    @Override
    public Optional<Entity> findOneWithHistory(@NonNull Condition<Entities> condition, long historyStartTime,
            long historyEndTime) {

        Set<Entity> dataSet = findAllWithHistory(condition, historyStartTime, historyEndTime);
        return dataSet == null || dataSet.isEmpty() ? Optional.empty() : Optional.of(Iterables.getOnlyElement(dataSet));
    }

    @Override
    public Set<Entity> updateEntities(@NonNull Set<Entity> entityList) {
        invalidateAllCache();
        return getHttpClient().updateEntities(entityList);
    }

    @Override
    public Entity createEntity(long systemId, Map<String, Object> entity, Map<String, Object> partition) {
        SystemSpec systemSpec = this.systemService.findById(systemId)
                .orElseThrow(() -> new IllegalArgumentException("No system with id " + systemId + " found"));
        validateEntityAndPartitionKeys(systemSpec, entity, partition);
        FindOrCreateEntityRequest request = FindOrCreateEntityRequest.builder().entityKeyValues(entity)
                .partitionKeyValues(partition).build();
        return getHttpClient().createEntity(systemId, request);
    }

    @Override
    public Set<Entity> createEntities(@NonNull Set<CreateEntityRequest> entityRequests) {
        Map<Long, List<CreateEntityRequest>> requestsBySystem = entityRequests.stream()
                .collect(groupingBy(CreateEntityRequest::getSystemId));
        requestsBySystem.forEach((systemId, requests) -> {
            SystemSpec systemSpec = this.systemService.findById(systemId)
                    .orElseThrow(() -> new IllegalArgumentException("No system with id " + systemId + " found"));
            requests.forEach(request -> validateEntityAndPartitionKeys(systemSpec, request.getEntityKeyValues(),
                    request.getPartitionKeyValues()));
        });
        return getHttpClient().createEntities(entityRequests);
    }

    @Experimental
    @Override
    public List<Entity> findAll(@NonNull EntityQueryWithOptions options) {
        validate(options.getCondition());
        return getHttpClient().findAll(toRSQL(options.getCondition()), options.toJson());
    }

    @Experimental
    @Override
    public Optional<Entity> findOne(@NonNull EntityQueryWithOptions options) {
        List<Entity> entities = findAll(options);
        return toOptional(entities);
    }

    @Experimental
    @Override
    public Set<Entity> findAll(
            @NonNull ConditionWithOptions<cern.nxcals.api.metadata.queries.Entities, EntityQueryWithOptions> condition) {
        return new HashSet<>(findAll(condition.withOptions()));
    }

    @Experimental
    @Override
    public Optional<Entity> findOne(
            @NonNull ConditionWithOptions<cern.nxcals.api.metadata.queries.Entities, EntityQueryWithOptions> condition) {
        return findOne(condition.withOptions());
    }

    private Condition<Entities> conditionFor(long systemId, Map<String, Object> entityKeyValues) {
        //SystemSpec is not cached
        SystemSpec systemSpec = systemService.findOne(SystemSpecs.suchThat().id().eq(systemId))
                .orElseThrow(() -> new IllegalArgumentException("No such system id " + systemId));
        return Entities.suchThat().systemId().eq(systemSpec.getId()).and().keyValues().eq(systemSpec, entityKeyValues);
    }

    private Entity serviceExtendEntityHistoryDataFor(long entityId, String schema, long from) {
        return getHttpClient().extendEntityFirstHistoryDataFor(entityId, from, schema);
    }

    private <T> Boolean isRecordDefinitionFoundInCachedHistory(Entity entity, T partitionIdentifier,
            BiFunction<T, Partition, Boolean> partitionMatcher, String schema, long recordNanos) {
        //made iterative for performance, streams are slower
        Set<EntityHistory> histData = entity.getEntityHistory();
        if (histData == null || histData.isEmpty()) {
            return false;
        }

        Instant recordStamp = TimeUtils.getInstantFromNanos(recordNanos);

        for (EntityHistory hist : histData) {
            if (!hist.getValidity().getStartTime().isAfter(recordStamp) && (hist.getValidity().getEndTime() == null
                    || hist.getValidity().getEndTime().isAfter(recordStamp))) {
                //Found the history entry for this recordTimestamp
                if (partitionMatcher.apply(partitionIdentifier, hist.getPartition()) && hist.getEntitySchema()
                        .getSchemaJson().equals(schema)) {
                    //Schema & partition match
                    return true;
                } else if (!hist.getValidity().isRightInfinite()) {
                    //The historical record is closed from both ends and this timestamp is inside it but
                    //the schema or partition do not match - this is wrong, we don't allow to rewrite history with different schema or partition.
                    //This is prevented already on the client side to avoid hitting the server with many calls like that for wrong data
                    log.warn("History rewrite error for entity {}: schema {} or partition {} do not match in found "
                                    + "history entry {} for recordTime {}", entity, schema, partitionIdentifier, hist,
                            recordNanos);

                    throw new IllegalStateException(MessageFormat
                            .format("Data conflict detected, schema or partition history rewrite error, "
                                            + "for entityId={0,number,#}, entityKey={1}, partition={2} system={3} ,"
                                            + "recordNanos={4}, historyId={5,number,#}, validFrom={6}, validTo={7}",
                                    entity.getId(), entity.getEntityKeyValues(), partitionIdentifier,
                                    entity.getSystemSpec().getName(), recordNanos, hist.getId(),
                                    hist.getValidity().getStartTime(), hist.getValidity().getEndTime()));
                }
            }
        }
        return false;
    }

    private void validateEntityAndPartitionKeys(SystemSpec systemSpec, Map<String, Object> entity,
            Map<String, Object> partition) {
        convertMapIntoAvroSchemaString(entity, systemSpec.getEntityKeyDefinitions());
        convertMapIntoAvroSchemaString(partition, systemSpec.getPartitionKeyDefinitions());
    }

    @Override
    public Optional<Entity> findById(long id) {
        return findOne(Entities.suchThat().id().eq(id));
    }

    @Override
    public void invalidateAllCache() {
        super.invalidateAllCache();
    }

    @Override
    public void invalidateCacheFor(Entity entity) {
        String cacheKeyForEntitiesFoundByKeyValues = toRSQL(
                conditionFor(entity.getSystemSpec().getId(), entity.getEntityKeyValues()));
        String cacheKeyEntitiesFoundById = toRSQL(Entities.suchThat().id().eq(entity.getId()));
        invalidateCacheFor(cacheKeyForEntitiesFoundByKeyValues);
        invalidateCacheFor(cacheKeyEntitiesFoundById);
    }
}
