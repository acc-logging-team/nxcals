/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.metadata.feign;

import cern.nxcals.common.domain.ExtractionCriteria;
import cern.nxcals.common.domain.ExtractionUnit;
import feign.Headers;
import feign.RequestLine;

import static cern.nxcals.common.web.Endpoints.EXTRACTION;
import static cern.nxcals.common.web.HttpHeaders.ACCEPT_APPLICATION_JSON;
import static cern.nxcals.common.web.HttpHeaders.CONTENT_TYPE_APPLICATION_JSON;
import static cern.nxcals.common.web.HttpVerbs.POST;

/**
 * Feign declarative service interface for consuming Entity Resource service.
 */
@SuppressWarnings("squid:UndocumentedApi")
public interface EntityResourceClient {
    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(POST + EXTRACTION)
    ExtractionUnit findBy(ExtractionCriteria criteria);
}
