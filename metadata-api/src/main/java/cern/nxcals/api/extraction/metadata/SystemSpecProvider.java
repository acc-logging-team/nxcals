/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.extraction.metadata.feign.SystemSpecClient;
import cern.nxcals.api.extraction.metadata.queries.SystemSpecs;
import cern.nxcals.internal.extraction.metadata.InternalSystemSpecService;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import lombok.NonNull;

import java.util.Optional;
import java.util.Set;

/**
 * Implementation of the SystemService using HTTP calls.
 */
// TODO: resolve in NXCALS-2313
class SystemSpecProvider extends AbstractProvider<SystemSpec, SystemSpecClient, SystemSpecs>
        implements InternalSystemSpecService {

    SystemSpecProvider(SystemSpecClient httpClient) {
        super(httpClient);
    }

    @Override
    public Optional<SystemSpec> findByName(@NonNull String name) {
        return findOne(SystemSpecs.suchThat().name().eq(name));
    }

    @Override
    public Set<SystemSpec> findAll() {
        return findAll(SystemSpecs.suchThat().id().exists());
    }

    @Override
    public Optional<SystemSpec> findById(long systemId) {
        return findOne(SystemSpecs.suchThat().id().eq(systemId));
    }

    @Override
    public Optional<SystemSpec> findOne(@NonNull Condition<SystemSpecs> condition) {
        return findOneWithCache(condition);
    }
}