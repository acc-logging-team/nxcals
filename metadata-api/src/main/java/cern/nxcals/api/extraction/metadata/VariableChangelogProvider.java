/*
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.domain.VariableChangelog;
import cern.nxcals.api.extraction.metadata.feign.VariableChangelogClient;
import cern.nxcals.api.extraction.metadata.queries.VariableChangelogs;
import cern.nxcals.internal.extraction.metadata.InternalVariableChangelogService;

import java.util.Optional;

/**
 * Implementation of {@link InternalVariableChangelogService} for getting {@link VariableChangelog} objects
 */
class VariableChangelogProvider extends AbstractProvider<VariableChangelog, VariableChangelogClient, VariableChangelogs>
        implements InternalVariableChangelogService {

        VariableChangelogProvider(VariableChangelogClient httpClient) {
        super(httpClient);
    }

    @Override
    public Optional<VariableChangelog> findById(long id) {
        return findOne(VariableChangelogs.suchThat().id().eq(id));
    }
}