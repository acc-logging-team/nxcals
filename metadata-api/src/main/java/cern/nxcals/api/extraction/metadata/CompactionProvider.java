/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.extraction.metadata.feign.CompactionClient;
import cern.nxcals.common.domain.DataProcessingJob;
import cern.nxcals.common.domain.DataProcessingJob.JobType;
import cern.nxcals.internal.extraction.metadata.InternalCompactionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
class CompactionProvider implements InternalCompactionService {
    private final CompactionClient compactionClient;

    @Override
    @SuppressWarnings("unchecked")
    public <T extends DataProcessingJob> List<T> getDataProcessJobs(int maxJobs, JobType jobType) {
        return (List<T>) compactionClient.getJobs(maxJobs, jobType);
    }
}
