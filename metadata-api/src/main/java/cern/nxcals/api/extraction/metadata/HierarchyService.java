/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.Hierarchy;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableHierarchies;
import cern.nxcals.api.domain.VariableHierarchyIds;
import cern.nxcals.api.extraction.metadata.queries.Hierarchies;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface HierarchyService extends Queryable<Hierarchy, Hierarchies> {

    /**
     * Creates hierarchy as in the parameter
     * NOTE: this is a mutative method. It is not thread-safe or meant to be used in parallel
     *
     * @param hierarchy to be created
     * @return Created hierarchy
     */
    Hierarchy create(Hierarchy hierarchy);

    /**
     * Updates an existing hierarchy to the new values as in the parameter
     * NOTE: this is a mutative method. It is not thread-safe or meant to be used in parallel
     *
     * @param hierarchy to be updated
     * @return Updated hierarchy or null if the hierarchy does not exist
     */
    Hierarchy update(Hierarchy hierarchy);

    /**
     * Deletes a leaf hierarchy
     * NOTE: this is a mutative method. It is not thread-safe or meant to be used in parallel
     *
     * @param hierarchy to be deleted
     */
    void deleteLeaf(Hierarchy hierarchy);

    /**
     * Creates hierarchies as in the parameter
     * NOTE: this is a mutative method. It is not thread-safe or meant to be used in parallel
     *
     * @param hierarchies to be created
     * @return Created hierarchies
     */
    Set<Hierarchy> createAll(Set<Hierarchy> hierarchies);

    /**
     * Updates existing hierarchies to the new values as in the provided {@link Set} of parameters
     * NOTE: this is a mutative method. It is not thread-safe or meant to be used in parallel
     *
     * @param hierarchies to be updated
     * @return Updated hierarchies
     */
    Set<Hierarchy> updateAll(Set<Hierarchy> hierarchies);

    /**
     * Deletes leaf hierarchies
     * NOTE: this is a mutative method. It is not thread-safe or meant to be used in parallel
     *
     * @param hierarchies to be deleted
     */
    void deleteAllLeaves(Set<Hierarchy> hierarchies);

    /**
     * Returns all the top level hierarchies in a given system
     *
     * @param system to find top level
     * @return set of top level hierarchies
     */
    Set<Hierarchy> getTopLevel(SystemSpec system);

    /**
     * Returns a full chain of hierarchies from the given parameter to the root
     *
     * @param hierarchy end-of-chain hierarchy
     * @return Optional with value or empty.
     */
    List<Hierarchy> getChainTo(Hierarchy hierarchy);

    /**
     * Attaches the given {@link Collection} of variables to the given hierarchy id. The hierarchy id and the variables must exist.
     * NOTE: it replaces any existing attached {@link Set}.
     * NOTE: this is a mutative method. It is not thread-safe or meant to be used in parallel
     *
     * @param hierarchyId hierarchy to attach variables to
     * @param variables   {@link Collection} of variables to attach
     * @deprecated this method will be removed in a future release. Please use setVariables(long hierarchyId, Set&lt;Long&gt; variableIds);
     */
    @Deprecated
    void setVariables(long hierarchyId, Collection<Variable> variables);

    /**
     * Attaches the given {@link Set} of variables to the given hierarchy id. The hierarchy id and the variables must exist.
     * NOTE: it replaces any existing attached {@link Set}.
     * NOTE: this is a mutative method. It is not thread-safe or meant to be used in parallel
     *
     * @param hierarchyId hierarchy to attach variables to
     * @param variableIds {@link Set} of variable ids to attach
     */
    void setVariables(long hierarchyId, Set<Long> variableIds);

    /**
     * Attaches the given {@link Set} of variables to the given hierarchy id. The hierarchy id and the variables must exist.
     * NOTE: it adds to any existing attached {@link Set}. Does not fail if variable already present.
     * NOTE: this is a mutative method. It is not thread-safe or meant to be used in parallel
     *
     * @param hierarchyId hierarchy to attach variables to
     * @param variableIds {@link Set} of variable ids to attach
     */
    void addVariables(long hierarchyId, Set<Long> variableIds);

    /**
     * Detaches the given {@link Set} of variables to the given hierarchy id. The hierarchy id and the variables must exist.
     * NOTE: it removes from any existing attached {@link Set}. Does not fail if variable already absent.
     * NOTE: this is a mutative method. It is not thread-safe or meant to be used in parallel
     *
     * @param hierarchyId hierarchy to attach variables to
     * @param variableIds {@link Set} of variable ids to detach
     */
    void removeVariables(long hierarchyId, Set<Long> variableIds);

    /**
     * Returns the {@link Set} of variables attached to a given hierarchy node
     *
     * @param hierarchyId of the hierarchy to query
     * @return {@link Set} of variables attached
     */
    Set<Variable> getVariables(long hierarchyId);

    /**
     * Attaches the given {@link Collection} of entities to the given hierarchy id. The hierarchy id and the entities must exist.
     * NOTE: it replaces any existing attached {@link Set}.
     * NOTE: this is a mutative method. It is not thread-safe or meant to be used in parallel
     *
     * @param hierarchyId hierarchy to attach variables to
     * @param entities    {@link Collection} of entities to attach
     * @deprecated this method will be removed in a future release. Please use setEntities(long hierarchyId, Set&lt;Long&gt; entityIds);
     */
    @Deprecated
    void setEntities(long hierarchyId, Collection<Entity> entities);

    /**
     * Attaches the given {@link Set} of entities to the given hierarchy id. The hierarchy id and the entities must exist.
     * NOTE: it replaces any existing attached {@link Set}.
     * NOTE: this is a mutative method. It is not thread-safe or meant to be used in parallel
     *
     * @param hierarchyId hierarchy to attach variables to
     * @param entityIds   {@link Set} of entity ids to attach
     */
    void setEntities(long hierarchyId, Set<Long> entityIds);

    /**
     * Detaches the given {@link Set} of entities to the given hierarchy id. The hierarchy id and the entities must exist.
     * NOTE: it removes from any existing attached {@link Set}. Does not fail if entities already absent.
     * NOTE: this is a mutative method. It is not thread-safe or meant to be used in parallel
     *
     * @param hierarchyId hierarchy to attach variables to
     * @param entityIds   {@link Set} of entity ids to detach
     */
    void removeEntities(long hierarchyId, Set<Long> entityIds);

    /**
     * Attaches the given {@link Set} of entities to the given hierarchy id. The hierarchy id and the entities must exist.
     * NOTE: it adds any existing attached {@link Set}. Does not fail if entity already present.
     * NOTE: this is a mutative method. It is not thread-safe or meant to be used in parallel
     *
     * @param hierarchyId hierarchy to attach variables to
     * @param entityIds   {@link Set} of entity ids to attach
     */
    void addEntities(long hierarchyId, Set<Long> entityIds);

    /**
     * Returns the {@link Set} of entities attached to a given hierarchy node
     *
     * @param hierarchyId of the hierarchy to query
     * @return {@link Set} of entities attached
     */
    Set<Entity> getEntities(long hierarchyId);

    /**
     * Returns a {@link Set} of hierarchies where the given variable, as referenced by the provided id.
     *
     * @param variableId the identification number of the target {@link Variable}
     * @return {@link Set} of associated hierarchies
     */
    Set<Hierarchy> getHierarchiesForVariable(long variableId);

    /**
     * Returns a {@link Set} of hierarchies grouped by the given variables as referenced by their ids.
     *
     * @param variableIds a {@link Set} of {@link Variable} ids that are attached to hierarchies
     * @return {@link Set} of {@link VariableHierarchies} representing associated hierarchies grouped by variable id
     */
    Set<VariableHierarchies> getHierarchiesForVariables(Set<Long> variableIds);

    /**
     * Returns a {@link Set} of hierarchy ids grouped by the given variables ids.
     *
     * @param variableIds a {@link Set} of {@link Variable} ids that are attached to hierarchies
     * @return {@link Set} of {@link VariableHierarchyIds} representing associated {@link Hierarchy} ids grouped by variable id
     */
    Set<VariableHierarchyIds> getHierarchyIdsForVariables(Set<Long> variableIds);

}
