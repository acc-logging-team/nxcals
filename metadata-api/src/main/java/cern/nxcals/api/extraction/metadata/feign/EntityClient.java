/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.metadata.feign;

import cern.nxcals.api.domain.CreateEntityRequest;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.common.domain.FindOrCreateEntityRequest;
import feign.Body;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

import java.util.List;
import java.util.Set;

import static cern.nxcals.common.web.Endpoints.BATCH;
import static cern.nxcals.common.web.Endpoints.ENTITIES;
import static cern.nxcals.common.web.Endpoints.ENTITIES_FIND_ALL;
import static cern.nxcals.common.web.Endpoints.ENTITIES_FIND_ALL_WITH_HISTORY;
import static cern.nxcals.common.web.Endpoints.ENTITIES_FIND_ALL_WITH_OPTIONS;
import static cern.nxcals.common.web.Endpoints.ENTITY_EXTEND_FIRST_HISTORY;
import static cern.nxcals.common.web.Endpoints.ENTITY_UPDATE;
import static cern.nxcals.common.web.HttpHeaders.ACCEPT_APPLICATION_JSON;
import static cern.nxcals.common.web.HttpHeaders.CONTENT_TYPE_APPLICATION_JSON;
import static cern.nxcals.common.web.HttpHeaders.CONTENT_TYPE_TEXT_PLAIN;
import static cern.nxcals.common.web.HttpVerbs.POST;
import static cern.nxcals.common.web.HttpVerbs.PUT;

/**
 * Feign declarative service interface for consuming EntityService.
 */
@SuppressWarnings("squid:UndocumentedApi")
public interface EntityClient extends FeignQuerySupport<Entity>{


    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_TEXT_PLAIN})
    @RequestLine(POST + ENTITIES_FIND_ALL)
    @Body("{condition}")
    @Override
    Set<Entity> findAll(@Param("condition") String condition);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_TEXT_PLAIN })
    @RequestLine(POST + ENTITIES_FIND_ALL_WITH_HISTORY
            + "?startTime={startTime}&"
            + "endTime={endTime}"
    )
    @Body("{condition}")
    Set<Entity> findAll(@Param("condition") String condition, @Param("startTime") long startTime,
            @Param("endTime") long endTime);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_TEXT_PLAIN })
    @RequestLine(POST + ENTITIES_FIND_ALL_WITH_OPTIONS
            + "?queryOptions={queryOptions}"
    )
    @Body("{condition}")
    List<Entity> findAll(@Param("condition") String condition, @Param("queryOptions") String queryOptions);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(PUT + ENTITIES
            + "?systemId={systemId}&"
            + "recordTimestamp={recordTimestamp}")
    Entity findOrCreateEntityFor(
            @Param("systemId") long systemId,
            @Param("recordTimestamp") long recordTimestamp,
            FindOrCreateEntityRequest findOrCreateEntityRequest
    );

    @Headers({ ACCEPT_APPLICATION_JSON })
    @Body("{schema}")
    @RequestLine(PUT + ENTITY_EXTEND_FIRST_HISTORY
            + "?entityId={entityId}&"
            + "from={from}")
    Entity extendEntityFirstHistoryDataFor(
            @Param("entityId") long entityId,
            @Param("from") long from,
            @Param("schema") String schema
    );

    @Headers({ ACCEPT_APPLICATION_JSON })
    @Body("{schema}")
    @RequestLine(PUT + ENTITIES
            + "?systemId={systemId}"
            + "&entityId={entityId}"
            + "&partitionId={partitionId}"
            + "&recordTimestamp={recordTimestamp}")
    Entity findOrCreateEntityFor(
            @Param("systemId") long systemId,
            @Param("entityId") long entityId,
            @Param("partitionId") long partitionId,
            @Param("recordTimestamp") long recordTimestamp,
            @Param("schema") String schema);



    @Headers({ CONTENT_TYPE_APPLICATION_JSON, ACCEPT_APPLICATION_JSON })
    @RequestLine(PUT + ENTITY_UPDATE)
    Set<Entity> updateEntities( Set<Entity> entityDataList);


    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(PUT + ENTITIES
            + "?systemId={systemId}")
    Entity createEntity(@Param("systemId") long systemId, FindOrCreateEntityRequest request);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(POST + ENTITIES + BATCH)
    Set<Entity> createEntities(Set<CreateEntityRequest> requests);

}
