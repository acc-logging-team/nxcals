/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.metadata;

import cern.nxcals.common.utils.Lazy;
import cern.nxcals.internal.extraction.metadata.InternalSystemSpecService;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Public API to access NXCALS Meta-Services.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ServiceClientFactory {
    private static final Lazy<SystemSpecService> SYSTEM_SPEC_SERVICE = new Lazy<>(() -> new SystemSpecProvider(InternalClientFactory.createSystemSpecService()));
    private static final Lazy<EntitySchemaService> ENTITY_SCHEMA_SERVICE = new Lazy<>(() -> new EntitySchemaProvider(InternalClientFactory.createEntitySchemaService()));
    private static final Lazy<EntityService> ENTITY_SERVICE = new Lazy<>(() -> new EntityProvider(InternalClientFactory.createEntityService(),  (InternalSystemSpecService) SYSTEM_SPEC_SERVICE.get()));
    private static final Lazy<GroupService> GROUP_SERVICE = new Lazy<>(() -> new GroupProvider(InternalClientFactory.createGroupService()));
    private static final Lazy<VariableService> VARIABLE_SERVICE = new Lazy<>(() -> new VariableProvider(InternalClientFactory.createVariableService()));
    private static final Lazy<HierarchyService> HIERARCHY_SERVICE = new Lazy<>(() -> new HierarchyProvider(InternalClientFactory.createHierarchyService()));
    private static final Lazy<HierarchyChangelogService> HIERARCHY_CHANGELOG_SERVICE = new Lazy<>(() -> new HierarchyChangelogProvider(InternalClientFactory.createHierarchyChangelogService()));
    private static final Lazy<HierarchyVariablesChangelogService> HIERARCHY_VARIABLES_CHANGELOG_SERVICE = new Lazy<>(() -> new HierarchyVariablesChangelogProvider(InternalClientFactory.createHierarchyVariablesChangelogService()));
    private static final Lazy<VariableChangelogService> VARIABLE_CHANGELOG_SERVICE = new Lazy<>(() -> new VariableChangelogProvider(InternalClientFactory.createVariableChangelogService()));
    private static final Lazy<VariableConfigChangelogService> VARIABLE_CONFIG_CHANGELOG_SERVICE = new Lazy<>(() -> new VariableConfigChangelogProvider(InternalClientFactory.createVariableConfigChangelogService()));
    private static final Lazy<EntityChangelogService> ENTITY_CHANGELOG_SERVICE = new Lazy<>(() -> new EntityChangelogProvider(InternalClientFactory.createEntityChangelogService()));

    /**
     * @return Only instance of {@link EntitySchemaService}.
     */
    public static EntitySchemaService createEntitySchemaService() {
        return ENTITY_SCHEMA_SERVICE.get();
    }

    /**
     * @return Only instance of {@link EntityService}.
     */
    public static EntityService createEntityService() {
        return ENTITY_SERVICE.get();
    }

    /**
     * @return Only instance of {@link SystemSpecService}.
     */
    public static SystemSpecService createSystemSpecService() {
        return SYSTEM_SPEC_SERVICE.get();
    }

    /**
     * @return Only instance of {@link GroupService}.
     */
    public static GroupService createGroupService() {
        return GROUP_SERVICE.get();
    }

    /**
     * @return Only instance of {@link VariableService}.
     */
    public static VariableService createVariableService() {
        return VARIABLE_SERVICE.get();
    }

    /**
     * @return Only instance of {@link HierarchyService}.
     */
    public static HierarchyService createHierarchyService() {
        return HIERARCHY_SERVICE.get();
    }
    /**
     * @return Only instance of {@link HierarchyChangelogService}.
     */
    public static HierarchyChangelogService createHierarchyChangelogService() {
        return HIERARCHY_CHANGELOG_SERVICE.get();
    }

    /**
     * @return Only instance of {@link HierarchyVariablesChangelogService}.
     */
    public static HierarchyVariablesChangelogService createHierarchyVariablesChangelogService() {
        return HIERARCHY_VARIABLES_CHANGELOG_SERVICE.get();
    }
    /**
     * @return Only instance of {@link VariableChangelogService}.
     */
    public static VariableChangelogService createVariableChangelogService() {
        return VARIABLE_CHANGELOG_SERVICE.get();
    }
    /**
     * @return Only instance of {@link VariableConfigChangelogService}.
     */
    public static VariableConfigChangelogService createVariableConfigChangelogService() {
        return VARIABLE_CONFIG_CHANGELOG_SERVICE.get();
    }

    public static EntityChangelogService createEntityChangelogService() {
        return ENTITY_CHANGELOG_SERVICE.get();
    }
}