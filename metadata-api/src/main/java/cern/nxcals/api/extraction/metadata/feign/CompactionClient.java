/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.api.extraction.metadata.feign;

import cern.nxcals.common.domain.DataProcessingJob;
import feign.Param;
import feign.RequestLine;

import java.util.List;

import static cern.nxcals.common.web.Endpoints.COMPACTION_JOBS;
import static cern.nxcals.common.web.HttpVerbs.GET;

/**
 * Feign declarative service interface for consuming Compaction service.
 */
@SuppressWarnings("squid:UndocumentedApi")
public interface CompactionClient {
    @RequestLine(GET + COMPACTION_JOBS + "?maxJobs={maxJobs}&jobType={jobType}")
    List<DataProcessingJob> getJobs(@Param("maxJobs") int maxJobs, @Param("jobType") DataProcessingJob.JobType jobType);
}
