package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.converters.Py4jLongConverter;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.Group;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.extraction.metadata.feign.GroupClient;
import cern.nxcals.api.extraction.metadata.queries.Groups;
import cern.nxcals.internal.extraction.metadata.InternalGroupService;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class GroupProvider extends AbstractProvider<Group, GroupClient, Groups> implements InternalGroupService {
    public GroupProvider(GroupClient httpClient) {
        super(httpClient);
    }

    @Override
    public Optional<Group> findById(long id) {
        return findOne(Groups.suchThat().id().eq(id));
    }

    @Override
    public Group create(Group group) {
        return getHttpClient().create(group);
    }

    @Override
    public Group update(Group group) {
        return getHttpClient().update(group);
    }

    @Override
    public void delete(long groupId) {
        getHttpClient().delete(groupId);
    }

    @Override
    public Map<String, Set<Variable>> getVariables(long groupId) {
        return getHttpClient().getVariables(groupId);
    }

    @Override
    public void setVariables(long groupId, Map<String, Set<Long>> variableIdsPerAssociation) {
        getHttpClient().setVariables(groupId, Py4jLongConverter.convertMap(new HashMap<>(variableIdsPerAssociation)));
    }

    @Override
    public void addVariables(long groupId, Map<String, Set<Long>> variableIdsPerAssociation) {
        getHttpClient().addVariables(groupId, Py4jLongConverter.convertMap(new HashMap<>(variableIdsPerAssociation)));
    }

    @Override
    public void removeVariables(long groupId, Map<String, Set<Long>> variableIdsPerAssociation) {
        getHttpClient().removeVariables(groupId,
                Py4jLongConverter.convertMap(new HashMap<>(variableIdsPerAssociation)));
    }

    @Override
    public Map<String, Set<Entity>> getEntities(long groupId) {
        return getHttpClient().getEntities(groupId);
    }

    @Override
    public void setEntities(long groupId, Map<String, Set<Long>> entityIdsPerAssociation) {
        getHttpClient().setEntities(groupId, Py4jLongConverter.convertMap(new HashMap<>(entityIdsPerAssociation)));
    }

    @Override
    public void addEntities(long groupId, Map<String, Set<Long>> entityIdsPerAssociation) {
        getHttpClient().addEntities(groupId, Py4jLongConverter.convertMap(new HashMap<>(entityIdsPerAssociation)));
    }

    @Override
    public void removeEntities(long groupId, Map<String, Set<Long>> entityIdsPerAssociation) {
        getHttpClient().removeEntities(groupId, Py4jLongConverter.convertMap(new HashMap<>(entityIdsPerAssociation)));
    }
}
