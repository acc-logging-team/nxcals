package cern.nxcals.api.extraction.metadata.feign;

import cern.nxcals.api.extraction.data.builders.fluent.ThreadLocalStorage;
import cern.nxcals.common.domain.CallDetails;
import feign.RequestInterceptor;
import feign.RequestTemplate;

class CallDetailsInterceptor implements RequestInterceptor {
    @Override
    public void apply(RequestTemplate template) {

        CallDetails callDetails = ThreadLocalStorage.getCallDetails();
        ThreadLocalStorage.clear();

        // callDetails are not set when querying metadata
        if (callDetails != null) {
            template.header("application-name", callDetails.getApplicationName());
            template.header("declaring-class", callDetails.getDeclaringClass());
            template.header("method-name", callDetails.getMethodName());
        }
    }
}