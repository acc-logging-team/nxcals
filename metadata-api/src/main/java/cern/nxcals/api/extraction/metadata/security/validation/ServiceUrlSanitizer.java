package cern.nxcals.api.extraction.metadata.security.validation;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.UrlValidator;

import java.util.Arrays;
import java.util.List;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;

import static java.lang.String.format;

@Slf4j
public enum ServiceUrlSanitizer implements UnaryOperator<String> {
    INSTANCE;

    private static final UrlValidator URL_VALIDATOR = new UrlValidator(UrlValidator.ALLOW_LOCAL_URLS);
    private static final String HTTP_REGEX_STRING = "^(http)://.*$";

    @Override
    public String apply(@NonNull String serviceUrl) {
        if (StringUtils.isBlank(serviceUrl)) {
            throw new IllegalArgumentException(
                    "No service URLs found! Please, provide valid set of comma separated service URLs");
        }
        List<String> urls = toList(serviceUrl);
        for (int i = 0; i < urls.size(); i++) {
            String url = urls.get(i);
            if (!URL_VALIDATOR.isValid(url)) {
                throw new IllegalArgumentException(format("Invalid content [%s] for property [service.url]! "
                                + "Invalid URL value [%s]. Please, provide valid set of comma separated service URLs",
                        serviceUrl, url));
            }
            if (url.matches(HTTP_REGEX_STRING)) {
                log.warn(
                        "Provided service url [{}] is under http scheme (might be problematic)! Will enforce it to https instead.",
                        url);
                urls.set(i, this.enforceHttpsScheme(url));
            }
        }
        return String.join(",", urls);
    }

    private String enforceHttpsScheme(String url) {
        return url.replaceFirst("http", "https");
    }

    private List<String> toList(String serviceUrl) {
        return Arrays.stream(serviceUrl.split(","))
                .map(String::trim).collect(Collectors.toList());
    }

}
