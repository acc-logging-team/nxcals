package cern.nxcals.api.extraction.metadata.feign;

import cern.nxcals.api.domain.PartitionResourceHistory;
import feign.Body;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

import java.util.Set;

import static cern.nxcals.common.web.Endpoints.PARTITION_RESOURCE_HISTORIES;
import static cern.nxcals.common.web.Endpoints.PARTITION_RESOURCE_HISTORIES_FIND_ALL;
import static cern.nxcals.common.web.HttpHeaders.ACCEPT_APPLICATION_JSON;
import static cern.nxcals.common.web.HttpHeaders.CONTENT_TYPE_APPLICATION_JSON;
import static cern.nxcals.common.web.HttpHeaders.CONTENT_TYPE_TEXT_PLAIN;
import static cern.nxcals.common.web.HttpVerbs.DELETE;
import static cern.nxcals.common.web.HttpVerbs.POST;
import static cern.nxcals.common.web.HttpVerbs.PUT;

public interface PartitionResourceHistoryClient extends FeignQuerySupport<PartitionResourceHistory> {
    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_TEXT_PLAIN })
    @RequestLine(POST + PARTITION_RESOURCE_HISTORIES_FIND_ALL)
    @Body("{condition}")
    Set<PartitionResourceHistory> findAll(@Param("condition") String condition);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(value = PUT + PARTITION_RESOURCE_HISTORIES)
    PartitionResourceHistory update(PartitionResourceHistory partitionResource);

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON })
    @RequestLine(POST + PARTITION_RESOURCE_HISTORIES)
    PartitionResourceHistory create(PartitionResourceHistory partitionResource);

    @Headers({ACCEPT_APPLICATION_JSON, CONTENT_TYPE_APPLICATION_JSON})
    @RequestLine(DELETE + PARTITION_RESOURCE_HISTORIES)
    void delete(Long partitionResourceInfoId);
}
