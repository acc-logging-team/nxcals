package cern.nxcals.api.extraction.metadata.security;

import com.google.common.annotations.VisibleForTesting;
import feign.Client;
import feign.Request;
import feign.Response;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpHeaders;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * NXCALS Implementation of feign {@link Client}.
 * It adds Kerberos token to the HTTP request header.
 */
@Slf4j
public class SecurityAwareFeignClient extends Client.Default {

    private final List<AuthTokenProvider> authProviders;

    @Builder
    SecurityAwareFeignClient(
            List<AuthTokenProvider> authProviders,
            SSLSocketFactory sslContextFactory,
            HostnameVerifier hostnameVerifier) {
        super(sslContextFactory, hostnameVerifier);
        this.authProviders = authProviders;
    }

    @VisibleForTesting
    Request applyAuth(Request request) {
        Map<String, Collection<String>> headers = new HashMap<>(request.headers());

        for (AuthTokenProvider authProvider : authProviders) {
            Optional<String> headerOptional = authProvider.apply(request);
            if (headerOptional.isPresent()) {
                headers.put(HttpHeaders.AUTHORIZATION, Collections.singletonList(headerOptional.get()));

                log.debug("Obtained authentication token for request with url {} with {} auth provider",
                        request.url(), authProvider);

                return Request.create(request.httpMethod(), request.url(), headers, request.body(), request.charset(), request.requestTemplate());
            }
        }
        throw new IllegalStateException(
                "Cannot authenticate using given authentications providers " + authProviders);
    }

    @Override
    public Response execute(Request request, Request.Options options) throws IOException {
        Request requestWithAuth = applyAuth(request);
        return super.execute(requestWithAuth, options);
    }
}
