package cern.nxcals.api.extraction.metadata.feign;

import cern.nxcals.api.domain.HierarchyVariablesChangelog;
import feign.Body;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

import java.util.Set;

import static cern.nxcals.common.web.Endpoints.HIERARCHY_VARIABLES_CHANGELOGS_FIND_ALL;
import static cern.nxcals.common.web.HttpHeaders.ACCEPT_APPLICATION_JSON;
import static cern.nxcals.common.web.HttpHeaders.CONTENT_TYPE_TEXT_PLAIN;
import static cern.nxcals.common.web.HttpVerbs.POST;

/**
 * Feign declarative service interface for consuming HierarchyVariablesChangelog service.
 */
@SuppressWarnings("squid:UndocumentedApi")
public interface HierarchyVariablesChangelogClient extends FeignQuerySupport<HierarchyVariablesChangelog> {

    @Headers({ ACCEPT_APPLICATION_JSON, CONTENT_TYPE_TEXT_PLAIN })
    @RequestLine(POST + HIERARCHY_VARIABLES_CHANGELOGS_FIND_ALL)
    @Body("{condition}")
    Set<HierarchyVariablesChangelog> findAll(@Param("condition") String condition);

}
