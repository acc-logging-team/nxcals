package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.domain.PartitionResource;
import cern.nxcals.api.extraction.metadata.queries.PartitionResources;


public interface PartitionResourceService extends Queryable<PartitionResource, PartitionResources> {
    /**
     * Creates PartitionResource as in the parameter
     * NOTE: this is a mutative method. It is not thread-safe or meant to be used in parallel
     *
     * @param partitionResource to be created
     * @return Created PartitionResource
     */
    PartitionResource create(PartitionResource partitionResource);

    /**
     * Updates an existing PartitionResource to the new values as in the parameter
     * NOTE: this is a mutative method. It is not thread-safe or meant to be used in parallel
     *
     * @param partitionResource to be updated
     * @return Updated PartitionResource
     */
    PartitionResource update(PartitionResource partitionResource);

    /**
     * Deletes an existing PartitionResource
     *
     * @param partitionResourceId the id of the PartitionResource to be deleted
     */
    void delete(long partitionResourceId);

}
