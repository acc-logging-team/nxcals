/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.domain.Identifiable;
import cern.nxcals.api.extraction.metadata.feign.FeignQuerySupport;
import cern.nxcals.common.utils.ConfigHolder;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.rutledgepaulv.qbuilders.builders.QBuilder;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.github.rutledgepaulv.qbuilders.nodes.AndNode;
import com.github.rutledgepaulv.qbuilders.nodes.ComparisonNode;
import com.github.rutledgepaulv.qbuilders.nodes.LogicalNode;
import com.github.rutledgepaulv.qbuilders.nodes.OrNode;
import com.github.rutledgepaulv.qbuilders.operators.ComparisonOperator;
import com.github.rutledgepaulv.qbuilders.visitors.AbstractVoidContextNodeVisitor;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Iterables;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static com.google.common.base.Preconditions.checkArgument;

/**
 * Abstract class for data providers.
 *
 * @param <K> Client data type.
 * @param <C> Client service type.
 * @param <Q> Condition (query) type
 */
@Slf4j
@RequiredArgsConstructor
public abstract class AbstractProvider<K extends Identifiable, C extends FeignQuerySupport<K>, Q extends QBuilder<Q>>
        implements Queryable<K, Q> {

    private static final int MIN_INITIAL_CAPACITY = 100;
    private static final int MIN_MAXIMUM_SIZE = 500;
    private static final int MIN_EXPIRE_AFTER_ACCESS = 600;
    private static final int MIN_EXPIRE_AFTER_WRITE = 600;
    private static final ValidatingVisitor validator = new ValidatingVisitor();
    private final C httpClient;
    private final Cache<String, K> queryCache = createCacheWithConfigurations();

    C getHttpClient() {
        return this.httpClient;
    }

    protected void validate(Condition<?> condition) {
        condition.query(validator);
    }

    @Override
    public Set<K> findAll(@NonNull Condition<Q> condition) {
        validate(condition);
        return getHttpClient().findAll(toRSQL(condition));
    }

    /**
     * This implementation is never cached.
     * If you need a cache version please override it and use findOneWithCache implementation provided here.
     *
     * @param condition
     * @return
     */
    @Override
    public Optional<K> findOne(@NonNull Condition<Q> condition) {
        validate(condition);
        return Optional.ofNullable(findOne(toRSQL(condition)));
    }

    /**
     * A helper method that allows overriding classes to implement their own findOne with caching.
     *
     * @param condition - a condition for searching zero or one values.
     * @return
     */
    Optional<K> findOneWithCache(@NonNull Condition<Q> condition) {
        validate(condition);
        return Optional.ofNullable(queryCache.get(toRSQL(condition), this::findOne));
    }

    void invalidateCacheFor(String key) {
        queryCache.invalidate(key);
    }

    void invalidateAllCache() {
        queryCache.invalidateAll();
    }

    Optional<K> getFromCache(String key) {
        return Optional.ofNullable(queryCache.getIfPresent(key));
    }

    Optional<K> getFromCache(String key, Function<String, K> supplier) {
        return Optional.ofNullable(queryCache.get(key, supplier));
    }

    void putToCache(String key, K value) {
        this.queryCache.put(key, value);
    }

    private K findOne(String query) {
        Set<K> all = getHttpClient().findAll(query);
        return all == null || all.isEmpty() ? null : Iterables.getOnlyElement(all);
    }

    /**
     * Validator for conditions.
     */
    private static class ValidatingVisitor extends AbstractVoidContextNodeVisitor<Void> {

        @Override
        protected Void visit(AndNode node) {
            return visitLogicalNode(node);
        }

        @Override
        protected Void visit(OrNode node) {
            return visitLogicalNode(node);
        }

        private Void visitLogicalNode(LogicalNode node) {
            node.getChildren().forEach(this::visitAny);
            return null;
        }

        @Override
        protected Void visit(ComparisonNode node) {
            ComparisonOperator operator = node.getOperator();
            if (ComparisonOperator.IN.equals(operator) || ComparisonOperator.NIN.equals(operator)) {
                if (node.getValues() == null) {
                    throw new NullPointerException(
                            "The IN/NOT IN Operator does not accept null argument for field " + node.getField());
                }
                if(node.getValues().isEmpty()) {
                    throw new IllegalArgumentException(
                            "Please provide values for IN/NOT IN Operator. Currently using "
                                    + node.getValues().size() + " values for field " + node.getField());

                }
            }
            if (ComparisonOperator.SUB_CONDITION_ANY.equals(operator)) {
                throw new IllegalArgumentException(
                        "Sub metadata operator not implemented. Currently used for field " + node.getField());
            }

            return null;
        }
    }

    private Cache<String, K> createCacheWithConfigurations() {
        int initialCapacity = ConfigHolder.getInt("cache.spec.initial.capacity", MIN_MAXIMUM_SIZE);
        checkArgument(initialCapacity >= MIN_INITIAL_CAPACITY, "Initial capacity of cache should be bigger than " + MIN_INITIAL_CAPACITY);
        int maximumSize = ConfigHolder.getInt("cache.spec.maximum.size", Integer.MAX_VALUE);
        checkArgument( maximumSize >= MIN_MAXIMUM_SIZE, "Maximum size of cache should be greater than " + MIN_MAXIMUM_SIZE);
        int expireAfterAccess = ConfigHolder.getInt("cache.spec.expire.after.access.seconds", Integer.MAX_VALUE);
        checkArgument(expireAfterAccess >= MIN_EXPIRE_AFTER_ACCESS, "Value of expireAfterAccess should be greater than " + MIN_EXPIRE_AFTER_ACCESS);
        int expireAfterWrite = ConfigHolder.getInt("cache.spec.expire.after.write.seconds", Integer.MAX_VALUE);
        checkArgument(expireAfterWrite >= MIN_EXPIRE_AFTER_WRITE, "Value of expireAfterWrite should be greater than " + MIN_EXPIRE_AFTER_WRITE);

        return Caffeine.newBuilder()
                .initialCapacity(initialCapacity)
                .maximumSize(maximumSize)
                .expireAfterAccess(Duration.of(expireAfterAccess, ChronoUnit.SECONDS))
                .expireAfterWrite(Duration.of(expireAfterWrite, ChronoUnit.SECONDS))
                .build();
    }

    @VisibleForTesting
    protected Cache<String, K> getCache() {
        return this.queryCache;
    }

}
