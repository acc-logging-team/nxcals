/*
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.converters.Py4jLongConverter;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.extraction.metadata.feign.VariableClient;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import cern.nxcals.api.metadata.queries.ConditionWithOptions;
import cern.nxcals.api.metadata.queries.VariableQueryWithOptions;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.common.annotation.Experimental;
import cern.nxcals.common.utils.AvroUtils;
import cern.nxcals.common.utils.MapUtils;
import cern.nxcals.internal.extraction.metadata.InternalVariableService;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import lombok.NonNull;
import org.apache.avro.Schema;

import java.time.Instant;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;

import static cern.nxcals.common.utils.OptionalUtils.toOptional;
import static cern.nxcals.common.utils.RSQLUtils.toRSQL;

/**
 * Implementation of {@link VariableService} for getting {@link Variable} objects
 */
class VariableProvider extends AbstractProvider<Variable, VariableClient, Variables>
        implements InternalVariableService {

    VariableProvider(VariableClient httpClient) {
        super(httpClient);
    }

    @Override
    public Optional<Variable> findById(long id) {
        return findOne(Variables.suchThat().id().eq(id));
    }

    @Override
    public Variable create(@NonNull Variable variable) {
        return modifyWith(() -> getHttpClient().create(variable));
    }

    @Override
    public Variable update(@NonNull Variable variable) {
        return modifyWith(() -> getHttpClient().update(variable));
    }

    @Override
    public Set<Variable> createAll(Set<Variable> variables) {
        return modifyWith(() -> getHttpClient().createAll(variables));
    }

    @Override
    public Set<Variable> updateAll(Set<Variable> variables) {
        return modifyWith(() -> getHttpClient().updateAll(variables));
    }

    @Override
    public void delete(long variableId) {
        invalidateAllCache();
        getHttpClient().delete(variableId);
    }

    @Override
    public void deleteAll(Set<Long> variableIds) {
        invalidateAllCache();
        getHttpClient().deleteAll(Py4jLongConverter.convert(variableIds));
    }

    @Experimental
    public List<Variable> findAll(@NonNull VariableQueryWithOptions options) {
        validate(options.getCondition());
        return getHttpClient().findAll(toRSQL(options.getCondition()), options.toJson());
    }

    @Experimental
    public Optional<Variable> findOne(@NonNull VariableQueryWithOptions options) {
        List<Variable> all = findAll(options);
        return toOptional(all);
    }

    @Experimental
    public Set<Variable> findAll(
            ConditionWithOptions<cern.nxcals.api.metadata.queries.Variables, VariableQueryWithOptions> condition) {
        return new HashSet<>(findAll(condition.withOptions()));
    }

    @Experimental
    public Optional<Variable> findOne(
            ConditionWithOptions<cern.nxcals.api.metadata.queries.Variables, VariableQueryWithOptions> condition) {
        return findOne(condition.withOptions());
    }

    @Override
    public Map<TimeWindow, Schema> findSchemasById(long variableId, TimeWindow timeWindow) {
        return findSchemasById(Sets.newHashSet(variableId), timeWindow).getOrDefault(variableId,
                Collections.emptyMap());
    }

    @Override
    public Map<Long, Map<TimeWindow, Schema>> findSchemasById(Set<Long> variableIds, TimeWindow timeWindow) {
        Map<Long, Map<String, Set<TimeWindow>>> schemas = getHttpClient().findSchemas(
                variableIds, getStartTime(timeWindow), getEndTime(timeWindow));

        Map<Long, Map<Schema, Set<TimeWindow>>> withParsedSchemas = Maps.transformValues(schemas, map ->
                MapUtils.mapKeys(map, AvroUtils::parseSchema));

        return Maps.transformValues(withParsedSchemas, MapUtils::revertMap);
    }

    private <T> T modifyWith(Supplier<T> modifier) {
        //This method is mutating the variable, thus invalidating all cached values.
        //There is no way to invalidate just one variable as we cache query results for findOne that can be many
        //leading to the same variable.
        invalidateAllCache();
        return modifier.get();
    }

    private Instant getStartTime(TimeWindow timeWindow) {
        return TimeUtils.getInstantFromNanos(timeWindow.getStartTimeNanos());
    }

    private Instant getEndTime(TimeWindow timeWindow) {
        return TimeUtils.getInstantFromNanos(timeWindow.getEndTimeNanos());
    }
}
