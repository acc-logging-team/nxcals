/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.extraction.metadata.feign.FeignBuilderProvider;
import cern.nxcals.api.extraction.metadata.security.AuthTokenProvider;
import cern.nxcals.api.extraction.metadata.security.AuthenticationException;
import cern.nxcals.api.extraction.metadata.security.KerberosTokenProvider;
import cern.nxcals.api.extraction.metadata.security.PropertiesKeys;
import cern.nxcals.api.extraction.metadata.security.RbacTokenProvider;
import cern.nxcals.api.extraction.metadata.security.SecurityAwareFeignClient;
import cern.nxcals.api.extraction.metadata.security.kerberos.GssKerberosContext;
import cern.nxcals.api.extraction.metadata.security.validation.ServiceUrlSanitizer;
import cern.nxcals.common.utils.ConfigHolder;
import cern.nxcals.common.utils.Lazy;
import com.google.common.annotations.VisibleForTesting;
import com.netflix.config.ConfigurationManager;
import com.typesafe.config.Config;
import feign.Logger;
import feign.ribbon.RibbonClient;
import feign.slf4j.Slf4jLogger;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.ssl.SSLContexts;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static cern.nxcals.common.Constants.KERBEROS_AUTH_DISABLE;
import static cern.nxcals.common.Constants.RBAC_AUTH_DISABLE;
import static cern.nxcals.common.Constants.SERVICE_TYPE;
import static cern.nxcals.common.utils.ConfigHolder.getConfig;
import static cern.nxcals.common.utils.ConfigHolder.getConfigIfPresent;
import static java.lang.String.format;

/**
 * Abstract base class for Feign clients. Knows how to configure the whole chain of serialization and security.
 */
@Slf4j
@RequiredArgsConstructor
class AbstractClientFactory {
    private static final String SERVICE_URL_CONFIG = "service.url";
    private static final String TRUST_STORE_RESOURCE_NAME = "nxcals_cacerts.trs";
    private static final String TRUST_STORE_PASS = "nxcals";
    private static final String MISSING_SERVICE_URL_PROPERTY_MESSAGE = format(
            "Cannot find list of NXCALS service urls! No property [%s] was specified.", SERVICE_URL_CONFIG);
    private static final ServiceUrlSanitizer SERVICE_URL_SANITIZER = ServiceUrlSanitizer.INSTANCE;

    private final String serviceUrl;
    private final RibbonClient client;

    AbstractClientFactory() {
        this.serviceUrl = createServiceURL();
        this.client = RibbonClient.builder().delegate(createClientDelegate()).build();
    }

    private SecurityAwareFeignClient createClientDelegate() {
        SecurityAwareFeignClient.SecurityAwareFeignClientBuilder builder = SecurityAwareFeignClient.builder()
                .authProviders(getAuthTokenProviders());
        if (noExternalTrustStoreAvailable()) {
            builder.sslContextFactory(getContextFactoryWithInternalTrustStore());
        }
        return builder.build();
    }

    private List<AuthTokenProvider> getAuthTokenProviders() {
        boolean kerberosEnabled = !ConfigHolder.getBoolean(KERBEROS_AUTH_DISABLE, false);
        boolean rbacEnabled = !ConfigHolder.getBoolean(RBAC_AUTH_DISABLE, false);

        return getAuthTokenProviders(kerberosEnabled, rbacEnabled);
    }

    @VisibleForTesting
    List<AuthTokenProvider> getAuthTokenProviders(boolean kerberosEnabled, boolean rbacEnabled) {
        if (!rbacEnabled && !kerberosEnabled) {
            throw new AuthenticationException(String.format(
                    "Both RBAC and Kerberos authentication mechanisms were disabled! Only one of '%s', '%s' properties can be set to true.",
                    RBAC_AUTH_DISABLE, KERBEROS_AUTH_DISABLE));
        }

        List<AuthTokenProvider> providers = new ArrayList<>();
        if (rbacEnabled) {
            providers.add(getRbacTokenProvider());
        }
        if (kerberosEnabled) {
            providers.add(getKerberosTokenProvider());
        }

        return providers;
    }

    private RbacTokenProvider getRbacTokenProvider() {
        return RbacTokenProvider.builder().build();
    }

    private KerberosTokenProvider getKerberosTokenProvider() {
        return KerberosTokenProvider.builder()
                .serviceType(SERVICE_TYPE)
                .kerberosContext(new Lazy<>(() -> new GssKerberosContext(
                        getUserPrincipalProperty(),
                        getKeytabLocation(),
                        getKerberosLoginOptions()))).build();
    }

    private String getKeytabLocation() {
        return ConfigHolder
                .getProperty(PropertiesKeys.USER_KEYTAB_PATH_CONFIG.getPathInProperties(), null);
    }

    private String getUserPrincipalProperty() {
        return ConfigHolder
                .getProperty(PropertiesKeys.USER_PRINCIPAL_PATH.getPathInProperties(), null);
    }

    <T> T createServiceFor(Class<T> clazz) {
        return FeignBuilderProvider.INSTANCE.get()
                .client(client)
                .logger(new Slf4jLogger(clazz))
                .logLevel(Logger.Level.BASIC)
                .target(clazz, serviceUrl);
    }

    private String createServiceURL() {
        Config config = getConfig();
        if (!config.hasPath(SERVICE_URL_CONFIG)) {
            throw new IllegalStateException(MISSING_SERVICE_URL_PROPERTY_MESSAGE);
        }
        ConfigurationManager.getConfigInstance().setProperty("nxcals-service.ribbon.listOfServers",
                SERVICE_URL_SANITIZER.apply(config.getString(SERVICE_URL_CONFIG)));
        return "https://nxcals-service";
    }

    private Map<String, Object> getKerberosLoginOptions() {
        return getConfigIfPresent("kerberos.login")
                .entrySet()
                .stream()
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        entry -> entry.getValue().unwrapped()));
    }

    private SSLSocketFactory getContextFactoryWithInternalTrustStore() {
        log.debug("No system properties found for trustStore! Will try to load default file from resources.");
        try {
            URL trustStoreResource = Thread.currentThread().getContextClassLoader()
                    .getResource(TRUST_STORE_RESOURCE_NAME);
            SSLContext sslContext = SSLContexts.custom()
                    .loadTrustMaterial(trustStoreResource, TRUST_STORE_PASS.toCharArray())
                    .build();
            return sslContext.getSocketFactory();
        } catch (Exception e) {
            log.error("Could not load default SSL trustStore file from resources!", e);
            throw new IllegalStateException(e);
        }
    }

    private boolean noExternalTrustStoreAvailable() {
        return System.getProperty("javax.net.ssl.trustStore") == null;
    }
}
