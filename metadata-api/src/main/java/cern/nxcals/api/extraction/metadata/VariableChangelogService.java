/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.domain.VariableChangelog;
import cern.nxcals.api.extraction.metadata.queries.VariableChangelogs;

public interface VariableChangelogService extends Queryable<VariableChangelog, VariableChangelogs> {
}