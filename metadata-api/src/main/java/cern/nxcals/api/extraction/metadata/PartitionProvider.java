/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.domain.Partition;
import cern.nxcals.api.extraction.metadata.feign.PartitionClient;
import cern.nxcals.api.extraction.metadata.queries.Partitions;
import cern.nxcals.internal.extraction.metadata.InternalPartitionService;
import lombok.NonNull;

import java.util.Optional;

/**
 * Implementation of the PartitionService
 */
class PartitionProvider extends AbstractProvider<Partition, PartitionClient, Partitions> implements InternalPartitionService {
    PartitionProvider(PartitionClient httpClient) {
        super(httpClient);
    }

    @Override
    public Optional<Partition> findById(long id) {
        return findOne(Partitions.suchThat().id().eq(id));
    }

    @Override
    public Partition update(@NonNull Partition partition) {
        return getHttpClient().update(partition);
    }

    @Override
    public Partition create(@NonNull Partition partition) {
        return getHttpClient().create(partition);
    }
}