/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.domain.EntitySchema;
import cern.nxcals.api.extraction.metadata.feign.EntitySchemaClient;
import cern.nxcals.api.extraction.metadata.queries.EntitySchemas;
import cern.nxcals.internal.extraction.metadata.InternalEntitySchemaService;
import lombok.NonNull;

import java.util.Optional;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;

class EntitySchemaProvider extends AbstractProvider<EntitySchema, EntitySchemaClient, EntitySchemas> implements InternalEntitySchemaService {
    EntitySchemaProvider(EntitySchemaClient httpClient) {
        super(httpClient);
    }

    @Override
    public Optional<EntitySchema> findById(long schemaId) {
        return findOne(EntitySchemas.suchThat().id().eq(schemaId));
    }

    @Override
    public Optional<EntitySchema> findOne(@NonNull Condition<EntitySchemas> condition) {
        return findOneWithCache(condition);
    }
}