package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.domain.HierarchyChangelog;
import cern.nxcals.api.extraction.metadata.feign.HierarchyChangelogClient;
import cern.nxcals.api.extraction.metadata.queries.HierarchyChangelogs;
import cern.nxcals.internal.extraction.metadata.InternalHierarchyChangelogService;

import java.util.Optional;

/**
 * Implementation of {@link InternalHierarchyChangelogService} for getting {@link HierarchyChangelog} objects
 */
public class HierarchyChangelogProvider
        extends AbstractProvider<HierarchyChangelog, HierarchyChangelogClient, HierarchyChangelogs>
        implements InternalHierarchyChangelogService {

    HierarchyChangelogProvider(HierarchyChangelogClient httpClient) {
        super(httpClient);
    }

    @Override
    public Optional<HierarchyChangelog> findById(long id) {
        return findOne(HierarchyChangelogs.suchThat().id().eq(id));
    }
}
