package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.domain.PartitionResourceHistory;
import cern.nxcals.api.extraction.metadata.queries.PartitionResourceHistories;

public interface PartitionResourceHistoryService extends Queryable<PartitionResourceHistory, PartitionResourceHistories> {
    /**
     * Creates PartitionResourceHistory as in the parameter
     * NOTE: this is a mutative method. It is not thread-safe or meant to be used in parallel
     *
     * @param partitionResourceInfo to be created
     * @return Created PartitionResourceHistory
     */
    PartitionResourceHistory create(PartitionResourceHistory partitionResourceInfo);

    /**
     * Updates an existing PartitionResourceHistory to the new values as in the parameter
     * NOTE: this is a mutative method. It is not thread-safe or meant to be used in parallel
     *
     * @param partitionResourceInfo to be updated
     * @return Updated PartitionResource
     */
    PartitionResourceHistory update(PartitionResourceHistory partitionResourceInfo);

    /**
     * Deletes an existing PartitionResourceHistory
     *
     * @param partitionResourceInfoId the id of the PartitionResourceInfo to be deleted
     */
    void delete(long partitionResourceInfoId);

}
