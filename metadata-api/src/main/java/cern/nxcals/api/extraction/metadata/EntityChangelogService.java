/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.domain.EntityChangelog;
import cern.nxcals.api.extraction.metadata.queries.EntityChangelogs;

public interface EntityChangelogService extends Queryable<EntityChangelog, EntityChangelogs> {
}