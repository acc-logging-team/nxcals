package cern.nxcals.internal.extraction.metadata;

import cern.nxcals.api.extraction.metadata.VariableService;

public interface InternalVariableService extends VariableService {
}
