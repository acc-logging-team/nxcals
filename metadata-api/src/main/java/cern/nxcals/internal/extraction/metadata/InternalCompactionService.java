package cern.nxcals.internal.extraction.metadata;

import cern.nxcals.common.domain.DataProcessingJob;
import cern.nxcals.common.domain.DataProcessingJob.JobType;

import java.util.List;

/**
 * Used from compactor to generate a next N of data process jobs.
 */
public interface InternalCompactionService {
    <T extends DataProcessingJob> List<T> getDataProcessJobs(int maxJobs, JobType jobType);
}
