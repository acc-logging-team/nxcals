package cern.nxcals.internal.extraction.metadata;

import cern.nxcals.api.extraction.metadata.EntityChangelogService;

public interface InternalEntityChangelogService extends EntityChangelogService {
}
