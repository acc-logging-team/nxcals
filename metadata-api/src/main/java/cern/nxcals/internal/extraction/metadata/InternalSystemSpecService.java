package cern.nxcals.internal.extraction.metadata;

import cern.nxcals.api.extraction.metadata.SystemSpecService;

public interface InternalSystemSpecService extends SystemSpecService {
}
