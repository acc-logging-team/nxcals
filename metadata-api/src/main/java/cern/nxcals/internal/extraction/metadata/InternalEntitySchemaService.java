package cern.nxcals.internal.extraction.metadata;

import cern.nxcals.api.extraction.metadata.EntitySchemaService;

public interface InternalEntitySchemaService extends EntitySchemaService {
}
