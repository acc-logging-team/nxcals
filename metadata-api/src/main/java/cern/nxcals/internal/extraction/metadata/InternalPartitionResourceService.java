package cern.nxcals.internal.extraction.metadata;

import cern.nxcals.api.extraction.metadata.PartitionResourceService;

public interface InternalPartitionResourceService extends PartitionResourceService {
}
