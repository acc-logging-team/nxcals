package cern.nxcals.internal.extraction.metadata;

import cern.nxcals.common.domain.ExtractionCriteria;
import cern.nxcals.common.domain.ExtractionUnit;

public interface InternalEntityResourceService {
    ExtractionUnit findBy(ExtractionCriteria criteria);
}
