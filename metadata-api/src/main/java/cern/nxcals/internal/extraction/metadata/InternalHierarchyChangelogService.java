package cern.nxcals.internal.extraction.metadata;

import cern.nxcals.api.extraction.metadata.HierarchyChangelogService;

public interface InternalHierarchyChangelogService extends HierarchyChangelogService {
}
