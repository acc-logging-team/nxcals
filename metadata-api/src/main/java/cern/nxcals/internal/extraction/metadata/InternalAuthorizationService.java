package cern.nxcals.internal.extraction.metadata;

import cern.nxcals.api.custom.domain.DelegationToken;

public interface InternalAuthorizationService {
    DelegationToken createDelegationToken();
}
