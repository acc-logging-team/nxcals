package cern.nxcals.internal.extraction.metadata;

import cern.nxcals.api.extraction.metadata.VariableChangelogService;

public interface InternalVariableChangelogService extends VariableChangelogService {
}
