/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.internal.extraction.metadata;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.KeyValues;
import cern.nxcals.api.extraction.metadata.EntityService;

public interface InternalEntityService extends EntityService {
    /**
     * Finds or creates an entity for given ids. In this case only the history is sometimes created (depending on the passed timestamp).
     *
     * @param systemId
     * @param entityId
     * @param partitionId
     * @param schema
     * @param timestamp
     * @return
     */
    Entity findOrCreateEntityFor(long systemId, long entityId, long partitionId, String schema, long timestamp);

    Entity findOrCreateEntityFor(long systemId, KeyValues entityCachedRequest,
                                 KeyValues partitionKeyValues, String recordFieldDefinitions, long recordTimestamp);

    /**
     * Extends entity's history for a given schema in a given time window
     *
     * @param entityId entity's identifier
     * @param schema   entity's schema to be used in the given time window
     * @param from     start of the time window
     * @return modified {@link Entity}
     */
    Entity extendEntityFirstHistoryDataFor(long entityId, String schema, long from);

    void invalidateAllCache();

    void invalidateCacheFor(Entity entity);

}
