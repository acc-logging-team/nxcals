/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.internal.extraction.metadata;

import cern.nxcals.api.domain.Partition;
import cern.nxcals.api.extraction.metadata.PartitionService;

public interface InternalPartitionService extends PartitionService {
    Partition create(Partition partition);
    Partition update(Partition partition);
}
