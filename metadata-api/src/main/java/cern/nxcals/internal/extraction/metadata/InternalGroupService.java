package cern.nxcals.internal.extraction.metadata;


import cern.nxcals.api.extraction.metadata.GroupService;

public interface InternalGroupService extends GroupService {
}
