package cern.nxcals.internal.extraction.metadata;

import cern.nxcals.api.extraction.metadata.PartitionResourceHistoryService;

public interface InternalPartitionResourceHistoryService extends PartitionResourceHistoryService {
}

