package cern.nxcals.internal.extraction.metadata;

import cern.nxcals.api.extraction.metadata.VariableConfigChangelogService;

public interface InternalVariableConfigChangelogService extends VariableConfigChangelogService {
}
