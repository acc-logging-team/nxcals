package cern.nxcals.api.extraction.metadata.feign;

import cern.nxcals.api.extraction.data.builders.fluent.ThreadLocalStorage;
import cern.nxcals.common.domain.CallDetails;
import feign.RequestTemplate;
import org.junit.jupiter.api.Test;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;


class CallDetailsInterceptorTest {

    @Test
    void shouldApplyHeaders() {

        final String APP_NAME = "Application";
        final String DECLARING_CLASS = "Class";
        final String METHOD_NAME = "Method";

        CallDetails callDetails = CallDetails.builder().applicationName(APP_NAME).declaringClass(DECLARING_CLASS).methodName(METHOD_NAME).build();
        ThreadLocalStorage.setCallDetails(callDetails);

        RequestTemplate requestTemplate = new RequestTemplate();

        CallDetailsInterceptor callDetailsInterceptor = new CallDetailsInterceptor();
        callDetailsInterceptor.apply(requestTemplate);

        assertEquals( Arrays.asList(APP_NAME, DECLARING_CLASS, METHOD_NAME),
                requestTemplate.headers().values().stream().flatMap(Collection::stream).collect(Collectors.toList()));
    }
}