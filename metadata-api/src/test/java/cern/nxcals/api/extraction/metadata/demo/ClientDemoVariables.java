/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.api.extraction.metadata.demo;

import avro.shaded.com.google.common.collect.ImmutableSortedSet;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.api.extraction.metadata.InternalServiceClientFactory;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import cern.nxcals.internal.extraction.metadata.InternalSystemSpecService;
import cern.nxcals.internal.extraction.metadata.InternalVariableService;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Created by ntsvetko on 3/10/17.
 */
public class ClientDemoVariables {

    static {
        String user = System.getProperty("user.name");
        System.setProperty("service.url", "https://nxcals-" + user + "1.cern.ch:19093");
    }

    static VariableConfig createVariableConfigData(long entityId, String fieldName, Instant validFromStamp,
            Instant validToStamp) {
        return VariableConfig.builder().entityId(entityId).fieldName(fieldName)
                .validity(TimeWindow.between(validFromStamp, validToStamp)).build();
    }

    private static Variable createVariableData(String variableName, SystemSpec systemSpec, String description,
            String unit, SortedSet<VariableConfig> configs) {
        return Variable.builder().variableName(variableName).systemSpec(systemSpec).description(description).unit(unit)
                .configs(configs).build();
    }

    public static void main(String[] args) {
        final String MOCK_SYSTEM_NAME = "CMW-MIG-DEV-NTSVETKO";
        InternalSystemSpecService systemService = InternalServiceClientFactory.createSystemSpecService();
        SystemSpec systemData = systemService.findByName(MOCK_SYSTEM_NAME)
                .orElseThrow(() -> new IllegalArgumentException("No such system name " + MOCK_SYSTEM_NAME));

        InternalVariableService variableService = InternalServiceClientFactory.createVariableService();
        Variable variableData = variableService.findOne(
                Variables.suchThat().systemName().eq(systemData.getName()).and().variableName().eq("TestVariable"))
                .orElseThrow(() -> new IllegalArgumentException("No such variable name TestVariable"));
        System.out.println("FOUND variable: " + variableData);

        Instant t1 = Instant.now().minus(5, ChronoUnit.DAYS);
        Instant t2 = Instant.now().minus(2, ChronoUnit.DAYS);

        VariableConfig config1 = createVariableConfigData(10066, "field3", null, t1);
        VariableConfig config2 = createVariableConfigData(10066, "field2", t1, t2);
        VariableConfig config3 = createVariableConfigData(10066, "field4", t2, null);

        SortedSet<VariableConfig> configs = ImmutableSortedSet.of(config1, config2, config3);

        Variable varData = createVariableData("TestVariable4", systemData, "Description", "unit", configs);
        Variable var1 = variableService.create(varData);
        System.out.println("Updated variable data: " + var1);

        Variable testVariable = variableService.findOne(
                Variables.suchThat().systemName().eq(systemData.getName()).and().variableName()
                        .eq(var1.getVariableName()))
                .orElseThrow(() -> new IllegalArgumentException("No such variable name " + var1.getVariableName()));

        Instant splitTime = t1.minus(3, ChronoUnit.HOURS);

        SortedSet<VariableConfig> newConfig = new TreeSet<>();
        VariableConfig firstConf = testVariable.getConfigs().first();
        newConfig.add(firstConf.toBuilder()
                .validity(TimeWindow.between(firstConf.getValidity().getStartTime(), splitTime)).build());
        newConfig
                .add(firstConf.toBuilder().validity(TimeWindow.between(splitTime, firstConf.getValidity().getEndTime()))
                        .build());

        Variable updatedVariable = testVariable.toBuilder().configs(newConfig).build();

        Variable var3 = variableService.update(updatedVariable);

        System.err.println(var3);
    }

}
