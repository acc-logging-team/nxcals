/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.api.extraction.metadata.demo;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.KeyValues;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.api.extraction.metadata.EntityService;
import cern.nxcals.api.extraction.metadata.InternalServiceClientFactory;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import cern.nxcals.internal.extraction.metadata.InternalEntityResourceService;
import cern.nxcals.internal.extraction.metadata.InternalEntitySchemaService;
import cern.nxcals.internal.extraction.metadata.InternalEntityService;
import cern.nxcals.internal.extraction.metadata.InternalPartitionService;
import cern.nxcals.internal.extraction.metadata.InternalSystemSpecService;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSortedSet;
import org.apache.commons.lang3.RandomStringUtils;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.SortedSet;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ClientDemo {
    protected static final InternalSystemSpecService internalSystemService = InternalServiceClientFactory
            .createSystemSpecService();
    protected static final EntityService entityService = ServiceClientFactory.createEntityService();
    protected static final InternalEntityService internalEntityService = InternalServiceClientFactory
            .createEntityService();
    protected static final InternalPartitionService internalPartitionService = InternalServiceClientFactory
            .createPartitionService();
    protected static final VariableService variableService = ServiceClientFactory.createVariableService();
    protected static final InternalEntitySchemaService internalSchemaService = InternalServiceClientFactory
            .createEntitySchemaService();
    protected static final InternalEntityResourceService entitiesResourcesService = InternalServiceClientFactory
            .createEntityResourceService();
    static final String MOCK_SYSTEM_NAME = "MOCK-SYSTEM";
    static final long RECORD_TIMESTAMP = 1476789831111222334L;
    private static final String RANDOM_STRING = RandomStringUtils.randomAscii(64);
    static final Map<String, Object> ENTITY_KEY_VALUES = ImmutableMap.of("device", "device_value" + RANDOM_STRING);
    static final Map<String, Object> ENTITY_KEY_VALUES_RESOURCES_TEST = ImmutableMap
            .of("device", "device_value_resources_test" + RANDOM_STRING);
    static final Map<String, Object> ENTITY_KEY_VALUES_SCHEMA_TEST = ImmutableMap
            .of("device", "device_value_schema_test" + RANDOM_STRING);
    static final Map<String, Object> ENTITY_KEY_VALUES_VARIABLE_TEST = ImmutableMap
            .of("device", "device_value_variable_test" + RANDOM_STRING);
    static final Map<String, Object> PARTITION_KEY_VALUES = ImmutableMap
            .of("specification", "devClass1" + RANDOM_STRING);
    static InternalSystemSpecService systemService = InternalServiceClientFactory.createSystemSpecService();
    static final SystemSpec systemData = systemService.findByName(MOCK_SYSTEM_NAME)
            .orElseThrow(() -> new IllegalArgumentException("No such system name " + MOCK_SYSTEM_NAME));
    static String variableName = "VARIABLE_NAME" + RANDOM_STRING;

    //    protected static final SystemService systemService = ServiceClientFactory.createSystemSpecService();
    //    protected static final InternalSystemService internalSystemService = InternalServiceClientFactory
    //            .createSystemSpecService();
    //    protected static final EntityService entityService = ServiceClientFactory.createEntityService();
    //    protected static final InternalEntityService internalEntityService = InternalServiceClientFactory
    //            .createEntityService();
    //    protected static final InternalPartitionService internalPartitionService = InternalServiceClientFactory
    //            .createPartitionService();
    //    protected static final VariableService variableService = ServiceClientFactory.createVariableService();
    //    protected static final InternalSchemaService internalSchemaService = InternalServiceClientFactory
    //            .createEntitySchemaService();
    //    protected static final EntitiesResourcesService entitiesResourcesService = ServiceClientFactory
    //            .createEntityResourceService();
    static {
        try {
            System.setProperty("service.url", "https://" + InetAddress.getLocalHost().getHostName() + ":19093");
        } catch (UnknownHostException exception) {
            throw new RuntimeException(
                    "Cannot acquire hostname programmatically, provide the name full name of localhost");
        }

        System.setProperty("kerberos.principal", "mock-system-user");
        System.setProperty("kerberos.keytab", ".service.keytab");

        System.setProperty("java.security.krb5.conf", "build/LocalKdc/krb5.conf");

        System.setProperty("javax.net.ssl.trustStore", "selfsigned.jks");
        System.setProperty("javax.net.ssl.trustStorePassword", "nxcals");
    }

    public static void main(String[] args) {
        //lets first find some existing entity for which we will create variable

        KeyValues entityKeyValues = new KeyValues(0, ENTITY_KEY_VALUES_VARIABLE_TEST);
        KeyValues partitionKeyValues = new KeyValues(1, PARTITION_KEY_VALUES);

        Entity entityData = internalEntityService
                .findOrCreateEntityFor(systemData.getId(), entityKeyValues, partitionKeyValues, "brokenSchema1",
                        RECORD_TIMESTAMP);

        assertNotNull(entityData);

        VariableConfig varConfData1 = VariableConfig.builder().entityId(entityData.getId()).build();
        SortedSet<VariableConfig> varConfSet = ImmutableSortedSet.of(varConfData1);
        Variable varData = Variable.builder().variableName(variableName).systemSpec(entityData.getSystemSpec())
                .description("Description").configs(varConfSet).build();
        Variable variableData = variableService.create(varData);
        assertNotNull(variableData);
        assertEquals(variableName, variableData.getVariableName());

        Variable foundVariable = variableService.findOne(
                Variables.suchThat().systemName().eq(systemData.getName()).and().variableName().eq(variableName))
                .orElseThrow(() -> new IllegalArgumentException("No such variable name " + variableName));
        assertEquals(variableName, foundVariable.getVariableName());

    }
}
