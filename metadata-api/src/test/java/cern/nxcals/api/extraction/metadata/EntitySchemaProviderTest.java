/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.domain.EntitySchema;
import cern.nxcals.api.extraction.metadata.feign.EntitySchemaClient;
import cern.nxcals.api.extraction.metadata.queries.EntitySchemas;
import cern.nxcals.common.utils.ReflectionUtils;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.google.common.collect.Sets;
import org.apache.avro.Schema;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Marcin Sobieszek
 * @author jwozniak
 * @date Jul 22, 2016 11:39:57 AM
 */
@ExtendWith(MockitoExtension.class)
public class EntitySchemaProviderTest extends AbstractProviderTest {
    private EntitySchemaProvider schemaProvider;

    @Mock
    private EntitySchemaClient httpService;

    @BeforeEach
    public void setup() {
        schemaProvider = new EntitySchemaProvider(httpService);
        reset(httpService);
    }

    @Test
    public void shouldNotObtainSchemaForNonExistingId() {
        Condition<EntitySchemas> condition = EntitySchemas.suchThat().id().eq(-1L);
        String queryString = toRSQL(condition);

        when(this.httpService.findAll(queryString)).thenReturn(Collections.emptySet());

        EntitySchema data = this.schemaProvider.findOne(condition).orElse(null);

        assertNull(data);
        verify(httpService, times(1)).findAll(queryString);
    }

    @Test
    public void shouldGetSchemaDataById() {
        Condition<EntitySchemas> condition = EntitySchemas.suchThat().id().eq(1L);
        String queryString = toRSQL(condition);


        when(httpService.findAll(queryString)).thenReturn(
                Sets.newHashSet(createSchema(1, SCHEMA1)));
        EntitySchema data = schemaProvider.findOne(condition).orElseThrow(() -> new IllegalArgumentException("No such schema id 1"));
        assertEquals(1, data.getId());
        assertEquals(SCHEMA1.toString(), data.getSchemaJson());

        //testing caching
        data = schemaProvider.findOne(condition).orElseThrow(() -> new IllegalArgumentException("No such schema id 1"));
        assertEquals(1, data.getId());
        assertEquals(SCHEMA1.toString(), data.getSchemaJson());
        verify(httpService, times(1)).findAll(queryString);

        reset(httpService);

        Condition<EntitySchemas> condition2 = EntitySchemas.suchThat().id().eq(2L);
        String queryString2 = toRSQL(condition2);
        when(httpService.findAll(queryString2))
                .thenReturn(Sets.newHashSet(createSchema(2, SCHEMA2)));
        data = schemaProvider.findOne(condition2).orElseThrow(() -> new IllegalArgumentException("No such schema id 2"));
        assertEquals(2, data.getId());
        assertEquals(SCHEMA2.toString(), data.getSchemaJson());

        //testing caching
        data = schemaProvider.findOne(condition2).orElseThrow(() -> new IllegalArgumentException("No such schema id 2"));
        assertEquals(2, data.getId());
        assertEquals(SCHEMA2.toString(), data.getSchemaJson());
        verify(httpService, times(1)).findAll(queryString2);
    }

    private EntitySchema createSchema(long i, Schema schema1) {
        return ReflectionUtils.builderInstance(EntitySchema.InnerBuilder.class).id(i).schemaJson(schema1.toString()).build();
    }
}
