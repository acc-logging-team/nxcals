package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.Group;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.Visibility;
import cern.nxcals.api.extraction.metadata.feign.GroupClient;
import cern.nxcals.api.extraction.metadata.queries.Groups;
import cern.nxcals.common.utils.ReflectionUtils;
import com.google.common.collect.Sets;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@ExtendWith(MockitoExtension.class)
public class GroupProviderTest {
    private GroupProvider groupProvider;

    @Mock
    private GroupClient httpClient;
    private SystemSpec system;

    @BeforeEach
    public void init() {
        groupProvider = new GroupProvider(httpClient);
        system = systemSpec("my-system");
        reset(httpClient);
    }

    @Test
    public void shouldCreate() {
        final long id = 1L;
        final long recVersion = 2L;
        final String name = "my-group";

        Group groupOld = group(name, "my-group-label");

        when(httpClient.create(groupOld)).thenReturn(group(name, id, recVersion, "my-group-label"));

        Group groupNew = groupProvider.create(groupOld);

        verify(httpClient, times(1)).create(eq(groupOld));
        assertThat(groupNew).isEqualTo(group(name, id, recVersion, "my-group-label"));
        assertThat(groupNew.getId()).isEqualTo(id);
        assertThat(groupNew.getRecVersion()).isEqualTo(recVersion);
        assertThat(groupNew.getLabel()).isEqualTo("my-group-label");
    }

    @Test
    public void shouldUpdate() {
        final long id = 1L;
        final long recVersion = 2L;
        final String name = "my-group";

        Group groupOld = group(name, "my-group-label");

        when(httpClient.update(groupOld)).thenReturn(group(name, id, recVersion, "my-group-label"));

        Group groupNew = groupProvider.update(groupOld);

        verify(httpClient, times(1)).update(eq(groupOld));
        assertThat(groupNew).isEqualTo(group(name, id, recVersion, "my-group-label"));
        assertThat(groupNew.getId()).isEqualTo(id);
        assertThat(groupNew.getRecVersion()).isEqualTo(recVersion);
        assertThat(groupNew.getLabel()).isEqualTo("my-group-label");
    }

    @Test
    public void shouldFindById() {
        final long id = 1L;
        final long recVersion = 2L;
        final String name = "my-group";

        group(name, "my-group-label");

        String condition = toRSQL(Groups.suchThat().id().eq(id));
        when(httpClient.findAll(condition)).thenReturn(Collections.singleton(group(name, id, recVersion, "my-group-label")));

        Optional<Group> groupNew = groupProvider.findById(id);

        assertThat(groupNew.isPresent()).isTrue();

        verify(httpClient, times(1)).findAll(eq(condition));

        assertThat(groupNew.get()).isEqualTo(group(name, id, recVersion, "my-group-label"));
        assertThat(groupNew.get().getId()).isEqualTo(id);
        assertThat(groupNew.get().getRecVersion()).isEqualTo(recVersion);
    }

    @Test
    public void shouldDeleteGroup() {
        final long groupId = 1L;
        doNothing().when(httpClient).delete(groupId);

        groupProvider.delete(groupId);

        verify(httpClient, times(1)).delete(groupId);
    }

    // test: getVariables

    @Test
    public void shouldGetVariables() {
        final long id = 1L;
        final long recVersion = 2L;
        final String name = "my-group";

        Group group = group(name, id, recVersion, "my-group-label");

        Variable variable = mock(Variable.class);
        Map<String, Set<Variable>> variables = new HashMap<>();
        variables.put("vars", Collections.singleton(variable));

        when(httpClient.getVariables(group.getId())).thenReturn(variables);

        Map<String, Set<Variable>> fetchedVariables = groupProvider.getVariables(group.getId());
        assertThat(fetchedVariables).isNotNull();
        assertThat(fetchedVariables).size().isOne();
        assertThat(fetchedVariables.get("vars")).isNotNull();
        assertThat(fetchedVariables.get("vars")).size().isOne();
        assertThat(fetchedVariables.get("vars").iterator().next()).isEqualTo(variable);

        verify(httpClient, times(1)).getVariables(group.getId());
    }

    // test: setVariables

    @Test
    public void shouldSetVariables() {
        final long id = 1L;
        final long recVersion = 2L;
        final String name = "my-group";

        Group group = group(name, id, recVersion, "my-group-label");

        HashSet<Long> variableIds = Sets.newHashSet(1L, 2L);
        Map<String, Set<Long>> variables = new HashMap<>();
        variables.put("vars", variableIds);

        doNothing().when(httpClient).setVariables(group.getId(), variables);
        groupProvider.setVariables(group.getId(), variables);

        verify(httpClient, times(1)).setVariables(group.getId(), variables);
    }

    // test: addVariables

    @Test
    public void shouldAddVariables() {
        final long id = 1L;
        final long recVersion = 2L;
        final String name = "my-group";

        Group group = group(name, id, recVersion, "my-group-label");

        HashSet<Long> variableIds = Sets.newHashSet(1L, 2L);
        Map<String, Set<Long>> variables = new HashMap<>();
        variables.put("vars", variableIds);

        doNothing().when(httpClient).addVariables(group.getId(), variables);
        groupProvider.addVariables(group.getId(), variables);

        verify(httpClient, times(1)).addVariables(group.getId(), variables);
    }

    // test: removeVariables

    @Test
    public void shouldRemoveVariables() {
        final long id = 1L;
        final long recVersion = 2L;
        final String name = "my-group";

        Group group = group(name, id, recVersion, "my-group-label");

        HashSet<Long> variableIds = Sets.newHashSet(1L, 2L);
        Map<String, Set<Long>> variables = new HashMap<>();
        variables.put("vars", variableIds);

        doNothing().when(httpClient).removeVariables(group.getId(), variables);
        groupProvider.removeVariables(group.getId(), variables);

        verify(httpClient, times(1)).removeVariables(group.getId(), variables);
    }

    // test: getEntities

    @Test
    public void shouldGetEntities() {
        final long id = 1L;
        final long recVersion = 2L;
        final String name = "my-group";

        Group group = group(name, id, recVersion, "my-group-label");

        Entity entity = mock(Entity.class);
        Map<String, Set<Entity>> entities = new HashMap<>();
        entities.put("entity", Collections.singleton(entity));

        when(httpClient.getEntities(group.getId())).thenReturn(entities);

        Map<String, Set<Entity>> fetchedEntities = groupProvider.getEntities(group.getId());
        assertThat(fetchedEntities).isNotNull();
        assertThat(fetchedEntities).size().isOne();
        assertThat(fetchedEntities.get("entity")).isNotNull();
        assertThat(fetchedEntities.get("entity")).size().isOne();
        assertThat(fetchedEntities.get("entity").iterator().next()).isEqualTo(entity);

        verify(httpClient, times(1)).getEntities(group.getId());
    }

    // test: setEntities

    @Test
    public void shouldSetEntities() {
        final long id = 1L;
        final long recVersion = 2L;
        final String name = "my-group";

        Group group = group(name, id, recVersion, "my-group-label");

        Set<Long> entityIds = Sets.newHashSet(1L, 2L);
        Map<String, Set<Long>> entities = new HashMap<>();
        entities.put("entities", entityIds);

        doNothing().when(httpClient).setEntities(group.getId(), entities);
        groupProvider.setEntities(group.getId(), entities);

        verify(httpClient, times(1)).setEntities(group.getId(), entities);
    }

    // test: addEntities

    @Test
    public void shouldAddEntities() {
        final long id = 1L;
        final long recVersion = 2L;
        final String name = "my-group";

        Group group = group(name, id, recVersion, "my-group-label");

        Set<Long> entityIds = Sets.newHashSet(1L, 2L);
        Map<String, Set<Long>> entities = new HashMap<>();
        entities.put("entities", entityIds);

        doNothing().when(httpClient).addEntities(group.getId(), entities);
        groupProvider.addEntities(group.getId(), entities);

        verify(httpClient, times(1)).addEntities(group.getId(), entities);
    }

    // test: removeEntities

    @Test
    public void shouldRemoveEntities() {
        final long id = 1L;
        final long recVersion = 2L;
        final String name = "my-group";

        Group group = group(name, id, recVersion, "my-group-label");

        HashSet<Long> entityIds = Sets.newHashSet(1L, 2L);
        Map<String, Set<Long>> entities = new HashMap<>();
        entities.put(null, entityIds);

        doNothing().when(httpClient).removeEntities(group.getId(), entities);
        groupProvider.removeEntities(group.getId(), entities);

        verify(httpClient, times(1)).removeEntities(group.getId(), entities);
    }

    // helper methods

    private Group group(String name, String label) {
        return ReflectionUtils.builderInstance(Group.InnerBuilder.class)
                .label(label)
                .name(name)
                .systemSpec(system)
                .visibility(Visibility.PUBLIC)
                .description("desc")
                .build();
    }

    private Group group(String name, long id, long recVersion, String label) {
        return ReflectionUtils.builderInstance(Group.InnerBuilder.class)
                .id(id)
                .recVersion(recVersion)
                .owner("owner")
                .label(label)
                .name(name)
                .visibility(Visibility.PUBLIC)
                .systemSpec(system)
                .description("desc")
                .build();
    }

    private SystemSpec systemSpec(String name) {
        return SystemSpec.builder().name(name).partitionKeyDefinitions("").timeKeyDefinitions("").entityKeyDefinitions("").build();
    }
}
