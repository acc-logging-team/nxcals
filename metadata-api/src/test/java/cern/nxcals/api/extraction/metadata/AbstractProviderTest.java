/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.metadata;

import com.google.common.collect.ImmutableMap;
import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;

import java.util.Map;

import static cern.nxcals.api.extraction.metadata.TestSchemas.PARTITION_DOUBLE_SCHEMA_KEY;
import static cern.nxcals.api.extraction.metadata.TestSchemas.PARTITION_STRING_SCHEMA_KEY;

/**
 * Base class with constants for all tests.
 */
public abstract class AbstractProviderTest {
    static final Schema PARTITION_SCHEMA = SchemaBuilder.record("test").fields()
            .name("name").type().stringType().noDefault().endRecord();
    static final Schema ENTITY_SCHEMA = SchemaBuilder.record("test").fields()
            .name("value").type().intType().noDefault().endRecord();
    static final Schema TIME_KEY_SCHEMA = SchemaBuilder.record("test").fields()
            .name("timestamp").type().longType().noDefault().endRecord();
    static final long SYSTEM_ID = -1;
    static final String SYSTEM_NAME = "TEST_SYSTEM";
    static final Map<String, Object> ENTITY_KEY_VALUES = ImmutableMap.of("value", 1);
    static final Map<String, Object> ENTITY_KEY_VALUES1 = ImmutableMap.of("value", 2);
    static final Map<String, Object> PARTITION_KEY_VALUES = ImmutableMap.of("name", "3");
    static final Map<String, Object> PARTITION_KEY_VALUES1 = ImmutableMap.of("name", "4");
    static final long RECORD_TIME = 0;
    static final Schema SCHEMA1 = SchemaBuilder.record("schema1").fields()
            .name("schema1").type().stringType().noDefault().endRecord();
    static final Schema SCHEMA2 = SchemaBuilder.record("schema2").fields()
            .name("schema2").type().stringType().noDefault().endRecord();
    static final Schema SCHEMA3 = SchemaBuilder.record("schema3").fields()
            .name("schema3").type().stringType().noDefault().endRecord();
    static final String PARTITION_SCHEMA2_STRING = SchemaBuilder.record("test").fields()
            .name(PARTITION_STRING_SCHEMA_KEY).type().stringType().noDefault().name(PARTITION_DOUBLE_SCHEMA_KEY).type()
            .doubleType().noDefault().endRecord().toString();
    static final Map<String, Object> PARTITION_KEY_VALUES2 = ImmutableMap
            .of(PARTITION_STRING_SCHEMA_KEY, "string", PARTITION_DOUBLE_SCHEMA_KEY, 2d);

    protected static final String EXCEPTION_MESSAGE = "TEST_EXCEPTION_MESSAGE";
}
