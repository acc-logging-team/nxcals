/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.api.extraction.metadata;

import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;

public class TestSchemas {

    private TestSchemas() {
        //Test static class
    }

    static final String PARTITION_STRING_SCHEMA_KEY = "partition_string";
    static final String PARTITION_STRING_SCHEMA_KEY_1 = "different_partition_string";
    static final String PARTITION_DOUBLE_SCHEMA_KEY = "partition_double";

    static final String ENTITY_STRING_SCHEMA_KEY = "entity_string";
    static final String ENTITY_DOUBLE_SCHEMA_KEY = "entity_double";
    static final String ENTITY_STRING_SCHEMA_KEY_1 = "entity_string_1";
    static final String RECORD_VERSION_STRING_SCHEMA_KEY = "record_version_string";

    public static final Schema RECORD_VERSION_SCHEMA = SchemaBuilder.record("test_type").fields()
            .name(RECORD_VERSION_STRING_SCHEMA_KEY).type().stringType().noDefault()
            .endRecord();
}
