/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.EntityHistory;
import cern.nxcals.api.domain.EntitySchema;
import cern.nxcals.api.domain.KeyValues;
import cern.nxcals.api.domain.Partition;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.exceptions.FatalDataConflictRuntimeException;
import cern.nxcals.api.extraction.metadata.feign.EntityClient;
import cern.nxcals.api.extraction.metadata.queries.Entities;
import cern.nxcals.api.extraction.metadata.queries.SystemSpecs;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.common.domain.FindOrCreateEntityRequest;
import cern.nxcals.common.utils.ReflectionUtils;
import cern.nxcals.internal.extraction.metadata.InternalSystemSpecService;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.google.common.collect.Sets;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.function.Function;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static java.util.Collections.emptySet;
import static java.util.Collections.emptySortedSet;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class EntityProviderTest extends AbstractProviderTest {
    private EntityProvider entityProvider;

    @Mock
    private EntityClient httpClient;

    @Mock
    private InternalSystemSpecService systemService;

    @BeforeEach
    public void setup() {
        entityProvider = new EntityProvider(httpClient, systemService);
        reset(httpClient);
    }

    private Entity createEntityData(long id, Function<Entity, SortedSet<EntityHistory>> historyMaker,
            Map<String, Object> entityKeyValues) {
        SystemSpec systemSpec = createSystemSpec();

        Partition partition = ReflectionUtils.builderInstance(Partition.InnerBuilder.class).id(1L)
                .systemSpec(systemSpec).keyValues(PARTITION_KEY_VALUES).build();

        Entity entity = ReflectionUtils.builderInstance(Entity.InnerBuilder.class).id(id).entityKeyValues(entityKeyValues).systemSpec(systemSpec)
                .partition(partition).unlock().recVersion(0).build();
        return entity.toBuilder().entityHistory(historyMaker.apply(entity)).build();
    }

    private SystemSpec createSystemSpec() {
        return ReflectionUtils.builderInstance(SystemSpec.InnerBuilder.class).id(1)
                .name(SYSTEM_NAME).entityKeyDefinitions(ENTITY_SCHEMA.toString())
                        .partitionKeyDefinitions(PARTITION_SCHEMA.toString())
                        .timeKeyDefinitions(TIME_KEY_SCHEMA.toString()).build();
    }

    private Entity createEntityDataWithHistory(long id, Map<String, Object> entityKeyValues,
            Map<String, Object> partitionKeyValues, List<String> schemas) {
        return createEntityDataWithHistoryWithTimeDiff(id, entityKeyValues, partitionKeyValues, schemas, 1);
    }

    private Entity createEntityDataWithHistoryWithTimeDiff(long id, Map<String, Object> entityKeyValues,
            Map<String, Object> partitionKeyValues, List<String> schemas, long histTimeDifference) {

        Function<Entity, SortedSet<EntityHistory>> historyMaker = entity -> {
            SortedSet<EntityHistory> histData = new TreeSet<>();
            for (int i = 0; i < schemas.size(); i++) {
                histData.add(createHistory(entity, TimeWindow
                                .between(TimeUtils.getInstantFromNanos(i * histTimeDifference), i == schemas.size() - 1 ?
                                        null :
                                        TimeUtils.getInstantFromNanos((i + 1) * histTimeDifference)), i, schemas.get(i),
                        partitionKeyValues));
            }
            return histData;
        };

        return createEntityData(id, historyMaker, entityKeyValues);
    }

    @Test
    public void shouldNotObtainEntityDataForNonExistingEntityKeyValues() {
        Entity keyData = createEntityDataWithHistory(0, ENTITY_KEY_VALUES, PARTITION_KEY_VALUES,
                Collections.singletonList(SCHEMA1.toString()));

        Condition<Entities> queryCondition = Entities.suchThat().systemId().eq(keyData.getSystemSpec().getId()).and()
                .keyValues().eq(keyData.getSystemSpec(), keyData.getEntityKeyValues());
        String queryString = toRSQL(queryCondition);

        when(httpClient.findAll(queryString)).thenReturn(emptySet());
        Entity data = entityProvider.findOne(queryCondition).orElse(null);
        assertNull(data);
        verify(httpClient, times(1)).findAll(queryString);
    }

    @Test
    public void shouldNotObtainEntityDataForExistingEntityKeyValuesFromCache() {
        Entity keyData = createEntityData(0, e -> emptySortedSet(), ENTITY_KEY_VALUES);

        Condition<Entities> queryCondition = Entities.suchThat().systemId().eq(keyData.getSystemSpec().getId()).and()
                .keyValues().eq(keyData.getSystemSpec(), keyData.getEntityKeyValues());
        String queryString = toRSQL(queryCondition);

        when(httpClient.findAll(queryString)).thenReturn(Sets.newHashSet(keyData));

        for (int i = 0; i < 10; i++) {
            Entity data = entityProvider.findOne(queryCondition)
                    .orElseThrow(() -> new IllegalArgumentException("No such system"));
            assertNotNull(data);
            assertEquals(keyData, data);
        }
        verify(httpClient, times(10)).findAll(queryString);
    }

    @Test
    public void shouldObtainEntityDataForExistingEntityKeyValuesFromCache() {
        Entity keyData = createEntityData(0, e -> emptySortedSet(), ENTITY_KEY_VALUES);

        Condition<Entities> queryCondition = Entities.suchThat().systemId().eq(keyData.getSystemSpec().getId()).and()
                .keyValues().eq(keyData.getSystemSpec(), keyData.getEntityKeyValues());
        String queryString = toRSQL(queryCondition);

        when(httpClient.findAll(queryString)).thenReturn(Sets.newHashSet(keyData));

        for (int i = 0; i < 10; i++) {
            Entity data = entityProvider.findOneWithCache(queryCondition)
                    .orElseThrow(() -> new IllegalArgumentException("No such system"));
            assertNotNull(data);
            assertEquals(keyData, data);
        }
        verify(httpClient, times(1)).findAll(queryString);
    }

    @Test
    public void shouldExtendEntityHistoryDataForMigration() {
        // given
        long entityId = 100;
        List<String> schemas = Arrays.asList(SCHEMA1.toString(), SCHEMA1.toString());
        long histTimeDifference = 1;
        long firstTimestamp = 10;

        Function<Entity, SortedSet<EntityHistory>> historyMaker = e -> {
            SortedSet<EntityHistory> history = new TreeSet<>();
            history.add(createHistory(e, TimeWindow.between(0L, 10L), 0, SCHEMA2.toString(), PARTITION_KEY_VALUES));
            for (int i = 0; i < schemas.size(); i++) {
                TimeWindow between = TimeWindow
                        .between(TimeUtils.getInstantFromNanos(i * histTimeDifference + firstTimestamp),
                                i == schemas.size() - 1 ?
                                        null :
                                        TimeUtils.getInstantFromNanos((i + 1) * histTimeDifference + firstTimestamp));
                EntityHistory entityHistoryData = createHistory(e, between, i + 1, schemas.get(i),
                        PARTITION_KEY_VALUES);
                history.add(entityHistoryData);
            }
            return history;
        };

        Entity keyData2 = createEntityData(entityId, historyMaker, ENTITY_KEY_VALUES);

        // when
        when(httpClient.extendEntityFirstHistoryDataFor(entityId, 0L, SCHEMA2.toString())).thenReturn(keyData2);

        Entity data = entityProvider.extendEntityFirstHistoryDataFor(entityId, SCHEMA2.toString(), 0L);

        // then
        assertNotNull(data);
        assertEquals(keyData2, data);
    }

    private EntityHistory createHistory(Entity entity, TimeWindow between, long id, String s,
            Map<String, Object> partitionKeyValues) {
        return ReflectionUtils.builderInstance(EntityHistory.InnerBuilder.class).id(id).entity(entity).entitySchema(createSchema(s))
                .partition(createPartition(partitionKeyValues)).validity(between).build();
    }

    private Partition createPartition(Map<String, Object> partitionKeyValues) {
        return ReflectionUtils.builderInstance(Partition.InnerBuilder.class).id(0L).systemSpec(createSystemSpec())
                .keyValues(partitionKeyValues).build();
    }

    private EntitySchema createSchema(String s) {
        return ReflectionUtils.builderInstance(EntitySchema.InnerBuilder.class).id(0L).schemaJson(s).build();
    }

    @Test
    public void shouldCreateEntityDataForExistingKeyValuesAndSchemaFromCache() {
        Entity keyData = createEntityDataWithHistory(0, ENTITY_KEY_VALUES, PARTITION_KEY_VALUES,
                Arrays.asList(SCHEMA1.toString(), SCHEMA2.toString()));
        FindOrCreateEntityRequest findOrCreateEntityRequest = FindOrCreateEntityRequest.builder()
                .entityKeyValues(ENTITY_KEY_VALUES).partitionKeyValues(PARTITION_KEY_VALUES).schema(SCHEMA1.toString())
                .build();
        when(httpClient.findOrCreateEntityFor(keyData.getSystemSpec().getId(), RECORD_TIME, findOrCreateEntityRequest))
                .thenReturn(keyData);

        Condition<SystemSpecs> systemQueryCondition = SystemSpecs.suchThat().id().eq(keyData.getSystemSpec().getId());
        when(systemService.findOne(argThat(cond -> toRSQL(cond).equals(toRSQL(systemQueryCondition)))))
                .thenReturn(Optional.of(keyData.getSystemSpec()));

        KeyValues entityKeyValues = mock(KeyValues.class);
        when(entityKeyValues.getKeyValues()).thenReturn(ENTITY_KEY_VALUES);

        KeyValues partitionKeyValues = mock(KeyValues.class);
        when(partitionKeyValues.getKeyValues()).thenReturn(PARTITION_KEY_VALUES);

        for (int i = 0; i < 10; i++) {
            Entity data = entityProvider
                    .findOrCreateEntityFor(keyData.getSystemSpec().getId(), entityKeyValues, partitionKeyValues,
                            SCHEMA1.toString(), RECORD_TIME);
            assertNotNull(data);
            assertEquals(keyData, data);
        }
        verify(httpClient, times(1))
                .findOrCreateEntityFor(keyData.getSystemSpec().getId(), RECORD_TIME, findOrCreateEntityRequest);
    }

    @Test
    public void shouldCreateDifferentEntityDataForExistingKeyValuesAndDifferentSchemaFromCache() {
        // given
        Entity keyData = createEntityDataWithHistory(0, ENTITY_KEY_VALUES, PARTITION_KEY_VALUES,
                Collections.singletonList(SCHEMA1.toString()));
        Entity keyData1 = createEntityDataWithHistory(1, ENTITY_KEY_VALUES, PARTITION_KEY_VALUES,
                Collections.singletonList(SCHEMA2.toString()));

        FindOrCreateEntityRequest findOrCreateEntityRequest1 = FindOrCreateEntityRequest.builder()
                .entityKeyValues(ENTITY_KEY_VALUES).partitionKeyValues(PARTITION_KEY_VALUES).schema(SCHEMA1.toString())
                .build();
        FindOrCreateEntityRequest findOrCreateEntityRequest2 = FindOrCreateEntityRequest.builder()
                .entityKeyValues(ENTITY_KEY_VALUES1).partitionKeyValues(PARTITION_KEY_VALUES).schema(SCHEMA2.toString())
                .build();

        when(httpClient.findOrCreateEntityFor(keyData.getSystemSpec().getId(), RECORD_TIME, findOrCreateEntityRequest1))
                .thenReturn(keyData);
        when(httpClient.findOrCreateEntityFor(keyData.getSystemSpec().getId(), RECORD_TIME, findOrCreateEntityRequest2))
                .thenReturn(keyData1);

        KeyValues entityKeyValues = mock(KeyValues.class);
        when(entityKeyValues.getKeyValues()).thenReturn(ENTITY_KEY_VALUES);

        KeyValues entityKeyValues1 = mock(KeyValues.class);
        when(entityKeyValues1.getKeyValues()).thenReturn(ENTITY_KEY_VALUES1);

        KeyValues partitionKeyValues = mock(KeyValues.class);
        when(partitionKeyValues.getKeyValues()).thenReturn(PARTITION_KEY_VALUES);

        Condition<SystemSpecs> systemQueryCondition = SystemSpecs.suchThat().id().eq(keyData.getSystemSpec().getId());
        when(systemService.findOne(argThat(cond -> toRSQL(cond).equals(toRSQL(systemQueryCondition)))))
                .thenReturn(Optional.of(keyData.getSystemSpec()));

        // when
        Entity data0 = entityProvider
                .findOrCreateEntityFor(keyData.getSystemSpec().getId(), entityKeyValues, partitionKeyValues,
                        SCHEMA1.toString(), RECORD_TIME);
        Entity data1 = entityProvider
                .findOrCreateEntityFor(keyData.getSystemSpec().getId(), entityKeyValues, partitionKeyValues,
                        SCHEMA1.toString(), RECORD_TIME);
        Entity data2 = entityProvider
                .findOrCreateEntityFor(keyData.getSystemSpec().getId(), entityKeyValues1, partitionKeyValues,
                        SCHEMA2.toString(), RECORD_TIME);
        Entity data3 = entityProvider
                .findOrCreateEntityFor(keyData.getSystemSpec().getId(), entityKeyValues1, partitionKeyValues,
                        SCHEMA2.toString(), RECORD_TIME);

        // then
        assertNotNull(data0);
        assertNotNull(data1);
        assertNotNull(data2);
        assertNotNull(data3);

        assertEquals(keyData, data0);
        assertEquals(data0, data1);
        assertEquals(keyData1, data2);
        assertEquals(data2, data3);

        verify(httpClient, times(1))
                .findOrCreateEntityFor(keyData.getSystemSpec().getId(), RECORD_TIME, findOrCreateEntityRequest1);
        verify(httpClient, times(1))
                .findOrCreateEntityFor(keyData.getSystemSpec().getId(), RECORD_TIME, findOrCreateEntityRequest2);
    }

    @Test
    public void shouldCreateDifferentEntityDataForExistingKeyValuesAndDifferentPartitionFromCache() {
        // given
        Entity keyData = createEntityDataWithHistory(0, ENTITY_KEY_VALUES, PARTITION_KEY_VALUES,
                Collections.singletonList(SCHEMA1.toString()));
        Entity keyData1 = createEntityDataWithHistory(1, ENTITY_KEY_VALUES, PARTITION_KEY_VALUES1,
                Collections.singletonList(SCHEMA1.toString()));

        FindOrCreateEntityRequest findOrCreateEntityRequest1 = FindOrCreateEntityRequest.builder()
                .entityKeyValues(ENTITY_KEY_VALUES).partitionKeyValues(PARTITION_KEY_VALUES).schema(SCHEMA1.toString())
                .build();
        FindOrCreateEntityRequest findOrCreateEntityRequest2 = FindOrCreateEntityRequest.builder()
                .entityKeyValues(ENTITY_KEY_VALUES1).partitionKeyValues(PARTITION_KEY_VALUES1)
                .schema(SCHEMA1.toString()).build();

        when(httpClient.findOrCreateEntityFor(keyData.getSystemSpec().getId(), RECORD_TIME, findOrCreateEntityRequest1))
                .thenReturn(keyData);
        when(httpClient.findOrCreateEntityFor(keyData.getSystemSpec().getId(), RECORD_TIME, findOrCreateEntityRequest2))
                .thenReturn(keyData1);

        KeyValues entityKeyValues = mock(KeyValues.class);
        when(entityKeyValues.getKeyValues()).thenReturn(ENTITY_KEY_VALUES);

        KeyValues entityKeyValues1 = mock(KeyValues.class);
        when(entityKeyValues1.getKeyValues()).thenReturn(ENTITY_KEY_VALUES1);

        KeyValues partitionKeyValues = mock(KeyValues.class);
        when(partitionKeyValues.getKeyValues()).thenReturn(PARTITION_KEY_VALUES);

        KeyValues partitionKeyValues1 = mock(KeyValues.class);
        when(partitionKeyValues1.getKeyValues()).thenReturn(PARTITION_KEY_VALUES1);

        Condition<SystemSpecs> systemQueryCondition = SystemSpecs.suchThat().id().eq(keyData.getSystemSpec().getId());
        when(systemService.findOne(argThat(cond -> toRSQL(cond).equals(toRSQL(systemQueryCondition)))))
                .thenReturn(Optional.of(keyData.getSystemSpec()));

        // when
        Entity data0 = entityProvider
                .findOrCreateEntityFor(keyData.getSystemSpec().getId(), entityKeyValues, partitionKeyValues,
                        SCHEMA1.toString(), RECORD_TIME);
        Entity data1 = entityProvider
                .findOrCreateEntityFor(keyData.getSystemSpec().getId(), entityKeyValues, partitionKeyValues,
                        SCHEMA1.toString(), RECORD_TIME);

        Entity data2 = entityProvider
                .findOrCreateEntityFor(keyData.getSystemSpec().getId(), entityKeyValues1, partitionKeyValues1,
                        SCHEMA1.toString(), RECORD_TIME);
        Entity data3 = entityProvider
                .findOrCreateEntityFor(keyData.getSystemSpec().getId(), entityKeyValues1, partitionKeyValues1,
                        SCHEMA1.toString(), RECORD_TIME);

        // then
        assertNotNull(data0);
        assertNotNull(data1);
        assertNotNull(data2);
        assertNotNull(data3);

        assertEquals(keyData, data0);
        assertEquals(data0, data1);
        assertEquals(keyData1, data2);
        assertEquals(data2, data3);

        verify(httpClient, times(1))
                .findOrCreateEntityFor(keyData.getSystemSpec().getId(), RECORD_TIME, findOrCreateEntityRequest1);
        verify(httpClient, times(1))
                .findOrCreateEntityFor(keyData.getSystemSpec().getId(), RECORD_TIME, findOrCreateEntityRequest2);
    }

    @Test
    public void shouldThrowOnHistoryRewriteWithWrongPartition() {
        // given
        Entity keyData = createEntityDataWithHistory(0, ENTITY_KEY_VALUES, PARTITION_KEY_VALUES,
                Arrays.asList(SCHEMA2.toString(), SCHEMA3.toString()));

        Condition<SystemSpecs> systemQueryCondition = SystemSpecs.suchThat().id().eq(keyData.getSystemSpec().getId());
        when(systemService.findOne(argThat(cond -> toRSQL(cond).equals(toRSQL(systemQueryCondition)))))
                .thenReturn(Optional.of(keyData.getSystemSpec()));

        Condition<Entities> queryCondition = Entities.suchThat().systemId().eq(keyData.getSystemSpec().getId()).and()
                .keyValues().eq(keyData.getSystemSpec(), keyData.getEntityKeyValues());
        String queryString = toRSQL(queryCondition);

        when(httpClient.findAll(queryString)).thenReturn(Sets.newHashSet(keyData));

        KeyValues entityKeyValues = mock(KeyValues.class);
        when(entityKeyValues.getKeyValues()).thenReturn(ENTITY_KEY_VALUES);

        KeyValues partitionKeyValues = mock(KeyValues.class);
        when(partitionKeyValues.getKeyValues()).thenReturn(PARTITION_KEY_VALUES);

        KeyValues partitionKeyValues1 = mock(KeyValues.class);
        when(partitionKeyValues1.getKeyValues()).thenReturn(PARTITION_KEY_VALUES1);

        // when
        entityProvider.findOrCreateEntityFor(keyData.getSystemSpec().getId(), entityKeyValues, partitionKeyValues,
                SCHEMA2.toString(), RECORD_TIME);

        //then exception
        assertThrows(IllegalStateException.class, () ->
                entityProvider.findOrCreateEntityFor(keyData.getSystemSpec().getId(), entityKeyValues, partitionKeyValues1,
                SCHEMA2.toString(), RECORD_TIME));

    }

    @Test
    public void shouldThrowOnHistoryRewriteWithWrongSchema() {
        // given
        Entity keyData = createEntityDataWithHistory(0, ENTITY_KEY_VALUES, PARTITION_KEY_VALUES,
                Arrays.asList(SCHEMA2.toString(), SCHEMA3.toString()));

        FindOrCreateEntityRequest findOrCreateEntityRequest = FindOrCreateEntityRequest.builder()
                .entityKeyValues(ENTITY_KEY_VALUES).partitionKeyValues(PARTITION_KEY_VALUES).schema(SCHEMA2.toString())
                .build();

        when(httpClient.findOrCreateEntityFor(keyData.getSystemSpec().getId(), RECORD_TIME, findOrCreateEntityRequest))
                .thenReturn(keyData);

        Condition<SystemSpecs> systemQueryCondition = SystemSpecs.suchThat().id().eq(keyData.getSystemSpec().getId());
        when(systemService.findOne(argThat(cond -> toRSQL(cond).equals(toRSQL(systemQueryCondition)))))
                .thenReturn(Optional.of(keyData.getSystemSpec()));

        KeyValues entityKeyValues = mock(KeyValues.class);
        when(entityKeyValues.getKeyValues()).thenReturn(ENTITY_KEY_VALUES);

        KeyValues partitionKeyValues = mock(KeyValues.class);
        when(partitionKeyValues.getKeyValues()).thenReturn(PARTITION_KEY_VALUES);

        // when
        entityProvider.findOrCreateEntityFor(keyData.getSystemSpec().getId(), entityKeyValues, partitionKeyValues,
                SCHEMA2.toString(), RECORD_TIME);

        assertThrows(IllegalStateException.class, () ->
                entityProvider.findOrCreateEntityFor(keyData.getSystemSpec().getId(), entityKeyValues, partitionKeyValues,
                SCHEMA1.toString(), RECORD_TIME));
    }

    @Test
    public void shouldThrowOnHistoryRewriteWithWrongSchemaAndUpdateCache() {
        // given
        Entity keyData = createEntityDataWithHistory(0, ENTITY_KEY_VALUES, PARTITION_KEY_VALUES,
                Arrays.asList(SCHEMA2.toString(), SCHEMA3.toString()));

        FindOrCreateEntityRequest findOrCreateEntityRequest1 = FindOrCreateEntityRequest.builder()
                .entityKeyValues(ENTITY_KEY_VALUES).
                        partitionKeyValues(PARTITION_KEY_VALUES).schema(SCHEMA2.toString()).build();

        FindOrCreateEntityRequest findOrCreateEntityRequest2 = FindOrCreateEntityRequest.builder()
                .entityKeyValues(ENTITY_KEY_VALUES).partitionKeyValues(PARTITION_KEY_VALUES).schema(SCHEMA1.toString())
                .build();

        Condition<SystemSpecs> systemQueryCondition = SystemSpecs.suchThat().id().eq(keyData.getSystemSpec().getId());
        when(systemService.findOne(argThat(cond -> toRSQL(cond).equals(toRSQL(systemQueryCondition)))))
                .thenReturn(Optional.of(keyData.getSystemSpec()));

        Condition<Entities> queryCondition = Entities.suchThat().systemId().eq(keyData.getSystemSpec().getId()).and()
                .keyValues().eq(keyData.getSystemSpec(), ENTITY_KEY_VALUES);
        String queryString = toRSQL(queryCondition);

        when(httpClient.findOrCreateEntityFor(keyData.getSystemSpec().getId(), RECORD_TIME, findOrCreateEntityRequest1))
                .thenReturn(keyData);

        //this one says Data Conflict for this record.
        when(httpClient
                .findOrCreateEntityFor(keyData.getSystemSpec().getId(), RECORD_TIME + 10, findOrCreateEntityRequest2))
                .thenThrow(new FatalDataConflictRuntimeException());

        KeyValues entityKeyValues = mock(KeyValues.class);
        when(entityKeyValues.getKeyValues()).thenReturn(ENTITY_KEY_VALUES);

        KeyValues partitionKeyValues = mock(KeyValues.class);
        when(partitionKeyValues.getKeyValues()).thenReturn(PARTITION_KEY_VALUES);

        // when
        entityProvider.findOrCreateEntityFor(keyData.getSystemSpec().getId(), entityKeyValues, partitionKeyValues,
                SCHEMA2.toString(), RECORD_TIME);

        //thats a wrong call.

        try {
            //this should not be able to verify the record without calling the service which should throw an exception.
            //Under this condition the entityProvider should call the service to find the most recent state of this entity.
            entityProvider.findOrCreateEntityFor(keyData.getSystemSpec().getId(), entityKeyValues, partitionKeyValues,
                    SCHEMA1.toString(), RECORD_TIME + 10);
            fail("Should throw exception");
        } catch (IllegalStateException e) {
            assertThat(e.getCause()).isInstanceOf(FatalDataConflictRuntimeException.class);
        }

        // then
        verify(httpClient, times(1)).findAll(queryString);

    }

    @Test
    public void shouldAcceptNewSchemaWithOneHistoricalValue() {
        //given
        Entity keyData1 = createEntityDataWithHistoryWithTimeDiff(0, ENTITY_KEY_VALUES, PARTITION_KEY_VALUES,
                Collections.singletonList(SCHEMA1.toString()), 100);
        Entity keyData2 = createEntityDataWithHistoryWithTimeDiff(0, ENTITY_KEY_VALUES, PARTITION_KEY_VALUES,
                Arrays.asList(SCHEMA1.toString(), SCHEMA2.toString()), 100);

        final long recordTimestamp = 100;

        FindOrCreateEntityRequest findOrCreateEntityRequest1 = FindOrCreateEntityRequest.builder()
                .entityKeyValues(ENTITY_KEY_VALUES).partitionKeyValues(PARTITION_KEY_VALUES).schema(SCHEMA1.toString())
                .build();
        FindOrCreateEntityRequest findOrCreateEntityRequest2 = FindOrCreateEntityRequest.builder()
                .entityKeyValues(ENTITY_KEY_VALUES).partitionKeyValues(PARTITION_KEY_VALUES).schema(SCHEMA2.toString())
                .build();

        when(httpClient
                .findOrCreateEntityFor(keyData1.getSystemSpec().getId(), RECORD_TIME, findOrCreateEntityRequest1))
                .thenReturn(keyData1);

        when(httpClient
                .findOrCreateEntityFor(keyData1.getSystemSpec().getId(), recordTimestamp, findOrCreateEntityRequest2))
                .thenReturn(keyData2);

        Condition<SystemSpecs> systemQueryCondition = SystemSpecs.suchThat().id().eq(keyData1.getSystemSpec().getId());
        when(systemService.findOne(argThat(cond -> toRSQL(cond).equals(toRSQL(systemQueryCondition)))))
                .thenReturn(Optional.of(keyData1.getSystemSpec()));

        KeyValues entityKeyValues = mock(KeyValues.class);
        when(entityKeyValues.getKeyValues()).thenReturn(ENTITY_KEY_VALUES);

        KeyValues partitionKeyValues = mock(KeyValues.class);
        when(partitionKeyValues.getKeyValues()).thenReturn(PARTITION_KEY_VALUES);

        //ask first for existing one to add to the cache
        entityProvider.findOrCreateEntityFor(keyData1.getSystemSpec().getId(), entityKeyValues, partitionKeyValues,
                SCHEMA1.toString(), RECORD_TIME);

        // when
        Entity data0 = entityProvider
                .findOrCreateEntityFor(keyData1.getSystemSpec().getId(), entityKeyValues, partitionKeyValues,
                        SCHEMA2.toString(), recordTimestamp);

        //then
        assertEquals(data0, keyData2);
    }

    @Test
    public void shouldCreateSameEntityDataForExistingKeyValuesAndDifferentSchemaFromCache() {
        // given
        long secondRecordTimestamp = RECORD_TIME + 1;
        Entity keyData = createEntityDataWithHistory(0, ENTITY_KEY_VALUES, PARTITION_KEY_VALUES,
                Collections.singletonList(SCHEMA1.toString()));
        Entity keyData1 = createEntityDataWithHistory(0, ENTITY_KEY_VALUES, PARTITION_KEY_VALUES,
                Arrays.asList(SCHEMA1.toString(), SCHEMA2.toString()));

        FindOrCreateEntityRequest findOrCreateEntityRequest1 = FindOrCreateEntityRequest.builder()
                .entityKeyValues(ENTITY_KEY_VALUES).partitionKeyValues(PARTITION_KEY_VALUES).schema(SCHEMA1.toString())
                .build();
        FindOrCreateEntityRequest findOrCreateEntityRequest2 = FindOrCreateEntityRequest.builder()
                .entityKeyValues(ENTITY_KEY_VALUES).partitionKeyValues(PARTITION_KEY_VALUES).schema(SCHEMA2.toString())
                .build();

        when(httpClient.findOrCreateEntityFor(keyData.getSystemSpec().getId(), RECORD_TIME, findOrCreateEntityRequest1))
                .thenReturn(keyData);
        when(httpClient.findOrCreateEntityFor(keyData.getSystemSpec().getId(), secondRecordTimestamp,
                findOrCreateEntityRequest2)).thenReturn(keyData1);

        Condition<SystemSpecs> systemQueryCondition = SystemSpecs.suchThat().id().eq(keyData.getSystemSpec().getId());
        when(systemService.findOne(argThat(cond -> toRSQL(cond).equals(toRSQL(systemQueryCondition)))))
                .thenReturn(Optional.of(keyData.getSystemSpec()));

        KeyValues cachedRequest = mock(KeyValues.class);
        when(cachedRequest.getKeyValues()).thenReturn(ENTITY_KEY_VALUES);

        KeyValues partitionKeyValues = mock(KeyValues.class);
        when(partitionKeyValues.getKeyValues()).thenReturn(PARTITION_KEY_VALUES);

        // when
        Entity data0 = entityProvider
                .findOrCreateEntityFor(keyData.getSystemSpec().getId(), cachedRequest, partitionKeyValues,
                        SCHEMA1.toString(), RECORD_TIME);

        Entity data2 = entityProvider
                .findOrCreateEntityFor(keyData.getSystemSpec().getId(), cachedRequest, partitionKeyValues,
                        SCHEMA2.toString(), secondRecordTimestamp);

        Entity data1 = entityProvider
                .findOrCreateEntityFor(keyData.getSystemSpec().getId(), cachedRequest, partitionKeyValues,
                        SCHEMA1.toString(), RECORD_TIME);

        Entity data3 = entityProvider
                .findOrCreateEntityFor(keyData.getSystemSpec().getId(), cachedRequest, partitionKeyValues,
                        SCHEMA2.toString(), secondRecordTimestamp);

        // then
        assertNotNull(data0);
        assertNotNull(data1);
        assertNotNull(data2);
        assertNotNull(data3);

        assertEquals(keyData, data0);
        assertEquals(data0, data1);
        assertEquals(keyData1, data2);
        assertEquals(data2, data3);

        verify(httpClient, times(1))
                .findOrCreateEntityFor(keyData.getSystemSpec().getId(), RECORD_TIME, findOrCreateEntityRequest1);
        verify(httpClient, times(1)).findOrCreateEntityFor(keyData.getSystemSpec().getId(), secondRecordTimestamp,
                findOrCreateEntityRequest2);
    }

    @Test
    public void shouldAcceptNewSchemaWithOneHistoricalValueWithIds() {
        //given
        Entity keyData1 = createEntityDataWithHistoryWithTimeDiff(0, ENTITY_KEY_VALUES, PARTITION_KEY_VALUES,
                Collections.singletonList(SCHEMA1.toString()), 100);
        Entity keyData2 = createEntityDataWithHistoryWithTimeDiff(0, ENTITY_KEY_VALUES, PARTITION_KEY_VALUES,
                Arrays.asList(SCHEMA1.toString(), SCHEMA2.toString()), 100);

        String queryString = toRSQL(Entities.suchThat().id().eq(keyData1.getId()));

        final long recordTimestamp = 100;

        when(httpClient.findAll(queryString)).thenReturn(Sets.newHashSet(keyData1));

        lenient().when(httpClient.findOrCreateEntityFor(keyData1.getSystemSpec().getId(), keyData1.getId(),
                keyData1.getPartition().getId(), recordTimestamp, SCHEMA2.toString())).thenReturn(keyData2);

        //ask first for existing one to add to the cache
        entityProvider.findOrCreateEntityFor(keyData1.getSystemSpec().getId(), keyData1.getId(),
                keyData1.getPartition().getId(), SCHEMA1.toString(), RECORD_TIME);

        // when
        Entity data0 = entityProvider.findOrCreateEntityFor(keyData1.getSystemSpec().getId(), keyData1.getId(),
                keyData1.getPartition().getId(), SCHEMA2.toString(), recordTimestamp);

        //then
        assertEquals(data0, keyData2);

    }

    @Test
    public void shouldThrowOnEntityNotFound() {
        //given
        Entity keyData1 = createEntityDataWithHistoryWithTimeDiff(0, ENTITY_KEY_VALUES, PARTITION_KEY_VALUES,
                Collections.singletonList(SCHEMA1.toString()), 100);

        String queryString = toRSQL(Entities.suchThat().id().eq(keyData1.getId()));
        when(httpClient.findAll(queryString)).thenReturn(emptySet());

        //when
        assertThrows(IllegalArgumentException.class, () -> entityProvider.findOrCreateEntityFor(keyData1.getSystemSpec().getId(), keyData1.getId(),
                keyData1.getPartition().getId(), SCHEMA1.toString(), RECORD_TIME));

    }

    @Test
    public void shouldThrowOnHistoryRewrite() {
        //given
        Entity keyData1 = createEntityDataWithHistoryWithTimeDiff(0, ENTITY_KEY_VALUES, PARTITION_KEY_VALUES,
                Arrays.asList(SCHEMA1.toString(), SCHEMA2.toString()), 100);

        String queryString = toRSQL(Entities.suchThat().id().eq(keyData1.getId()));

        when(httpClient.findAll(queryString)).thenReturn(Sets.newHashSet(keyData1));

        // when
        assertThrows(IllegalStateException.class, () -> entityProvider.findOrCreateEntityFor(keyData1.getSystemSpec().getId(), keyData1.getId(),
                keyData1.getPartition().getId(), SCHEMA1.toString(), RECORD_TIME));
    }

    @Test
    public void shouldFindBySystemIdKeyValuesAndTimeWindow() {
        //given
        Entity keyData = createEntityDataWithHistory(0, ENTITY_KEY_VALUES, PARTITION_KEY_VALUES,
                Arrays.asList(SCHEMA1.toString(), SCHEMA2.toString(), SCHEMA3.toString()));

        Condition<Entities> queryCondition = Entities.suchThat().systemId().eq(keyData.getSystemSpec().getId()).and()
                .keyValues().eq(keyData.getSystemSpec(), keyData.getEntityKeyValues());
        String queryString = toRSQL(queryCondition);

        when(httpClient.findAll(queryString, RECORD_TIME, RECORD_TIME + 3)).thenReturn(Sets.newHashSet(keyData));

        //when
        Entity data = entityProvider.findOneWithHistory(queryCondition, RECORD_TIME, RECORD_TIME + 3)
                .orElseThrow(() -> new IllegalArgumentException("No such entity"));

        //then
        assertNotNull(data);
        assertEquals(3, data.getEntityHistory().size());
    }

    @Test
    public void shouldUpdateEntityNameBasedOnListOfEntityData() {
        //given
        Entity entityData = createEntityDataWithHistory(0, ENTITY_KEY_VALUES, PARTITION_KEY_VALUES,
                Collections.singletonList(SCHEMA1.toString()));

        Set<Entity> inputEntityDataList = Sets.newHashSet(entityData);
        Set<Entity> entityDataList = Sets.newHashSet();
        Entity entityDataMock = ReflectionUtils.enhanceWithId(entityData.toBuilder(), entityData.getId() + 1).build();
        entityDataList.add(entityDataMock);
        when(httpClient.updateEntities(any(Set.class))).thenReturn(entityDataList);
        //when

        Set<Entity> entityDataReturnedList = entityProvider.updateEntities(inputEntityDataList);

        //then
        assertThat(entityDataReturnedList).hasSize(1).contains(entityDataMock);
    }

    @Test
    public void shouldFindEntityById() {
        //given
        Entity keyData = createEntityDataWithHistory(0, ENTITY_KEY_VALUES, PARTITION_KEY_VALUES,
                Arrays.asList(SCHEMA1.toString(), SCHEMA2.toString(), SCHEMA3.toString()));

        Condition<Entities> condition = Entities.suchThat().id().eq(keyData.getId());
        String stringQuery = toRSQL(condition);

        when(httpClient.findAll(stringQuery)).thenReturn(Sets.newHashSet(keyData));

        //when
        Entity data = entityProvider.findOne(condition)
                .orElseThrow(() -> new IllegalArgumentException("No such entity"));

        //then
        assertNotNull(data);
        assertEquals(0, data.getId());
    }

    @Test
    public void shouldDisplayExceptionCause() {
        //given
        KeyValues entityKeyValues = new KeyValues(0, ENTITY_KEY_VALUES);
        KeyValues partitionKeyValues = new KeyValues(0, PARTITION_KEY_VALUES);

        FindOrCreateEntityRequest findOrCreateEntityRequest = FindOrCreateEntityRequest.builder()
                .entityKeyValues(ENTITY_KEY_VALUES).partitionKeyValues(PARTITION_KEY_VALUES).schema(SCHEMA1.toString())
                .build();

        SystemSpec systemSpec = createSystemSpec();
        Condition<SystemSpecs> systemQueryCondition = SystemSpecs.suchThat().id().eq(-1L);
        when(systemService.findOne(argThat(cond -> toRSQL(cond).equals(toRSQL(systemQueryCondition)))))
                .thenReturn(Optional.of(systemSpec));

        Exception expectedCause = new IndexOutOfBoundsException(EXCEPTION_MESSAGE);

        when(httpClient.findOrCreateEntityFor(SYSTEM_ID, RECORD_TIME, findOrCreateEntityRequest)).
                thenThrow(expectedCause);

        //when
        Exception actualEx = null;

        try {
            entityProvider.findOrCreateEntityFor(SYSTEM_ID, entityKeyValues, partitionKeyValues, SCHEMA1.toString(),
                    RECORD_TIME);
            fail("Should throw " + expectedCause);
        } catch (Exception e) {
            actualEx = e;

        }
        //then
        assertEquals(expectedCause, actualEx.getCause());
    }

    @Test
    public void shouldThrowIllegalStateOrIllegalArgumentExceptions() {
        //given
        KeyValues entityKeyValues = new KeyValues(0, ENTITY_KEY_VALUES);
        KeyValues partitionKeyValues = new KeyValues(0, PARTITION_KEY_VALUES);

        FindOrCreateEntityRequest findOrCreateEntityRequest1 = FindOrCreateEntityRequest.builder()
                .entityKeyValues(ENTITY_KEY_VALUES).partitionKeyValues(PARTITION_KEY_VALUES).schema(SCHEMA1.toString())
                .build();

        FindOrCreateEntityRequest findOrCreateEntityRequest2 = FindOrCreateEntityRequest.builder()
                .entityKeyValues(ENTITY_KEY_VALUES).partitionKeyValues(PARTITION_KEY_VALUES).schema(SCHEMA2.toString())
                .build();

        Exception expectedCause1 = new IllegalStateException(EXCEPTION_MESSAGE);
        Exception expectedCause2 = new IllegalArgumentException(EXCEPTION_MESSAGE);

        SystemSpec systemSpec = createSystemSpec();
        Condition<SystemSpecs> systemQueryCondition = SystemSpecs.suchThat().id().eq(-1L);
        when(systemService.findOne(argThat(cond -> toRSQL(cond).equals(toRSQL(systemQueryCondition)))))
                .thenReturn(Optional.of(systemSpec));

        when(httpClient.findOrCreateEntityFor(SYSTEM_ID, RECORD_TIME, findOrCreateEntityRequest1)).
                thenThrow(expectedCause1);
        when(httpClient.findOrCreateEntityFor(SYSTEM_ID, RECORD_TIME, findOrCreateEntityRequest2)).
                thenThrow(expectedCause2);

        //when
        Exception actualEx1 = null;
        Exception actualEx2 = null;

        try {
            entityProvider.findOrCreateEntityFor(SYSTEM_ID, entityKeyValues, partitionKeyValues, SCHEMA1.toString(),
                    RECORD_TIME);
        } catch (Exception e) {
            actualEx1 = e;
        }

        try {
            entityProvider.findOrCreateEntityFor(SYSTEM_ID, entityKeyValues, partitionKeyValues, SCHEMA2.toString(),
                    RECORD_TIME);
        } catch (Exception e) {
            actualEx2 = e;
        }

        //then
        assertEquals(expectedCause1, actualEx1);
        assertEquals(expectedCause2, actualEx2);
    }

    @Test
    public void shouldFindAllBySystemNameAndPartitionKeyValues() {
        //given
        Entity keyData = createEntityDataWithHistory(SYSTEM_ID, ENTITY_KEY_VALUES, PARTITION_KEY_VALUES,
                Arrays.asList(SCHEMA1.toString(), SCHEMA2.toString(), SCHEMA3.toString()));

        Condition<Entities> queryCondition = Entities.suchThat().systemName().eq(keyData.getSystemSpec().getName())
                .and().partitionKeyValues().eq(keyData.getSystemSpec(), keyData.getPartition().getKeyValues());
        String queryString = toRSQL(queryCondition);
        when(httpClient.findAll(queryString)).thenReturn(Sets.newHashSet(keyData));

        //when
        Set<Entity> fetchedEntities = entityProvider.findAll(queryCondition);

        //then
        assertNotNull(fetchedEntities);
        assertEquals(1, fetchedEntities.size());
    }

    @Test
    public void shouldThrowExceptionWhenFindAllBySystemNameAndPartitionKeyValuesWithNullMap() {
        //given
        Entity keyData = createEntityDataWithHistory(SYSTEM_ID, ENTITY_KEY_VALUES, PARTITION_KEY_VALUES,
                Arrays.asList(SCHEMA1.toString(), SCHEMA2.toString(), SCHEMA3.toString()));

        assertThrows(NullPointerException.class, () ->
                entityProvider.findAll(Entities.suchThat().systemName().eq(SYSTEM_NAME).and().partitionKeyValues()
                .eq(keyData.getSystemSpec(), null)));
    }

    @Test
    public void shouldThrowExceptionWhenFindAllEntitiesByIdInWithNullList() {
        assertThrows(NullPointerException.class, () ->
            entityProvider.findAll(Entities.suchThat().id().in((Collection<Long>) null)));
    }

    @Test
    public void shouldFindAllEntitiesByIdInList() {
        //given
        Entity keyData = createEntityDataWithHistory(0, ENTITY_KEY_VALUES, PARTITION_KEY_VALUES,
                Arrays.asList(SCHEMA1.toString(), SCHEMA2.toString(), SCHEMA3.toString()));

        Entity otherKeyData = createEntityDataWithHistory(1, ENTITY_KEY_VALUES1, PARTITION_KEY_VALUES1,
                Arrays.asList(SCHEMA1.toString(), SCHEMA2.toString(), SCHEMA3.toString()));

        List<Long> requestedEntityIds = Arrays.asList(keyData.getId(), otherKeyData.getId());

        Condition<Entities> condition = Entities.suchThat().id().in(requestedEntityIds);
        String stringQuery = toRSQL(condition);
        when(httpClient.findAll(stringQuery)).thenReturn(Sets.newHashSet(keyData, otherKeyData));

        //when

        Set<Entity> fetchedEntities = entityProvider.findAll(condition);

        //then
        assertNotNull(fetchedEntities);
        assertEquals(2, fetchedEntities.size());
    }

    @Test
    public void shouldThrowOnMultipleValuesInFindOne() {
        //given
        Entity keyData = createEntityDataWithHistory(0, ENTITY_KEY_VALUES, PARTITION_KEY_VALUES,
                Arrays.asList(SCHEMA1.toString(), SCHEMA2.toString(), SCHEMA3.toString()));

        Entity otherKeyData = createEntityDataWithHistory(1, ENTITY_KEY_VALUES1, PARTITION_KEY_VALUES1,
                Arrays.asList(SCHEMA1.toString(), SCHEMA2.toString(), SCHEMA3.toString()));

        List<Long> requestedEntityIds = Arrays.asList(keyData.getId(), otherKeyData.getId());

        Condition<Entities> condition = Entities.suchThat().id().in(requestedEntityIds);
        String stringQuery = toRSQL(condition);
        when(httpClient.findAll(stringQuery)).thenReturn(Sets.newHashSet(keyData, otherKeyData));

        //when
        assertThrows(IllegalArgumentException.class, () -> entityProvider.findOne(condition));
    }

    @Test
    public void shouldInvalidateAllCache() {
        Entity keyData1 = createEntityDataWithHistory(0, ENTITY_KEY_VALUES, PARTITION_KEY_VALUES,
                Arrays.asList(SCHEMA1.toString()));
        Entity keyData2 = createEntityDataWithHistory(1, ENTITY_KEY_VALUES1, PARTITION_KEY_VALUES1,
                Arrays.asList(SCHEMA2.toString()));

        FindOrCreateEntityRequest findOrCreateEntityRequest = FindOrCreateEntityRequest.builder()
                .entityKeyValues(ENTITY_KEY_VALUES).partitionKeyValues(PARTITION_KEY_VALUES).schema(SCHEMA1.toString())
                .build();
        when(httpClient.findOrCreateEntityFor(keyData1.getSystemSpec().getId(), RECORD_TIME, findOrCreateEntityRequest))
                .thenReturn(keyData1);

        Condition<SystemSpecs> systemQueryCondition = SystemSpecs.suchThat().id().eq(keyData1.getSystemSpec().getId());
        when(systemService.findOne(argThat(cond -> toRSQL(cond).equals(toRSQL(systemQueryCondition)))))
                .thenReturn(Optional.of(keyData1.getSystemSpec()));

        KeyValues entityKeyValues = new KeyValues(0, ENTITY_KEY_VALUES);
        KeyValues partitionKeyValues = new KeyValues(0, PARTITION_KEY_VALUES);

        String queryString = toRSQL(Entities.suchThat().id().eq(keyData2.getId()));

        when(httpClient.findAll(queryString)).thenReturn(Sets.newHashSet(keyData2));

        when(httpClient.findOrCreateEntityFor(keyData2.getSystemSpec().getId(), keyData2.getId(),
                keyData2.getPartition().getId(), RECORD_TIME, SCHEMA2.toString())).thenReturn(keyData2);

        //ask first for existing one to add to the cache
        entityProvider.findOrCreateEntityFor(keyData2.getSystemSpec().getId(), keyData2.getId(),
                keyData2.getPartition().getId(), SCHEMA2.toString(), RECORD_TIME);

        // test caching
        for (int i = 0; i < 10; i++) {
            Entity entity1 = entityProvider.findOrCreateEntityFor(keyData1.getSystemSpec().getId(), entityKeyValues, partitionKeyValues,
                    SCHEMA1.toString(), RECORD_TIME);
            Entity entity2 = entityProvider.findOrCreateEntityFor(keyData2.getSystemSpec().getId(), keyData2.getId(), keyData2.getPartition().getId(), SCHEMA2.toString(), RECORD_TIME);
            assertNotNull(entity1);
            assertNotNull(entity2);
            assertEquals(keyData1, entity1);
            assertEquals(keyData2, entity2);
        }

        verify(httpClient, times(1))
                .findOrCreateEntityFor(keyData1.getSystemSpec().getId(), RECORD_TIME, findOrCreateEntityRequest);
        verify(httpClient, times(1))
                .findAll(queryString);

        // test invalidation of cache
        for (int i = 0; i < 10; i++) {
            Entity entity1 = entityProvider.findOrCreateEntityFor(keyData1.getSystemSpec().getId(), entityKeyValues, partitionKeyValues,
                    SCHEMA1.toString(), RECORD_TIME);
            Entity entity2 = entityProvider.findOrCreateEntityFor(keyData2.getSystemSpec().getId(), keyData2.getId(), keyData2.getPartition().getId(), SCHEMA2.toString(), RECORD_TIME);
            assertNotNull(entity1);
            assertNotNull(entity2);
            assertEquals(keyData1, entity1);
            assertEquals(keyData2, entity2);
            entityProvider.invalidateAllCache();
        }
        verify(httpClient, times(10))
                .findOrCreateEntityFor(keyData1.getSystemSpec().getId(), RECORD_TIME, findOrCreateEntityRequest);
        verify(httpClient, times(10))
                .findAll(queryString);
    }

    @Test
    public void shouldInvalidateForOnlyOneEntityFoundByKeyValues() {
        Entity keyData1 = createEntityDataWithHistory(0, ENTITY_KEY_VALUES, PARTITION_KEY_VALUES,
                Arrays.asList(SCHEMA1.toString()));
        Entity keyData2 = createEntityDataWithHistory(1, ENTITY_KEY_VALUES1, PARTITION_KEY_VALUES1,
                Arrays.asList(SCHEMA2.toString()));

        FindOrCreateEntityRequest findOrCreateEntityRequest = FindOrCreateEntityRequest.builder()
                .entityKeyValues(ENTITY_KEY_VALUES).partitionKeyValues(PARTITION_KEY_VALUES).schema(SCHEMA1.toString())
                .build();
        when(httpClient.findOrCreateEntityFor(keyData1.getSystemSpec().getId(), RECORD_TIME, findOrCreateEntityRequest))
                .thenReturn(keyData1);

        Condition<SystemSpecs> systemQueryCondition = SystemSpecs.suchThat().id().eq(keyData1.getSystemSpec().getId());
        when(systemService.findOne(argThat(cond -> toRSQL(cond).equals(toRSQL(systemQueryCondition)))))
                .thenReturn(Optional.of(keyData1.getSystemSpec()));

        KeyValues entityKeyValues = new KeyValues(0, ENTITY_KEY_VALUES);
        KeyValues partitionKeyValues = new KeyValues(0, PARTITION_KEY_VALUES);

        String queryString = toRSQL(Entities.suchThat().id().eq(keyData2.getId()));

        when(httpClient.findAll(queryString)).thenReturn(Sets.newHashSet(keyData2));

        when(httpClient.findOrCreateEntityFor(keyData2.getSystemSpec().getId(), keyData2.getId(),
                keyData2.getPartition().getId(), RECORD_TIME, SCHEMA2.toString())).thenReturn(keyData2);

        //ask first for existing one to add to the cache
        entityProvider.findOrCreateEntityFor(keyData2.getSystemSpec().getId(), keyData2.getId(),
                keyData2.getPartition().getId(), SCHEMA2.toString(), RECORD_TIME);

        // test invalidation of cache
        for (int i = 0; i < 10; i++) {
            Entity entity1 = entityProvider.findOrCreateEntityFor(keyData1.getSystemSpec().getId(), entityKeyValues, partitionKeyValues,
                    SCHEMA1.toString(), RECORD_TIME);
            Entity entity2 = entityProvider.findOrCreateEntityFor(keyData2.getSystemSpec().getId(), keyData2.getId(), keyData2.getPartition().getId(), SCHEMA2.toString(), RECORD_TIME);
            assertNotNull(entity1);
            assertNotNull(entity2);
            assertEquals(keyData1, entity1);
            assertEquals(keyData2, entity2);
            entityProvider.invalidateCacheFor(entity1);
        }
        verify(httpClient, times(10))
                .findOrCreateEntityFor(keyData1.getSystemSpec().getId(), RECORD_TIME, findOrCreateEntityRequest);
        verify(httpClient, times(1))
                .findAll(queryString);
    }

    @Test
    public void shouldInvalidateForOnlyOneEntityFoundById() {
        Entity keyData1 = createEntityDataWithHistory(0, ENTITY_KEY_VALUES, PARTITION_KEY_VALUES,
                Arrays.asList(SCHEMA1.toString()));
        Entity keyData2 = createEntityDataWithHistory(1, ENTITY_KEY_VALUES1, PARTITION_KEY_VALUES1,
                Arrays.asList(SCHEMA2.toString()));

        FindOrCreateEntityRequest findOrCreateEntityRequest = FindOrCreateEntityRequest.builder()
                .entityKeyValues(ENTITY_KEY_VALUES).partitionKeyValues(PARTITION_KEY_VALUES).schema(SCHEMA1.toString())
                .build();
        when(httpClient.findOrCreateEntityFor(keyData1.getSystemSpec().getId(), RECORD_TIME, findOrCreateEntityRequest))
                .thenReturn(keyData1);

        Condition<SystemSpecs> systemQueryCondition = SystemSpecs.suchThat().id().eq(keyData1.getSystemSpec().getId());
        when(systemService.findOne(argThat(cond -> toRSQL(cond).equals(toRSQL(systemQueryCondition)))))
                .thenReturn(Optional.of(keyData1.getSystemSpec()));

        KeyValues entityKeyValues = new KeyValues(0, ENTITY_KEY_VALUES);
        KeyValues partitionKeyValues = new KeyValues(0, PARTITION_KEY_VALUES);

        String queryString = toRSQL(Entities.suchThat().id().eq(keyData2.getId()));

        when(httpClient.findAll(queryString)).thenReturn(Sets.newHashSet(keyData2));

        when(httpClient.findOrCreateEntityFor(keyData2.getSystemSpec().getId(), keyData2.getId(),
                keyData2.getPartition().getId(), RECORD_TIME, SCHEMA2.toString())).thenReturn(keyData2);

        //ask first for existing one to add to the cache
        entityProvider.findOrCreateEntityFor(keyData2.getSystemSpec().getId(), keyData2.getId(),
                keyData2.getPartition().getId(), SCHEMA2.toString(), RECORD_TIME);

        // test invalidation of cache
        for (int i = 0; i < 10; i++) {
            Entity entity1 = entityProvider.findOrCreateEntityFor(keyData1.getSystemSpec().getId(), entityKeyValues, partitionKeyValues,
                    SCHEMA1.toString(), RECORD_TIME);
            Entity entity2 = entityProvider.findOrCreateEntityFor(keyData2.getSystemSpec().getId(), keyData2.getId(), keyData2.getPartition().getId(), SCHEMA2.toString(), RECORD_TIME);
            assertNotNull(entity1);
            assertNotNull(entity2);
            assertEquals(keyData1, entity1);
            assertEquals(keyData2, entity2);
            entityProvider.invalidateCacheFor(entity2);
        }
        verify(httpClient, times(1))
                .findOrCreateEntityFor(keyData1.getSystemSpec().getId(), RECORD_TIME, findOrCreateEntityRequest);
        verify(httpClient, times(10))
                .findAll(queryString);
    }

    @Test
    public void shouldInvalidateForAllEntitiesFoundBySameId() {
        Entity keyData1 = createEntityDataWithHistory(0, ENTITY_KEY_VALUES, PARTITION_KEY_VALUES,
                Arrays.asList(SCHEMA1.toString()));
        Entity keyData2 = createEntityDataWithHistory(0, ENTITY_KEY_VALUES, PARTITION_KEY_VALUES,
                Arrays.asList(SCHEMA2.toString()));

        Condition<SystemSpecs> systemQueryCondition = SystemSpecs.suchThat().id().eq(keyData1.getSystemSpec().getId());
        when(systemService.findOne(argThat(cond -> toRSQL(cond).equals(toRSQL(systemQueryCondition)))))
                .thenReturn(Optional.of(keyData1.getSystemSpec()));

        String queryString1 = toRSQL(Entities.suchThat().id().eq(keyData1.getId()));

        when(httpClient.findAll(queryString1)).thenReturn(Sets.newHashSet(keyData1));

        String queryString2 = toRSQL(Entities.suchThat().id().eq(keyData2.getId()));

        when(httpClient.findAll(queryString2)).thenReturn(Sets.newHashSet(keyData2));

        when(httpClient.findOrCreateEntityFor(keyData1.getSystemSpec().getId(), keyData1.getId(),
                keyData1.getPartition().getId(), RECORD_TIME, SCHEMA1.toString())).thenReturn(keyData1);

        when(httpClient.findOrCreateEntityFor(keyData2.getSystemSpec().getId(), keyData2.getId(),
                keyData2.getPartition().getId(), RECORD_TIME, SCHEMA2.toString())).thenReturn(keyData2);

        //ask first for existing one to add to the cache
        entityProvider.findOrCreateEntityFor(keyData2.getSystemSpec().getId(), keyData2.getId(),
                keyData2.getPartition().getId(), SCHEMA2.toString(), RECORD_TIME);

        // test invalidation of cache
        for (int i = 0; i < 10; i++) {
            Entity entity1 = entityProvider.findOrCreateEntityFor(keyData1.getSystemSpec().getId(), keyData1.getId(), keyData1.getPartition().getId(), SCHEMA1.toString(), RECORD_TIME);
            Entity entity2 = entityProvider.findOrCreateEntityFor(keyData2.getSystemSpec().getId(), keyData2.getId(), keyData2.getPartition().getId(), SCHEMA2.toString(), RECORD_TIME);
            assertNotNull(entity1);
            assertNotNull(entity2);
            assertEquals(keyData1, entity1);
            assertEquals(keyData2, entity2);
            entityProvider.invalidateCacheFor(entity2);
        }
        verify(httpClient, times(10))
                .findAll(queryString1);
        verify(httpClient, times(10))
                .findAll(queryString2);
    }

    @Test
    public void shouldInvalidateForAllEntitiesFoundByKeyValues() {
        Entity keyData1 = createEntityDataWithHistory(0, ENTITY_KEY_VALUES, PARTITION_KEY_VALUES,
                Arrays.asList(SCHEMA1.toString()));
        Entity keyData2 = createEntityDataWithHistory(1, ENTITY_KEY_VALUES, PARTITION_KEY_VALUES,
                Arrays.asList(SCHEMA1.toString()));

        FindOrCreateEntityRequest findOrCreateEntityRequest = FindOrCreateEntityRequest.builder()
                .entityKeyValues(ENTITY_KEY_VALUES).partitionKeyValues(PARTITION_KEY_VALUES).schema(SCHEMA1.toString())
                .build();
        when(httpClient.findOrCreateEntityFor(keyData1.getSystemSpec().getId(), RECORD_TIME, findOrCreateEntityRequest))
                .thenReturn(keyData1);

        FindOrCreateEntityRequest findOrCreateEntityRequest2 = FindOrCreateEntityRequest.builder()
                .entityKeyValues(ENTITY_KEY_VALUES).partitionKeyValues(PARTITION_KEY_VALUES).schema(SCHEMA1.toString())
                .build();
        when(httpClient.findOrCreateEntityFor(keyData2.getSystemSpec().getId(), RECORD_TIME, findOrCreateEntityRequest))
                .thenReturn(keyData2);


        Condition<SystemSpecs> systemQueryCondition = SystemSpecs.suchThat().id().eq(keyData1.getSystemSpec().getId());
        when(systemService.findOne(argThat(cond -> toRSQL(cond).equals(toRSQL(systemQueryCondition)))))
                .thenReturn(Optional.of(keyData1.getSystemSpec()));

        KeyValues entityKeyValues1 = new KeyValues(0, ENTITY_KEY_VALUES);
        KeyValues partitionKeyValues1 = new KeyValues(0, PARTITION_KEY_VALUES);

        KeyValues entityKeyValues2 = new KeyValues(1, ENTITY_KEY_VALUES);
        KeyValues partitionKeyValues2 = new KeyValues(1, PARTITION_KEY_VALUES);

        // test invalidation of cache
        for (int i = 0; i < 10; i++) {
            Entity entity1 = entityProvider.findOrCreateEntityFor(keyData1.getSystemSpec().getId(), entityKeyValues1, partitionKeyValues1,
                    SCHEMA1.toString(), RECORD_TIME);
            Entity entity2 = entityProvider.findOrCreateEntityFor(keyData2.getSystemSpec().getId(), entityKeyValues2, partitionKeyValues2,
                    SCHEMA1.toString(), RECORD_TIME);
            assertNotNull(entity1);
            assertNotNull(entity2);
            assertEquals(keyData1, entity1);
            assertEquals(keyData2, entity2);
            assertEquals(keyData1, keyData2);
            entityProvider.invalidateCacheFor(entity1);
        }
        verify(httpClient, times(10))
                .findOrCreateEntityFor(keyData1.getSystemSpec().getId(), RECORD_TIME, findOrCreateEntityRequest);
        verify(httpClient, times(10))
                .findOrCreateEntityFor(keyData2.getSystemSpec().getId(), RECORD_TIME, findOrCreateEntityRequest2);
    }

    @Test
    public void shouldHaveCacheConfigurationsSet() {
        Duration actualConfiguration = entityProvider.getCache().policy().expireAfterAccess()
                .orElseThrow(() -> new IllegalStateException("The setting for the cache about expireAfterAccess is not configured"))
                .getExpiresAfter();
        // 2000 is the value set in the reference.conf file for the purpose of this test
        Duration expectedConfiguration = Duration.of(2000, ChronoUnit.SECONDS);
        assertEquals(expectedConfiguration, actualConfiguration);
    }
}
