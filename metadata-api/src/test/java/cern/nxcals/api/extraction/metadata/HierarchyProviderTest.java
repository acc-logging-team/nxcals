package cern.nxcals.api.extraction.metadata;

import avro.shaded.com.google.common.collect.Iterables;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.Hierarchy;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableHierarchies;
import cern.nxcals.api.domain.VariableHierarchyIds;
import cern.nxcals.api.extraction.metadata.feign.HierarchyClient;
import cern.nxcals.api.extraction.metadata.queries.Hierarchies;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class HierarchyProviderTest {
    private static final String MOCK_SYSTEM_NAME = "MOCK-SYSTEM";
    private static final String TEST_NODE_NAME = "NODE";

    private HierarchyProvider hierarchyProvider;

    @Mock
    private HierarchyClient httpService;

    @BeforeEach
    public void setup() {
        hierarchyProvider = new HierarchyProvider(httpService);
        reset(httpService);
    }

    @Test
    public void shouldNotAcceptNullFindOneByQuery() {
        assertThrows(NullPointerException.class, () -> hierarchyProvider.findOne(null));
    }

    @Test
    public void shouldNotAcceptNullFindAllByQuery() {
        assertThrows(NullPointerException.class, () -> hierarchyProvider.findAll(null));
    }

    @Test
    public void shouldFindOneByQuery() {
        //given
        Condition<Hierarchies> query = Hierarchies.suchThat().systemName().eq(MOCK_SYSTEM_NAME).and().name().eq(TEST_NODE_NAME);
        String queryString = toRSQL(query);
        Hierarchy hierarchy = mock(Hierarchy.class);
        when(httpService.findAll(queryString)).thenReturn(Sets.newHashSet(hierarchy));

        //when
        Optional<Hierarchy> data = hierarchyProvider.findOne(query);

        //then
        assertNotNull(data.get());
    }

    @Test
    public void shouldFindAllByQuery() {
        //given
        Condition<Hierarchies> query = Hierarchies.suchThat().systemName().eq(MOCK_SYSTEM_NAME).and().name().eq(TEST_NODE_NAME);
        String queryString = toRSQL(query);
        Hierarchy hierarchy = mock(Hierarchy.class);
        when(httpService.findAll(queryString)).thenReturn(Sets.newHashSet(hierarchy));

        //when
        Set<Hierarchy> data = hierarchyProvider.findAll(query);

        //then
        assertNotNull(data);
        assertThat(data).containsExactly(hierarchy);
    }

    @Test
    public void shouldFindChainWithNormalPath() {
        Hierarchy hierarchy = mock(Hierarchy.class, RETURNS_DEEP_STUBS);
        when(hierarchy.getNodePath()).thenReturn("/A/B/C/D");
        when(hierarchy.getSystemSpec().getId()).thenReturn(1L);

        Condition<Hierarchies> query = Hierarchies.suchThat().systemId().eq(1L)
                .and().path().in(ImmutableList.of("/A", "/A/B", "/A/B/C", "/A/B/C/D"));
        String queryString = toRSQL(query);

        hierarchyProvider.getChainTo(hierarchy);

        verify(httpService).findAll(eq(queryString));
    }

    @Test
    public void shouldFindChainWithDegeneratePath() {
        Hierarchy hierarchy = mock(Hierarchy.class, RETURNS_DEEP_STUBS);
        when(hierarchy.getSystemSpec().getId()).thenReturn(1L);
        when(hierarchy.getNodePath()).thenReturn("/A");

        Condition<Hierarchies> query = Hierarchies.suchThat().systemId().eq(1L)
                .and().path().in(ImmutableList.of("/A"));
        String queryString = toRSQL(query);

        when(httpService.findAll(queryString)).thenReturn(Collections.singleton(hierarchy));

        List<Hierarchy> chain = hierarchyProvider.getChainTo(hierarchy);
        assertNotNull(chain);
        assertEquals(1L, chain.size());
        assertEquals(hierarchy.getNodePath(), Iterables.getOnlyElement(chain).getNodePath());

        verify(httpService).findAll(queryString);
    }

    @Test
    public void shouldFindChainWithEmptyPath() {
        Hierarchy hierarchy = mock(Hierarchy.class, RETURNS_DEEP_STUBS);
        when(hierarchy.getNodePath()).thenReturn("");

        assertThat(hierarchyProvider.getChainTo(hierarchy)).containsExactly(hierarchy);
    }

    @Test
    public void shouldFindTopLevel() {
        SystemSpec system = mock(SystemSpec.class);
        when(system.getId()).thenReturn(1L);

        hierarchyProvider.getTopLevel(system);

        Condition<Hierarchies> query = Hierarchies.suchThat().areTopLevel().and().systemId().eq(1L);
        String queryString = toRSQL(query);
        verify(httpService).findAll(eq(queryString));
    }

    @Test
    public void shouldUpdateVariables() {
        hierarchyProvider.setVariables(1L, Collections.singleton(mock(Variable.class)));
        hierarchyProvider.addVariables(1L, Collections.singleton(100L));
        hierarchyProvider.removeVariables(1L, Collections.singleton(100L));

        verify(httpService).setVariables(eq(1L), any());
        verify(httpService).addVariables(eq(1L), eq(Sets.newHashSet(100L)));
        verify(httpService).removeVariables(eq(1L), eq(Sets.newHashSet(100L)));
    }

    @Test
    public void shouldFindVariables() {
        Set<Variable> variables = Collections.singleton(mock(Variable.class));
        when(httpService.getVariables(1L)).thenReturn(variables);

        Set<Variable> found = hierarchyProvider.getVariables(1L);

        assertNotNull(found);
        assertEquals(found, variables);
        verify(httpService).getVariables(eq(1L));
    }

    @Test
    public void shouldUpdateEntities() {
        hierarchyProvider.setEntities(1L, Collections.singleton(mock(Entity.class)));
        hierarchyProvider.addEntities(1L, Collections.singleton(100L));
        hierarchyProvider.removeEntities(1L, Collections.singleton(100L));

        verify(httpService).setEntities(eq(1L), any());
        verify(httpService).addEntities(eq(1L), eq(Sets.newHashSet(100L)));
        verify(httpService).removeEntities(eq(1L), eq(Sets.newHashSet(100L)));
    }

    @Test
    public void shouldFindEntities() {
        Set<Entity> entities = Collections.singleton(mock(Entity.class));
        when(httpService.getEntities(1L)).thenReturn(entities);

        Set<Entity> found = hierarchyProvider.getEntities(1L);

        assertNotNull(found);
        assertEquals(found, entities);
        verify(httpService).getEntities(eq(1L));
    }


    @Test
    public void shouldCreateHierarchy() {
        Hierarchy node = mock(Hierarchy.class);
        when(httpService.create(node)).thenReturn(node);
        Hierarchy hierarchy = hierarchyProvider.create(node);

        assertNotNull(hierarchy);
        assertEquals(node, hierarchy);
        verify(httpService).create(eq(node));
    }

    @Test
    public void shouldCreateAllHierarchies() {
        Hierarchy node1 = mock(Hierarchy.class);
        when(node1.getName()).thenReturn("A");
        Hierarchy node2 = mock(Hierarchy.class);
        when(node2.getName()).thenReturn("B");

        Set<Hierarchy> hierarchies = new HashSet<>(Arrays.asList(node1, node2));
        when(httpService.createAll(hierarchies)).thenReturn(hierarchies);

        Map<String, Hierarchy> createdHierarchies = hierarchyProvider.createAll(hierarchies).stream()
                .collect(Collectors.toMap(Hierarchy::getName, Function.identity()));

        for (Hierarchy node : hierarchies) {
            Hierarchy hierarchy = createdHierarchies.get(node.getName());
            assertNotNull(hierarchy);
            assertEquals(node, hierarchy);
        }
        verify(httpService, times(1)).createAll(hierarchies);
    }

    @Test
    public void shouldUpdateHierarchy() {
        Hierarchy node = mock(Hierarchy.class);
        when(httpService.update(node)).thenReturn(node);
        Hierarchy hierarchy = hierarchyProvider.update(node);

        assertNotNull(hierarchy);
        assertEquals(node, hierarchy);
        verify(httpService).update(eq(node));
    }

    @Test
    public void shouldUpdateAllHierarchies() {
        Hierarchy node1 = mock(Hierarchy.class);
        when(node1.getName()).thenReturn("A");
        Hierarchy node2 = mock(Hierarchy.class);
        when(node2.getName()).thenReturn("B");

        Set<Hierarchy> hierarchies = new HashSet<>(Arrays.asList(node1, node2));
        when(httpService.updateAll(hierarchies)).thenReturn(hierarchies);

        Map<String, Hierarchy> createdHierarchies = hierarchyProvider.updateAll(hierarchies).stream()
                .collect(Collectors.toMap(Hierarchy::getName, Function.identity()));

        for (Hierarchy node : hierarchies) {
            Hierarchy hierarchy = createdHierarchies.get(node.getName());
            assertNotNull(hierarchy);
            assertEquals(node, hierarchy);
        }
        verify(httpService, times(1)).updateAll(hierarchies);
    }

    @Test
    public void shouldDeleteLeafHierarchy() {
        long hierarchyId = 10L;
        Hierarchy node = mock(Hierarchy.class);
        when(node.getId()).thenReturn(hierarchyId);
        doNothing().when(httpService).deleteLeaf(hierarchyId);

        hierarchyProvider.deleteLeaf(node);

        verify(httpService, times(1)).deleteLeaf(hierarchyId);
    }

    @Test
    public void shouldDeleteAllLeafHierarchies() {
        Set<Hierarchy> hierarchies = new HashSet<>();
        Set<Long> hierarchyIds = new HashSet<>(Arrays.asList(1L, 2L, 3L, 4L));
        for (long id : hierarchyIds) {
            Hierarchy node = mock(Hierarchy.class);
            when(node.getId()).thenReturn(id);
            hierarchies.add(node);
        }

        doNothing().when(httpService).deleteAllLeaves(hierarchyIds);

        hierarchyProvider.deleteAllLeaves(hierarchies);

        verify(httpService, times(1)).deleteAllLeaves(hierarchyIds);
    }

    @Test
    public void shouldGetHierarchiesForVariables() {
        Set<Long> variableIds = Sets.newHashSet(1L, 2L);
        Hierarchy node1 = mock(Hierarchy.class);
        when(node1.getId()).thenReturn(1L);
        Hierarchy node2 = mock(Hierarchy.class);
        when(node2.getId()).thenReturn(2L);

        Set<VariableHierarchies> variableHierarchies = new HashSet<>();
        variableHierarchies
                .add(VariableHierarchies.builder().variableId(1L).hierarchies(Sets.newHashSet(node1, node2)).build());
        variableHierarchies
                .add(VariableHierarchies.builder().variableId(2L).hierarchies(Sets.newHashSet(node1)).build());

        when(httpService.getHierarchiesForVariables(variableIds)).thenReturn(variableHierarchies);

        Map<Long, Set<Hierarchy>> hierarchiesByVariableId = hierarchyProvider.getHierarchiesForVariables(variableIds)
                .stream()
                .collect(Collectors.toMap(VariableHierarchies::getVariableId, VariableHierarchies::getHierarchies));
        assertEquals(2, hierarchiesByVariableId.size());

        Set<Hierarchy> hierarchies = hierarchiesByVariableId.get(1L);
        assertNotNull(hierarchies);
        assertEquals(2, hierarchies.size());
        assertArrayEquals(new long[] { 1L, 2L }, hierarchies.stream().mapToLong(Hierarchy::getId).sorted().toArray());

        Set<Hierarchy> hierarchies2 = hierarchiesByVariableId.get(2L);
        assertNotNull(hierarchies2);
        assertEquals(1, hierarchies2.size());
        assertEquals(1L, Iterables.getOnlyElement(hierarchies2).getId());

        verify(httpService, times(1)).getHierarchiesForVariables(variableIds);
    }

    @Test
    public void shouldGetHierarchyIdsForVariables() {
        Set<Long> variableIds = Sets.newHashSet(1L, 2L);
        Hierarchy node1 = mock(Hierarchy.class);
        when(node1.getId()).thenReturn(1L);
        Hierarchy node2 = mock(Hierarchy.class);
        when(node2.getId()).thenReturn(2L);

        Set<VariableHierarchyIds> variableHierarchyIds = new HashSet<>();
        variableHierarchyIds.add(VariableHierarchyIds.builder().variableId(1L)
                .hierarchyIds(Sets.newHashSet(node1.getId(), node2.getId())).build());
        variableHierarchyIds
                .add(VariableHierarchyIds.builder().variableId(2L).hierarchyIds(Sets.newHashSet(node1.getId()))
                        .build());

        when(httpService.getHierarchyIdsForVariables(variableIds)).thenReturn(variableHierarchyIds);

        Map<Long, Set<Long>> hierarchyIdsByVariableId = hierarchyProvider.getHierarchyIdsForVariables(variableIds)
                .stream()
                .collect(Collectors.toMap(VariableHierarchyIds::getVariableId, VariableHierarchyIds::getHierarchyIds));
        assertEquals(2, hierarchyIdsByVariableId.size());

        Set<Long> hierarchies = hierarchyIdsByVariableId.get(1L);
        assertNotNull(hierarchies);
        assertEquals(2, hierarchies.size());
        assertThat(hierarchies).containsOnly(1L, 2L);

        Set<Long> hierarchies2 = hierarchyIdsByVariableId.get(2L);
        assertNotNull(hierarchies2);
        assertEquals(1, hierarchies2.size());
        assertThat(hierarchies2).containsOnly(1L);

        verify(httpService, times(1)).getHierarchyIdsForVariables(variableIds);
    }
}
