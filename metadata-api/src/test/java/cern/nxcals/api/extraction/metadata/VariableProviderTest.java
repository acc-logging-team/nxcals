/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.api.extraction.metadata.feign.VariableClient;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import cern.nxcals.api.metadata.queries.ConditionWithOptions;
import cern.nxcals.api.metadata.queries.VariableQueryWithOptions;
import cern.nxcals.common.utils.MapUtils;
import cern.nxcals.common.utils.SetUtils;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.google.common.collect.Sets;
import org.apache.avro.Schema;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.SortedSet;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static java.util.Collections.emptySet;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class VariableProviderTest {

    private static final String MOCK_SYSTEM_NAME = "MOCK-SYSTEM";
    private static final String TEST_VAR_NAME = "NAME";
    private VariableProvider variableProvider;

    @Mock
    private VariableClient httpService;

    @BeforeEach
    public void setup() {
        this.variableProvider = new VariableProvider(httpService);
        reset(httpService);
    }

    private Variable createVariableData(String variableName, String description,
            SortedSet<VariableConfig> configs) {
        return Variable.builder().variableName(variableName).description(description).configs(configs).build();
    }

    @Test
    public void shouldFindOneByQuery() {
        //given
        Condition<Variables> query = Variables.suchThat().systemName().eq(MOCK_SYSTEM_NAME).and().variableName()
                .eq(TEST_VAR_NAME);
        String queryString = toRSQL(query);
        Variable variableData = mock(Variable.class);
        when(httpService.findAll(queryString)).thenReturn(Sets.newHashSet(variableData));

        //when
        Optional<Variable> data = variableProvider.findOne(query);

        //then
        assertNotNull(data.get());
    }

    @Test
    public void shouldFindAllByQuery() {
        //given
        Condition<Variables> query = Variables.suchThat().systemName().eq(MOCK_SYSTEM_NAME).and().variableName()
                .eq(TEST_VAR_NAME);
        String queryString = toRSQL(query);
        Variable variableData = mock(Variable.class);
        when(httpService.findAll(queryString)).thenReturn(Sets.newHashSet(variableData));

        //when
        Set<Variable> data = variableProvider
                .findAll(query);

        //then
        assertNotNull(data);
        assertThat(data).containsExactly(variableData);
    }

    @Test
    void shouldFindAllWithoutConfigByQuery() {
        //given
        Condition<cern.nxcals.api.metadata.queries.Variables> condition = cern.nxcals.api.metadata.queries.Variables.suchThat()
                .systemName().eq(MOCK_SYSTEM_NAME).and().variableName()
                .eq(TEST_VAR_NAME);
        VariableQueryWithOptions query = new VariableQueryWithOptions(condition).noConfigs();
        String queryString = toRSQL(query.getCondition());
        Variable variableData = mock(Variable.class);
        when(httpService.findAll(queryString,
                VariableQueryWithOptions.getDefaultInstance().noConfigs().toJson())).thenReturn(
                List.of(variableData));

        //when
        List<Variable> data = variableProvider.findAll(query);

        //then
        assertNotNull(data);
        assertThat(data).containsExactly(variableData);
    }

    @Test
    void shouldFindAllWithNewVariableQuery() {
        //given
        ConditionWithOptions<cern.nxcals.api.metadata.queries.Variables, VariableQueryWithOptions> condition =
                cern.nxcals.api.metadata.queries.Variables.suchThat()
                        .systemName().eq(MOCK_SYSTEM_NAME).and().variableName()
                        .eq(TEST_VAR_NAME);
        String queryString = toRSQL(condition);
        Variable variableData = mock(Variable.class);
        when(httpService.findAll(queryString,
                VariableQueryWithOptions.getDefaultInstance().toJson())).thenReturn(
                List.of(variableData));

        //when
        Set<Variable> data = variableProvider.findAll(condition);

        //then
        assertNotNull(data);
        assertThat(data).containsExactly(variableData);
    }

    @Nested
    class FindSchemasTests {
        private final Instant now = Instant.now();
        private final long id1 = 1L;
        private final long id2 = 2L;

        private final Set<Long> oneIdSet = Set.of(id1);
        private final Set<Long> ids = Set.of(id1, id2);
        private final Set<Variable> variables = SetUtils.map(ids, id -> {
            Variable var = mock(Variable.class);
            when(var.getId()).thenReturn(id);
            return var;
        });

        private final Schema schema1 = Schema.create(Schema.Type.LONG);
        private final Schema schema2 = Schema.create(Schema.Type.BOOLEAN);
        private final TimeWindow tw = TimeWindow.between(now, now);

        @Test
        void shouldFindSchemaForOneVariable() {
            // given
            when(httpService.findSchemas(oneIdSet, now, now)).thenReturn(
                    Map.of(
                            id1, Map.of(
                                    schema1.toString(),
                                    Set.of(tw)
                            )
                    )
            );

            // when
            Map<TimeWindow, Schema> schemas = variableProvider.findSchemasById(id1, tw);

            // then
            assertTrue(schemas.containsKey(tw));
            assertEquals(schema1, schemas.get(tw));
        }

        @Test
        void shouldReturnEmptyMapIfThereIsNoSchemas() {
            // given
            when(httpService.findSchemas(oneIdSet, now, now)).thenReturn(
                    Map.of(
                            id1, Collections.emptyMap()
                    )
            );

            // when
            Map<TimeWindow, Schema> schemas = variableProvider.findSchemasById(id1, tw);

            // then
            assertTrue(schemas.isEmpty());
        }

        @Test
        void shouldReturnEmptyMapIfThereIsNoValidSchemas() {
            // given
            when(httpService.findSchemas(oneIdSet, now, now)).thenReturn(
                    Map.of(
                            id1, Map.of(
                                    schema1.toString(),
                                    emptySet()
                            )
                    )
            );

            // when
            Map<TimeWindow, Schema> schemas = variableProvider.findSchemasById(id1, tw);

            // then
            assertTrue(schemas.isEmpty());
        }

        @Test
        void shouldReturnMapForQueryForMultipleVariablesById() {
            // given
            when(httpService.findSchemas(ids, now, now)).thenReturn(
                    Map.of(
                            id1, Map.of(
                                    schema1.toString(),
                                    Set.of(tw)
                            ),
                            id2, Map.of(
                                    schema2.toString(),
                                    Set.of(tw)
                            )
                    )
            );

            // when
            Map<Long, Map<TimeWindow, Schema>> schemas = variableProvider.findSchemasById(ids, tw);

            // then
            assertEquals(ids, schemas.keySet());
            assertTrue(schemas.get(id1).containsKey(tw));
            assertTrue(schemas.get(id2).containsKey(tw));
            assertEquals(schema1, schemas.get(id1).get(tw));
            assertEquals(schema2, schemas.get(id2).get(tw));
        }

        @Test
        void shouldReturnMapForQueryForMultipleVariables() {
            // given
            when(httpService.findSchemas(ids, now, now)).thenReturn(
                    Map.of(
                            id1, Map.of(
                                    schema1.toString(),
                                    Set.of(tw)
                            ),
                            id2, Map.of(
                                    schema2.toString(),
                                    Set.of(tw)
                            )
                    )
            );

            // when
            Map<Variable, Map<TimeWindow, Schema>> schemas = variableProvider.findSchemas(variables, tw);

            // then
            assertEquals(variables, schemas.keySet());

            Map<Long, Map<TimeWindow, Schema>> byIds = MapUtils.mapKeys(schemas, Variable::getId);
            assertTrue(byIds.get(id1).containsKey(tw));
            assertTrue(byIds.get(id2).containsKey(tw));
            assertEquals(schema1, byIds.get(id1).get(tw));
            assertEquals(schema2, byIds.get(id2).get(tw));
        }
    }

}
