/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.domain.Partition;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.extraction.metadata.feign.PartitionClient;
import cern.nxcals.api.extraction.metadata.queries.Partitions;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.google.common.collect.Sets;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;

import static cern.nxcals.common.utils.RSQLUtils.toRSQL;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@ExtendWith(MockitoExtension.class)
public class PartitionProviderTest extends AbstractProviderTest {
    private PartitionProvider partitionProvider;

    @Mock
    private PartitionClient httpService;

    @BeforeEach
    public void setup() {
        partitionProvider = new PartitionProvider(httpService);
        reset(httpService);
    }

    @Test
    public void shouldNotObtainPartitionDataForNonExistingPartition() {
        SystemSpec systemSpec = SystemSpec.builder().name(SYSTEM_NAME).entityKeyDefinitions("").partitionKeyDefinitions(PARTITION_SCHEMA2_STRING).timeKeyDefinitions("").build();
        Condition<Partitions> condition = Partitions.suchThat().systemId().eq(SYSTEM_ID).and().keyValues()
                .eq(systemSpec,PARTITION_KEY_VALUES2);
        String queryString = toRSQL(condition);

        when(httpService.findAll(queryString)).thenReturn(Collections.emptySet());
        Partition data = partitionProvider.findOne(condition).orElse(null);
        assertNull(data);

        //test no cache
        data = partitionProvider.findOne(condition).orElse(null);
        assertNull(data);
        verify(httpService, times(2)).findAll(queryString);
    }

    @Test
    public void shouldNotObtainPartitionCachedDataForExistingPart() {
        SystemSpec systemSpec = SystemSpec.builder().name(SYSTEM_NAME).entityKeyDefinitions("").partitionKeyDefinitions(PARTITION_SCHEMA2_STRING).timeKeyDefinitions("").build();

        Partition partData = Partition.builder().systemSpec(systemSpec).keyValues(PARTITION_KEY_VALUES2).build();
        Condition<Partitions> condition = Partitions.suchThat().systemId().eq(SYSTEM_ID).and().keyValues()
                .eq(systemSpec,PARTITION_KEY_VALUES2);

        String queryString = toRSQL(condition);

        when(httpService.findAll(queryString)).thenReturn(Sets.newHashSet(partData));

        int numberOfIterations = 10;

        for (int i = 0; i < numberOfIterations; i++) {
            Partition data = partitionProvider.findOne(condition)
                    .orElseThrow(() -> new IllegalArgumentException("No such partition"));
            assertNotNull(data);
            assertEquals(partData, data);

            data = partitionProvider.findOne(condition)
                    .orElseThrow(() -> new IllegalArgumentException("No such partition"));
            assertNotNull(data);
            assertEquals(partData, data);
        }
        verify(httpService, times(2 * numberOfIterations)).findAll(queryString);
    }
}
