package cern.nxcals.api.extraction.metadata.security;

import cern.rbac.common.RbaToken;
import cern.rbac.common.TokenFormatException;
import cern.rbac.common.test.TestTokenBuilder;
import cern.rbac.util.holder.ClientTierTokenHolder;
import feign.Request;
import feign.RequestTemplate;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;
import java.util.Base64;
import java.util.HashMap;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;

public class RbacTokenProviderTest {

    private Request request;

    @BeforeEach
    public void setUp() {
        request = Request.create(Request.HttpMethod.GET, "https://service.com", new HashMap<>(), Request.Body.empty(), new RequestTemplate());
    }

    @AfterEach
    public void tearDown() {
        ClientTierTokenHolder.clear();
    }

    @Test
    public void shouldProvideRbacTokenIfPresent() throws TokenFormatException {
        //given
        RbaToken token = TestTokenBuilder.newInstance().username("acclog").build();
        ClientTierTokenHolder.setRbaToken(token);
        RbacTokenProvider provider = new RbacTokenProvider();

        //when
        Optional<String> auth = provider.apply(request);

        //then
        byte[] bytes = Base64.getDecoder()
                .decode(auth.orElseThrow(() -> new IllegalStateException("test faild")));
        RbaToken.parseNoValidate(ByteBuffer.wrap(bytes));
    }

    @Test
    public void shouldNotProvideRbacTokenIfAbsent() throws TokenFormatException {
        //given
        RbacTokenProvider provider = new RbacTokenProvider();

        //when
        Optional<String> auth = provider.apply(request);

        //then
        assertFalse(auth.isPresent());

    }
}