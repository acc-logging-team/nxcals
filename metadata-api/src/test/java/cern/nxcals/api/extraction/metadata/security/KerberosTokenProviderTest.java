package cern.nxcals.api.extraction.metadata.security;

import cern.nxcals.api.extraction.metadata.security.kerberos.KerberosContext;
import cern.nxcals.common.utils.Lazy;
import feign.Request;
import feign.RequestTemplate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.Optional;

import static cern.nxcals.common.Constants.SERVICE_TYPE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class KerberosTokenProviderTest {

    public static final String AUTH_TOKEN = "AuthToken";
    @Mock
    private KerberosContext kerberosContext;
    private Request request;

    @BeforeEach
    public void setUp() throws Exception {
        request = Request.create(Request.HttpMethod.GET, "https://service.com", new HashMap<>(), Request.Body.empty(), new RequestTemplate());
    }

    @Test
    public void shouldReturnAuthToken() {
        //given
        when(kerberosContext.requestTokenFor(any())).thenReturn(AUTH_TOKEN);
        KerberosTokenProvider kerberosTokenProvider = new KerberosTokenProvider(new Lazy<>(() -> kerberosContext),
                SERVICE_TYPE);
        //when
        Optional<String> auth = kerberosTokenProvider.apply(request);

        //then
        assertEquals(KerberosTokenProvider.NEGOTIATE + AUTH_TOKEN,
                auth.orElseThrow(() -> new IllegalStateException("Test failed")));
    }

    @Test
    public void shouldThrowOnError() {
        //given
        when(kerberosContext.requestTokenFor(any())).thenThrow(new NullPointerException("Test"));
        KerberosTokenProvider kerberosTokenProvider = new KerberosTokenProvider(new Lazy<>(() -> kerberosContext),
                SERVICE_TYPE);
        //when

        //then exception
        assertThrows(KerberosAuthenticationException.class, () -> kerberosTokenProvider.apply(request));
    }
}
