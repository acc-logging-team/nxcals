package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.extraction.metadata.feign.CompactionClient;
import cern.nxcals.common.domain.CompactionJob;
import cern.nxcals.common.domain.DataProcessingJob;
import cern.nxcals.common.domain.DataProcessingJob.JobType;
import cern.nxcals.common.domain.MergeCompactionJob;
import cern.nxcals.common.domain.RestageJob;
import cern.nxcals.internal.extraction.metadata.InternalCompactionService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class CompactionProviderTest {
    @Mock
    private CompactionClient httpClient;

    @Mock
    private InternalCompactionService compactionService;

    private CompactionProvider compactionProvider;

    @BeforeEach
    public void setUp() throws Exception {
        compactionProvider = new CompactionProvider(httpClient);
    }

    @Test
    public void shouldGetCompactionJobOnlyWhenRequestedByProperJobType() {
        // given
        int maxJobs = 10;
        JobType jobType = JobType.COMPACT;

        // when
        Mockito.when(httpClient.getJobs(maxJobs, jobType)).thenReturn(generateCompactionJobs(maxJobs));

        List<DataProcessingJob> dataProcessingJobs = compactionProvider.getDataProcessJobs(maxJobs, jobType);

        assertEquals(10, dataProcessingJobs.size());
        assertEquals(jobType, dataProcessingJobs.iterator().next().getType());

        Mockito.verify(httpClient, Mockito.only()).getJobs(maxJobs, jobType);
    }

    @Test
    public void shouldGetRestageJobOnlyWhenRequestedByProperJobType() {
        // given
        int maxJobs = 10;
        JobType jobType = JobType.RESTAGE;

        // when
        Mockito.when(httpClient.getJobs(maxJobs, jobType)).thenReturn(generateRestageJobs(maxJobs));

        List<DataProcessingJob> dataProcessingJobs = compactionProvider.getDataProcessJobs(maxJobs, jobType);

        assertEquals(10, dataProcessingJobs.size());
        assertEquals(jobType, dataProcessingJobs.iterator().next().getType());

        Mockito.verify(httpClient, Mockito.only()).getJobs(maxJobs, jobType);
    }

    @Test
    public void shouldGetMergeCompactJobOnlyWhenRequestedByProperJobType() {
        // given
        int maxJobs = 10;
        JobType jobType = JobType.MERGE_COMPACT;

        // when
        Mockito.when(httpClient.getJobs(maxJobs, jobType)).thenReturn(generateMergeCompactJobs(maxJobs));

        List<DataProcessingJob> dataProcessingJobs = compactionProvider.getDataProcessJobs(maxJobs, jobType);

        assertEquals(10, dataProcessingJobs.size());
        assertEquals(jobType, dataProcessingJobs.iterator().next().getType());

        Mockito.verify(httpClient, Mockito.only()).getJobs(maxJobs, jobType);
    }

    private List<DataProcessingJob> generateCompactionJobs(int size) {
        List<DataProcessingJob> jobs = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            jobs.add(CompactionJob.builder()
                    .sourceDir(URI.create("/source"))
                    .destinationDir(URI.create("/dest"))
                    .partitionCount(1)
                    .systemId(0)
                    .files(Collections.emptyList())
                    .sortEnabled(false)
                    .build());
        }
        return jobs;
    }

    private List<DataProcessingJob> generateRestageJobs(int size) {
        List<DataProcessingJob> jobs = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            jobs.add(RestageJob.builder()
                    .sourceDir(URI.create("/source"))
                    .destinationDir(URI.create("/dest"))
                    .systemId(0)
                    .files(Collections.emptyList())
                    .dataFiles(Collections.emptyList())
                    .build());
        }
        return jobs;
    }

    private List<DataProcessingJob> generateMergeCompactJobs(int size) {
        List<DataProcessingJob> jobs = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            jobs.add(MergeCompactionJob.builder()
                    .sourceDir(URI.create("/source"))
                    .destinationDir(URI.create("/dest"))
                    .systemId(0)
                    .files(Collections.emptyList())
                    .restagingReportFile(URI.create("/restage/report.info"))
                    .build());
        }
        return jobs;
    }
}
