package cern.nxcals.api.extraction.metadata;

import cern.nxcals.api.extraction.metadata.security.AuthTokenProvider;
import cern.nxcals.api.extraction.metadata.security.AuthenticationException;
import cern.nxcals.api.extraction.metadata.security.KerberosTokenProvider;
import cern.nxcals.api.extraction.metadata.security.RbacTokenProvider;
import feign.ribbon.RibbonClient;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertThrows;

class AbstractClientFactoryTest {

    @Mock
    RibbonClient ribboncClient;

    AbstractClientFactory clientFactory = new AbstractClientFactory("dummy", ribboncClient);

    @Test
    void shouldGetBothAuthTokenProviders() {
        //when
        List<AuthTokenProvider> providers = clientFactory.getAuthTokenProviders(true, true);
        //then
        assertEquals(2, providers.size());
        //ensure that by default RBAC has a higher order
        assertInstanceOf(RbacTokenProvider.class, providers.get(0));
        assertInstanceOf(KerberosTokenProvider.class, providers.get(1));
    }

    @Test
    void shouldGetOnlyRbacAuthTokenProviderWhenKerberosDisabled() {
        //when
        List<AuthTokenProvider> providers = clientFactory.getAuthTokenProviders(false, true);
        //then
        assertEquals(1, providers.size());
        assertInstanceOf(RbacTokenProvider.class, providers.get(0));
    }

    @Test
    void shouldGetOnlyKerberosAuthTokenProviderWhenRbacDisabled() {
        //when
        List<AuthTokenProvider> providers = clientFactory.getAuthTokenProviders(true, false);
        //then
        assertEquals(1, providers.size());
        assertInstanceOf(KerberosTokenProvider.class, providers.get(0));
    }

    @Test
    void shouldThrowWhenBothRbacAndKerberosDisabled() {
        assertThrows(AuthenticationException.class, () -> clientFactory.getAuthTokenProviders(false, false));
    }

}