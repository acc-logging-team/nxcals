package cern.nxcals.api.extraction.metadata.demo;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.api.extraction.metadata.EntityService;
import cern.nxcals.api.extraction.metadata.InternalServiceClientFactory;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.extraction.metadata.queries.Entities;
import cern.nxcals.api.extraction.metadata.queries.SystemSpecs;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import cern.nxcals.internal.extraction.metadata.InternalEntityService;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ClientDemoVariablesCreation {

    //    private static final EntityService entityService = ServiceClientFactory.createEntityService();
    //    private static final InternalEntityService internalEntityService = InternalServiceClientFactory
    //            .createEntityService();
    //    private static final InternalPartitionService internalPartitionService = InternalServiceClientFactory
    //            .createPartitionService();
    //    private static final VariableService variableService = ServiceClientFactory.createVariableService();
    //    private static final InternalSchemaService internalSchemaService = InternalServiceClientFactory
    //            .createEntitySchemaService();
    //    private static final EntitiesResourcesService entitiesResourcesService = ServiceClientFactory
    //            .createEntityResourceService();

    protected static final InternalEntityService internalEntityService = InternalServiceClientFactory
            .createEntityService();
    private static final EntityService entityService = ServiceClientFactory.createEntityService();
    private static final VariableService variableService = ServiceClientFactory.createVariableService();

    private static final String RANDOM_STRING = RandomStringUtils.randomAscii(64);

    private static final String SYSTEM_NAME = "CMW-MIG-DEV-VHYHYNIA";
    private static final SystemSpecService systemService = ServiceClientFactory.createSystemSpecService();
    private static final SystemSpec SYSTEM = systemService.findOne(SystemSpecs.suchThat().name().eq(SYSTEM_NAME))
            .orElseThrow(() -> new IllegalArgumentException("No such system name " + SYSTEM_NAME));

    private static final long SYSTEM_ID = 4;
    private static final long ENTITY_ID_1 = 35731;
    private static final long ENTITY_ID_2 = 35732;
    private static final String ENTITY_DEVICE_1 = "device_test1_vhyhynia1";
    private static final String ENTITY_DEVICE_2 = "device_test2_vhyhynia2";
    private static final String ENTITY_PROPERTY_1 = "property_test1_vhyhynia1";
    private static final String ENTITY_PROPERTY_2 = "property_test2_vhyhynia2";
    private static final Map<String, Object> ENTITY_KEY_VALUES_1 = ImmutableMap
            .of("device", ENTITY_DEVICE_1, "property", ENTITY_PROPERTY_1);
    private static final Map<String, Object> ENTITY_KEY_VALUES_2 = ImmutableMap
            .of("device", ENTITY_DEVICE_2, "property", ENTITY_PROPERTY_2);
    private static final String VARIABLE_NAME_TEST1_VHYHYNIA1 = "variable_test1_vhyhynia1";
    private static final String VARIABLE_NAME_TEST2_VHYHYNIA2 = "variable_test2_vhyhynia2";
    private static final String FIELD_NAME_TEST1_VHYHYNIA1 = "field_test1_vhyhynia1";
    private static final String FIELD_NAME_TEST2_VHYHYNIA2 = "field_test2_vhyhynia2";
    private static final String VARIABLE_DESCRIPTION_TEST1_VHYHYNIA1 = "description_test1_vhyhynia1";
    private static final String VARIABLE_DESCRIPTION_TEST2_VHYHYNIA2 = "description_test2_vhyhynia2";

    //    static String variableName = "VARIABLE_NAME" + RANDOM_STRING;

    //        protected static final SystemService systemService = ServiceClientFactory.createSystemSpecService();
    //    protected static final InternalSystemService internalSystemService = InternalServiceClientFactory
    //            .createSystemSpecService();
    //    protected static final EntityService entityService = ServiceClientFactory.createEntityService();
    //    protected static final InternalEntityService internalEntityService = InternalServiceClientFactory
    //            .createEntityService();
    //    protected static final InternalPartitionService internalPartitionService = InternalServiceClientFactory
    //            .createPartitionService();
    //    protected static final VariableService variableService = ServiceClientFactory.createVariableService();
    //    protected static final InternalSchemaService internalSchemaService = InternalServiceClientFactory
    //            .createEntitySchemaService();
    //    protected static final EntitiesResourcesService entitiesResourcesService = ServiceClientFactory
    //            .createEntityResourceService();

    static {
        System.setProperty("user.timezone", "UTC");
        //        System.setProperty("java.security.krb5.conf", "/opt/vhyhynia/nxcals/build/LocalKdc/krb5.conf");
        //        System.setProperty("service.url", "https://nxcals-vhyhynia1.cern.ch:19093,https://nxcals-vhyhynia2.cern.ch:19093");
        System.setProperty("service.url", "https://nxcals-vhyhynia1.cern.ch:19093");
        System.setProperty("kerberos.principal", "vhyhynia");
        System.setProperty("kerberos.keytab", "/afs/cern.ch/user/v/vhyhynia/.keytab");
        //        System.setProperty("javax.net.ssl.trustStore", "nxcals_cacerts");
        //        System.setProperty("javax.net.ssl.trustStorePassword", "nxcals");
    }

    public static void main(String[] args) {
        createVariableForGivenEntity(ENTITY_ID_1, FIELD_NAME_TEST1_VHYHYNIA1, VARIABLE_NAME_TEST1_VHYHYNIA1,
                VARIABLE_DESCRIPTION_TEST1_VHYHYNIA1);
        //        createVariableForGivenEntity(ENTITY_ID_2, FIELD_NAME_TEST2_VHYHYNIA2, VARIABLE_NAME_TEST2_VHYHYNIA2,
        //                VARIABLE_DESCRIPTION_TEST2_VHYHYNIA2);

    }

    // the main purpose of this method is to populate database with the needed variable
    // that corresponds to the specified entity
    private static void createVariableForGivenEntity(Long entityId, String fieldName, String variableName,
            String variableDescription) {

        //lets first find some existing entity for which we will create variable
        // given
        Variable variableData = getVariableDataForNeededEntity(entityId, fieldName, variableName,
                variableDescription);
        // when
        variableService.create(variableData);
        // then
        assertNotNull(variableData);
        assertEquals(variableName, variableData.getVariableName());

        Variable foundVariable = variableService
                .findOne(Variables.suchThat().systemName().eq(SYSTEM.getName()).and().variableName().eq(variableName))
                .orElseThrow(() -> new IllegalArgumentException("No such variable name " + variableName));
        assertEquals(variableName, foundVariable.getVariableName());
    }

    private static Variable getVariableDataForNeededEntity(Long entityId, String fieldName, String variableName,
                                                           String variableDescription) {
        Entity entityData = internalEntityService.findOne(Entities.suchThat().id().eq(entityId))
                .orElseThrow(() -> new IllegalArgumentException("No such entity id " + entityId));
        assertNotNull(entityData);

        VariableConfig varConfData1 = VariableConfig.builder().entityId(entityData.getId()).fieldName(fieldName)
                .build();
        SortedSet<VariableConfig> varConfSet = new TreeSet<>();
        varConfSet.add(varConfData1);
        return Variable.builder()
                .variableName(variableName)
                .systemSpec(SYSTEM).description(variableDescription)
                .configs(varConfSet).build();
    }

    private static void createVariableForGivenEntityKeyValues(Long systemId, Map<String, Object> entityKeyValues,
            String fieldName, String variableName, String variableDescription) {

        //lets first find some existing entity for which we will create variable
        // given
        Variable variableData = getVariableDataForNeededEntityKeyValues(systemId, entityKeyValues, fieldName,
                variableName,
                variableDescription);
        // when
        variableService.create(variableData);
        // then
        assertNotNull(variableData);
        assertEquals(variableName, variableData.getVariableName());

        Variable foundVariable = variableService
                .findOne(Variables.suchThat().systemName().eq(SYSTEM.getName()).and().variableName().eq(variableName))
                .orElseThrow(() -> new IllegalArgumentException("No such variable name " + variableName));
        assertEquals(variableName, foundVariable.getVariableName());
    }

    private static Variable getVariableDataForNeededEntityKeyValues(Long systemId,
            Map<String, Object> entityKeyValues,
                                                                    String fieldName, String variableName, String variableDescription) {

        SystemSpec systemSpec = systemService.findById(systemId).orElseThrow(() -> new IllegalArgumentException("No such system with id " + systemId));
        Entity entityData = entityService
                .findOne(Entities.suchThat().systemId().eq(systemId).and().keyValues().eq(systemSpec, entityKeyValues))
                .orElseThrow(() -> new IllegalArgumentException("No such entity  " + entityKeyValues));
        assertNotNull(entityData);

        VariableConfig varConfData1 = VariableConfig.builder().entityId(entityData.getId()).fieldName(fieldName)
                .build();
        SortedSet<VariableConfig> varConfSet = new TreeSet<>();
        varConfSet.add(varConfData1);
        return Variable.builder()
                .variableName(variableName)
                .systemSpec(SYSTEM).description(variableDescription)
                .configs(varConfSet).build();
    }

}
