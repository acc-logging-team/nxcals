package cern.nxcals.api.extraction.metadata.demo;

import cern.nxcals.api.extraction.metadata.InternalServiceClientFactory;
import cern.nxcals.common.domain.DataProcessingJob;
import cern.nxcals.internal.extraction.metadata.InternalCompactionService;

import java.util.List;

/**
 * Created by ntsvetko on 7/28/17.
 */
public class CompactionServiceDemo {
//    static {
//        //This is for local debug
//        try {
//            System.setProperty("service.url", "https://" + InetAddress.getLocalHost().getHostName() + ":19093");
//        } catch (UnknownHostException exception) {
//            throw new RuntimeException(
//                    "Cannot acquire hostname programmatically, provide the name full name of localhost");
//        }
//
//        System.setProperty("kerberos.principal", "mock-system-user");
//        System.setProperty("kerberos.keytab", ".service.keytab");
//
//        System.setProperty("java.security.krb5.conf", "build/LocalKdc/krb5.conf");
//
//        System.setProperty("javax.net.ssl.trustStore", "selfsigned.jks");
//        System.setProperty("javax.net.ssl.trustStorePassword", "nxcals");
//
//    }

    static {
        //This is when we want to access our nxcals-<user>.cernc.ch dev env service
        String USER = System.getProperty("user.name");
        System.setProperty("service.url", "http://nxcals-" + USER + "1.cern.ch:19093");
    }

    public static void main(String[] args) {
        InternalCompactionService compactionService = InternalServiceClientFactory.createCompactionService();
        List<DataProcessingJob> dataProcessingJobs = compactionService.getDataProcessJobs(
                1, DataProcessingJob.JobType.COMPACT);
        System.out.println(dataProcessingJobs);

        List<DataProcessingJob> dataProcessingJobs1 = compactionService.getDataProcessJobs(
                1, DataProcessingJob.JobType.RESTAGE);
        System.out.println(dataProcessingJobs1);

        List<DataProcessingJob> dataProcessingJobs2 = compactionService.getDataProcessJobs(
                1, DataProcessingJob.JobType.MERGE_COMPACT);
        System.out.println(dataProcessingJobs2);
    }
}
