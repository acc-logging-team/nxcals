package cern.nxcals.api.extraction.metadata.security.validation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class ServiceUrlSanitizerTest {

    private ServiceUrlSanitizer instance;

    @BeforeEach
    public void setUp() throws Exception {
        instance = ServiceUrlSanitizer.INSTANCE;
    }

    @Test
    public void shouldThrowOnNullServiceUrl() {
        String serviceUrl = null;

        assertThrows(NullPointerException.class, () -> instance.apply(serviceUrl));
    }

    @Test
    public void shouldThrowOnEmptyServiceUrl() {
        String serviceUrl = "";

        assertThrows(IllegalArgumentException.class, () -> instance.apply(serviceUrl));
    }

    @Test
    public void shouldThrowOnBlankServiceUrl() {
        String serviceUrl = " ";

        assertThrows(IllegalArgumentException.class, () -> instance.apply(serviceUrl));
    }

    @Test
    public void shouldThrowOnInvalidServiceUrlPlaceholders() {
        String serviceUrl = " , ,. ,, , ";

        assertThrows(IllegalArgumentException.class, () -> instance.apply(serviceUrl));
    }

    @Test
    public void shouldThrowOnInvalidServiceUrl() {
        String serviceUrl = "thisIsNotAUrl,definitely,cannot%ContactService.com";

        assertThrows(IllegalArgumentException.class, () -> instance.apply(serviceUrl));
    }

    @Test
    public void shouldThrowWhenServiceUrlContainsInvalidUrl() {
        String serviceUrl = "https://www.valid.com:8166,cannot%ContactService.com";

        assertThrows(IllegalArgumentException.class, () -> instance.apply(serviceUrl));
    }

    @Test
    public void shouldThrowWhenServiceUrlContainsEmptyUrlPlaceholders() {
        String url = "https://www.valid.com:8166";
        String otherUrl = "https://other-valid.com:8166";

        assertThrows(IllegalArgumentException.class, () -> instance.apply(url + ",,    ," + otherUrl));
    }

    @Test
    public void shouldIgnoreWhenServiceUrlContainsLeadingCommas() {
        String url = "https://www.valid.com:8166";
        String otherUrl = "https://other-valid.com:8166";
        String serviceUrl = String.join(",", url, otherUrl) + ",";

        String sanitizedServiceUrl = instance.apply(serviceUrl);
        assertFalse(sanitizedServiceUrl.contains(" "));
        assertEquals(String.join(",", url, otherUrl), sanitizedServiceUrl);
    }

    @Test
    public void shouldNotSanitizeSingleValidServiceUrl() {
        String serviceUrl = "https://www.valid.com:8166";

        String sanitizedServiceUrl = instance.apply(serviceUrl);
        assertEquals(serviceUrl, sanitizedServiceUrl);
    }

    @Test
    public void shouldNotSanitizeValidServiceUrl() {
        String url = "https://www.valid.com:8166";
        String otherUrl = "https://other-valid.com:8166";
        String serviceUrl = String.join(",", url, otherUrl);

        String sanitizedServiceUrl = instance.apply(serviceUrl);
        assertFalse(sanitizedServiceUrl.contains(" "));
        assertEquals(serviceUrl, sanitizedServiceUrl);
    }

    @Test
    public void shouldPromoteToHttpsSchemeWhenServiceUrlContainsHttp() {
        String url = "https://www.valid.com:8166";
        String otherUrl = "other-valid.com:8166";

        String sanitizedServiceUrl = instance.apply(String.join(",", url, "http://" + otherUrl));

        assertEquals(String.join(",", url, "https://" + otherUrl), sanitizedServiceUrl);
    }

    @Test
    public void shouldOnlyPromoteSchemeToHttpsWhenServiceUrlContainsMultipleHttpMatches() {
        String url = "www.valid.http.com:8166";
        String otherUrl = "other-httpsvalid.http.com:8166";

        String sanitizedServiceUrl = instance.apply(String.join(",", "http://" + url, "http://" + otherUrl));

        assertEquals(String.join(",", "https://" + url, "https://" + otherUrl), sanitizedServiceUrl);
    }

}
