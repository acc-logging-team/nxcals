#
# Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
#
version=1.6.7-SNAPSHOT
systemProp.http.proxyHost=photons-resources.cern.ch
systemProp.http.proxyPort=8888
systemProp.http.nonProxyHosts=*.cern.ch|localhost
systemProp.https.proxyHost=photons-resources.cern.ch
systemProp.https.proxyPort=8888
systemProp.https.nonProxyHosts=*.cern.ch|localhost
org.gradle.jvmargs=-Xmx4g
#Hadoop config
baseHadoopConfigUrl=http://hadoop-config.web.cern.ch/files/hadoop/conf/etc
ansibleCommonVersion=1.0.25
cernJapcVersion=7.21.1
#Hadoop versions ;
hadoopVersion=3.3.6
hbaseConnectorVersion=1.1.1-3.5-s_2.12-NXCALS_1
sparkVersion=3.5.3
hadoopVersionSuffixInSpark=3
scalaLibraryVersion=2.12.15
hbaseVersion=2.3.4
guavaVersion=33.2.1-jre
curatorVersion=5.1.0
zookeeperVersion=3.5.9
gsonVersion=2.11.0
jodaTimeVersion=2.13.0
springSecurityKerberosVersion=1.0.1.RELEASE
log4jVersion=2.24.3
slf4jVersion=1.7.36
servletApiVersion=4.0.1
aspectjVersion=1.9.22.1
caffeineVersion=2.6.2
avroVersion=1.8.2
kafkaClientsVersion=2.7.0
kafkaStreamsVersion=0.10.2.1
typesafeConfigVersion=1.3.1
validationApiVersion=2.0.1.Final
htraceVersion=4.2.0-incubating
dropwizardMetricsVersion=4.2.22
tomcatEmbedElVersion=8.5.14
cmwDataxVersion=10.4.1
ccdaClientVersion=1.30.1
accsoftCommonsDbAccessVersion=2.8.2
ojdbc8Version=19.24.0.0
liquibaseVersion=4.7.1
assertjCoreVersion=3.26.3
jmhVersion=1.36
commonsConfigurationVersion=1.10
commonsCollectionsVersion=3.2.2
apacheHttpComponentsClientVersion=4.5.14
apacheHttpComponentsCoreVersion=4.4.16
apacheCommonsValidatorVersion=1.7
commonsPoolVersion=1.6
xerialSnappyVersion=1.1.8.4
commonsBeanutilsVersion=1.9.4
nettyVersion=3.9.9.Final
nettyAllVersion=4.1.68.Final
elApiVersion=3.0.0
scaffeineVersion=2.5.0
qbuildersVersion=1.11
rsqlParserVersion=2.1.0
rsqlJpaVersion=2.0.2
#needed by the SHC connector, pulled in with newer version by Spark 2.4+, forced to the older
json4sVersion=3.7.0-M11
fasterxmlJacksonVersion=2.17.2
openFeignVersion=11.10
netflixArchaiusVersion=0.7.6
junitVersion=4.13.2
junitJupiterVersion=5.11.0
apacheCommonsMathVersion=3.6.1
apacheCommonsLangVersion=3.17.0
commonsIoVersion=2.17.0
commonsCodecVersion=1.15
commonsCliVersion=1.9.0
jaxbApiVersion=2.3.1
jaxbRuntimeVersion=2.3.9
h2Version=1.4.197
jerseyVersion=2.32
jettyVersion=9.4.31.v20200723
hibernateVersion=5.6.15.Final
hibernateValidatorVersion=6.0.15.Final
springBootVersion=2.7.18
springVersion=5.3.39
springSecurityVersion=5.8.14
springRetryVersion=1.2.4.RELEASE
micrometerVersion=1.1.3
junitPioneerVersion=2.2.0
# jdk 11 partially compatible
lombokVersion=1.18.34
mockitoVersion=5.0.0
mockitoJunitJupiterVersion=3.11.2
hamcrestVersion=2.2
rbacClientVersion=10.7.1
rbacUtilVersion=10.7.1
rbacCommonVersion=10.7.1
objenesisVersion=3.4
#grpc
grpcVersion=1.53.0
protobufVersion=3.21.12
gradleProtobufPluginVersion=0.9.3
grpcSpringBootStarter=3.5.5
grpcProtobufJavaFormatVersion=1.4
pythonVersion=3.9.7
accPyVersion=2023.06
accPyVersions=2021.12 2023.06
py4jVersion=0.10.9.7
#python
numpyVersion=1.15
wheelVersion=0.44.0
pysparkVersion=3.5.3
coverageVersion=6.5.0
pytestVersion=8.3.3
pythonDateutilVersion=2.9
pytzVersion=2024.2
pandasVersion=2.2.2
typingExtensionsVersion=4.11.0
venvPackVersion=0.2.0
cmwTracingVersion=0.6.1
stubGenJVersion=0.2.12
ponyStubsVersion=0.5.2
commonsLoggingVersion=1.3.4
jakartaServletApiVersion=4.0.4
javassistVersion=3.30.2-GA
jschVersion=0.1.55
#Python code validation (aligned with acc-py 2020.11)
preCommitVersion=2.21.0
preCommitHooksVersion=4.4.0
mypyVersion=1.3.0
flake8Version=5.0.4
ruffVersion=0.3.0
#pytimber
pytimberMajorVersion=4
requiredDocsPythonPackages=mkdocs==1.6.0 pymdown-extensions==10.8.1 pygments==2.18.0 mkdocs-macros-plugin==1.0.5 mkdocs-material==9.5.26 jinja2==3.1.4 markupsafe==2.1.5 linkchecker==10.4.0 acc-py-sphinx==0.12.2 typing_extensions==4.11.0
#CERTS
cernCASrc=https://cafiles.cern.ch/cafiles/certificates/CERN%20Grid%20Certification%20Authority(1).crt
cernCAName=CERN_CA.crt
systemProp.sonar.host.url=http://nxcals-jenkins-master.cern.ch:9000
#Sonar exclusions
systemProp.sonar.coverage.exclusions=**/backport/domain/**/*.java,**/backport/client/**/*.java,**/SparkContext.java,**/SparkPropertiesConfig.java,**/SchemaConstants.java,**/*Exception.java,**/config/*.java,**/common/web/*.java, **/cern/nxcals/**/Application.java,**/cern/nxcals/kafka/etl/config/*.java,**/monitoring*/**/domain/*.java,**/*Changelog*.java,**/pyquery/builders.py,**/extraction/data/builders.py
systemProp.sonar.exclusions=**/*Changelog.java,**/common/config/SparkContext.java,**/common/security/KerberosRelogin.java
systemProp.sonar.issue.ignore.multicriteria=e1
systemProp.sonar.issue.ignore.multicriteria.e1.ruleKey=python:S107
systemProp.sonar.issue.ignore.multicriteria.e1.resourceKey=python/extraction-api-python3/nxcals/api/extraction/data/base.py
# Image specific
jmxExporterAgentUrl=https://repo.maven.apache.org/maven2/io/prometheus/jmx/jmx_prometheus_javaagent/0.17.0/jmx_prometheus_javaagent-0.17.0.jar
jmxAgentFileName=jmx_prometheus_javaagent.jar
imageLibsPath=/app/libs
jibVersion=3.4.3
imageRepository=gitlab-registry.cern.ch/acc-logging-team/nxcals
baseImage=registry.cern.ch/acc/docker/acc-base-images/acc_el9_openjdk11:stable
