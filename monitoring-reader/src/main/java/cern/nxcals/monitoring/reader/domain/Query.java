package cern.nxcals.monitoring.reader.domain;

import lombok.Data;
import lombok.NonNull;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.time.Instant;
import java.util.function.Supplier;

/**
 * A given query (a dataset) created based on the configs.
 */
@Data
public class Query {
    @NonNull
    private final Instant startTime;
    @NonNull
    private final Instant endTime;
    @NonNull
    private final Supplier<Dataset<Row>> datasetSupplier;

    public Dataset<Row> getDataset() {
        return datasetSupplier.get();
    }
}
