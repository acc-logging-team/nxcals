package cern.nxcals.monitoring.reader.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.temporal.ChronoUnit;
import java.util.List;

/**
 * Created by jwozniak on 14/01/17.
 */
@Data
@NoArgsConstructor
public class CheckConfig {
    private List<String> fields;
    private long startTime;
    private long timeSlots;
    private ChronoUnit timeUnit;
    private String condition;
    private String debug;
    private String name;
}
