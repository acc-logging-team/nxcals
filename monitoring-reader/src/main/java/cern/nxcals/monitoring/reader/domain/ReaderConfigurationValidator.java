package cern.nxcals.monitoring.reader.domain;

import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Service
public class ReaderConfigurationValidator implements Validator {
    private static final String ERROR_CODE = "Absent check";
    private static final String ERROR_MESSAGE = "Entity %s - no such check: %s";

    @Override
    public boolean supports(Class<?> clazz) {
        return ReaderConfiguration.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ReaderConfiguration configuration = (ReaderConfiguration) target;
        for (EntityConfig entity : configuration.getEntities()) {
            for (String checkName : entity.getCheckRefs()) {
                if (!configuration.getChecks().containsKey(checkName)) {
                    errors.reject(ERROR_CODE, String.format(ERROR_MESSAGE, entity.getName(), checkName));
                }
            }
        }
    }
}
