/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.monitoring.reader.config;

import cern.nxcals.api.config.SparkContext;
import cern.nxcals.common.metrics.MetricsConfig;
import cern.nxcals.monitoring.reader.domain.ReaderConfiguration;
import cern.nxcals.monitoring.reader.domain.ReaderConfigurationValidator;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.validation.Validator;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@EnableConfigurationProperties(ReaderConfiguration.class)
@Configuration
@Import({ MetricsConfig.class, SparkContext.class })
public class SpringConfig {
    @Bean
    public static ConversionService conversionService() {
        return new DefaultConversionService();
    }

    @Bean
    public ScriptEngine scriptEngine() {
        return new ScriptEngineManager().getEngineByName("nashorn");
    }

    @Bean
    public ScheduledExecutorService scheduledExecutorService() {
        return Executors.newScheduledThreadPool(1);
    }

    @Bean
    public static Validator configurationPropertiesValidator() {
        return new ReaderConfigurationValidator();
    }

}
