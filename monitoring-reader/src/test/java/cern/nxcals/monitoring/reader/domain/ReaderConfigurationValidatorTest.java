package cern.nxcals.monitoring.reader.domain;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.validation.Errors;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ReaderConfigurationValidatorTest {

    @Mock
    private CheckConfig checkConfig;

    @Mock
    private EntityConfig entityConfig;

    @Test
    public void shouldPassOnCorrectConfig() {
        //given
        when(entityConfig.getCheckRefs()).thenReturn(Collections.singletonList("A"));

        ReaderConfiguration configuration = new ReaderConfiguration();
        configuration.setChecks(Collections.singletonMap("A", checkConfig));
        configuration.setEntities(Collections.singletonList(entityConfig));
        //when

        ReaderConfigurationValidator validator = new ReaderConfigurationValidator();
        Errors errors = mock(Errors.class);
        validator.validate(configuration, errors);

        verify(errors, never()).reject(anyString(), anyString());
    }

    @Test
    public void shouldFailOnWrongConfig() {
        //given

        when(entityConfig.getCheckRefs()).thenReturn(Collections.singletonList("A"));

        ReaderConfiguration configuration = new ReaderConfiguration();
        configuration.setChecks(Collections.singletonMap("B", checkConfig));
        configuration.setEntities(Collections.singletonList(entityConfig));
        //when

        ReaderConfigurationValidator validator = new ReaderConfigurationValidator();
        Errors errors = mock(Errors.class);
        validator.validate(configuration, errors);

        verify(errors, times(1)).reject(anyString(), anyString());
    }
}