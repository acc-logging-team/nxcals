package cern.nxcals.monitoring.reader.domain;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EntityConfigTest {

    @Test
    public void shouldGetName() {
        //given
        EntityConfig entityConfig = new EntityConfig();
        entityConfig.setSystem("system");
        Map<String, Object> entities = new HashMap<>();
        entities.put("entity", "value");
        entityConfig.setEntityKeys(entities);
        //when
        String name = entityConfig.getName();
        //then
        assertEquals("system_value",name);
    }
}