package cern.nxcals.monitoring.reader.service;

import cern.nxcals.common.metrics.MetricsRegistry;
import cern.nxcals.monitoring.reader.domain.CheckConfig;
import cern.nxcals.monitoring.reader.domain.EntityConfig;
import cern.nxcals.monitoring.reader.domain.Query;
import cern.nxcals.monitoring.reader.domain.ReaderConfiguration;
import com.google.common.collect.ImmutableMap;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.script.Bindings;
import javax.script.ScriptEngine;
import javax.script.ScriptException;
import java.util.Collections;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class QueryProcessorTest {
    @Mock
    private ReaderConfiguration configurationProvider;
    @Mock
    private EntityConfig entityConfig;
    @Mock
    private CheckConfig checkConfig;
    @Mock
    private QueryBuilder queryBuilder;
    @Mock
    private Query query;
    @Mock
    private MetricsRegistry metricsRegistry;
    @Mock
    private ScriptEngine scriptEngine;
    @Mock
    private Dataset<Row> dataset;
    @Mock
    private Bindings bindings;
    @Mock
    private ScheduledExecutorService scheduledExecutorService;
    private static final Long PERIOD = 10L;

    private void check(long datalosses, int exceptions) {
        verify(queryBuilder, atLeastOnce()).build(entityConfig, checkConfig);
        verify(metricsRegistry, atLeastOnce()).updateGaugeTo("entity_config_dataloss", datalosses);
        verify(metricsRegistry, atLeastOnce()).updateGaugeTo("entity_config_exceptions", exceptions);
        verify(metricsRegistry, atLeastOnce()).updateGaugeTo(eq("entity_config_execution_time"), anyLong());
    }

    private void waitForLatch(CountDownLatch latch) throws InterruptedException {
        latch.await(10, TimeUnit.SECONDS);
    }

    private CountDownLatch getCountDownLatch() {
        CountDownLatch latch = new CountDownLatch(3);
        doAnswer((invocation) -> {
            latch.countDown();
            return null;
        }).when(metricsRegistry).updateGaugeTo(anyString(), anyLong());
        return latch;
    }

    @Nested
    class QueryProcessorTestNested {

        @BeforeEach
        public void setup() throws ScriptException {
            when(configurationProvider.getPeriod()).thenReturn(PERIOD);
            when(configurationProvider.getPeriodTimeUnit()).thenReturn(TimeUnit.SECONDS);
            when(configurationProvider.getEntities()).thenReturn(Collections.singletonList(entityConfig));
            when(entityConfig.getCheckRefs()).thenReturn(Collections.singletonList("REF"));
            when(configurationProvider.getChecks()).thenReturn(ImmutableMap.of("REF", checkConfig));
            when(queryBuilder.build(entityConfig, checkConfig)).thenReturn(Collections.singletonList(query));
            when(query.getDataset()).thenReturn(dataset);
            when(scriptEngine.createBindings()).thenReturn(bindings);
            when(checkConfig.getCondition()).thenReturn("cond");
            when(scriptEngine.eval("cond", bindings)).thenReturn(Boolean.TRUE);
            when(entityConfig.getName()).thenReturn("entity");
            when(checkConfig.getName()).thenReturn("config");
        }

        @Test
        public void processOk() throws InterruptedException {
            //given
            ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
            QueryProcessor queryProcessor = new QueryProcessor(configurationProvider, queryBuilder, metricsRegistry, scriptEngine, executor);
            CountDownLatch latch = getCountDownLatch();

            //when
            queryProcessor.process();
            waitForLatch(latch);

            //then
            check(0L, 0);

        }

        @Test
        public void processWithDataLoss() throws Exception {
            //given
            when(scriptEngine.eval("cond", bindings)).thenReturn(Boolean.FALSE);
            ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
            QueryProcessor queryProcessor = new QueryProcessor(configurationProvider, queryBuilder, metricsRegistry, scriptEngine, executor);
            CountDownLatch latch = getCountDownLatch();

            //when
            queryProcessor.process();
            waitForLatch(latch);

            //then
            check(1L, 0);

        }

        @Test
        public void processScriptError() throws InterruptedException, ScriptException {
            //given
            ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
            QueryProcessor queryProcessor = new QueryProcessor(configurationProvider, queryBuilder, metricsRegistry, scriptEngine, executor);
            when(scriptEngine.eval("cond", bindings)).thenThrow(new RuntimeException("This is an intentional exception for testing!"));
            CountDownLatch latch = getCountDownLatch();

            //when
            queryProcessor.process();
            waitForLatch(latch);

            //then
            check(0L, 1);

        }

    }

    @Test
    public void shouldScheduleRun() {
        //given
        when(configurationProvider.getPeriod()).thenReturn(PERIOD);
        when(configurationProvider.getPeriodTimeUnit()).thenReturn(TimeUnit.SECONDS);

        QueryProcessor queryProcessor = new QueryProcessor(configurationProvider, queryBuilder, metricsRegistry, scriptEngine,
                scheduledExecutorService);
        when(configurationProvider.getPeriod()).thenReturn(PERIOD);
        when(configurationProvider.getPeriodTimeUnit()).thenReturn(TimeUnit.SECONDS);
        //when
        queryProcessor.process();

        //then
        verify(scheduledExecutorService).scheduleAtFixedRate(any(), eq(0L), eq(PERIOD), eq(TimeUnit.SECONDS));

    }

    @Test
    public void processError() throws InterruptedException {
        //given
        when(configurationProvider.getPeriod()).thenReturn(PERIOD);
        when(configurationProvider.getPeriodTimeUnit()).thenReturn(TimeUnit.SECONDS);
        when(configurationProvider.getEntities()).thenReturn(Collections.singletonList(entityConfig));
        when(entityConfig.getCheckRefs()).thenReturn(Collections.singletonList("REF"));
        when(configurationProvider.getChecks()).thenReturn(ImmutableMap.of("REF", checkConfig));
        when(queryBuilder.build(entityConfig, checkConfig)).thenReturn(Collections.singletonList(query));
        when(entityConfig.getName()).thenReturn("entity");
        when(checkConfig.getName()).thenReturn("config");

        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        QueryProcessor queryProcessor = new QueryProcessor(configurationProvider, queryBuilder, metricsRegistry, scriptEngine, executor);
        when(queryBuilder.build(entityConfig, checkConfig)).thenThrow(new RuntimeException());
        CountDownLatch latch = getCountDownLatch();

        //when
        queryProcessor.process();
        waitForLatch(latch);

        //then
        check(0L, 1);
    }

}
