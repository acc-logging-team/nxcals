package cern.nxcals.monitoring.reader.service;

import cern.nxcals.monitoring.reader.domain.CheckConfig;
import cern.nxcals.monitoring.reader.domain.EntityConfig;
import cern.nxcals.monitoring.reader.domain.Query;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.function.Supplier;

import static cern.nxcals.monitoring.reader.service.QueryBuilder.FORMATTER;
import static java.time.temporal.ChronoUnit.DAYS;
import static java.time.temporal.ChronoUnit.HOURS;
import static java.time.temporal.ChronoUnit.MINUTES;
import static java.time.temporal.ChronoUnit.SECONDS;
import static java.util.Collections.singletonMap;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

/**
 * Created by msobiesz on 05/09/17.
 */
public class QueryBuilderTest {
    private static final DateTimeFormatter FORMATTER_WITH_ZONE = FORMATTER.withZone(ZoneId.systemDefault());

    private QueryBuilder instance;
    private EntityConfig eConfig;

    @Mock
    private Clock clockMock;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        when(this.clockMock.getZone()).thenReturn(ZoneId.systemDefault());
        this.instance = spy(new QueryBuilder(mock(SparkSession.class), this.clockMock));
        Supplier<Dataset<Row>> datasetSupplier = () -> mock(Dataset.class);
        doReturn(datasetSupplier).when(instance).getDatasetSupplier(any(), any(), any());
        this.eConfig = getEntityConfig();
    }

    @Test
    public void shouldNotParseWrongExtractTime() {
        assertThrows(DateTimeParseException.class, () -> this.instance.setExtractSince("1970/11/11 12:00"));
    }

    @Test
    public void shouldParseCorrectExtractTime() {
        LocalDateTime now = LocalDateTime.now().truncatedTo(SECONDS);
        this.instance.setExtractSince(now.format(FORMATTER));
        LocalDateTime extractSince = this.instance.getExtractSince();
        assertEquals(now, extractSince);
    }

    @Test
    public void shouldExtractDataForMinutesButNotHours() {
        Instant now = Instant.ofEpochSecond(2 * 60 * 60);
        when(this.clockMock.instant()).thenReturn(now);
        this.instance.setExtractSince(FORMATTER_WITH_ZONE.format(now.minus(30, MINUTES).plusSeconds(2)));

        CheckConfig cConfig = checkConfigMock(60, 60, MINUTES);;
        List<Query> data = extract(cConfig);

        assertSize(data, 29);

        cConfig = checkConfigMock(1, 2, HOURS);
        data = extract(cConfig);
        assertEmpty(data);

        cConfig = checkConfigMock(1, 2, DAYS);
        data = extract(cConfig);
        assertEmpty(data);
    }

    @Test
    public void shouldExtractDataForMinutesAndHoursButNotDays() {
        Instant now = Instant.ofEpochSecond(5 * 60 * 60);
        when(this.clockMock.instant()).thenReturn(now);
        this.instance.setExtractSince(FORMATTER_WITH_ZONE.format(now.minus(2, HOURS).plusSeconds(2)));

        CheckConfig cConfig = checkConfigMock(2 * 60, 3 * 60, MINUTES);
        List<Query> data = extract(cConfig);

        assertSize(data, 59);

        cConfig = checkConfigMock(2, 3, HOURS);
        data = extract(cConfig);
        assertEmpty(data);

        cConfig = checkConfigMock(1, 2, DAYS);
        data = extract(cConfig);
        assertEmpty(data);
    }

    @Test
    public void shouldValidateWithRespectToCurrentTimeUnit() {
        Instant now = Instant.ofEpochSecond(5 * 24 * 60 * 60 + 10 * 60 * 60);
        when(this.clockMock.instant()).thenReturn(now);

        CheckConfig cConfig = checkConfigMock(4, 5, DAYS);
        List<Query> data = extract(cConfig);

        // the same code above
        assertEquals(4, data.size());
        assertEquals(1, data.get(0).getStartTime().atOffset(ZoneOffset.UTC).getDayOfMonth());
        assertEquals(2, data.get(1).getStartTime().atOffset(ZoneOffset.UTC).getDayOfMonth());
        assertEquals(3, data.get(2).getStartTime().atOffset(ZoneOffset.UTC).getDayOfMonth());
        assertEquals(4, data.get(3).getStartTime().atOffset(ZoneOffset.UTC).getDayOfMonth());
    }


    @Test
    public void shouldExtractDataForMinutesAndHoursAndDays() {
        Instant now = Instant.ofEpochSecond(5 * 24 * 60 * 60);
        when(this.clockMock.instant()).thenReturn(now);
        this.instance.setExtractSince(FORMATTER_WITH_ZONE.format(now.minus(2, DAYS).plusSeconds(2)));

        CheckConfig cConfig = checkConfigMock(2 * 24 * 60L, 3 * 24 * 60L, MINUTES);
        List<Query> data = extract(cConfig);;

        assertSize(data, 24 * 60 - 1);

        cConfig = checkConfigMock(2 * 24, 3 * 24, HOURS);
        data = extract(cConfig);
        assertSize(data, 23);

        cConfig = checkConfigMock(2, 3, DAYS);
        data = extract(cConfig);
        assertSize(data, 0);
    }

    @Test
    public void shouldNotExtractDataForMinutesAndHoursAndDays() {
        Instant now = Instant.ofEpochSecond(5 * 24 * 60 * 60);
        when(this.clockMock.instant()).thenReturn(now);
        this.instance.setExtractSince(FORMATTER_WITH_ZONE.format(now));

        CheckConfig cConfig = checkConfigMock(2 * 24 * 60L, 3 * 24 * 60L, MINUTES);
        List<Query> data = extract(cConfig);

        assertEmpty(data);

        cConfig = checkConfigMock(2 * 24, 3 * 24, HOURS);
        data = extract(cConfig);
        assertEmpty(data);

        cConfig = checkConfigMock(2, 3, DAYS);
        data = extract(cConfig);
        assertEmpty(data);
    }

    private EntityConfig getEntityConfig() {
        EntityConfig eConfig = mock(EntityConfig.class);
        when(eConfig.getSystem()).thenReturn("CMW");
        when(eConfig.getEntityKeys()).thenReturn(singletonMap("a", "b"));
        return eConfig;
    }

    private CheckConfig checkConfigMock(long timeSlots, long startTime, ChronoUnit unit) {
        CheckConfig cConfig = mock(CheckConfig.class);
        when(cConfig.getStartTime()).thenReturn(startTime);
        when(cConfig.getTimeUnit()).thenReturn(unit);
        when(cConfig.getTimeSlots()).thenReturn(timeSlots);
        return cConfig;
    }

    private void assertEmpty(List<Query> data) {
        assertNotNull(data);
        assertTrue(data.isEmpty());
    }

    private void assertSize(List<Query> data, long size) {
        assertNotNull(data);
        assertEquals(data.size(), size);
    }

    private List<Query> extract(CheckConfig cConfig) {
        return this.instance.build(eConfig, cConfig);
    }
}
