package cern.nxcals.monitoring.reader;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

/**
 * Created by jwozniak on 09/01/17.
 */
public class TimeTesting {

    public static void main(String[] args) {
        Instant timeBase = Instant.now().truncatedTo(ChronoUnit.MINUTES);

        System.out.println(timeBase);
        System.out.println(Instant.now());
    }

}
