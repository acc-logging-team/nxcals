/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.monitoring.reader;

import cern.nxcals.monitoring.reader.service.QueryProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class ExtractorApplicationDev {

    private static String USER = System.getProperty("user.name");

    static {
        System.setProperty("nxcals.reader.metrics.domain", "nxcals_monitoring");
        //System.setProperty("nxcals.reader.parameters", "NXCALS_LHC_DEV1/Logging, NXCALS_SPS_DEV1/Logging, NXCALS_CPS_DEV1/Logging, NXCALS_NONE_DEV1/Logging");
        System.setProperty("org.apache.logging.log4j.simplelog.StatusLogger.level", "TRACE");
        System.setProperty("logging.config", "classpath:monitoring-reader-log4j2.yml");
        System.setProperty("service.url", "http://nxcals-" + USER + "1.cern.ch:19093");
//        System.setProperty("kerberos.principal", USER);
//        System.setProperty("kerberos.keytab", "/opt/" + USER + "/.keytab");
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(ExtractorApplicationDev.class);

    public static void main(String[] args) throws Exception {
        try {

            ApplicationContext ctx = SpringApplication.run(ExtractorApplicationDev.class);

            QueryProcessor queryProcessor = ctx.getBean(QueryProcessor.class);
            queryProcessor.process();

        } catch (Throwable e) {
            e.printStackTrace();
            LOGGER.error("Exception while running publisher app", e);
            System.exit(1);
        }
    }
}
