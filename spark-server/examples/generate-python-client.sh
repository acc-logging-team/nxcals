#!/usr/bin/env bash

# must have pip install grpcio and pip install grpcio-tools
python -m grpc_tools.protoc -I../../common/src/main/proto/cern/nxcals/api/extraction/thin --python_out=. --grpc_python_out=. ../../common/src/main/proto/cern/nxcals/api/extraction/thin/extraction-service.proto
