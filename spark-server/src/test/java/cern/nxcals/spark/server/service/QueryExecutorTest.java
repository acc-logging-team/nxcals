package cern.nxcals.spark.server.service;

import cern.nxcals.api.extraction.data.Avro;
import cern.nxcals.api.extraction.thin.AvroData;
import cern.nxcals.api.extraction.thin.AvroQuery;
import com.google.common.collect.Lists;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericRecord;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class QueryExecutorTest {

    @Mock
    private ScriptExecutor scriptExecutor;
    private Dataset<Row> dataSet;
    private final SparkSession sparkSession = SparkSession.builder()
            .master("local")
            .appName("QueryExecutorTest")
            .getOrCreate();

    @BeforeEach
    public void setUp() throws Exception {
        dataSet = createDataset();
    }

    private Dataset<Row> createDataset() {
        StructType structType = new StructType(new StructField[] {new StructField("f1", DataTypes.LongType ,true,  Metadata.empty()),
                new StructField("f2", DataTypes.LongType ,true, Metadata.empty())});

        List<Row> list = Lists.newArrayList(RowFactory.create(1L, 2L), RowFactory.create(3L, 4L));
        return sparkSession.createDataFrame(list, structType);
    }
    @Test
    public void testQuery() {
        //given
        QueryExecutor queryExecutor = new QueryExecutor(scriptExecutor);
        when(scriptExecutor.executeForDataset("test")).thenReturn(dataSet);

        //when
        AvroData avroResponse = queryExecutor.query(AvroQuery.newBuilder().setScript("test").build());

        //then
        List<GenericRecord> records = Avro.records(avroResponse);

        assertEquals(2, records.size());
        assertEquals(1L, records.get(0).get("f1"));
        assertEquals(2L, records.get(0).get("f2"));
        assertEquals(3L, records.get(1).get("f1"));
        assertEquals(4L, records.get(1).get("f2"));

        Schema schema = Avro.schema(avroResponse);
        assertNotNull(schema.getField("f1"));
        assertNotNull(schema.getField("f2"));

        assertEquals(2, avroResponse.getRecordCount());
    }
}