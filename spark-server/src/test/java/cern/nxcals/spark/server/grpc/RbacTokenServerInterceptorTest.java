package cern.nxcals.spark.server.grpc;

import cern.nxcals.common.grpc.RbacTokenInterceptor;
import cern.rbac.common.RbaToken;
import io.grpc.Metadata;
import io.grpc.ServerCall;
import io.grpc.ServerCallHandler;
import io.grpc.StatusRuntimeException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class RbacTokenServerInterceptorTest {

    @Mock
    private ServerCall<Object, Object> call;
    @Mock
    private ServerCallHandler<Object, Object> next;

    @Test
    public void shouldSetThreadLocalToken() {
        //given
        RbacTokenServerInterceptor interceptor = new RbacTokenServerInterceptor();
        interceptor.setValidateToken(false);
        Metadata headers = new Metadata();
        headers.put(RbacTokenInterceptor.TOKEN_KEY, RbaToken.EMPTY_TOKEN.getEncoded());
        when(next.startCall(argThat((v)->true),argThat((v)->true))).thenAnswer((invocation)->{
            assertNotNull(RbacTokenServerInterceptor.AUTH_TOKEN.get());
            return null;
        });

        //when
        interceptor.interceptCall(call, headers, next);
        //then checked in the answer (within the context of the call)

        //this one should be null, outside of the call context
        assertNull(RbacTokenServerInterceptor.AUTH_TOKEN.get());
    }

    @Test
    public void shouldThrowOnInvalidToken() {
        //given interceptor with token validation
        RbacTokenServerInterceptor interceptor = new RbacTokenServerInterceptor();

        Metadata headers = new Metadata();
        headers.put(RbacTokenInterceptor.TOKEN_KEY, RbaToken.EMPTY_TOKEN.getEncoded());
        //when
        assertThrows(StatusRuntimeException.class, () -> interceptor.interceptCall(call, headers, next));

        //then exception

    }

    @Test
    public void shouldThrowOnAbsentToken() {
        //given interceptor with token validation
        RbacTokenServerInterceptor interceptor = new RbacTokenServerInterceptor();

        Metadata headers = new Metadata();

        //when
        assertThrows(StatusRuntimeException.class, () -> interceptor.interceptCall(call, headers, next));

        //then exception

    }
}
