package cern.nxcals.spark.server.grpc;

import cern.nxcals.api.extraction.thin.AvroData;
import cern.nxcals.api.extraction.thin.AvroQuery;
import cern.nxcals.spark.server.service.QueryExecutor;
import cern.rbac.common.RbaToken;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.argThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ExtractionServiceGrpcControllerTest {
    @Mock
    private QueryExecutor queryExecutor;
    @Mock
    private StreamObserver<AvroData> observer;

    @Test
    public void shouldExecuteForAvro() {
        //given
        ExtractionServiceGrpcController controller = getExtractionServiceGrpcController();
        AvroQuery avroRequest = AvroQuery.newBuilder().build();
        AvroData avroResponse = AvroData.newBuilder().build();
        when(queryExecutor.query(avroRequest)).thenReturn(avroResponse);

        //when
        controller.query(avroRequest, observer);

        //then
        verify(observer, times(1)).onNext(avroResponse);
        verify(observer, times(1)).onCompleted();
        verify(observer, times(0)).onError(argThat(e -> true));

    }

    private ExtractionServiceGrpcController getExtractionServiceGrpcController() {
        ExtractionServiceGrpcController controller = new ExtractionServiceGrpcController(queryExecutor);
        controller.setTokenSupplier(() -> RbaToken.EMPTY_TOKEN);
        return controller;
    }

    @Test
    public void shouldExecuteForAvroAndReturnInvalidArgumentStatus() {
        //given
        ExtractionServiceGrpcController controller = getExtractionServiceGrpcController();
        AvroQuery avroRequest = AvroQuery.newBuilder().build();
        AvroData avroResponse = AvroData.newBuilder().build();
        IllegalArgumentException exception = new IllegalArgumentException("test");
        when(queryExecutor.query(avroRequest)).thenThrow(exception);

        //when
        controller.query(avroRequest, observer);

        //then
        verify(observer, times(0)).onNext(avroResponse);
        verify(observer, times(0)).onCompleted();

        ArgumentCaptor<Throwable> captor = ArgumentCaptor.forClass(Throwable.class);
        verify(observer, times(1)).onError(captor.capture());
        assertEquals(exception, ((StatusRuntimeException)captor.getValue()).getStatus().getCause());
        assertEquals(exception.getMessage(), ((StatusRuntimeException)captor.getValue()).getStatus().getDescription());
        assertEquals(Status.Code.INVALID_ARGUMENT, ((StatusRuntimeException)captor.getValue()).getStatus().getCode());

    }

    @Test
    public void shouldExecuteForAvroAndReturnInternalErrorStatus() {
        //given
        ExtractionServiceGrpcController controller = getExtractionServiceGrpcController();
        AvroQuery avroRequest = AvroQuery.newBuilder().build();
        AvroData avroResponse = AvroData.newBuilder().build();
        RuntimeException exception = new RuntimeException("test");
        when(queryExecutor.query(avroRequest)).thenThrow(exception);

        //when
        controller.query(avroRequest, observer);

        //then
        verify(observer, times(0)).onNext(avroResponse);
        verify(observer, times(0)).onCompleted();

        ArgumentCaptor<Throwable> captor = ArgumentCaptor.forClass(Throwable.class);
        verify(observer, times(1)).onError(captor.capture());
        assertEquals(exception, ((StatusRuntimeException)captor.getValue()).getStatus().getCause());
        assertEquals(exception.getMessage(), ((StatusRuntimeException)captor.getValue()).getStatus().getDescription());
        assertEquals(Status.Code.INTERNAL, ((StatusRuntimeException)captor.getValue()).getStatus().getCode());

    }
}