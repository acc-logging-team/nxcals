package cern.nxcals.spark.server.service;

import cern.nxcals.api.custom.domain.BeamMode;
import cern.nxcals.api.custom.domain.Fill;
import cern.nxcals.api.custom.service.FillService;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.extraction.thin.FillData;
import cern.nxcals.api.utils.TimeUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class FillExecutorTest {
    @Mock
    private FillService fillService;
    private FillExecutor fillExecutor;

    private static final int FILL_NUMBER = 100;

    @BeforeEach
    public void setUp()  {
        fillExecutor = new FillExecutor(fillService);
    }

    private Optional<Fill> createFill() {
        BeamMode beamMode = new BeamMode(TimeWindow.between(10, 20), "BMODE");
        List<BeamMode> beamModes = new ArrayList<>();
        beamModes.add(beamMode);

        return  Optional.of(new Fill(FILL_NUMBER, TimeWindow.between(0, 100), beamModes));

    }

    @Test
    public void testFindFill() {
        //given
        when(fillService.findFill(FILL_NUMBER)).thenReturn(createFill());
        //when
        FillData response = fillExecutor.findFill(FILL_NUMBER);
        //then
        assertEquals(FILL_NUMBER, response.getNumber());
    }

    @Test
    public void testGetLastCompleted() {
        //given
        when(fillService.getLastCompleted()).thenReturn(createFill());
        //when
        FillData response = fillExecutor.getLastCompleted();
        //then
        assertEquals(FILL_NUMBER, response.getNumber());
    }

    @Test
    public void testFindFills() {
        //given
        List<Fill> fills = new ArrayList<>();
        fills.add(createFill().orElseThrow(RuntimeException::new));

        when(fillService.findFills(TimeUtils.getInstantFromNanos(0), TimeUtils.getInstantFromNanos(100)))
                .thenReturn(fills);
        //when
        List<FillData> response = fillExecutor.findFills(cern.nxcals.api.extraction.thin.TimeWindow.newBuilder()
                .setStartTime(0).setEndTime(100).build());
        //then
        assertEquals(1, response.size());
    }
}