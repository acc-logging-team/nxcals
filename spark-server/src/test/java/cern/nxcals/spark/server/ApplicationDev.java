/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.spark.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class ApplicationDev {

    private static SpringApplication app;

    private static final String USER = System.getProperty("user.name");

    static {

        System.setProperty("org.apache.logging.log4j.simplelog.StatusLogger.level", "TRACE");
        System.setProperty("logging.config", "classpath:spark-server-log4j2.yml");
        //        System.setProperty("service.url", "http://nxcals-" + USER + "1.cern.ch:19093");

        //        System.setProperty("service.url",
        //                "https://cs-ccr-nxcals6.cern.ch:19093,https://cs-ccr-nxcals7.cern.ch:19093,https://cs-ccr-nxcals8.cern.ch:19093");
        System.setProperty("service.url",
                "https://cs-ccr-testbed2.cern.ch:19093,https://cs-ccr-testbed3.cern.ch:19093,https://cs-ccr-nxcalstbs1.cern.ch:19093");
        //        System.setProperty("java.security.krb5.conf", "krb5.conf.local");
        //        System.setProperty("java.security.krb5.realm", "CERN.CH");
        //        System.setProperty("java.security.krb5.kdc", "cerndc.cern.ch:88");
        System.setProperty("kerberos.principal", USER);
        System.setProperty("kerberos.keytab", "/opt/" + USER + "/.keytab");
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationDev.class);

    public static void main(String[] args) throws Exception {
        try {
            app = new SpringApplication(ApplicationDev.class);
            app.addListeners(new ApplicationPidFileWriter());
            ConfigurableApplicationContext ctx = app.run(args);

        } catch (Throwable e) {
            e.printStackTrace();
            LOGGER.error("Exception while running publisher app", e);
            System.exit(1);
        }
    }
}
