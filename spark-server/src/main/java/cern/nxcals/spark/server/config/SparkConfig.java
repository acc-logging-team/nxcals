/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.spark.server.config;

import cern.nxcals.api.config.AuthenticationContext;
import cern.nxcals.api.config.SparkProperties;
import cern.nxcals.api.utils.SparkUtils;
import cern.nxcals.common.config.SparkSessionModifier;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.SparkSession;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;

import java.util.List;

@Configuration
@Import(AuthenticationContext.class)
public class SparkConfig {

    @Bean
    public SparkConf createSparkConf(SparkProperties config) {
        return SparkUtils.createSparkConf(config);
    }

    @Bean
    @DependsOn("kerberos")
    @Lazy
    @Profile("lazy-spark-session")
    public SparkSession lazySparkSession(SparkConf conf, List<SparkSessionModifier> modifiers) {
        return createSparkSession(conf, modifiers);
    }

    @Bean
    @DependsOn("kerberos")
    @Profile("!lazy-spark-session")
    public SparkSession sparkSession(SparkConf conf, List<SparkSessionModifier> modifiers) {
        return createSparkSession(conf, modifiers);
    }

    private SparkSession createSparkSession(SparkConf conf, List<SparkSessionModifier> modifiers) {
        SparkSession session = SparkUtils.createSparkSession(conf);
        SparkUtils.modifySparkSession(session, modifiers);
        return session;
    }

    @Bean
    @ConfigurationProperties(prefix = "spark")
    public SparkProperties createSparkPropertiesConfig() {
        return new SparkProperties();
    }
}
