package cern.nxcals.spark.server.service;

import cern.nxcals.api.extraction.thin.AvroData;
import cern.nxcals.api.extraction.thin.AvroQuery;
import cern.nxcals.common.avro.DefaultGenericRecordToBytesEncoder;
import cern.nxcals.common.utils.AvroUtils;
import cern.nxcals.common.utils.IllegalCharacterConverter;
import com.google.protobuf.ByteString;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.avro.Schema;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.avro.AvroSerializer;
import org.apache.spark.sql.avro.SchemaConverters;
import org.apache.spark.sql.catalyst.InternalRow;
import org.apache.spark.sql.catalyst.encoders.ExpressionEncoder;
import org.apache.spark.sql.types.StructType;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class QueryExecutor {
    private final ScriptExecutor scriptExecutor;

    public AvroData query(AvroQuery request) {
        Dataset<Row> rowDataset = convertIllegalFields(scriptExecutor.executeForDataset(request.getScript()));
        return toAvroData(request, rowDataset);
    }

    private Dataset<Row> convertIllegalFields(Dataset<Row> inputDataset) {
        IllegalCharacterConverter converter = IllegalCharacterConverter.get();

        Column[] columns = Arrays.stream(inputDataset.columns()).map(name ->
                IllegalCharacterConverter.isLegal(name) ? inputDataset.col(name) :
                        inputDataset.col(name).alias(converter.convertToLegal(name))).toArray(Column[]::new);

        return inputDataset.select(columns);
    }

    private AvroData toAvroData(AvroQuery request, Dataset<Row> rowDataset) {
        try (ByteString.Output outputStream = ByteString.newOutput()) {
            StructType structType = rowDataset.schema();
            Schema schema = SchemaConverters.toAvroType(structType, false, "nxcals_record", "nxcals");
            AvroSerializer avroSerializer = new AvroSerializer(structType, schema, false);
            ExpressionEncoder<Row> encoder = ExpressionEncoder.apply(structType);
            String codec = getCodec(request);
            DataFileWriter<Object> writer = createWriter(schema, outputStream, codec);
            List<Row> rowList = rowDataset.collectAsList();

            encode(avroSerializer, encoder, writer, rowList);

            return AvroData.newBuilder().setAvroSchema(schema.toString())
                    .setAvroBytes(outputStream.toByteString()).setRecordCount(rowList.size()).build();
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }

    private String getCodec(AvroQuery request) {
        return request.getCompression().toString().toLowerCase();
    }

    private void encode(AvroSerializer avroSerializer, ExpressionEncoder<Row> enconder, DataFileWriter<Object> writer,
            List<Row> rowList) {
        ExpressionEncoder.Serializer<Row> serializer = enconder.createSerializer();
        rowList.forEach(row -> {
            InternalRow internalRow = serializer.apply(row);
            GenericRecord genericRecord = (GenericRecord) avroSerializer.serialize(internalRow);
            byte[] bytes = DefaultGenericRecordToBytesEncoder.convertToBytes(genericRecord);
            append(writer, bytes);
        });
        close(writer);
    }


    private void close(DataFileWriter<Object> writer) {
        try {
            writer.close();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private void append(DataFileWriter<Object> writer, byte[] bytes) {
        try {
            writer.appendEncoded(ByteBuffer.wrap(bytes));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private DataFileWriter<Object> createWriter(Schema schema, OutputStream outputStream, String codec) {
        try {
            return AvroUtils.createDataFileWriter(schema, outputStream, codec, 1024);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

}
