package cern.nxcals.spark.server.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Created by jwozniak on 14/01/17.
 */
@ConfigurationProperties("nxcals.spark.server")
@Data
@NoArgsConstructor
public class SparkServerConfiguration {
    private int maxTasks = 10;
}
