/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.spark.server;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableAsync
@EnableScheduling
@SpringBootApplication
@Slf4j
public class Application {
    public static void main(String[] args) {
        try {
            SpringApplication app = new SpringApplication(Application.class);
            app.addListeners(new ApplicationPidFileWriter());
            app.run();
        } catch (Exception ex) {
            log.error("Exception while starting Spark Server", ex);
            System.exit(1);
        }
    }
}
