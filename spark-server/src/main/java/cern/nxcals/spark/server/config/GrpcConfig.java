package cern.nxcals.spark.server.config;

import cern.nxcals.spark.server.grpc.RbacTokenServerInterceptor;
import io.grpc.ServerInterceptor;
import org.lognet.springboot.grpc.GRpcGlobalInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class GrpcConfig {
    @Bean
    @GRpcGlobalInterceptor
    public ServerInterceptor rbacTokenServerInterceptor() {
        return new RbacTokenServerInterceptor();
    }

}
