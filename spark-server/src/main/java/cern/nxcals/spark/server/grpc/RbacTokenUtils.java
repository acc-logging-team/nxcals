package cern.nxcals.spark.server.grpc;

import cern.rbac.common.RbaToken;
import cern.rbac.util.holder.MiddleTierRbaTokenHolder;
import lombok.experimental.UtilityClass;

import java.util.function.Supplier;

@UtilityClass
class RbacTokenUtils {
    static void setRbaToken(Supplier<RbaToken> tokenSupplier) {
        RbaToken rbaToken = tokenSupplier.get();
        if (rbaToken != null) {
            MiddleTierRbaTokenHolder.set(rbaToken);
        } else {
            throw new IllegalArgumentException("No RBAC token passed");
        }
    }

    static void clearRbaToken() {
        MiddleTierRbaTokenHolder.clear();
    }
}
