package cern.nxcals.spark.server.service;

import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.extraction.data.builders.DevicePropertyDataQuery;
import cern.nxcals.api.utils.TimeUtils;
import com.google.common.annotations.VisibleForTesting;
import lombok.AccessLevel;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import javax.script.Bindings;
import javax.script.ScriptEngine;
import java.util.Set;
import java.util.stream.Stream;

/**
 * Processor executing queries.
 */
@Service
@Slf4j
@SuppressWarnings("squid:S1181") // catching throwable
public class ScriptExecutor {

    private static final String PREFIX = Stream.of(DataQuery.class, DevicePropertyDataQuery.class, TimeUtils.class,
            Set.class).map(clazz -> "var " + clazz.getSimpleName() + " = Java.type('" + clazz.getName() + "');\n"
    ).reduce("", String::concat);
    private final ScriptEngine engine;
    private final SparkSession sparkSession;

    @Autowired
    public ScriptExecutor(ScriptEngine engine, @Lazy SparkSession sparkSession) {
        this.engine = engine;
        this.sparkSession = sparkSession;
    }

    @VisibleForTesting
    @Setter(AccessLevel.PACKAGE)
    private Runnable errorHandler = () -> System.exit(-1);

    public Dataset<Row> executeForDataset(String script) {
        if (sparkSession.sparkContext().isStopped()) {
            log.error("Spark session is stopped, exiting...");
            errorHandler.run();
            throw new IllegalStateException("Spark session is stopped");
        }

        try {
            String toExecute = PREFIX + script;
            Bindings bindings = engine.createBindings();
            bindings.put("sparkSession", sparkSession);
            Object eval = engine.eval(toExecute, bindings);
            if (eval instanceof Dataset) {
                return (Dataset<Row>) eval;
            } else {
                throw new IllegalArgumentException("A script must return a Spark Dataset<Row> [" + script + "]");
            }
        } catch (Throwable e) {
            //Makes sense to catch Throwable, we are executing an externally provided script here
            log.error("Failed to script {}", script, e);
            throw new IllegalArgumentException(e);
        }
    }

}
