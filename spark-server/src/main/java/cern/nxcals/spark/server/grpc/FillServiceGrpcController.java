package cern.nxcals.spark.server.grpc;

import cern.nxcals.api.extraction.thin.FillData;
import cern.nxcals.api.extraction.thin.FillQueryByNumber;
import cern.nxcals.api.extraction.thin.FillQueryByWindow;
import cern.nxcals.api.extraction.thin.FillServiceGrpc;
import cern.nxcals.spark.server.service.FillExecutor;
import cern.rbac.common.RbaToken;
import com.google.common.annotations.VisibleForTesting;
import com.google.protobuf.Empty;
import io.grpc.stub.StreamObserver;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.lognet.springboot.grpc.GRpcService;

import java.util.function.Supplier;

@RequiredArgsConstructor
@GRpcService
@Slf4j
public class FillServiceGrpcController extends FillServiceGrpc.FillServiceImplBase {
    private final FillExecutor fillExecutor;

    @VisibleForTesting
    @Setter(AccessLevel.PACKAGE)
    Supplier<RbaToken> tokenSupplier = RbacTokenServerInterceptor.AUTH_TOKEN::get;

    @Override
    public void findFill(FillQueryByNumber request, StreamObserver<FillData> responseObserver) {
        log.debug("Start client findFill request={}", request);
        GrpcControllerProcessor.process(() -> fillExecutor.findFill(request.getFillNr()), responseObserver, tokenSupplier);
        log.debug("Finished client findFill request={}", request);
    }

    @Override
    public void findFills(FillQueryByWindow request, StreamObserver<FillData> responseObserver) {
        log.debug("Start client findFill request={}", request);
        GrpcControllerProcessor.processList(() -> fillExecutor.findFills(request.getTimeWindow()), responseObserver, tokenSupplier);
        log.debug("Finished client findFill request={}", request);
    }

    @Override
    public void getLastCompleted(Empty request, StreamObserver<FillData> responseObserver) {
        log.debug("Start client getLastCompleted request={}", request);
        GrpcControllerProcessor.process(fillExecutor::getLastCompleted, responseObserver, tokenSupplier);
        log.debug("Finished client getLastCompleted request={}", request);
    }
}
