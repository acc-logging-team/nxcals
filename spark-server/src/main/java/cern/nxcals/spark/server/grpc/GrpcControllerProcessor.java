package cern.nxcals.spark.server.grpc;

import cern.rbac.common.RbaToken;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.function.Supplier;

@Slf4j
@UtilityClass
public class GrpcControllerProcessor {
    public static  <T> void process(Supplier<T> supplier, StreamObserver<T> responseObserver, Supplier<RbaToken> tokenSupplier) {
        try {
            RbacTokenUtils.setRbaToken(tokenSupplier);

            T response = supplier.get();
            responseObserver.onNext(response);
            responseObserver.onCompleted();

        } catch (IllegalArgumentException ex) {
            log.warn("Illegal argument passed", ex);
            responseObserver.onError(
                    Status.INVALID_ARGUMENT.withCause(ex).withDescription(ex.getMessage()).asRuntimeException());
        } catch (Exception ex) {
            log.error("Internal exception", ex);
            responseObserver
                    .onError(Status.INTERNAL.withCause(ex).withDescription(ex.getMessage()).asRuntimeException());
        } catch (Error error) {
            log.error("Internal error", error);
            responseObserver
                    .onError(Status.INTERNAL.withCause(error).withDescription(error.getMessage()).asRuntimeException());
        } finally {
            RbacTokenUtils.clearRbaToken();
        }
    }

    public static  <T> void processList(Supplier<List<T>> supplier, StreamObserver<T> responseObserver, Supplier<RbaToken> tokenSupplier) {
        try {
            RbacTokenUtils.setRbaToken(tokenSupplier);

            List<T> response = supplier.get();
            response.forEach(responseObserver::onNext);

            responseObserver.onCompleted();

        } catch (IllegalArgumentException ex) {
            log.warn("Illegal argument passed", ex);
            responseObserver.onError(
                    Status.INVALID_ARGUMENT.withCause(ex).withDescription(ex.getMessage()).asRuntimeException());
        } catch (Exception ex) {
            log.error("Internal error", ex);
            responseObserver
                    .onError(Status.INTERNAL.withCause(ex).withDescription(ex.getMessage()).asRuntimeException());
        } finally {
            RbacTokenUtils.clearRbaToken();
        }
    }
}
