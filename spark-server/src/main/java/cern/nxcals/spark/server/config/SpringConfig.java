/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.spark.server.config;

import cern.nxcals.api.config.AuthenticationContext;
import cern.nxcals.api.custom.service.FillService;
import cern.nxcals.api.custom.service.Services;
import cern.nxcals.common.metrics.MetricsConfig;
import cern.nxcals.spark.server.domain.SparkServerConfiguration;
import org.apache.spark.sql.SparkSession;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.support.DefaultConversionService;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@EnableConfigurationProperties(SparkServerConfiguration.class)
@Configuration
@Import({ MetricsConfig.class, AuthenticationContext.class })
public class SpringConfig {

    @Bean
    public FillService fillService(SparkSession session) { return Services.newInstance(session).fillService(); }

    @Bean
    public static ConversionService conversionService() {
        return new DefaultConversionService();
    }

    @Bean
    public ScriptEngine scriptEngine() {
        return new ScriptEngineManager().getEngineByName("nashorn");
    }

    @Bean
    public ScheduledExecutorService scheduledExecutorService() {
        return Executors.newScheduledThreadPool(1);
    }



}
