delete from roles_permissions;
delete from users_roles;
delete from users;
delete from roles;
delete from permissions;
delete from realms;

insert into realms(ID, REALM_NAME) values (1, 'CERN.CH');

insert into users (ID, REALM_ID, USER_NAME) values (1, 1, 'acclog');


insert into roles (ID, ROLE_NAME) values (1, 'ROLE_ADMIN');


insert into PERMISSIONS (ID, PERMISSION_NAME) values (1, 'TEST-CMW:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (2, 'TEST-CMW:WRITE');

insert into PERMISSIONS (ID, PERMISSION_NAME) values (3, 'MOCK-SYSTEM:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (4, 'MOCK-SYSTEM:WRITE');

insert into PERMISSIONS (ID, PERMISSION_NAME) values (5, 'CMW:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (6, 'CMW:WRITE');

insert into PERMISSIONS (ID, PERMISSION_NAME) values (7, 'PM:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (8, 'PM:WRITE');

insert into PERMISSIONS (ID, PERMISSION_NAME) values (9, 'VARIABLE:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (10, 'VARIABLE:WRITE');

insert into PERMISSIONS (ID, PERMISSION_NAME) values (11, 'HIERARCHY:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (12, 'HIERARCHY:WRITE');

insert into PERMISSIONS (ID, PERMISSION_NAME) values (13, 'GROUP:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (14, 'GROUP:WRITE');


insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID)
select 1, id from permissions;


insert into USERS_ROLES (USER_ID, ROLE_ID) VALUES (1, 1);

