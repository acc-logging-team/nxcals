-- run this against your db.

delete from roles_permissions;
delete from users_roles;
delete from users;
delete from roles;
delete from permissions;
delete from realms;


insert into realms(ID, REALM_NAME) values (1, 'CERN.CH');
insert into realms(ID, REALM_NAME) values (2, 'OTHER.CH');

insert into users (ID, REALM_ID, USER_NAME) values (3, 1,'cmw-user');
insert into users (ID, REALM_ID, USER_NAME) values (4, 1,'mock-system-user');
insert into users (ID, REALM_ID, USER_NAME) values (5, 1,'test-cmw-user');
insert into users (ID, REALM_ID, USER_NAME) values (6, 1,'roleless-user');
insert into users (ID, REALM_ID, USER_NAME) values (7, 1,'permissionless-user');
insert into users (ID, REALM_ID, USER_NAME) values (8, 1,'reader-user');
insert into users (ID, REALM_ID, USER_NAME) values (9, 1,'admin');
insert into users (ID, REALM_ID, USER_NAME) values (10, 1,'acclog');
insert into users (ID, REALM_ID, USER_NAME) values (11, 1,'gavgitid');
insert into users (ID, REALM_ID, USER_NAME) values (12, 1,'jwozniak');
insert into users (ID, REALM_ID, USER_NAME) values (13, 1,'mamajews');
insert into users (ID, REALM_ID, USER_NAME) values (14, 1,'msobiesz');
insert into users (ID, REALM_ID, USER_NAME) values (15, 1,'ntsvetko');
insert into users (ID, REALM_ID, USER_NAME) values (16, 1,'psowinsk');
insert into users (ID, REALM_ID, USER_NAME) values (17, 1,'timartin');
insert into users (ID, REALM_ID, USER_NAME) values (19, 1,'kkrynick');
insert into users (ID, REALM_ID, USER_NAME) values (20, 1,'vhyhynia');
insert into users (ID, REALM_ID, USER_NAME) values (21, 1,'achadaj');
insert into users (ID, REALM_ID, USER_NAME) values (22, 1,'llefever');
insert into users (ID, REALM_ID, USER_NAME) values (23, 1,'burbanie');
insert into users (ID, REALM_ID, USER_NAME) values (25, 1,'bmoldove');
insert into users (ID, REALM_ID, USER_NAME) values (26, 1,'achelaru');
insert into users (ID, REALM_ID, USER_NAME) values (27, 1,'mvoelkle');
insert into users (ID, REALM_ID, USER_NAME) values (28, 1,'rmucha');

insert into roles (ID, ROLE_NAME) values (1, 'ROLE_CMW_USER');
insert into roles (ID, ROLE_NAME) values (2, 'ROLE_TEST_CMW_USER');
insert into roles (ID, ROLE_NAME) values (3, 'ROLE_MOCK_SYSTEM_USER');
insert into roles (ID, ROLE_NAME) values (4, 'ROLE_READER');
insert into roles (ID, ROLE_NAME) values (5, 'ROLE_ADMIN');
insert into roles (ID, ROLE_NAME) values (6, 'ROLE_PERMISSIONLESS');
insert into roles (ID, ROLE_NAME) values (7, 'ROLE_PM_USER');
insert into roles (ID, ROLE_NAME) values (8, 'ROLE_VARIABLE_EDITOR');
insert into roles (ID, ROLE_NAME) values (9, 'ROLE_GROUP_EDITOR');


insert into PERMISSIONS (ID, PERMISSION_NAME) values (1, 'TEST-CMW:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (2, 'TEST-CMW:WRITE');

insert into PERMISSIONS (ID, PERMISSION_NAME) values (3, 'MOCK-SYSTEM:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (4, 'MOCK-SYSTEM:WRITE');

insert into PERMISSIONS (ID, PERMISSION_NAME) values (5, 'CMW:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (6, 'CMW:WRITE');

insert into PERMISSIONS (ID, PERMISSION_NAME) values (7, 'PM:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (8, 'PM:WRITE');

insert into PERMISSIONS (ID, PERMISSION_NAME) values (9, 'VARIABLE:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (10, 'VARIABLE:WRITE');

insert into PERMISSIONS (ID, PERMISSION_NAME) values (11, 'HIERARCHY:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (12, 'HIERARCHY:WRITE');

insert into PERMISSIONS (ID, PERMISSION_NAME) values (13, 'ALL');

insert into PERMISSIONS (ID, PERMISSION_NAME) values (14, 'GROUP:READ');
insert into PERMISSIONS (ID, PERMISSION_NAME) values (15, 'GROUP:WRITE');

insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID) VALUES (1, 1);
insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID) VALUES (1, 2);
insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID) VALUES (1, 5);
insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID) VALUES (1, 6);

insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID) VALUES (2, 1);
insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID) VALUES (2, 2);

insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID) VALUES (3, 3);
insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID) VALUES (3, 4);

insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID) VALUES (4, 1);
insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID) VALUES (4, 3);
insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID) VALUES (4, 5);
insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID) VALUES (4, 7);

insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID)
select 5, id from permissions;

insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID) VALUES (7, 7);
insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID) VALUES (7, 8);

insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID) VALUES (8, 9);
insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID) VALUES (8, 10);

insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID) VALUES (9, 11);
insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID) VALUES (9, 12);
insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID) VALUES (9, 14);
insert into ROLES_PERMISSIONS (ROLE_ID, PERMISSION_ID) VALUES (9, 15);


insert into USERS_ROLES (USER_ID, ROLE_ID)
select id, 5 from users where user_name not like '%-%';

insert into USERS_ROLES (USER_ID, ROLE_ID)
select id, 8 from users where user_name not like '%-%';

insert into USERS_ROLES (USER_ID, ROLE_ID) VALUES (3, 1);
insert into USERS_ROLES (USER_ID, ROLE_ID) VALUES (3, 2);

insert into USERS_ROLES (USER_ID, ROLE_ID) VALUES (4, 2);
insert into USERS_ROLES (USER_ID, ROLE_ID) VALUES (4, 3);

insert into USERS_ROLES (USER_ID, ROLE_ID) VALUES (5, 2);

insert into USERS_ROLES (USER_ID, ROLE_ID) VALUES (7, 6);

insert into USERS_ROLES (USER_ID, ROLE_ID) VALUES (8, 4);

MERGE INTO SYSTEMS s1
USING (SELECT 'CERN' system_name_,
            '{"type":"record","name":"CernKey","namespace":"cern.nxcals","fields":[]}' entity_key_defs_,
            '{"type":"record","name":"CernPartition","namespace":"cern.nxcals","fields":[]}' partition_key_defs_,
            '{"type":"record","name":"CernTimestamp","fields":[]}' time_key_defs_,
            null record_version_key_defs_,
            0 rec_version_
       FROM dual
       ) s2
ON (s1.SYSTEM_NAME=s2.system_name_)
WHEN NOT MATCHED THEN
INSERT (SYSTEM_ID,SYSTEM_NAME,ENTITY_KEY_DEFS,PARTITION_KEY_DEFS,TIME_KEY_DEFS,RECORD_VERSION_KEY_DEFS,REC_VERSION)
VALUES ((SELECT MAX(SYSTEM_ID)+1 FROM SYSTEMS), s2.system_name_, s2.entity_key_defs_, s2.partition_key_defs_, s2.time_key_defs_, s2.record_version_key_defs_, s2.rec_version_);

-- Don't forget to commit.
