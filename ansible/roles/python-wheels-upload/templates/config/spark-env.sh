#!/usr/bin/env bash

#
# Licensed to the Apache Software Foundation (ASF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The ASF licenses this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# This file is sourced when running various Spark programs.
# Copy it as spark-env.sh and edit that to configure Spark for your site.

# Options read when launching programs locally with
# ./bin/run-example or ./bin/spark-submit
# - HADOOP_CONF_DIR, to point Spark towards Hadoop configuration files
# - SPARK_LOCAL_IP, to set the IP address Spark binds to on this node
# - SPARK_PUBLIC_DNS, to set the public dns name of the driver program
# - SPARK_CLASSPATH, default classpath entries to append

# Options read by executors and drivers running inside the cluster
# - SPARK_LOCAL_IP, to set the IP address Spark binds to on this node
# - SPARK_PUBLIC_DNS, to set the public DNS name of the driver program
# - SPARK_CLASSPATH, default classpath entries to append
# - SPARK_LOCAL_DIRS, storage directories to use on this node for shuffle and RDD data
# - MESOS_NATIVE_JAVA_LIBRARY, to point to your libmesos.so if you use Mesos

# Options read in YARN client mode
# - HADOOP_CONF_DIR, to point Spark towards Hadoop configuration files
# - SPARK_EXECUTOR_INSTANCES, Number of executors to start (Default: 2)
# - SPARK_EXECUTOR_CORES, Number of cores for the executors (Default: 1).
# - SPARK_EXECUTOR_MEMORY, Memory per Executor (e.g. 1000M, 2G) (Default: 1G)
# - SPARK_DRIVER_MEMORY, Memory for Driver (e.g. 1000M, 2G) (Default: 1G)

# Options for the daemons used in the standalone deploy mode
# - SPARK_MASTER_HOST, to bind the master to a different IP address or hostname
# - SPARK_MASTER_PORT / SPARK_MASTER_WEBUI_PORT, to use non-default ports for the master
# - SPARK_MASTER_OPTS, to set config properties only for the master (e.g. "-Dx=y")
# - SPARK_WORKER_CORES, to set the number of cores to use on this machine
# - SPARK_WORKER_MEMORY, to set how much total memory workers have to give executors (e.g. 1000m, 2g)
# - SPARK_WORKER_PORT / SPARK_WORKER_WEBUI_PORT, to use non-default ports for the worker
# - SPARK_WORKER_INSTANCES, to set the number of worker processes per node
# - SPARK_WORKER_DIR, to set the working directory of worker processes
# - SPARK_WORKER_OPTS, to set config properties only for the worker (e.g. "-Dx=y")
# - SPARK_DAEMON_MEMORY, to allocate to the master, worker and history server themselves (default: 1g).
# - SPARK_HISTORY_OPTS, to set config properties only for the history server (e.g. "-Dx=y")
# - SPARK_SHUFFLE_OPTS, to set config properties only for the external shuffle service (e.g. "-Dx=y")
# - SPARK_DAEMON_JAVA_OPTS, to set config properties for all daemons (e.g. "-Dx=y")
# - SPARK_PUBLIC_DNS, to set the public dns name of the master or workers

# Generic options for the daemons used in the standalone deploy mode
# - SPARK_CONF_DIR      Alternate conf dir. (Default: ${SPARK_HOME}/conf)
# - SPARK_LOG_DIR       Where log files are stored.  (Default: ${SPARK_HOME}/logs)
# - SPARK_PID_DIR       Where the pid file is stored. (Default: /tmp)
# - SPARK_IDENT_STRING  A string representing this instance of spark. (Default: $USER)
# - SPARK_NICENESS      The scheduling priority for daemons. (Default: 0)
# - SPARK_NO_DAEMONIZE  Run the proposed command in the foreground. It will not output a PID file.


# We need to check if we are using YARN or not for Python to work correctly.
# Setting LCG path to python in local mode will not work if LCG is not mounted on the driver machine.
# In the local mode we set python to the one from the environment.
# In order to determine that we check the spark-default.conf file and script input arguments ($@).
# If you use spark-submit and set this inside the Python script we are unable to determine that. (jwozniak)

NXCALS_VERSION="{{application_version}}"

#Global error handling
handle_error() {
    echo "An error occurred on line $1"
    exit 1
}
trap 'handle_error $LINENO' ERR

if command -v flock &> /dev/null; then
    FLOCK="flock -x 100"
  else
    echo "flock is not installed, race condition possible if running multiple scripts for a single venv in parallel!"
    FLOCK=
fi


function md5_cmd {
  if [[ $(uname) == "Darwin" ]]; then
    md5
  else
    md5sum
  fi
}



function get_writable_dir() {
  dir=$(dirname "$(mktemp -u)")
  python_version=$("${VIRTUAL_ENV}/bin/python" -V)
  NXCALS_WORKSPACE_TEMP_DIR="$dir/nxcals-$(echo -n "${SPARK_HOME}${USER}${python_version}${NXCALS_VERSION}" | md5_cmd | cut -d " " -f 1)"
  if [ ! -d "$NXCALS_WORKSPACE_TEMP_DIR" ]; then
    mkdir -p "$NXCALS_WORKSPACE_TEMP_DIR"
    chmod 700 "$NXCALS_WORKSPACE_TEMP_DIR"
  fi
  export NXCALS_WORKSPACE_TEMP_DIR
}

if [ ! "$NXCALS_WORKSPACE_TEMP_DIR" ]; then
  get_writable_dir
fi

function pack_venv() {
  if [ ! "$NXCALS_PACK_ALL_PACKAGES" ]; then
    venv-pack --python-prefix "$PYTHON_PREFIX" --output "$PACKED_VENV_FILE" \
      --exclude nxcals-bundle/nxcals_jars/\* --exclude nxcals-bundle/jars/\* --exclude nxcals-bundle/examples/\* \
      --exclude \*/pyspark/jars/\* --exclude \*/pyspark/examples/\*
  else
    venv-pack --python-prefix "$PYTHON_PREFIX" --output "$PACKED_VENV_FILE"
  fi
}

function fix_venv() {
    echo "Extracing packed venv to fix symlink to exec..."
    # Fix packed venv - symlinks to python exec may be broken
    FIXED_VENV_DIR="$NXCALS_WORKSPACE_TEMP_DIR/venv"
    mkdir -p $FIXED_VENV_DIR
    tar -xzf $NXCALS_WORKSPACE_TEMP_DIR/nxcals-python3-env.tar.gz -C $FIXED_VENV_DIR

    echo "Fixing symlink to exec in venv..."

    for file in $FIXED_VENV_DIR/bin/python*; do
        if [ -L "$file" ]; then
          newTarget=$PYTHON_PREFIX/bin/python$PYTHON_VERSION
          rm $file
          ln -s $newTarget $file
        fi
    done

    echo "Packing again venv..."
    OLD_PWD=`pwd`
    rm $NXCALS_WORKSPACE_TEMP_DIR/nxcals-python3-env.tar.gz
    cd $FIXED_VENV_DIR
    tar -czf $NXCALS_WORKSPACE_TEMP_DIR/nxcals-python3-env.tar.gz ./*
    cd $OLD_PWD
    rm -r $FIXED_VENV_DIR
}


#A must as the pySpark is using those 2 variables to set the python on the executor. Both vars must be set. The driver uses what is the current python3 and the executor must use the LCG.
#Exlusion is only for the jupyter setting - it shouldn't be overwritten
JUPYTER_PYSPARK_REGEX='^\s*jupyter(\s.*|$)'
if [[ ! $PYSPARK_DRIVER_PYTHON =~ $JUPYTER_PYSPARK_REGEX ]]; then
  PYSPARK_DRIVER_PYTHON=${VIRTUAL_ENV}/bin/python
  export PYSPARK_DRIVER_PYTHON
fi

# This has to be empty, we use jar with hadoop config
export HADOOP_CONF_DIR=
SPARK_DEFAULTS="$SPARK_CONF_DIR"/spark-defaults.conf
OLD_SPARK_CONF_DIR="$SPARK_CONF_DIR"
export SPARK_CONF_DIR="$NXCALS_WORKSPACE_TEMP_DIR/conf"

PACKED_VENV_FILE="${NXCALS_WORKSPACE_TEMP_DIR}"/{{spark_packed_venv_name}}
LOCK="${NXCALS_WORKSPACE_TEMP_DIR}"/.lock

echo "ENVIRONMENT:"
echo "NXCALS_VERSION=${NXCALS_VERSION}"
echo "VIRTUAL_ENV=${VIRTUAL_ENV}"
echo "SPARK_HOME=${SPARK_HOME}"
echo "SPARK_CONF_DIR=${SPARK_CONF_DIR}"
echo "SPARK_DEFAULTS=${SPARK_DEFAULTS}"
echo "NXCALS_WORKSPACE_TEMP_DIR=${NXCALS_WORKSPACE_TEMP_DIR}"
echo "PACKED_VENV_FILE=${PACKED_VENV_FILE}"
echo "PYSPARK_PYTHON=${PYSPARK_PYTHON}"
echo "PYSPARK_DRIVER_PYTHON=${PYSPARK_DRIVER_PYTHON}"
echo "PYSPARK_DRIVER_PYTHON_OPTS=${PYSPARK_DRIVER_PYTHON_OPTS}"
echo "LOCK=${LOCK}"
echo
echo "IMPORTANT:"
echo "Rebuilding of the packed venv is required in cases there are new or modified packages provided by a user. In order to recreate the packed venv please:"
echo "        - remove the file (rm $PACKED_VENV_FILE)"
echo "        - execute your script again (rebuild will be performed during the startup of PySpark/Python)"
echo "Target directory can be set with an env variable NXCALS_WORKSPACE_TEMP_DIR (if not set a temp dir will be used)."
echo "Adding NXCALS related files to packed venv can be enabled by setting NXCALS_PACK_ALL_PACKAGES with any value."


(
  $FLOCK
  if [ ! -e "$NXCALS_WORKSPACE_TEMP_DIR/conf/spark-defaults.conf" ]; then
      echo "Copying $SPARK_DEFAULTS to $SPARK_CONF_DIR ..."
      mkdir -p "$SPARK_CONF_DIR"

      cp "$OLD_SPARK_CONF_DIR"/spark-defaults.conf "$SPARK_CONF_DIR"
      cp "$OLD_SPARK_CONF_DIR"/log4j2.properties "$SPARK_CONF_DIR"
      NEW_SPARK_CONF="$SPARK_CONF_DIR/spark-defaults.conf"

      # make the spark.jars path absolute otherwise they are relative to the current working directory
      # Mac OSX requires escaping brackets


      if [[ $(uname) == "Darwin" ]]; then
          sed -i -r 's,\([^/]\)nxcals_jars/\([^,]*\),\1'"$SPARK_HOME"'/nxcals_jars/\2,g' "$NEW_SPARK_CONF"
      else
          sed -i -r 's,([^/])nxcals_jars/([^,]*),\1'"$SPARK_HOME"'/nxcals_jars/\2,g' "$NEW_SPARK_CONF"
      fi

      # Replace the placeholder for the virtual_env path in spark-defaults.conf
      # The archive is specified with '#environment' because that is how spark
      # knows where to unzip it on the executors under a new directory called environment.
      # For further information: http://spark.apache.org/docs/latest/api/python/user_guide/python_packaging.html
      sed -i -r 's@spark.yarn.dist.archives.*@spark.yarn.dist.archives '"$NXCALS_WORKSPACE_TEMP_DIR"'/'{{spark_packed_venv_name}}'#'{{spark_bundle_pyspark_venv_name}}'@g' "$NEW_SPARK_CONF"
  fi
) 100>$LOCK

echo "Trying to determine YARN usage to make Python work correctly (conf/spark-env.sh)..."

exit_code=0
grep -q -e "^\s*spark.master\s*yarn" "${SPARK_CONF_DIR}/spark-defaults.conf" || exit_code=$?

if [[ "${exit_code}" == "0" || $@ =~ .*master.*yarn.* ]]; then
  echo "Using YARN"
  # Normally $PYSPARK_PYTHON is set in the spark_session_builder.get_or_create().
  # But when user calls pyspark directly, this option is required.
  export PYSPARK_PYTHON='./environment/bin/python'

  if [[ ! -e "$PACKED_VENV_FILE" ]]; then
    echo "Building packed venv ..."

    PYTHON_VERSION=`python3 --version | grep -oE '([0-9]+\.[0-9]+)\.' | grep -oE '([0-9]+\.[0-9]+)'`
    PYTHON_PREFIX="/var/nxcals/acc-py-current-$PYTHON_VERSION/"

    if [[ "$PYTHON_VERSION" != "3.9" &&  "$PYTHON_VERSION" != "3.11" ]]; then
      echo "ERROR: YARN cluster doesn't support Python in version $PYTHON_VERSION. Supported versions are either 3.9 or 3.11"
      exit 1
    else
      (
        $FLOCK
        if [[ ! -e "$PACKED_VENV_FILE" ]]; then
            echo "Creating packed venv..."
            pack_venv
            fix_venv
            echo -e "Packed venv created.\n...done!"
        else
            echo "Packed venv already built..."
        fi
      ) 100>$LOCK
    fi
  fi
  echo "IMPORTANT:"
  echo "In order to recreate the packed venv please remove the file (rm $PACKED_VENV_FILE)"
  echo "Then, during the next start of spark session there will be created a new packed venv."
  echo "Target directory can be set with an env variable NXCALS_WORKSPACE_TEMP_DIR (if not set a temp dir will be used)."
  echo "Adding NXCALS related files to packed venv can be enabled by setting NXCALS_PACK_ALL_PACKAGES with any value."
else
  echo "Not using YARN"
  export PYSPARK_PYTHON=${VIRTUAL_ENV}/bin/python
fi
