#!/bin/bash

export JAVA_HOME={{java_path_to_alias}}

export JVMFLAGS="{{java_opts}}"
export ZOOCFGDIR={{install_dir}}/{{module}}-{{version}}/config
export ZOOPIDFILE={{install_dir}}/{{module}}-{{version}}/application.pid
export ZOO_LOG_DIR={{install_dir}}/{{module}}-{{version}}/log

./bin/zkServer.sh start


