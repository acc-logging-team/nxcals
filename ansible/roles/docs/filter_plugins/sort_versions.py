from distutils.version import LooseVersion

def filter_sort_versions(value):
    return sorted(value, key=LooseVersion, reverse=True)

class FilterModule(object):
    def filters(self):
        return {'sort_versions': filter_sort_versions}
