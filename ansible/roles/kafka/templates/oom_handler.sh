#!/bin/bash

echo 'Process killed, will reboot in a loop. Investigate.' | mail -s '[Env:{{ project_namespace | upper }}] OutOfMemoryError on {{instance}}@{{ansible_hostname}}' {{oom_notification_recipients}}
kill -9 `cat {{install_dir}}/{{ instance }}/application.pid`
