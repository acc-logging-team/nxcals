# PyTimber

A new version of backward compatible wrapping of the CERN Accelerator Logging Service [NXCALS](https://nxcals-docs.web.cern.ch/current/) API.

[nxcals]: https://confluence.cern.ch/display/NXCALS/NXCALS+Home

## Installation

Please refer to the [installation documentation](https://confluence.cern.ch/display/PYT/New+PyTimber+%284.x.x.x%29+installation).

## Usage

Complete API documentation:
https://nxcals-docs.web.cern.ch/current/python-nxcals-docs/api/pytimber.html#module-pytimber

### Example
Import:

```python
import pytimber
ldb = pytimber.LoggingDB(spark_session = spark)
```

Search for variables:

```python
print(ldb.search_variables('HX:BETA%'))
```

Get data:

```python
t1 = '2023-01-01 00:00:00.000'
t2 = '2023-12-31 00:00:00.000'
print(ldb.get('HX:FILLN', t1, t2))
```

```python
t1 = '2023-06-07 00:00:00.000'
t2 = '2023-06-07 01:00:00.000'
print(ldb.get('LHC.BQBBQ.CONTINUOUS_HS.B1:ACQ_DATA_H', t1, t2))
```

Explore variable hierarchies:

```python
result=ldb.get_variables_for_hierarchies("/LHC/Collimators/BPM/bpm%")
print(result["/LHC/Collimators/BPM/bpmColl"])
```

Get data for a particular LHC fill:

```python
fill = ldb.get_lhc_fill_data(4760)
print(fill)
t1 = fill['startTime']
t2 = fill['endTime']
print(ldb.get('LHC.BQBBQ.CONTINUOUS_HS.B1:ACQ_DATA_H', t1, t2))
```

Find all fill number in the last 48 hours that contained a ramp:

```python
import datetime
t2 = datetime.datetime.now()
t1 = t2 - datetime.timedelta(hours=48)
fills = ldb.get_lhc_fills_by_time(t1, t2, beam_modes='RAMP')
print([f['fillNumber'] for f in fills])
```

By default all times are returned as Unix timestamps. If you pass
`unixtime=False` to `get()`, `getAligned()`, `getLHCFillData()` or
`getLHCFillsByTime()` then `datetime` objects are returned instead.

## Usage with PageStore

pytimber can be combined with PageStore for local data storage. Usage example:

```python
from pytimber import pagestore
import time

ldb = pytimber.LoggingDB(spark_session=spark)
mydb = pagestore.PageStore('mydata.db', './datadb')

t1 = time.mktime(time.strptime('Fri Apr 25 00:00:00 2016'))
mydb.store(ldb.get('%RQTD%I_MEAS', t1, t1+60))
mydb.store(ldb.get('%RQTD%I_MEAS', t1+60, t1+120))

mydata = mydb.get('RPMBB.UA47.RQTD.A45B2:I_MEAS', t1+90, t1+110)
data = ldb.get('RPMBB.UA47.RQTD.A45B2:I_MEAS', t1+90, t1+110)
for k in data:
    print(mydata[k][0] - data[k][0])
    print(mydata[k][1] - data[k][1])
```
