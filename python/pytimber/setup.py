"""
setup.py for pytimber.

For reference see
https://packaging.python.org/guides/distributing-packages-using-setuptools/

"""

import os
from pathlib import Path
from typing import Dict, Iterable

from setuptools import setup

HERE = Path(__file__).parent.absolute()
with (HERE / "README.md").open("rt") as fh:
    LONG_DESCRIPTION = fh.read().strip()

VERSION = os.environ.get("version", "5.0.0.0")
CORE_REQUIREMENTS = os.environ.get("required_packages", "").split(" ")
TEST_REQUIREMENTS = os.environ.get("required_test_packages", "").split(" ")
REQUIREMENTS: Dict[str, Iterable[str]] = {
    "core": CORE_REQUIREMENTS,
    "test": TEST_REQUIREMENTS,
}

assert VERSION

setup(
    name="pytimber",
    version=VERSION,
    description="A Python wrapping of NXCALS API",
    long_description=LONG_DESCRIPTION,
    long_description_content_type="text/markdown",
    maintainer="NXCALS Team (BE-CSS-CPA)",
    maintainer_email="acc-logging-support@cern.ch",
    author="Riccardo De Maria",
    url="https://confluence.cern.ch/display/NXCALS",
    project_urls={
        "Documentation": "https://confluence.cern.ch/display/PYT/User+Documentation",
        "API Reference": "https://nxcals-docs.web.cern.ch/current/python-nxcals-docs/api/pytimber.html#module-pytimber",
        "Repository": "https://gitlab.cern.ch/acc-logging-team/nxcals/-/tree/develop/python/pytimber",
    },
    packages=["pytimber"],
    install_requires=REQUIREMENTS["core"],
    python_requires=">= 3.9",
    classifiers=[
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.11",
    ],
)
