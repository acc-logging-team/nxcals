from datetime import datetime
from typing import Any, Dict, Iterable, Union

from nxcals.api.extraction.data.builders import DataQuery
from pyspark.sql import Row, SparkSession
from pyspark.sql.functions import avg, count, max, min, stddev_pop

from .utils import (
    TIMESTAMP,
    VALUE,
    VARIABLE_NAME,
)
from .utils import (
    PyTimberTimeType,
    get_nanos_from,
    nanos_to_datetime,
    is_value_array,
    groupby,
)
from .variable import Variable
from dataclasses import dataclass


@dataclass
class Statistics:
    MinTstamp: Union[float, datetime]
    MaxTstamp: Union[float, datetime]
    ValueCount: int


@dataclass
class ScalarStatistics(Statistics):
    MinValue: float
    MaxValue: float
    AvgValue: float
    StandardDeviationValue: float


def _get_stat_from(
    row: Row, unix_time: bool, is_array: bool
) -> Union[Statistics, ScalarStatistics]:
    min_stamp = nanos_to_datetime(row.min_stamp)
    max_stamp = nanos_to_datetime(row.max_stamp)

    min_tstamp = min_stamp.timestamp() if unix_time else min_stamp
    max_tstamp = max_stamp.timestamp() if unix_time else max_stamp
    common_values = (min_tstamp, max_tstamp, row.count_)

    if is_array:
        return Statistics(
            *common_values,
        )

    return ScalarStatistics(
        *common_values,
        row.min,
        row.max,
        row.avg,
        row.stddev,
    )


class StatsManager:
    def __init__(self, spark: SparkSession):
        self._spark = spark

    def get_stats(
        self,
        variables: Iterable[Variable],
        t1: PyTimberTimeType,
        t2: PyTimberTimeType,
        unix_time: bool,
    ) -> Dict[str, Union[Statistics, ScalarStatistics]]:
        var_names_by_system_type = {}

        vars_without_declared_type = [v for v in variables if not v.declared_type]
        if vars_without_declared_type:
            raise ValueError(
                f"Declared type not specified for: {[v.name for v in vars_without_declared_type]}"
            )

        key_mapper = lambda v: (v.system, v.declared_type.toString())
        value_mapper = lambda v: v.name
        var_names_by_system_type = groupby(variables, key_mapper, value_mapper)

        ret = {}
        for (system, declared_type), variable_names in var_names_by_system_type.items():
            ret.update(
                self._get_stats_for(
                    system, declared_type, variable_names, t1, t2, unix_time
                )
            )

        return ret

    def _get_stats_for(
        self,
        system: str,
        declared_type: Any,
        variables: Iterable[Variable],
        from_time: PyTimberTimeType,
        to_time: PyTimberTimeType,
        unix_time: bool,
    ) -> Dict[str, Union[Statistics, ScalarStatistics]]:
        dataset = (
            DataQuery.builder(self._spark)
            .variables()
            .system(system)
            .nameIn(variables)
            .timeWindow(get_nanos_from(from_time), get_nanos_from(to_time))
            .build()
        )

        is_array = is_value_array(dataset.schema)

        agg_columns = [
            count(VALUE).alias("count_"),
            max(TIMESTAMP).alias("max_stamp"),
            min(TIMESTAMP).alias("min_stamp"),
        ]

        if not is_array:
            agg_columns += [
                max(VALUE).alias("max"),
                min(VALUE).alias("min"),
                avg(VALUE).alias("avg"),
                stddev_pop(VALUE).alias("stddev"),
            ]

        dataset = dataset.groupBy(VARIABLE_NAME).agg(*agg_columns)

        return {
            row[VARIABLE_NAME]: _get_stat_from(row, unix_time, is_array)
            for row in dataset.collect()
        }
