from typing import Any, Optional

from pyspark.sql import SparkSession

WARNING_MESSAGE_NO_SPARK_SESSION_BUILDER = "Cannot autocreate spark session, because package nxcals-spark-session-builder is missing"
ERROR_MESSSAGE_NO_SPARK_SESSION = (
    "Neither spark_session argument is passed nor package nxcals_spark_session_builder is installed."
    + "Cannot process without spark session. Pass spark session or install nxcals_spark_session_builder."
    + "On SWAN please use pytimber.LoggingDB(spark_session=spark)."
)


def try_to_get_or_create(*args: Any, **kwargs: Any) -> Optional[SparkSession]:
    try:
        from nxcals.spark_session_builder import Flavor
        from nxcals.spark_session_builder import get_or_create as _get_or_create
    except ImportError:
        print(WARNING_MESSAGE_NO_SPARK_SESSION_BUILDER)
        return None
    flavor = (
        Flavor.value_of(
            kwargs["flavor"],
        )
        if "flavor" in kwargs.keys() and kwargs["flavor"]
        else None
    )

    kwargs.pop("flavor")

    return _get_or_create(flavor=flavor, *args, **kwargs)


def _get_spark_session(*args: Any, **kwargs: Any) -> SparkSession:
    maybe_spark_session = try_to_get_or_create(*args, **kwargs)
    if maybe_spark_session:
        return maybe_spark_session

    raise RuntimeError(ERROR_MESSSAGE_NO_SPARK_SESSION)
