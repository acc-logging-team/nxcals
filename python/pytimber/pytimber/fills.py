from typing import Any, Dict, List, NamedTuple, Optional, Set

from pyspark.sql import SparkSession
from pytimber.utils import PatternOrList, index_is_in_bound
from py4j.java_gateway import JavaObject

from .utils import PyTimberTimeType, nanos_to_timestamp, to_time_window


class FillInterval(NamedTuple):
    fillNumber: int
    modeFirstAttribute: float
    modeSecondAttribute: float


def _to_fill(java_fill: Any) -> Dict[str, Any]:
    return {
        "fillNumber": java_fill.getNumber(),
        "startTime": nanos_to_timestamp(java_fill.getValidity().getStartTimeNanos()),
        "endTime": nanos_to_timestamp(java_fill.getValidity().getEndTimeNanos()),
        "beamModes": [
            {
                "mode": mode.getBeamModeValue(),
                "startTime": nanos_to_timestamp(mode.getValidity().getStartTimeNanos()),
                "endTime": nanos_to_timestamp(mode.getValidity().getEndTimeNanos()),
            }
            for mode in java_fill.getBeamModes()
        ],
    }


def _get_beam_modes_from(java_fill: Any) -> Set[str]:
    return set([mode.getBeamModeValue() for mode in java_fill.getBeamModes()])


class FillManager:
    def __init__(self, spark: SparkSession):
        self._spark = spark
        self._fill_service = self._spark._jvm.cern.nxcals.api.custom.service.fill.FillServiceFactory.getInstance(
            self._spark._jsparkSession,
        )

    def get_lhc_fill_data(
        self, fill_number: Optional[int] = None
    ) -> Optional[Dict[str, Any]]:
        java_fill = (
            self._get_fill(fill_number)
            if fill_number
            else self._get_last_completed_fill()
        )
        if not java_fill:
            return None

        return _to_fill(java_fill)

    def get_lhc_fills_by_time(
        self,
        from_time: PyTimberTimeType,
        to_time: PyTimberTimeType,
        beam_modes: Optional[PatternOrList] = None,
    ) -> List[Dict[str, Any]]:
        fills = self._get_fills(from_time, to_time)
        if not beam_modes:
            return [_to_fill(f) for f in fills]

        modes = (
            [mode.strip() for mode in beam_modes.split(",")]
            if isinstance(beam_modes, str)
            else beam_modes
        )
        return [
            _to_fill(f)
            for f in fills
            if not set(modes).isdisjoint(_get_beam_modes_from(f))
        ]

    def get_interval_by_lhc_modes(
        self,
        t1: PyTimberTimeType,
        t2: PyTimberTimeType,
        mode1: str,
        mode2: str,
        mode1time: str = "startTime",
        mode2time: str = "endTime",
        mode1idx: int = 0,
        mode2idx: int = -1,
    ) -> List[FillInterval]:
        fills = self.get_lhc_fills_by_time(t1, t2, [mode1, mode2])
        out = []
        for fill in fills:
            mode1_values = [
                bm[mode1time] for bm in fill["beamModes"] if bm["mode"] == mode1
            ]
            mode2_values = [
                bm[mode2time] for bm in fill["beamModes"] if bm["mode"] == mode2
            ]

            if index_is_in_bound(mode1idx, len(mode1_values)) and index_is_in_bound(
                mode2idx, len(mode2_values)
            ):
                out.append(
                    FillInterval(
                        fill["fillNumber"],
                        mode1_values[mode1idx],
                        mode2_values[mode2idx],
                    )
                )
        return out

    # ################# private stuff ############################
    def _get_fill(self, fill_no: int) -> Any:
        return self._get_optional(self._fill_service.findFill(fill_no))

    def _get_last_completed_fill(self) -> Any:
        return self._get_optional(self._fill_service.getLastCompleted())

    def _get_fills(
        self, from_time: PyTimberTimeType, to_time: PyTimberTimeType
    ) -> List[Any]:
        tw = to_time_window(self._spark, from_time, to_time)
        return list(self._fill_service.findFills(tw.getStartTime(), tw.getEndTime()))

    @staticmethod
    def _get_optional(maybe_res: JavaObject) -> Optional[JavaObject]:
        return maybe_res.get() if maybe_res.isPresent() else None
