import json
from datetime import datetime, timezone
from typing import Any, Dict, Iterable, List, NamedTuple, Optional, Tuple

from pyspark.sql import SparkSession
from pytimber import utils
from pytimber.utils import (
    TIME_FORMAT,
    PatternOrList,
    PyTimberTimeType,
)


class _SnapshotSearchCriteria(NamedTuple):
    fundamental_pattern: Optional[str] = None
    start_time: Optional[PyTimberTimeType] = None
    end_time: Optional[PyTimberTimeType] = None

    def get_search_criteria(
        self,
    ) -> Tuple[Optional[str], Optional[PyTimberTimeType], Optional[PyTimberTimeType]]:
        return (self.fundamental_pattern, self.start_time, self.end_time)


class SnapshotManager:
    def __init__(self, spark: SparkSession):
        self.spark = spark
        self._nxcals_custom_metadata_api = (
            self.spark._jvm.cern.nxcals.api.custom.extraction.metadata
        )

        self._nxcals_custom_api = self.spark._jvm.cern.nxcals.api.custom
        self._snapshot_service = self._nxcals_custom_metadata_api.SnapshotServiceFactory.createSnapshotService()
        self._time_definition = self._nxcals_custom_api.domain.Snapshot.TimeDefinition

    def get_snapshots(
        self,
        pattern_or_list: PatternOrList,
        owner_pattern: Optional[str] = None,
        description_pattern: Optional[str] = None,
    ) -> List[Any]:
        snapshots = self._nxcals_custom_metadata_api.queries.Snapshots

        if isinstance(pattern_or_list, str):
            snap_condition = snapshots.suchThat().name().like(pattern_or_list)
        elif isinstance(pattern_or_list, Iterable):
            snap_condition = getattr(
                snapshots.suchThat().name(),
                "in",
            )(pattern_or_list)
        else:
            raise ValueError(f"{pattern_or_list} neither pattern nor list")

        if owner_pattern:
            snap_condition = (
                getattr(
                    snap_condition,
                    "and",
                )()
                .ownerName()
                .like(owner_pattern)
            )

        if description_pattern is not None:
            snap_condition = (
                getattr(
                    snap_condition,
                    "and",
                )()
                .description()
                .like(description_pattern)
            )

        return list(self._snapshot_service.findAll(snap_condition))

    def get_variables(self, snapshot: Any) -> Any:
        group_property_name = self._nxcals_custom_api.domain.GroupPropertyName

        return self._snapshot_service.getVariables(
            snapshot.getId(),
        )[group_property_name.getSelectedVariables.toString()]

    # Do not rename parameters. They have to match values stored in the database.
    # python:S117 Local variable and function parameter names should comply with a naming convention
    def create_snapshot_search_criteria(
        self,
        fillNumber: Optional[str] = None,  # NOSONAR
        isEndTimeDynamic: Optional[str] = None,  # NOSONAR
        getTimeZone: Optional[str] = None,  # NOSONAR
        beamModeStart: Optional[str] = None,  # NOSONAR
        beamModeEnd: Optional[str] = None,  # NOSONAR
        getPriorTime: Optional[str] = None,  # NOSONAR
        getTime: Optional[str] = None,  # NOSONAR
        getDynamicDuration: Optional[str] = None,  # NOSONAR
        getStartTime: Optional[str] = None,  # NOSONAR
        getEndTime: Optional[str] = None,  # NOSONAR
        fundamentalFilter: Optional[str] = None,  # NOSONAR
        **kwargs: Any,
    ) -> _SnapshotSearchCriteria:
        """
        Please note that:
        fillNumber attribute is mutually exclusive with isEndTimeDynamic since they represent 2 different ways of
        specyfing the timew indow: "by fill" vs "dynamic". If both are not present, a search with a fixed time window
        will be performed.
        The fundamentalFilter attribute is independent of the search method.
        """

        # Note: accessing kwargs from this function is not encouraged in order to keep the signature readable.

        if fillNumber is not None:
            _beam_mode_data = self._nxcals_custom_api.domain.constants.BeamModeData
            time_definition = self._time_definition.forFill(
                int(fillNumber),
                _beam_mode_data.fromJson(beamModeStart),
                _beam_mode_data.fromJson(beamModeEnd),
            )

        elif isEndTimeDynamic is not None:
            if isEndTimeDynamic == "true":
                if getDynamicDuration is None:
                    raise ValueError("getDynamicDuration cannot be None")

                if getPriorTime is None:
                    raise ValueError("getPriorTime cannot be None")

                if getTime is None:
                    raise ValueError("getTime cannot be None")

                time_zone = (
                    self._nxcals_custom_api.domain.constants.LoggingTimeZone.valueOf(
                        getTimeZone
                    )
                )

                prior_time = getattr(
                    self._nxcals_custom_api.domain.snapshot.PriorTime, "from"
                )(getPriorTime)

                time_unit = (
                    self._nxcals_custom_api.domain.constants.DynamicTimeUnit.valueOf(
                        getTime
                    )
                )

                time_definition = self._time_definition.dynamic(
                    int(getDynamicDuration), time_zone, prior_time, time_unit
                )
            else:
                # Get fixed range
                t1 = utils.get_instant_from(
                    self.spark,
                    utils.get_nanos_from(
                        SnapshotManager._to_datetime(getTimeZone, getStartTime)
                    ),
                )
                t2 = utils.get_instant_from(
                    self.spark,
                    utils.get_nanos_from(
                        SnapshotManager._to_datetime(getTimeZone, getEndTime)
                    ),
                )

                time_definition = self._time_definition.fixedDates(t1, t2)
        else:
            raise ValueError(
                "Neither 'fillNumber' nor 'isEndTimeDynamic' is present in the snapshot attributes"
            )

        fundamental_pattern: Optional[str] = None
        if fundamentalFilter is not None:
            fund_filter = json.loads(fundamentalFilter)
            fundamental_pattern = ":".join(
                [
                    fund_filter["accelerator"],
                    fund_filter["lsaCycle"],
                    fund_filter["timingUser"],
                ],
            )
        return _SnapshotSearchCriteria(
            fundamental_pattern,
            time_definition.getTimeWindow().getStartTimeNanos(),
            time_definition.getTimeWindow().getEndTimeNanos(),
        )

    def get_snapshot_names(
        self,
        pattern_or_list: PatternOrList,
        owner_pattern: str = "%",
        description_pattern: str = "%",
    ) -> List[str]:
        return [
            snapshot.getName()
            for snapshot in self.get_snapshots(
                pattern_or_list,
                owner_pattern,
                description_pattern,
            )
        ]

    def get_variables_for_snapshots(
        self,
        pattern_or_list: PatternOrList,
        owner_pattern: str = "%",
    ) -> Dict[str, List[str]]:
        result = {}

        snapshots = self.get_snapshots(
            pattern_or_list,
            owner_pattern,
        )

        for snapshot in snapshots:
            variables = self.get_variables(snapshot)
            if variables:
                result[snapshot.getName()] = [
                    variable.getVariableName() for variable in variables
                ]

        return result

    @staticmethod
    def _to_datetime(time_zone: Optional[str], ts_str: Optional[str]) -> datetime:
        if time_zone is None:
            raise ValueError("Time zone cannot be None")

        if ts_str is None:
            raise ValueError("Timestamp string cannot be None")

        if time_zone == "UTC_TIME":
            utc_time = datetime.strptime(ts_str, TIME_FORMAT)
            return utc_time.replace(tzinfo=timezone.utc)

        return datetime.strptime(ts_str, TIME_FORMAT)
