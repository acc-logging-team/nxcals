from typing import Any, Iterable, List, Optional, TypeVar, Union

from pyspark.sql import DataFrame, Row, SparkSession

from .utils import PyTimberTimeType, to_time_window

T = TypeVar("T")


class Fundamentals:
    """
    Represents fundamentals data.

    Args:
        fundamental_pattern: The fundamental pattern in the format:
            <acc-pattern>:<lsa-cycle-pattern>:<timing-user-pattern>.
        dest_in: An optional list of destination items to filter in.
        dest_not_in: An optional list of destination items to filter out.

    Attributes:
        acc (str): The acc pattern extracted from the fundamental pattern.
        cycle (str): The lsa cycle pattern extracted from the fundamental pattern.
        user (str): The timing user pattern extracted from the fundamental pattern.

    Raises:
        ValueError: If the provided fundamental pattern is not in the correct format.

    """

    LSA_CYCLE = "LSA_CYCLE"
    USER = "USER"

    def __init__(
        self,
        fundamental_pattern: str,
        dest_in: Optional[Iterable[str]] = None,
        dest_not_in: Optional[Iterable[str]] = None,
    ):
        self.fundamental_pattern = fundamental_pattern
        self.dest_in = dest_in
        self.dest_not_in = dest_not_in

    @property
    def fundamental_pattern(self) -> str:
        """
        Get the fundamental pattern.

        Returns:
            The fundamental pattern.

        """
        return self._fundamental_pattern

    @fundamental_pattern.setter
    def fundamental_pattern(self, pattern: str) -> None:
        """
        Set the fundamental pattern.

        Args:
            pattern: The fundamental pattern to set.

        Raises:
            ValueError: If the provided fundamental pattern is not in the correct format.

        """
        self._validate_and_split_pattern(pattern)
        self._fundamental_pattern = pattern

    def _validate_and_split_pattern(self, pattern: str) -> None:
        triplet = pattern.split(":")
        if len(triplet) != 3:
            raise ValueError(
                f"Wrong pattern: {pattern}, please provide a pattern of a format"
                f" <acc-pattern>:<lsa-cycle-pattern>:<timing-user-pattern>",
            )
        self.acc = triplet[0]
        self.cycle = triplet[1]
        self.user = triplet[2]

    def apply_filter(self, dataset: Any) -> Any:
        """
        Apply fundamentals to a dataset.

        Args:
            dataset: The dataset to apply fundamentals to.

        Returns:
            The filtered dataset.

        """
        if self.dest_in:
            dataset = dataset.where(
                "DEST in (" + ",".join(f"'{item}'" for item in self.dest_in) + ")",
            )

        if self.dest_not_in:
            dataset = dataset.where(
                "DEST not in ("
                + ",".join(f"'{item}'" for item in self.dest_not_in)
                + ")",
            )

        return dataset


FundamentalsType = Union[str, Fundamentals]


class FundamentalVariable:
    def __init__(self, name: str):
        """
        Initialize the FundamentalVariable.

        Args:
            name: The name of the virtual fundamental variable.

        """
        self.name = name

    def get_variable_data_type(self) -> str:
        """
        Get the data type of the virtual fundamental variable.

        Returns:
            The data type of the virtual fundamental variable.

        """
        return "FUNDAMENTAL"

    def get_description(self) -> str:
        """
        Get the description of the virtual fundamental variable.

        Returns:
            The description of the virtual fundamental variable.

        """
        return f"Virtual Fundamental Variable {self.name}"

    def get_system(self) -> str:
        """
        Get the system of the virtual fundamental variable.

        Returns:
            The system of the virtual fundamental variable.

        """
        return "CMW"

    def __str__(self) -> str:
        return self.name


def _get_fundamental_variable_from(row: Row, acc: str) -> FundamentalVariable:
    cycle = row[Fundamentals.LSA_CYCLE]
    user = row[Fundamentals.USER]
    return FundamentalVariable(f"{acc}:{cycle}:{user}")


def _convert_to_fundamentals(pattern_or_record: FundamentalsType) -> Fundamentals:
    return (
        Fundamentals(pattern_or_record)
        if isinstance(pattern_or_record, str)
        else pattern_or_record
    )


class FundamentalsManager:
    def __init__(self, spark: SparkSession):
        self._spark = spark
        self.custom = self._spark._jvm.cern.nxcals.api.custom
        self.fund_service = (
            self.custom.service.fundamental.FundamentalServiceFactory.getInstance(
                self._spark._jsparkSession,
            )
        )

    def get_fundamentals(
        self, t1: PyTimberTimeType, t2: PyTimberTimeType, pattern: FundamentalsType
    ) -> List[FundamentalVariable]:
        fund = _convert_to_fundamentals(pattern)
        ds = (
            self.get_fundamentals_ds(t1, t2, fund)
            .select(Fundamentals.LSA_CYCLE, Fundamentals.USER)
            .distinct()
        )

        return [_get_fundamental_variable_from(row, fund.acc) for row in ds.collect()]

    def get_fundamentals_ds(
        self,
        from_time: PyTimberTimeType,
        to_time: PyTimberTimeType,
        pattern_or_record: FundamentalsType,
        to_df: bool = True,
    ) -> Union[Any, DataFrame]:
        tw = to_time_window(self._spark, from_time, to_time)
        fund = _convert_to_fundamentals(pattern_or_record)
        f_filter = self._create_fundamental_filter_from(fund)

        ds = self.fund_service.getAll(tw, {f_filter})
        if to_df:
            ds = DataFrame(ds, self._spark)
        return fund.apply_filter(ds)

    def _create_fundamental_filter_from(self, triplet: Fundamentals) -> Any:
        builder = self.custom.domain.FundamentalFilter.builder()

        builder.accelerator(triplet.acc)
        builder.lsaCycle(triplet.cycle)
        builder.timingUser(triplet.user)

        return builder.build()
