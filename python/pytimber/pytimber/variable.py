import logging
from typing import Any, Iterable, List, NamedTuple

from pyspark.sql import SparkSession
from pytimber.utils import PatternOrList


class Variable(NamedTuple):
    system: str
    name: str
    unit: str
    description: str
    id: int
    declared_type: Any
    java_object: Any

    @staticmethod
    def from_java_object(v) -> "Variable":
        return Variable(
            v.getSystemSpec().getName(),
            v.getVariableName(),
            v.getUnit(),
            v.getDescription(),
            v.getId(),
            v.getDeclaredType(),
            v,
        )


class VariableManager:
    def __init__(self, spark: SparkSession):
        self.spark = spark

    def get_variables(self, pattern: PatternOrList) -> List[Variable]:
        return [
            Variable.from_java_object(v) for v in self.get_nxcals_variables(pattern)
        ]

    def get_nxcals_variables(self, pattern: PatternOrList) -> List[Any]:
        logging.info(f"Getting variables (pattern: {pattern})")
        metadata = self.spark._jvm.cern.nxcals.api.extraction.metadata
        variables = metadata.queries.Variables
        variable_service = metadata.ServiceClientFactory.createVariableService()

        if isinstance(pattern, str):
            return list(
                variable_service.findAll(
                    variables.suchThat().variableName().like(pattern)
                )
            )
        elif isinstance(pattern, Iterable):
            return list(
                variable_service.findAll(
                    getattr(variables.suchThat().variableName(), "in")(pattern)
                )
            )
        else:
            raise ValueError(f"{pattern} neither pattern nor list")
