import logging
import warnings
from datetime import datetime
from enum import Enum, IntEnum, auto
from functools import lru_cache
from typing import Any, Dict, List, Optional, Tuple, Union, Generic, TypeVar, Set

import abc

import numpy as np
import pandas as pd
from numpy import ndarray
from nxcals.api.extraction.data.builders import DataQuery
from pandas.core.series import Series
from pyspark.sql import DataFrame, SparkSession, Column

from .fundamentals import FundamentalsManager, FundamentalsType
from .utils import (
    AGGREGATION_TIMESTAMP_FIELD,
    AGGREGATION_VALUE_FIELD,
    TIMESTAMP,
    VALUE,
    VARIABLE_NAME,
    ValueArray,
)
from .utils import (
    PyTimberTimeType,
    PatternOrList,
    VariableDataFrameResultType,
    VariableQueryResultType,
    get_nanos_from,
    to_time_window,
    is_value_array,
)
from .variable import VariableManager, Variable

TIMESTAMPS_KEY = "timestamps"
T = TypeVar("T")


class AutoName(Enum):
    @staticmethod
    def _generate_next_value_(
        name: str, start: int, count: int, last_values: List[Any]
    ) -> Any:
        return name


class ScaleAlgorithm(AutoName):
    MAX = auto()
    MIN = auto()
    AVG = auto()
    COUNT = auto()
    SUM = auto()
    REPEAT = auto()
    INTERPOLATE = auto()

    @staticmethod
    def value_of(value: str) -> "ScaleAlgorithm":
        if value in ScaleAlgorithm.__members__:
            return ScaleAlgorithm[value]

        raise ValueError(
            f"Could not find ScaleAlgorithm {value} among {[d for d in ScaleAlgorithm]}",
        )


class ScaleInterval(IntEnum):
    SECOND = 1
    MINUTE = 60 * SECOND
    HOUR = 60 * MINUTE
    DAY = 24 * HOUR
    WEEK = 7 * DAY
    MONTH = 30 * DAY
    YEAR = 365 * DAY

    @staticmethod
    def value_of(value: str) -> "ScaleInterval":
        if value in ScaleInterval.__members__:
            return ScaleInterval[value]

        raise ValueError(
            f"Could not find ScaleInterval {value} among {[d for d in ScaleInterval]}",
        )


class _Strategy(Enum):
    NEXT = "next"
    LAST = "last"


class DataConverter(Generic[T]):
    @abc.abstractmethod
    def convert_from_get(
        self,
        dataset_dict: Dict[Tuple[str, ...], VariableDataFrameResultType],
        unix_time: bool,
    ) -> Dict[str, T]:
        pass

    @abc.abstractmethod
    def convert_from_get_aligned(
        self,
        dataset_dict: Dict[str, VariableDataFrameResultType],
        master_name: Variable,
        variables: List[Variable],
    ) -> Dict[str, T]:
        pass

    @abc.abstractmethod
    def convert_from_get_scaled(
        self, dataset_dict: Dict[str, VariableDataFrameResultType], unix_time: bool
    ) -> Dict[str, T]:
        pass

    @abc.abstractmethod
    def convert_from_get_as_pivot(
        self,
        dataset: DataFrame,
        unix_time: bool,
    ) -> T:
        pass


class SparkDataConverter(DataConverter[VariableDataFrameResultType]):
    def convert_from_get(
        self,
        dataset_dict: Dict[Tuple[str, ...], VariableDataFrameResultType],
        unix_time: bool,
    ) -> Dict[str, VariableDataFrameResultType]:
        result = {}

        for variables in dataset_dict:
            grouped_ds = dataset_dict[variables]

            for variable_name in variables:
                variable_ds = grouped_ds.filter(
                    grouped_ds.nxcals_variable_name == variable_name
                )
                result[variable_name] = variable_ds
        return result

    def convert_from_get_aligned(
        self,
        dataset_dict: Dict[str, VariableDataFrameResultType],
        master_name: Variable,
        variables: List[Variable],
    ) -> Dict[str, VariableDataFrameResultType]:
        return dataset_dict

    def convert_from_get_scaled(
        self,
        dataset_dict: Dict[str, VariableDataFrameResultType],
        unix_time: bool,
    ) -> Dict[str, VariableDataFrameResultType]:
        return dataset_dict

    def convert_from_get_as_pivot(
        self,
        dataset: DataFrame,
        unix_time: bool,
    ) -> DataFrame:
        return dataset


class NumpyDataConverter(DataConverter[VariableQueryResultType]):
    def convert_from_get(
        self,
        dataset_dict: Dict[Tuple[str], VariableDataFrameResultType],
        unix_time: bool,
    ) -> Dict[str, VariableQueryResultType]:
        result: Dict[str, VariableQueryResultType] = {}

        for variables in dataset_dict:
            ret = (
                self._process_dataset(
                    variables, dataset_dict[variables].sort(TIMESTAMP), unix_time
                )
                if dataset_dict[variables].sort(TIMESTAMP)
                else {}
            )
            result = {**result, **ret}
        return result

    def convert_from_get_aligned(
        self,
        dataset_dict: Dict[str, VariableDataFrameResultType],
        master_variable: Variable,
        variables: List[Variable],
    ) -> Dict[str, ValueArray[Any, Any]]:
        if not dataset_dict or dataset_dict[TIMESTAMPS_KEY].isEmpty():
            return {}

        pandas_df_timestamps = dataset_dict[TIMESTAMPS_KEY].select(TIMESTAMP).toPandas()
        master_name = master_variable.name
        ret = {
            TIMESTAMPS_KEY: NumpyDataConverter._timestamps_to_datetime_np_array(
                pandas_df_timestamps[TIMESTAMP], True
            ),
            master_name: NumpyDataConverter._convert_values_from_spark(
                dataset_dict[master_name], VALUE
            ),
        }

        for var in variables:
            if master_name != var.name:
                variable_ds = dataset_dict[var.name]
                ret[var.name] = NumpyDataConverter._convert_values_from_spark(
                    variable_ds, AGGREGATION_VALUE_FIELD
                )

        return ret

    @staticmethod
    def _convert_values_from_spark(
        ds: DataFrame, column_name: str
    ) -> ValueArray[Any, Any]:
        if is_value_array(ds.schema, column_name):
            return NumpyDataConverter._convert_array_from_spark(ds, column_name)
        else:
            return NumpyDataConverter._convert_scalars_from_spark(ds, column_name)

    @staticmethod
    def _convert_scalars_from_spark(
        ds: DataFrame, column_name: str
    ) -> ValueArray[Any, Any]:
        return ds.select(column_name).toPandas()[column_name].to_numpy()

    @staticmethod
    def _convert_array_from_spark(
        ds: DataFrame, column_name: str
    ) -> ValueArray[Any, Any]:
        return NumpyDataConverter._extract_array_values(
            ds.select(
                f"{column_name}.elements",
                f"{column_name}.dimensions",
            )
            .toPandas()
            .to_numpy()
        )

    def convert_from_get_scaled(
        self, dataset_dict: Dict[str, VariableDataFrameResultType], unix_time: bool
    ) -> Dict[str, VariableQueryResultType]:
        ret = {}

        for var_name in dataset_dict:
            ts_val = dataset_dict[var_name].toPandas()

            ret[var_name] = (
                NumpyDataConverter._timestamps_to_datetime_np_array(
                    ts_val.timestamp, unix_time
                ),
                ts_val.value.to_numpy(),
            )
        return ret

    def convert_from_get_as_pivot(
        self,
        dataset: DataFrame,
        unix_time: bool,
    ) -> Dict[int, Dict[str, Any]]:
        return dataset.rdd.map(
            lambda row: (
                row.nxcals_timestamp,
                {k: row[k] for k in row.asDict() if k != "nxcals_timestamp"},
            )
        ).collectAsMap()

    def _process_dataset(
        self, variables: Tuple[str], ds: DataFrame, unix_time: bool
    ) -> Dict[str, VariableQueryResultType]:
        is_array = is_value_array(ds.schema)

        if is_array:
            vds = ds.select(
                TIMESTAMP,
                f"{VALUE}.elements",
                f"{VALUE}.dimensions",
                "nxcals_variable_name",
            )
        else:
            vds = ds.select(TIMESTAMP, VALUE, "nxcals_variable_name")

        pandas_df = vds.toPandas()
        variable_names_series = pandas_df[VARIABLE_NAME]

        result = {}

        for variable_name in variables:
            variable_ds = pandas_df.loc[variable_names_series == variable_name]

            if not variable_ds.empty:
                if is_array:
                    values = NumpyDataConverter._extract_array_values(
                        variable_ds[["elements", "dimensions"]].to_numpy()
                    )
                else:
                    values = variable_ds[VALUE].to_numpy()

                result[variable_name] = (
                    NumpyDataConverter._timestamps_to_datetime_np_array(
                        variable_ds[TIMESTAMP], unix_time
                    ),
                    values,
                )
        return result

    @staticmethod
    def _timestamps_to_datetime_np_array(
        raw_timestamps: Series, unix_time: bool
    ) -> ValueArray[Any, Any]:
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", FutureWarning)
            datetimes_as_np_array = np.array(
                pd.to_datetime(raw_timestamps, unit="ns", utc=True).dt.to_pydatetime()
            )

        if unix_time:
            timestamps = np.array([x.timestamp() for x in datetimes_as_np_array])
        else:
            timestamps = datetimes_as_np_array
        return timestamps

    @staticmethod
    def _extract_array_values(
        raw_values: ndarray[Any, Any],
    ) -> Union[ndarray[Any, Any], List[ndarray[Any, Any]]]:
        if len(raw_values) == 0:
            return []

        dims = len(raw_values[0][1])

        if dims == 1:
            return raw_values[:, 0]
        elif dims == 2:
            return [
                NumpyDataConverter._extract_matrix_value(value) for value in raw_values
            ]

        raise ValueError(
            f"Max 2D arrays are supported, obtained {dims}D array",
        )

    @staticmethod
    def _extract_matrix_value(value: ndarray[Any, Any]) -> ndarray[Any, Any]:
        data = value[0]
        rows, cols = value[1]

        return np.reshape(data, [rows, cols])


class DataManager:
    def __init__(self, spark: SparkSession):
        self._spark = spark
        self._variable_manager = VariableManager(spark)
        self._funds_manager = FundamentalsManager(spark)
        self._agg_service = DataManager._init_agg_service(
            spark,
            self._funds_manager.fund_service,
        )
        self._agg_mappings = DataManager._init_agg_mappings(spark)

    def _get_as_dataset(
        self,
        pattern: PatternOrList,
        t1: PyTimberTimeType,
        t2: Optional[Union[PyTimberTimeType, str]] = None,
        fundamental: Optional[FundamentalsType] = None,
    ) -> Dict[Tuple[str, ...], VariableDataFrameResultType]:
        if not self._is_valid_time_window(fundamental, t2):
            logging.warning(
                "Unsupported: if filtering by fundamentals you must provide a correct time window",
            )
            return {}

        variables = self._variable_manager.get_variables(pattern)

        if not t2 or t2 == _Strategy.LAST.value:
            datasets = self._get_lookup_data(variables, t1, _Strategy.LAST)
        elif t2 == _Strategy.NEXT.value:
            datasets = self._get_lookup_data(variables, t1, _Strategy.NEXT)
        else:
            datasets = self._get_dataset_for(variables, t1, t2, fundamental)

        result = {
            tuple(var.name for var in variables): ds
            for variable_type, (variables, ds) in datasets.items()
        }

        return result

    def _get_aligned_as_dataset(
        self,
        master_variable: Variable,
        variables: List[Variable],
        t1: PyTimberTimeType,
        t2: PyTimberTimeType,
        fundamental: Optional[FundamentalsType] = None,
    ) -> Dict[str, VariableDataFrameResultType]:
        master_name = master_variable.name
        variable_with_ds = list(
            self._get_dataset_for(
                [master_variable],
                t1,
                t2,
                fundamental,
                to_df=False,
            ).values()
        )

        if not variable_with_ds:
            return {}

        _, master_ds = variable_with_ds[0]

        if not master_ds or master_ds.isEmpty():
            return {}

        master_df = DataFrame(master_ds, self._spark).sort([TIMESTAMP])
        ret: Dict[str, VariableDataFrameResultType] = {
            TIMESTAMPS_KEY: master_df.select(TIMESTAMP),
            master_name: master_df,
        }

        props = self._get_aligned_properties(master_ds)
        for var in variables:
            if master_name != var.name:
                ds = self._agg_service.getData(var.java_object, props)
                df = DataFrame(ds, self._spark)

                df = df.sort(AGGREGATION_TIMESTAMP_FIELD)
                ret[var.name] = df

        return ret

    def _get_scaled_as_dataset(
        self,
        pattern: PatternOrList,
        t1: PyTimberTimeType,
        t2: PyTimberTimeType,
        scale_algorithm: ScaleAlgorithm = ScaleAlgorithm.SUM,
        scale_interval: ScaleInterval = ScaleInterval.MINUTE,
        scale_size: int = 1,
    ) -> Dict[str, VariableDataFrameResultType]:
        variables = self._variable_manager.get_nxcals_variables(pattern)
        if not variables:
            raise ValueError(f"No variables found for given {pattern}")

        props = self._get_scaled_properties(
            t1,
            t2,
            scale_algorithm,
            scale_interval,
            scale_size,
        )
        ret: Dict[str, VariableDataFrameResultType] = {}
        for var in variables:
            var_name = var.getVariableName()
            ds = self._agg_service.getData(var, props)

            ret[var_name] = DataFrame(ds, self._spark).select(
                AGGREGATION_TIMESTAMP_FIELD, AGGREGATION_VALUE_FIELD
            )
        return ret

    def _get_pivot_as_dataset(
        self,
        pattern: PatternOrList,
        t1: PyTimberTimeType,
        t2: Optional[Union[PyTimberTimeType, str]] = None,
    ) -> DataFrame:
        variables = self._variable_manager.get_variables(pattern)
        if not variables:
            raise ValueError(f"No variables found for given {pattern}")

        variable_names_by_system = DataManager._get_variables_names_grouped_by_system(
            variables
        )

        datasets_to_join = [
            DataQuery.getAsPivot(self._spark, t1, t2, system, variable_names)
            for system, variable_names in variable_names_by_system.items()
        ]

        dataset = datasets_to_join[0]
        for other_dataset in datasets_to_join[1:]:
            dataset = dataset.join(other_dataset, TIMESTAMP, "fullouter")

        return dataset

    @staticmethod
    def _get_variables_names_grouped_by_system(
        variables: List[Variable],
    ) -> Dict[str, Set[str]]:
        res = dict()
        for variable in variables:
            value = res.get(variable.system, set())
            value.add(variable.name)
            res[variable.system] = value

        return res

    # ############################### private stuff #############################################

    def _get_dataset_for(
        self,
        variables: List[Variable],
        t1: PyTimberTimeType,
        t2: PyTimberTimeType,
        fundamental: Optional[FundamentalsType] = None,
        to_df: bool = True,
    ) -> Dict[str, Tuple[List[Variable], Union[DataFrame, Any]]]:
        """

        :param variables: list with variables to extract
        :param t1:
        :param t2:
        :param fundamental: optional fundamental to filter by
        :param to_df: if convert to pyspark df
        :return: Dict, where key is name of type, and value is dict variable -> dataset
        """
        if not variables:
            return {}

        variables_grouped_by_type = self._group_variables_by_type(variables, t1, t2)

        datasets_grouped_by_type = {
            result_type: (variables, self._extract_variables(variables, t1, t2, to_df))
            for result_type, variables in variables_grouped_by_type.items()
        }

        if fundamental:
            f_ds = self._funds_manager.get_fundamentals_ds(t1, t2, fundamental, to_df)

            return {
                result_type: (variables, ds.join(f_ds.drop(VARIABLE_NAME), TIMESTAMP))
                for result_type, (variables, ds) in datasets_grouped_by_type.items()
            }

        return datasets_grouped_by_type

    def _extract_variables(
        self,
        variables: Set[Variable],
        t1: PyTimberTimeType,
        t2: PyTimberTimeType,
        to_df: bool = True,
    ) -> Union[DataFrame, Any]:
        t1_nanos = get_nanos_from(t1)
        t2_nanos = get_nanos_from(t2)
        variable_names = [variable.name for variable in variables]
        system = next(iter(variables)).system
        return (
            DataQuery.builder(self._spark)
            .variables()
            .system(system)
            .nameIn(variable_names)
            .timeWindow(t1_nanos, t2_nanos)
            .build(to_df)
        )

    def _group_variables_by_type(
        self, variables: List[Variable], t1: PyTimberTimeType, t2: PyTimberTimeType
    ) -> Dict[str, Set[Variable]]:
        java_variables = {variable.java_object for variable in variables}
        extraction_utils = (
            self._spark._jvm.cern.nxcals.api.extraction.data.ExtractionUtils
        )

        java_output = extraction_utils.groupVariablesByType(
            self._spark._jsparkSession,
            java_variables,
            to_time_window(self._spark, t1, t2),
        )

        return {
            str(entry.getKey()): {
                Variable.from_java_object(variable) for variable in entry.getValue()
            }
            for entry in java_output.entrySet()
        }

    def _group_and_union_by_type(
        self, datasets: Dict[Variable, Union[DataFrame, Any]]
    ) -> Dict[str, Tuple[List[Variable], Union[DataFrame, Any]]]:
        result: Dict[str, Tuple[List[Variable], Union[DataFrame, Any]]] = {}
        for variable, variable_ds in datasets.items():
            result_type = self._get_type(variable_ds)
            if result_type in result:
                variable_list, ds = result[result_type]
                variable_list.append(variable)
                result[result_type] = (
                    variable_list,
                    ds.union(variable_ds.select(ds.columns)),
                )
            else:
                result[result_type] = ([variable], variable_ds)

        return result

    def _get_type(self, ds: Union[DataFrame, Any]) -> str:
        if not isinstance(ds, DataFrame):
            pyspark_ds = DataFrame(ds, self._spark)
        else:
            pyspark_ds = ds

        return str(pyspark_ds.schema[VALUE])

    def _is_valid_time_window(
        self,
        fundamental: Optional[FundamentalsType] = None,
        t2: Optional[PyTimberTimeType] = None,
    ) -> bool:
        if t2 in (_Strategy.NEXT.value, _Strategy.LAST.value, None) and fundamental:
            return False
        return True

    def _get_lookup_data(
        self, variables: List[Variable], t1: PyTimberTimeType, strategy: _Strategy
    ) -> Dict[str, Tuple[List[Variable], Union[DataFrame, Any]]]:
        service = self._spark._jvm.cern.nxcals.api.custom.service.extraction.ExtractionServiceFactory.getInstance(
            self._spark._jsparkSession,
        )
        tw = to_time_window(self._spark, t1, datetime.now())
        props = self._create_extraction_properties(tw, strategy)

        datasets = {
            variable: service.getData(variable.java_object, props)
            for variable in variables
        }

        datasets_without_empty = {
            variable: DataFrame(ds, self._spark)
            for variable, ds in datasets.items()
            if ds
        }

        return self._group_and_union_by_type(datasets_without_empty)

    def _create_extraction_properties(
        self, java_time_window: Any, strategy: _Strategy
    ) -> Any:
        extraction = self._spark._jvm.cern.nxcals.api.custom.service.extraction

        builder = extraction.ExtractionProperties.builder()
        ls = self._get_strategy_for(extraction, strategy)

        return (
            builder.timeWindow(
                java_time_window.getStartTime(),
                java_time_window.getEndTime(),
            )
            .lookupStrategy(ls)
            .build()
        )

    def _get_strategy_for(self, extraction: Any, strategy: _Strategy) -> Any:
        if strategy == _Strategy.LAST:
            return extraction.LookupStrategy.LAST_BEFORE_START_ONLY
        elif strategy == _Strategy.NEXT:
            return extraction.LookupStrategy.NEXT_AFTER_START_ONLY

        raise ValueError(f"{strategy} unknown strategy")

    def _get_master_and_variables_for(
        self,
        pattern: PatternOrList,
        master: Optional[str] = None,
    ) -> Tuple[Variable, List[Variable]]:
        variables = self._variable_manager.get_variables(pattern)
        if not variables:
            raise ValueError(f"No variables found for given {pattern}")

        master_variable = (
            self._variable_manager.get_variables(master)[0] if master else variables[0]
        )

        if not master_variable:
            raise ValueError("Cannot obtain a master variable")

        return master_variable, variables

    def _get_aligned_properties(self, master_ds: Any) -> Any:
        builder = self._spark._jvm.cern.nxcals.api.custom.service.aggregation.DatasetAggregationProperties.builder()
        return builder.drivingDataset(master_ds).build()

    def _get_scaled_properties(
        self,
        t1: PyTimberTimeType,
        t2: PyTimberTimeType,
        scale_algorithm: ScaleAlgorithm,
        scale_interval: ScaleInterval,
        scale_size: int,
    ) -> Any:
        builder = self._spark._jvm.cern.nxcals.api.custom.service.aggregation.WindowAggregationProperties.builder()
        builder.function(self._agg_mappings[scale_algorithm]).timeWindow(
            to_time_window(self._spark, t1, t2)
        ).interval(
            scale_interval.value * scale_size,
            self._spark._jvm.java.time.temporal.ChronoUnit.SECONDS,
        )

        return builder.build()

    @staticmethod
    def _init_agg_service(spark: SparkSession, fund_service: Any) -> Any:
        factory = spark._jvm.cern.nxcals.api.custom.service.aggregation.AggregationServiceFactory
        return factory.getInstance(spark._jsparkSession, fund_service)

    @staticmethod
    @lru_cache(maxsize=None)
    # if the function is called multiple times with the same spark argument, the cached result will be returned
    # instead of recomputing the mappings
    def _init_agg_mappings(spark: SparkSession) -> Dict[ScaleAlgorithm, Any]:
        package = spark._jvm.cern.nxcals.api.custom.service.aggregation
        return {
            ScaleAlgorithm.SUM: package.AggregationFunctions.SUM,
            ScaleAlgorithm.MIN: package.AggregationFunctions.MIN,
            ScaleAlgorithm.MAX: package.AggregationFunctions.MAX,
            ScaleAlgorithm.COUNT: package.AggregationFunctions.COUNT,
            ScaleAlgorithm.AVG: package.AggregationFunctions.AVG,
            ScaleAlgorithm.REPEAT: package.AggregationFunctions.REPEAT,
            ScaleAlgorithm.INTERPOLATE: package.AggregationFunctions.INTERPOLATE,
        }
