import warnings
from datetime import datetime
from typing import Any, Dict, List, Mapping, Optional, Tuple, Union, TypeVar, Generic

from pyspark.sql import SparkSession
from pytimber.contexts import ContextManager
from pytimber.hierarchy import HierarchyManager
from pytimber.snapshots import SnapshotManager

from .data import (
    DataManager,
    ScaleAlgorithm,
    ScaleInterval,
    NumpyDataConverter,
    SparkDataConverter,
    DataConverter,
)
from .fills import FillInterval, FillManager
from .fundamentals import (
    Fundamentals,
    FundamentalsManager,
    FundamentalsType,
    FundamentalVariable,
)
from .spark_session_utils import _get_spark_session
from .stats import StatsManager, Statistics
from .utils import (
    DEFAULT_HIERARCHY_NXCALS_SYSTEM,
    OLDEST_CONTEXT_TIMESTAMP,
    PatternOrList,
    PyTimberTimeType,
    TimestampArray,
    TracingUtils,
    ValueArray,
    VariableDataFrameResultType,
    VariableQueryResultType,
    deprecated,
    log_function_call,
    verify_unix_time_is_set,
)
from .variable import VariableManager

from cmw_tracing import LogTracer, CMWEnvironment, set_tracing_environment

Stat = Statistics
NXCALS_PRO_SERVICE_URL = "https://cs-ccr-nxcals6.cern.ch:19093,https://cs-ccr-nxcals7.cern.ch:19093,https://cs-ccr-nxcals8.cern.ch:19093"

T = TypeVar("T")


class AbstractLoggingDB(Generic[T]):
    def __init__(
        self,
        data_converter: DataConverter[T],
        appid: str = "PYTIMBER4",
        clientid: str = "PYTIMBER3",
        source: str = "nxcals",
        loglevel: Optional[Union[str, int]] = None,
        sparkconf: Optional[str] = None,
        sparkprops: Optional[Mapping[str, str]] = None,
        kerberosprincipal: Optional[str] = None,
        kerberoskeytab: Optional[str] = None,
        data_location: str = "pro",
        spark_session: Optional[SparkSession] = None,
        service_url: str = NXCALS_PRO_SERVICE_URL,
        sparkloglevel: Optional[str] = "ERROR",
    ):
        """
        Initializes a new instance of the PyTimber client.

        Args:
            appid (str, optional): The name of the PyTimber application. Default is 'PYTIMBER4'.
            clientid (str, optional): Obsolete argument kept for backward compatibility with no effect
                on the current version. Previous meaning: the name of the PyTimber client. Default was 'PYTIMBER3'.
            source (str, optional): Obsolete argument kept for backward compatibility with no effect
                on the current version. Previous meaning: the name of the data source. Default was (always) 'nxcals'.
            loglevel (Union[str, int], optional): Obsolete argument kept for backward compatibility with no effect
                on the current version. Previous meaning: the logging level. Default is None.
            sparkconf (str, optional): The pre-configuration name for Apache Spark (can be: "small", "medium", "large").
                Default is None.
            sparkprops (Mapping[str, str], optional): A dictionary of Spark properties. Default is None.
            kerberosprincipal (str, optional): The Kerberos principal. Default is None.
            kerberoskeytab (str, optional): The path to the Kerberos keytab file. Default is None.
            data_location (str, optional): Obsolete argument kept for backward compatibility with no effect
                on the current version. Please make us of service_url.
                Previous meaning: the location of the data. Default was 'pro'
            spark_session (SparkSession, optional): A SparkSession instance. Default is None.
            service_url (str): The URL of the NXCALS service. Default is pointing to PRO environment.
            sparkloglevel (Union[str, int], optional): the Spark logging level. Default is ERROR. Valid log levels include:
                ALL, DEBUG, ERROR, FATAL, INFO, OFF, TRACE, WARN

        """

        if loglevel is not None:
            self._deprecated_warning("loglevel")
        if data_location is not None:
            self._deprecated_warning("data_location")
        if kerberosprincipal is not None:
            self._deprecated_warning(
                "kerberosprincipal",
                "Please add spark.kerberos.principal to sparkprops.",
            )
        if kerberoskeytab is not None:
            self._deprecated_warning(
                "kerberoskeytab", "Please add spark.kerberos.keytab to sparkprops."
            )

        self._spark = spark_session or _get_spark_session(
            app_name=appid,
            service_url=service_url,
            flavor=sparkconf,
            conf=sparkprops,
        )

        if sparkloglevel:
            self._spark.sparkContext.setLogLevel(sparkloglevel)

        self._fill_manager = FillManager(self._spark)
        self._variable_manager = VariableManager(self._spark)
        self._hierarchy_manager = HierarchyManager(self._spark)
        self._snapshot_manager = SnapshotManager(self._spark)
        self._context_manager = ContextManager(self._spark)
        self._stat_manager = StatsManager(self._spark)
        self._funds_manager = FundamentalsManager(self._spark)
        self._data_manager = DataManager(self._spark)

        set_tracing_environment(CMWEnvironment.PRO)
        self._tracer = LogTracer("NXCALS", appid, service_url, "PyTimber")
        self._tracer.info(TracingUtils.get_all_info())

        self.data_converter = data_converter

    # ################# Variable stuff ############################
    @deprecated("Use search_variables instead")
    def search(self, pattern: PatternOrList) -> List[str]:
        return self.search_variables(pattern)

    @log_function_call
    def search_variables(self, pattern: PatternOrList) -> List[str]:
        """
        Searches for variable names that match the given pattern.

        Args:
            pattern: A string or iterable of strings to search for in variable names.

        Returns:
            A list of variable names matching the search pattern.

        """
        variables = self._variable_manager.get_variables(pattern)
        return [v.name for v in variables]

    @deprecated("Use get_variable_description instead")
    def getDescription(  # NOSONAR
        self,
        pattern: PatternOrList,
    ) -> Dict[str, str]:
        return self.get_variable_description(pattern)

    @log_function_call
    def get_variable_description(self, pattern: PatternOrList) -> Dict[str, str]:
        """
        Gets description(s) for the given variable pattern.

        Args:
            pattern: A pattern or list of names to search for variables

        Returns:
            Dict: A dictionary of variable names and descriptions matching the search pattern.

        """
        variables = self._variable_manager.get_variables(pattern)
        return {v.name: v.description for v in variables}

    @deprecated("Use get_variable_unit instead")
    def getUnit(  # NOSONAR
        self,
        pattern: PatternOrList,
    ) -> Dict[str, str]:
        return self.get_variable_unit(pattern)

    @log_function_call
    def get_variable_unit(self, pattern: PatternOrList) -> Dict[str, str]:
        """
        Gets unit(s) for the given variable pattern.

        Args:
            pattern: A pattern or list of names to search for in variable names.

        Returns:
            Dict: A dictionary of variable names and units matching the search pattern.

        """
        variables = self._variable_manager.get_variables(pattern)
        return {v.name: v.unit for v in variables}

    @deprecated("Use get_variable_origin instead")
    def getVariablesOrigin(  # NOSONAR
        self,
        pattern_or_list: PatternOrList,
    ) -> Dict[str, str]:
        return self.get_variable_origin(pattern_or_list)

    @log_function_call
    def get_variable_origin(self, pattern: PatternOrList) -> Dict[str, str]:
        """
        Gets the system origin(s) for the given variable pattern.

        Args:
            pattern (PatternOrList): A pattern or list of names to search for variables.

        Returns:
            Dict[str, str]: A dictionary with variable name as key and system origin as value.

        """
        variables = self._variable_manager.get_variables(pattern)
        return {v.name: v.system for v in variables}

    # ################# Fill stuff ############################
    @deprecated("Use get_lhc_fill_data instead")
    def getLHCFillData(  # NOSONAR
        self,
        fill_number: Optional[int] = None,
        unixtime: bool = True,
    ) -> Optional[Dict[str, Any]]:
        verify_unix_time_is_set(unixtime)

        return self.get_lhc_fill_data(fill_number)

    @log_function_call
    def get_lhc_fill_data(
        self, fill_number: Optional[int] = None
    ) -> Optional[Dict[str, Any]]:
        """
        Gets times and beam modes for a particular LHC fill. Parameter fill_number can be an integer to get
        a particular fill or None to get the last completed fill.

        Args:
            fill_number: The fill number to retrieve data for. Defaults to None.

        Returns:
            A dictionary containing the following keys: fillNumber, startTime,
            endTime, beamModes (with mode, startTime and endTime) for the specified fill number.
            Returns None if no data is found for the given fill number.

        """
        return self._fill_manager.get_lhc_fill_data(fill_number)

    @deprecated("Use get_lhc_fills_by_time instead")
    def getLHCFillsByTime(  # NOSONAR
        self,
        t1: PyTimberTimeType,
        t2: PyTimberTimeType,
        beam_modes: Optional[PatternOrList] = None,
        unixtime: bool = True,
    ) -> List[Dict[str, Any]]:
        verify_unix_time_is_set(unixtime)
        return self.get_lhc_fills_by_time(t1, t2, beam_modes)

    @log_function_call
    def get_lhc_fills_by_time(
        self,
        from_time: PyTimberTimeType,
        to_time: PyTimberTimeType,
        beam_modes: Optional[PatternOrList] = None,
    ) -> List[Dict[str, Any]]:
        """
        Returns a list of LHC fills data that occurred within the specified time range.

        Args:
            from_time: The start time of the period of interest. Can be a string, datetime object,
                datetime64 object or int.
            to_time: The end time of the period of interest. Can be a string, datetime object,
                datetime64 object or int.
            beam_modes: A pattern or list of patterns to filter the beam modes to be included
                in the result. If None, all beam modes are included. Default is None.

        Returns:
            A list of dictionaries representing the LHC fills data that occurred within the
            specified time range. Each dictionary contains the following keys: fillNumber, startTime,
            endTime, beamModes (with mode, startTime and endTime).

        """
        return self._fill_manager.get_lhc_fills_by_time(from_time, to_time, beam_modes)

    @deprecated("Use get_interval_by_lhc_modes instead")
    def getIntervalsByLHCModes(  # NOSONAR
        self,
        t1: PyTimberTimeType,
        t2: PyTimberTimeType,
        mode1: str,
        mode2: str,
        unixtime: bool = True,
        mode1time: str = "startTime",
        mode2time: str = "endTime",
        mode1idx: int = 0,
        mode2idx: int = -1,
    ) -> List[FillInterval]:
        verify_unix_time_is_set(unixtime)
        return self.get_interval_by_lhc_modes(
            t1, t2, mode1, mode2, mode1time, mode2time, mode1idx, mode2idx
        )

    @log_function_call
    def get_interval_by_lhc_modes(
        self,
        t1: PyTimberTimeType,
        t2: PyTimberTimeType,
        mode1: str,
        mode2: str,
        mode1time: str = "startTime",
        mode2time: str = "endTime",
        mode1idx: int = 0,
        mode2idx: int = -1,
    ) -> List[FillInterval]:
        """
        Returns a list of FillInterval objects for the given time window and two LHC beam modes

        The optional parameters 'mode[12]time' can take 'startTime' or
        'endTime' and select which time to use for the given mode.
        The optional parameter 'mode[12]idx' selects which mode should be used in case of multiple occurrence of
        the given mode.

        Args:
            t1: The start time of the interval. Can be a string, datetime object, datetime64 object or int.
            t2: The end time of the interval. Can be a string, datetime object, datetime64 object or int.
            mode1: The name of the first beam mode ("SETUP", STABLE" etc...).
            mode2: The name of the second beam mode.
            mode1time: The attribute name ("startTime" or "endTime") of the time for mode1,
                defaults to "startTime".
            mode2time: The attribute name ("startTime" or "endTime") of the time for mode2,
             defaults to "endTime".
            mode1idx: The index of the occurrence of the first beam mode
                to use. Defaults to 0.
            mode2idx: The index of the occurrence of the second beam mode
                to use. Defaults to -1.

        Returns:
            A list of FillInterval objects containing the fill number and the start and end times
            for mode1 and mode2.
        """
        return self._fill_manager.get_interval_by_lhc_modes(
            t1,
            t2,
            mode1,
            mode2,
            mode1time,
            mode2time,
            mode1idx,
            mode2idx,
        )

    # ################# Stats stuff ############################
    @deprecated("Use get_variable_stats instead")
    def getStats(  # NOSONAR
        self,
        pattern_or_list: PatternOrList,
        t1: PyTimberTimeType,
        t2: PyTimberTimeType,
        unixtime: bool = True,
    ) -> Dict[str, Stat]:
        return self.get_variable_stats(pattern_or_list, t1, t2, unixtime)

    @log_function_call
    def get_variable_stats(
        self,
        pattern: PatternOrList,
        time_from: PyTimberTimeType,
        time_to: PyTimberTimeType,
        unix_time: bool = True,
    ) -> Dict[str, Stat]:
        """
        Gets variable statistics for the specified time range.

        Args:
            pattern: A pattern or list of names to search for variables
            time_from: A time range lower bound in str, datetime, datetime64, int, int64.
            time_to: A time range upper bound in str, datetime, datetime64, int, int64.
            unix_time: format of time for MinTstamp and MaxTstamp. If true then represented as float otherwise datetime.

        Returns:
            A dictionary mapping variable names to StatTuple objects containing the following statistics:
            - MinTstamp: the earliest timestamp of data for the variable in the given time range.
            - MaxTstamp: the latest timestamp of data for the variable in the given time range.
            - ValueCount: the number of data points for the variable in the given time range.
            - MinValue: the minimum data value for the variable in the given time range.
            - MaxValue: the maximum data value for the variable in the given time range.
            - AvgValue: the average data value for the variable in the given time range.
            - StandardDeviationValue: the standard deviation of data values for the variable in the given time range.
        """
        variables = self._variable_manager.get_variables(pattern)
        return self._stat_manager.get_stats(variables, time_from, time_to, unix_time)

    # ################# Fundamentals stuff ############################
    @deprecated("Use get_fundamentals instead")
    def getFundamentals(  # NOSONAR
        self,
        t1: PyTimberTimeType,
        t2: PyTimberTimeType,
        fundamental: FundamentalsType,
    ) -> List[FundamentalVariable]:
        return self.get_fundamentals(t1, t2, fundamental)

    @log_function_call
    def get_fundamentals(
        self,
        t1: PyTimberTimeType,
        t2: PyTimberTimeType,
        pattern: FundamentalsType,
    ) -> List[FundamentalVariable]:
        """
        Gets list of `FundamentalVariable` objects between two timestamps for a given pattern.

        Args:
            t1: The start timestamp of the query, as str, datetime, datetime64, timedelta, int, int64.
            t2: The end timestamp of the query, as str, datetime, datetime64, timedelta, int, int64.
            pattern: The pattern to search for, which can be a string or a `Fundamentals` object,
            for example:
                "ACCELERATOR:CYCLE%:USER%"
                Fundamentals("ACCELERATOR:CYCLE%:USER%", ["DEST0"])

        Returns:
            A list of `FundamentalVariable` objects matching the query pattern.
        """
        return self._funds_manager.get_fundamentals(t1, t2, pattern)

    @deprecated("Use search_fundamentals instead")
    def searchFundamental(  # NOSONAR
        self,
        fundamental: FundamentalsType,
        t1: PyTimberTimeType,
        t2: Optional[PyTimberTimeType] = None,
    ) -> List[str]:
        return self.search_fundamentals(fundamental, t1, t2)

    @log_function_call
    def search_fundamentals(
        self,
        fundamental: FundamentalsType,
        t1: PyTimberTimeType,
        t2: Optional[PyTimberTimeType] = None,
    ) -> List[str]:
        """
        Searches for fundamental variables names that match the specified pattern within the specified time range.

        Args:
            fundamental: A string or a `Fundamentals` object representing the pattern to search for,
            for example:
                "ACCELERATOR:CYCLE%:USER%"
                Fundamentals("ACCELERATOR:CYCLE%:USER%", ["DEST0"])
            t1: The start time of the search range, as str, datetime, datetime64, timedelta, int, int64.
            t2: The end time of the search range, as str, datetime, datetime64, timedelta, int, int64.
                Defaults to the current time if not specified.

        Returns:
            A list of names of the matching fundamental variables.

        """
        to_time = t2 or datetime.now()
        funds = self.get_fundamentals(t1, to_time, fundamental)
        return [f.name for f in funds]

    # ################# Vectors' metadata ############################
    @deprecated("Use get_meta_data instead")
    def getMetaData(  # NOSONAR
        self,
        pattern_or_list: PatternOrList,
    ) -> Dict[str, Tuple[List[float], List[Any]]]:
        return self.get_meta_data(pattern_or_list)

    @log_function_call
    def get_meta_data(
        self, pattern: PatternOrList
    ) -> Dict[str, Tuple[List[float], List[Any]]]:
        """
        Retrieves metadata for vector variables (so called vector contexts) matching a pattern.
        A context represents vector elements names. This information can changed over the time.

        Args:
            pattern: A pattern or list of names to search for vector variables.

        Returns:
            A dictionary with variable names as keys and tuples of vector context lists as values.
            The metadata tuple contains two lists: the first contains timestamps (in seconds since the Unix epoch)
            and the second contains context values.

        """
        now = datetime.now()
        variables = self._variable_manager.get_variables(pattern)
        ret = {}
        for var in variables:
            metadata = self._context_manager.get_metadata_for(
                var,
                OLDEST_CONTEXT_TIMESTAMP,
                now,
            )
            if metadata:
                ret[var.name] = (
                    [m[0] for m in metadata],
                    [m[1] for m in metadata],
                )
        return ret

    @log_function_call
    def get(
        self,
        pattern_or_list: PatternOrList,
        t1: PyTimberTimeType,
        t2: Optional[Union[PyTimberTimeType, str]] = None,
        fundamentals: Optional[FundamentalsType] = None,
        unixtime: bool = True,
        fundamental: Optional[str] = None,
    ) -> Dict[str, T]:
        """
        Query for a list of variables or for variables whose
        name matches a pattern (string) in a time window from t1 to t2 and return corresponding variable(s) data.

        Args:
            pattern_or_list: A pattern or list of names to search for variables.
            t1: The start time of the time window, as a str, datetime, datetime64, timedelta, int, int64.
            t2: The optional end time of the time window, as a str, datetime, datetime64,
               timedelta, int, int64 or strategy: last or next.

                If t2 is missing, None, "last", the last data point before t1 is given
                If t2 is "last", the last data point before t1 is given.
                If t2 is "next", the first data point after t1 is given.
                If t2 is a timestamp or a datetime object, the data within the time window [t1, t2] is returned.
                Default value is None.

            fundamentals: A fundamentals object, to filter data points based on the fundamentals

                If neither pattern nor Fundamentals objects is provided for the fundamental all the data is returned.

                If a fundamentals value is provided, the end of the time window has to
                be explicitly provided.

            fundamental: pattern used for the fundamental filtering.

                This parameter is deprecated, fundamentals should be used instead.

            unix_time: Whether to return POSIX timestamps as float values.
                If `True`, return POSIX timestamps. If `False`, return datetime objects.
                Default value is `True`.

        Returns:
            A dictionary of data points, with variable names as keys and tuple of arrays as values.
            If `unix_time` is `True`, the timestamps are in POSIX format. Otherwise, they are `datetime` objects.
            If the query has no result, an empty dictionary is returned.

        """
        if fundamentals is not None and fundamental is not None:
            raise ValueError(
                "Only one parameter for fundamentals filtering can be provided at a time."
            )

        if fundamental is not None:
            warnings.warn(
                "fundamental is a deprecated input parameter, use fundamentals instead."
            )
            fundamentals = Fundamentals(fundamental)

        return self.data_converter.convert_from_get(
            dataset_dict=self._data_manager._get_as_dataset(
                pattern_or_list, t1, t2, fundamentals
            ),
            unix_time=unixtime,
        )

    @deprecated("Use get_variable instead")
    def getVariable(  # NOSONAR
        self,
        variable: str,
        t1: PyTimberTimeType,
        t2: Optional[PyTimberTimeType] = None,
        fundamental: Optional[FundamentalsType] = None,
        unixtime: bool = True,
    ) -> T:
        return self.get_variable(variable, t1, t2, fundamental, unixtime)

    @log_function_call
    def get_variable(
        self,
        variable: str,
        t1: PyTimberTimeType,
        t2: Optional[Union[PyTimberTimeType, str]] = None,
        fundamental: Optional[FundamentalsType] = None,
        unix_time: bool = True,
    ) -> T:
        """
        Query for a specific variable within a time window and with the option to filter by fundamentals and return
        corresponding variable data. Similar to get method. Refer to get documentation for more details.

        Args:
            variable: The variable name, as a string.
            t1: The start time of the time window, as a str, datetime, datetime64, timedelta, int, int64.
            t2: The end time of the time window, as a str, datetime, datetime64, timedelta, int, int64
                or a strategy.
            fundamental: An optional fundamentals object, to filter data points
                based on the fundamentals.
            unix_time: Whether to return POSIX timestamps as float values.
                If True, return POSIX timestamps. If False, return datetime objects. Default value is True.

        Returns:
            A tuple containing the arrays of timestamps and values for the queried variable.
            If `unix_time` is `True`, the timestamps are in POSIX format. Otherwise, they are `datetime` objects.
            If the query has no result, an empty tuple is returned.

        """
        return self.get(variable, t1, t2, fundamental, unix_time)[variable]

    @deprecated("Use get_scaled instead")
    def getScaled(  # NOSONAR
        self,
        pattern_or_list: PatternOrList,
        t1: PyTimberTimeType,
        t2: PyTimberTimeType,
        unixtime: bool = True,
        scaleAlgorithm: str = "SUM",  # NOSONAR
        scaleInterval: str = "MINUTE",  # NOSONAR
        scaleSize: Union[int, str] = 1,  # NOSONAR
    ) -> Dict[str, T]:
        return self.get_scaled(
            pattern_or_list,
            t1,
            t2,
            unixtime,
            ScaleAlgorithm.value_of(scaleAlgorithm),
            ScaleInterval.value_of(
                scaleInterval,
            ),
            scaleSize if isinstance(scaleSize, int) else int(scaleSize),
        )

    @log_function_call
    def get_scaled(
        self,
        pattern: PatternOrList,
        t1: PyTimberTimeType,
        t2: PyTimberTimeType,
        unix_time: bool = True,
        scale_algorithm: ScaleAlgorithm = ScaleAlgorithm.SUM,
        scale_interval: ScaleInterval = ScaleInterval.MINUTE,
        scale_size: int = 1,
    ) -> Dict[str, T]:
        """
        Retrieves the scaled data for the given pattern of variables in a time window from t1 to t2.

        If no pattern if given for the fundamental all the data are returned.

        If a fundamental pattern is provided, the end of the time window has to
        be explicitly provided.

        Applies the scaling with supplied scaleAlgorithm, scaleSize, and scaleInterval.

        Args:
            pattern: A pattern or list of names to search for variables.
            t1: The start time of the time window, as a timestamp or a datetime object.
            t2: The end time of the time window, as a timestamp or a datetime object.
            unix_time: Whether to return POSIX timestamps as float values. If True, return POSIX timestamps.
                If False, return datetime objects. Default value is True.
            scale_algorithm: The scaling algorithm to apply. Default is "ScaleAlgorithm.SUM".
            scale_interval: The interval on which to apply the scaling.
                Default is "ScaleInterval.MINUTE".
            scale_size: The size of the scaling interval, as an integer.
                Default value is 1.

        Returns:
            A dictionary of data points, with variable names as keys and tuple of arrays as values.
            If `unix_time` is `True`, the timestamps are in POSIX format. Otherwise, they are `datetime` objects.

            If the query has no result, an empty dictionary is returned.
        """
        return self.data_converter.convert_from_get_scaled(
            self._data_manager._get_scaled_as_dataset(
                pattern, t1, t2, scale_algorithm, scale_interval, scale_size
            ),
            unix_time,
        )

    @deprecated("Use get aligned instead")
    def getAligned(  # NOSONAR
        self,
        pattern_or_list: PatternOrList,
        t1: PyTimberTimeType,
        t2: PyTimberTimeType,
        fundamental: Optional[FundamentalsType] = None,
        master: Optional[str] = None,
        unixtime: Optional[bool] = True,
    ) -> Dict[str, T]:
        verify_unix_time_is_set(unixtime)
        return self.get_aligned(pattern_or_list, t1, t2, fundamental, master)

    @log_function_call
    def get_aligned(
        self,
        pattern: PatternOrList,
        t1: PyTimberTimeType,
        t2: PyTimberTimeType,
        fundamental: Optional[FundamentalsType] = None,
        master: Optional[str] = None,
    ) -> Dict[str, T]:
        """
        Retrieves the aligned data for the given pattern of variables within the specified time range [t1, t2],
        based on the fundamental and master variable.

        Args:
            pattern: A pattern or list of names to search for variables.
                In case a master variable is not explicitly specified a first variable from the list
                is considered as a master.
            t1: The start time of the time range in ISO 8601 format or Unix time.
            t2: The end time of the time range in ISO 8601 format or Unix time.
            fundamental: An optional list of fundamentals to include in the query.
            master: The optional name of the designated master variable to which data is aligned.

        Returns:
            A dictionary with the aligned timestamps and values of the requested variables.
        """
        master_variable, variables = self._data_manager._get_master_and_variables_for(
            pattern, master
        )

        return self.data_converter.convert_from_get_aligned(
            self._data_manager._get_aligned_as_dataset(
                master_variable, variables, t1, t2, fundamental
            ),
            master_variable,
            variables,
        )

    @deprecated("Use get_children_for_hierarchies instead")
    def getChildrenForHierarchies(  # NOSONAR
        self,
        pattern_or_list: PatternOrList,
        system: str = DEFAULT_HIERARCHY_NXCALS_SYSTEM,
    ) -> Dict[str, List[str]]:
        return self.get_children_for_hierarchies(pattern_or_list, system)

    @log_function_call
    def get_children_for_hierarchies(
        self,
        pattern_or_list: PatternOrList,
        system: str = DEFAULT_HIERARCHY_NXCALS_SYSTEM,
    ) -> Dict[str, List[str]]:
        """
        Get a list of children hierarchy paths for given parent hierarchy paths.
        Wildcard for the pattern is '%'.

        Args:
            pattern_or_list: pattern for the selection of parent node paths
                or a list of parent node paths
            system: under which hierarchies are defined (default CERN)
        Returns:
            Dictionary of hierarchy paths with corresponding list of children hierarchy paths

        """
        result = {}

        for h in self._hierarchy_manager.get_hierarchies(pattern_or_list, system):
            result[h.getNodePath()] = [c.getNodePath() for c in h.getChildren()]

        return result

    @deprecated("Use get_variables_for_hierarchies instead")
    def getVariablesForHierarchies(  # NOSONAR
        self,
        pattern_or_list: PatternOrList,
        system: str = DEFAULT_HIERARCHY_NXCALS_SYSTEM,
    ) -> Dict[str, List[str]]:
        return self.get_variables_for_hierarchies(pattern_or_list, system)

    @log_function_call
    def get_variables_for_hierarchies(
        self,
        pattern_or_list: PatternOrList,
        system: str = DEFAULT_HIERARCHY_NXCALS_SYSTEM,
    ) -> Dict[str, List[str]]:
        """
        Get a list of variables attached to hierarchies.
        Wildcard for the pattern is '%'.

        Args:
            pattern_or_list: pattern for the selection of hierarchy paths or a list of hierarchy paths
            system: under which hierarchies are defined (default CERN)
        Returns:
            Dictionary of hierarchy paths with corresponding list of attached variables
        """
        result = {}
        for h in self._hierarchy_manager.get_hierarchies(pattern_or_list, system):
            result[h.getNodePath()] = [
                v.getVariableName()
                for v in self._hierarchy_manager.get_variables(h.getId())
            ]
        return result

    @deprecated("Use get_hierarchies_for_variables instead")
    def getHierarchiesForVariables(  # NOSONAR
        self,
        pattern_or_list: PatternOrList,
    ) -> Dict[str, List[str]]:
        return self.get_hierarchies_for_variables(pattern_or_list)

    @log_function_call
    def get_hierarchies_for_variables(
        self, pattern_or_list: PatternOrList
    ) -> Dict[str, List[str]]:
        """
        Get a list of hierarchy paths for variables (in which given variables are defined).
        Wildcard for the pattern is '%'.

        Args:
            pattern_or_list: pattern for the selection of variables names or a list of variables names.
        Returns:
            Dictionary of variables with corresponding list of hierarchy paths

        """
        variables = self._variable_manager.get_variables(pattern_or_list)
        result = {}

        for v in variables:
            hierarchies = self._hierarchy_manager.get_hierarchies_for_variable(
                v.id,
            )
            result[v.name] = [h.getNodePath() for h in hierarchies]

        return result

    @deprecated("Use get_snapshot_names instead")
    def getSnapshotNames(  # NOSONAR
        self,
        pattern_or_list: Union[str, List[str], Tuple[str]],
        owner_pattern: str = "%",
        description_pattern: str = "%",
    ) -> List[str]:
        return self.get_snapshot_names(
            pattern_or_list, owner_pattern, description_pattern
        )

    @log_function_call
    def get_snapshot_names(
        self,
        pattern_or_list: PatternOrList,
        owner_pattern: str = "%",
        description_pattern: str = "%",
    ) -> List[str]:
        """
        Get a list of snapshots names based on a list of strings or a pattern,
        filtered by owner pattern and description pattern.

        Wildcard for the pattern is '%'.

        Args:
            pattern_or_list: pattern for the selection of snapshot names or a list of snapshot names
            owner_pattern: pattern for the filtering by owner name
            description_pattern: pattern for the filtering by snapshot description

        Returns:
            A list of snapshot names

        """
        return self._snapshot_manager.get_snapshot_names(
            pattern_or_list, owner_pattern, description_pattern
        )

    @deprecated("Use get_variables_for_snapshots instead")
    def getVariablesForSnapshots(  # NOSONAR
        self,
        pattern_or_list: PatternOrList,
        owner_pattern: str = "%",
    ) -> Dict[str, List[str]]:
        return self.get_variables_for_snapshots(pattern_or_list, owner_pattern)

    @log_function_call
    def get_variables_for_snapshots(
        self,
        pattern_or_list: PatternOrList,
        owner_pattern: str = "%",
    ) -> Dict[str, List[str]]:
        """
        Get variables attached to snapshots.
        Wildcard for the pattern is '%'.

        Args:
            pattern_or_list: pattern for the selection of snapshot names or a list of snapshot names
            owner_pattern: pattern for the filtering by owner name

        Returns:
           Dictionary of snapshots with attached list of variable names

        """
        return self._snapshot_manager.get_variables_for_snapshots(
            pattern_or_list, owner_pattern
        )

    @deprecated("Use get_data_using_snapshots instead")
    def getDataUsingSnapshots(  # NOSONAR
        self,
        pattern_or_list: PatternOrList,
        owner_pattern: str = "%",
        unixtime: bool = True,
        search_criteria: Optional[Dict[str, str]] = None,
        t1: PyTimberTimeType = None,
        t2: Optional[Union[PyTimberTimeType, str]] = None,
    ) -> Dict[str, Dict[str, T]]:
        return self.get_data_using_snapshots(
            pattern_or_list, owner_pattern, unixtime, search_criteria, t1, t2
        )

    @log_function_call
    def get_data_using_snapshots(
        self,
        pattern_or_list: PatternOrList,
        owner_pattern: str = "%",
        unix_time: bool = True,
        search_criteria: Optional[Dict[str, str]] = None,
        t1: PyTimberTimeType = None,
        t2: Optional[Union[PyTimberTimeType, str]] = None,
    ) -> Dict[str, Dict[str, T]]:
        """
        Get data for variables attached to snapshots which are selected by provided list of strings or pattern
        in a time window defined in the snapshot configuration

        Args:
            pattern_or_list: pattern for the selection of snapshot names or a list of snapshot names
            owner_pattern: pattern for the filtering by owner name
            unix_time: Whether to return POSIX timestamps as float values. If True, return POSIX timestamps.
                If False, return datetime objects. Default value is True.
            search_criteria: a dictionary representing snapshot properties, for example:
                {
                    'getTimeZone': 'UTC_TIME',
                    'isEndTimeDynamic': 'false',
                    'getEndTime': '2050-01-01 00:00:00.000',
                    'getStartTime':	'2023-01-01 00:00:00.000',
                    'fundamentalFilter': f'{{"accelerator": "ACC", "lsaCycle": "CYCLE%", "timingUser": "USER%"}}',
                },
            t1: The start time of the time window, as a str, datetime, datetime64, timedelta, int, int64.
                Default value is None.
            t2: The end time of the time window, as a str, datetime, datetime64, timedelta, int, int64
                or a strategy.

                Cannot specify search_criteria and t1/t2 at the same time.

                If t1/t2 specified, it overrides the time range calculated from the stored snapshot definition.
                If t2 is missing, None, "last", the last data point before t1 is given
                If t2 is "last", the last data point before t1 is given.
                If t2 is "next", the first data point after t1 is given.
                If t2 is a timestamp or a datetime object, the data within the time window [t1, t2] is returned.
                Default value is None.

        Returns:
            A dictionary of snapshots names having dictionaries of variables
            with corresponding data for a given time period

        """
        start_time: PyTimberTimeType = 0
        end_time: Optional[PyTimberTimeType] = None
        fundamental_pattern = None

        if search_criteria:
            if t1 or t2:
                raise ValueError(
                    "Cannot specify t1/t2 when search_criteria is present."
                )

            fundamental_pattern, start_time, end_time = (
                self._snapshot_manager.create_snapshot_search_criteria(
                    **search_criteria
                )
            )
        elif t2 and t1 is None:
            raise ValueError("Cannot specify t2 without providing a value for t1.")

        snapshot_list = self._snapshot_manager.get_snapshots(
            pattern_or_list,
            owner_pattern,
        )

        result = {}
        for snapshot in snapshot_list:
            if not search_criteria:
                snapshot_search_criteria = (
                    self._snapshot_manager.create_snapshot_search_criteria(
                        **snapshot.getProperties(),
                    )
                )
                fundamental_pattern, start_time, end_time = (
                    snapshot_search_criteria.get_search_criteria()
                )
                if t1:
                    start_time = t1
                    end_time = t2

            variables_for_snapshot = self.get_variables_for_snapshots(
                [snapshot.getName()],
                snapshot.getOwner(),
            )[snapshot.getName()]
            result[snapshot.getName()] = self.get(
                variables_for_snapshot,
                start_time,
                end_time,
                fundamental_pattern,
                unix_time,
            )

        return result

    @log_function_call
    def get_as_pivot(
        self,
        pattern_or_list: PatternOrList,
        t1: PyTimberTimeType,
        t2: Optional[Union[PyTimberTimeType, str]] = None,
        unixtime: bool = True,
    ) -> T:
        return self.data_converter.convert_from_get_as_pivot(
            dataset=self._data_manager._get_pivot_as_dataset(pattern_or_list, t1, t2),
            unix_time=unixtime,
        )

    # ################# private methods ############################

    def _deprecated_warning(
        self, old_param_name: str, advice: Optional[str] = None
    ) -> None:
        message = f"The {old_param_name} argument is deprecated and will be removed in future versions. {advice}"
        warnings.warn(message, DeprecationWarning)


class LoggingDB(AbstractLoggingDB[VariableQueryResultType]):
    def __init__(
        self,
        appid: str = "PYTIMBER4",
        clientid: str = "PYTIMBER3",
        source: str = "nxcals",
        loglevel: Optional[Union[str, int]] = None,
        sparkconf: Optional[str] = None,
        sparkprops: Optional[Mapping[str, str]] = None,
        kerberosprincipal: Optional[str] = None,
        kerberoskeytab: Optional[str] = None,
        data_location: str = "pro",
        spark_session: Optional[SparkSession] = None,
        service_url: str = NXCALS_PRO_SERVICE_URL,
        sparkloglevel: Optional[str] = "ERROR",
    ):
        super().__init__(
            NumpyDataConverter(),
            appid,
            clientid,
            source,
            loglevel,
            sparkconf,
            sparkprops,
            kerberosprincipal,
            kerberoskeytab,
            data_location,
            spark_session,
            service_url,
            sparkloglevel,
        )


class SparkLoggingDB(AbstractLoggingDB[VariableDataFrameResultType]):
    def __init__(
        self,
        appid: str = "SPARKPYTIMBER4",
        clientid: str = "PYTIMBER3",
        source: str = "nxcals",
        loglevel: Optional[Union[str, int]] = None,
        sparkconf: Optional[str] = None,
        sparkprops: Optional[Mapping[str, str]] = None,
        kerberosprincipal: Optional[str] = None,
        kerberoskeytab: Optional[str] = None,
        data_location: str = "pro",
        spark_session: Optional[SparkSession] = None,
        service_url: str = NXCALS_PRO_SERVICE_URL,
        sparkloglevel: Optional[str] = "ERROR",
    ):
        super().__init__(
            SparkDataConverter(),
            appid,
            clientid,
            source,
            loglevel,
            sparkconf,
            sparkprops,
            kerberosprincipal,
            kerberoskeytab,
            data_location,
            spark_session,
            service_url,
            sparkloglevel,
        )
