import functools
import sys
import time
import warnings
from ._version import version as __version__
from datetime import datetime, timedelta, timezone, tzinfo
from typing import (
    Any,
    Callable,
    Dict,
    Iterable,
    List,
    Optional,
    Tuple,
    TypeVar,
    Union,
    Hashable,
)

import numpy as np
import pandas as pd
from nxcals.api.extraction.data.common import TimeType
from pyspark.sql import DataFrame, SparkSession
from pyspark.sql.types import StructType

from platform import python_version
import getpass
import os
import socket
import uuid

VALUE = "nxcals_value"
TIMESTAMP = "nxcals_timestamp"
VARIABLE_NAME = "nxcals_variable_name"

AGGREGATION_VALUE_FIELD = "value"
AGGREGATION_TIMESTAMP_FIELD = "timestamp"

_PROCESS_START_TIME = time.time()
_PROCESS_UNIQUE_ID = uuid.uuid4()

if sys.version_info >= (3, 10):
    from typing import TypeAlias
else:
    from typing_extensions import TypeAlias

OLDEST_CONTEXT_TIMESTAMP = "2009-01-01 00:00:00.000"

DEFAULT_HIERARCHY_NXCALS_SYSTEM = "CERN"

TIME_FORMAT = "%Y-%m-%d %H:%M:%S.%f"

T = TypeVar("T")
PatternOrList = Union[str, Iterable[str]]

ValueArray: TypeAlias = np.ndarray
TimestampArray: TypeAlias = np.ndarray
VariableQueryResultType = Tuple[TimestampArray[Any, Any], ValueArray[Any, Any]]
VariableDataFrameResultType = Union[DataFrame, Any]
PyTimberTimeType = Union[TimeType, float]


def get_or_create(maybe_obj: Optional[T], generator: Callable[[], T]) -> T:
    if maybe_obj is not None:
        return maybe_obj

    return generator()


def verify_unix_time_is_set(unix_time: Optional[bool]) -> None:
    if not unix_time:
        raise ValueError("Currently only unix time representation is supported.")


def to_time_window(
    spark: SparkSession, from_time: PyTimberTimeType, to_time: PyTimberTimeType
) -> Any:
    t1, t2 = (get_instant_from(spark, from_time), get_instant_from(spark, to_time))
    return spark._jvm.cern.nxcals.api.domain.TimeWindow.between(t1, t2)


def get_instant_from(spark: SparkSession, t: PyTimberTimeType) -> Any:
    ns = get_nanos_from(t)
    time_utils = spark._jvm.cern.nxcals.api.utils.TimeUtils
    return time_utils.getInstantFromNanos(ns)


def nanos_to_timestamp(nanos: int, utc: bool = True) -> Any:
    return nanos_to_datetime(nanos, utc).timestamp()


def nanos_to_datetime(nanos: int, utc: bool = True) -> Any:
    return pd.to_datetime(nanos, unit="ns", utc=utc).to_pydatetime()


def get_nanos_from(t: PyTimberTimeType) -> int:
    if isinstance(t, np.datetime64):
        return int(t - np.datetime64(0, "ns"))

    if isinstance(t, float):
        timestamp_as_datetime = datetime.fromtimestamp(t, timezone.utc)
        return get_nanos_from_datetime(timestamp_as_datetime)

    if isinstance(t, datetime):
        localized_timestamp = (
            t if t.tzinfo is not None else t.replace(tzinfo=get_local_timezone())
        )
        return get_nanos_from_datetime(localized_timestamp)

    if isinstance(t, timedelta):
        warnings.warn(
            "Timedelta is a deprecated input parameter type, use other types."
        )
        return get_nanos_from_timedelta(t)

    if isinstance(t, str):
        return get_nanos_from_string(t)

    if "__int__" in dir(t):
        return int(t)

    raise ValueError(f"Unsupported timestamp type {t}")


def get_nanos_from_datetime(timestamp: datetime) -> int:
    return get_nanos_from_timedelta(timestamp - datetime.fromtimestamp(0, timezone.utc))


def get_nanos_from_timedelta(time_delta_since_epoch: timedelta) -> int:
    return int(time_delta_since_epoch.total_seconds() * 10**9)


def get_nanos_from_string(timestamp_as_string: str) -> int:
    datetime_object = pd.to_datetime(timestamp_as_string)

    if not datetime_object.tz:
        datetime_object = datetime_object.tz_localize(get_local_timezone())

    return datetime_object.value


def deprecated(msg: str) -> Callable[[Callable[..., T]], Callable[..., T]]:
    def deprecated_decorator(func: Callable[..., T]) -> Callable[..., T]:
        @functools.wraps(func)
        def wrapper(*args: Any, **kwargs: Any) -> T:
            warnings.warn(msg)
            return func(*args, **kwargs)

        wrapper.__doc__ = msg
        return wrapper

    return deprecated_decorator


def log_function_call(func: Callable[..., T]) -> Callable[..., T]:
    @functools.wraps(func)
    def wrapper(self, *args: Any, **kwargs: Any) -> Any:
        header = {"process": TracingUtils.get_process_runtime()}

        function_name = func.__name__
        class_name = self.__class__.__name__
        function_name = (
            class_name + "." + function_name
            if class_name == "SparkLoggingDB"
            else function_name
        )

        msg = {
            "function": function_name,
            "positional_args": args,
            "keyword_args": str(kwargs),
        }
        self._tracer.info({**header, **msg})
        return func(self, *args, **kwargs)

    return wrapper


def index_is_in_bound(index: int, list_length: int) -> bool:
    return -list_length <= index < list_length


def is_value_array(schema: StructType, value_field: str = VALUE) -> bool:
    field = schema[value_field]
    is_struct = isinstance(field.dataType, StructType)
    if not is_struct:
        return False

    names = field.dataType.names
    if not {"elements", "dimensions"}.issubset(set(names)):
        raise ValueError("Only arrays as nested structures are supported")

    return True


K = TypeVar("K", bound=Hashable)
V = TypeVar("V")


def groupby(
    collection: Iterable[T],
    key_mapper: Callable[[T], K] = lambda x: x,
    value_mapper: Callable[[T], V] = lambda x: x,
) -> Dict[K, List[V]]:
    grouped = {}
    for element in collection:
        key = key_mapper(element)
        value = value_mapper(element)
        if key in grouped:
            grouped[key].append(value)
        else:
            grouped[key] = [value]
    return grouped


def get_local_timezone() -> Union[tzinfo, None]:
    return datetime.now(timezone.utc).astimezone().tzinfo


class TracingUtils:
    @staticmethod
    def get_pretty_os_name() -> str:
        pretty_os_name = None
        if os.path.exists("/etc/os-release"):
            with open("/etc/os-release") as f:
                d = {}
                for line in f:
                    if not line.strip():
                        continue
                    k, v = line.rstrip().split("=")
                    d[k] = v.replace('"', "")
            pretty_os_name = d.get("PRETTY_NAME", None)
        if not pretty_os_name:
            # Centos 5 didn't have an /etc/os-release.
            if os.path.exists("/etc/redhat-release"):
                with open("/etc/redhat-release") as f:
                    pretty_os_name = f.read().strip()
        return pretty_os_name or "unknown"

    @staticmethod
    def get_os_info() -> Dict[str, Any]:
        if os.name == "posix":
            uname = os.uname()
            return {
                "name": TracingUtils.get_pretty_os_name(),
                "platform": sys.platform,
                "sysname": uname[0],
                "nodename": uname[1],
            }
        else:
            return {"platform": sys.platform}

    @staticmethod
    def get_process_runtime() -> Dict[str, Any]:
        return {
            "start_time": int(_PROCESS_START_TIME),
            "uptime_seconds": int(time.time() - _PROCESS_START_TIME),
            "session_id": _PROCESS_UNIQUE_ID,
        }

    @staticmethod
    def get_all_info() -> Dict[str, Any]:
        info = dict(
            cwd=os.getcwd(),
            pid=os.getpid(),
            process=TracingUtils.get_process_runtime(),
            os=TracingUtils.get_os_info(),
            user=getpass.getuser(),
            hostname=socket.getfqdn(),
            python_version=python_version(),
            pytimber_version=__version__,
        )
        return info
