# py4j-utils - Utility methods for Py4j 

For the moment this contains functionality to use type stubs for classes that are accessed through the Py4j gateway.
At a later stage, it might be published as a separate package outside of NXCALS.

## Getting started with NXCALS

Installing NXCALS using `pip install nxcals`, makes type stubs for the whole NXCALS Java API available.

You can get started with the following executable snippet: 

```python
from nxcals import spark_session_builder
from py4jgw.cern.nxcals.api.domain import Variable
from py4jgw.cern.nxcals.api.extraction.metadata import ServiceClientFactory
from py4jgw.cern.nxcals.api.extraction.metadata.queries import Variables

spark = spark_session_builder.get_or_create(app_name='nxcals-stubs-demo')

svc = ServiceClientFactory.createVariableService()
cond = Variables.suchThat().variableName().like("%TGM%").and_().description().exists()
vars = svc.findAll(cond)
v: Variable # needed to get code completion on v
for v in vars:
    print(v.getVariableName())
```

Make sure that the import statement `from nxcals import spark_session_builder` is located **before** the 
`from py4jgw.xxx import yyy` statements. 

If this order is not respected, or if the first line is missing, you get an error message:
```python
Traceback (most recent call last):
  File "use_nxcals_stubs/demo/demo0.py", line 1, in <module>
    from py4jgw.cern.nxcals.api.domain import Variable
ModuleNotFoundError: No module named 'py4jgw'
```
The reason is that the import system has not been correctly initialized, as explained in the next section.

## Behind the scenes
This package contains two pieces of functionality:
- an [import handler and loader](py4j_utils/py4j_importer.py) that uses the
  py4j gateway to resolve imports and to instantiate JavaClasses and JavaObjects and to invoke methods on them.
  It is implemented with a lazy resolution mechanism described below. 
- functionality to enable protected keywords, e.g. Java method such as `or()`, `and()`, `in()`, that are valid 
  in Java but protected keywords in Python. This is done by appending an underscore to the method name in Python, 
  which become `or_()`, `and_()`, `in_()`. This suffix is removed again when calling the Java method.

The importer is normally used in combination with a stubs package, e.g. `nxcals-p4yj-stubs`, which has three functions:
- during development, stubs help the IDE provide code completion and automatic generation of imports
- during code validation (e.g. with `mypy`), stubs help check correct types 
- during execution, the stubs provides a list of valid class and package names to the `Py4jImporter`. 

### Code completion
To write code and get code completion and automatic import generation, it is sufficient to install the stub package, 
e.g. `nxcals-p4yj-stubs` for the NXCALS API. At that moment the IDE (e.g. PyCharm) does all the work.

Similarily, when validating code with tools like `mypy`, only the type information is used.

### Code execution
However, to *execute* a Python program as the one above, the import system must be initialized. For NXCALS,
this is done inside the import statement 
```python
from nxcals import spark_session_builder
```

which internally calls 
```python
from nxcals_py4j_stubs.import_initializer import py4j_utils_importer
```

This registers a custom import handler (`Py4jImporter`) with the Python import system.

### Lazy creation of Py4j JavaClass objects

It is important to distinguish two phases in the life of `Py4jImporter` 

1. The "import phase", when imports statements are resolved and checked, e.g. 
```python
from py4jgw.cern.nxcals.api.extraction.metadata import ServiceClientFactory
```

2. The actual "execution phase" when method are invoked on the imported classes, e.g.
```python
svc = ServiceClientFactory.createVariableService()
```

During the import phase, the py4j gateway is not yet running, which means that an import statement
cannot yet create the JavaClass inside the Py4j gateway.  Instead, the `Py4jImporter` creates a `JavaClassProxy` 
that act as a placeholder  and lazily instantiates the corresponding Py4j JavaClass objects once the gateway is up.

For the second phase, the Py4j gateway is started by the line
```python
spark = spark_session_builder.get_or_create(app_name='nxcals-stubs-demo')
```
which internally passes a handle to the gateway (a `JVMView`) to the `Py4jImporter` with the statement
```python
py4j_utils_importer.py4j_jvm = spark._jvm
```

Once the `Py4jImporter` has a handle to the Py4j gateway, the `JavaClassProxy` objects initialize the corresponding 
JavaClass objects and pass method invokations on to them.


