import os
from setuptools import find_packages, setup

# Package version meta-data.
VERSION = os.environ.get("version")
REQUIREMENTS = os.environ.get("required_packages", "")
TEST_REQUIREMENTS = os.environ.get("required_test_packages", "")

if VERSION is None:
    print("Please define nxcals version as environment property")
else:
    print("Building wheel for version:", VERSION)

setup(
    name="nxcals-py4j-utils",
    version=VERSION,
    description="Utilities to enable NXCALS stubs to work with py4j",
    long_description="Utilities to enable NXCALS stubs to work with py4j",
    author="NXCALS Team (BE-CSS-CPA)",
    author_email="acc-logging-support@cern.ch",
    url="https://confluence.cern.ch/display/NXCALS",
    install_requires=REQUIREMENTS.split(" "),
    packages=find_packages(exclude=("tests",)),
    include_package_data=True,
    test_suite="tests",
    tests_require=TEST_REQUIREMENTS.split(" "),
    python_requires=">= 3.9",
    classifiers=[
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.11",
    ],
)
