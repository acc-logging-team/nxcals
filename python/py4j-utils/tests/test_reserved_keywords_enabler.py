import pytest

from py4j_utils import reserved_keyword_enabler
from py4j_utils.reserved_keyword_enabler import _keyword_handling__getattr__


class MyDelegate:
    @_keyword_handling__getattr__
    def getattr_method(self, name):
        return name


def test__remove_trailing_underscore_on_keywords():
    assert (
        "not_a_keyword"
        == reserved_keyword_enabler._remove_trailing_underscore_on_keywords(
            "not_a_keyword"
        )
    )
    assert "and" == reserved_keyword_enabler._remove_trailing_underscore_on_keywords(
        "and_"
    )
    assert "and__" == reserved_keyword_enabler._remove_trailing_underscore_on_keywords(
        "and__"
    )
    with pytest.raises(ValueError):
        reserved_keyword_enabler._remove_trailing_underscore_on_keywords(None)
    with pytest.raises(ValueError):
        reserved_keyword_enabler._remove_trailing_underscore_on_keywords("")


def test_remove_trailing_underscore_on_keywords_with_delegation():
    myDelegate = MyDelegate()
    # TODO: clarify whether the warning "parameters unfilled" is correct and how it can be fixed
    assert myDelegate.getattr_method("not_a_keyword") == "not_a_keyword"
    assert myDelegate.getattr_method("not_a_keyword_") == "not_a_keyword_"
    assert myDelegate.getattr_method("and_") == "and"
    assert myDelegate.getattr_method("and__") == "and__"
