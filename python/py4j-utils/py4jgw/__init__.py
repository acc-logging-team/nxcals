# This is a file with the only purpose to initialize the py4j import system.
# Make sure the name of this file is alphabetically before py4jgw
#
# See use_lazy_imports.py on how to use this
from py4j_utils.py4j_importer import enable_py4j_imports
from py4j_utils.reserved_keyword_enabler import enable_reserved_keywords

py4j_utils_importer = enable_py4j_imports(None)
enable_reserved_keywords()


def initialize_py4jgw(jvm):
    global py4j_utils_importer
    py4j_utils_importer.py4j_jvm = jvm
