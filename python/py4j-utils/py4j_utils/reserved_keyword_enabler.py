"""
Code to enable the use of Java methods that are reserved keywords in Python, such as add(), or(), in(), etc.
These methods can be used in the Python program by adding a trailing underscore, e.g. add_(), or_(), in_(), etc.
The trailing underscore is stripped off in java_gateway before invoking the Java method.

Just call `enable_reserved_keywords()` in your program before using py4j.

This approach is inspired by jpype.
"""

from functools import lru_cache
from keyword import kwlist
from typing import Any, Callable


def enable_reserved_keywords() -> None:
    """Allows the use of reserved keywords by monkey patching JavaObject.__getattr__ with a decorating method"""
    from py4j.java_gateway import JavaObject  # type: ignore

    JavaObject.__getattr__ = _keyword_handling__getattr__(JavaObject.__getattr__)


# A bound (instance) method with a string argument that returns anything
delegate_type = Callable[[Any, str], Any]


@lru_cache(maxsize=None)
def _remove_trailing_underscore_on_keywords(name: str) -> str:
    if not name:
        raise ValueError(
            "Empty or None string received: Must pass non-empty string as 'name'"
        )
    if not name.endswith("_"):
        return name
    nam = name[:-1]
    return nam if nam in kwlist else name


def _keyword_handling__getattr__(original__method: delegate_type) -> delegate_type:
    """
    Returns a function that strips of the underscore before invoking the original method()
    This uses the same approach as Python decorators.
    We have to patch "manually" because we cannot decorate the source code
    """

    def keyword_unescaping__getattr__(self: Any, name: str) -> Any:
        unescaped = _remove_trailing_underscore_on_keywords(name)
        return original__method(self, unescaped)

    return keyword_unescaping__getattr__
