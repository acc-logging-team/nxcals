import importlib.abc
import logging
import os
import pathlib
import sys
import types
from importlib.abc import MetaPathFinder, Loader
from importlib.machinery import ModuleSpec
from typing import Optional, Union, Iterable, Set, Sequence, List

from py4j.java_gateway import (
    JavaPackage,
    JavaClass,
    JavaObject,
    JVMView,
    is_magic_member,
)  # type: ignore


class Py4jModuleSpec(ModuleSpec):
    def __init__(
        self,
        name: str,
        loader: Optional[importlib.abc.Loader],
        *,
        origin: Optional[str] = None,
        loader_state=None,
        is_package: Optional[bool],
        py4j_java_entity: Optional[Union[JavaPackage, JavaObject, JavaClass]],
    ) -> None:
        super().__init__(
            name,
            loader,
            origin=origin,
            loader_state=loader_state,
            is_package=is_package,
        )
        self._py4j_java_entity = py4j_java_entity

    @property
    def py4j_java_entity(self) -> Optional[Union[JavaPackage, JavaObject, JavaClass]]:
        return self._py4j_java_entity


PY4JGW_PREFIX = "py4jgw"
PY4JGW_SUBS = f"{PY4JGW_PREFIX}"
PY4JGW_PREFIX_WITH_DOT = f"{PY4JGW_PREFIX}."


def enable_py4j_imports(
    py4j_jvm: Optional[JVMView],
    importer_unique_name: Optional[str] = None,
    py4j_stubs_package_names: Union[None, str, Set[str]] = PY4JGW_SUBS,
    py4j_valid_packages: Optional[Set[str]] = None,
) -> "Py4jImporter":
    """Enables Py4J imports and returns a `Py4jImporter`
    This method must be called before any import statement is executed, otherwise the imports fail with the error:
    "ModuleNotFoundError: No module named 'py4jgw'"

    :param py4j_jvm - the py4j gateway, typically obtained from spark._jvm. Typically initialized with None and
     initialized later by setting the `py4j_jvm` property on the returned `Py4jImporter` instance.
    :param importer_unique_name: a unique name for this importer, needed only if more than one importer is used
    :param py4j_stubs_package_names the name of the py4j-stubs package that shall be resolved. If this stubs
           package is installed, the `Py4jImporter` will use it to find all packages for which stubs are defined
    :param py4j_valid_packages: an explicit set of valid packages served by the py4j gateway.
    """
    py4j_valid_packages = py4j_valid_packages or set()
    py4j_packages_fqn_stubs = (
        set()
        if not py4j_stubs_package_names
        else _find_packages_with_stubs_in_sys_path(
            _to_string_set(py4j_stubs_package_names)
        )
    )
    py4j_packages_fqn = py4j_packages_fqn_stubs.union(py4j_valid_packages)
    return add_or_update(py4j_jvm, importer_unique_name, py4j_packages_fqn)


def _is_empty(directory: pathlib.Path) -> bool:
    return len(os.listdir(directory)) == 0


def _find_stub_package_fqns(stub_package_dir: pathlib.Path) -> Set[str]:
    stub_dirs = {
        d.relative_to(stub_package_dir.parent)
        for d in stub_package_dir.rglob("*")
        if d.is_dir() and not _is_empty(d)
    }
    stub_dirs.add(
        stub_package_dir.relative_to(stub_package_dir.parent)
    )  # add stub_package_dir iself
    packages = {str(sd).replace("-stubs", "").replace("/", ".") for sd in stub_dirs}
    return packages


def _find_stub_package_fully_qualified_names(
    stub_library_name: str = PY4JGW_SUBS, sys_path_dir: Optional[pathlib.Path] = None
) -> Set[str]:
    """generate a set of all packages that contain stubs by walking the package installed in the venv"""
    if not sys_path_dir or not stub_library_name:
        return set()
    stub_package_dir = sys_path_dir / stub_library_name
    if stub_package_dir.exists():
        return _find_stub_package_fqns(stub_package_dir)
    else:
        return set()


def _find_packages_with_stubs_in_sys_path(stub_library_names: Set[str]) -> Set[str]:
    stub_packages_fqn_found = set()
    stub_lib_names_with_packages_found = set()
    for path_string in sys.path:
        for stub_lib_name in stub_library_names:
            stub_package_fqn_found = _find_stub_package_fully_qualified_names(
                stub_lib_name, sys_path_dir=pathlib.Path(path_string)
            )
            if stub_package_fqn_found:
                stub_packages_fqn_found.update(stub_package_fqn_found)
                stub_lib_names_with_packages_found.add(stub_lib_name)

    if not stub_packages_fqn_found:
        logging.warning(f"No installed package found for {stub_library_names}")
        return set()
    stub_lib_names_not_found = stub_library_names.difference(
        stub_lib_names_with_packages_found
    )
    if stub_lib_names_not_found:
        logging.warning(
            "Not all installed package found, missing packages for stub_library_name "
            + f"{stub_lib_names_not_found}"
        )
    return stub_packages_fqn_found


def _to_string_set(names: Union[str, Iterable[str]]) -> Set[str]:
    # tricky because string is iterable and set(string) returns a set of its chars
    return (
        set(names)
        if isinstance(names, Iterable) and not isinstance(names, str)
        else {names}
    )


class Py4jImporter(MetaPathFinder, Loader):
    """
    A combination of MetaPathFinder and Loader.
    """

    # Py4J has no mechanism to check if an imported package is valid, therefore we pass a list of py4j_packages FQNs
    # that the importer shall resolve. The others are ignored and possibly resolved by a subsequent import handler.
    def __init__(
        self,
        py4j_jvm: Optional[JVMView],
        unique_name: Optional[str] = None,
        py4j_packages_fqn: Optional[Set[str]] = None,
    ):
        """
        Py4jImporter
        :param py4j_jvm: the link to the Py4J JVM, typically spark._jvm. Can be initialized later by setting the
               property `py4j_jvm`
        :param unique_name: a unique name of this importer, used to identify it when there is more than one importer,
        can be None
        :param py4j_packages_fqn: a set of valid package names of the Java classes served by Py4J
        """
        self._py4j_jvm = py4j_jvm
        self._unique_name = unique_name
        self._valid_py_packages_fqn = py4j_packages_fqn or set()

    def find_spec(
        self,
        fullname: str,
        path: Optional[Sequence[str]],
        target: Optional[types.ModuleType] = None,
    ) -> Optional[ModuleSpec]:
        if fullname in sys.modules:
            return sys.modules[fullname].__spec__
        if fullname == PY4JGW_PREFIX:
            return Py4jModuleSpec(
                fullname, self, is_package=True, py4j_java_entity=None
            )
        if fullname in self._valid_py_packages_fqn:
            return Py4jModuleSpec(
                fullname, self, is_package=True, py4j_java_entity=None
            )
        if not fullname.startswith(PY4JGW_PREFIX_WITH_DOT):
            return None
        java_fqn = fullname[len(PY4JGW_PREFIX_WITH_DOT) :]
        res = self._resolve_import(java_fqn)
        spec = Py4jModuleSpec(
            fullname,
            self,
            is_package=isinstance(res, JavaPackage),
            py4j_java_entity=res,
        )
        return spec

    def _resolve_import(self, java_fqn: str) -> Union["JavaClassProxy", JavaClass]:
        if self._py4j_jvm:
            return getattr(self._py4j_jvm, java_fqn)
        else:
            return JavaClassProxy(java_fqn, self)

    def create_module(self, spec: ModuleSpec) -> Optional[types.ModuleType]:
        if not isinstance(spec, Py4jModuleSpec):
            raise TypeError(f"unexpected ModuleSpec received: {spec}")
        return spec.py4j_java_entity

    def exec_module(self, module: types.ModuleType) -> None:
        pass

    def update_packages(self, valid_py_packages_fqn: Set[str]) -> None:
        if valid_py_packages_fqn:
            self._valid_py_packages_fqn.update(valid_py_packages_fqn)

    @property
    def py4j_jvm(self) -> Optional[JVMView]:
        return self._py4j_jvm

    @py4j_jvm.setter
    def py4j_jvm(self, py4j_jvm: JVMView) -> None:
        self._py4j_jvm = py4j_jvm

    @property
    def unique_name(self) -> Optional[str]:
        return self._unique_name


class JavaClassProxy:
    """
    A proxy to the py4j.java_gateway.JavaClass Py4j that defers initialization to the moment when gateway is started
    This makes it possible to import classes before the gateway is running
    """

    def __init__(self, fqn: str, _py4j_importer: Py4jImporter):
        self._py4j_java_class: Optional[JavaClass] = None
        self.fqn = fqn
        self.package = fqn[: fqn.rfind(".")]
        self._py4j_importer = _py4j_importer

    def _lazy_init(self) -> None:
        if self._py4j_java_class:
            return
        jvm = self._get_loader_jvm()
        if jvm:
            # Lazy initialization of the py4j JavaClass
            self._py4j_java_class = getattr(jvm, self.fqn)
        else:
            raise RuntimeError(
                "PySpark and/or py4j gateway is not yet initialized, please create an NXCALS spark session with "
                "`spark_session_builder.get_or_create(...)` before using classes imported from p4jgw.*"
            )

    def __call__(self, *args):
        self._lazy_init()
        if self._py4j_java_class:
            return self._py4j_java_class(*args)
        else:
            raise RuntimeError(
                "PySpark and/or py4j gateway is not yet initialized, please create an NXCALS spark session with "
                "`spark_session_builder.get_or_create(...)` before using classes imported from p4jgw.*"
            )

    @property
    def __name__(self) -> str:
        return self.fqn

    @property
    def __package__(self) -> str:
        return self.package

    @property
    def __loader__(self) -> Loader:
        return self._py4j_importer

    def _get_loader_jvm(self) -> Optional[JVMView]:
        return self._py4j_importer.py4j_jvm

    def __getattr__(self, item: str):
        ## mimicking implementation in py4j.java_gateway.JavaClass:
        if is_magic_member(item):
            raise AttributeError(f"JavaClass has no method {item}")
        self._lazy_init()
        return getattr(self._py4j_java_class, item)


def add_or_update(
    py4j_jvm: JVMView,
    importer_unique_name: Optional[str],
    py4j_packages_fqn: Optional[Set[str]],
) -> Py4jImporter:
    """
    Adds a new Py4jImporter or updates an existing one
    :param py4j_jvm: the link to the Py4J JVM, typically spark._jvm
    :param importer_unique_name: a unique name used to identify it when there is more than one importer
    :param py4j_packages_fqn: a set of valid package names of the Java classes served by Py4J
    :return: the Py4JImporter
    """
    return _add_or_update(
        sys.meta_path,  # type:ignore
        py4j_jvm,
        importer_unique_name,
        py4j_packages_fqn,
    )


def _add_or_update(
    meta_path: List[MetaPathFinder],
    py4j_jvm: JVMView,
    importer_unique_name: Optional[str],
    py4j_packages_fqn: Optional[Set[str]],
) -> Py4jImporter:
    for imp in meta_path:
        if isinstance(imp, Py4jImporter) and imp.unique_name == importer_unique_name:
            if py4j_packages_fqn:
                imp.update_packages(py4j_packages_fqn)
            if py4j_jvm:
                imp.py4j_jvm = py4j_jvm
            return imp
    # no importer found, create and append new one
    sys_importer = Py4jImporter(py4j_jvm, importer_unique_name, py4j_packages_fqn)
    meta_path.append(sys_importer)
    return sys_importer
