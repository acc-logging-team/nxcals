import os
import setuptools
from pathlib import Path

nxcals_version = os.environ["NXCALS_VERSION"]

HERE = Path(__file__).parent.absolute()
with (HERE / "README.md").open("rt") as fh:
    LONG_DESCRIPTION = fh.read().strip()

setuptools.setup(
    name="nxcals",
    version=nxcals_version,
    description="NXCALS meta-package",
    long_description=LONG_DESCRIPTION,
    long_description_content_type="text/markdown",
    author="NXCALS Team (BE-CSS-CPA)",
    author_email="acc-logging-support@cern.ch",
    url="https://confluence.cern.ch/display/NXCALS",
    project_urls={
        "Documentation": "https://nxcals-docs.web.cern.ch",
        "Repository": "https://gitlab.cern.ch/acc-logging-team/nxcals",
    },
    install_requires=(
        f"nxcals-spark-session-builder=={nxcals_version}",
        f"nxcals-extraction-api-python3=={nxcals_version}",
        f"nxcals-type-stubs=={nxcals_version}",
    ),
    python_requires=">= 3.9",
    classifiers=[
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.11",
    ],
)
