# nxcals-type-stubs

Python type stubs for the NXCALS Java API, which enable code completion and type checking in an IDE like PyCharm.  

## Usage
If you install NXCALS with `pip install nxcals` the following code should work.

```python
# The following lines import the type stubs contained in the package nxcals-type-stubs 
# in the PyCharm IDE they are automatically generated through code completion
from py4jgw.cern.nxcals.api.domain import Variable
from py4jgw.cern.nxcals.api.extraction.metadata import ServiceClientFactory
from py4jgw.cern.nxcals.api.extraction.metadata.queries import Variables

from nxcals import spark_session_builder 

# initialize an NXCALS spark session. This must be called before the NXCALS API stubs can be called
spark = spark_session_builder.get_or_create(app_name='stub_tests')

svc = ServiceClientFactory.createVariableService()
varCond = Variables.suchThat().variableName().like("%TGM%").and_().description().exists()

vars = svc.findAll(varCond)

v: Variable # specify the type to get code completion later
for v in vars:
    print(v.getVariableName())
```

It is important to initialize a spark session with 
`spark = spark_session_builder.get_or_create(app_name='stub_tests')`
before invoking the NXCALS API, e.g. with `svc = ServiceClientFactory.createVariableService()`.

If you don't follow this rule, you will get an Error message:

```
Please make sure that you initialize a spark session with 
`spark = spark_session_builder.get_or_create(app_name='stub_tests')`
before you invoke the API, e.g. with `svc = ServiceClientFactory.createVariableService()`
```


## Description for maintainers
This project produces a pip-installable package with type hints.

Essentially this is a gradle project that 
- Drives invocation of StubgenJ to generate stubs for the whole NXCALS Java API, the Java JDK and 
  the Fluent SQL classes (com.github.rutledgepaulv.qbuilders) used in the NXCALS API. 
- Adds a prefix `py4jgw` to name-space the generated stubs, to avoid clashes with source code in the `cern` package 
  and with jpype. 
- Creates a python wheel, whereby stubs are generated on-the fly and packaged as a wheel, not committed as source code.
- Uses MyPy to assess if the generation was successful.

This package is used by package `nxcals-py4j-utils`, which provides run-time support for the import mechanism. 
Without `nxcals-py4j-utils` an attempt to execute the code in the "Usage" section will throw an exception.

Possible future improvements:
- adding the prefix `py4jgw` as namespace should be done in stubgenj, not in this project.
