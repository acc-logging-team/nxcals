import pathlib
import sys

from mypy import api

# the typestubs have been generated in the build directory
project_dir = pathlib.Path(__file__).parent.parent.absolute()
build_dir = project_dir / "build"
sys.path.append(str(build_dir))


def should_compile(code: str):
    out, err, exit_status = api.run(["-c", code])
    assert exit_status == 0 and not err, out


def should_fail(code: str):
    out, err, exit_status = api.run(["-c", code])
    assert exit_status != 0 or err, out
