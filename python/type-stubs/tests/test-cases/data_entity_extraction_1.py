from py4jgw.cern.nxcals.api.extraction.data.builders import DataQuery
from pyspark.sql import SparkSession

spark_session: SparkSession

(
    DataQuery.builder(spark_session._jsparkSession)
    .entities()
    .system("CMW")
    .keyValuesEq({"dev": "device", "property": "prop"})
    .timeWindow(1, 10)
    .build()
)
