from py4jgw.cern.nxcals.api.extraction.metadata import ServiceClientFactory
from py4jgw.cern.nxcals.api.metadata.queries import Variables

svc = ServiceClientFactory.createVariableService()
varCond = (
    Variables.suchThat()
    .variableName()
    .like("%TGM%")
    .and_()
    .description()
    .like("timing")
)
svc.findAll(varCond)
