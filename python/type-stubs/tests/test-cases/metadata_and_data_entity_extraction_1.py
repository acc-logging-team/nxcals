from py4jgw.cern.nxcals.api.domain import TimeWindow
from py4jgw.cern.nxcals.api.extraction.data.builders import DataQuery
from py4jgw.cern.nxcals.api.extraction.metadata import ServiceClientFactory
from py4jgw.cern.nxcals.api.metadata.queries import Entities
from pyspark.sql import SparkSession


system_service = ServiceClientFactory.createSystemSpecService()

maybe_system = system_service.findByName("CMW")

entity_service = ServiceClientFactory.createEntityService()
svc = ServiceClientFactory.createEntityService()
entity_cond = (
    Entities.suchThat()
    .keyValues()
    .like(maybe_system.get(), {"dev": "device%"})
    .or_()
    .systemName()
    .eq("WINCCOA")
)


maybe_entity = svc.findOne(entity_cond)
spark_session: SparkSession

timeWindow = TimeWindow.after(1000)

(
    DataQuery.getFor(
        spark_session._jsparkSession,
        timeWindow=timeWindow,
        entity=maybe_entity.get(),
    )
)
