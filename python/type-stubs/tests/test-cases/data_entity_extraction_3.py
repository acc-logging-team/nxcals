from py4jgw.cern.nxcals.api.domain import TimeWindow
from py4jgw.cern.nxcals.api.extraction.data.builders import DataQuery
from pyspark.sql import SparkSession

spark_session: SparkSession

timeWindow = TimeWindow.fromStrings("2022", "2023")

(
    DataQuery.builder(spark_session._jsparkSession)
    .entities()
    .idEq(10)
    .system("CMW")
    .keyValuesLike({"key": "device"})
    .timeWindow(timeWindow)
    .build()
)
