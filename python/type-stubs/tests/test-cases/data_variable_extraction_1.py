from py4jgw.cern.nxcals.api.domain import TimeWindow
from py4jgw.cern.nxcals.api.extraction.data.builders import DataQuery
from pyspark.sql import SparkSession

spark_session: SparkSession

timeWindow = TimeWindow.after(1000)

(
    DataQuery.builder(spark_session._jsparkSession)
    .variables()
    .idEq(10)
    .system("CMW")
    .nameEq("a")
    .nameLike("aaa%")
    .timeWindow(timeWindow)
    .build()
)
