from py4jgw.cern.nxcals.api.domain import TimeWindow
from py4jgw.cern.nxcals.api.extraction.data.builders import DataQuery
from py4jgw.cern.nxcals.api.extraction.metadata import ServiceClientFactory
from py4jgw.cern.nxcals.api.metadata.queries import Variables
from pyspark.sql import SparkSession


svc = ServiceClientFactory.createVariableService()
varCond = (
    Variables.suchThat()
    .variableName()
    .like("%TGM%")
    .and_()
    .description()
    .like("timing")
)

maybe_variable = svc.findOne(varCond)
spark_session: SparkSession

timeWindow = TimeWindow.after(1000)

(
    DataQuery.getFor(
        spark_session._jsparkSession,
        timeWindow=timeWindow,
        variable=maybe_variable.get(),
    )
)
