from py4jgw.cern.nxcals.api.extraction.metadata import ServiceClientFactory
from py4jgw.cern.nxcals.api.domain import Variable, VariableDeclaredType

system_service = ServiceClientFactory.createSystemSpecService()

maybe_system = system_service.findByName("CMW")

svc = ServiceClientFactory.createVariableService()
variable = (
    Variable.builder()
    .variableName("name")
    .description("desc")
    .declaredType(VariableDeclaredType.MATRIX_NUMERIC)
    .systemSpec(maybe_system.get())
    .build()
)

svc.create(variable)
# svc.createAll({variable}) # Needs adding Union between Java set and python set by stubgenj
