import glob
import os
import re
from pathlib import Path
from typing import List

from setuptools import setup

HERE = Path(__file__).parent.absolute()

PY4JGW_DIR = "py4jgw"
# Package version meta-data.
VERSION = os.environ.get("version")
REQUIREMENTS = os.environ.get("required_packages", "")
TEST_REQUIREMENTS = os.environ.get("required_test_packages", "")

if VERSION is None:
    print("Please define nxcals version as environment property")
else:
    print("Building wheel for version:", VERSION)


def _to_package(rel_dir: str):
    return re.sub(r"/$", "", rel_dir).replace("/", ".")


def find_stub_packages() -> List[str]:
    stub_package_dir_names = glob.glob(f"{PY4JGW_DIR}/**/", recursive=True)
    stub_packages = [_to_package(s) for s in stub_package_dir_names]
    return stub_packages


ALL_PYI = [("*/" * depth) + "*.pyi" for depth in range(0, 10)]
ALL_PYI.append("py.typed")

setup(
    name="nxcals-type-stubs",
    version=VERSION,
    description="Utilities to enable NXCALS stubs to work with py4j",
    long_description="Package containing python stubs of Java classes to simplify working with them through py4j",
    author="NXCALS Team (BE-CSS-CPA)",
    author_email="acc-logging-support@cern.ch",
    url="https://confluence.cern.ch/display/NXCALS",
    project_urls={
        "Documentation": "https://nxcals-docs.web.cern.ch/current/user-guide/data-access/py4j-jpype-installaton/#examples-of-using-the-high-level-approach-of-py4j-with-the-nxcals-session-builder"
    },
    install_requires=REQUIREMENTS.split(" ") if REQUIREMENTS else [],
    packages=find_stub_packages(),
    include_package_data=True,
    test_suite="tests",
    tests_require=TEST_REQUIREMENTS.split(" ") if TEST_REQUIREMENTS else [],
    python_requires=">= 3.9",
    classifiers=[
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.11",
        "Operating System :: OS Independent",
    ],
    package_data={PY4JGW_DIR: ALL_PYI},
)
