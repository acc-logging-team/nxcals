from decimal import Decimal
from typing import List, TypeVar, Any, Optional

from py4j.java_gateway import JavaObject, JVMView
from py4j.compat import long, basestring

from py4j.protocol import JAVA_MAX_INT, JAVA_MIN_INT

from pyspark.sql import SparkSession

T = TypeVar("T")


def to_java_array(
    spark: SparkSession, arr: List[T], java_class: Optional[JavaObject] = None
) -> JavaObject:
    """
    Convert a Python list to a Java array.
    :param spark: currently active pyspark session
    :param java_class: java object with class
    :param arr: the Python list to convert
    :return: the Java array
    """
    jg = spark.sparkContext._gateway
    array_type = (
        java_class if java_class is not None else _determine_type(arr[0], jg.jvm)
    )
    jarr = jg.new_array(array_type, len(arr))
    for i in range(len(arr)):
        jarr[i] = arr[i]
    return jarr


def _determine_type(parameter: Any, jvm: JVMView) -> str:
    java = jvm.java
    if isinstance(parameter, JavaObject):
        return parameter.getClass()
    elif isinstance(parameter, bool):
        return java.lang.Bool
    elif isinstance(parameter, Decimal):
        return java.math.BigDecimal
    elif isinstance(parameter, int) and JAVA_MAX_INT >= parameter >= JAVA_MIN_INT:
        return java.lang.Integer
    elif isinstance(parameter, long) or isinstance(parameter, int):
        return java.lang.Long
    elif isinstance(parameter, float):
        return java.lang.Double
    elif isinstance(parameter, basestring):
        return java.lang.String
    else:
        raise TypeError("Can't determine correct type")
