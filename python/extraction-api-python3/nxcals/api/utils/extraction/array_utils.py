from typing import List

import numpy as np
import pyspark.sql as sp

from pyspark.sql.functions import col, udf
from pyspark.sql.types import ArrayType, StructType, DataType

ARRAY_ELEMENTS_FIELD_NAME = "elements"
ARRAY_DIMENSIONS_FIELD_NAME = "dimensions"


class ArrayUtils:
    @staticmethod
    def _extract_array_fields(schema: StructType) -> list[str]:
        """
        Extracts field names that contain structured array data (with 'elements' and 'dimensions').

        :param schema: Spark StructType schema
        :return: List of field names that represent arrays
        """
        return [
            field.name
            for field in schema.fields
            if isinstance(field.dataType, StructType)
            and {ARRAY_ELEMENTS_FIELD_NAME, ARRAY_DIMENSIONS_FIELD_NAME}.issubset(
                set(field.dataType.names)
            )
        ]

    @staticmethod
    def _validate_array_fields(schema, value_fields: List[str]):
        """
        Validates if the given columns exists in the schema and have the required structure.

        :param schema: Spark StructType schema
        :param value_fields: Column names to validate
        :raises ValueError: If column is missing or does not conform to expected structure
        """
        for value_field in value_fields:
            try:
                field = schema[value_field]
            except KeyError:
                raise ValueError(f"Field '{value_field}' does not exist in the schema")

            if not isinstance(field.dataType, StructType):
                raise ValueError(f"Field '{value_field}' is not a struct type")

            field_names = set(field.dataType.names)
            if field_names != {"elements", "dimensions"}:
                raise ValueError(
                    f"Field '{value_field}' must contain both 'elements' and 'dimensions' fields"
                )

    @staticmethod
    def _reshape_array(elements, dimensions):
        """
        Reshapes a flat list into a multidimensional NumPy array.

        :param elements: List[int] - The flattened array.
        :param dimensions: List[int] - The shape of the desired multidimensional array.
        :return: List - Reshaped array converted back to a nested list.
        """
        if len(dimensions) == 1:
            return elements

        np_array = np.array(elements)

        total_elements = np.prod(dimensions)
        if len(elements) != total_elements:
            raise ValueError(
                f"Cannot reshape array: {len(elements)} elements cannot fit into shape {dimensions}."
            )

        reshaped_array = np_array.reshape(dimensions).tolist()
        return reshaped_array

    @staticmethod
    def _get_nested_array_type(depth: int, base_type) -> DataType:
        """
        Recursively generates a nested ArrayType based on the depth.

        :param depth: Number of dimensions.
        :param base_type: The base type (default: IntegerType).
        :return: Nested ArrayType.
        """
        array_type = base_type
        for _ in range(depth):
            array_type = ArrayType(array_type)
        return array_type

    @staticmethod
    def reshape(
        df: sp.DataFrame, array_columns: list[str] | None = None
    ) -> sp.DataFrame:
        """
        Extracts and reshape array columns for easier processing.

        :param df: Input Spark DataFrame
        :param array_columns: List of column names containing structured array data
        :return: Transformed DataFrame with extracted 'elements' and 'dimensions' columns
        """
        schema = df.schema

        if not array_columns:
            array_columns = ArrayUtils._extract_array_fields(schema)
        else:
            ArrayUtils._validate_array_fields(schema, array_columns)

        first_row = df.first()
        for column in array_columns:
            base_array_type = (
                schema[column].dataType[ARRAY_ELEMENTS_FIELD_NAME].dataType
            )

            first_row_dimension = first_row[column][ARRAY_DIMENSIONS_FIELD_NAME]
            if first_row_dimension:
                dimension_length = len(first_row_dimension)

                if dimension_length == 1:
                    reshape_array_udf = udf(
                        lambda elements, dims: ArrayUtils._reshape_array(
                            elements, dims
                        ),
                        base_array_type,
                    )
                else:
                    nested_array_type = ArrayUtils._get_nested_array_type(
                        dimension_length - 1, base_array_type
                    )
                    reshape_array_udf = udf(
                        lambda elements, dims: ArrayUtils._reshape_array(
                            elements, dims
                        ),
                        nested_array_type,
                    )

                df = df.withColumn(
                    column,
                    reshape_array_udf(
                        col(f"{column}.{ARRAY_ELEMENTS_FIELD_NAME}"),
                        col(f"{column}.{ARRAY_DIMENSIONS_FIELD_NAME}"),
                    ),
                )

        return df
