from typing import Iterable

from pyspark.sql import SparkSession, DataFrame
from pyspark.errors.exceptions.captured import IllegalArgumentException

from nxcals.api.extraction.data.common import TimeType, _to_time_window


class ServiceFactory:
    @staticmethod
    def fundamentals_service(spark_session: SparkSession) -> "FundamentalService":
        return FundamentalService(spark_session)


class FundamentalService:
    def __init__(self, spark: SparkSession):
        self._spark = spark
        self._service = spark._jvm.cern.nxcals.api.custom.service.fundamental.FundamentalServiceFactory.getInstance(
            spark._jsparkSession
        )

    def get_for(
        self, start_time: TimeType, end_time: TimeType, accelerators: Iterable[str]
    ) -> DataFrame:
        """
        Retrieve fundamentals dataset for given accelerator(s), for a given time window. Returns PySpark DataFrame
        :param start_time: extraction start time
        :param end_time: extraction end time
        :param accelerators: collection with accelerator names
        :return: PySpark DataFrame
        """
        try:
            time_window = _to_time_window(self._spark, start_time, end_time)
            java_df = self._service.getFor(time_window, set(accelerators))
            return DataFrame(java_df, self._spark)
        except IllegalArgumentException as e:
            raise ValueError() from e
