from typing import Dict, List

from pyspark.sql import DataFrame, SparkSession

from .common import (
    SystemStage,
    StartStage,
    EntityAliasStage,
    TimeType,
    KeyValuesStage,
    VariableAliasStage,
    VariableStage,
    EntityQuery,
    _escape_wildcard_character,
)
from .stages import (
    SystemStage,
    EntityStage,
    BuildStage,
    EntityStageLoop,
    VariableStageLoop,
    SystemOrIdStage,
    VariableStage as VariableStageV2,
)


class BuilderBase:
    def __init__(self, spark, java_builder):
        self._spark = spark
        self.__java_builder = java_builder

    def __getattr__(self, name):
        def execute(*args):
            self.__java_builder = getattr(self.__java_builder, name)(*args)

        return execute

    def _get_java_builder(self):
        return self.__java_builder


class DataQueryBase:
    def __init__(self, spark, builder_class, builders_package):
        self.spark = spark
        self.builder_class = builder_class
        self.builders_package = builders_package

    def byEntities(self):
        builder = self.builder_class(
            self.spark,
            self.builders_package(self.spark)
            .DataQuery.builder(self.spark._jsparkSession)
            .byEntities(),
        )
        return SystemStage(
            StartStage(EntityAliasStage(KeyValuesStage(builder=builder)))
        )

    def byVariables(self):
        builder = self.builder_class(
            self.spark,
            self.builders_package(self.spark)
            .DataQuery.builder(self.spark._jsparkSession)
            .byVariables(),
        )
        return SystemStage(
            StartStage(VariableAliasStage(VariableStage(builder=builder)))
        )

    def variables(
        self,
    ) -> SystemOrIdStage[
        VariableStageV2[BuildStage[DataFrame]],
        VariableStageLoop[BuildStage[DataFrame]],
        BuildStage[DataFrame],
    ]:
        builder = self.builder_class(
            self.spark,
            self.builders_package(self.spark)
            .DataQuery.builder(self.spark._jsparkSession)
            .variables(),
        )
        return SystemOrIdStage(VariableStageV2(BuildStage(builder=builder)))

    def entities(
        self,
    ) -> SystemOrIdStage[
        EntityStage[BuildStage[DataFrame]],
        EntityStageLoop[BuildStage[DataFrame]],
        BuildStage[DataFrame],
    ]:
        builder = self.builder_class(
            self.spark,
            self.builders_package(self.spark)
            .DataQuery.builder(self.spark._jsparkSession)
            .entities(),
        )
        return SystemOrIdStage(EntityStage(BuildStage(builder=builder)))

    @staticmethod
    def _get_for_variables(
        query_class,
        spark: SparkSession,
        start_time: TimeType,
        end_time: TimeType,
        system: str,
        variables: List[str] = [],
        variables_like: List[str] = [],
        field_aliases: Dict[str, List[str]] = {},
    ):
        if len(variables + variables_like) == 0:
            raise ValueError("No variables nor variable patterns")

        query = query_class.builder(spark).variables().system(system).nameIn(variables)

        for pattern in variables_like:
            query.nameLike(pattern)

        return (
            query.timeWindow(start_time, end_time)
            .fieldAliases(
                {alias: set(fields) for alias, fields in field_aliases.items()}
            )
            .build()
        )

    @staticmethod
    def _get_for_entities(
        query_class,
        spark: SparkSession,
        start_time: TimeType,
        end_time: TimeType,
        system: str,
        entity_queries: List[EntityQuery],
        field_aliases: Dict[str, List[str]] = {},
    ):
        query = query_class.builder(spark).entities().system(system)
        for entity_query in entity_queries:
            if entity_query.has_wildcards():
                key_values = {
                    key: _escape_wildcard_character(value)
                    for key, value in entity_query.key_values.items()
                }
                patterns = dict(key_values, **entity_query.key_values_like)
                query = query.keyValuesLike(patterns)
            else:
                query = query.keyValuesEq(entity_query.key_values)
        return (
            query.timeWindow(start_time, end_time)
            .fieldAliases(
                {alias: set(fields) for alias, fields in field_aliases.items()}
            )
            .build()
        )
