from __future__ import annotations

from typing import List, Dict

from pyspark.sql import DataFrame, SparkSession

from .base import (
    DataQueryBase,
    BuilderBase,
)
from .common import (
    SystemStage,
    StartStage,
    EntityAliasStage,
    EntityQuery,
    DeviceStage,
    TimeType,
)
from .stages import (
    BuildStage,
    DeviceStage as DeviceStageV2,
    SystemStage as SystemStageV2,
)


def builders_package(spark):
    return spark._jvm.cern.nxcals.api.custom.extraction.data.builders.expanded


class Builder(BuilderBase):
    def _build(self, to_df: bool = True):
        df = self._get_java_builder().build()
        if to_df:
            # I assume, that _spark is pyspark SparkSession
            return [DataFrame(x, self._spark) for x in df]
        return df


class DataQuery(DataQueryBase):
    """
    A class that provides methods for building queries on data that has been loaded into a Spark dataframe.
    """

    @staticmethod
    def builder(spark):
        return DataQuery(spark, Builder, builders_package)

    @staticmethod
    def getForVariables(
        spark: SparkSession,
        start_time: TimeType,
        end_time: TimeType,
        system: str,
        variables: List[str] = [],
        variables_like: List[str] = [],
        field_aliases: Dict[str, List[str]] = {},
    ) -> List[DataFrame]:
        return DataQueryBase._get_for_variables(
            DataQuery,
            spark,
            start_time,
            end_time,
            system,
            variables,
            variables_like,
            field_aliases,
        )

    @staticmethod
    def getForEntities(
        spark: SparkSession,
        start_time: TimeType,
        end_time: TimeType,
        system: str,
        entity_queries: List[EntityQuery],
        field_aliases: Dict[str, List[str]] = {},
    ) -> List[DataFrame]:
        return DataQueryBase._get_for_entities(
            DataQuery,
            spark,
            start_time,
            end_time,
            system,
            entity_queries,
            field_aliases,
        )


class DevicePropertyDataQuery:
    @staticmethod
    def builder(spark):
        builder = Builder(
            spark,
            builders_package(spark).DevicePropertyDataQuery.builder(
                spark._jsparkSession
            ),
        )
        return SystemStage(StartStage(EntityAliasStage(DeviceStage(builder=builder))))


class ParameterDataQuery:
    @staticmethod
    def builder(spark) -> SystemStageV2[DeviceStageV2[BuildStage[List[DataFrame]]]]:
        builder = Builder(
            spark,
            builders_package(spark).ParameterDataQuery.builder(spark._jsparkSession),
        )
        return SystemStageV2(DeviceStageV2(BuildStage(builder=builder)))
