from typing import Any, Dict, List, Set, Generic, TypeVar
from .common import Stage, nanos, TimeType

NextStage = TypeVar("NextStage")
LoopStage = TypeVar("LoopStage")
NextLoopStage = TypeVar("NextLoopStage")
NextNextStage = TypeVar("NextNextStage")

Result = TypeVar("Result")


class AliasStage(Stage):
    def fieldAliases(self, aliases: Dict[str, Set[str]]):
        self._builder.fieldAliases(aliases)
        return self


class BuildStage(AliasStage, Generic[Result]):
    def build(self, to_df: bool = True) -> Result:
        return self._builder._build(to_df)


class TimeStage(Stage, Generic[NextStage]):
    def atTime(self, at_time: TimeType) -> NextStage:
        """Search for data from given moment.

        at_time -- time in one of formats: datetime64, datetime, timedelta, str or int as nanos
        """
        self._builder.atTime(nanos(at_time))
        return self._next_stage

    def timeWindow(self, start_time: TimeType, end_time: TimeType) -> NextStage:
        """Search for data from given time window.

        start_time -- start time in one of formats: datetime64, datetime, timedelta, str or int (nanos)
        end_time -- end time in one of formats: datetime64, datetime, timedelta, str or int (nanos)
        """
        self._builder.timeWindow(nanos(start_time), nanos(end_time))
        return self._next_stage


class SystemStage(Stage, Generic[NextStage]):
    def system(self, system) -> NextStage:
        self._builder.system(str(system))
        return self._next_stage


class SystemOrIdStage(
    SystemStage[NextStage], Generic[NextStage, NextLoopStage, NextNextStage]
):
    def idEq(self, id: int) -> "SystemOrIdStageLoop[NextLoopStage, NextNextStage]":
        """Query by id

        id -- id as int"""
        return SystemOrIdStageLoop(self._next_stage.idEq(id))

    def idIn(
        self, ids: Set[int]
    ) -> "SystemOrIdStageLoop[NextLoopStage, NextNextStage]":
        """Query by ids

        ids -- set of ids"""
        return SystemOrIdStageLoop(self._next_stage.idIn(ids))


class SystemOrIdStageLoop(
    SystemOrIdStage[NextStage, NextStage, NextNextStage], TimeStage[NextNextStage]
):
    def atTime(self, at_time: TimeType) -> NextNextStage:
        """Search for data from given moment.

        at_time -- time in one of formats: datetime64, datetime, timedelta, str or int as nanos
        """
        self._builder.atTime(nanos(at_time))
        return self._next_stage._next_stage

    def timeWindow(self, start_time: TimeType, end_time: TimeType) -> NextNextStage:
        """Search for data from given time window.

        start_time -- start time in one of formats: datetime64, datetime, timedelta, str or int (nanos)
        end_time -- end time in one of formats: datetime64, datetime, timedelta, str or int (nanos)
        """
        self._builder.timeWindow(nanos(start_time), nanos(end_time))
        return self._next_stage._next_stage


class DeviceStage(Stage, Generic[NextStage]):
    def parameterEq(self, parameter: str) -> "DeviceStageLoop[NextStage]":
        """Search for given parameter.

        parameter -- string in form "device/property"
        """
        self._builder.parameterEq(parameter)
        return DeviceStageLoop(self._next_stage)

    def parameterIn(self, parameters: List[str]) -> "DeviceStageLoop[NextStage]":
        """Search for given parameters.

        parameters -- list of strings in form of "device/property"
        """
        self._builder.parameterIn(list(parameters))
        return DeviceStageLoop(self._next_stage)

    def parameterLike(self, pattern: str) -> "DeviceStageLoop[NextStage]":
        """Search for parameters, which match given pattern.

        pattern -- strings in form of "device/property" and in Oracle SQL pattern form (% and _ as wildcards)
        """
        self._builder.parameterLike(pattern)
        return DeviceStageLoop(self._next_stage)

    def deviceEq(self, device: str) -> "PropertyStage[NextStage]":
        """Allow to query for entities from CMW by device-property names.

        device -- string with device name
        """
        self._builder.deviceEq(device)
        return PropertyStage(self._next_stage)

    def deviceLike(self, pattern: str) -> "PropertyStage[NextStage]":
        """Allow to query for entities from CMW by device-property names.

        pattern -- string with device name pattern in Oracle SQL pattern form (% and _ as wildcards)
        """
        self._builder.deviceLike(pattern)
        return PropertyStage(self._next_stage)


class PropertyStage(Stage, Generic[NextStage]):
    def propertyEq(self, property: str) -> "DeviceStageLoop[NextStage]":
        """Define property in device-property entity query

        property -- string with device name
        """
        self._builder.propertyEq(property)
        return DeviceStageLoop(self._next_stage)

    def propertyLike(self, pattern: str) -> "DeviceStageLoop[NextStage]":
        """Define property pattern in device-property entity query

        pattern -- string with property name pattern in Oracle SQL pattern form (% and _ as wildcards)
        """
        self._builder.propertyLike(pattern)
        return DeviceStageLoop(self._next_stage)


class DeviceStageLoop(DeviceStage[NextStage], TimeStage[NextStage]):
    pass


class IDStageUtils:
    @staticmethod
    def idEq(builder, id: int):
        """Query for the object's data by its id

        id -- object id as int"""
        builder.idEq(builder._spark._jvm.Long(str(id)))

    @staticmethod
    def idIn(builder, ids: Set[int]):
        """Query for objects' data by their ids

        ids -- set of objects' ids"""
        for id in ids:
            IDStageUtils.idEq(
                builder, id
            )  # there was a problem with int (python) to Long casting


class EntityStage(Stage, Generic[NextStage]):
    def keyValuesEq(self, key_values: Dict[str, Any]) -> "EntityStageLoop[NextStage]":
        """Query for entity which has exactly given keys and values

        key_values -- map with keys and values as strings"""
        self._builder.keyValuesEq(key_values)
        return EntityStageLoop(self._next_stage)

    def keyValuesIn(
        self, key_values: List[Dict[str, Any]]
    ) -> "EntityStageLoop[NextStage]":
        """Query for entities which have given key values

        key_values -- list of maps with keys and values as strings"""
        self._builder.keyValuesIn(key_values)
        return EntityStageLoop(self._next_stage)

    def keyValuesLike(self, patterns: Dict[str, Any]) -> "EntityStageLoop[NextStage]":
        """Query for entities which match given patterns

        key_values -- map with keys and value patterns in Oracle SQL pattern form (% and _ as wildcards)"""
        self._builder.keyValuesLike(patterns)
        return EntityStageLoop(self._next_stage)

    def idEq(self, id: int) -> "EntityStageLoop[NextStage]":
        """Query for entity by its id

        id -- entity as number"""
        IDStageUtils.idEq(self._builder, id)
        return EntityStageLoop[NextStage](self._next_stage)

    def idIn(self, ids: Set[int]) -> "EntityStageLoop[NextStage]":
        """Query for entities by their ids

        ids -- set of entities ids"""
        IDStageUtils.idIn(self._builder, ids)
        return EntityStageLoop[NextStage](self._next_stage)


class EntityStageLoop(EntityStage[NextStage], TimeStage[NextStage]):
    pass


class VariableStage(Stage, Generic[NextStage]):
    def nameEq(self, name: str) -> "VariableStageLoop[NextStage]":
        """Query for variable with given name

        name -- variable name as string"""
        self._builder.nameEq(name)
        return VariableStageLoop[NextStage](self._next_stage)

    def nameIn(self, names: List[str]) -> "VariableStageLoop[NextStage]":
        """Query for variables in list

        names -- list of strings with variable names"""
        self._builder.nameIn(names)
        return VariableStageLoop[NextStage](self._next_stage)

    def nameLike(self, pattern: str) -> "VariableStageLoop[NextStage]":
        """Query for variables, which match given pattern

        pattern -- string with variable name pattern in Oracle SQL pattern form (% and _ as wildcards)"""
        self._builder.nameLike(pattern)
        return VariableStageLoop[NextStage](self._next_stage)

    def idEq(self, id: int) -> "VariableStageLoop[NextStage]":
        """Query for variable by its id

        id -- variable as number"""
        IDStageUtils.idEq(self._builder, id)
        return VariableStageLoop[NextStage](self._next_stage)

    def idIn(self, ids: Set[int]) -> "VariableStageLoop[NextStage]":
        """Query for variables by thier ids

        ids -- set of variable ids"""
        IDStageUtils.idIn(self._builder, ids)
        return VariableStageLoop[NextStage](self._next_stage)


class VariableStageLoop(TimeStage[NextStage], VariableStage[NextStage]):
    pass
