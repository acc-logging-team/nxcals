import warnings
from datetime import datetime, timedelta
from numpy import datetime64, int64
from py4j.java_gateway import JavaObject
from pyspark.sql import SparkSession
from typing import Any, Dict, List, Optional, Union
from zoneinfo import ZoneInfo

TimeType = Union[str, datetime, datetime64, timedelta, int, int64]


def _to_time_window(
    spark: SparkSession, start_time: TimeType, end_time: TimeType
) -> JavaObject:
    return spark._jvm.cern.nxcals.api.domain.TimeWindow.between(
        nanos(start_time), nanos(end_time)
    )


def _escape_wildcard_character(maybe_str: Any):
    if isinstance(maybe_str, str):
        return maybe_str.replace("%", "\\%").replace("_", "\\_")
    else:
        return maybe_str


def nanos(value: Any) -> int:
    if isinstance(value, datetime64):
        epoch = datetime64("1970-01-01T00:00:00Z", "ns")
        return int((value - epoch).astype("timedelta64[ns]").astype(int))

    if isinstance(value, datetime):
        if value.tzinfo is not None:
            value = value.astimezone(ZoneInfo("UTC")).replace(tzinfo=None)
        value = value - datetime(1970, 1, 1)
        # value is timedelta now, handled in the next if

    if isinstance(value, timedelta):
        # Convert timedelta to nanoseconds manually
        return (
            value.days * 86_400 * 10**9
            + value.seconds * 10**9
            + value.microseconds * 10**3
        )

    if isinstance(value, str):
        return nanos(datetime64(value))

    if hasattr(value, "__int__"):
        return int(value)

    raise ValueError(f"Unsupported timestamp type {type(value).__name__}: {value}")


class Stage:
    def __init__(self, next=None, builder=None):
        self._builder = next._builder if next is not None else builder
        self._next_stage = next


class SystemStage(Stage):
    def system(self, system):
        self._builder.system(str(system))
        return self._next_stage


class StartStage(Stage):
    def atTime(self, at_time):
        self._builder.atTime(nanos(at_time))
        return self._next_stage

    def startTime(self, start_time):
        self._builder.startTime(nanos(start_time))
        return EndStage(self._next_stage)


class EndStage(Stage):
    def endTime(self, end_time):
        self._builder.endTime(nanos(end_time))
        return self._next_stage

    def duration(self, duration):
        self._builder.duration(nanos(duration))
        return self._next_stage


class VariableStage(Stage):
    def variable(self, variable):
        self._builder.variable(str(variable))
        return VariableBuildStageLoop(self)

    def variables(self, variables):
        self._builder.variables(list(variables))
        return VariableBuildStageLoop(self)

    def variableLike(self, variable):
        self._builder.variableLike(str(variable))
        return VariableBuildStageLoop(self)

    def variablesLike(self, variables: List[str]):
        self._builder.variablesLike(list(variables))
        return VariableBuildStageLoop(self)


class BuildStage(Stage):
    def buildDataset(self):
        warnings.warn(
            "please use build() instead",
            DeprecationWarning,
        )
        return self.build()

    def build(self):
        return self._builder._build()


class VariableBuildStageLoop(VariableStage, BuildStage):
    pass


class EntityStage(Stage):
    def entity(self):
        self._builder.entity()
        return self._next_stage


class AliasStage(Stage):
    def fieldAliases(self, aliases):
        self._builder.fieldAliases(aliases)
        return self

    def fieldAlias(self, alias, field):
        self._builder.fieldAlias(alias, field)
        return self


class EntityAliasStage(EntityStage, AliasStage):
    pass


class VariableAliasStage(VariableStage, AliasStage):
    pass


class KeyValuesStage(Stage):
    def keyValue(self, key, value):
        self._builder.keyValue(str(key), str(value))
        return KeyValuesStageLoop(KeyValuesStage(self))

    def keyValueLike(self, key, value):
        self._builder.keyValueLike(str(key), str(value))
        return KeyValuesStageLoop(KeyValuesStage(self))

    def keyValues(self, key_values):
        self._builder.keyValues(key_values)
        return KeyValuesStageLoop(KeyValuesStage(self))

    def keyValuesLike(self, key_values):
        self._builder.keyValuesLike(key_values)
        return KeyValuesStageLoop(KeyValuesStage(self))


class KeyValuesStageLoop(KeyValuesStage, EntityStage, BuildStage):
    pass


class DeviceStage(Stage):
    def device(self, device):
        self._builder.device(str(device))
        return PropertyStage(self)

    def deviceLike(self, device):
        self._builder.deviceLike(str(device))
        return PropertyStage(self)

    def parameter(self, parameter):
        self._builder.parameter(str(parameter))
        return DeviceStageLoop(DeviceStage(self))

    def parameters(self, parameters):
        self._builder.parameters(list(parameters))
        return DeviceStageLoop(DeviceStage(self))

    def parameterLike(self, parameter):
        self._builder.parameterLike(str(parameter))
        return DeviceStageLoop(DeviceStage(self))


class PropertyStage(Stage):
    def property(self, property_name):
        self._builder.property(str(property_name))
        return DeviceStageLoop(DeviceStage(self))

    def propertyLike(self, property_name):
        self._builder.propertyLike(str(property_name))
        return DeviceStageLoop(DeviceStage(self))


class DeviceStageLoop(EntityStage, BuildStage):
    pass


class EntityQuery:
    def __init__(
        self,
        key_values: Optional[Dict[str, Any]] = None,
        key_values_like: Optional[Dict[str, Any]] = None,
    ) -> None:
        if key_values is None and key_values_like is None:
            raise ValueError(
                "Either key_values or key_values_like must be defined",
            )

        self.key_values = key_values if key_values is not None else {}
        self.key_values_like = key_values_like if key_values_like is not None else {}

    def has_wildcards(self) -> bool:
        return len(self.key_values_like) > 0
