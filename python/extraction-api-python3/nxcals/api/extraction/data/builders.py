from __future__ import annotations

from typing import List, Dict, Iterable

from pyspark.sql import DataFrame, SparkSession

from .base import (
    DataQueryBase,
    BuilderBase,
)
from .common import (
    SystemStage,
    StartStage,
    EntityAliasStage,
    EntityQuery,
    DeviceStage,
    TimeType,
    _to_time_window,
)
from .stages import (
    BuildStage,
    DeviceStage as DeviceStageV2,
    SystemStage as SystemStageV2,
)


def builders_package(spark):
    return spark._jvm.cern.nxcals.api.extraction.data.builders


class Builder(BuilderBase):
    def _build(self, to_df: bool = True):
        df = self._get_java_builder().build()
        if to_df:
            # I assume, that _spark is pyspark SparkSession
            return DataFrame(df, self._spark)
        return df


class DataQuery(DataQueryBase):
    """
    A class that provides methods for building queries on data that has been loaded into a Spark dataframe.
    """

    @staticmethod
    def builder(spark):
        return DataQuery(spark, Builder, builders_package)

    @staticmethod
    def getForVariables(
        spark: SparkSession,
        start_time: TimeType,
        end_time: TimeType,
        system: str,
        variables: List[str] = [],
        variables_like: List[str] = [],
        field_aliases: Dict[str, List[str]] = {},
    ) -> DataFrame:
        return DataQueryBase._get_for_variables(
            DataQuery,
            spark,
            start_time,
            end_time,
            system,
            variables,
            variables_like,
            field_aliases,
        )

    @staticmethod
    def getForEntities(
        spark: SparkSession,
        start_time: TimeType,
        end_time: TimeType,
        system: str,
        entity_queries: List[EntityQuery],
        field_aliases: Dict[str, List[str]] = {},
    ) -> DataFrame:
        return DataQueryBase._get_for_entities(
            DataQuery,
            spark,
            start_time,
            end_time,
            system,
            entity_queries,
            field_aliases,
        )

    @staticmethod
    def getAsPivot(
        spark: SparkSession,
        start_time: TimeType,
        end_time: TimeType,
        system: str,
        variables: Iterable[str] = (),
        variables_like: Iterable[str] = (),
    ) -> DataFrame:
        """
        Create a dataset with variable names as columns. Values are joined on timestamps. Experimental, may be changed
        in the future. Doesn't support variables pointing to whole entities.
        :param spark: spark session
        :param start_time: begin of extraction time window
        :param end_time: end of extraction time window
        :param system: system name, where variables will be searched from variable_names
        :param variables: iterable containing variable names, which must be registered in system. All must exist, throws
         exception otherwise
        :param variables_like: iterable containing variable name patterns, which must be registered in system
        :return: PySpark DataFrame, with one column "nxcals_timestamp" and other columns named after variables, e.g.
        "nxcals_timestamp", "BLM1:arr", "BLM2:voltage".
        """
        time_window = _to_time_window(spark, start_time, end_time)

        java_df = builders_package(spark).DataQuery.getAsPivot(
            spark._jsparkSession, time_window, system, variables, variables_like
        )

        return DataFrame(java_df, spark)


class DevicePropertyDataQuery:
    @staticmethod
    def builder(spark):
        builder = Builder(
            spark,
            builders_package(spark).DevicePropertyDataQuery.builder(
                spark._jsparkSession
            ),
        )
        return SystemStage(StartStage(EntityAliasStage(DeviceStage(builder=builder))))


class ParameterDataQuery:
    @staticmethod
    def builder(spark) -> SystemStageV2[DeviceStageV2[BuildStage[DataFrame]]]:
        builder = Builder(
            spark,
            builders_package(spark).ParameterDataQuery.builder(spark._jsparkSession),
        )
        return SystemStageV2(DeviceStageV2(BuildStage(builder=builder)))
