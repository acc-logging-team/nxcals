import os
from setuptools import find_packages, setup

# Package version meta-data.
VERSION = os.environ.get("version", "4.0")
REQUIREMENTS = os.environ.get("required_packages", "")
TEST_REQUIREMENTS = os.environ.get("required_test_packages", "")

if VERSION is None:
    print("Please define nxcals version as environment property")
else:
    print("Building wheel for version:", VERSION)

setup(
    name="nxcals-extraction-api-python3",
    version=VERSION,
    description="Python data extraction API for NXCALS system",
    long_description="Python data extraction API for NXCALS system. For more information please check docs [nxcals-docs.web.cern.ch](https://nxcals-docs.web.cern.ch)",
    author="NXCALS Team (BE-CSS-CPA)",
    author_email="acc-logging-support@cern.ch",
    url="https://confluence.cern.ch/display/NXCALS",
    project_urls={
        "Documentation": "https://nxcals-docs.web.cern.ch",
        "Repository": "https://gitlab.cern.ch/acc-logging-team/nxcals",
    },
    install_requires=REQUIREMENTS.split(" "),
    packages=find_packages(exclude=("tests",)),
    include_package_data=True,
    test_suite="tests",
    tests_require=TEST_REQUIREMENTS.split(" "),
    python_requires=">= 3.9",
    classifiers=[
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.11",
    ],
)
