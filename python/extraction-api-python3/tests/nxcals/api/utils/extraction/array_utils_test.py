from unittest import TestCase
from pyspark.sql.types import IntegerType, ArrayType, StructType, StructField
from nxcals.api.utils.extraction.array_utils import (
    ArrayUtils,
    ARRAY_ELEMENTS_FIELD_NAME,
    ARRAY_DIMENSIONS_FIELD_NAME,
)


class should_build_query(TestCase):
    def test_extract_array_fields(self):
        schema = StructType(
            [
                StructField("normal_col", IntegerType(), True),
                StructField(
                    "array_field",
                    StructType(
                        [
                            StructField(
                                ARRAY_ELEMENTS_FIELD_NAME,
                                ArrayType(IntegerType()),
                                True,
                            ),
                            StructField(
                                ARRAY_DIMENSIONS_FIELD_NAME,
                                ArrayType(IntegerType()),
                                True,
                            ),
                        ]
                    ),
                    True,
                ),
            ]
        )

        extracted_fields = ArrayUtils._extract_array_fields(schema)
        self.assertEqual(extracted_fields, ["array_field"])

    def test_get_nested_array_type(self):
        nested_type = ArrayUtils._get_nested_array_type(3, IntegerType())
        self.assertIsInstance(nested_type, ArrayType)
        self.assertIsInstance(nested_type.elementType, ArrayType)
        self.assertIsInstance(nested_type.elementType.elementType, ArrayType)
        self.assertIsInstance(
            nested_type.elementType.elementType.elementType, IntegerType
        )

    def test_validate_array_fields_valid(self):
        schema = StructType(
            [
                StructField(
                    "valid_array",
                    StructType(
                        [
                            StructField(
                                ARRAY_ELEMENTS_FIELD_NAME,
                                ArrayType(IntegerType()),
                                True,
                            ),
                            StructField(
                                ARRAY_DIMENSIONS_FIELD_NAME,
                                ArrayType(IntegerType()),
                                True,
                            ),
                        ]
                    ),
                    True,
                )
            ]
        )

        ArrayUtils._validate_array_fields(schema, ["valid_array"])

    def test_validate_array_fields_invalid(self):
        schema = StructType(
            [
                StructField(
                    "invalid_array",
                    StructType(
                        [
                            StructField(
                                ARRAY_ELEMENTS_FIELD_NAME,
                                ArrayType(IntegerType()),
                                True,
                            )
                        ]
                    ),
                    True,
                )
            ]
        )

        with self.assertRaises(
            ValueError, msg="must contain both 'elements' and 'dimensions'"
        ):
            ArrayUtils._validate_array_fields(schema, ["invalid_array"])

    def test_reshape_array_valid(self):
        reshaped = ArrayUtils._reshape_array([1, 2, 3, 4], [2, 2])
        self.assertEqual(reshaped, [[1, 2], [3, 4]])

    def test_reshape_array_1D(self):
        reshaped = ArrayUtils._reshape_array([1, 2, 3, 4], [4])
        self.assertEqual(reshaped, [1, 2, 3, 4])
