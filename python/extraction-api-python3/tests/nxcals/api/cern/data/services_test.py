import unittest
from unittest.mock import MagicMock
from pyspark.sql import DataFrame
from nxcals.api.cern.data.services import ServiceFactory
from nxcals.api.extraction.data.common import nanos


class TestFundamentalService(unittest.TestCase):
    def setUp(self):
        self.spark = MagicMock()
        self.java_service_mock = MagicMock()
        self.spark._jvm.cern.nxcals.api.custom.service.fundamental.FundamentalServiceFactory.getInstance.return_value = self.java_service_mock
        self.service = ServiceFactory.fundamentals_service(self.spark)

    def test_get_for(self):
        self.java_service_mock.getFor.return_value = MagicMock()
        mock_to_time_window = self.spark._jvm.cern.nxcals.api.domain.TimeWindow.between
        mock_to_time_window.return_value = "mock_time_window"

        start_time, end_time, accelerators = (
            "2024-01-01 10:00:00.000",
            "2024-01-01 10:00:00.000",
            ["LHC"],
        )
        result = self.service.get_for(start_time, end_time, accelerators)

        mock_to_time_window.assert_called_once_with(nanos(start_time), nanos(end_time))
        self.java_service_mock.getFor.assert_called_once_with(
            "mock_time_window", set(accelerators)
        )
        self.assertIsInstance(result, DataFrame)

    def test_extract_with_fundamentals_invalid_input(self):
        with self.assertRaises(ValueError):
            self.service.get_for("start", "end", ["LHC"])


if __name__ == "__main__":
    unittest.main()
