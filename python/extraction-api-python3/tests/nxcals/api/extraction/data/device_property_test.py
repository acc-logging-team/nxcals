from datetime import datetime, timedelta
from unittest import TestCase

from nxcals.api.extraction.data.builders import DevicePropertyDataQuery
from .common_test import NopClass

spark_mock = NopClass()
system_name = "system_name"
start_time = datetime(2010, 12, 10, 1, 0, 0)
at_time_nanos = 10**9
end_time = datetime(2010, 12, 10, 3, 0, 0)
start_time_str = str(datetime(2010, 12, 10, 1, 0, 0))
end_time_str = str(datetime(2010, 12, 10, 3, 0, 0))
duration = timedelta(hours=2)
aliases = {"alias": "variable"}
single_field = "field"
many_fields_list = ["fieldA", "fieldB"]
many_fields_set = ("fieldA", "fieldB")
device = "Device"
property = "Property"
parameter = "Device/Property"


def correct_time_stage():
    return (
        DevicePropertyDataQuery.builder(spark_mock)
        .system(system_name)
        .startTime(start_time)
        .endTime(end_time)
    )


def correct_time_stage_str():
    return (
        DevicePropertyDataQuery.builder(spark_mock)
        .system(system_name)
        .startTime(start_time_str)
        .endTime(end_time_str)
    )


class should_set_system(TestCase):
    def runTest(self):
        DevicePropertyDataQuery.builder(spark_mock).system(system_name)


class should_set_start_time(TestCase):
    def runTest(self):
        DevicePropertyDataQuery.builder(spark_mock).system(system_name).startTime(
            start_time
        )


class should_set_end_time(TestCase):
    def runTest(self):
        correct_time_stage()


class should_set_end_time_str(TestCase):
    def runTest(self):
        correct_time_stage_str()


class should_set_at_time(TestCase):
    def runTest(self):
        DevicePropertyDataQuery.builder(spark_mock).system(system_name).atTime(
            start_time
        )


class should_set_at_time_in_nanos(TestCase):
    def runTest(self):
        DevicePropertyDataQuery.builder(spark_mock).system(system_name).atTime(
            at_time_nanos
        )


class should_set_duration(TestCase):
    def runTest(self):
        DevicePropertyDataQuery.builder(spark_mock).system(system_name).startTime(
            start_time
        ).duration(duration)


class should_set_aliases(TestCase):
    def runTest(self):
        correct_time_stage().fieldAliases(aliases)


class should_start_entity_phase(TestCase):
    def runTest(self):
        correct_time_stage().entity()


class should_add_device(TestCase):
    def runTest(self):
        correct_time_stage().entity().device(device)


class should_add_property(TestCase):
    def runTest(self):
        correct_time_stage().entity().device(device).property(property)


class should_add_property_before_device(TestCase):
    def runTest(self):
        with self.assertRaises(AttributeError):
            correct_time_stage().entity().property(property).device(device)


class should_add_parameter(TestCase):
    def runTest(self):
        correct_time_stage().entity().parameter(parameter)


class should_loop_after_device_property(TestCase):
    def runTest(self):
        correct_time_stage().entity().device(device).property(property).entity()


class should_loop_after_parameter(TestCase):
    def runTest(self):
        correct_time_stage().entity().parameter(device).entity()


class should_loop_after_parameters(TestCase):
    def runTest(self):
        correct_time_stage().entity().parameters([device]).entity()


class should_set_variable_before_start_time(TestCase):
    def runTest(self):
        with self.assertRaises(AttributeError):
            DevicePropertyDataQuery.builder(spark_mock).startTime(start_time).system(
                system_name
            )


class should_set_start_time_before_end_time(TestCase):
    def runTest(self):
        with self.assertRaises(AttributeError):
            DevicePropertyDataQuery.builder(spark_mock).system(system_name).endTime(
                end_time
            )


class should_set_start_time_before_duration(TestCase):
    def runTest(self):
        with self.assertRaises(AttributeError):
            DevicePropertyDataQuery.builder(spark_mock).system(system_name).duration(
                duration
            )


class should_set_end_time_before_entity(TestCase):
    def runTest(self):
        with self.assertRaises(AttributeError):
            DevicePropertyDataQuery.builder(spark_mock).system(system_name).startTime(
                start_time
            ).entity()


class should_set_end_time_before_fields(TestCase):
    def runTest(self):
        with self.assertRaises(AttributeError):
            DevicePropertyDataQuery.builder(spark_mock).system(system_name).startTime(
                start_time
            ).fields(single_field)


class should_set_entity_before_device(TestCase):
    def runTest(self):
        with self.assertRaises(AttributeError):
            correct_time_stage().device(device, property)


class should_set_entity_before_parameter(TestCase):
    def runTest(self):
        with self.assertRaises(AttributeError):
            correct_time_stage().parameter(parameter)


class should_require_key_value_between_entities(TestCase):
    def runTest(self):
        with self.assertRaises(AttributeError):
            correct_time_stage().entity().entity()


class should_not_accept_device_after_device(TestCase):
    def runTest(self):
        with self.assertRaises(AttributeError):
            correct_time_stage().entity().device(device).device(device)


class should_not_accept_parameter_after_parameter(TestCase):
    def runTest(self):
        with self.assertRaises(AttributeError):
            correct_time_stage().entity().parameter(parameter).parameter(parameter)
