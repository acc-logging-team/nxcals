from datetime import datetime, timedelta
from unittest import TestCase
from unittest.mock import MagicMock

from nxcals.api.extraction.data.builders import DataQuery, EntityQuery
from .common_test import NopClass, get_invocations
from nxcals.api.extraction.data.common import nanos

spark_mock = NopClass()
system_name = "system_name"
start_time = datetime(2010, 12, 10, 1, 0, 0)
end_time = datetime(2010, 12, 10, 3, 0, 0)
start_time_str = str(datetime(2010, 12, 10, 1, 0, 0))
end_time_str = str(datetime(2010, 12, 10, 3, 0, 0))
at_time_nanos = 10**9
duration = timedelta(hours=2)
aliases = {"alias": ["value"]}
single_field = "field"
many_fields_list = ["fieldA", "fieldB"]
many_fields_set = ("fieldA", "fieldB")
key = "Key"
value = "Value"
key_value = {key: value}
id = 1


def correct_time_stage():
    return (
        DataQuery.builder(spark_mock)
        .byEntities()
        .system(system_name)
        .startTime(start_time)
        .endTime(end_time)
    )


def correct_time_stage_str():
    return (
        DataQuery.builder(spark_mock)
        .byEntities()
        .system(system_name)
        .startTime(start_time_str)
        .endTime(end_time_str)
    )


class should_set_system(TestCase):
    def runTest(self):
        DataQuery.builder(spark_mock).byEntities().system(system_name)


class should_set_start_time(TestCase):
    def runTest(self):
        DataQuery.builder(spark_mock).byEntities().system(
            system_name,
        ).startTime(start_time)


class should_set_end_time(TestCase):
    def runTest(self):
        correct_time_stage()


class should_set_end_time_str(TestCase):
    def runTest(self):
        correct_time_stage_str()


class should_set_at_time(TestCase):
    def runTest(self):
        DataQuery.builder(spark_mock).byEntities().system(system_name).atTime(
            start_time
        )


class should_set_at_time_in_nanos(TestCase):
    def runTest(self):
        DataQuery.builder(spark_mock).byEntities().system(system_name).atTime(
            at_time_nanos
        )


class should_set_duration(TestCase):
    def runTest(self):
        DataQuery.builder(spark_mock).byEntities().system(system_name).startTime(
            start_time
        ).duration(duration)


class should_set_aliases(TestCase):
    def runTest(self):
        correct_time_stage().fieldAliases(aliases)


class should_set_alias(TestCase):
    def runTest(self):
        correct_time_stage().fieldAlias("alias", "field").fieldAliases(aliases)


class should_start_entity_phase(TestCase):
    def runTest(self):
        correct_time_stage().entity()


class should_add_key_value_pair(TestCase):
    def runTest(self):
        correct_time_stage().entity().keyValue(key, value)


class should_add_multipe_key_value_pairs(TestCase):
    def runTest(self):
        correct_time_stage().entity().keyValue(key, value).keyValue(
            key, value
        ).keyValue(key, value)


class should_add_key_value_pairs(TestCase):
    def runTest(self):
        correct_time_stage().entity().keyValues(key_value)


class should_loop_after_key_values(TestCase):
    def runTest(self):
        correct_time_stage().entity().keyValues(key_value).entity().keyValues(key_value)


class should_loop_after_key_value(TestCase):
    def runTest(self):
        correct_time_stage().entity().keyValue(key, value).entity().keyValue(key, value)


class should_accept_key_value_after_key_values(TestCase):
    def runTest(self):
        correct_time_stage().entity().keyValues(key_value).keyValue(key, value)


class should_set_variable_before_start_time(TestCase):
    def runTest(self):
        with self.assertRaises(AttributeError):
            DataQuery.builder(spark_mock).byEntities().startTime(start_time).system(
                system_name
            )


class should_set_start_time_before_end_time(TestCase):
    def runTest(self):
        with self.assertRaises(AttributeError):
            DataQuery.builder(spark_mock).byEntities().system(system_name).endTime(
                end_time
            )


class should_set_start_time_before_duration(TestCase):
    def runTest(self):
        with self.assertRaises(AttributeError):
            DataQuery.builder(spark_mock).byEntities().system(system_name).duration(
                duration
            )


class should_set_end_time_before_entity(TestCase):
    def runTest(self):
        with self.assertRaises(AttributeError):
            DataQuery.builder(spark_mock).byEntities().system(system_name).startTime(
                start_time
            ).entity()


class should_set_end_time_before_fields(TestCase):
    def runTest(self):
        with self.assertRaises(AttributeError):
            DataQuery.builder(spark_mock).byEntities().system(system_name).startTime(
                start_time
            ).fields(single_field)


class should_set_entity_before_key_value(TestCase):
    def runTest(self):
        with self.assertRaises(AttributeError):
            correct_time_stage().keyValue(key, value)


class should_require_key_value_between_entities(TestCase):
    def runTest(self):
        with self.assertRaises(AttributeError):
            correct_time_stage().entity().entity()


class should_pass_with_one_key_value_in_static_function(TestCase):
    def runTest(self):
        DataQuery.getForEntities(
            spark_mock,
            start_time,
            end_time,
            system_name,
            entity_queries=[EntityQuery(key_value)],
        )


class should_pass_with_one_key_value_like_in_static_function(TestCase):
    def runTest(self):
        DataQuery.getForEntities(
            spark_mock,
            start_time,
            end_time,
            system_name,
            entity_queries=[EntityQuery({}, key_value)],
        )


class should_pass_with_multiple_entity_queries_in_static_function(TestCase):
    def runTest(self):
        DataQuery.getForEntities(
            spark_mock,
            start_time,
            end_time,
            system_name,
            entity_queries=[
                EntityQuery(key_value),
                EntityQuery({}, key_value),
            ],
        )


class should_pass_with_multiple_entity_queries_and_field_aliases_in_static_function(
    TestCase
):
    def runTest(self):
        DataQuery.getForEntities(
            spark_mock,
            start_time,
            end_time,
            system_name,
            entity_queries=[
                EntityQuery(key_value),
                EntityQuery({}, key_value),
            ],
            field_aliases=aliases,
        )


class should_correctly_create_the_query_using_entities(TestCase):
    def runTest(self):
        spark_mock = NopClass()
        DataQuery.builder(spark_mock).entities().system(system_name).keyValuesEq(
            key_value
        ).keyValuesLike(key_value).keyValuesIn([key_value]).atTime(
            start_time
        ).fieldAliases(aliases).build()

        invocations = spark_mock.invocations
        self.assertIn(("system", [system_name]), invocations)
        self.assertIn(("keyValuesEq", [key_value]), invocations)
        self.assertIn(("keyValuesIn", [[key_value]]), invocations)
        self.assertIn(("keyValuesLike", [key_value]), invocations)
        self.assertIn(("atTime", [nanos(start_time)]), invocations)
        self.assertIn(("fieldAliases", [aliases]), invocations)
        self.assertIn(("build", []), invocations)


class should_correctly_escape_special_character_in_value(TestCase):
    def runTest(self):
        spark_mock = NopClass()
        key_value_with_wildcard = {"key-value-with-wildcard": "value%"}
        key_value_with_escaped_wildcard = {"key-value-with-wildcard": "value\\%"}

        DataQuery.getForEntities(
            spark_mock,
            start_time,
            end_time,
            system_name,
            [EntityQuery(key_value_with_wildcard, key_value)],
        )

        invocations = spark_mock.invocations
        self.assertIn(("system", [system_name]), invocations)
        self.assertIn(
            ("keyValuesLike", [dict(key_value_with_escaped_wildcard, **key_value)]),
            invocations,
        )
        self.assertIn(("timeWindow", [nanos(start_time), nanos(end_time)]), invocations)
        self.assertIn(("build", []), invocations)


class should_not_escape_if_value_is_not_string(TestCase):
    def runTest(self):
        spark_mock = NopClass()
        key_value_with_wildcard = {"key-value-with-wildcard": []}

        DataQuery.getForEntities(
            spark_mock,
            start_time,
            end_time,
            system_name,
            [EntityQuery(key_value_with_wildcard, key_value)],
        )

        invocations = spark_mock.invocations
        self.assertIn(("system", [system_name]), invocations)
        self.assertIn(
            ("keyValuesLike", [dict(key_value_with_wildcard, **key_value)]), invocations
        )
        self.assertIn(("timeWindow", [nanos(start_time), nanos(end_time)]), invocations)
        self.assertIn(("build", []), invocations)


class should_accept_id_and_later_another_entity(TestCase):
    def runTest(self):
        spark_mock = MagicMock()
        spark_mock._jvm.Long = MagicMock(return_value=id)

        DataQuery.builder(spark_mock).entities().system(system_name).idEq(
            id
        ).keyValuesEq(key_value).atTime(start_time).build()

        invocations = get_invocations(spark_mock)
        self.assertIn(("system", [system_name]), invocations)
        self.assertIn(("keyValuesEq", [key_value]), invocations)
        self.assertIn(("idEq", [id]), invocations)
        self.assertIn(("atTime", [nanos(start_time)]), invocations)
        self.assertIn(("build", []), invocations)


class should_accept_key_value_and_later_another_entity(TestCase):
    def runTest(self):
        spark_mock = MagicMock()
        spark_mock._jvm.Long = MagicMock(return_value=id)

        DataQuery.builder(spark_mock).entities().system(system_name).keyValuesEq(
            key_value
        ).idEq(id).atTime(start_time).build()

        invocations = get_invocations(spark_mock)
        self.assertIn(("system", [system_name]), invocations)
        self.assertIn(("keyValuesEq", [key_value]), invocations)
        self.assertIn(("idEq", [id]), invocations)
        self.assertIn(("atTime", [nanos(start_time)]), invocations)
        self.assertIn(("build", []), invocations)


class should_accept_ids_and_id(TestCase):
    def runTest(self):
        spark_mock = MagicMock()
        spark_mock._jvm.Long = MagicMock(return_value=id)

        DataQuery.builder(spark_mock).entities().system(system_name).idIn({id}).idEq(
            id
        ).atTime(start_time).build()

        invocations = get_invocations(spark_mock)
        self.assertIn(("system", [system_name]), invocations)
        self.assertEqual(
            2, len(list(filter(lambda x: x == ("idEq", [id]), invocations)))
        )
        self.assertIn(("atTime", [nanos(start_time)]), invocations)
        self.assertIn(("build", []), invocations)


class should_accept_id_and_ids(TestCase):
    def runTest(self):
        spark_mock = MagicMock()
        spark_mock._jvm.Long = MagicMock(return_value=id)

        DataQuery.builder(spark_mock).entities().system(system_name).idEq(id).idIn(
            {id}
        ).atTime(start_time).build()

        invocations = get_invocations(spark_mock)
        self.assertIn(("system", [system_name]), invocations)
        self.assertEqual(
            2, len(list(filter(lambda x: x == ("idEq", [id]), invocations)))
        )
        self.assertIn(("atTime", [nanos(start_time)]), invocations)
        self.assertIn(("build", []), invocations)


class should_accept_id_and_ids_without_system(TestCase):
    def runTest(self):
        spark_mock = MagicMock()
        spark_mock._jvm.Long = lambda x: int(x)

        DataQuery.builder(spark_mock).entities().idEq(1).idEq(2).idEq(1).idIn(
            {3, 4}
        ).atTime(start_time).build()

        invocations = get_invocations(spark_mock)
        self.assertEqual(
            2, len(list(filter(lambda x: x == ("idEq", [1]), invocations)))
        )
        self.assertIn(("idEq", [2]), invocations)
        self.assertIn(("idEq", [3]), invocations)
        self.assertIn(("idEq", [4]), invocations)
        self.assertIn(("atTime", [nanos(start_time)]), invocations)
        self.assertIn(("build", []), invocations)


class should_accept_id_and_key_value(TestCase):
    def runTest(self):
        spark_mock = MagicMock()
        spark_mock._jvm.Long = MagicMock(return_value=id)

        DataQuery.builder(spark_mock).entities().idEq(id).system(
            system_name
        ).keyValuesEq(key_value).atTime(start_time).build()

        invocations = get_invocations(spark_mock)
        self.assertIn(("system", [system_name]), invocations)
        self.assertIn(("keyValuesEq", [key_value]), invocations)
        self.assertIn(("idEq", [id]), invocations)
        self.assertIn(("atTime", [nanos(start_time)]), invocations)
        self.assertIn(("build", []), invocations)


class should_accept_id_and_system(TestCase):
    def runTest(self):
        spark_mock = MagicMock()
        spark_mock._jvm.Long = MagicMock(return_value=id)

        DataQuery.builder(spark_mock).entities().idEq(id).system(system_name).atTime(
            start_time
        ).build()

        invocations = get_invocations(spark_mock)
        self.assertIn(("system", [system_name]), invocations)
        self.assertIn(("idEq", [id]), invocations)
        self.assertIn(("atTime", [nanos(start_time)]), invocations)
        self.assertIn(("build", []), invocations)
