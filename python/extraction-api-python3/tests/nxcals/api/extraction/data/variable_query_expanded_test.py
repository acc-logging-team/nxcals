from datetime import datetime, timedelta
from typing import Union
from unittest import TestCase
from unittest.mock import MagicMock

from nxcals.api.extraction.data.builders_expanded import DataQuery
from nxcals.api.extraction.data.common import nanos
from .common_test import NopClass, get_invocations

spark_mock = NopClass()
variable_name = "variable_name"
aliases = {"alias": "variable"}
system_name = "system_name"
start_time = datetime(2010, 12, 10, 1, 0, 0)
end_time = datetime(2010, 12, 10, 3, 0, 0)
start_time_str = str(datetime(2010, 12, 10, 1, 0, 0))
end_time_str = str(datetime(2010, 12, 10, 3, 0, 0))
duration = timedelta(hours=2)
id = 1


class VariableTestCase(TestCase):
    def check_invocations_of_variable_query_in_mock(
        self, spark_mock: Union[NopClass, MagicMock]
    ):
        invocations = (
            get_invocations(spark_mock)
            if isinstance(spark_mock, MagicMock)
            else spark_mock.invocations
        )
        self.assertIn(("system", [system_name]), invocations)
        self.assertIn(("build", []), invocations)


class should_set_system(TestCase):
    def runTest(self):
        DataQuery.builder(spark_mock).byVariables().system(system_name)


class should_set_start_time(TestCase):
    def runTest(self):
        DataQuery.builder(spark_mock).byVariables().system(
            system_name,
        ).startTime(start_time)


class should_set_end_time(TestCase):
    def runTest(self):
        DataQuery.builder(spark_mock).byVariables().system(
            system_name,
        ).startTime(start_time).endTime(end_time)


class should_set_end_time_str(TestCase):
    def runTest(self):
        DataQuery.builder(spark_mock).byVariables().system(
            system_name,
        ).startTime(start_time_str).endTime(end_time_str)


class should_set_at_time(TestCase):
    def runTest(self):
        DataQuery.builder(spark_mock).byVariables().system(
            system_name,
        ).atTime(start_time)


class should_set_duration(TestCase):
    def runTest(self):
        DataQuery.builder(spark_mock).byVariables().system(
            system_name,
        ).startTime(start_time).duration(duration)


class should_set_alias(TestCase):
    def runTest(self):
        DataQuery.builder(spark_mock).byVariables().system(system_name).startTime(
            start_time
        ).duration(duration).fieldAliases(aliases)


class should_set_multiple_aliases(TestCase):
    def runTest(self):
        DataQuery.builder(spark_mock).byVariables().system(system_name).startTime(
            start_time
        ).duration(duration).fieldAliases(aliases).fieldAliases(aliases)


class should_set_variable_with_alias(TestCase):
    def runTest(self):
        DataQuery.builder(spark_mock).byVariables().system(system_name).atTime(
            start_time
        ).fieldAliases(aliases).variable(variable_name)


class should_set_variable_with_multiple_aliases(TestCase):
    def runTest(self):
        DataQuery.builder(spark_mock).byVariables().system(system_name).atTime(
            start_time
        ).fieldAliases(aliases).fieldAliases(aliases).fieldAlias(
            "alias", "field"
        ).variable(variable_name)


class should_not_allow_alias_after_variable(TestCase):
    def runTest(self):
        with self.assertRaises(AttributeError):
            DataQuery.builder(spark_mock).byVariables().startTime(variable_name).system(
                system_name
            ).atTime(start_time).variable(variable_name).fieldAliases(aliases)


class should_set_variable(TestCase):
    def runTest(self):
        DataQuery.builder(spark_mock).byVariables().system(
            system_name,
        ).atTime(start_time).variable(variable_name)


class should_set_variables(TestCase):
    def runTest(self):
        DataQuery.builder(spark_mock).byVariables().system(
            system_name,
        ).atTime(start_time).variables([variable_name])


class should_allow_multiples_variables(TestCase):
    def runTest(self):
        DataQuery.builder(spark_mock).byVariables().system(system_name).atTime(
            start_time,
        ).variable(variable_name).variable(variable_name)


class should_set_system_before_start_time(TestCase):
    def runTest(self):
        with self.assertRaises(AttributeError):
            DataQuery.builder(spark_mock).byVariables().startTime(
                variable_name,
            ).system(system_name)


class should_set_start_time_before_end_time(TestCase):
    def runTest(self):
        with self.assertRaises(AttributeError):
            DataQuery.builder(spark_mock).byVariables().system(
                system_name,
            ).endTime(end_time).startTime(start_time)


class should_set_start_time_before_duration(TestCase):
    def runTest(self):
        with self.assertRaises(AttributeError):
            DataQuery.builder(spark_mock).byVariables().system(
                system_name,
            ).variable(variable_name).duration(duration)


class should_pass_with_variable_id(VariableTestCase):
    def runTest(self):
        spark_mock = MagicMock()
        spark_mock._jvm.Long = MagicMock(return_value=id)

        DataQuery.builder(spark_mock).variables().system(system_name).idEq(id).atTime(
            start_time
        ).fieldAliases(aliases).build()

        self.check_invocations_of_variable_query_in_mock(spark_mock)
        invocations = get_invocations(spark_mock)

        self.assertIn(("idEq", [id]), invocations)
        self.assertIn(("atTime", [nanos(start_time_str)]), invocations)
        self.assertIn(("fieldAliases", [aliases]), invocations)


class should_pass_with_variable_ids(VariableTestCase):
    def runTest(self):
        spark_mock = MagicMock()
        spark_mock._jvm.Long = MagicMock(return_value=id)

        DataQuery.builder(spark_mock).variables().system(system_name).idIn({id}).atTime(
            start_time
        ).fieldAliases(aliases).build()

        self.check_invocations_of_variable_query_in_mock(spark_mock)
        invocations = get_invocations(spark_mock)

        self.assertEqual(
            1, len(list(filter(lambda x: x == ("idEq", [id]), invocations)))
        )
        self.assertIn(("atTime", [nanos(start_time_str)]), invocations)
        self.assertIn(("fieldAliases", [aliases]), invocations)


class should_pass_with_variable_ids_and_id(VariableTestCase):
    def runTest(self):
        spark_mock = MagicMock()
        spark_mock._jvm.Long = MagicMock(return_value=id)

        DataQuery.builder(spark_mock).variables().system(system_name).idIn({id}).idEq(
            id
        ).atTime(start_time).fieldAliases(aliases).build()

        self.check_invocations_of_variable_query_in_mock(spark_mock)
        invocations = get_invocations(spark_mock)

        self.assertEqual(
            2, len(list(filter(lambda x: x == ("idEq", [id]), invocations)))
        )
        self.assertIn(("atTime", [nanos(start_time_str)]), invocations)
        self.assertIn(("fieldAliases", [aliases]), invocations)


class should_pass_with_variable_id_and_name(VariableTestCase):
    def runTest(self):
        spark_mock = MagicMock()
        spark_mock._jvm.Long = MagicMock(return_value=id)

        DataQuery.builder(spark_mock).variables().system(system_name).idEq(id).nameEq(
            variable_name
        ).atTime(start_time).fieldAliases(aliases).build()

        self.check_invocations_of_variable_query_in_mock(spark_mock)
        invocations = get_invocations(spark_mock)

        self.assertIn(("nameEq", [variable_name]), invocations)
        self.assertIn(("idEq", [id]), invocations)
        self.assertIn(("atTime", [nanos(start_time_str)]), invocations)
        self.assertIn(("fieldAliases", [aliases]), invocations)


class should_pass_with_variable_name_and_id(VariableTestCase):
    def runTest(self):
        spark_mock = MagicMock()
        spark_mock._jvm.Long = MagicMock(return_value=id)

        DataQuery.builder(spark_mock).variables().system(system_name).nameEq(
            variable_name
        ).idEq(id).atTime(start_time).fieldAliases(aliases).build()

        self.check_invocations_of_variable_query_in_mock(spark_mock)
        invocations = get_invocations(spark_mock)

        self.assertIn(("nameEq", [variable_name]), invocations)
        self.assertIn(("idEq", [id]), invocations)
        self.assertIn(("atTime", [nanos(start_time_str)]), invocations)
        self.assertIn(("fieldAliases", [aliases]), invocations)


class should_pass_with_variable_name_and_id_in_query_in_different_order(
    VariableTestCase
):
    def runTest(self):
        spark_mock = MagicMock()
        spark_mock._jvm.Long = MagicMock(return_value=id)

        DataQuery.builder(spark_mock).variables().idEq(id).idEq(id).system(
            system_name
        ).nameEq(variable_name).atTime(start_time).fieldAliases(aliases).build()

        self.check_invocations_of_variable_query_in_mock(spark_mock)
        invocations = get_invocations(spark_mock)

        self.assertIn(("nameEq", [variable_name]), invocations)
        self.assertIn(("idEq", [id]), invocations)
        self.assertIn(("atTime", [nanos(start_time_str)]), invocations)
        self.assertIn(("fieldAliases", [aliases]), invocations)


class should_pass_with_and_ids_in_query_without_system(VariableTestCase):
    def runTest(self):
        spark_mock = MagicMock()
        spark_mock._jvm.Long = lambda x: int(x)

        DataQuery.builder(spark_mock).variables().idEq(id).idEq(id).idEq(2).idIn(
            {3, 4}
        ).atTime(start_time).fieldAliases(aliases).build()

        invocations = get_invocations(spark_mock)

        self.assertIn(("idEq", [id]), invocations)
        self.assertIn(("idEq", [2]), invocations)
        self.assertIn(("idEq", [3]), invocations)
        self.assertIn(("idEq", [4]), invocations)
        self.assertIn(("atTime", [nanos(start_time_str)]), invocations)
        self.assertIn(("fieldAliases", [aliases]), invocations)
        self.assertIn(("build", []), invocations)
