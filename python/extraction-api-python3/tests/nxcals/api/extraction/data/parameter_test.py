from datetime import datetime, timedelta
from unittest import TestCase

from nxcals.api.extraction.data.builders import ParameterDataQuery
from nxcals.api.extraction.data.common import nanos
from .common_test import NopClass

system_name = "system_name"
start_time = datetime(2010, 12, 10, 1, 0, 0)
at_time_nanos = 10**9
end_time = datetime(2010, 12, 10, 3, 0, 0)
start_time_str = str(datetime(2010, 12, 10, 1, 0, 0))
end_time_str = str(datetime(2010, 12, 10, 3, 0, 0))
duration = timedelta(hours=2)
aliases = {"alias": ("variable")}
single_field = "field"
many_fields_list = ["fieldA", "fieldB"]
many_fields_set = ("fieldA", "fieldB")
device = "Device"
property = "Property"
parameter = "Device/Property"


class should_build_query(TestCase):
    def runTest(self):
        spark_mock = NopClass()
        ParameterDataQuery.builder(spark_mock).system(system_name).deviceEq(
            device
        ).propertyEq(property).deviceLike(device).propertyLike(property).parameterEq(
            parameter
        ).parameterIn([parameter]).parameterLike(parameter).timeWindow(
            start_time_str, end_time_str
        ).fieldAliases(aliases).build()
        invocations = spark_mock.invocations
        self.assertIn(("system", [system_name]), invocations)
        self.assertIn(("deviceEq", [device]), invocations)
        self.assertIn(("deviceLike", [device]), invocations)
        self.assertIn(("propertyEq", [property]), invocations)
        self.assertIn(("propertyLike", [property]), invocations)
        self.assertIn(("parameterEq", [parameter]), invocations)
        self.assertIn(("parameterIn", [[parameter]]), invocations)
        self.assertIn(("parameterLike", [parameter]), invocations)
        self.assertIn(
            ("timeWindow", [nanos(start_time_str), nanos(end_time_str)]), invocations
        )
        self.assertIn(("fieldAliases", [aliases]), invocations)
        self.assertIn(("build", []), invocations)
