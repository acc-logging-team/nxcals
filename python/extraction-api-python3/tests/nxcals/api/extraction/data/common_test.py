import numpy as np
from datetime import datetime, timedelta
from typing import Any, List, Tuple
from unittest import TestCase
from unittest.mock import MagicMock
from zoneinfo import ZoneInfo

from nxcals.api.extraction.data.common import nanos


class NopClass:
    def __init__(self) -> None:
        self.last_invocation = ""
        self.invocations: List[Tuple[str, List[Any]]] = []

    def __call__(self, *args):
        self.invocations = self.invocations + [(self.last_invocation, list(args))]
        return self

    def __getattr__(self, *args):
        self.last_invocation = args[0]
        return self


spark_mock = NopClass()
builder_mock = NopClass()


class should_accept_datetime64(TestCase):
    def runTest(self):
        # nanosecond precision
        input = np.datetime64("2015-01-01T12:30:30.123456789")
        out = nanos(input)
        assert 1420115430123456789 == out


class should_accept_datetime(TestCase):
    def runTest(self):
        # microsecond precision
        input = datetime(2015, 1, 1, 12, 30, 30, 123456)
        out = nanos(input)
        assert 1420115430123456000 == out


class should_accept_numerics(TestCase):
    def runTest(self):
        # nanosecond precision
        input = 1420115430123456789
        out = nanos(input)
        assert 1420115430123456789 == out


class should_accept_numpy_int64(TestCase):
    def runTest(self):
        # nanosecond precision
        input = np.int64(1420115430123456789)
        out = nanos(input)
        assert 1420115430123456789 == out


class should_accept_strings_with_nanos(TestCase):
    def runTest(self):
        # nanosecond precision
        input = "2015-01-01 12:30:30.123456789"
        out = nanos(input)
        assert 1420115430123456789 == out


class should_accept_strings_with_micros(TestCase):
    def runTest(self):
        # microsecond precision
        input = "2015-01-01 12:30:30.123456"
        out = nanos(input)
        assert 1420115430123456000 == out


class should_accept_strings_of_different_format_with_micros(TestCase):
    def runTest(self):
        # microsecond precision
        input = "2015-01-01T12:30:30.123456"
        out = nanos(input)
        assert 1420115430123456000 == out


class should_accept_strings_of_different_format_with_nanos(TestCase):
    def runTest(self):
        # nanosecond precision
        input = "2015-01-01T12:30:30.123456789"
        out = nanos(input)
        assert 1420115430123456789 == out


class should_not_accept_unsupported_format(TestCase):
    def runTest(self):
        with self.assertRaises(ValueError):
            nanos((1, 2, 3, 4))


def get_invocations(mock: MagicMock) -> List[Tuple[str, List[Any]]]:
    return [
        (methodPath.split(".")[-1], list(args) + list(kwargs.values()))
        for (methodPath, args, kwargs) in mock.mock_calls
    ]


class TestDatetimeNanos(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.epoch = datetime(1970, 1, 1, tzinfo=ZoneInfo("UTC"))

    def test_datetime_with_utc(self):
        dt = datetime(2024, 1, 1, 12, 0, 0, tzinfo=ZoneInfo("UTC"))
        expected_ns = (dt - self.epoch).days * 86_400 * 10**9 + dt.hour * 3_600 * 10**9
        self.assertEqual(nanos(dt), expected_ns)

    def test_datetime_with_other_tz(self):
        dt = datetime(2024, 1, 1, 12, 0, 0, tzinfo=ZoneInfo("America/New_York"))
        dt_utc = dt.astimezone(ZoneInfo("UTC")).replace(tzinfo=None)
        expected_ns = (
            dt_utc - self.epoch.replace(tzinfo=None)
        ).days * 86_400 * 10**9 + dt_utc.hour * 3_600 * 10**9
        self.assertEqual(nanos(dt), expected_ns)

    def test_datetime_without_tz(self):
        dt = datetime(2024, 1, 1, 12, 0, 0)
        expected_ns = (
            dt - datetime(1970, 1, 1)
        ).days * 86_400 * 10**9 + dt.hour * 3_600 * 10**9
        self.assertEqual(nanos(dt), expected_ns)

    def test_datetime_with_microseconds(self):
        dt = datetime(2024, 1, 1, 12, 34, 56, 789123, tzinfo=ZoneInfo("UTC"))
        expected_ns = (
            (dt - self.epoch).days * 86_400 * 10**9
            + dt.hour * 3_600 * 10**9
            + dt.minute * 60 * 10**9
            + dt.second * 10**9
            + dt.microsecond * 10**3
        )
        self.assertEqual(nanos(dt), expected_ns)

    def test_zero_datetime(self):
        dt = datetime(1970, 1, 1, tzinfo=ZoneInfo("UTC"))
        self.assertEqual(nanos(dt), 0)


class TestTimedeltaNanos(TestCase):
    def test_timedelta(self):
        td = timedelta(days=1, hours=2, minutes=30, seconds=15, microseconds=500000)
        expected_ns = (
            td.days * 86_400 * 10**9 + td.seconds * 10**9 + td.microseconds * 10**3
        )
        self.assertEqual(nanos(td), expected_ns)

    def test_zero_timedelta(self):
        td = timedelta(0)
        self.assertEqual(nanos(td), 0)


class TestNumpyDatetimeNanos(TestCase):
    def test_numpy_datetime64(self):
        dt64 = np.datetime64("2024-01-01T12:34:56.123456789")
        expected_ns = int(
            (dt64 - np.datetime64("1970-01-01T00:00:00Z", "ns"))
            .astype("timedelta64[ns]")
            .astype(int)
        )
        self.assertEqual(nanos(dt64), expected_ns)


class TestStringInputNanos(TestCase):
    def test_string_input(self):
        date_str = "2024-01-01T12:34:56.123456789"
        expected_ns = int(
            (np.datetime64(date_str) - np.datetime64("1970-01-01T00:00:00Z", "ns"))
            .astype("timedelta64[ns]")
            .astype(int)
        )
        self.assertEqual(nanos(date_str), expected_ns)


class TestIntegerInputNanos(TestCase):
    def test_integer_input(self):
        self.assertEqual(nanos(1_000_000_000), 1_000_000_000)


class TestInvalidInput(TestCase):
    def test_invalid_input(self):
        with self.assertRaises(ValueError):
            nanos([1, 2, 3])  # Unsupported type
