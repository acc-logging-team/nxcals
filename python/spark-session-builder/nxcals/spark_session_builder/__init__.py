"""
Helper package to configure pyspark for NXCALS

Examples:

pip install nxcals

from nxcals.spark_session_builder import get_or_create
spark = get_or_create()
spark = get_or_create(app_name="myapp")
spark = get_or_create(service_url="")

from nxcals.spark_session_builder import get_or_create, Flavor
spark = get_or_create(flavor=Flavor.YARN_MEDIUM)
spark = get_or_create(flavor=Flavor.YARN_MEDIUM, conf={'spark.executor.memory': '5g'})
spark = get_or_create(flavor=Flavor.YARN_MEDIUM, hadoop_env="dev_mvoelkle")

import nxcals as nx
spark = nx.spark_session_builder.get_or_create(flavor=nx.spark_session_builder.Flavor.YARN_MEDIUM)
"""

import enum
import glob
import os
import sys
import zipfile
from pyhocon import ConfigFactory, ConfigTree
from pyspark.sql import SparkSession
from typing import Any, Mapping, NamedTuple, Optional
from .py4j_converters import register_spark_session_converter

register_spark_session_converter()

try:
    from py4jgw import initialize_py4jgw
except ModuleNotFoundError:
    pass  # nxcals_py4j_stubs does not have to be installed
except BaseException as ex:
    print(f"WARN: error executing 'from py4jgw import py4j_utils_importer', {ex}")

if "SPARK_HOME" not in os.environ:
    SPARK_HOME = os.path.join(sys.prefix, "nxcals-bundle")
    os.environ["SPARK_HOME"] = SPARK_HOME
else:
    SPARK_HOME = os.environ["SPARK_HOME"]


def _parse_reference_conf(directory: str, jar_glob: str):
    path = os.path.join(SPARK_HOME, directory, jar_glob)
    for jar_path in glob.glob(path):
        with zipfile.ZipFile(jar_path, "r") as jar_file:
            return ConfigFactory.parse_string(jar_file.read("reference.conf").decode())
    raise RuntimeError(f"Could not find file {path}")


def _config_items(config: ConfigTree, prefix: str = ""):
    for key, value in config.items():
        if isinstance(value, ConfigTree):
            yield from _config_items(config=value, prefix=f"{prefix}{key}.")
        else:
            yield f"{prefix}{key}", config.get_string(key)


def _get_jvm_sys_prop(spark: SparkSession, property: str) -> str:
    return spark._jvm.java.lang.System.getProperty(property)


def _set_jvm_sys_prop(spark: SparkSession, property: str, value: str):
    spark._jvm.java.lang.System.setProperty(property, value)


def _create_kerberos_relogin(spark: SparkSession, principal: str, keytab: str) -> Any:
    kerberos_relogin = spark._jvm.cern.nxcals.api.security.KerberosRelogin(
        principal, keytab, True
    )
    kerberos_relogin.start()
    return kerberos_relogin


class SparkProperties(NamedTuple):
    master: str
    conf: Mapping[str, str]


class Flavor(enum.Enum):
    LOCAL = None
    YARN_SMALL = "nxcals_session_small"
    YARN_MEDIUM = "nxcals_session_medium"
    YARN_LARGE = "nxcals_session_large"

    def get_properties(self, hadoop_env: str) -> SparkProperties:
        flavor_name = self.value
        if flavor_name is None:
            return SparkProperties(master="local[*]", conf={})

        _YARN_CONFIG = _parse_reference_conf("jars", "nxcals-hadoop-*-config-*.jar")[
            "nxcals_spark_options"
        ]
        conf = {
            key: value.replace("ENVIRONMENT", hadoop_env)
            for key, value in _config_items(_YARN_CONFIG)
        }
        _FLAVOR_CONFIGS = _parse_reference_conf(
            "nxcals_jars", "nxcals-cern-extraction-api-*.jar"
        )
        conf.update(_config_items(_FLAVOR_CONFIGS[flavor_name]))
        return SparkProperties(master="yarn", conf=conf)

    @staticmethod
    def value_of(value: str) -> "Flavor":
        for data in Flavor:
            if data.value == "nxcals_session_" + value.lower():
                return data
        raise ValueError(f"Could not find flavor {value}")


# global as there is no other place to hold it
_nxcals_relogin_service = None


def _fix_kerberos(spark: SparkSession):
    # This is needed to setup up for us to be able to call our Metadata API from within Spark jvm.
    KERBEROS_PRINCIPAL = "kerberos.principal"
    KERBEROS_KEYTAB = "kerberos.keytab"
    # Stealing those settings from spark config if set
    kerberos_principal = spark.sparkContext.getConf().get("spark.kerberos.principal")
    kerberos_keytab = spark.sparkContext.getConf().get("spark.kerberos.keytab")

    # setting them in jvm if not already present there
    if (
        kerberos_principal is not None
        and _get_jvm_sys_prop(spark, KERBEROS_PRINCIPAL) is None
    ):
        _set_jvm_sys_prop(spark, KERBEROS_PRINCIPAL, kerberos_principal)
    if (
        kerberos_keytab is not None
        and _get_jvm_sys_prop(spark, KERBEROS_KEYTAB) is None
    ):
        _set_jvm_sys_prop(spark, KERBEROS_KEYTAB, kerberos_keytab)

    # if those are finally present instantiating KerberosRelogin in jvm
    if (
        _get_jvm_sys_prop(spark, KERBEROS_PRINCIPAL) is not None
        and _get_jvm_sys_prop(spark, KERBEROS_KEYTAB) is not None
    ):
        global _nxcals_relogin_service
        _nxcals_relogin_service = _create_kerberos_relogin(
            spark,
            _get_jvm_sys_prop(spark, KERBEROS_PRINCIPAL),
            _get_jvm_sys_prop(spark, KERBEROS_KEYTAB),
        )


def get_or_create(
    app_name: Optional[str] = None,
    service_url: Optional[str] = None,
    flavor: Optional[Flavor] = Flavor.LOCAL,
    hadoop_env: str = "pro",
    master: Optional[str] = None,
    conf: Optional[Mapping[str, str]] = None,
) -> SparkSession:
    if flavor is None:
        flavor = Flavor.LOCAL

    # Setting this asap
    if flavor != Flavor.LOCAL or master == "yarn":
        os.environ["PYSPARK_PYTHON"] = "./environment/bin/python"
    else:
        os.environ["PYSPARK_PYTHON"] = sys.executable

    builder = SparkSession.builder

    if flavor != Flavor.LOCAL and master is not None and master != "yarn":
        raise RuntimeError(
            f"SparkSession is being set with {flavor} flavor but master is {master}"
        )

    properties = flavor.get_properties(hadoop_env)
    builder.master(properties.master)
    for key, value in properties.conf.items():
        builder.config(key, value)

    if app_name is not None:
        builder.appName(app_name)
    if master is not None:
        builder.master(master)

    if conf is not None:
        for key, value in conf.items():
            builder.config(key, value)

    spark = builder.getOrCreate()

    if service_url is not None:
        _set_jvm_sys_prop(spark, "service.url", service_url)

    if "py4jgw" in sys.modules and "initialize_py4jgw" in globals():
        initialize_py4jgw(spark._jvm)

    _fix_kerberos(spark)

    return spark
