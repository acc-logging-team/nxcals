package cern.nxcals.api.metadata.winccoa;

import cern.nxcals.api.config.SparkContext;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableConfig;
import cern.nxcals.api.extraction.metadata.EntityService;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.extraction.metadata.queries.Entities;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.time.Instant;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

@Import(SparkContext.class)
@SpringBootApplication
@Slf4j
public class HandleNXCALS7964 {
    static {
        String user = System.getProperty("user.name");
        //        System.setProperty("kerberos.principal", "acclog");
        //        System.setProperty("kerberos.keytab", "/opt/" + user + "/.keytab-acclog");
        System.setProperty("kerberos.principal", user);
        System.setProperty("kerberos.keytab", "/opt/" + user + "/.keytab");
        System.setProperty("service.url",
                "https://cs-ccr-nxcals6.cern.ch:19093,https://cs-ccr-nxcals7.cern.ch:19093,https://cs-ccr-nxcals8.cern.ch:19093");
        //        System.setProperty("service.url",
        //                "https://nxcals-rmucha-2.cern.ch:19093");
    }

    private static final String system = "WINCCOA";
    private static final String winccoaEntityKey = "variable_name";

    private void generateData() {
        // generate data - for test in dev env
        //        EntityService entityService = ServiceClientFactory.createEntityService();
        //        VariableService variableService = ServiceClientFactory.createVariableService();
        //        SystemSpec winccoaSystemSpec = ServiceClientFactory.createSystemSpecService().findByName(system).orElseThrow();
        //        for (String variableName : variableList) {
        //            Entity entity = entityService.createEntity(winccoaSystemSpec.getId(),
        //                    Map.of(winccoaEntityKey, variableName), Map.of("application_arcgroup", "CIET_SPSCRB.EVENTHISTORY"));
        //            //            createdEntities.put(variableName, entity);
        //            variableService.create(Variable.builder()
        //                    .systemSpec(winccoaSystemSpec)
        //                    .variableName(variableName)
        //                    .configs(ImmutableSortedSet.of(
        //                            VariableConfig.builder()
        //                                    .validity(TimeWindow.infinite())
        //                                    .entityId(entity.getId())
        //                                    .fieldName("value")
        //                                    .build()
        //                    ))
        //                    .build());
        //        }
    }

    public static void main(String[] args) throws IOException, URISyntaxException {
        ConfigurableApplicationContext context = SpringApplication.run(HandleNXCALS7964.class, args);
        //        migrate();
        //        addVariableConfigs();
    }

    private static void migrate() throws IOException, URISyntaxException {

        EntityService entityService = ServiceClientFactory.createEntityService();
        VariableService variableService = ServiceClientFactory.createVariableService();
        SystemSpec winccoaSystemSpec = ServiceClientFactory.createSystemSpecService().findByName(system).orElseThrow();

        URL resource = HandleNXCALS7964.class.getClassLoader().getResource("variable-list");
        File variablesFile = new File(resource.toURI());
        List<String> variableList = Files.readAllLines(variablesFile.toPath());

        Set<Variable> variables = variableService.findAll(Variables.suchThat().variableName().in(variableList));

        if (variables.size() != variableList.size()) {
            Set<String> foundVariableNames = variables.stream().map(Variable::getVariableName)
                    .collect(Collectors.toSet());
            Set<String> missingVariables = Sets.difference(new HashSet<>(variableList), foundVariableNames);
            throw new IllegalArgumentException(
                    "Incorrect variable list, following variables were not found: " + missingVariables);
        }

        // rename entities
        Map<String, Object>[] keyValues = variableList.stream().map(
                variableName -> Map.of("variable_name", variableName)
        ).toArray(Map[]::new);
        Set<Entity> entitiesToRename = entityService.findAll(
                Entities.suchThat().systemName().eq("WINCCOA").and().keyValues().in(winccoaSystemSpec, keyValues));

        if (entitiesToRename.size() != variableList.size()) {
            Set<String> foundEntities = entitiesToRename.stream().map(Entity::getEntityKeyValues).map(x -> x.toString())
                    .collect(Collectors.toSet());
            Set<String> missingVariables = Sets.difference(new HashSet<>(variableList), foundEntities);
            throw new IllegalArgumentException(
                    "Incorrect variable list, following entities were not found: " + missingVariables);
        }

        System.out.println(entitiesToRename.size());

        Set<Entity> modifiedEntities = new HashSet<>();
        for (Entity entity : entitiesToRename) {
            Map<String, Object> keyValue = entity.getEntityKeyValues();
            Entity modifiedEntity = entity.toBuilder()
                    .entityKeyValues(Map.of(winccoaEntityKey, keyValue.get(winccoaEntityKey).toString() + "_old"))
                    .build();
            System.out.println(modifiedEntity.getEntityKeyValues());
            modifiedEntities.add(modifiedEntity);
        }

        entityService.updateEntities(modifiedEntities);

        //                 create a new entities
        Map<String, Entity> createdEntities = new HashMap<>();
        for (String variableName : variableList) {
            Entity entity = entityService.createEntity(winccoaSystemSpec.getId(),
                    Map.of(winccoaEntityKey, variableName), Map.of("application_arcgroup", "ATLAS_AR.EVENTHISTORY"));
            createdEntities.put(variableName, entity);
        }

        //         find created entities - in case, if variable update fail
        //        Map<String, Entity> createdEntities = new HashMap<>();
        //        for (String variableName : variableList) {
        //            Entity entity = entityService.findOne(Entities.suchThat().systemName().eq(system).and().keyValues()
        //                    .eq(winccoaSystemSpec, Map.of(winccoaEntityKey, variableName))).orElseThrow();
        //            createdEntities.put(variableName, entity);
        //        }

        //         update variables

        Set<Variable> modifiedVariables = new HashSet<>();
        Instant now = Instant.now();
        for (Variable variable : variables) {
            SortedSet<VariableConfig> configs = new TreeSet<>(variable.getConfigs());
            VariableConfig closedConfig = null;
            for (VariableConfig config : new HashSet<>(configs)) {
                TimeWindow validity = config.getValidity();
                if (validity.isRightInfinite()) {
                    configs.remove(config);
                    configs.add(config.toBuilder().validity(validity.rightLimit(now)).build());
                    closedConfig = config;
                }
            }
            if (closedConfig == null) {
                log.error("No variable config was found for variable: " + variable.getVariableName());
                continue;
            }
            configs.add(
                    VariableConfig.builder()
                            .entityId(createdEntities.get(variable.getVariableName()).getId())
                            .fieldName(closedConfig.getFieldName())
                            .validity(TimeWindow.after(now))
                            .build()
            );
            modifiedVariables.add(variable.toBuilder().configs(configs).build());
        }

        variableService.updateAll(modifiedVariables);
        System.out.println("Done!");
    }

    private static void addVariableConfigs() {
        EntityService entityService = ServiceClientFactory.createEntityService();
        VariableService variableService = ServiceClientFactory.createVariableService();
        SystemSpec winccoaSystemSpec = ServiceClientFactory.createSystemSpecService().findByName(system).orElseThrow();

        Entity newEntity = entityService.findOne(Entities.suchThat().keyValues().eq(winccoaSystemSpec,
                Map.of(winccoaEntityKey, "COM_AR1N_AKXA_CBPS_7044.POSST"))).orElseThrow();

        Entity oldEntity = entityService.findOne(Entities.suchThat().keyValues().eq(winccoaSystemSpec,
                Map.of(winccoaEntityKey, "COM_AR1N_AKXA_CBPS_7044.POSST_old"))).orElseThrow();
        Instant now = Instant.now();
        Variable variable = variableService.findOne(
                Variables.suchThat().variableName().eq("COM_AR1N_AKXA_CBPS_7044.POSST")).orElseThrow();
        Variable modifiedVariable = variable.toBuilder().configs(
                ImmutableSortedSet.of(
                        VariableConfig.builder()
                                .fieldName("value")
                                .entityId(oldEntity.getId())
                                .variableName(variable.getVariableName())
                                .validity(TimeWindow.before(now))
                                .build(),
                        VariableConfig.builder()
                                .fieldName("value")
                                .entityId(newEntity.getId())
                                .variableName(variable.getVariableName())
                                .validity(TimeWindow.after(now))
                                .build()
                )
        ).build();
        variableService.update(modifiedVariable);
    }
}
