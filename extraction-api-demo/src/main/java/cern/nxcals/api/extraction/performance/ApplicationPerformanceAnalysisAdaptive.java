/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.api.extraction.performance;

import cern.nxcals.api.config.SparkContext;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.common.SystemFields;
import com.google.common.collect.Lists;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.avro.Schema;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.DoubleType;
import org.apache.spark.sql.types.FloatType;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.unsafe.hash.Murmur3_x86_32;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

/**
 * Trying to come up with the final split that will use an adaptive approach.
 * Adaptive:
 * - number of partitions per day should depend on the data sizes not to blow up the number of files and gain on the biggest ones.
 * - size of the row group should also adapt to the total size ~ aiming at 200 RG showed good results with capped at 256MB.
 * - a question from where (daily size) to start this algorithm, perhaps bigger than 10GB?
 */
@Import(SparkContext.class)
@SpringBootApplication
@Slf4j
public class ApplicationPerformanceAnalysisAdaptive {


    public static final Random rand = new Random();
    public static final String REPARTITION_PATH = "/project/nxcals/perf_tests_all_adaptive/data";
    private static final String PARTITION_COLUMN = "partition";
    private static final String ENTITY_BUCKET_COLUMN = "entityBucket";

    private static final FileSystem fileSystem = createFileSystem();
    private static final long SECONDS_IN_DAY = 24 * 60 * 60;
    private static final int INTERVAL_IN_MINUTES = 1 * 60; //our split interval
    private static final boolean SHOULD_FORCE_REPARTITION = false;


    private static final long ROW_GROUP_MIN_SIZE = 16 * 1024 * 1024L;
    private static final long ROW_GROUP_MAX_SIZE = 512 * 1024 * 1024L;
    private static final long DEFAULT_HDFS_BLOCK_SIZE = 256 * 1024 * 1024L;
    private static final long EXTENDED_HDFS_BLOCK_SIZE = 512 * 1024 * 1024L;
    private static final int ONE_GB_FILE_SIZE = 1024 * 1024 * 1024;
    //in the order of biggest to smallest, allows for easy finding the one that is first smaller than number of slots...
    public static List<PInfo> fixedPartitions = Lists.newArrayList(
            PInfo.of(2, 2), //4 1GB - 4GB
            PInfo.of(3, 2), //6
            PInfo.of(4, 2), //8
            PInfo.of(4, 3), //12
            PInfo.of(4, 4), //16
            PInfo.of(6, 3), //18
            PInfo.of(8, 3), //24
            PInfo.of(8, 4), //32
            PInfo.of(12, 3),//36
            PInfo.of(12, 4), //48
            PInfo.of(12, 5), //60
            PInfo.of(12, 6), //72
            PInfo.of(12, 7), //84
            PInfo.of(24, 4), //96
            PInfo.of(24, 5), //120
            PInfo.of(24, 6), //144
            PInfo.of(24, 7), //168
            PInfo.of(24, 8), //192
            PInfo.of(24, 9), //216
            PInfo.of(24, 10), //240
            PInfo.of(24, 12), //288 - max we currently have...
            PInfo.of(48, 7), //336
            PInfo.of(48, 8), //384
            PInfo.of(48, 9), //432
            PInfo.of(48, 10), //480
            PInfo.of(48, 12) //576
    );

    static {
//        System.setProperty("logging.config", "classpath:log4j2.yml");
//
//        //         Uncomment in order to overwrite the default security settings.
//        //        System.setProperty("javax.net.ssl.trustStore", "/opt/nxcalsuser/nxcals_cacerts");
//        //        System.setProperty("javax.net.ssl.trustStorePassword", "nxcals");
//
//        //        Kerberos credentials (or comment them out to allow them be automatically discovered via krb ticket cache)
//        //        String user = System.getProperty("user.name");
        System.setProperty("kerberos.principal", "jwozniak");
        System.setProperty("kerberos.keytab", "/opt/jwozniak/.keytab");
//
//        //         TEST Env
//        //        System.setProperty("service.url", "https://nxcals-test4.cern.ch:19093,https://nxcals-test5.cern.ch:19093,https://nxcals-test6.cern.ch:19093");
//        //         TESTBED
//        //        System.setProperty("service.url",
//        //                "https://cs-ccr-testbed2.cern.ch:19093,https://cs-ccr-testbed3.cern.ch:19093,https://cs-ccr-nxcalstbs1.cern.ch:19093");
//        //         PRO
        System.setProperty("service.url",
                "https://cs-ccr-nxcals6.cern.ch:19093,https://cs-ccr-nxcals7.cern.ch:19093,https://cs-ccr-nxcals8.cern.ch:19093");
//        //Hadoop 3
//        System.setProperty("service.url",
//                "https://cs-ccr-nxcalstst2.cern.ch:19093,https://cs-ccr-nxcalstst3.cern.ch:19093,https://cs-ccr-nxcalstst4.cern.ch:19093");
    }

    private static SystemSpecService systemSpecService = ServiceClientFactory.createSystemSpecService();

    public static void main(String[] args) {

        ConfigurableApplicationContext context = SpringApplication.run(ApplicationPerformanceAnalysisAdaptive.class, args);
        SparkSession sparkSession = context.getBean(SparkSession.class);


        List<String> inputPaths = getInputPaths();
        Collections.reverse(fixedPartitions);

        for (String path : inputPaths) {
            log.info("Start processing {}", path);
            Pair<List<String>, PInfo> info = repartitionToPath(sparkSession, path, SHOULD_FORCE_REPARTITION, 5);
            for (int i = 0; i < 1; ++i) {
                log.info("************ Run {}", i);
                List<Query> queries = getQueries(sparkSession, info);
                log.info("Using queries {}", queries);
                log.info("Start querying orginal data");
                List<String> orgPaths = constructPaths(path, 5);
                List<QueryResult> original = queryInPath(sparkSession, orgPaths, queries, info.getRight(), false, false);
                log.info("Start querying repartitioned data");
                List<QueryResult> repartitioned = queryInPath(sparkSession, info.getLeft(), queries, info.getRight(), true, true);

                compareResults(info.getRight(), orgPaths, original, info.getLeft(), repartitioned);
            }
        }
    }

    private static List<String> constructPaths(String path, int daysInPast) {
        List<String> paths = new ArrayList<>();
        for (int i = 0; i < daysInPast; ++i) {
            paths.add(buidPath(path, i));
        }
        return paths;
    }

    private static FileSystem createFileSystem() {
        org.apache.hadoop.conf.Configuration conf = new org.apache.hadoop.conf.Configuration();
        try {
            return FileSystem.get(conf);
        } catch (IOException e) {
            throw new UncheckedIOException("Cannot access filesystem", e);
        }
    }

    private static void warmUpSpark(SparkSession sparkSession) {
        log.info("Warming up the spark session and java");
        for (int i = 0; i < 10; i++) {
            sparkSession.read().parquet("/project/nxcals/nxcals_pro/data/2/14074/15804/2021/8/15").distinct().collectAsList();
            sparkSession.read().parquet("/project/nxcals/nxcals_pro/data/2/14074/15804/2021/8/15").select(SystemFields.NXC_ENTITY_ID.getValue()).distinct().collectAsList();
        }
        log.info("Warm up finished");
    }

    private static Map<String, QueryAnalysis> analyseQueries(List<QueryResult> original, List<QueryResult> repartitioned) {
        //short, medium, long queries
        Map<String, List<Pair<QueryResult, QueryResult>>> querySplits = splitResults(original, repartitioned);


        return querySplits.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey,
                entry -> {

                    long better = 0;
                    long worse = 0;

                    long betterSum = 0;
                    long worseSum = 0;

                    for (int i = 0; i < entry.getValue().size(); i++) {
                        QueryResult org = entry.getValue().get(i).getLeft();
                        QueryResult rep = entry.getValue().get(i).getRight();
                        check(org, rep);
                        long orgAvgTime = average(org);
                        long repAvgTime = average(rep);
                        long diff = Math.abs(orgAvgTime - repAvgTime);
                        long percent = (long) (((double) diff / (double) orgAvgTime) * 100);
                        if (orgAvgTime > repAvgTime) {
                            log.info("For query {}  BETTER for rep by {}% diff={} org={} rep={}", org.query, percent, diff, orgAvgTime, repAvgTime);
                            better++;
                            betterSum += percent;
                        } else {
                            log.warn("**** For query {} WORSE for rep by {}% diff={} org={} rep={}", org.query, percent, diff, orgAvgTime, repAvgTime);
                            worse++;
                            worseSum += percent;
                        }

                    }

                    long betterAvg = 0;
                    long worseAvg = 0;
                    if (better > 0) {
                        betterAvg = betterSum / better;
                    }
                    if (worse > 0) {
                        worseAvg = worseSum / worse;
                    }

                    return new QueryAnalysis(better, worse, betterAvg, worseAvg);

                }));

    }

    private static Map<String, List<Pair<QueryResult, QueryResult>>> splitResults(List<QueryResult> original, List<QueryResult> repartitioned) {
        Map<String, List<Pair<QueryResult, QueryResult>>> ret = new HashMap();
        ret.put("SHORT", new ArrayList<>());
        ret.put("MEDIUM", new ArrayList<>());
        ret.put("LONG", new ArrayList<>());


        for (int i = 0; i < original.size(); i++) {
            QueryResult org = original.get(i);
            QueryResult rep = repartitioned.get(i);
            check(org, rep);
            if (org.getQuery().getQueryDuration().toMinutes() < 60) {//short query
                ret.get("SHORT").add(Pair.of(org, rep));
            } else if (org.getQuery().getQueryDuration().toHours() < 24) {//medium query
                ret.get("MEDIUM").add(Pair.of(org, rep));
            } else { //long query
                ret.get("LONG").add(Pair.of(org, rep));
            }
        }
        return ret;
    }

    private static void compareResults(PInfo pInfo, List<String> orgPaths, List<QueryResult> original, List<String> repartitionedPaths, List<QueryResult> repartitioned) {
        log.info("Comparing results for {} repartitioned to {}", orgPaths, repartitionedPaths);
        log.info("Query times for original: {}", original);
        log.info("Query times for repartioned: {}", repartitioned);

        Map<String, QueryAnalysis> resultMap = analyseQueries(original, repartitioned);

        Directory orgDir = analysePath(orgPaths.get(0));
        Directory repDir = analysePath(repartitionedPaths.get(0));

        long countPercent = (long) ((Math.abs(repDir.fileCount - orgDir.fileCount) / (double) orgDir.fileCount) * 100);
        long sizePercent = (long) ((Math.abs(repDir.totalSize - orgDir.totalSize) / (double) orgDir.totalSize) * 100);

        resultMap.forEach((key, value) ->
                log.info("Finished analysis for {} queryType={}, split={}, better {} with avg {}% worse {} with avg {}% (org dir count: {} {} MB, rep dir count: {} {} MB, count diff: {}% size diff: {}%)",
                        repartitionedPaths.get(0),
                        key,
                        pInfo,
                        value.betterCount, value.betterAvg, value.worseCount, value.worseAvg,
                        orgDir.fileCount,
                        String.format("%,.3f", orgDir.totalSize / (1024 * 1024.0)),
                        repDir.fileCount,
                        String.format("%,.3f", repDir.totalSize / (1024 * 1024.0)),
                        countPercent, sizePercent));

    }

    @SneakyThrows
    private static Directory analysePath(String path) {
        RemoteIterator<LocatedFileStatus> iterator = fileSystem.listFiles(new Path(path), true);
        long count = 0;
        long size = 0;
        while (iterator.hasNext()) {
            LocatedFileStatus file = iterator.next();
            if (file.isFile() && file.getPath().getName().endsWith(".parquet")) {
                size += file.getLen();
                count++;
            }
        }
        return new Directory(count, size);
    }

    private static void check(QueryResult org, QueryResult rep) {
        if (org.count != rep.count || !org.query.equals(rep.query)) {
            throw new RuntimeException("This is not the same query q1=" + org + " q2=" + rep);
        }
    }

    private static long average(QueryResult org) {
        return (long) org.queryTimes.stream().mapToLong(r -> r).average().orElseThrow(() -> new RuntimeException("Cannot calculate average"));
    }

    private static List<QueryResult> queryInPath(SparkSession sparkSession, List<String> paths, List<Query> queries, PInfo partition, boolean usePartition, boolean useEntityBuckets) {
        log.info("Start querying in paths {}", paths);
        String timestampColumn = getTimestampColumn(paths.get(0));

        //Cannot parallelize as this introduces a very big random factor to query times as they affect each other.
        return queries.stream().map(query -> query(sparkSession, paths, query, timestampColumn, partition, usePartition, useEntityBuckets)).collect(Collectors.toList());
    }

    private static QueryResult query(SparkSession sparkSession, List<String> paths, Query query, String timestampColumn, PInfo partition, boolean usePartition, boolean useEntityBuckets) {
        long count = -1;

        long entityBucket = getBucket(query.entity, partition.getEntityBuckets());

        List<String> filteredPaths = filterPaths(paths, query, partition, usePartition);
        log.debug("Paths filtered {} for query {}", filteredPaths, query);
        Dataset<Row> dataset = sparkSession.read().parquet(filteredPaths.toArray(new String[0]));
        Column[] columns = getColumns(dataset, query);
        List<Long> queryTimes = new ArrayList<>();

        for (int i = 0; i < 3; ++i) { //query multiple times for the same dataset to take average.
            long start = System.currentTimeMillis();

            long startPartition = getPartition(query.timeWindow.getStartTimeNanos(), partition.getTimePartitions());
            long endPartition = getPartition(query.timeWindow.getEndTimeNanos(), partition.getTimePartitions());
            String where = SystemFields.NXC_ENTITY_ID.getValue() +
                    " = " + query.entity +
                    " and " + timestampColumn + " >= " + query.timeWindow.getStartTimeNanos() +
                    " and " + timestampColumn + " <= " + query.timeWindow.getEndTimeNanos();

//            if (usePartition) {
//                where += " and partition >= " + startPartition + " and partition <= " + endPartition;
//            }
//            if (useEntityBuckets) {
//
//                where += " and " + ENTITY_BUCKET_COLUMN + " = " + entityBucket;
//            }

            Dataset<Row> queryDs = dataset.select(columns).where(where);
            List<Row> rows = queryDs.collectAsList();
            long queryTime = System.currentTimeMillis() - start;
//            queryDs.explain(true);
            log.info("Run a query for {} in paths {} took {} ms, count={}, minPart={}, maxPart={}, entityBucket={} usePart={} useEntityBuckets={}", query, filteredPaths, queryTime, rows.size(), startPartition, endPartition, entityBucket, usePartition, useEntityBuckets);
            queryTimes.add(queryTime);
            count = rows.size();
        }

        return new QueryResult(query, queryTimes, count);
    }

    private static List<String> filterPaths(List<String> paths, Query query, PInfo partition, boolean usePartition) {
        int minDay = query.getTimeWindow().getStartTime().atZone(ZoneId.of("UCT")).getDayOfMonth();
        int maxDay = query.getTimeWindow().getEndTime().atZone(ZoneId.of("UCT")).getDayOfMonth();

        if (usePartition) {
            long entityBucket = getBucket(query.entity, partition.getEntityBuckets());
            long startPartition = getPartition(query.timeWindow.getStartTimeNanos(), partition.getTimePartitions());
            long endPartition = getPartition(query.timeWindow.getEndTimeNanos(), partition.getTimePartitions());

            return paths.stream()
                    .filter(path -> getDay(path) >= minDay && getDay(path) <= maxDay) //filter those outside scope
                    .map(path -> {
                        int day = getDay(path);

                        if (minDay == maxDay && day == minDay) {
                            //query for the same day
                            return path + "/partition=" + generateAlternations(startPartition, endPartition) + "/entityBucket=" + entityBucket + "/*.parquet";
                        } else if (day == minDay) {
                            return path + "/partition=" + generateAlternations(startPartition, (partition.getTimePartitions() - 1)) + "/entityBucket=" + entityBucket + "/*.parquet";

                        } else if (day == maxDay) {
                            return path + "/partition=" + generateAlternations(0, endPartition) + "/entityBucket=" + entityBucket + "/*.parquet";
                        } else {
                            return path + "/partition=*/entityBucket=" + entityBucket + "/*.parquet";
                        }
                    }).collect(Collectors.toList()); //add time & entity backet filtering
        } else {
            //just filter out paths outside of the query
            return paths.stream().filter(path -> getDay(path) >= minDay && getDay(path) <= maxDay).collect(Collectors.toList());
        }
    }

    private static String generateAlternations(long start, long end) {
        return "{" +

                LongStream.rangeClosed(start, end).boxed()
                        .map(String::valueOf)
                        .collect(Collectors.joining(","))
                + "}";
    }


    // /project/nxcals/nxcals_pro/data/2/17491/210991/2021/8/15
    private static int getDay(String path) {
        return Integer.parseInt(path.split("/")[10]);
    }

    private static long getPartition(long timestamp, int partitionCount) {
//        long intervalInSeconds = SECONDS_IN_DAY / partInfo.getTimePartitions();
//        Column partitionColumn = dataset.col(timestampColumn)
//                .divide(functions.lit(TimeUtils.CONVERT_EPOCH_NANOS_TO_SECONDS_VALUE)) //to seconds only
//                .mod(functions.lit(SECONDS_IN_DAY)) //will have seconds since the beginning of the day
//                .divide(functions.lit(intervalInSeconds)) //dividing by interval lenght will get the partition number
//                .cast(DataTypes.LongType); //interested in integrals only

        return ((timestamp / TimeUtils.CONVERT_EPOCH_NANOS_TO_SECONDS_VALUE) % SECONDS_IN_DAY) / (SECONDS_IN_DAY / partitionCount);
    }

    private static Column[] getColumns(Dataset<Row> dataset, Query query) {
        return query.getFields().stream().map(dataset::col).toArray(Column[]::new);
    }

    private static List<Query> getQueries(SparkSession sparkSession, Pair<List<String>, PInfo> pathsAndPartion) {
        List<Long> entities = getEntities(sparkSession, pathsAndPartion.getKey().get(0), 3);
        List<TimeWindow> timeWindows = getTimewindows(sparkSession, pathsAndPartion);
        Set<String> fields = getFields(sparkSession, pathsAndPartion.getKey().get(0));
        log.info("For paths {} using entities {} and timewindows {}", pathsAndPartion.getKey(), entities, timeWindows);
        List<Query> queries = new ArrayList<>();

        for (Long entity : entities) {
            for (TimeWindow tw : timeWindows) {
                queries.add(new Query(entity, tw, fields));
            }
        }
        return queries;
    }

    private static Set<String> getFields(SparkSession sparkSession, String path) {
        String timestampColumn = getTimestampColumn(path);
        Dataset<Row> dataset = sparkSession.read().parquet(path);
        StructField[] fields = dataset.schema().fields();
        Set<String> queryFields = new HashSet<>();
        queryFields.add(SystemFields.NXC_ENTITY_ID.getValue());
        queryFields.add(timestampColumn);
//        queryFields.add(getArrayField(fields)); <-- generating too many OOM problems, skipping arrays for this analysis
        queryFields.add(getScalarField(fields));
        return queryFields;
    }

    private static String getScalarField(StructField[] fields) {
        for (StructField field : fields) {
            if (field.dataType() instanceof DoubleType || field.dataType() instanceof FloatType) {
                return field.name();
            }
        }
        return SystemFields.NXC_SYSTEM_ID.getValue();
    }

    private static String getArrayField(StructField[] fields) {
        for (StructField field : fields) {
            if (field.dataType() instanceof StructType) {
                return field.name();
            }
        }
        return SystemFields.NXC_PARTITION_ID.getValue();
    }

    private static PInfo selectPartitioning(Directory directory) {
        long targetNbOf1GBFiles = directory.getTotalSize() / ONE_GB_FILE_SIZE;

        for (PInfo pInfo : fixedPartitions) {
            //return the first smaller than number of slots (1GB files).
            if (pInfo.slots() < targetNbOf1GBFiles) {
                return pInfo;
            }
        }
        //if not return the smallest one (collection is reversed).
        return fixedPartitions.get(fixedPartitions.size() - 1);
    }

    @SneakyThrows
    private static Pair<List<String>, PInfo> repartitionToPath(SparkSession sparkSession, String initPath, boolean force, int daysBackwards) {
        String timestampColumn = getTimestampColumn(initPath);
        Directory directory = analysePath(initPath);
        PInfo partInfo = selectPartitioning(directory);


        List<String> outputPaths = new ArrayList<>();

        for (int i = 0; i < daysBackwards; ++i) {
            String path = buidPath(initPath, i);
            String destPathStr = path.replace("/project/nxcals/nxcals_pro/data", REPARTITION_PATH);
            Path destPath = new Path(destPathStr);
            outputPaths.add(destPathStr);
            if (!force && fileSystem.exists(destPath) && fileSystem.listStatus(destPath).length > 1) {
                log.info("Path exists {}, skipping repartition", destPath);
                continue;
            }

            long intervalInSeconds = SECONDS_IN_DAY / partInfo.getTimePartitions();
            log.info("Started repartition from  {} into {} timestamp column {}, partitioning = {}, interval_in_sec = {}", path, destPath, timestampColumn, partInfo, intervalInSeconds);
            Dataset<Row> dataset = sparkSession.read().parquet(path);


            Column partitionColumn = dataset.col(timestampColumn)
                    .divide(functions.lit(TimeUtils.CONVERT_EPOCH_NANOS_TO_SECONDS_VALUE)) //to seconds only
                    .mod(functions.lit(SECONDS_IN_DAY)) //will have seconds since the beginning of the day
                    .divide(functions.lit(intervalInSeconds)) //dividing by interval lenght will get the partition number
                    .cast(DataTypes.LongType); //interested in integrals only

            Dataset<Row> withPart = dataset.withColumn(PARTITION_COLUMN, partitionColumn);
            List<Row> partitions = withPart.select(PARTITION_COLUMN).distinct().sort(PARTITION_COLUMN).collectAsList();
            log.info("Found {} partitions: {}", partitions.size(), partitions);

            int partitionCount = partitions.size();
            //entity bucketing
            withPart = withPart.withColumn(ENTITY_BUCKET_COLUMN, functions.pmod(functions.hash(withPart.col(SystemFields.NXC_ENTITY_ID.getValue())), functions.lit(partInfo.getEntityBuckets())));
//            withPart.select(SystemFields.NXC_ENTITY_ID.getValue(), ENTITY_BUCKET_COLUMN).distinct().show(100);
            int entityBucketCount = (int) withPart.select(ENTITY_BUCKET_COLUMN).distinct().count();
            partitionCount = partitionCount * entityBucketCount;


            log.info("Saving to dest path {}", destPathStr);

            writeParquet(sparkSession, withPart, directory, destPathStr, partInfo);
            log.info("Finished repartition from  {} into {}", path, destPathStr);
        }

        Collections.reverse(outputPaths); // to have them from the oldest towards newest.
        return Pair.of(outputPaths, partInfo);

    }

    // /project/nxcals/nxcals_pro/data/2/15826/177635/2021/9/20",
    private static String buidPath(String path, int i) {

        String[] split = path.split("/");
        return "/" + split[1] + "/" + split[2] + "/" + split[3]
                + "/" + split[4] + "/" + split[5] + "/" + split[6] + "/" + split[7] + "/" + split[8]
                + "/" + split[9] + "/" + (Integer.parseInt(split[10]) - i);


    }

    private static long getBucket(long val, long nbOfBuckets) {
        //42 is used by Spark as seed...
        return pmod(Murmur3_x86_32.hashLong(val, 42), nbOfBuckets);
    }

    private static long pmod(long a, long n) {
        long r = a % n;
        if (r < 0) {
            return (r + n) % n;
        } else {
            return r;
        }
    }

    private static String getTimestampColumn(String path) {
        ///project/nxcals/nxcals_pro/data/17/29222/201119/2021/8/15
        long systemId = Long.parseLong(path.split("/")[5]);
        SystemSpec systemSpec = systemSpecService.findById(systemId).orElseThrow(() -> new RuntimeException("No such system id " + systemId));
        return new Schema.Parser().parse(systemSpec.getTimeKeyDefinitions()).getFields().stream().map(Schema.Field::name)
                .collect(Collectors.toList()).get(0);


    }

    private static void writeParquet(SparkSession sparkSession, Dataset<Row> withPart,
                                     Directory orgDirStats, String destPathStr, PInfo partInfo) {
        //paranoid check to avoid overwriting pro data...
        String timestampColumn = getTimestampColumn(destPathStr);
        if (destPathStr.contains("nxcals_pro")) {
            throw new RuntimeException("Should not have nxcals_pro in destination path for this test!!!");
        }
        long hdfsBlockSize = DEFAULT_HDFS_BLOCK_SIZE;

        //aiming at ~200 row groups as default , with min 16MB, max 256MB (HDFS block size),
        //using multiples of 16MB to normalize.

        long parquetRowGroupSize = (((orgDirStats.totalSize / 200) / ROW_GROUP_MIN_SIZE) + 1) * ROW_GROUP_MIN_SIZE;

        if (parquetRowGroupSize < ROW_GROUP_MIN_SIZE) {
            parquetRowGroupSize = ROW_GROUP_MIN_SIZE; //lower bound this is probably not needed with the normalization...
        } else if (parquetRowGroupSize > ROW_GROUP_MAX_SIZE) {
            parquetRowGroupSize = ROW_GROUP_MAX_SIZE; //upper bound is needed.
        }
        if (parquetRowGroupSize > hdfsBlockSize) {
            hdfsBlockSize = EXTENDED_HDFS_BLOCK_SIZE;
        }

        log.info("For path {} using parquet row group size {} MB, predicted row group count {}, hfds block size {} MB", destPathStr, parquetRowGroupSize / (1024 * 1024), orgDirStats.totalSize / parquetRowGroupSize, hdfsBlockSize / (1024 * 1024));

//        sparkSession.sparkContext().hadoopConfiguration().setLong("dfs.block.size", hdfsBlockSize);

        withPart
                .repartitionByRange((int) partInfo.slots(), withPart.col(PARTITION_COLUMN), withPart.col(ENTITY_BUCKET_COLUMN))
                .sortWithinPartitions(PARTITION_COLUMN, ENTITY_BUCKET_COLUMN, timestampColumn) //must add sorting by partition, otherwise unsorted order when saving by partition happens
                //https://stackoverflow.com/questions/52159938/cant-write-ordered-data-to-parquet-in-spark
                .write()
                .option("parquet.block.size", parquetRowGroupSize)
                .option("parquet.dictionary.page.size", 3_145_728)
                .option("parquet.page.size.row.check.min", 10) //this has no effect...
                .option("dfs.block.size", hdfsBlockSize)
                .partitionBy(PARTITION_COLUMN, ENTITY_BUCKET_COLUMN) //
                .mode(SaveMode.Overwrite)
                .parquet(destPathStr);

    }

    @SneakyThrows
    private static void processOutputParitions(String destPathStr) {
        log.info("Processing output path {}", destPathStr);
        Path output = new Path(destPathStr + "/partition=*/*.parquet");
        FileStatus[] fileStatuses = fileSystem.globStatus(output);
        for (FileStatus file : fileStatuses) {

            String parent = file.getPath().getParent().getName();
            String renamed = parent + "__" + file.getPath().getName();
            fileSystem.rename(file.getPath(), new Path(destPathStr + renamed));


        }
    }

    private static List<String> convertPaths(List<String> inputPaths) {
        return inputPaths.stream().map(p -> p + "/partition=*/entityBucket=*/*.parquet").collect(Collectors.toList());
    }

    private static List<TimeWindow> getTimewindows(SparkSession sparkSession, Pair<List<String>, PInfo> pathsAndPartition) {
        String timestampColumn = getTimestampColumn(pathsAndPartition.getKey().get(0));
        List<String> converted = convertPaths(pathsAndPartition.getKey());

        Dataset<Row> dataset = sparkSession.read().parquet(converted.toArray(new String[0]));

        List<Row> minMax = dataset.agg(functions.min(dataset.col(timestampColumn)).alias("min"), functions.max(dataset.col(timestampColumn)).alias("max")).collectAsList();
        log.info("Min/max timestamps {}", minMax);
        long minTs = minMax.get(0).getLong(0);
        long maxTs = minMax.get(0).getLong(1);


        List<TimeWindow> windows = new ArrayList<>();


        addShortQueries(windows, minTs, maxTs);
        addMediumQueries(windows, minTs, maxTs);
        addLongQueries(windows, minTs, maxTs);

        return windows;
    }

    //timewindows spanning 5 days
    private static void addLongQueries(List<TimeWindow> windows, long minTs, long maxTs) {
        Instant min = TimeUtils.getInstantFromNanos(minTs);
        Instant max = TimeUtils.getInstantFromNanos(maxTs);
        for (int i = 0; i < 5; i++) { //just 5 queries
            Instant start = min.plus(rand.nextInt(23 * 60) + 1L, ChronoUnit.MINUTES);
            if (TimeUtils.getNanosFromInstant(start) > maxTs) {
                start = TimeUtils.getInstantFromNanos(maxTs);
            }
            Instant stop = max.minus(rand.nextInt(23 * 60) + 1L, ChronoUnit.MINUTES);
            if (TimeUtils.getNanosFromInstant(stop) > maxTs) {
                stop = TimeUtils.getInstantFromNanos(maxTs);
            }
            windows.add(TimeWindow.between(start, stop));
        }
    }

    //timewindows longer than 1h but not longer than 24h
    private static void addMediumQueries(List<TimeWindow> windows, long minTs, long maxTs) {
        for (int i = 12; i < 108; i++) { //we have 5 days in hours
            if (rand.nextInt(100) % 10 == 0) { //randomize a bit the hours we take as mid points
                Instant mid = TimeUtils.getInstantFromNanos(minTs).plus(i, ChronoUnit.HOURS).plus(rand.nextInt(20), ChronoUnit.MINUTES);
                ;
                Instant start = mid.minus(rand.nextInt(10) + 1, ChronoUnit.HOURS);
                if (TimeUtils.getNanosFromInstant(start) > maxTs) {
                    start = TimeUtils.getInstantFromNanos(maxTs);
                }
                Instant stop = mid.plus(rand.nextInt(10) + 1, ChronoUnit.HOURS);
                if (TimeUtils.getNanosFromInstant(stop) > maxTs) {
                    stop = TimeUtils.getInstantFromNanos(maxTs);
                }
                windows.add(TimeWindow.between(start, stop));
            }
        }

    }

    //timewindows shorter than 1h
    private static void addShortQueries(List<TimeWindow> windows, long minTs, long maxTs) {
        for (int i = 1; i < 118; i++) { //we have 5 days
            if (rand.nextInt(100) % 10 == 0) { //randomize a bit the hours we take as mid points
                Instant mid = TimeUtils.getInstantFromNanos(minTs).plus(i, ChronoUnit.HOURS).plus(rand.nextInt(20), ChronoUnit.MINUTES);
                Instant start = mid.minus(rand.nextInt(20) + 1L, ChronoUnit.MINUTES);
                if (TimeUtils.getNanosFromInstant(start) > maxTs) {
                    start = TimeUtils.getInstantFromNanos(maxTs);
                }
                Instant stop = mid.plus(rand.nextInt(20) + 1L, ChronoUnit.MINUTES);
                if (TimeUtils.getNanosFromInstant(stop) > maxTs) {
                    stop = TimeUtils.getInstantFromNanos(maxTs);
                }
                windows.add(TimeWindow.between(start, stop));
            }
        }
    }

    private static long limitToCurrentDay(long timestamp, long nanos) {
        long result = timestamp + nanos;
        Instant val = TimeUtils.getInstantFromNanos(result);
        if (val.atZone(ZoneId.of("UTC")).getDayOfMonth() != TimeUtils.getInstantFromNanos(timestamp).atZone(ZoneId.of("UTC")).getDayOfMonth()) {
            return timestamp;
        } else {
            return result;
        }
    }

    private static Long generateNanos() {
        int minutes = rand.nextInt(INTERVAL_IN_MINUTES);
        return minutes * 60_000_000_000L;
    }

    private static List<Long> getEntities(SparkSession sparkSession, String path, int limit) {
        Dataset<Row> dataset = sparkSession.read().parquet(path);

        Dataset<Row> distinct = dataset.select(SystemFields.NXC_ENTITY_ID.getValue()).distinct();

        long total = distinct.count();
        log.info("Total number {} of entities in path {}", total, path);
        List<Long> entityIds =
                distinct.sample(0.1).limit(limit).collectAsList()
                        .stream().map(r -> (Long) r.getAs(SystemFields.NXC_ENTITY_ID.getValue())).collect(Collectors.toList());
        if (entityIds.isEmpty()) {
            //try without sampling
            return distinct.limit(limit)
                    .collectAsList().stream().map(r -> (Long) r.getAs(SystemFields.NXC_ENTITY_ID.getValue())).collect(Collectors.toList());
        } else {
            return entityIds;
        }

//        return Lists.newArrayList(9299294L);
    }


//    private static void writeHive(Dataset<Row> withPart, String destPathStr) {
//        withPart
//                .repartitionByRange(96, withPart.col(PARTITION_COLUMN))
////                .repartition(1)
////                .sortWithinPartitions(PARTITION_COLUMN,TIMESTAMP_COLUMN) //must add sorting by partition, otherwise unsorted order when saving by partition happens
//                .sortWithinPartitions(PARTITION_COLUMN, TIMESTAMP_COLUMN) //must add sorting by partition, otherwise unsorted order when saving by partition happens
//                //https://stackoverflow.com/questions/52159938/cant-write-ordered-data-to-parquet-in-spark
//                .drop(PARTITION_COLUMN)
//                .write()
//                .option("parquet.block.size", 64 * 1024 * 1024L)
//                .option("parquet.dictionary.page.size", 3_145_728)
////                .partitionBy(PARTITION_COLUMN)
//                .sortBy(TIMESTAMP_COLUMN)
////                .partitionBy("partition") <-- This seems to make things worse, maybe we are losing the time sorting?
//                .mode(SaveMode.Overwrite);
////                .saveAsTable(getTableName(destPathStr));
//
//
//    }

    private static List<String> getInputPathsForTest() {
        return Lists.newArrayList(
                //1GB

//                "/project/nxcals/nxcals_pro/data/2/17491/210991/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/21222/186115/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/17412/15804/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/26724/196613/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/135221/222995/2021/8/15"
                //100MB
//                "/project/nxcals/nxcals_pro/data/2/11661/14032/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/11850/244493/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/5/13394/175271/2021/8/15"
                "/project/nxcals/nxcals_pro/data/2/11490/177370/2021/8/15"
        );

    }

    private static List<String> getInputPaths() {
        return Lists.newArrayList(

////500MB++ files with more than 1 file created, aiming at 1 file here
//                "/project/nxcals/nxcals_pro/data/2/15826/177635/2021/9/20",
//                "/project/nxcals/nxcals_pro/data/2/20234/186115/2021/9/20",
//                "/project/nxcals/nxcals_pro/data/2/12561/15822/2021/9/20",
//                "/project/nxcals/nxcals_pro/data/2/127221/347498/2021/9/20",
//                "/project/nxcals/nxcals_pro/data/2/21230/186123/2021/9/20",
//                "/project/nxcals/nxcals_pro/data/2/11504/13904/2021/9/20",
//                "/project/nxcals/nxcals_pro/data/16/28221/198117/2021/9/20",
//                "/project/nxcals/nxcals_pro/data/16/27722/198117/2021/9/20",
//                "/project/nxcals/nxcals_pro/data/2/12661/186136/2021/9/20",
//                "/project/nxcals/nxcals_pro/data/2/20756/177475/2021/9/20",
//                "/project/nxcals/nxcals_pro/data/2/21239/177475/2021/9/20",
//                "/project/nxcals/nxcals_pro/data/2/13265/340492/2021/9/20",
//                "/project/nxcals/nxcals_pro/data/2/15877/177319/2021/9/20",
//                "/project/nxcals/nxcals_pro/data/2/17133/177475/2021/9/20",
//
//////                //Big files > 1GB
//                "/project/nxcals/nxcals_pro/data/2/11588/13988/2021/9/20",
//                "/project/nxcals/nxcals_pro/data/2/13323/175332/2021/9/20",
//                "/project/nxcals/nxcals_pro/data/2/30722/222492/2021/9/20",
//                "/project/nxcals/nxcals_pro/data/2/14673/186131/2021/9/20",
//                "/project/nxcals/nxcals_pro/data/2/16861/177429/2021/9/20",
//                "/project/nxcals/nxcals_pro/data/2/12941/186135/2021/9/20",
//
////                "/project/nxcals/nxcals_pro/data/2/17201/177631/2021/9/20", //ALWAYS OOM here for larger queries...
//                "/project/nxcals/nxcals_pro/data/2/12866/268492/2021/9/20",
//                "/project/nxcals/nxcals_pro/data/2/12839/266495/2021/9/20",
//                "/project/nxcals/nxcals_pro/data/2/135221/222995/2021/9/20",
//                "/project/nxcals/nxcals_pro/data/2/11768/14123/2021/9/20",
//                "/project/nxcals/nxcals_pro/data/2/20224/186112/2021/9/20",
//                "/project/nxcals/nxcals_pro/data/2/12869/175115/2021/9/20",
//                "/project/nxcals/nxcals_pro/data/2/21222/186115/2021/9/20",
//                "/project/nxcals/nxcals_pro/data/2/26724/196613/2021/9/20",
//                "/project/nxcals/nxcals_pro/data/2/112222/315498/2021/9/20", //data skewed, not all partitions are present...
                "/project/nxcals/nxcals_pro/data/2/11905/372491/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/13649/211991/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/20246/186122/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/11863/13943/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/17412/15804/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/21241/186120/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/103223/335506/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/88221/177302/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/11860/14189/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/21242/186618/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/87222/177302/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/20248/186122/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/20250/186120/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/15702/366491/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/17313/326502/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/11734/14072/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/13638/175479/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/15372/177302/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/36721/222995/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/15715/177145/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/20247/186141/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/20251/15804/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/20744/186115/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/20754/186618/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/20245/186616/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/134221/335491/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/11468/13865/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/13306/15804/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/71221/176908/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/26728/196616/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/130223/332493/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/17424/15804/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/15382/175351/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/11745/186611/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/49223/278494/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/11784/14118/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/13438/224491/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/11936/177661/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/11785/14119/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/11792/14135/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/90224/298500/2021/9/20",
                "/project/nxcals/nxcals_pro/data/2/11490/177370/2021/9/20"


        );
    }

    @Data
    public static class QueryAnalysis {
        private final long betterCount;
        private final long worseCount;
        private final long betterAvg;
        private final long worseAvg;
    }

    @Data
    public static class Query {
        private final Long entity;
        private final TimeWindow timeWindow;
        private final Set<String> fields;

        public Duration getQueryDuration() {
            return Duration.between(timeWindow.getStartTime(), timeWindow.getEndTime());
        }

    }

    @Data
    public static class QueryResult {
        private final Query query;
        private final List<Long> queryTimes;
        private final long count;


    }

    @Data
    public static class PInfo {
        private final int timePartitions;
        private final int entityBuckets;

        public static PInfo of(int timePartitions, int entityBuckets) {
            return new PInfo(timePartitions, entityBuckets);
        }

        public long slots() {
            return timePartitions * entityBuckets;
        }
    }

    @Data
    public static class Directory {
        private final long fileCount;
        private final long totalSize;
    }


//    private Dataset<Row> repartitionByTimeInterval(DataProcessingJob job, Dataset<Row> dataset) {
//        String timestampField = getTimestampCache().get(job.getSystemId());
//        Column epochTimestampCol = dataset.col(timestampField)
//                .divide(functions.lit(TimeUtils.CONVERT_EPOCH_NANOS_TO_SECONDS_VALUE))
//                .cast(DataTypes.TimestampType);
//        return dataset.withColumn(PARTITION_BY_HOUR_COLUMN, functions.hour(epochTimestampCol))
//                .repartition(col(PARTITION_BY_HOUR_COLUMN));
//    }
//
//    private Map<String, Dataset<Row>> repartitionByHourMap(DataProcessingJob job, Dataset<Row> dataset) {
//        Dataset<Row> datasetPartitionedByHour = repartitionByHour(job, dataset);
//        //remove redundant repartition column at the end of operation
//        return IntStream.range(0, 24).boxed()
//                .collect(Collectors.toMap(hour -> String.format(HOUR_DIRECTORY_NAME_FORMAT, hour),
//                        hour -> datasetPartitionedByHour.where(col(PARTITION_BY_HOUR_COLUMN)
//                                .equalTo(functions.lit(hour)))
//                                .drop(PARTITION_BY_HOUR_COLUMN)));
//    }
}

