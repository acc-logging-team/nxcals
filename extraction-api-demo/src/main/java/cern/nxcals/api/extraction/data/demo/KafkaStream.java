package cern.nxcals.api.extraction.data.demo;

import cern.nxcals.api.domain.EntitySchema;
import cern.nxcals.api.domain.RecordKey;
import cern.nxcals.api.extraction.metadata.EntityService;
import cern.nxcals.api.extraction.metadata.InternalServiceClientFactory;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.queries.EntitySchemas;
import cern.nxcals.common.avro.DefaultBytesToGenericRecordDecoder;
import cern.nxcals.internal.extraction.metadata.InternalEntitySchemaService;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KStreamBuilder;

import java.util.Properties;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;

/**
 * Created by jwozniak on 17/06/17.
 */
public class KafkaStream {

    public static void main(String[] args) throws Exception {
        System.setProperty("service.url", "http://nxcals-jwozniak1:19093");

        EntityService entityService = ServiceClientFactory.createEntityService();
        InternalEntitySchemaService schemaService = InternalServiceClientFactory.createEntitySchemaService();
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "streams-test");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "nxcals-jwozniak3:9092");
        //        props.put(StreamsConfig.KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        props.put(StreamsConfig.KEY_SERDE_CLASS_CONFIG, Serdes.Bytes().getClass().getName());
        props.put(StreamsConfig.VALUE_SERDE_CLASS_CONFIG, Serdes.Bytes().getClass().getName());
        //        props.put(StreamsConfig.VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());

        // setting offset reset to earliest so that we can re-run the demo code with the same pre-loaded data
        // Note: To re-run the demo, you need to use the offset reset tool:
        // https://cwiki.apache.org/confluence/display/KAFKA/Kafka+Streams+Application+Reset+Tool
        //        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");

        Function<Long, EntitySchema> schemaProvider = (id) -> schemaService.findOne(
                EntitySchemas.suchThat().id().eq(id))
                .orElseThrow(() -> new IllegalArgumentException("No such schema id " + id));

        DefaultBytesToGenericRecordDecoder decoder = new DefaultBytesToGenericRecordDecoder(schemaProvider);

        // Serializers/deserializers (serde) for String and Long types
        final Serde<String> stringSerde = Serdes.String();
        Serde<Bytes> bytesSerde = Serdes.Bytes();

        KStreamBuilder builder = new KStreamBuilder();
        AtomicLong count = new AtomicLong(0);

        KStream<Bytes, Bytes> stream = builder.stream(bytesSerde, bytesSerde, "CMW");
        stream.map((key, value) -> new KeyValue<>(RecordKey.deserialize(key.get()), value))
                .filter((key, value) -> key.getEntityId() == 500404)
                .map((key, value) -> new KeyValue<>(key, decoder.apply(key.getSchemaId(), value.get())))
                .foreach((key, value) -> {
                    System.err.println("got " + count.getAndIncrement() + " record for entity: " + key);
                    System.err.println("Record:" + value);
                });

        KafkaStreams streams = new KafkaStreams(builder, props);
        streams.start();

        //        KStream<String, Long> wordCounts = textLines
        //                // Split each text line, by whitespace, into words.  The text lines are the message
        //                // values, i.e. we can ignore whatever data is in the message keys and thus invoke
        //                // `flatMapValues` instead of the more generic `flatMap`.
        //                .flatMapValues(value -> Arrays.asList(value.toLowerCase().split("\\W+")))
        //                // We use `groupBy` to ensure the words are available as message keys
        //                .groupBy((key, value) -> value)
        //                // Count the occurrences of each word (message key).
        //                .count("Counts")
        //                // Convert the `KTable<String, Long>` into a `KStream<String, Long>`.
        //                .toStream();

        // Write the `KStream<String, Long>` to the output topic.
        //        wordCounts.to(stringSerde, longSerde, "streams-wordcount-output");
        Thread.sleep(1000000000);
    }
}
