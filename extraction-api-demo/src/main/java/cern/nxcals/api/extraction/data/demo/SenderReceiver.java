package cern.nxcals.api.extraction.data.demo;

import cern.cmw.datax.DataBuilder;
import cern.cmw.datax.ImmutableData;
import cern.nxcals.api.config.SparkContext;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.ingestion.Publisher;
import cern.nxcals.api.ingestion.PublisherFactory;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

@SpringBootConfiguration
@Import(SparkContext.class)
@DependsOn("kerberos")
@EnableConfigurationProperties
@PropertySource("classpath:application.yml")
@Slf4j
public class SenderReceiver {
    static {
        System.setProperty("logging.config", "classpath:log4j2.yml");
        //Please don't commit uncommented. With many boot aps in the same package other classes get loaded.

        //        System.setProperty("kafka.producer.linger.ms", "0");
        //        System.setProperty("kerberos.principal", "acclog");
        //        System.setProperty("kerberos.keytab","/afs/cern.ch/user/j/jwozniak/.keytab-acclog");
        //        System.setProperty("service.url","https://nxcals-test4.cern.ch:19093,https://nxcals-test5.cern.ch:19093,https://nxcals-test6.cern.ch:19093");
        //        System.setProperty("kafka.producer.bootstrap.servers","https://nxcals-test4.cern.ch:9092,https://nxcals-test5.cern.ch:9092,https://nxcals-test6.cern.ch:9092");
    }

    private static final Instant FROM = Instant.now();
    private static final String RANDOM_DEVICE = UUID.randomUUID().toString();
    private static final String DEVICE1 = "test_device_" + RANDOM_DEVICE;
    private static final String RANDOM_PROPERTY1 = UUID.randomUUID().toString();
    private static final String PROPERTY_PREFIX = "test_property_";
    private static final String PROPERTY1 = PROPERTY_PREFIX + RANDOM_PROPERTY1;
    private static final String PROPERTY2 = "test1property2" + RANDOM_PROPERTY1;
    private static final String RANDOM_PROPERTY3 = UUID.randomUUID().toString();
    private static final String PROPERTY3 = "test1property2" + RANDOM_PROPERTY3;
    private static final String SYSTEM = "TEST-CMW";
    private static final String CLASS1 = "test_class";
    private static final String DEVICE_KEY = "device";
    private static final String PROPERTY_KEY = "property";
    private static final String CLASS_KEY = "class";
    private static final String RECORD_VERSION_KEY = "__record_version__";
    private static final String RECORD_TIMESTAMP_KEY = "__record_timestamp__";
    private static final int MAX_VALUES = 100;
    private static final int MAX_WAIT_TIME = 10;

    @Autowired
    private SparkSession sparkSession;
    private static boolean dataVisible = false;

    public static void main(String[] args) {
        try {
            ConfigurableApplicationContext app = SpringApplication.run(SenderReceiver.class);
            SenderReceiver senderReceiver = app.getBean(SenderReceiver.class);
            senderReceiver.sendAndReceive();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void sendAndReceive() throws InterruptedException {

        Publisher<ImmutableData> publisher = PublisherFactory.newInstance()
                .createPublisher(SYSTEM, Function.identity());
        log.info("Sending data to NXCALS for device {}, props: {}, {}, {}", DEVICE1, PROPERTY1, PROPERTY2, PROPERTY3);
        for (int i = 0; i < MAX_VALUES; ++i) {
            try {
                long timestamp = FROM.plus(i, ChronoUnit.MILLIS).toEpochMilli() * 1000_000;
                //We send 100 each but the Fundamental variable is taking 50 from each (100 in total).
                publisher.publish(
                        createFundamentalDataType(i, PROPERTY1, timestamp, "__LSA_CYCLE__", "CYCLE" + (i % 10),
                                "LHC1"));
                publisher.publish(
                        createFundamentalDataType(i, PROPERTY2, timestamp, "lsaCycleName", "CYCLE" + (i % 10),
                                "LHC2"));

                publisher.publish(createDataType(i, PROPERTY3, timestamp));

            } catch (Exception e) {
                log.error("Error sending values to NXCALS", e);
                throw new IllegalStateException("Error sending values to NXCALS");
            }
        }
        log.info("Sending finished!");
        waitForData();

    }

    private static ImmutableData createDataType(int i, String property, long timestamp) {
        DataBuilder builder = ImmutableData.builder();
        builder.add(DEVICE_KEY, DEVICE1);
        builder.add(PROPERTY_KEY, property);
        builder.add(CLASS_KEY, CLASS1);
        builder.add(RECORD_VERSION_KEY, 0L);
        builder.add(RECORD_TIMESTAMP_KEY, timestamp);
        builder.add("field", i * 1000_000);
        return builder.build();

    }

    private static ImmutableData createFundamentalDataType(int i, String property, long timestamp,
            String lsaCycleProperty, String lsaCycle, String user) {
        DataBuilder builder = ImmutableData.builder();
        builder.add(DEVICE_KEY, DEVICE1);
        builder.add(PROPERTY_KEY, property);
        builder.add(CLASS_KEY, CLASS1);
        builder.add(RECORD_VERSION_KEY, 0L);
        builder.add(RECORD_TIMESTAMP_KEY, timestamp);
        builder.add(lsaCycleProperty, lsaCycle);
        builder.add("USER", user);
        builder.add("BP_NUM", i);
        return builder.build();

    }

    public void waitForData() throws InterruptedException {
        if (!dataVisible) {
            //Have to wait 30 seconds in order for the data to be processed and visible in NXCALS extraction
            log.info("Waiting max {} minutes for the data to be visible in NXCALS...", MAX_WAIT_TIME);
            waitForData(TimeUnit.MINUTES.toMillis(MAX_WAIT_TIME));
            log.info("Ok, data should be processed now, proceeding with tests");
            dataVisible = true;
        } else {
            log.info("Data already checked");
        }

    }

    private void waitForData(long maxTime) throws InterruptedException {
        long start = System.currentTimeMillis();

        while (true) {
            if (System.currentTimeMillis() - start > maxTime) {
                throw new IllegalStateException("Data was not processed in the required maxTime=" + maxTime + " ms");
            }

            if (getCount(PROPERTY3, "field", 38_000_000) == 61 && getCount(PROPERTY1) == MAX_VALUES
                    && getCount(PROPERTY2) == MAX_VALUES
                    && getCount(PROPERTY3) == MAX_VALUES) {
                return;
            }

            log.info("Still no data visible, waiting more...");
            TimeUnit.SECONDS.sleep(1);
        }
    }

    private long getCount(String property1, String field, int value) {
        //@formatter:off
        Dataset<Row> dataset = DataQuery.builder(sparkSession).byEntities().system(SYSTEM)
                .startTime(FROM)
                .endTime(FROM.plusMillis(MAX_VALUES))
                .entity().keyValueLike(DEVICE_KEY, DEVICE1).keyValue(PROPERTY_KEY, property1)
                .build();
        dataset.show(100);
        long count = dataset.where(dataset.col(field).$greater(value)).count();
        log.info("Count {}", count);
        return count;
        //@formatter:on

    }

    private long getCount(String property1) {
        //@formatter:off
        return DataQuery.builder(sparkSession).byEntities().system(SYSTEM)
                .startTime(FROM)
                .endTime(FROM.plusMillis(MAX_VALUES))
                .entity().keyValueLike(DEVICE_KEY, DEVICE1).keyValue(PROPERTY_KEY, property1)
                .build()
                .count();
        //@formatter:on

    }

    private long getCountForVariable(String variableName) {
        //@formatter:off
        return DataQuery.builder(sparkSession).byVariables().system(SYSTEM)
                .startTime(FROM)
                .endTime(FROM.plusMillis(MAX_VALUES))
                .variable(variableName)
                .build()
                .count();


        //@formatter:on

    }

}
