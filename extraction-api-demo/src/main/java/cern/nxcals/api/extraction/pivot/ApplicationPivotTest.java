package cern.nxcals.api.extraction.pivot;

import cern.nxcals.api.config.SparkContext;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.extraction.data.ExtractionUtils;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.metadata.queries.Variables;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataType;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;

import java.time.Instant;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Import(SparkContext.class)
@SpringBootApplication
@Slf4j
public class ApplicationPivotTest {
    static {
        //        System.setProperty("logging.config", "classpath:log4j2.yml");
        //
        //        //         Uncomment in order to overwrite the default security settings.
        //        //        System.setProperty("javax.net.ssl.trustStore", "/opt/nxcalsuser/nxcals_cacerts");
        //        //        System.setProperty("javax.net.ssl.trustStorePassword", "nxcals");
        //
        //        //        Kerberos credentials (or comment them out to allow them be automatically discovered via krb ticket cache)
        String user = System.getProperty("user.name");
        System.setProperty("kerberos.principal", "acclog");
        System.setProperty("kerberos.keytab", "/opt/" + user + "/.keytab-acclog");
        //
        //        //         TEST Env
        //        //        System.setProperty("service.url", "https://nxcals-test4.cern.ch:19093,https://nxcals-test5.cern.ch:19093,https://nxcals-test6.cern.ch:19093");
        //        //         TESTBED
        //        //        System.setProperty("service.url",
        //        //                "https://cs-ccr-testbed2.cern.ch:19093,https://cs-ccr-testbed3.cern.ch:19093,https://cs-ccr-nxcalstbs1.cern.ch:19093");
        //        //         PRO
        System.setProperty("service.url",
                "https://cs-ccr-nxcals6.cern.ch:19093,https://cs-ccr-nxcals7.cern.ch:19093,https://cs-ccr-nxcals8.cern.ch:19093");
        //        //Hadoop 3
        //        System.setProperty("service.url",
        //                "https://cs-ccr-nxcalstst2.cern.ch:19093,https://cs-ccr-nxcalstst3.cern.ch:19093,https://cs-ccr-nxcalstst4.cern.ch:19093");
    }

    @SneakyThrows
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(ApplicationPivotTest.class, args);
        //        metadata();
        extraction();
    }

    private static void metadata() {
        SparkSession sparkSession = SparkSession.builder().getOrCreate();
        TimeWindow tw = TimeWindow.fromStrings("2024-11-24 13:10:00.000", "2024-11-24 14:15:00.000");
        VariableService variableService = ServiceClientFactory.createVariableService();
        Set<Variable> variables = new HashSet<>(variableService.findAll(
                Variables.suchThat().configsFieldName().doesNotExist().withOptions().limit(10)));
        Map<DataType, Set<Variable>> groupedVariables = ExtractionUtils.groupVariablesByType(sparkSession, variables,
                tw);
        log.info(groupedVariables.toString());
    }

    private static void extraction() {
        SparkSession sparkSession = SparkSession.builder().getOrCreate();

        TimeWindow tw = TimeWindow.fromStrings("2024-11-24 13:10:00.000", "2024-11-24 14:15:00.000");
        log.info("Start");
        for (int i = 0; i < 10; i++) {
            Instant s = Instant.now();
            log.info("Hop");
            Dataset<Row> ds1 = DataQuery.getAsPivot(sparkSession, tw, "CMW", Collections.emptyList(),
                    List.of("TT10.BCTFI.102834%"));
            log.info("Done!");
            Instant e = Instant.now();
            log.info("Metadata: " + (e.toEpochMilli() - s.toEpochMilli()));
            s = Instant.now();
            List<Row> c1 = ds1.collectAsList();
            e = Instant.now();
            log.info("Extraction: " + (e.toEpochMilli() - s.toEpochMilli()));
        }
    }
}
