/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.api.extraction.tags;

import cern.nxcals.api.config.SparkContext;
import cern.nxcals.api.custom.domain.Tag;
import cern.nxcals.api.custom.extraction.metadata.TagService;
import cern.nxcals.api.custom.extraction.metadata.TagServiceFactory;
import cern.nxcals.api.custom.extraction.metadata.queries.Tags;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.Visibility;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.extraction.metadata.EntityService;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.queries.Entities;
import cern.nxcals.api.utils.TimeUtils;
import cern.rbac.client.authentication.AuthenticationClient;
import cern.rbac.client.authentication.AuthenticationException;
import cern.rbac.common.RbaToken;
import cern.rbac.util.holder.ClientTierTokenHolder;
import com.google.common.collect.ImmutableMap;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.jetbrains.annotations.NotNull;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;

import java.time.Instant;
import java.util.Collections;
import java.util.Set;

import static cern.nxcals.api.custom.extraction.metadata.TagService.DEFAULT_RELATION;
import static cern.nxcals.api.extraction.data.builders.DataQuery.builder;

@Import(SparkContext.class)
@SpringBootApplication
@Slf4j
public class ApplicationDev {

    static {
        //        System.setProperty("logging.config", "classpath:log4j2.yml");
        //
        //        //         Uncomment in order to overwrite the default security settings.
        //        //        System.setProperty("javax.net.ssl.trustStore", "/opt/nxcalsuser/nxcals_cacerts");
        //        //        System.setProperty("javax.net.ssl.trustStorePassword", "nxcals");
        //
        //        //        Kerberos credentials (or comment them out to allow them be automatically discovered via krb ticket cache)
        //        //        String user = System.getProperty("user.name");
        //        System.setProperty("kerberos.principal", "jwozniak");
        System.setProperty("kerberos.principal", "acclog");
        //        System.setProperty("kerberos.keytab", "/opt/jwozniak/.keytab");
        System.setProperty("kerberos.keytab", "/opt/jwozniak/.keytab-acclog");
        //
        //        //         TEST Env
        //                System.setProperty("service.url", "https://nxcals-test4.cern.ch:19093,https://nxcals-test5.cern.ch:19093,https://nxcals-test6.cern.ch:19093");
        //        //         TESTBED
        //        //        System.setProperty("service.url",
        //        //                "https://cs-ccr-testbed2.cern.ch:19093,https://cs-ccr-testbed3.cern.ch:19093,https://cs-ccr-nxcalstbs1.cern.ch:19093");
        //        //         PRO
        //        System.setProperty("service.url",
        //                "https://cs-ccr-nxcals6.cern.ch:19093,https://cs-ccr-nxcals7.cern.ch:19093,https://cs-ccr-nxcals8.cern.ch:19093");

        //        //TESTBED
        //                System.setProperty("service.url",
        //                        "https://cs-ccr-nxcalstbs1.cern.ch:19093,https://cs-ccr-nxcalstbs2.cern.ch:19093");
        //dev-jwozniak
        System.setProperty("service.url", "https://nxcals-jwozniak-1.cern.ch:19093");

        //        System.setProperty("NXCALS_RBAC_AUTH", "true");
        //        System.setProperty("kerberos.auth.disable", "true");

    }



    private static void login() {

        try {
            AuthenticationClient authenticationClient = AuthenticationClient.create();
            //Don't commit your password here!!!
            RbaToken token = authenticationClient.loginExplicit("jwozniak", "");
            ClientTierTokenHolder.setRbaToken(token);
        } catch (AuthenticationException e) {
            throw new IllegalArgumentException("Cannot login", e);
        }

    }


    public static void main(String[] args) {

        SparkSession sparkSession = getSparkSession(args);

        TagService tagService = TagServiceFactory.createTagService();

        //Tag creation
        Tag tag = createTag(tagService, sparkSession);

        //Updating tag metadata
        findAndUpdateTag(tagService,tag.getName(), "New description");

        //Getting some data from a Tag info
        getDataFromTag(sparkSession,tagService, tag);

        //Deleting a tag
        tagService.delete(tag.getId());

        sparkSession.close();

    }

    @NotNull
    private static SparkSession getSparkSession(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(ApplicationDev.class, args);
        SparkSession sparkSession = context.getBean(SparkSession.class);
        log.info("Got spark session {}", sparkSession);
        return sparkSession;
    }


    @SneakyThrows
    private static Tag createTag(TagService tagService, SparkSession sparkSession) {

        //select some timestamp for your TAG
        Instant tagTimestamp = findTimestamp(sparkSession);

        //select some Entities/Variables
        Set<Entity> entities = findEntities("%F16.BCT%Acquisition%");
        log.info("Found device/props: " + entities.size());

        //create a TAG
        Tag tag = tagService.create("TEST_TAG_" + Instant.now().toEpochMilli(),
                "description", Visibility.PUBLIC,
                "acclog", "CMW",
                tagTimestamp,
                entities, Collections.emptyList());

        log.info("############# TAG CREATED {} ", tag.getName());
        Thread.sleep(4000);
        return tag;
    }

    //Finding some random timestamp from a Fundamental.
    private static Instant findTimestamp(SparkSession sparkSession) {
        Dataset<Row> fundamentals = builder(sparkSession).variables().system("CMW").nameEq("CPS:NXCALS_FUNDAMENTAL")
                .timeWindow("2023-06-11 00:00:00", "2023-06-11 02:00:00").build();

        return TimeUtils.getInstantFromNanos(fundamentals.select("nxcals_timestamp").first().getLong(0));
    }

    //Finding some random device/properties from CMW system.
    private static Set<Entity> findEntities(String namelike) {
        EntityService entityService = ServiceClientFactory.createEntityService();
        return entityService.findAll(
                Entities.suchThat().keyValues().caseInsensitive().like(namelike).and().systemName().eq("CMW"));
    }

    @SneakyThrows
    private static void findAndUpdateTag(TagService service, String tagName, String newDescription) {
        //Find a TAG
        Tag tag = service.findOne(Tags.suchThat().name().eq(tagName))
                .orElseThrow(() -> new RuntimeException("No such tag: " + tagName));

        //Update TAG information
        Tag newTag = tag.toBuilder().description(newDescription).properties(ImmutableMap.of("new Key", "value")).build();


        newTag = service.update(newTag);



        tag = service.findOne(Tags.suchThat().name().eq(tagName))
                .orElseThrow(() -> new RuntimeException("No such tag: " + tagName));

        log.info("############# TAG FOUND AND UPDATED {} ", tag);
        Thread.sleep(4000);

    }

    public static void getDataFromTag(SparkSession sparkSession, TagService service, Tag tag) {
        //Getting Entities from a TAG
        Set<Entity> entities = service.getEntities(tag.getId()).get(DEFAULT_RELATION);

        //Querying for DATA of those Entities
        for(Entity ent : entities) {
            log.info("Entity data " + ent.getEntityKeyValues() + " at time: " + tag.getTimestamp());

            Dataset<Row> dataset = DataQuery.builder(sparkSession)
                    .entities().idEq(ent.getId()).atTime(tag.getTimestamp()).build();

            if(!dataset.isEmpty()) {
                log.info("$$$$$$$$$$$$$ Data for {}", ent.getEntityKeyValues());
                dataset.select("device", "property", "cyclestamp", "selector").show();
            } else {
                log.info("???????????? NO Data for {}", ent.getEntityKeyValues());
            }
        }

    }
}
