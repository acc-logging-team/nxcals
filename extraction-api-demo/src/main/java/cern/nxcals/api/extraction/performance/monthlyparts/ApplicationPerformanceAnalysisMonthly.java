/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.api.extraction.performance.monthlyparts;

import cern.nxcals.api.config.SparkContext;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.common.SystemFields;
import com.google.common.collect.Lists;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.avro.Schema;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.DoubleType;
import org.apache.spark.sql.types.FloatType;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.unsafe.hash.Murmur3_x86_32;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.time.Instant;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

@Import(SparkContext.class)
@SpringBootApplication
@Slf4j
public class ApplicationPerformanceAnalysisMonthly {


    private static final String PARTITION_COLUMN = "partition";
    public static final Random rand = new Random();
    private static final FileSystem fileSystem = createFileSystem();
    private static final long SECONDS_IN_DAY = 24 * 60 * 60;
    private static final int INTERVAL_IN_MINUTES = 1 * 60; //our split interval
    private static final long INTERVAL_IN_SECONDS = INTERVAL_IN_MINUTES * 60;
    private static final boolean SHOULD_FORCE_REPARTITION = true;
    private static final long NUMBER_OF_PARTITIONS = SECONDS_IN_DAY / INTERVAL_IN_SECONDS;
    private static final long PARQUET_BLOCK_SIZE_MB = 128;
    private static final long PARQUET_BLOCK_SIZE = PARQUET_BLOCK_SIZE_MB * 1024 * 1024L;
    private static final boolean SHOULD_USE_PARTITION_IN_QUERY = true;
    private static final boolean SHOULD_USE_ENTITY_BUCKETS = true;
    private static final boolean SHOULD_USE_ADAPTIVE_ROW_GROUP = true;
    private static final int NUMBER_OF_ENTITY_BUCKETS = 4;

    private static final String ENTITY_BUCKET_COLUMN = "entityBucket";
    private static final long ROW_GROUP_MIN_SIZE = 16 * 1024 * 1024L;
    private static final long ROW_GROUP_MAX_SIZE = 512 * 1024 * 1024L;
    private static final long DEFAULT_HDFS_BLOCK_SIZE = 256 * 1024 * 1024L;
    private static final long EXTENDED_HDFS_BLOCK_SIZE = 512 * 1024 * 1024L;

    public static final String REPARTITION_PATH = "/project/nxcals/perf_tests_merging_months/data";

    static {
//        System.setProperty("logging.config", "classpath:log4j2.yml");
//
//        //         Uncomment in order to overwrite the default security settings.
//        //        System.setProperty("javax.net.ssl.trustStore", "/opt/nxcalsuser/nxcals_cacerts");
//        //        System.setProperty("javax.net.ssl.trustStorePassword", "nxcals");
//
//        //        Kerberos credentials (or comment them out to allow them be automatically discovered via krb ticket cache)
//        //        String user = System.getProperty("user.name");
//        System.setProperty("kerberos.principal", "jwozniak");
//        System.setProperty("kerberos.keytab", "/opt/jwozniak/.keytab");
//
//        //         TEST Env
//        //        System.setProperty("service.url", "https://nxcals-test4.cern.ch:19093,https://nxcals-test5.cern.ch:19093,https://nxcals-test6.cern.ch:19093");
//        //         TESTBED
//        //        System.setProperty("service.url",
//        //                "https://cs-ccr-testbed2.cern.ch:19093,https://cs-ccr-testbed3.cern.ch:19093,https://cs-ccr-nxcalstbs1.cern.ch:19093");
//        //         PRO
        System.setProperty("service.url",
                "https://cs-ccr-nxcals6.cern.ch:19093,https://cs-ccr-nxcals7.cern.ch:19093,https://cs-ccr-nxcals8.cern.ch:19093");
//        //Hadoop 3
//        System.setProperty("service.url",
//                "https://cs-ccr-nxcalstst2.cern.ch:19093,https://cs-ccr-nxcalstst3.cern.ch:19093,https://cs-ccr-nxcalstst4.cern.ch:19093");
    }

    private static SystemSpecService systemSpecService = ServiceClientFactory.createSystemSpecService();

    public static void main(String[] args) {

        ConfigurableApplicationContext context = SpringApplication.run(ApplicationPerformanceAnalysisMonthly.class, args);
        SparkSession sparkSession = context.getBean(SparkSession.class);

        sparkSession.sparkContext().hadoopConfiguration()
                .setInt("parquet.block.size", 1000); //1KB instead of the default 128MB
        sparkSession.sparkContext().hadoopConfiguration()
                .setInt("parquet.dictionary.page.size", 3_145_728); //3MB for bigger dictionaries

        warmUpSpark(sparkSession);

        List<String> inputPaths = getInputPaths();

        Map<String, List<Query>> queriesPerPath = new HashMap();
        //pre-partition in parallel, that goes fast
        inputPaths.parallelStream().forEach(path -> {
            log.info("Start processing {}", path);
            repartitionToPath(sparkSession, path, false);
            List<Query> queries = getQueries(sparkSession, path + "/*/*.parquet");
            queriesPerPath.put(path, queries);
        });

        for (String path : inputPaths) {
            String repartitionedPath = repartitionToPath(sparkSession, path, false);
            for (int i = 0; i < 1; ++i) {
                log.info("************ Run {}", i);
                List<Query> queries = queriesPerPath.get(path);
                log.info("For path: {} Queries: {}", path, queries);
                List<QueryResult> original = queryInPath(sparkSession, path + "/*/*.parquet", queries, false, false);
                List<QueryResult> repartitioned = queryInPath(sparkSession, repartitionedPath + "/*.parquet", queries,
                        false, false);
                compareResults(path, original, repartitionedPath, repartitioned);
            }
        }
    }

    private static FileSystem createFileSystem() {
        org.apache.hadoop.conf.Configuration conf = new org.apache.hadoop.conf.Configuration();
        try {
            return FileSystem.get(conf);
        } catch (IOException e) {
            throw new UncheckedIOException("Cannot access filesystem", e);
        }
    }

    private static void warmUpSpark(SparkSession sparkSession) {
        log.info("Warming up the spark session and java");
        for (int i = 0; i < 10; i++) {
            sparkSession.read().parquet("/project/nxcals/nxcals_pro/data/2/14074/15804/2021/8/15").distinct().collectAsList();
            sparkSession.read().parquet("/project/nxcals/nxcals_pro/data/2/14074/15804/2021/8/15").select(SystemFields.NXC_ENTITY_ID.getValue()).distinct().collectAsList();
        }
        log.info("Warm up finished");
    }

    private static void compareResults(String path, List<QueryResult> original, String repartitionedPath, List<QueryResult> repartitioned) {
        log.info("Comparing results for {} repartitioned to {}", path, repartitionedPath);
        log.info("Query times for original: {}", original);
        log.info("Query times for repartioned: {}", repartitioned);
        long better = 0;
        long worse = 0;
        long betterSum = 0;
        long worseSum = 0;
        for (int i = 0; i < original.size(); i++) {
            QueryResult org = original.get(i);
            QueryResult rep = repartitioned.get(i);
            check(org, rep);
            long orgAvgTime = average(org);
            long repAvgTime = average(rep);
            long diff = Math.abs(orgAvgTime - repAvgTime);
            long percent = (long) (((double) diff / (double) orgAvgTime) * 100);
            if (orgAvgTime > repAvgTime) {
                log.info("For query {}  BETTER for rep by {}% diff={} org={} rep={}", org.query, percent, diff, orgAvgTime, repAvgTime);
                better++;
                betterSum += percent;
            } else {
                log.warn("**** For query {} WORSE for rep by {}% diff={} org={} rep={}", org.query, percent, diff, orgAvgTime, repAvgTime);
                worse++;
                worseSum += percent;
            }

        }

        long betterAvg = 0;
        long worseAvg = 0;
        if (better > 0) {
            betterAvg = betterSum / better;
        }
        if (worse > 0) {
            worseAvg = worseSum / worse;
        }

        Directory orgDir = analysePath(path);
        Directory repDir = analysePath(repartitionedPath);

        long countPercent = (long) ((Math.abs(repDir.fileCount - orgDir.fileCount) / (double) orgDir.fileCount) * 100);
        long sizePercent = (long) ((Math.abs(repDir.totalSize - orgDir.totalSize) / (double) orgDir.totalSize) * 100);

        log.info("Finished analysis for {}, split={} min, rowg={} MB, query part={}, better {} with avg {}% worse {} with avg {}% (org dir count: {} {} MB, rep dir count: {} {} MB, count diff: {}% size diff: {}%)",
                repartitionedPath, INTERVAL_IN_MINUTES, (SHOULD_USE_ADAPTIVE_ROW_GROUP ? "adaptive" : PARQUET_BLOCK_SIZE / (1024 * 1024)),
                SHOULD_USE_PARTITION_IN_QUERY,
                better, betterAvg, worse, worseAvg,
                orgDir.fileCount,
                String.format("%,.3f", orgDir.totalSize / (1024 * 1024.0)),
                repDir.fileCount,
                String.format("%,.3f", repDir.totalSize / (1024 * 1024.0)),
                countPercent, sizePercent);

    }

    @SneakyThrows
    private static Directory analysePath(String path) {
        RemoteIterator<LocatedFileStatus> iterator = fileSystem.listFiles(new Path(path), true);
        long count = 0;
        long size = 0;
        while (iterator.hasNext()) {
            LocatedFileStatus file = iterator.next();
            if (file.isFile() && file.getPath().getName().endsWith(".parquet")) {
                size += file.getLen();
                count++;
            }
        }
        return new Directory(count, size);
    }

    @Data
    public static class Directory {
        private final long fileCount;
        private final long totalSize;
    }

    private static void check(QueryResult org, QueryResult rep) {
        if (org.count != rep.count || !org.query.equals(rep.query)) {
            throw new RuntimeException("This is not the same query q1=" + org + " q2=" + rep);
        }
    }

    private static long average(QueryResult org) {
        return (long) org.queryTimes.stream().mapToLong(r -> r).average().orElseThrow(() -> new RuntimeException("Cannot calculate average"));
    }

    private static List<QueryResult> queryInPath(SparkSession sparkSession, String path, List<Query> queries, boolean usePartition, boolean useEntityBuckets) {
        log.info("Start querying in path {}", path);
        String timestampColumn = getTimestampColumn(path);
        Dataset<Row> dataset = sparkSession.read().parquet(path);
        //Cannot parallelize as this introduces a very big random factor to query times as they affect each other.
        return queries.stream().map(query -> query(dataset, query, timestampColumn, usePartition, useEntityBuckets)).collect(Collectors.toList());
    }

    private static QueryResult query(Dataset<Row> dataset, Query query, String timestampColumn, boolean usePartition, boolean useEntityBuckets) {
        long count = -1;
        Column[] columns = getColumns(dataset, query);
        List<Long> queryTimes = new ArrayList<>();
        for (int i = 0; i < 3; ++i) { //query multiple times for the same dataset to take average.
            long start = System.currentTimeMillis();

            long startPartition = getPartition(query.timeWindow.getStartTimeNanos());
            long endPartition = getPartition(query.timeWindow.getEndTimeNanos());
            long entityBucket = getBucket(query.entity, NUMBER_OF_ENTITY_BUCKETS);
            String where = SystemFields.NXC_ENTITY_ID.getValue() +
                    " = " + query.entity +
                    " and " + timestampColumn + " >= " + query.timeWindow.getStartTimeNanos() +
                    " and " + timestampColumn + " <= " + query.timeWindow.getEndTimeNanos();
            if (usePartition) {
                where += " and partition >= " + startPartition + " and partition <= " + endPartition;
            }
            if (useEntityBuckets) {

                where += " and " + ENTITY_BUCKET_COLUMN + " = " + entityBucket;
            }

            Dataset<Row> queryDs = dataset.select(columns).where(where);
            List<Row> rows = queryDs.collectAsList();
            long queryTime = System.currentTimeMillis() - start;
//            queryDs.explain(true);
            log.info("Run a query for {}, took {} ms, count={}, minPart={}, maxPart={}, entityBucket={} usePart={} useEntityBuckets={}", query, queryTime, rows.size(), startPartition, endPartition, entityBucket, usePartition, useEntityBuckets);
            queryTimes.add(queryTime);
            count = rows.size();
        }

        return new QueryResult(query, queryTimes, count);
    }

    private static long getPartition(long value) {
//        Column partitionColumn = dataset.col(TIMESTAMP_COLUMN)
//                .divide(functions.lit(TimeUtils.CONVERT_EPOCH_NANOS_TO_SECONDS_VALUE)) //to seconds only
//                .mod(functions.lit(SECONDS_IN_DAY)) //will have seconds since the beginning of the day
//                .divide(functions.lit(INTERVAL_IN_SECONDS)) //dividing by interval lenght will get the partition number
//                .cast(DataTypes.LongType); //interested in integrals

        return ((value / TimeUtils.CONVERT_EPOCH_NANOS_TO_SECONDS_VALUE) % SECONDS_IN_DAY) / INTERVAL_IN_SECONDS;
    }

    private static Column[] getColumns(Dataset<Row> dataset, Query query) {
        return query.getFields().stream().map(dataset::col).toArray(Column[]::new);
    }

    private static List<Query> getQueries(SparkSession sparkSession, String path) {
        List<Long> entities = getEntities(sparkSession, path, 3);
        List<TimeWindow> timeWindows = getTimewindows(sparkSession, path);
        Set<String> fields = getFields(sparkSession, path);
        log.info("For path {} using entities {} and timewindows {}", path, entities, timeWindows);
        List<Query> queries = new ArrayList<>();

        for (Long entity : entities) {
            for (TimeWindow tw : timeWindows) {
                queries.add(new Query(entity, tw, fields));
            }
        }
        return queries;
    }

    private static Set<String> getFields(SparkSession sparkSession, String path) {
        String timestampColumn = getTimestampColumn(path);
        Dataset<Row> dataset = sparkSession.read().parquet(path);
        StructField[] fields = dataset.schema().fields();
        Set<String> queryFields = new HashSet<>();
        queryFields.add(SystemFields.NXC_ENTITY_ID.getValue());
        queryFields.add(timestampColumn);
//        queryFields.add(getArrayField(fields)); <-- generating too many OOM problems, skipping arrays for this analysis
        queryFields.add(getScalarField(fields));
        return queryFields;
    }

    private static String getScalarField(StructField[] fields) {
        for (StructField field : fields) {
            if (field.dataType() instanceof DoubleType || field.dataType() instanceof FloatType) {
                return field.name();
            }
        }
        return SystemFields.NXC_SYSTEM_ID.getValue();
    }

    private static String getArrayField(StructField[] fields) {
        for (StructField field : fields) {
            if (field.dataType() instanceof StructType) {
                return field.name();
            }
        }
        return SystemFields.NXC_PARTITION_ID.getValue();
    }

    @SneakyThrows
    private static String repartitionToPath(SparkSession sparkSession, String path, boolean force) {
        String timestampColumn = getTimestampColumn(path);
        String destPathStr = path.replace("/project/nxcals/nxcals_pro/data", REPARTITION_PATH);
        Path destPath = new Path(destPathStr);

        if (!force && fileSystem.exists(destPath) && fileSystem.listStatus(destPath).length > 1) {
            log.info("Path exists {}, skipping repartition", destPath);
            return destPathStr;
        }

        log.info("Started repartition from  {} into {} timestamp column {}", path, destPath, timestampColumn);
        Dataset<Row> dataset = sparkSession.read().parquet(path + "/*"); //all days from a given month

        log.info("Saving to dest path {}", destPathStr);
        //        Directory orgDir = analysePath(path);
        writeParquet(sparkSession, dataset, destPathStr);
        log.info("Finished repartition from  {} into {}", path, destPathStr);
        return destPathStr;

    }

    private static long getBucket(long val, long nbOfBuckets) {
        //42 is used by Spark as seed...
        return pmod(Murmur3_x86_32.hashLong(val, 42), nbOfBuckets);
    }

    private static long pmod(long a, long n) {
        long r = a % n;
        if (r < 0) {
            return (r + n) % n;
        } else {
            return r;
        }
    }

    private static String getTimestampColumn(String path) {
        ///project/nxcals/nxcals_pro/data/17/29222/201119/2021/8
        long systemId = Long.parseLong(path.split("/")[5]);
        SystemSpec systemSpec = systemSpecService.findById(systemId).orElseThrow(() -> new RuntimeException("No such system id " + systemId));
        return new Schema.Parser().parse(systemSpec.getTimeKeyDefinitions()).getFields().stream().map(Schema.Field::name)
                .collect(Collectors.toList()).get(0);


    }

    private static void writeParquet(SparkSession sparkSession, Dataset<Row> withPart, String destPathStr) {
        //paranoid check to avoid overwriting pro data...
        if (destPathStr.contains("nxcals_pro")) {
            throw new RuntimeException("Should not have nxcals_pro in destination path for this test!!!");
        }
        long parquetRowGroupSize = 1000;
        //        long hdfsBlockSize = DEFAULT_HDFS_BLOCK_SIZE;
        //        if (adaptiveRowGroup) {
        //            //aiming at ~200 row groups as default , with min 16MB, max 256MB (HDFS block size),
        //            //using multiples of 16MB to normalize.
        //
        //            parquetRowGroupSize = (((orgDirStats.totalSize / 200) / ROW_GROUP_MIN_SIZE) + 1) * ROW_GROUP_MIN_SIZE;
        //
        //            if (parquetRowGroupSize < ROW_GROUP_MIN_SIZE) {
        //                parquetRowGroupSize = ROW_GROUP_MIN_SIZE; //lower bound this is probably not needed with the normalization...
        //            } else if (parquetRowGroupSize > ROW_GROUP_MAX_SIZE) {
        //                parquetRowGroupSize = ROW_GROUP_MAX_SIZE; //upper bound is needed.
        //            }
        //            if (parquetRowGroupSize > hdfsBlockSize) {
        //                hdfsBlockSize = EXTENDED_HDFS_BLOCK_SIZE;
        //            }
        //
        //        }
        //
        //        hdfsBlockSize = EXTENDED_HDFS_BLOCK_SIZE;
        //        log.info("For path {} using parquet row group size {} MB, predicted row group count {}, hfds block size {} MB", destPathStr, parquetRowGroupSize / (1024 * 1024), orgDirStats.totalSize / parquetRowGroupSize, hdfsBlockSize / (1024 * 1024));

        //        sparkSession.sparkContext().hadoopConfiguration().setLong("dfs.block.size", hdfsBlockSize);

        withPart
                .repartition(1)
                .write()
                .option("parquet.block.size", parquetRowGroupSize)
                .option("parquet.dictionary.page.size", 3_145_728)
                .mode(SaveMode.Overwrite)
                .parquet(destPathStr);

    }

    @SneakyThrows
    private static void processOutputParitions(String destPathStr) {
        log.info("Processing output path {}", destPathStr);
        Path output = new Path(destPathStr + "/partition=*/*.parquet");
        FileStatus[] fileStatuses = fileSystem.globStatus(output);
        for (FileStatus file : fileStatuses) {

            String parent = file.getPath().getParent().getName();
            String renamed = parent + "__" + file.getPath().getName();
            fileSystem.rename(file.getPath(), new Path(destPathStr + renamed));


        }
    }

    private static List<TimeWindow> getTimewindows(SparkSession sparkSession, String path) {
        String timestampColumn = getTimestampColumn(path);
        Dataset<Row> dataset = sparkSession.read().parquet(path);
        List<Long> samples = dataset.select(timestampColumn).distinct()
                .sample(0.1).limit(10).collectAsList().stream().map(r -> (Long) r.getAs(timestampColumn)).collect(Collectors.toList());

        List<Row> minMax = dataset.agg(functions.min(dataset.col(timestampColumn)).alias("min"), functions.max(dataset.col(timestampColumn)).alias("max")).collectAsList();
        log.info("Min/max timestamps {}", minMax);
        long minTs = minMax.get(0).getLong(0);
        long maxTs = minMax.get(0).getLong(1);

        List<Long> timestamps = new ArrayList<>();
        timestamps.add(minTs + rand.nextInt(1_000_000));
        timestamps.addAll(samples);
        timestamps.add(maxTs - rand.nextInt(1_000_000));


        List<TimeWindow> windows = new ArrayList<>();
        for (int i = 0; i < timestamps.size() - 1; i = i + 2) {
            Long t1 = timestamps.get(i);
            Long t2 = timestamps.get(i + 1);
            if (t1 <= t2) {
                windows.add(TimeWindow.between(t1, t2)); //random window
                windows.add(TimeWindow.between(t1, limitToCurrentDay(t1, generateNanos()))); //short time window (<INTERVAL min)

            } else {
                windows.add(TimeWindow.between(t2, t1));
                windows.add(TimeWindow.between(t2, limitToCurrentDay(t2, generateNanos())));
            }

        }

        return windows;
    }

    private static long limitToCurrentDay(long timestamp, long nanos) {
        long result = timestamp + nanos;
        Instant val = TimeUtils.getInstantFromNanos(result);
        if (val.atZone(ZoneId.of("UTC")).getDayOfMonth() != TimeUtils.getInstantFromNanos(timestamp).atZone(ZoneId.of("UTC")).getDayOfMonth()) {
            return timestamp;
        } else {
            return result;
        }
    }

    private static Long generateNanos() {
        int minutes = rand.nextInt(INTERVAL_IN_MINUTES);
        return minutes * 60_000_000_000L;
    }

    private static List<Long> getEntities(SparkSession sparkSession, String path, int limit) {
        Dataset<Row> dataset = sparkSession.read().parquet(path);

        Dataset<Row> distinct = dataset.select(SystemFields.NXC_ENTITY_ID.getValue()).distinct();

        long total = distinct.count();
        log.info("Total number {} of entities in path {}", total, path);
        List<Long> entityIds =
                distinct.sample(0.1).limit(limit).collectAsList()
                        .stream().map(r -> (Long) r.getAs(SystemFields.NXC_ENTITY_ID.getValue())).collect(Collectors.toList());
        if (entityIds.isEmpty()) {
            //try without sampling
            return distinct.limit(limit)
                    .collectAsList().stream().map(r -> (Long) r.getAs(SystemFields.NXC_ENTITY_ID.getValue())).collect(Collectors.toList());
        } else {
            return entityIds;
        }

//        return Lists.newArrayList(9299294L);
    }


    private static List<String> getInputPathsForTest() {
        return Lists.newArrayList(
                "/project/nxcals/nxcals_pro/data/2/148226/14411/2021/8",
                "/project/nxcals/nxcals_pro/data/2/148223/358492/2021/8",
                "/project/nxcals/nxcals_pro/data/2/17512/182044/2021/8",
                "/project/nxcals/nxcals_pro/data/2/98221/366492/2021/8",
                "/project/nxcals/nxcals_pro/data/2/148224/358491/2021/8"

        );

    }

    private static List<String> getInputPaths() {
        return Lists.newArrayList(

                "/project/nxcals/nxcals_pro/data/2/148226/14411/2021/8",
                "/project/nxcals/nxcals_pro/data/2/148223/358492/2021/8",
                "/project/nxcals/nxcals_pro/data/2/17512/182044/2021/8",
                "/project/nxcals/nxcals_pro/data/2/98221/366492/2021/8",
                "/project/nxcals/nxcals_pro/data/2/148224/358491/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15020/15933/2021/8",
                "/project/nxcals/nxcals_pro/data/2/54221/289492/2021/8",
                "/project/nxcals/nxcals_pro/data/2/72222/186115/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15081/177671/2021/8",
                "/project/nxcals/nxcals_pro/data/2/17363/177892/2021/8",
                "/project/nxcals/nxcals_pro/data/2/88221/176908/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14813/252511/2021/8",
                "/project/nxcals/nxcals_pro/data/2/21233/177475/2021/8",
                "/project/nxcals/nxcals_pro/data/2/21253/177475/2021/8",
                "/project/nxcals/nxcals_pro/data/2/20267/187114/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15046/182371/2021/8",
                "/project/nxcals/nxcals_pro/data/2/20240/187114/2021/8",
                "/project/nxcals/nxcals_pro/data/2/21231/177475/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12212/14390/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14825/324491/2021/8",
                "/project/nxcals/nxcals_pro/data/2/21226/187114/2021/8",
                "/project/nxcals/nxcals_pro/data/2/17362/182496/2021/8",
                "/project/nxcals/nxcals_pro/data/2/154225/355498/2021/8",
                "/project/nxcals/nxcals_pro/data/2/55223/298491/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16725/176635/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16734/176635/2021/8",
                "/project/nxcals/nxcals_pro/data/2/55226/277492/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16979/15395/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14379/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14395/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14939/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15146/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14398/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/17352/182490/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14425/175828/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14509/175828/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14422/175828/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14427/175828/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14433/175828/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14437/175828/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14444/175828/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14461/175828/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14468/175828/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14493/175828/2021/8",
                "/project/nxcals/nxcals_pro/data/2/62222/267496/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12273/14401/2021/8",
                "/project/nxcals/nxcals_pro/data/2/113221/14428/2021/8",
                "/project/nxcals/nxcals_pro/data/2/114221/14428/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12028/357497/2021/8",
                "/project/nxcals/nxcals_pro/data/2/109221/14428/2021/8",
                "/project/nxcals/nxcals_pro/data/2/110221/14428/2021/8",
                "/project/nxcals/nxcals_pro/data/2/110222/14428/2021/8",
                "/project/nxcals/nxcals_pro/data/2/111221/14428/2021/8",
                "/project/nxcals/nxcals_pro/data/2/32739/220493/2021/8",
                "/project/nxcals/nxcals_pro/data/2/33233/267491/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12107/14170/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12118/14170/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12127/14170/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12130/14170/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14711/175932/2021/8",
                "/project/nxcals/nxcals_pro/data/2/57229/264491/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12028/356499/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15078/215001/2021/8",
                "/project/nxcals/nxcals_pro/data/2/21256/177475/2021/8",
                "/project/nxcals/nxcals_pro/data/2/18061/182581/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14303/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14323/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14324/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14382/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/42232/244491/2021/8",
                "/project/nxcals/nxcals_pro/data/2/21251/186118/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14901/182531/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14479/175828/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11876/14207/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11877/14207/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15090/281491/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12028/356500/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12028/357498/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12205/14443/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14536/175849/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14548/175849/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14593/175849/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14569/175849/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14597/175849/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14557/175849/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14571/175849/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14592/175849/2021/8",
                "/project/nxcals/nxcals_pro/data/2/44227/240497/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12104/14389/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12196/14389/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12184/14170/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12201/14170/2021/8",
                "/project/nxcals/nxcals_pro/data/2/19225/14170/2021/8",
                "/project/nxcals/nxcals_pro/data/2/19723/14170/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12162/14434/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14327/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14912/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11865/14073/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12137/14073/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12153/14073/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12187/14073/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12411/14922/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12170/14152/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12180/14152/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12183/14152/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12191/14152/2021/8",
                "/project/nxcals/nxcals_pro/data/2/19224/14152/2021/8",
                "/project/nxcals/nxcals_pro/data/2/19722/14152/2021/8",
                "/project/nxcals/nxcals_pro/data/2/17517/182594/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15158/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11647/14020/2021/8",
                "/project/nxcals/nxcals_pro/data/2/159221/360493/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15372/176908/2021/8",
                "/project/nxcals/nxcals_pro/data/2/159222/360495/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15039/333498/2021/8",
                "/project/nxcals/nxcals_pro/data/2/21246/189113/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14426/175828/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14469/175828/2021/8",
                "/project/nxcals/nxcals_pro/data/2/148234/350491/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14517/175828/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14522/175828/2021/8",
                "/project/nxcals/nxcals_pro/data/2/28722/14153/2021/8",
                "/project/nxcals/nxcals_pro/data/2/125227/326497/2021/8",
                "/project/nxcals/nxcals_pro/data/2/21259/182496/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14297/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14298/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14308/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14310/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14312/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14321/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14326/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14328/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14334/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14357/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14358/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14375/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14376/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14377/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14387/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14391/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14392/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14394/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/20228/182496/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12302/14152/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12722/14152/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12078/13996/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12090/13996/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12070/13996/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12073/13996/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12084/13996/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12093/13996/2021/8",
                "/project/nxcals/nxcals_pro/data/2/17651/182131/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12111/14390/2021/8",
                "/project/nxcals/nxcals_pro/data/2/20735/216493/2021/8",
                "/project/nxcals/nxcals_pro/data/2/60236/303495/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14258/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14267/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14268/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14269/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14271/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14272/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14278/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14280/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14281/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14293/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14337/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14344/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14345/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14347/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14354/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14355/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14365/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14367/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14368/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14373/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/29229/293492/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14340/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/105221/311495/2021/8",
                "/project/nxcals/nxcals_pro/data/2/157221/14499/2021/8",
                "/project/nxcals/nxcals_pro/data/2/80225/283492/2021/8",
                "/project/nxcals/nxcals_pro/data/2/154226/360491/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14307/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/161226/15933/2021/8",
                "/project/nxcals/nxcals_pro/data/2/17522/182052/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14305/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14313/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14318/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14322/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14332/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14333/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14335/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14360/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14378/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14380/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14386/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14396/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14397/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14411/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14413/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12139/14427/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14576/175849/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11948/13996/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11953/13996/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14314/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14359/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12168/14170/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12171/14170/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14588/175849/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12167/14170/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12140/14170/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12160/14170/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12178/14170/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12651/14170/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12682/14170/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12272/14170/2021/8",
                "/project/nxcals/nxcals_pro/data/2/149223/346499/2021/8",
                "/project/nxcals/nxcals_pro/data/2/62224/278498/2021/8",
                "/project/nxcals/nxcals_pro/data/2/61232/304495/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16593/177364/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16592/177364/2021/8",
                "/project/nxcals/nxcals_pro/data/2/44235/182592/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12158/14170/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12159/14170/2021/8",
                "/project/nxcals/nxcals_pro/data/2/23722/216992/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13444/176393/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12172/14170/2021/8",
                "/project/nxcals/nxcals_pro/data/2/149226/346499/2021/8",
                "/project/nxcals/nxcals_pro/data/2/208222/414496/2021/8",
                "/project/nxcals/nxcals_pro/data/2/60234/303496/2021/8",
                "/project/nxcals/nxcals_pro/data/2/148235/347502/2021/8",
                "/project/nxcals/nxcals_pro/data/2/150242/346504/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15702/182481/2021/8",
                "/project/nxcals/nxcals_pro/data/2/143221/361503/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12992/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13000/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13024/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13044/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/20777/182071/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12936/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12960/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12969/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12981/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12996/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12998/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13005/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13011/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13021/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13030/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13034/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13058/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13064/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13073/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12972/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12974/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12975/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12986/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12997/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13007/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13019/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13042/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12964/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/17502/182051/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13004/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13006/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13002/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12999/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13020/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13032/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13036/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13050/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13051/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13052/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13055/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13056/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13059/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13067/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13069/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13071/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13074/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13076/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13077/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13079/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13083/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13084/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13085/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13087/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13089/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13093/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13094/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13097/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13102/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13103/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13105/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13106/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13107/177803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/61229/302496/2021/8",
                "/project/nxcals/nxcals_pro/data/2/17511/182038/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15806/361501/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12511/15762/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12855/212491/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12968/177799/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12977/177799/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12989/177799/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12982/177799/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12983/177799/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12987/177799/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12955/177799/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13012/177799/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13014/177799/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13017/175216/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13016/177799/2021/8",
                "/project/nxcals/nxcals_pro/data/2/20758/177475/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12190/14443/2021/8",
                "/project/nxcals/nxcals_pro/data/2/20752/186115/2021/8",
                "/project/nxcals/nxcals_pro/data/2/17430/182594/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13013/177799/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13018/177799/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13031/177799/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13043/177799/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13046/177799/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13048/177799/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13057/177799/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13063/177799/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13078/177799/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13104/177799/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14248/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14273/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14274/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14279/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14282/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14284/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14295/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14336/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14338/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14339/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14341/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14346/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14352/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14356/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14371/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14374/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14929/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/33735/213008/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14922/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14350/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/80227/282495/2021/8",
                "/project/nxcals/nxcals_pro/data/2/148233/346502/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12209/14451/2021/8",
                "/project/nxcals/nxcals_pro/data/2/20749/182496/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12931/176735/2021/8",
                "/project/nxcals/nxcals_pro/data/2/74222/341507/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12258/14497/2021/8",
                "/project/nxcals/nxcals_pro/data/2/143221/359491/2021/8",
                "/project/nxcals/nxcals_pro/data/2/20738/182496/2021/8",
                "/project/nxcals/nxcals_pro/data/2/21224/182496/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12193/14152/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12202/14152/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12292/14580/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12115/14152/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12116/14152/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12109/14152/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12119/14152/2021/8",
                "/project/nxcals/nxcals_pro/data/2/142223/343499/2021/8",
                "/project/nxcals/nxcals_pro/data/2/60240/304496/2021/8",
                "/project/nxcals/nxcals_pro/data/2/143221/356494/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15025/182504/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11828/14160/2021/8",
                "/project/nxcals/nxcals_pro/data/2/60221/262492/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15058/177694/2021/8",
                "/project/nxcals/nxcals_pro/data/2/150221/345492/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13181/175291/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14849/346496/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14852/346496/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13664/15933/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13449/15933/2021/8",
                "/project/nxcals/nxcals_pro/data/2/17523/182051/2021/8",
                "/project/nxcals/nxcals_pro/data/2/17514/182039/2021/8",
                "/project/nxcals/nxcals_pro/data/2/123227/176365/2021/8",
                "/project/nxcals/nxcals_pro/data/2/21252/186123/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13645/15933/2021/8",
                "/project/nxcals/nxcals_pro/data/2/150241/345492/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12240/14522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11869/13996/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11891/13996/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12250/14543/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12341/192615/2021/8",
                "/project/nxcals/nxcals_pro/data/2/26723/14073/2021/8",
                "/project/nxcals/nxcals_pro/data/2/57234/182594/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12267/14516/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13794/175654/2021/8",
                "/project/nxcals/nxcals_pro/data/2/150243/350491/2021/8",
                "/project/nxcals/nxcals_pro/data/2/137221/15933/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12268/14517/2021/8",
                "/project/nxcals/nxcals_pro/data/2/155226/14411/2021/8",
                "/project/nxcals/nxcals_pro/data/2/156221/356492/2021/8",
                "/project/nxcals/nxcals_pro/data/2/20275/186637/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12905/175192/2021/8",
                "/project/nxcals/nxcals_pro/data/2/21250/177892/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12110/14170/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12120/14170/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12131/14170/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12132/14170/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13068/175213/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12994/175213/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12113/14411/2021/8",
                "/project/nxcals/nxcals_pro/data/2/42224/232491/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12944/175213/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12949/175213/2021/8",
                "/project/nxcals/nxcals_pro/data/2/90221/295492/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13037/175231/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12959/175211/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13025/175211/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12976/175211/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13091/175211/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12262/182552/2021/8",
                "/project/nxcals/nxcals_pro/data/2/155224/355492/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11657/324501/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13124/175231/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12938/175211/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12939/175211/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12984/175211/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13028/175211/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12233/13989/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12259/14498/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12372/14828/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15026/189112/2021/8",
                "/project/nxcals/nxcals_pro/data/2/26222/182592/2021/8",
                "/project/nxcals/nxcals_pro/data/2/91223/295495/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12062/14365/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12149/182532/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13448/15933/2021/8",
                "/project/nxcals/nxcals_pro/data/2/33236/217991/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15340/176789/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16745/222992/2021/8",
                "/project/nxcals/nxcals_pro/data/2/153221/14073/2021/8",
                "/project/nxcals/nxcals_pro/data/2/21267/182592/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16763/222992/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13691/175541/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13061/175215/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12124/14398/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12946/175215/2021/8",
                "/project/nxcals/nxcals_pro/data/2/153222/14073/2021/8",
                "/project/nxcals/nxcals_pro/data/2/20242/186123/2021/8",
                "/project/nxcals/nxcals_pro/data/2/104222/311493/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12226/14460/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16856/222992/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16858/222992/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13294/177576/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13301/177576/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13302/177576/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13304/177576/2021/8",
                "/project/nxcals/nxcals_pro/data/2/20266/187114/2021/8",
                "/project/nxcals/nxcals_pro/data/2/103224/311494/2021/8",
                "/project/nxcals/nxcals_pro/data/2/20263/187114/2021/8",
                "/project/nxcals/nxcals_pro/data/2/20229/187114/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12263/14512/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13801/175661/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16931/213007/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15268/176512/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12102/14401/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16800/222992/2021/8",

                //from this file we have around 31 files per month, 130k and more, before we have some scattered files only

                "/project/nxcals/nxcals_pro/data/2/11961/13972/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11986/14309/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16023/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12125/182081/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16507/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12099/14389/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16515/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16518/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16519/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16543/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16556/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16558/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16559/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16566/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16549/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16567/15567/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11804/14129/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16564/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16530/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16552/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16553/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16053/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13596/175415/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16101/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16571/15567/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16093/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16476/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16359/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16479/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/20235/177892/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13625/175426/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11988/176331/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15083/187129/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16044/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16095/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16022/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16375/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/21255/182496/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15945/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16114/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12260/14499/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16563/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16677/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11815/14170/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11838/14170/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11839/14170/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12030/14170/2021/8",
                "/project/nxcals/nxcals_pro/data/2/21260/182496/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11736/14073/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11816/14170/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12043/14170/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16416/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16468/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16629/310491/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16648/310491/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16650/310491/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16651/310491/2021/8",
                "/project/nxcals/nxcals_pro/data/2/20729/177892/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16404/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16366/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16410/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12950/175216/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16620/310491/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16628/310491/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16647/310491/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16649/310491/2021/8",
                "/project/nxcals/nxcals_pro/data/2/18726/184620/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16865/186643/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16871/186643/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16881/186643/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16882/186643/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16883/186643/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16884/186643/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16108/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16872/186643/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16873/186643/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14794/182331/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12100/14390/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11546/13946/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16582/177364/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11592/13991/2021/8",
                "/project/nxcals/nxcals_pro/data/2/20261/186629/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14246/181071/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16008/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16829/240492/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11670/14037/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11650/13971/2021/8",
                "/project/nxcals/nxcals_pro/data/2/21228/186140/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12853/13918/2021/8",
                "/project/nxcals/nxcals_pro/data/2/20236/182496/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16040/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12057/14381/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16568/177364/2021/8",
                "/project/nxcals/nxcals_pro/data/2/20769/185613/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11782/14116/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16713/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11748/177779/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12825/175075/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16473/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/83221/15933/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16374/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16630/309493/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11929/14253/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16671/309493/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16111/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16619/309493/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16627/309493/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12821/176341/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13092/175220/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12967/175220/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13086/175220/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12970/175220/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14832/346496/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14845/346496/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12836/323492/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13646/175511/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16431/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12381/14835/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12822/175072/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11831/14073/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11821/14073/2021/8",
                "/project/nxcals/nxcals_pro/data/2/21227/187134/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13647/175512/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11813/14073/2021/8",
                "/project/nxcals/nxcals_pro/data/2/141242/340505/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16377/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11809/14153/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11959/177375/2021/8",
                "/project/nxcals/nxcals_pro/data/2/20764/182496/2021/8",
                "/project/nxcals/nxcals_pro/data/2/141244/341505/2021/8",
                "/project/nxcals/nxcals_pro/data/2/26722/197114/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12844/175094/2021/8",
                "/project/nxcals/nxcals_pro/data/2/139240/339521/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12028/14445/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16573/177364/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11743/14096/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12973/175203/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12933/175203/2021/8",
                "/project/nxcals/nxcals_pro/data/2/42229/236501/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16899/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11478/13878/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11485/13884/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11492/13892/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11966/14283/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16764/222991/2021/8",
                "/project/nxcals/nxcals_pro/data/2/140241/341504/2021/8",
                "/project/nxcals/nxcals_pro/data/2/66222/267494/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11965/14282/2021/8",
                "/project/nxcals/nxcals_pro/data/2/18061/182488/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13786/15933/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11765/14079/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12256/182522/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11466/13863/2021/8",
                "/project/nxcals/nxcals_pro/data/2/38225/231491/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14911/176351/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16390/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16444/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/141239/339517/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11531/13931/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16806/222991/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15412/176791/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16825/222991/2021/8",
                "/project/nxcals/nxcals_pro/data/2/140238/339519/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11758/276491/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12843/175093/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11482/13881/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11991/14297/2021/8",
                "/project/nxcals/nxcals_pro/data/2/139239/339518/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11571/13971/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15324/176795/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11473/13873/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11939/14266/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16462/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/92222/296494/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16921/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/139241/339520/2021/8",
                "/project/nxcals/nxcals_pro/data/2/140240/341506/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11483/13882/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12827/190611/2021/8",
                "/project/nxcals/nxcals_pro/data/2/20721/216992/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16791/222991/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16743/222991/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13305/177576/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13291/177576/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13292/177576/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13312/177576/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11467/13862/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15322/176797/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15313/176770/2021/8",
                "/project/nxcals/nxcals_pro/data/2/141243/340506/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11477/13877/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11510/176373/2021/8",
                "/project/nxcals/nxcals_pro/data/2/20233/182496/2021/8",
                "/project/nxcals/nxcals_pro/data/2/20757/186115/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11465/13866/2021/8",
                "/project/nxcals/nxcals_pro/data/2/167221/365492/2021/8",
                "/project/nxcals/nxcals_pro/data/2/20753/186115/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11903/15871/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11983/14306/2021/8",
                "/project/nxcals/nxcals_pro/data/2/20750/186115/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15328/176810/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16788/222991/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15315/176788/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12075/14377/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15323/176892/2021/8",
                "/project/nxcals/nxcals_pro/data/2/140237/341503/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15343/176805/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15333/176793/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11558/13958/2021/8",
                "/project/nxcals/nxcals_pro/data/2/62221/255491/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15341/176821/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15332/176816/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11572/13972/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15338/176803/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15339/176820/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12833/175083/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15319/176817/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15325/176794/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15353/176809/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12731/174891/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15327/176768/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15331/176818/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15411/176802/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15334/176801/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15326/176796/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15342/176813/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12103/14402/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15357/176806/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15358/176815/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15316/176790/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15356/176814/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16007/177344/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16409/177344/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16498/177344/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15318/176819/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15355/176811/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16860/222991/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16762/222991/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15320/176800/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15336/176792/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15344/176799/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15351/176769/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15317/176798/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15335/176808/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15314/176807/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13313/177576/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13316/177576/2021/8",
                "/project/nxcals/nxcals_pro/data/2/128221/14153/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15401/176787/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15352/176767/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15354/176804/2021/8",
                "/project/nxcals/nxcals_pro/data/2/89221/295491/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11484/13883/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16071/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11618/14021/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15337/176812/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11463/13868/2021/8",
                "/project/nxcals/nxcals_pro/data/2/140232/341501/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12024/14329/2021/8",
                "/project/nxcals/nxcals_pro/data/2/140234/340499/2021/8",
                "/project/nxcals/nxcals_pro/data/2/141228/341501/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12491/15931/2021/8",
                "/project/nxcals/nxcals_pro/data/2/41221/230491/2021/8",
                "/project/nxcals/nxcals_pro/data/2/141236/339508/2021/8",
                "/project/nxcals/nxcals_pro/data/2/141230/340499/2021/8",
                "/project/nxcals/nxcals_pro/data/2/139227/339508/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13044/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13103/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12992/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13058/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13077/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13085/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13089/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13107/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12974/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12996/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12997/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12998/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13004/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13006/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13011/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13024/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13032/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13034/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13036/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13055/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13067/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13069/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13071/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13073/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13083/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13087/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13094/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13102/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13106/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12936/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12960/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12964/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12975/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12981/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12986/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12999/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13002/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13005/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13007/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13019/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13021/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13042/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13050/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13051/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13056/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13064/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13074/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13076/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13079/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13093/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13097/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13105/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12969/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12972/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13000/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13020/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13030/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13052/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13059/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13084/175205/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11970/14287/2021/8",
                "/project/nxcals/nxcals_pro/data/2/139230/341500/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12028/14463/2021/8",
                "/project/nxcals/nxcals_pro/data/2/140226/341499/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11662/14033/2021/8",
                "/project/nxcals/nxcals_pro/data/2/140223/341500/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14839/345500/2021/8",
                "/project/nxcals/nxcals_pro/data/2/140230/340500/2021/8",
                "/project/nxcals/nxcals_pro/data/2/141235/339510/2021/8",
                "/project/nxcals/nxcals_pro/data/2/141225/340500/2021/8",
                "/project/nxcals/nxcals_pro/data/2/139234/341499/2021/8",
                "/project/nxcals/nxcals_pro/data/2/82221/288491/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11612/13998/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12293/265491/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13296/177576/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13295/177576/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13300/177576/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13293/177576/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13298/177576/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13303/177576/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13315/177576/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13314/177576/2021/8",
                "/project/nxcals/nxcals_pro/data/2/139226/339510/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12968/175221/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12977/175221/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12983/175221/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12987/175221/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12989/175221/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13013/175221/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13016/175221/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13043/175221/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13057/175221/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13078/175221/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12955/175221/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12982/175221/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13012/175221/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13014/175221/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13048/175221/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13063/175221/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13018/175221/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13031/175221/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13046/175221/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13104/175221/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12114/177893/2021/8",
                "/project/nxcals/nxcals_pro/data/2/139228/341497/2021/8",
                "/project/nxcals/nxcals_pro/data/2/140225/340501/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16960/15395/2021/8",
                "/project/nxcals/nxcals_pro/data/2/38224/228496/2021/8",
                "/project/nxcals/nxcals_pro/data/2/39224/227500/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16956/15395/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13299/177576/2021/8",
                "/project/nxcals/nxcals_pro/data/2/141237/341497/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13321/177576/2021/8",
                "/project/nxcals/nxcals_pro/data/2/139233/340501/2021/8",
                "/project/nxcals/nxcals_pro/data/2/141229/341498/2021/8",
                "/project/nxcals/nxcals_pro/data/2/140233/341498/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12033/182053/2021/8",
                "/project/nxcals/nxcals_pro/data/2/24221/204127/2021/8",
                "/project/nxcals/nxcals_pro/data/2/82222/288493/2021/8",
                "/project/nxcals/nxcals_pro/data/2/13311/177576/2021/8",
                "/project/nxcals/nxcals_pro/data/2/17303/177274/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12870/14646/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16367/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/15910/177342/2021/8",
                "/project/nxcals/nxcals_pro/data/2/20763/186629/2021/8",
                "/project/nxcals/nxcals_pro/data/2/138223/337495/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11639/14010/2021/8",
                "/project/nxcals/nxcals_pro/data/2/141233/339509/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11566/182028/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11716/14054/2021/8",
                "/project/nxcals/nxcals_pro/data/2/20770/232492/2021/8",
                "/project/nxcals/nxcals_pro/data/2/139224/339509/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12642/15883/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11740/13998/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11975/204129/2021/8",
                "/project/nxcals/nxcals_pro/data/2/104224/309496/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12076/14377/2021/8",
                "/project/nxcals/nxcals_pro/data/2/20273/14153/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12643/15884/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14253/177082/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11623/178031/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14252/177774/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12553/15795/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16363/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/154223/357495/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14239/177773/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14245/177067/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14240/177076/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12051/14356/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16958/15395/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14254/177083/2021/8",
                "/project/nxcals/nxcals_pro/data/2/129221/14153/2021/8",
                "/project/nxcals/nxcals_pro/data/2/127222/14153/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11900/14227/2021/8",
                "/project/nxcals/nxcals_pro/data/2/21254/186143/2021/8",
                "/project/nxcals/nxcals_pro/data/2/160225/359505/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16954/15395/2021/8",
                "/project/nxcals/nxcals_pro/data/2/21258/187114/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16854/240492/2021/8",
                "/project/nxcals/nxcals_pro/data/2/82223/288492/2021/8",
                "/project/nxcals/nxcals_pro/data/2/150231/345499/2021/8",
                "/project/nxcals/nxcals_pro/data/2/150230/345499/2021/8",
                "/project/nxcals/nxcals_pro/data/2/150236/345499/2021/8",
                "/project/nxcals/nxcals_pro/data/2/150227/345499/2021/8",
                "/project/nxcals/nxcals_pro/data/2/150226/345499/2021/8",
                "/project/nxcals/nxcals_pro/data/2/150237/345499/2021/8",
                "/project/nxcals/nxcals_pro/data/2/150238/345499/2021/8",
                "/project/nxcals/nxcals_pro/data/2/150228/345499/2021/8",
                "/project/nxcals/nxcals_pro/data/2/150235/345499/2021/8",
                "/project/nxcals/nxcals_pro/data/2/150240/345499/2021/8",
                "/project/nxcals/nxcals_pro/data/2/150229/345499/2021/8",
                "/project/nxcals/nxcals_pro/data/2/150239/345499/2021/8",
                "/project/nxcals/nxcals_pro/data/2/150234/345499/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14735/181061/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12129/14152/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12117/14152/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12108/14152/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12128/14152/2021/8",
                "/project/nxcals/nxcals_pro/data/2/103222/309494/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16448/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/150233/345499/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11791/14134/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12028/340508/2021/8",
                "/project/nxcals/nxcals_pro/data/2/20739/182496/2021/8",
                "/project/nxcals/nxcals_pro/data/2/150224/345499/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14682/186611/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11909/14235/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14835/346496/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14815/347496/2021/8",
                "/project/nxcals/nxcals_pro/data/2/14828/347496/2021/8",
                "/project/nxcals/nxcals_pro/data/2/150225/345499/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16844/222991/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16849/222991/2021/8",
                "/project/nxcals/nxcals_pro/data/2/150232/345499/2021/8",
                "/project/nxcals/nxcals_pro/data/2/16897/177339/2021/8",
                "/project/nxcals/nxcals_pro/data/2/12028/190613/2021/8",
                "/project/nxcals/nxcals_pro/data/2/11733/14054/2021/8",
                "/project/nxcals/nxcals_pro/data/2/20258/187139/2021/8"

        );
    }

    @Data
    public static class Query {
        private final Long entity;
        private final TimeWindow timeWindow;
        private final Set<String> fields;
    }

    @Data
    public static class QueryResult {
        private final Query query;
        private final List<Long> queryTimes;
        private final long count;
    }


//    private Dataset<Row> repartitionByTimeInterval(DataProcessingJob job, Dataset<Row> dataset) {
//        String timestampField = getTimestampCache().get(job.getSystemId());
//        Column epochTimestampCol = dataset.col(timestampField)
//                .divide(functions.lit(TimeUtils.CONVERT_EPOCH_NANOS_TO_SECONDS_VALUE))
//                .cast(DataTypes.TimestampType);
//        return dataset.withColumn(PARTITION_BY_HOUR_COLUMN, functions.hour(epochTimestampCol))
//                .repartition(col(PARTITION_BY_HOUR_COLUMN));
//    }
//
//    private Map<String, Dataset<Row>> repartitionByHourMap(DataProcessingJob job, Dataset<Row> dataset) {
//        Dataset<Row> datasetPartitionedByHour = repartitionByHour(job, dataset);
//        //remove redundant repartition column at the end of operation
//        return IntStream.range(0, 24).boxed()
//                .collect(Collectors.toMap(hour -> String.format(HOUR_DIRECTORY_NAME_FORMAT, hour),
//                        hour -> datasetPartitionedByHour.where(col(PARTITION_BY_HOUR_COLUMN)
//                                .equalTo(functions.lit(hour)))
//                                .drop(PARTITION_BY_HOUR_COLUMN)));
//    }
}
