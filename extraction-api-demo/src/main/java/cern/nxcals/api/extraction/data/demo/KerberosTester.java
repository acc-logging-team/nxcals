package cern.nxcals.api.extraction.data.demo;

import cern.nxcals.api.config.SparkProperties;
import cern.nxcals.api.security.KerberosRelogin;
import cern.nxcals.api.utils.SparkUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.SparkSession;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

@Slf4j
public class KerberosTester {
    public static final String PRINCIPAL = "jwozniak";
    public static final String KEYTAB = "/opt/jwozniak/.keytab";

    static {
        System.setProperty("logging.config", "classpath:log4j2.yml");
    }

    public static void main(String[] args) throws Exception {
        log.debug("Start");
        KerberosRelogin kerberosRelogin = new KerberosRelogin(PRINCIPAL, KEYTAB, true);
        kerberosRelogin.start();

        SparkProperties sparkProperties = SparkProperties.defaults("KerberosTester");
        sparkProperties.getProperties().put("spark.kerberos.principal", PRINCIPAL);
        sparkProperties.getProperties().put("spark.kerberos.keytab", KEYTAB);

        sparkProperties.getProperties().put("spark.executor.instances", "2");
        sparkProperties.getProperties().put("spark.executor.cores", "2");
        sparkProperties.getProperties().put("spark.executor.memory", "2g");
        sparkProperties.getProperties().put("spark.yarn.jars", "hdfs:///project/nxcals/lib/spark-3.3.1/*.jar,hdfs:////project/nxcals/nxcals_lib/nxcals_pro/*.jar");
        sparkProperties.getProperties().put("spark.kerberos.access.hadoopFileSystems", "hadoop3");
        sparkProperties.setMasterType("yarn");

        SparkConf sparkConf = SparkUtils.createSparkConf(sparkProperties);
        SparkSession sparkSession = SparkUtils.createSparkSession(sparkConf);

        ExecutorService executorService = Executors.newFixedThreadPool(10);

        org.apache.hadoop.conf.Configuration conf = new org.apache.hadoop.conf.Configuration();
        FileSystem fileSystem = FileSystem.get(conf);

        AtomicBoolean error = new AtomicBoolean(false);

        CompletableFuture<Void> task1 = CompletableFuture.runAsync(() -> {
            while (!error.get()) {
                try {
                    log.info("Reading with Spark");
                    sparkSession.read().load("/project/nxcals/nxcals_test_h3/data/0/10001/10001/2020/10/31/*.parquet")
                            .show(1);
                    Thread.sleep(1000);
                } catch (Exception e) {
                    e.printStackTrace();
                    error.set(true);
                }
            }
        }, executorService);

        CompletableFuture<Void> task2 = CompletableFuture.runAsync(() -> {
            while (!error.get()) {
                try {
                    log.info("Reading with hdfs");
                    FileStatus[] fileStatuses = fileSystem
                            .listStatus(new Path("/project/nxcals/nxcals_test_h3/data/0/10001/10001/2020/10/31"));
                    for (FileStatus file : fileStatuses) {
                        System.out.println(file.getPath());
                    }
                    Thread.sleep(1000);
                } catch (Exception e) {
                    e.printStackTrace();
                    error.set(true);
                }
            }
        }, executorService);

        CompletableFuture.allOf(task1, task2).join();

    }

}
