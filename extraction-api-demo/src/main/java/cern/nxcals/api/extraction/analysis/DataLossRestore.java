/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.api.extraction.analysis;

import cern.nxcals.api.config.SparkContext;
import cern.nxcals.api.domain.Partition;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.extraction.metadata.InternalServiceClientFactory;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.common.utils.HdfsFileUtils;
import cern.nxcals.internal.extraction.metadata.InternalPartitionService;
import lombok.extern.slf4j.Slf4j;
import org.apache.avro.Schema;
import org.apache.commons.io.FileUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.DataTypes;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import static org.apache.spark.sql.functions.col;

@Import(SparkContext.class)
@SpringBootApplication
@Slf4j
public class DataLossRestore {

    static {
        //        System.setProperty("logging.config", "classpath:log4j2.yml");
        //
        //        //         Uncomment in order to overwrite the default security settings.
        //        //        System.setProperty("javax.net.ssl.trustStore", "/opt/nxcalsuser/nxcals_cacerts");
        //        //        System.setProperty("javax.net.ssl.trustStorePassword", "nxcals");
        //
        //        //        Kerberos credentials (or comment them out to allow them be automatically discovered via krb ticket cache)
        //        //        String user = System.getProperty("user.name");
        //        System.setProperty("kerberos.principal", "jwozniak");
        System.setProperty("kerberos.principal", "acclog");
        //        System.setProperty("kerberos.keytab", "/opt/jwozniak/.keytab");
        System.setProperty("kerberos.keytab", "/opt/jwozniak/.keytab-acclog");
        //
        //        //         TEST Env
        //                System.setProperty("service.url", "https://nxcals-test4.cern.ch:19093,https://nxcals-test5.cern.ch:19093,https://nxcals-test6.cern.ch:19093");
        //        //         TESTBED
        //        //        System.setProperty("service.url",
        //        //                "https://cs-ccr-testbed2.cern.ch:19093,https://cs-ccr-testbed3.cern.ch:19093,https://cs-ccr-nxcalstbs1.cern.ch:19093");
        //        //         PRO
        System.setProperty("service.url",
                "https://cs-ccr-nxcals6.cern.ch:19093,https://cs-ccr-nxcals7.cern.ch:19093,https://cs-ccr-nxcals8.cern.ch:19093");
        //        //Hadoop 3
        //        System.setProperty("service.url",
        //                "https://cs-ccr-nxcalstst2.cern.ch:19093,https://cs-ccr-nxcalstst3.cern.ch:19093,https://cs-ccr-nxcalstst4.cern.ch:19093");
        //dev-jwozniak
        //        System.setProperty("service.url",
        //                "https://nxcals-jwozniak-1.cern.ch:19093,https://nxcals-jwozniak-2.cern.ch:19093");

        //        System.setProperty("NXCALS_RBAC_AUTH", "true");
        //        System.setProperty("kerberos.auth.disable", "true");

    }

    public static void main(String[] args) throws IOException {

        ConfigurableApplicationContext context = SpringApplication.run(DataLossRestore.class, args);
        SparkSession sparkSession = context.getBean(SparkSession.class);
        log.info("Got spark session {}", sparkSession);
        List<String> dirs = FileUtils.readLines(new File("./dirs-last.out"), "UTF8");
        log.info("Dirs size {},  {} ", dirs.size(), dirs);
        SystemSpecService systemService = ServiceClientFactory.createSystemSpecService();
        InternalPartitionService partitionService = InternalServiceClientFactory.createPartitionService();

        ExecutorService executorService = Executors.newFixedThreadPool(1);

        AtomicLong count = new AtomicLong(dirs.size());
        PrintWriter errorFile = new PrintWriter("./dirs-errors2.out");
        PrintWriter restoredFile = new PrintWriter("./dirs-restored2.out");
        FileSystem fileSystem = FileSystem.get(new Configuration());

        Set<CompletableFuture<String>> collect = dirs.stream().map(dir ->
                CompletableFuture.supplyAsync(
                        () -> {
                            String[] split = dir.split("/");
                            long systemId = Long.parseLong(split[0]);
                            long partitionId = Long.parseLong(split[1]);
                            long schemaId = Long.parseLong(split[2]);
                            String dateStr = split[3];
                            String[] dateSplit = dateStr.split("-");
                            long year = Long.parseLong(dateSplit[0]);
                            long month = Long.parseLong(dateSplit[1]);
                            long day = Long.parseLong(dateSplit[2]);

                            SystemSpec systemSpec = systemService.findById(systemId)
                                    .orElseThrow(() -> new RuntimeException("No such system " + systemId));
                            String timestampName = Schema.parse(systemSpec.getTimeKeyDefinitions()).getFields().get(0)
                                    .name();
                            String part = getPartition(partitionService, partitionId);
                            String recordVersionKeyDefinitions = systemSpec.getRecordVersionKeyDefinitions();

                            String recordVersionName = null;
                            if (recordVersionKeyDefinitions != null) {
                                recordVersionName = Schema.parse(recordVersionKeyDefinitions).getFields().get(0).name();
                            }

                            log.info(
                                    "Restoring dir " + dir + " system=" + systemId + " part=" + partitionId + " schema="
                                            + schemaId + " y=" + year + " m=" + month + " d=" + day + " System "
                                            + systemSpec.getName() + " ts->" + timestampName
                                            + " part=" + part + " version=" + recordVersionName);

                            try {
                                ///project/nxcals/nxcals_pro/staging-backup-2024-07-06-11h42/ETL-1-1/2/10110/652/2024-07-04
                                Dataset<Row> avro = sparkSession.read().format("avro")
                                        .load("/project/nxcals/nxcals_pro/staging-backup-2024-07-06-11h42/*/" + dir + "/*/*.avro");

                                Dataset<Row> parquetRead = sparkSession.read()
                                        .parquet(
                                                "/project/nxcals/nxcals_pro/data/" + systemId + "/" + partitionId + "/" + schemaId + "/" + year + "/" + month + "/" + day + "/*.parquet");
                                List<Column> parquetColumns = new ArrayList<>();
                                parquetColumns.add(col("__sys_nxcals_entity_id__").as("parquet_entity_id"));
                                parquetColumns.add(col(timestampName).as("parquet_ts"));
                                if (recordVersionName != null) {
                                    parquetColumns.add(col(recordVersionName).as("parquet_version"));
                                }
                                Dataset<Row> parquet = parquetRead.select(parquetColumns.toArray(new Column[] {}));

                                Column joinColumn = avro.col("__sys_nxcals_entity_id__")
                                        .equalTo(parquet.col("parquet_entity_id"))
                                        .and(avro.col(timestampName).equalTo(parquet.col("parquet_ts")));

                                if (recordVersionName != null) {
                                    joinColumn = joinColumn
                                            .and(avro.col(recordVersionName).equalTo(parquet.col("parquet_version")));
                                }

                                Dataset<Row> output = avro.join(parquet,
                                        joinColumn
                                        , "leftouter"
                                ).where("parquet_entity_id is null");

                                //                                output.show();

                                output = output.drop("parquet_ts", "parquet_entity_id");

                                if (recordVersionName != null) {
                                    output = output.drop("parquet_version");
                                }

                                output = addTimePartitionColumn(output, timestampName);

                                //                                output.show();

                                //for the moment saving to some other staging dir, to be checked and changed to normal staging
                                saveAvro("/project/nxcals/nxcals_pro/staging-restore-test/RESTORED/" + dir, output);

                                renameDirs(fileSystem,
                                        "/project/nxcals/nxcals_pro/staging-restore-test/RESTORED/" + dir);

                                restoredFile.println(dir);
                                restoredFile.flush();

                            } catch (Exception ex) {
                                log.error(dir + " part=" + part + ", no data ", ex);
                                errorFile.println(dir + " " + ex.getMessage());
                                errorFile.flush();
                            } finally {
                                long left = count.addAndGet(-1L);
                                log.info("Finished job for {}, left {} ", dir, left);
                            }
                            return "Finish";
                        }, executorService)
        ).collect(Collectors.toSet());

        CompletableFuture.allOf(collect.toArray(new CompletableFuture[] {})).join();
        errorFile.close();
        restoredFile.close();
        log.info("Finished restoring!");
    }

    /**
     * Deleting /project/nxcals/nxcals_pro/staging-restore-test/RESTORED/16/27722/198117/2024-07-06/_SUCCESS
     * renaming:
     * /project/nxcals/nxcals_pro/staging-restore-test/RESTORED/16/27722/198117/2024-07-06/__sys_nxcals__time_partition__=0/part-00000-9cca7586-c6e3-41df-926b-aed0857acd97.c000.avro
     * to:
     * /project/nxcals/nxcals_pro/staging-restore-test/RESTORED/16/27722/198117/2024-07-06/0/part-00000-9cca7586-c6e3-41df-926b-aed0857acd97.c000.avro
     *
     * @param fileSystem
     * @param dir
     * @throws IOException
     */
    private static void renameDirs(FileSystem fileSystem, String dir) throws IOException {
        log.info("Renaming in path {}", dir);
        //recursive as the files are with partitions as directories
        fileSystem.delete(new Path(dir + "/_SUCCESS"), true);

        List<FileStatus> files = Arrays.asList(fileSystem.globStatus(new Path(dir + "/*")));

        log.info("Found dirs {} {}", files.size(), files.stream().map(f -> f.getPath().toString()).collect(
                Collectors.toList()));

        for (FileStatus fileStatus : files) {
                Path sourceFilePath = fileStatus.getPath();
            String dirName = sourceFilePath.getName();
                //__sys_nxcals__time_partition__=0

            if (dirName.startsWith("__sys_nxcals__time_partition__")) {
                String timePart = dirName.split("=")[1];
                Path destinationDir = new Path(dir + Path.SEPARATOR + timePart);
                log.info("Renaming {} into {}", sourceFilePath, destinationDir);
                HdfsFileUtils.rename(fileSystem, sourceFilePath, destinationDir);
            } else {
                log.error("Unknown path {}, cannot rename", sourceFilePath);
            }
        }
    }



    private static void saveAvro(String destPathStr, Dataset<Row> dataset) {
        log.info("Saving avro to {} ", destPathStr);
        dataset.write()
                .partitionBy("__sys_nxcals__time_partition__")
                .mode(SaveMode.Append)
                .format("avro")
                .save(destPathStr);
    }

    private static Dataset<Row> addTimePartitionColumn(Dataset<Row> dataset, String timestampColumnName) {
        //copied from adaptive compaction algo
        long intervalInSeconds = TimeUtils.SECONDS_IN_DAY / 24;
        Column timePartitionColumn = dataset.col(timestampColumnName)
                .divide(functions.lit(TimeUtils.CONVERT_EPOCH_NANOS_TO_SECONDS_VALUE)) //to seconds only
                .mod(functions.lit(TimeUtils.SECONDS_IN_DAY)) //will have seconds since the beginning of the day
                .divide(functions.lit(intervalInSeconds)) //dividing by interval length will get the partition number
                .cast(DataTypes.LongType); //interested  in integrals only
        dataset = dataset
                .withColumn("__sys_nxcals__time_partition__", timePartitionColumn);

        return dataset.repartitionByRange(24, dataset.col("__sys_nxcals__time_partition__"));
    }

    private static String getPartition(InternalPartitionService partitionService, long partitionId) {
        Optional<Partition> partition = partitionService.findById(partitionId);
        String part = "UNKNOWN";
        if (partition.isPresent()) {
            Partition p = partition.get();
            part = p.getKeyValues().toString();
        }
        return part;
    }
}
