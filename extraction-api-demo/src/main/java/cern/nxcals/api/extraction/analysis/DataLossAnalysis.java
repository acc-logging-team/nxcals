/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.api.extraction.analysis;

import cern.nxcals.api.config.SparkContext;
import cern.nxcals.api.domain.Partition;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.extraction.metadata.InternalServiceClientFactory;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.common.SystemFields;
import cern.nxcals.internal.extraction.metadata.InternalPartitionService;
import cern.rbac.client.authentication.AuthenticationClient;
import cern.rbac.client.authentication.AuthenticationException;
import cern.rbac.common.RbaToken;
import cern.rbac.util.holder.ClientTierTokenHolder;
import lombok.extern.slf4j.Slf4j;
import org.apache.avro.Schema;
import org.apache.commons.io.FileUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import static org.apache.spark.sql.functions.col;

@Import(SparkContext.class)
@SpringBootApplication
@Slf4j
public class DataLossAnalysis {

    static {
        //        System.setProperty("logging.config", "classpath:log4j2.yml");
        //
        //        //         Uncomment in order to overwrite the default security settings.
        //        //        System.setProperty("javax.net.ssl.trustStore", "/opt/nxcalsuser/nxcals_cacerts");
        //        //        System.setProperty("javax.net.ssl.trustStorePassword", "nxcals");
        //
        //        //        Kerberos credentials (or comment them out to allow them be automatically discovered via krb ticket cache)
        //        //        String user = System.getProperty("user.name");
        //        System.setProperty("kerberos.principal", "jwozniak");
        System.setProperty("kerberos.principal", "acclog");
        //        System.setProperty("kerberos.keytab", "/opt/jwozniak/.keytab");
        System.setProperty("kerberos.keytab", "/opt/jwozniak/.keytab-acclog");
        //
        //        //         TEST Env
        //                System.setProperty("service.url", "https://nxcals-test4.cern.ch:19093,https://nxcals-test5.cern.ch:19093,https://nxcals-test6.cern.ch:19093");
        //        //         TESTBED
        //        //        System.setProperty("service.url",
        //        //                "https://cs-ccr-testbed2.cern.ch:19093,https://cs-ccr-testbed3.cern.ch:19093,https://cs-ccr-nxcalstbs1.cern.ch:19093");
        //        //         PRO
        System.setProperty("service.url",
                "https://cs-ccr-nxcals6.cern.ch:19093,https://cs-ccr-nxcals7.cern.ch:19093,https://cs-ccr-nxcals8.cern.ch:19093");
        //        //Hadoop 3
        //        System.setProperty("service.url",
        //                "https://cs-ccr-nxcalstst2.cern.ch:19093,https://cs-ccr-nxcalstst3.cern.ch:19093,https://cs-ccr-nxcalstst4.cern.ch:19093");
        //dev-jwozniak
        //        System.setProperty("service.url",
        //                "https://nxcals-jwozniak-1.cern.ch:19093,https://nxcals-jwozniak-2.cern.ch:19093");

        //        System.setProperty("NXCALS_RBAC_AUTH", "true");
        //        System.setProperty("kerberos.auth.disable", "true");

    }

    private static void warmUpSpark(SparkSession sparkSession) {
        log.info("Warming up the spark session and java");
        for (int i = 0; i < 10; i++) {
            sparkSession.read().parquet("/project/nxcals/nxcals_pro/data/2/14074/15804/2021/9/20").sample(0.1)
                    .collectAsList();
            sparkSession.read().parquet("/project/nxcals/nxcals_pro/data/2/14074/15804/2021/9/20")
                    .select(SystemFields.NXC_ENTITY_ID.getValue()).distinct().sample(0.1).collectAsList();
            //            warmUpHbase(sparkSession);
        }
        log.info("Warm up finished");
    }

    private static void warmUpHbase(SparkSession sparkSession) {
        long start = System.currentTimeMillis();
        Dataset<Row> dataset = DataQuery.builder(sparkSession)
                .variables()
                .system("CMW").nameEq("RADMON.CHARM2:TEMP")
                .timeWindow("2023-03-09 00:00:00.00000000", "2023-03-11 00:00:00.00000000").build();
        List<Row> rows = dataset.collectAsList();
        log.info("Queried HBASE, got {} in {} ms", rows.size(), System.currentTimeMillis() - start);
    }

    private static void login() {
        try {
            AuthenticationClient authenticationClient = AuthenticationClient.create();
            //Don't commit your password here!!!
            RbaToken token = authenticationClient.loginExplicit("jwozniak", "");
            ClientTierTokenHolder.setRbaToken(token);
        } catch (AuthenticationException e) {
            throw new IllegalArgumentException("Cannot login", e);
        }
    }
    /*
    2018-03-14 07:35:11.507 [ERROR] [pool-17-thread-1] ProcessorImpl - ERROR: Entity=mock-system_nxcals_monitoring_dev6 check=byHour
    condition= var data = dataSet.takeAsList(10); dataSet.count() == 3600
    getStartTime=2018-03-13T09:00:00Z getEndTime=2018-03-13T09:59:59.999999999Z failed with message=Expected 3600 by hour but got: 3660	nxcals-test1.cern.ch
2018-03-14 07:35:25	2018-03-14 07:35:24.363 [ERROR] [pool-17-thread-1] ProcessorImpl - ERROR: Entity=mock-system_nxcals_monitoring_dev6 check=byHour condition= var data = dataSet.takeAsList(10); dataSet.count() == 3600 getStartTime=2018-03-13T17:00:00Z getEndTime=2018-03-13T17:59:59.999999999Z failed with message=Expected 3600 by hour but got: 3660
     */

    public static void main(String[] args) throws IOException {
        //        System.out.println(PlatformDependent.maxDirectMemory());

        //        login();

        ConfigurableApplicationContext context = SpringApplication.run(DataLossAnalysis.class, args);
        SparkSession sparkSession = context.getBean(SparkSession.class);
        log.info("Got spark session {}", sparkSession);
        //        warmUpSpark(sparkSession);
        List<String> dirs = FileUtils.readLines(new File("./dirs.out"), "UTF8");
        log.info("Dirs {} ", dirs);
        SystemSpecService systemService = ServiceClientFactory.createSystemSpecService();
        InternalPartitionService partitionService = InternalServiceClientFactory.createPartitionService();

        Set<String> ok = ConcurrentHashMap.newKeySet();
        Set<String> broken = ConcurrentHashMap.newKeySet();

        FileSystem fileSystem = FileSystem.get(new Configuration());

        ExecutorService executorService = Executors.newFixedThreadPool(8);

        ///project/nxcals/nxcals_pro/staging-backup-2024-07-06-11h42/ETL-1-1/2/10110/652/2024-07-04

        Set<CompletableFuture<String>> collect = dirs.stream().map(dir ->
                CompletableFuture.supplyAsync(
                        () -> {
                            String[] split = dir.split("/");
                            long systemId = Long.parseLong(split[0]);
                            long partitionId = Long.parseLong(split[1]);
                            long schemaId = Long.parseLong(split[2]);
                            String dateStr = split[3];
                            String[] dateSplit = dateStr.split("-");
                            long year = Long.parseLong(dateSplit[0]);
                            long month = Long.parseLong(dateSplit[1]);
                            long day = Long.parseLong(dateSplit[2]);

                            SystemSpec systemSpec = systemService.findById(systemId)
                                    .orElseThrow(() -> new RuntimeException("No such system " + systemId));
                            String timestampName = Schema.parse(systemSpec.getTimeKeyDefinitions()).getFields().get(0)
                                    .name();
                            String part = getPartition(partitionService, partitionId);
                            log.info(
                                    "Checking dir " + dir + " system=" + systemId + " part=" + partitionId + " schema=" + schemaId + " y=" + year + " m=" + month + " d=" + day + " System " + systemSpec.getName() + " ts->" + timestampName + " part=" + part);

                            try {
                                String avroDir = "/project/nxcals/nxcals_pro/staging-backup-2024-07-06-11h42/*/" + dir + "/*/*.avro";
                                FileStatus[] files = fileSystem.globStatus(new Path(avroDir));

                                for (FileStatus file : files) {
                                    //                    System.out.println("File: " + file.getPath() + " size: " + file.getLen());
                                    if (file.isFile() && file.getLen() == 0) {
                                        //                        System.out.println("Deleting file " + file.getPath() + " of size: " + file.getLen());
                                        fileSystem.delete(file.getPath(), false);
                                    }
                                }

                                Dataset<Row> avro = sparkSession.read().format("avro")
                                        .load("/project/nxcals/nxcals_pro/staging-backup-2024-07-06-11h42/*/" + dir + "/*/*.avro")
                                        .select(col("__sys_nxcals_entity_id__").as("avro_entity_id"),
                                                col(timestampName).as("avro_ts"));

                                Dataset<Row> parquet = sparkSession.read()
                                        .parquet(
                                                "/project/nxcals/nxcals_pro/data/" + systemId + "/" + partitionId + "/" + schemaId + "/" + year + "/" + month + "/" + day + "/*.parquet")
                                        .select(col("__sys_nxcals_entity_id__").as("parquet_entity_id"),
                                                col(timestampName).as("parquet_ts"));
                                boolean parquetEmpty = parquet.isEmpty();

                                boolean empty = avro.join(parquet,
                                        avro.col("avro_entity_id").equalTo(parquet.col("parquet_entity_id")).and(
                                                avro.col("avro_ts").equalTo(parquet.col("parquet_ts"))), "leftouter"
                                ).where("parquet_entity_id is null").isEmpty();

                                if (empty) {
                                    log.info(dir + " has data OK, part=" + part);
                                    ok.add(dir + " has data OK, part=" + part);
                                } else {
                                    broken.add(
                                            dir + " has data MISSING, is parquet empty=" + parquetEmpty + " part=" + part);
                                    log.error(
                                            dir + " has data MISSING, is parquet empty=" + parquetEmpty + " part=" + part);
                                }
                            } catch (Exception ex) {
                                broken.add(dir + " part=" + part + ", no data " + ex.getMessage());
                                log.error(dir + " part=" + part + ", no data " + ex.getMessage());
                            }
                            return "Finish";
                        }, executorService)
        ).collect(Collectors.toSet());

        CompletableFuture.allOf(collect.toArray(new CompletableFuture[] {})).join();

        log.info("OK dirs " + ok);
        log.error("MISSING DATA: ");
        broken.forEach(str -> log.error("{}", str));

    }

    private static String getPartition(InternalPartitionService partitionService, long partitionId) {
        Optional<Partition> partition = partitionService.findById(partitionId);
        String part = "UNKNOWN";
        if (partition.isPresent()) {
            Partition p = partition.get();
            part = p.getKeyValues().toString();
        }
        return part;
    }
}
