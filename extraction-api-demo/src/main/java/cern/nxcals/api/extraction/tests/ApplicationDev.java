/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.api.extraction.tests;

import cern.nxcals.api.config.SparkContext;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.common.SystemFields;
import cern.rbac.client.authentication.AuthenticationClient;
import cern.rbac.client.authentication.AuthenticationException;
import cern.rbac.common.RbaToken;
import cern.rbac.util.holder.ClientTierTokenHolder;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;

import java.util.List;

@Import(SparkContext.class)
@SpringBootApplication
@Slf4j
public class ApplicationDev {

    static {
        //        System.setProperty("logging.config", "classpath:log4j2.yml");
        //
        //        //         Uncomment in order to overwrite the default security settings.
        //        //        System.setProperty("javax.net.ssl.trustStore", "/opt/nxcalsuser/nxcals_cacerts");
        //        //        System.setProperty("javax.net.ssl.trustStorePassword", "nxcals");
        //
        //        //        Kerberos credentials (or comment them out to allow them be automatically discovered via krb ticket cache)
        //        //        String user = System.getProperty("user.name");
        System.setProperty("kerberos.principal", "jwozniak");
        //        System.setProperty("kerberos.principal", "acclog");
        System.setProperty("kerberos.keytab", "/opt/jwozniak/.keytab");
        //        System.setProperty("kerberos.keytab", "/opt/jwozniak/.keytab-acclog");
        //
        //        //         TEST Env
        //                System.setProperty("service.url", "https://nxcals-test4.cern.ch:19093,https://nxcals-test5.cern.ch:19093,https://nxcals-test6.cern.ch:19093");
        //        //         TESTBED
        //        //        System.setProperty("service.url",
        //        //                "https://cs-ccr-testbed2.cern.ch:19093,https://cs-ccr-testbed3.cern.ch:19093,https://cs-ccr-nxcalstbs1.cern.ch:19093");
        //        //         PRO
        //        System.setProperty("service.url",
        //                "https://cs-ccr-nxcals6.cern.ch:19093,https://cs-ccr-nxcals7.cern.ch:19093,https://cs-ccr-nxcals8.cern.ch:19093");
        //        //Hadoop 3
        //        System.setProperty("service.url",
        //                "https://cs-ccr-nxcalstst2.cern.ch:19093,https://cs-ccr-nxcalstst3.cern.ch:19093,https://cs-ccr-nxcalstst4.cern.ch:19093");
        //dev-jwozniak
        System.setProperty("service.url",
                "https://nxcals-jwozniak-1.cern.ch:19093,https://nxcals-jwozniak-2.cern.ch:19093");

        //        System.setProperty("NXCALS_RBAC_AUTH", "true");
        //        System.setProperty("kerberos.auth.disable", "true");

    }

    private static void warmUpSpark(SparkSession sparkSession) {
        log.info("Warming up the spark session and java");
        for (int i = 0; i < 10; i++) {
            sparkSession.read().parquet("/project/nxcals/nxcals_pro/data/2/14074/15804/2021/9/20").sample(0.1)
                    .collectAsList();
            sparkSession.read().parquet("/project/nxcals/nxcals_pro/data/2/14074/15804/2021/9/20")
                    .select(SystemFields.NXC_ENTITY_ID.getValue()).distinct().sample(0.1).collectAsList();
            //            warmUpHbase(sparkSession);
        }
        log.info("Warm up finished");
    }

    private static void warmUpHbase(SparkSession sparkSession) {
        long start = System.currentTimeMillis();
        Dataset<Row> dataset = DataQuery.builder(sparkSession)
                .variables()
                .system("CMW").nameEq("RADMON.CHARM2:TEMP")
                .timeWindow("2023-03-09 00:00:00.00000000", "2023-03-11 00:00:00.00000000").build();
        List<Row> rows = dataset.collectAsList();
        log.info("Queried HBASE, got {} in {} ms", rows.size(), System.currentTimeMillis() - start);
    }

    private static void login() {

        try {
            AuthenticationClient authenticationClient = AuthenticationClient.create();
            //Don't commit your password here!!!
            RbaToken token = authenticationClient.loginExplicit("jwozniak", "");
            ClientTierTokenHolder.setRbaToken(token);
        } catch (AuthenticationException e) {
            throw new IllegalArgumentException("Cannot login", e);
        }

    }
    /*
    2018-03-14 07:35:11.507 [ERROR] [pool-17-thread-1] ProcessorImpl - ERROR: Entity=mock-system_nxcals_monitoring_dev6 check=byHour
    condition= var data = dataSet.takeAsList(10); dataSet.count() == 3600
    getStartTime=2018-03-13T09:00:00Z getEndTime=2018-03-13T09:59:59.999999999Z failed with message=Expected 3600 by hour but got: 3660	nxcals-test1.cern.ch
2018-03-14 07:35:25	2018-03-14 07:35:24.363 [ERROR] [pool-17-thread-1] ProcessorImpl - ERROR: Entity=mock-system_nxcals_monitoring_dev6 check=byHour condition= var data = dataSet.takeAsList(10); dataSet.count() == 3600 getStartTime=2018-03-13T17:00:00Z getEndTime=2018-03-13T17:59:59.999999999Z failed with message=Expected 3600 by hour but got: 3660
     */

    public static void main(String[] args) {
        //        System.out.println(PlatformDependent.maxDirectMemory());

        //        login();

        ConfigurableApplicationContext context = SpringApplication.run(ApplicationDev.class, args);
        SparkSession sparkSession = context.getBean(SparkSession.class);
        log.info("Got spark session {}", sparkSession);
        //        warmUpSpark(sparkSession);

        for (int i = 0; i < 10000; ++i) {
            try {
                log.info("Search for data");
                long start = System.currentTimeMillis();
                Dataset<Row> dataset = DataQuery.builder(sparkSession)
                        .variables()
                        //                        .system("CMW").nameEq("HX:FILLN")
                        .system("CMW").nameEq("L4D.BPM.0303:PUTRACEPHASE")
                        //                        .system("CMW").nameEq("BI4.KSW16L1.IPOC.I_MAG:Waveform:energy")
                        .timeWindow("2023-03-11 12:33:00.00000000",
                                "2023-03-11 12:34:00.00000000") //w is accessing very small table
                        .build();
                List<Row> rows = dataset.collectAsList();
                log.info("Found {} values in {} ms", rows.size(), System.currentTimeMillis() - start);
            } catch (Exception ex) {
                log.error("Exception ", ex);
            }
        }

        //        FillService fillService = Services.newInstance(sparkSession).fillService();

        //        while(true) {
        //            log.info("Looking for fill 7252");
        //            Optional<Fill> fill = fillService.findFill(7252);
        //
        //            log.info("Found fill {}", fill.get());
        //
        //            log.info("Looking for multiple fills");
        //            List<Fill> fills = fillService.findFills(TimeUtils.getInstantFromString("2023-01-02 00:00:00.378000000"),
        //                    TimeUtils.getInstantFromString("2023-01-10 00:00:00.56000000"));
        //
        //            log.info("Found fills {}", fills);
        //
        //            log.info("Finding last fill");
        //
        //            Optional<Fill> lastCompleted = fillService.getLastCompleted();
        //
        //            log.info("Found {}", lastCompleted.get());
        //        }

        //        Random rand = new Random();
        //        ExecutorService executorService = Executors.newFixedThreadPool(5);
        //
        //        AtomicBoolean error = new AtomicBoolean(false);
        //
        //        for (int i = 0; i < 100000; ++i) {
        //            int minute = rand.nextInt(LocalDateTime.now().get(ChronoField.MINUTE_OF_HOUR)-3);
        //            String minuteStr = minute < 10 ? "0"+minute: "" + minute;
        //            int dev = rand.nextInt(9);
        //            int iter = i;
        //
        //            executorService.submit(() -> {
        //
        //                if(error.get()) {
        //                    return;
        //                }
        //                Dataset<Row> dataset = DataQuery.builder(sparkSession)
        //                        .byEntities()
        //                        .system("MOCK-SYSTEM")
        //                        .startTime("2021-01-18 17:" + minuteStr + ":00.0")
        //                        .endTime("2021-01-18 17:" + minuteStr + ":59.999999999")
        //                        .entity().keyValue("device", "NXCALS_MONITORING_DEV" + dev)
        //                        .build()
        //                        .where("`special-character`='test'")
        //                        .where("longField2 < 10000")
        //                        .select("timestamp", "longField2");
        //
        ////                dataset.printSchema();
        ////                dataset.show();
        //                List<Row> rows = dataset.collectAsList();
        //                long count = dataset.count();
        //                if (count != 60 || rows.size() != 60) {
        //                    dataset.explain("formatted");
        //                    System.err.println("Detected count " + count + " != row.size() " + rows.size() + " for minute:" + minuteStr + " and dev " + dev);
        //                    rows.sort(Comparator.comparing(row -> row.getAs("timestamp")));
        //                    System.err.println(rows);
        //                    System.err.println("Executing count again " + dataset.count() + " and collect again: " + dataset.collectAsList().size());
        //                    //                            System.exit(1);
        //                    try {
        //                        error.set(true);
        //                        Thread.sleep(1000000000L);
        //                    } catch (InterruptedException e) {
        //                        e.printStackTrace();
        //                    }
        //                } else {
        //                    System.out.println("Ok " + iter + " for minute:" + minuteStr + " and dev " + dev);
        //                }
        //
        //            });
        //
        //            //            long start = System.currentTimeMillis();
        //            //            dataset.select("doubleField").where("doubleField>0.3").show();
        //            //            List<Row> rows = dataset.repartition(1).where("doubleField>0.3").collectAsList();
        //
        //            //            System.out.println("Finished! count:" + rows.size() + " in time " + (System.currentTimeMillis()-start) + " ms") ;
        //        }
        //        MetaDataService metaService = ServiceBuilder.getInstance(sparkSession).createMetaService();
        //        VariableSet fundamentals = metaSfervice
        //                .getFundamentalsInTimeWindowWithNameLikePattern(
        //                        TimestampFactory.parseUTCTimestamp("2018-04-08 08:00:00.000"),
        //                        TimestampFactory.parseUTCTimestamp("2019-04-08 08:00:00.000"), "CP%:%:%");
        //
        //        fundamentals.getVariableNames().forEach(System.out::println);

        //        KeyValueStage valueStage = builder(sparkSession).byEntities().system("PM-EVENT")
        //                .startTime("2019-09-01 00:00:00.000").endTime("2019-09-01 00:01:00.000")
        //                .entity();
        //
        //        KeyValueStageLoop pmsystem = valueStage.keyValueLike("pmsystem", "%");
        //        KeyValueStageLoop pmclass = pmsystem.keyValueLike("pmclass", "%");
        //        KeyValueStageLoop pmsource = pmclass.keyValue("pmsource", "PLACEHOLDER");
        //
        //        KeyValueStageLoop keyValueStageLoop = pmsource.keyValueLike("classificationType", "%");
        //        Dataset<Row> ds = pmsource.build();

        // Create SparkSession

        ////        sparkSession.sparkContext().hadoopConfiguration();//.set("parquet.task.side.metadata", "true");
        //        sparkSession.read().format("com.databricks.spark.avro")
        //                .load("/project/nxcals/nxcals_dev_jwozniak/staging/ETL-2-2/0/515191/11644/2018-11-14/*.avro")
        ////                .sort("__sys_nxcals_entity_id__", "__record_timestamp__").repartition(1)
        //                .write().mode(SaveMode.Overwrite)
        //                .parquet("/project/nxcals/nxcals_dev_jwozniak/test");
        //

        //        ds.show();

        //        System.exit(0);
        //        dataset.printSchema();
        //        System.out.println(dataset.count());

        //        for(int i = 0 ; i < 2; i++) {
        //            long startTime = System.currentTimeMillis();
        //            Dataset<Row> ds =
        //                    DevicePropertyQuery.builder(sparkSession).system("CMW")
        //                    .startTime("2018-04-26 00:00:00.000").endTime("2018-04-28 01:00:15.000")
        //                    .entity()
        //                    .parameter("PR.SCOPE48.CH01/Acquisition")
        //                    .build();
        //
        ////                    DevicePropertyQuery.builder(sparkSession)
        ////                    .system("CMW")
        ////                    .startTime("2018-03-03 00:00:00.0")
        ////                    .endTime("2018-03-03 23:59:59.999999999")
        ////                    .fields("longArrayField")
        //////                    .fields("doubleField")
        ////                    .entity()
        ////                    .parameter( "ZS.BA2.F3.SPARKRATE.L3_1h_15m/ExpertAcquisitionDevice")
        ////
        //////                    .entity()
        //////                    .keyValue("device", "NXCALS_DEV_5")
        //////                    .keyValue("property", "Logging")
        ////                    .build();
        //
        ////            System.out.println("Building Dataset took " +(System.currentTimeMillis()-startTime) + " ms");
        ////
        ////            //        ds.printSchema();
        //            long beforeCount = System.currentTimeMillis();
        //            long count = ds.count();
        //            System.out.println("Count=" + count  + " took " + (System.currentTimeMillis()-beforeCount) + " ms");
        //
        //            long beforeTake = System.currentTimeMillis();
        ////            List<Row> rows = ds.takeAsList(10);
        //            //ds.select("longArrayField.elements").show();
        //            List<Row> rows = ds.collectAsList();
        //            System.out.println("Take " + rows.size() +" took " + (System.currentTimeMillis()-beforeTake) + " ms");

        //            System.out.println("Total Execution took " + (System.currentTimeMillis()-startTime) + " ms");
        //            System.out.println("---------------------------------------------------------");
        //            //        ds.show(10);
        //            //        ds.createOrReplaceTempView("myview");
        //        }
        //        Dataset<Row> result  = sparkSession.sql("select * from myview where `special-character` = 'test'");
        //        Dataset<Row> result = sparkSession.sql("Select timestamp as `test-test`, count(*) from myview group by timestamp having count(*) > 1");

        //        System.out.println("More than one timestamp");
        //        result.show(10);
        //
        //        Dataset<Row> res2 = sparkSession.sql("select device as `test-test` from myview where timestamp = 1520933103000000000");
        //        res2.createOrReplaceTempView("v2");
        //
        //        Dataset<Row> sql = sparkSession.sql("Select * from v2 where `test-test` = 'test'");
        //        sql.show(10);

    }
}
