/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.nxcals.api.extraction.performance;

import cern.nxcals.api.config.SparkContext;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.common.SystemFields;
import com.google.common.collect.Lists;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.avro.Schema;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.DoubleType;
import org.apache.spark.sql.types.FloatType;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.unsafe.hash.Murmur3_x86_32;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.time.Instant;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * Comparing with Databricks Delta implementation.
 */
@Import(SparkContext.class)
@SpringBootApplication
@Slf4j
public class ApplicationPerformanceAnalysisDelta {


    private static final String PARTITION_COLUMN = "partition";
    public static final Random rand = new Random();
    private static final FileSystem fileSystem = createFileSystem();
    private static final long SECONDS_IN_DAY = 24 * 60 * 60;
    private static final int INTERVAL_IN_MINUTES = 1 * 60; //our split interval
    private static final long INTERVAL_IN_SECONDS = INTERVAL_IN_MINUTES * 60;
    private static final boolean SHOULD_FORCE_REPARTITION = false;
    private static final long NUMBER_OF_PARTITIONS = SECONDS_IN_DAY / INTERVAL_IN_SECONDS;
    private static final long PARQUET_BLOCK_SIZE_MB = 128;
    private static final long PARQUET_BLOCK_SIZE = PARQUET_BLOCK_SIZE_MB * 1024 * 1024L;
    private static final boolean SHOULD_USE_PARTITION_IN_QUERY = true;
    private static final boolean SHOULD_USE_ENTITY_BUCKETS = true;
    private static final boolean SHOULD_USE_ADAPTIVE_ROW_GROUP = true;
    private static final int NUMBER_OF_ENTITY_BUCKETS = 4;

    private static final String ENTITY_BUCKET_COLUMN = "entityBucket";
    private static final long ROW_GROUP_MIN_SIZE = 16 * 1024 * 1024L;
    private static final long ROW_GROUP_MAX_SIZE = 512 * 1024 * 1024L;
    private static final long DEFAULT_HDFS_BLOCK_SIZE = 256 * 1024 * 1024L;
    private static final long EXTENDED_HDFS_BLOCK_SIZE = 512 * 1024 * 1024L;


//    public static final String REPARTITION_PATH = "/project/nxcals/perf_tests_" + NUMBER_OF_PARTITIONS + "p_" + (SHOULD_USE_ADAPTIVE_ROW_GROUP?"-adaptive-":PARQUET_BLOCK_SIZE_MB) + "MBrg" + (SHOULD_USE_ENTITY_BUCKETS ? "_entityBuckets" + NUMBER_OF_ENTITY_BUCKETS : "") + "/data";

    public static final String REPARTITION_PATH = "/project/nxcals/perf_tests_" + NUMBER_OF_PARTITIONS + "p_" + (SHOULD_USE_ADAPTIVE_ROW_GROUP ? "_adaptive_"
            + (ROW_GROUP_MIN_SIZE / (1024 * 1024)) + "_" + (ROW_GROUP_MAX_SIZE / (1024 * 1024)) : PARQUET_BLOCK_SIZE_MB) + "MBrg" + (SHOULD_USE_ENTITY_BUCKETS ? "_entityBuckets" + NUMBER_OF_ENTITY_BUCKETS : "") + "/data";
//    public static final String REPARTITION_PATH = "/project/nxcals/perf_tests_24p_16MBrg/data";

    static {
//        System.setProperty("logging.config", "classpath:log4j2.yml");
//
//        //         Uncomment in order to overwrite the default security settings.
//        //        System.setProperty("javax.net.ssl.trustStore", "/opt/nxcalsuser/nxcals_cacerts");
//        //        System.setProperty("javax.net.ssl.trustStorePassword", "nxcals");
//
//        //        Kerberos credentials (or comment them out to allow them be automatically discovered via krb ticket cache)
//        //        String user = System.getProperty("user.name");
        System.setProperty("kerberos.principal", "jwozniak");
        System.setProperty("kerberos.keytab", "/opt/jwozniak/.keytab");
//
//        //         TEST Env
//        //        System.setProperty("service.url", "https://nxcals-test4.cern.ch:19093,https://nxcals-test5.cern.ch:19093,https://nxcals-test6.cern.ch:19093");
//        //         TESTBED
//        //        System.setProperty("service.url",
//        //                "https://cs-ccr-testbed2.cern.ch:19093,https://cs-ccr-testbed3.cern.ch:19093,https://cs-ccr-nxcalstbs1.cern.ch:19093");
//        //         PRO
        System.setProperty("service.url",
                "https://cs-ccr-nxcals6.cern.ch:19093,https://cs-ccr-nxcals7.cern.ch:19093,https://cs-ccr-nxcals8.cern.ch:19093");
//        //Hadoop 3
//        System.setProperty("service.url",
//                "https://cs-ccr-nxcalstst2.cern.ch:19093,https://cs-ccr-nxcalstst3.cern.ch:19093,https://cs-ccr-nxcalstst4.cern.ch:19093");
    }

    private static SystemSpecService systemSpecService = ServiceClientFactory.createSystemSpecService();

    public static void main(String[] args) {

        ConfigurableApplicationContext context = SpringApplication.run(ApplicationPerformanceAnalysisDelta.class, args);
        SparkSession sparkSession = context.getBean(SparkSession.class);

        List<String> inputPaths = getInputPathsForTest();

        for (String path : inputPaths) {
            log.info("Start processing {}", path);
            String repartitionedPath = repartitionToPath(sparkSession, path, SHOULD_FORCE_REPARTITION, SHOULD_USE_ENTITY_BUCKETS, SHOULD_USE_ADAPTIVE_ROW_GROUP);
            for (int i = 0; i < 1; ++i) {
                log.info("************ Run {}", i);
                List<Query> queries = getQueries(sparkSession, path);
                List<QueryResult> original = queryInPath(sparkSession, path, queries, false, false);
                List<QueryResult> repartitioned = queryInPath(sparkSession, repartitionedPath, queries, SHOULD_USE_PARTITION_IN_QUERY, SHOULD_USE_ENTITY_BUCKETS);
                compareResults(path, original, repartitionedPath, repartitioned);
            }
        }
    }

    private static FileSystem createFileSystem() {
        org.apache.hadoop.conf.Configuration conf = new org.apache.hadoop.conf.Configuration();
        try {
            return FileSystem.get(conf);
        } catch (IOException e) {
            throw new UncheckedIOException("Cannot access filesystem", e);
        }
    }

    private static void warmUpSpark(SparkSession sparkSession) {
        log.info("Warming up the spark session and java");
        for (int i = 0; i < 10; i++) {
            sparkSession.read().parquet("/project/nxcals/nxcals_pro/data/2/14074/15804/2021/8/15").distinct().collectAsList();
            sparkSession.read().parquet("/project/nxcals/nxcals_pro/data/2/14074/15804/2021/8/15").select(SystemFields.NXC_ENTITY_ID.getValue()).distinct().collectAsList();
        }
        log.info("Warm up finished");
    }

    private static void compareResults(String path, List<QueryResult> original, String repartitionedPath, List<QueryResult> repartitioned) {
        log.info("Comparing results for {} repartitioned to {}", path, repartitionedPath);
        log.info("Query times for original: {}", original);
        log.info("Query times for repartioned: {}", repartitioned);
        long better = 0;
        long worse = 0;
        long betterSum = 0;
        long worseSum = 0;
        for (int i = 0; i < original.size(); i++) {
            QueryResult org = original.get(i);
            QueryResult rep = repartitioned.get(i);
            check(org, rep);
            long orgAvgTime = average(org);
            long repAvgTime = average(rep);
            long diff = Math.abs(orgAvgTime - repAvgTime);
            long percent = (long) (((double) diff / (double) orgAvgTime) * 100);
            if (orgAvgTime > repAvgTime) {
                log.info("For query {}  BETTER for rep by {}% diff={} org={} rep={}", org.query, percent, diff, orgAvgTime, repAvgTime);
                better++;
                betterSum += percent;
            } else {
                log.warn("**** For query {} WORSE for rep by {}% diff={} org={} rep={}", org.query, percent, diff, orgAvgTime, repAvgTime);
                worse++;
                worseSum += percent;
            }

        }

        long betterAvg = 0;
        long worseAvg = 0;
        if (better > 0) {
            betterAvg = betterSum / better;
        }
        if (worse > 0) {
            worseAvg = worseSum / worse;
        }

        Directory orgDir = analysePath(path);
        Directory repDir = analysePath(repartitionedPath);

        long countPercent = (long) ((Math.abs(repDir.fileCount - orgDir.fileCount) / (double) orgDir.fileCount) * 100);
        long sizePercent = (long) ((Math.abs(repDir.totalSize - orgDir.totalSize) / (double) orgDir.totalSize) * 100);

        log.info("Finished analysis for {}, split={} min, rowg={} MB, query part={}, better {} with avg {}% worse {} with avg {}% (org dir count: {} {} MB, rep dir count: {} {} MB, count diff: {}% size diff: {}%)",
                repartitionedPath, INTERVAL_IN_MINUTES, (SHOULD_USE_ADAPTIVE_ROW_GROUP ? "adaptive" : PARQUET_BLOCK_SIZE / (1024 * 1024)),
                SHOULD_USE_PARTITION_IN_QUERY,
                better, betterAvg, worse, worseAvg,
                orgDir.fileCount,
                String.format("%,.3f", orgDir.totalSize / (1024 * 1024.0)),
                repDir.fileCount,
                String.format("%,.3f", repDir.totalSize / (1024 * 1024.0)),
                countPercent, sizePercent);

    }

    @SneakyThrows
    private static Directory analysePath(String path) {
        RemoteIterator<LocatedFileStatus> iterator = fileSystem.listFiles(new Path(path), true);
        long count = 0;
        long size = 0;
        while (iterator.hasNext()) {
            LocatedFileStatus file = iterator.next();
            if (file.isFile() && file.getPath().getName().endsWith(".parquet")) {
                size += file.getLen();
                count++;
            }
        }
        return new Directory(count, size);
    }

    @Data
    public static class Directory {
        private final long fileCount;
        private final long totalSize;
    }

    private static void check(QueryResult org, QueryResult rep) {
        if (org.count != rep.count || !org.query.equals(rep.query)) {
            throw new RuntimeException("This is not the same query q1=" + org + " q2=" + rep);
        }
    }

    private static long average(QueryResult org) {
        return (long) org.queryTimes.stream().mapToLong(r -> r).average().orElseThrow(() -> new RuntimeException("Cannot calculate average"));
    }

    private static List<QueryResult> queryInPath(SparkSession sparkSession, String path, List<Query> queries, boolean usePartition, boolean useEntityBuckets) {
        log.info("Start querying in path {}", path);
        String timestampColumn = getTimestampColumn(path);
        Dataset<Row> dataset = sparkSession.read().parquet(path);
        //Cannot parallelize as this introduces a very big random factor to query times as they affect each other.
        return queries.stream().map(query -> query(dataset, query, timestampColumn, usePartition, useEntityBuckets)).collect(Collectors.toList());
    }

    private static QueryResult query(Dataset<Row> dataset, Query query, String timestampColumn, boolean usePartition, boolean useEntityBuckets) {
        long count = -1;
        Column[] columns = getColumns(dataset, query);
        List<Long> queryTimes = new ArrayList<>();
        for (int i = 0; i < 3; ++i) { //query multiple times for the same dataset to take average.
            long start = System.currentTimeMillis();

            long startPartition = getPartition(query.timeWindow.getStartTimeNanos());
            long endPartition = getPartition(query.timeWindow.getEndTimeNanos());
            long entityBucket = getBucket(query.entity, NUMBER_OF_ENTITY_BUCKETS);
            String where = SystemFields.NXC_ENTITY_ID.getValue() +
                    " = " + query.entity +
                    " and " + timestampColumn + " >= " + query.timeWindow.getStartTimeNanos() +
                    " and " + timestampColumn + " <= " + query.timeWindow.getEndTimeNanos();
            if (usePartition) {
                where += " and partition >= " + startPartition + " and partition <= " + endPartition;
            }
            if (useEntityBuckets) {

                where += " and " + ENTITY_BUCKET_COLUMN + " = " + entityBucket;
            }

            Dataset<Row> queryDs = dataset.select(columns).where(where);
            List<Row> rows = queryDs.collectAsList();
            long queryTime = System.currentTimeMillis() - start;
//            queryDs.explain(true);
            log.info("Run a query for {}, took {} ms, count={}, minPart={}, maxPart={}, entityBucket={} usePart={} useEntityBuckets={}", query, queryTime, rows.size(), startPartition, endPartition, entityBucket, usePartition, useEntityBuckets);
            queryTimes.add(queryTime);
            count = rows.size();
        }

        return new QueryResult(query, queryTimes, count);
    }

    private static long getPartition(long value) {
//        Column partitionColumn = dataset.col(TIMESTAMP_COLUMN)
//                .divide(functions.lit(TimeUtils.CONVERT_EPOCH_NANOS_TO_SECONDS_VALUE)) //to seconds only
//                .mod(functions.lit(SECONDS_IN_DAY)) //will have seconds since the beginning of the day
//                .divide(functions.lit(INTERVAL_IN_SECONDS)) //dividing by interval lenght will get the partition number
//                .cast(DataTypes.LongType); //interested in integrals

        return ((value / TimeUtils.CONVERT_EPOCH_NANOS_TO_SECONDS_VALUE) % SECONDS_IN_DAY) / INTERVAL_IN_SECONDS;
    }

    private static Column[] getColumns(Dataset<Row> dataset, Query query) {
        return query.getFields().stream().map(dataset::col).toArray(Column[]::new);
    }

    private static List<Query> getQueries(SparkSession sparkSession, String path) {
        List<Long> entities = getEntities(sparkSession, path, 3);
        List<TimeWindow> timeWindows = getTimewindows(sparkSession, path);
        Set<String> fields = getFields(sparkSession, path);
        log.info("For path {} using entities {} and timewindows {}", path, entities, timeWindows);
        List<Query> queries = new ArrayList<>();

        for (Long entity : entities) {
            for (TimeWindow tw : timeWindows) {
                queries.add(new Query(entity, tw, fields));
            }
        }
        return queries;
    }

    private static Set<String> getFields(SparkSession sparkSession, String path) {
        String timestampColumn = getTimestampColumn(path);
        Dataset<Row> dataset = sparkSession.read().parquet(path);
        StructField[] fields = dataset.schema().fields();
        Set<String> queryFields = new HashSet<>();
        queryFields.add(SystemFields.NXC_ENTITY_ID.getValue());
        queryFields.add(timestampColumn);
//        queryFields.add(getArrayField(fields)); <-- generating too many OOM problems, skipping arrays for this analysis
        queryFields.add(getScalarField(fields));
        return queryFields;
    }

    private static String getScalarField(StructField[] fields) {
        for (StructField field : fields) {
            if (field.dataType() instanceof DoubleType || field.dataType() instanceof FloatType) {
                return field.name();
            }
        }
        return SystemFields.NXC_SYSTEM_ID.getValue();
    }

    private static String getArrayField(StructField[] fields) {
        for (StructField field : fields) {
            if (field.dataType() instanceof StructType) {
                return field.name();
            }
        }
        return SystemFields.NXC_PARTITION_ID.getValue();
    }

    @SneakyThrows
    private static String repartitionToPath(SparkSession sparkSession, String path, boolean force, boolean useEntityBucket, boolean adaptiveRowGroup) {
        String timestampColumn = getTimestampColumn(path);
        String destPathStr = path.replace("/project/nxcals/nxcals_pro/data", REPARTITION_PATH);
        Path destPath = new Path(destPathStr);

        if (!force && fileSystem.exists(destPath) && fileSystem.listStatus(destPath).length > 1) {
            log.info("Path exists {}, skipping repartition", destPath);
            return destPathStr;
        }

        log.info("Started repartition from  {} into {} timestamp column {}", path, destPath, timestampColumn);
        Dataset<Row> dataset = sparkSession.read().parquet(path);

        Column partitionColumn = dataset.col(timestampColumn)
                .divide(functions.lit(TimeUtils.CONVERT_EPOCH_NANOS_TO_SECONDS_VALUE)) //to seconds only
                .mod(functions.lit(SECONDS_IN_DAY)) //will have seconds since the beginning of the day
                .divide(functions.lit(INTERVAL_IN_SECONDS)) //dividing by interval lenght will get the partition number
                .cast(DataTypes.LongType); //interested in integrals only

        Dataset<Row> withPart = dataset.withColumn(PARTITION_COLUMN, partitionColumn);
        List<Row> partitions = withPart.select(PARTITION_COLUMN).distinct().sort(PARTITION_COLUMN).collectAsList();
        log.info("Found {} partitions: {}", partitions.size(), partitions);

        int partitionCount = partitions.size();
        if (useEntityBucket) {
            withPart = withPart.withColumn(ENTITY_BUCKET_COLUMN, functions.pmod(functions.hash(withPart.col(SystemFields.NXC_ENTITY_ID.getValue())), functions.lit(NUMBER_OF_ENTITY_BUCKETS)));
//            withPart.select(SystemFields.NXC_ENTITY_ID.getValue(), ENTITY_BUCKET_COLUMN).distinct().show(100);
            int entityBucketCount = (int) withPart.select(ENTITY_BUCKET_COLUMN).distinct().count();
            partitionCount = partitionCount * entityBucketCount;
        }


        log.info("Saving to dest path {}", destPathStr);
        Directory orgDir = analysePath(path);
        writeParquet(sparkSession, withPart, orgDir, destPathStr, partitionCount, useEntityBucket, adaptiveRowGroup);
        log.info("Finished repartition from  {} into {}", path, destPathStr);
        return destPathStr;

    }

    private static long getBucket(long val, long nbOfBuckets) {
        //42 is used by Spark as seed...
        return pmod(Murmur3_x86_32.hashLong(val, 42), nbOfBuckets);
    }

    private static long pmod(long a, long n) {
        long r = a % n;
        if (r < 0) {
            return (r + n) % n;
        } else {
            return r;
        }
    }

    private static String getTimestampColumn(String path) {
        ///project/nxcals/nxcals_pro/data/17/29222/201119/2021/8/15
        long systemId = Long.parseLong(path.split("/")[5]);
        SystemSpec systemSpec = systemSpecService.findById(systemId).orElseThrow(() -> new RuntimeException("No such system id " + systemId));
        return new Schema.Parser().parse(systemSpec.getTimeKeyDefinitions()).getFields().stream().map(Schema.Field::name)
                .collect(Collectors.toList()).get(0);


    }

    private static void writeParquet(SparkSession sparkSession, Dataset<Row> withPart, Directory orgDirStats, String destPathStr, int partCount, boolean useEntityBucket, boolean adaptiveRowGroup) {
        //paranoid check to avoid overwriting pro data...
        String timestampColumn = getTimestampColumn(destPathStr);
        if (destPathStr.contains("nxcals_pro")) {
            throw new RuntimeException("Should not have nxcals_pro in destination path for this test!!!");
        }
        long parquetRowGroupSize = PARQUET_BLOCK_SIZE;
        long hdfsBlockSize = DEFAULT_HDFS_BLOCK_SIZE;
        if (adaptiveRowGroup) {
            //aiming at ~200 row groups as default , with min 16MB, max 256MB (HDFS block size),
            //using multiples of 16MB to normalize.

            parquetRowGroupSize = (((orgDirStats.totalSize / 200) / ROW_GROUP_MIN_SIZE) + 1) * ROW_GROUP_MIN_SIZE;

            if (parquetRowGroupSize < ROW_GROUP_MIN_SIZE) {
                parquetRowGroupSize = ROW_GROUP_MIN_SIZE; //lower bound this is probably not needed with the normalization...
            } else if (parquetRowGroupSize > ROW_GROUP_MAX_SIZE) {
                parquetRowGroupSize = ROW_GROUP_MAX_SIZE; //upper bound is needed.
            }
            if (parquetRowGroupSize > hdfsBlockSize) {
                hdfsBlockSize = EXTENDED_HDFS_BLOCK_SIZE;
            }

        }

        hdfsBlockSize = EXTENDED_HDFS_BLOCK_SIZE;
        log.info("For path {} using parquet row group size {} MB, predicted row group count {}, hfds block size {} MB", destPathStr, parquetRowGroupSize / (1024 * 1024), orgDirStats.totalSize / parquetRowGroupSize, hdfsBlockSize / (1024 * 1024));

        sparkSession.sparkContext().hadoopConfiguration().setLong("dfs.block.size", hdfsBlockSize);
        if (useEntityBucket) {
            withPart
                    .repartitionByRange(partCount, withPart.col(PARTITION_COLUMN), withPart.col(ENTITY_BUCKET_COLUMN))
                    .sortWithinPartitions(PARTITION_COLUMN, ENTITY_BUCKET_COLUMN, timestampColumn) //must add sorting by partition, otherwise unsorted order when saving by partition happens
                    //https://stackoverflow.com/questions/52159938/cant-write-ordered-data-to-parquet-in-spark
                    .write()
                    .option("parquet.block.size", parquetRowGroupSize)
                    .option("parquet.dictionary.page.size", 3_145_728)
                    .option("parquet.page.size.row.check.min", 10) //this has no effect...
                    .partitionBy(PARTITION_COLUMN, ENTITY_BUCKET_COLUMN) //
                    .mode(SaveMode.Overwrite)
                    .parquet(destPathStr);
        } else {
            withPart
                    .repartitionByRange(partCount, withPart.col(PARTITION_COLUMN))
//                .repartition(1)
//                .sortWithinPartitions(PARTITION_COLUMN,TIMESTAMP_COLUMN) //must add sorting by partition, otherwise unsorted order when saving by partition happens
                    .sortWithinPartitions(PARTITION_COLUMN, timestampColumn) //must add sorting by partition, otherwise unsorted order when saving by partition happens
                    //https://stackoverflow.com/questions/52159938/cant-write-ordered-data-to-parquet-in-spark
//                .drop(PARTITION_COLUMN)
                    .write()
                    .option("parquet.block.size", parquetRowGroupSize)
                    .option("parquet.dictionary.page.size", 3_145_728)
                    .option("parquet.page.size.row.check.min", 10) //this has no effect...
//                .partitionBy(PARTITION_COLUMN)
                    //.sortBy(TIMESTAMP_COLUMN)
                    .partitionBy(PARTITION_COLUMN) //
                    .mode(SaveMode.Overwrite)
                    .parquet(destPathStr);
        }
    }


//    private static void writeHive(Dataset<Row> withPart, String destPathStr) {
//        withPart
//                .repartitionByRange(96, withPart.col(PARTITION_COLUMN))
////                .repartition(1)
////                .sortWithinPartitions(PARTITION_COLUMN,TIMESTAMP_COLUMN) //must add sorting by partition, otherwise unsorted order when saving by partition happens
//                .sortWithinPartitions(PARTITION_COLUMN, TIMESTAMP_COLUMN) //must add sorting by partition, otherwise unsorted order when saving by partition happens
//                //https://stackoverflow.com/questions/52159938/cant-write-ordered-data-to-parquet-in-spark
//                .drop(PARTITION_COLUMN)
//                .write()
//                .option("parquet.block.size", 64 * 1024 * 1024L)
//                .option("parquet.dictionary.page.size", 3_145_728)
////                .partitionBy(PARTITION_COLUMN)
//                .sortBy(TIMESTAMP_COLUMN)
////                .partitionBy("partition") <-- This seems to make things worse, maybe we are losing the time sorting?
//                .mode(SaveMode.Overwrite);
////                .saveAsTable(getTableName(destPathStr));
//
//
//    }

    @SneakyThrows
    private static void processOutputParitions(String destPathStr) {
        log.info("Processing output path {}", destPathStr);
        Path output = new Path(destPathStr + "/partition=*/*.parquet");
        FileStatus[] fileStatuses = fileSystem.globStatus(output);
        for (FileStatus file : fileStatuses) {

            String parent = file.getPath().getParent().getName();
            String renamed = parent + "__" + file.getPath().getName();
            fileSystem.rename(file.getPath(), new Path(destPathStr + renamed));


        }
    }

    private static List<TimeWindow> getTimewindows(SparkSession sparkSession, String path) {
        String timestampColumn = getTimestampColumn(path);
        Dataset<Row> dataset = sparkSession.read().parquet(path);
        List<Long> samples = dataset.select(timestampColumn).distinct()
                .sample(0.1).limit(10).collectAsList().stream().map(r -> (Long) r.getAs(timestampColumn)).collect(Collectors.toList());

        List<Row> minMax = dataset.agg(functions.min(dataset.col(timestampColumn)).alias("min"), functions.max(dataset.col(timestampColumn)).alias("max")).collectAsList();
        log.info("Min/max timestamps {}", minMax);
        long minTs = minMax.get(0).getLong(0);
        long maxTs = minMax.get(0).getLong(1);

        List<Long> timestamps = new ArrayList<>();
        timestamps.add(minTs + rand.nextInt(1_000_000));
        timestamps.addAll(samples);
        timestamps.add(maxTs - rand.nextInt(1_000_000));


        List<TimeWindow> windows = new ArrayList<>();
        for (int i = 0; i < timestamps.size() - 1; i = i + 2) {
            Long t1 = timestamps.get(i);
            Long t2 = timestamps.get(i + 1);
            if (t1 <= t2) {
                windows.add(TimeWindow.between(t1, t2)); //random window
                windows.add(TimeWindow.between(t1, limitToCurrentDay(t1, generateNanos()))); //short time window (<INTERVAL min)

            } else {
                windows.add(TimeWindow.between(t2, t1));
                windows.add(TimeWindow.between(t2, limitToCurrentDay(t2, generateNanos())));
            }

        }

        return windows;
    }

    private static long limitToCurrentDay(long timestamp, long nanos) {
        long result = timestamp + nanos;
        Instant val = TimeUtils.getInstantFromNanos(result);
        if (val.atZone(ZoneId.of("UTC")).getDayOfMonth() != TimeUtils.getInstantFromNanos(timestamp).atZone(ZoneId.of("UTC")).getDayOfMonth()) {
            return timestamp;
        } else {
            return result;
        }
    }

    private static Long generateNanos() {
        int minutes = rand.nextInt(INTERVAL_IN_MINUTES);
        return minutes * 60_000_000_000L;
    }

    private static List<Long> getEntities(SparkSession sparkSession, String path, int limit) {
        Dataset<Row> dataset = sparkSession.read().parquet(path);

        Dataset<Row> distinct = dataset.select(SystemFields.NXC_ENTITY_ID.getValue()).distinct();

        long total = distinct.count();
        log.info("Total number {} of entities in path {}", total, path);
        List<Long> entityIds =
                distinct.sample(0.1).limit(limit).collectAsList()
                        .stream().map(r -> (Long) r.getAs(SystemFields.NXC_ENTITY_ID.getValue())).collect(Collectors.toList());
        if (entityIds.isEmpty()) {
            //try without sampling
            return distinct.limit(limit)
                    .collectAsList().stream().map(r -> (Long) r.getAs(SystemFields.NXC_ENTITY_ID.getValue())).collect(Collectors.toList());
        } else {
            return entityIds;
        }

//        return Lists.newArrayList(9299294L);
    }


    private static List<String> getInputPathsForTest() {
        return Lists.newArrayList(
                //1GB

                "/project/nxcals/nxcals_pro/data/2/17491/210991/2021/8/15",
                "/project/nxcals/nxcals_pro/data/2/21222/186115/2021/8/15",
                "/project/nxcals/nxcals_pro/data/2/17412/15804/2021/8/15",
                "/project/nxcals/nxcals_pro/data/2/26724/196613/2021/8/15",
                "/project/nxcals/nxcals_pro/data/2/135221/222995/2021/8/15"
                //100MB
//                "/project/nxcals/nxcals_pro/data/2/11661/14032/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/11850/244493/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/5/13394/175271/2021/8/15"

        );

    }

    private static List<String> getInputPaths() {
        return Lists.newArrayList(
//very small files << 10KB
//                "/project/nxcals/nxcals_pro/data/17/29222/201119/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/17/29729/202123/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/5/13502/175271/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/17/29726/201121/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/17/29726/202121/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/11961/13972/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/12260/14499/2021/8/15",
////~100KB files
//                "/project/nxcals/nxcals_pro/data/2/20731/182496/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/5/13406/175271/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/5/13412/175271/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/5/13388/175271/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/5/13362/175271/2021/8/15",
////~900KB files
//                "/project/nxcals/nxcals_pro/data/2/14640/186611/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/15831/15091/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/11751/14054/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/75221/175425/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/137222/337494/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/15833/15091/2021/8/15",
//                //~10MB-20MB files
//                "/project/nxcals/nxcals_pro/data/5/13493/175271/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/15391/177561/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/135230/175346/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/51221/276495/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/11534/13933/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/103221/309492/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/5/13503/175271/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/14074/15804/2021/8/15",
//
//                //100MB files
//                "/project/nxcals/nxcals_pro/data/2/11661/14032/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/11850/244493/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/5/13394/175271/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/15531/13982/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/5/13382/175271/2021/8/15",
////500MB files
//                "/project/nxcals/nxcals_pro/data/2/15865/15514/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/20222/186137/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/20244/186123/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/13629/175483/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/5/13358/175271/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/11513/13922/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/12661/186136/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/106221/14073/2021/8/15", //derived, 0.5GB
//
////                //Big files > 1GB
////
//
//                "/project/nxcals/nxcals_pro/data/2/17491/210991/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/30722/222492/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/12866/268492/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/112222/315498/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/13649/211991/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/17201/177631/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/12839/266495/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/20224/186112/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/14673/186131/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/16861/177429/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/12869/175115/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/21222/186115/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/17412/15804/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/26724/196613/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/135221/222995/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/11863/13943/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/20246/186122/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/33234/213004/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/11768/14123/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/21241/186120/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/103223/335506/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/11860/14189/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/88221/177302/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/21242/186618/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/15372/177302/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/87222/177302/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/20248/186122/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/20250/186120/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/11734/14072/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/15702/177120/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/17313/326502/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/13638/175479/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/15715/177145/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/36721/222995/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/20247/186141/2021/8/15",
//
//                "/project/nxcals/nxcals_pro/data/2/20251/15804/2021/8/15",
//
//                "/project/nxcals/nxcals_pro/data/2/20744/186115/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/20754/186618/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/20245/186616/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/11468/13865/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/11745/186611/2021/8/15", //10GB
//                "/project/nxcals/nxcals_pro/data/2/71221/176908/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/13306/15804/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/130223/332493/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/26728/196616/2021/8/15",
////
////                //very big files >>20 GB
////
//                "/project/nxcals/nxcals_pro/data/2/11905/335501/2021/8/15", //22GB
//                "/project/nxcals/nxcals_pro/data/2/17424/15804/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/15382/175351/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/13438/224491/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/49223/278494/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/11784/14118/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/11936/177661/2021/8/15",
//                "/project/nxcals/nxcals_pro/data/2/11785/14119/2021/8/15",
                "/project/nxcals/nxcals_pro/data/2/90224/298500/2021/8/15",
                "/project/nxcals/nxcals_pro/data/2/11792/14135/2021/8/15",
                "/project/nxcals/nxcals_pro/data/2/11490/177370/2021/8/15"
        );
    }

    @Data
    public static class Query {
        private final Long entity;
        private final TimeWindow timeWindow;
        private final Set<String> fields;
    }

    @Data
    public static class QueryResult {
        private final Query query;
        private final List<Long> queryTimes;
        private final long count;
    }


//    private Dataset<Row> repartitionByTimeInterval(DataProcessingJob job, Dataset<Row> dataset) {
//        String timestampField = getTimestampCache().get(job.getSystemId());
//        Column epochTimestampCol = dataset.col(timestampField)
//                .divide(functions.lit(TimeUtils.CONVERT_EPOCH_NANOS_TO_SECONDS_VALUE))
//                .cast(DataTypes.TimestampType);
//        return dataset.withColumn(PARTITION_BY_HOUR_COLUMN, functions.hour(epochTimestampCol))
//                .repartition(col(PARTITION_BY_HOUR_COLUMN));
//    }
//
//    private Map<String, Dataset<Row>> repartitionByHourMap(DataProcessingJob job, Dataset<Row> dataset) {
//        Dataset<Row> datasetPartitionedByHour = repartitionByHour(job, dataset);
//        //remove redundant repartition column at the end of operation
//        return IntStream.range(0, 24).boxed()
//                .collect(Collectors.toMap(hour -> String.format(HOUR_DIRECTORY_NAME_FORMAT, hour),
//                        hour -> datasetPartitionedByHour.where(col(PARTITION_BY_HOUR_COLUMN)
//                                .equalTo(functions.lit(hour)))
//                                .drop(PARTITION_BY_HOUR_COLUMN)));
//    }
}
