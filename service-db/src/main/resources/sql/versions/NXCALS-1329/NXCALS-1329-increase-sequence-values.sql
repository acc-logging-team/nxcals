update sequence_info
set seq_last_value = 10000
where seq_name in ('schema_seq', 'default_seq', 'entity_seq', 'entity_hist_seq', 'partition_seq', 'variable_seq')
     and seq_last_value < 10000;