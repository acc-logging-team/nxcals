create or replace view v_hierarchy_vars_changelog(id, old_variable_id, new_variable_id, old_hierarchy_id,
new_hierarchy_id, old_system_id, new_system_id, op_type, create_time_utc, client_info, transaction_id, module, action) as
select gvc.id, gvc.old_variable_id, gvc.new_variable_id, h1.hierarchy_id, h2.hierarchy_id, h1.system_id, h2.system_id,
gvc.op_type, gvc.create_time_utc, gvc.client_info, gvc.transaction_id, gvc.module, gvc.action
from group_variables_changelog gvc
left join hierarchies h1 on h1.group_id = gvc.old_group_id
left join hierarchies h2 on h2.group_id = gvc.new_group_id
where gvc.new_association_name = '__in__hierarchy__' or gvc.old_association_name = '__in__hierarchy__';