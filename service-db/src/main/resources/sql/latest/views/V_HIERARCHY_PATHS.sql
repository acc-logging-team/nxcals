create or replace force view v_hierarchy_paths(hierarchy_id, node_path, leaf, "LEVEL_NO") as
select hierarchy_id, sys_connect_by_path(hierarchy_name, '/') node_path, connect_by_isleaf "LEAF", level "LEVEL_NO"
from hierarchies
start with nvl(parent_id, -1) = -1
connect by prior hierarchy_id = parent_id;
