alter table group_variables add constraint gv_g_fk foreign key (group_id)
	  references groups (group_id) on delete cascade enable;
alter table group_variables add constraint gv_v_fk foreign key (variable_id)
	  references variables (variable_id) on delete cascade enable;
