--------------------------------------------------------
--  Ref Constraints for Table PARTITIONS
--------------------------------------------------------

  ALTER TABLE "PARTITIONS" ADD CONSTRAINT "PARTITIONS_SYS_FK" FOREIGN KEY ("SYSTEM_ID")
	  REFERENCES "SYSTEMS" ("SYSTEM_ID") ENABLE;
