alter table partition_resources add constraint system_id_fk foreign key (system_id)
    references systems (SYSTEM_ID) on delete cascade enable;
alter table partition_resources add constraint partition_id_fk foreign key (partition_id)
    references partitions (PARTITION_ID) on delete cascade enable;
alter table partition_resources add constraint schema_id_fk foreign key (schema_id)
    references schemas (SCHEMA_ID) on delete cascade enable;