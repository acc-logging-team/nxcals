alter table hierarchy_variables add constraint hv_h_fk foreign key (hierarchy_id)
	  references hierarchies (hierarchy_id) on delete cascade enable;
alter table hierarchy_variables add constraint hv_v_fk foreign key (variable_id)
	  references variables (variable_id) on delete cascade enable;
