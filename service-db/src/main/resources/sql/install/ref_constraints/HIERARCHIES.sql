alter table hierarchies add constraint h_mh_fk foreign key (parent_id)
	  references hierarchies (hierarchy_id) on delete cascade enable;
