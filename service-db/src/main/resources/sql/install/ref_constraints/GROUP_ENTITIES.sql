alter table group_entities add constraint ge_g_fk foreign key (group_id)
	  references groups (group_id) on delete cascade enable;
alter table group_entities add constraint ge_e_fk foreign key (entity_id)
	  references entities (entity_id) on delete cascade enable;
