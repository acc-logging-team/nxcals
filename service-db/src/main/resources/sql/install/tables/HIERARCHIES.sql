create table hierarchies(
  hierarchy_id number,
	hierarchy_name varchar2(256),
	parent_id number,
	description varchar2(255),
	rec_version number default 0
);
