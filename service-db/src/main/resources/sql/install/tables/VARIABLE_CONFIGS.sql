--------------------------------------------------------
--  DDL for Table VARIABLE_CONFIGS
--------------------------------------------------------

CREATE TABLE VARIABLE_CONFIGS(
  VARIABLE_CONFIG_ID NUMBER,
  VARIABLE_ID NUMBER,
  ENTITY_ID NUMBER,
  FIELD_NAME VARCHAR2(500),
  VALID_FROM_STAMP TIMESTAMP(9),
  VALID_TO_STAMP TIMESTAMP(9),
  REC_VERSION NUMBER DEFAULT 0
);
