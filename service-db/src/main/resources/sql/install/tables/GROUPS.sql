create table groups (
    group_id number,
    name varchar2(256),
    system_id number,
    rec_version number default 0
);
