create table group_properties_changelog (
    id number,
    new_group_id number,
    old_group_id number,
    new_name varchar(256),
    old_name varchar(256),
    new_value varchar(256),
    old_value varchar(256),
    op_type char(1),
    create_time_utc timestamp (9),
    transaction_id varchar2(48),
    module varchar2(48),
    action varchar2(32),
    client_info varchar2(64)
)
partition by range (create_time_utc) interval(interval '1' month) (
    partition pos_data_initial values less than (timestamp '2019-12-01 00:00:00')
);