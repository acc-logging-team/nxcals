--------------------------------------------------------
--  DDL for Table ENTITY_KEYS_HIST
--------------------------------------------------------

  CREATE TABLE ENTITIES_HIST 
   (	
    ENTITY_HIST_ID NUMBER,
    ENTITY_ID NUMBER, 
	SCHEMA_ID NUMBER, 
	PARTITION_ID NUMBER, 
    SYSTEM_ID NUMBER, 
    VALID_FROM_STAMP TIMESTAMP (9),
	VALID_TO_STAMP TIMESTAMP (9), 
	REC_VERSION NUMBER DEFAULT 0
   );
