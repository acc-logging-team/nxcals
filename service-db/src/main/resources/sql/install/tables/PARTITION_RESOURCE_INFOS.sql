create table partition_resource_infos (
    partition_resource_info_id number,
    partition_resource_id number,
    partition_resource_information varchar2(1024),
    compaction_type varchar2(32),
    storage_type varchar2(32),
    is_fixed char(1),
    valid_from_stamp timestamp(9),
    valid_to_stamp timestamp(9),
    rec_version number
)