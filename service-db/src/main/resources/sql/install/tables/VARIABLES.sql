--------------------------------------------------------
--  DDL for Table VARIABLES
--------------------------------------------------------

CREATE TABLE VARIABLES(
  VARIABLE_ID NUMBER,
  VARIABLE_NAME VARCHAR2(200),
  DESCRIPTION VARCHAR2(1000),
  CREATION_TIME_UTC TIMESTAMP(6),
  REC_VERSION NUMBER DEFAULT 0
);