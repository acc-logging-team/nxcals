--------------------------------------------------------
--  DDL for Table ENTITY_KEYS
--------------------------------------------------------

  CREATE TABLE ENTITIES 
   (	
    ENTITY_ID NUMBER, 
	KEY_VALUES VARCHAR2(4000 BYTE), 
	SCHEMA_ID NUMBER, 
	PARTITION_ID NUMBER, 
	SYSTEM_ID NUMBER, 
	REC_VERSION NUMBER DEFAULT 0
   ) ;
