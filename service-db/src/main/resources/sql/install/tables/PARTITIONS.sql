--------------------------------------------------------
--  DDL for Table PARTITION_KEYS
--------------------------------------------------------

  CREATE TABLE PARTITIONS 
   (	
    PARTITION_ID NUMBER, 
	KEY_VALUES VARCHAR2(4000 BYTE), 
	SYSTEM_ID NUMBER
   ) ;
