--------------------------------------------------------
--  DDL for Table SEQUENCE_INFO
--------------------------------------------------------

  CREATE TABLE SEQUENCE_INFO 
   (	SEQ_NAME VARCHAR2(100 BYTE), 
	SEQ_LAST_VALUE NUMBER, 
	SEQ_REC_VERSION NUMBER DEFAULT 0
   ) ;
