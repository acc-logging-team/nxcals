alter table partition_resources add constraint partition_resources_pk primary key (partition_resource_id) enable;
alter table partition_resources modify (system_id constraint sys_id_nn not null enable);
alter table partition_resources modify (partition_id constraint part_id_nn not null enable);
alter table partition_resources modify (schema_id constraint schema_id_nn not null enable);