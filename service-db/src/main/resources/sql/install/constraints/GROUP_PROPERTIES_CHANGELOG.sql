alter table group_properties_changelog add constraint gp_changelog_pk primary key (id);
alter table group_properties_changelog add constraint gp_op_type_chk check (op_type in ('D', 'I', 'U'));