--------------------------------------------------------
--  Constraints for Table ENTITIES_CHANGELOG
--------------------------------------------------------
ALTER TABLE ENTITIES_CHANGELOG ADD CONSTRAINT ENTITIES_CHANGELOG_PK PRIMARY KEY (ID);
ALTER TABLE ENTITIES_CHANGELOG ADD CONSTRAINT ENTITIES_OP_TYPE_CHK CHECK (OP_TYPE IN ('D', 'I', 'U'));