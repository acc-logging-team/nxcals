alter table hierarchies add constraint h_pk primary key (hierarchy_id) enable;
alter table hierarchies modify (hierarchy_name constraint h_hierarchy_name_nn not null enable);
alter table hierarchies add constraint h_parent_check check (parent_id is not null or hierarchy_id = 0) enable;
