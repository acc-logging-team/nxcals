--------------------------------------------------------
--  Constraints for Table ENTITIES_HIST_CHANGELOG
--------------------------------------------------------
ALTER TABLE ENTITIES_HIST_CHANGELOG ADD CONSTRAINT E_HIST_CHANGELOG_PK PRIMARY KEY (ID);
ALTER TABLE ENTITIES_HIST_CHANGELOG ADD CONSTRAINT E_HIST_OP_TYPE_CHK CHECK (OP_TYPE IN ('D', 'I', 'U'));