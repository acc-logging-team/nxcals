--------------------------------------------------------
--  Constraints for Table HIERARCHIES_CHANGELOG
--------------------------------------------------------
ALTER TABLE HIERARCHIES_CHANGELOG ADD CONSTRAINT HIERARCHIES_CHANGELOG_PK PRIMARY KEY (ID);
ALTER TABLE HIERARCHIES_CHANGELOG ADD CONSTRAINT HIERARCHIES_OP_TYPE_CHK CHECK (OP_TYPE IN ('D', 'I', 'U'));