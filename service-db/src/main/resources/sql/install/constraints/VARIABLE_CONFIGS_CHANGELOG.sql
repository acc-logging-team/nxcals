--------------------------------------------------------
--  Constraints for Table VARIABLE_CONFIGS_CHANGELOG
--------------------------------------------------------
ALTER TABLE VARIABLE_CONFIGS_CHANGELOG ADD CONSTRAINT VAR_CONFIGS_CHANGELOG_PK PRIMARY KEY (ID);
ALTER TABLE VARIABLE_CONFIGS_CHANGELOG ADD CONSTRAINT VAR_CONFIGS_OP_TYPE_CHK CHECK (OP_TYPE IN ('D', 'I', 'U'));
