alter table group_entities_changelog add constraint ge_changelog_pk primary key (id);
alter table group_entities_changelog add constraint ge_op_type_chk check (op_type in ('D', 'I', 'U'));