alter table group_variables_changelog add constraint gv_changelog_pk primary key (id);
alter table group_variables_changelog add constraint gv_op_type_chk check (op_type in ('D', 'I', 'U'));