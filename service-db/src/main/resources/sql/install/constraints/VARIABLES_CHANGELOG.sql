--------------------------------------------------------
--  Constraints for Table VARIABLES_CHANGELOG
--------------------------------------------------------
ALTER TABLE VARIABLES_CHANGELOG ADD CONSTRAINT VARIABLES_CHANGELOG_PK PRIMARY KEY (ID);
ALTER TABLE VARIABLES_CHANGELOG ADD CONSTRAINT VARIABLES_OP_TYPE_CHK CHECK (OP_TYPE IN ('D', 'I', 'U'));