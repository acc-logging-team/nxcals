alter table groups_changelog add constraint g_changelog_pk primary key (id);
alter table groups_changelog add constraint g_op_type_chk check (op_type in ('D', 'I', 'U'));