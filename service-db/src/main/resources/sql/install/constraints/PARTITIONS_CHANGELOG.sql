--------------------------------------------------------
--  Constraints for Table PARTITIONS_CHANGELOG
--------------------------------------------------------
ALTER TABLE PARTITIONS_CHANGELOG ADD CONSTRAINT PARTITIONS_CHANGELOG_PK PRIMARY KEY (ID);
ALTER TABLE PARTITIONS_CHANGELOG ADD CONSTRAINT PARTITIONS_OP_TYPE_CHK CHECK (OP_TYPE IN ('D', 'I', 'U'));