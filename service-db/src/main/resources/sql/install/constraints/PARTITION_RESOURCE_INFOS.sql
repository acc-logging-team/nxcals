alter table partition_resource_infos add constraint partition_resource_infos_pk primary key (partition_resource_info_id);
alter table partition_resource_infos modify (valid_from_stamp constraint valid_from_nn not null);
alter table partition_resource_infos modify (compaction_type constraint compaction_type_nn not null);
alter table partition_resource_infos modify (storage_type constraint storage_type_nn not null);
alter table partition_resource_infos modify (is_fixed constraint is_fixed_nn not null);
alter table partition_resource_infos  add constraint pri_check_type_is_fixed  check (is_fixed  in ('0', '1'));