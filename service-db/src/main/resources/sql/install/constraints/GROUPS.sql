alter table groups add constraint group_pk primary key (group_id) enable;
alter table groups modify (name constraint group_name_nn not null enable);
alter table groups modify (system_id constraint group_system_id_nn not null enable);
alter table groups modify (rec_version constraint group_rec_version_nn not null enable);