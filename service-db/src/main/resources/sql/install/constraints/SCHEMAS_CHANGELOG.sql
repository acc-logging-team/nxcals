alter table schemas_changelog add constraint schemas_changelog_pk primary key (id);
alter table schemas_changelog add constraint schemas_op_type_chk check (op_type in ('D', 'I', 'U'));