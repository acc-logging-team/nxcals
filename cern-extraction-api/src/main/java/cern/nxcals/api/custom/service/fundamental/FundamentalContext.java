package cern.nxcals.api.custom.service.fundamental;

import cern.nxcals.api.custom.domain.FundamentalFilter;
import cern.nxcals.api.custom.domain.util.QueryDatasetProvider;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import lombok.NonNull;
import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.Validate;
import org.apache.spark.sql.SparkSession;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@UtilityClass
public final class FundamentalContext {
    public static final String SYSTEM = "CMW";
    public static final String VIRTUAL_LSA_CYCLE_FIELD = "LSA_CYCLE";
    public static final String USER_FIELD = "USER";
    public static final String DESTINATION_FIELD = "DEST";

    private static final String TGM_LSA_CYCLE_FIELD = "__LSA_CYCLE__";
    private static final String XTIM_LSA_CYCLE_FIELD = "lsaCycleName";
    private static final String XTIM_DESTINATION_FIELD = "DYN_DEST";

    public static final String VARIABLE_NAME_SUFFIX = ":NXCALS_FUNDAMENTAL";

    private static final Map<String, List<String>> DATA_QUERY_FIELD_ALIASES = ImmutableMap.of(
            VIRTUAL_LSA_CYCLE_FIELD, ImmutableList.of(TGM_LSA_CYCLE_FIELD, XTIM_LSA_CYCLE_FIELD),
            DESTINATION_FIELD, ImmutableList.of(DESTINATION_FIELD, XTIM_DESTINATION_FIELD)
    );

    static final QueryDatasetProvider<Set<String>> VARIABLE_LIST_DATASET_PROVIDER = (SparkSession sparkSession,
            TimeWindow timeWindow, Set<String> fundamentalVariables) ->
            DataQuery.builder(sparkSession).byVariables()
                    .system(FundamentalContext.SYSTEM)
                    .startTime(timeWindow.getStartTime()).endTime(timeWindow.getEndTime())
                    .fieldAliases(FundamentalContext.DATA_QUERY_FIELD_ALIASES)
                    .variables(new ArrayList<>(fundamentalVariables))
                    .build();

    static String getVariableNameFrom(@NonNull FundamentalFilter fundamentalFilter) {
        return getVariableNameFrom(fundamentalFilter.getAccelerator());
    }

    static String getVariableNameFrom(@NonNull String acceleratorName) {
        Validate.notBlank(acceleratorName);
        return acceleratorName.toUpperCase() + VARIABLE_NAME_SUFFIX;
    }

}
