package cern.nxcals.api.custom.service.fill;

import cern.nxcals.api.custom.domain.BeamMode;
import cern.nxcals.api.custom.domain.Fill;
import cern.nxcals.api.custom.service.FillService;
import cern.nxcals.api.domain.TimeWindow;
import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections.MapUtils;

import javax.annotation.concurrent.ThreadSafe;
import java.time.Duration;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Optional;
import java.util.stream.Collectors;

import static cern.nxcals.api.custom.service.fill.FillProvider.FIRST_FILL;
import static cern.nxcals.api.custom.service.fill.FillUtils.TIME_WINDOW_COMPARATOR;
import static cern.nxcals.api.custom.service.fill.FillUtils.navigableMapFrom;

@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
@ThreadSafe
class FillServiceImpl implements FillService {
    private static final int FILL_SEARCH_STEP_DAYS = 150;

    private final FillSearch search = new FillSearch();
    private final FillProcessor processor = new FillProcessor();

    @NonNull
    private final FillProvider fillProvider;

    @Override
    public Optional<Fill> findFill(int number) {
        Instant curr = Instant.now();
        Duration step = Duration.ofDays(FILL_SEARCH_STEP_DAYS);
        NavigableMap<TimeWindow, Integer> fillsByTime = Collections.emptyNavigableMap();
        while (curr.isAfter(FIRST_FILL) && !fillRangeContains(fillsByTime, number)) {
            curr = curr.minus(step);
            fillsByTime = fetchFills(curr, curr.plus(step));
            step = step.multipliedBy(2);
        }
        return search.getFillForNumber(fillsByTime, number).map(tw -> toFill(tw, number));
    }

    @Override
    public Optional<Fill> getLastCompleted() {
        Instant curr = Instant.now();
        Duration step = Duration.ofDays(FILL_SEARCH_STEP_DAYS);
        NavigableMap<TimeWindow, Integer> foundFills = Collections.emptyNavigableMap();
        while (curr.isAfter(FIRST_FILL) && foundFills.isEmpty()) {
            curr = curr.minus(step);
            foundFills = fetchFills(curr, curr.plus(step));
            step = step.multipliedBy(2);
        }
        return foundFills.entrySet().stream().filter(entry->!entry.getKey().isRightInfinite()).
                max((l, r) -> TIME_WINDOW_COMPARATOR.compare(l.getKey(), r.getKey()))
                .map(e -> toFill(e.getKey(), e.getValue()));
    }

    @Override
    public List<Fill> findFills(Instant startTime, Instant endTime) {
        return toFills(fetchFills(startTime, endTime));
    }

    private boolean fillRangeContains(NavigableMap<TimeWindow, Integer> fills, int number) {
        return !fills.isEmpty() && fills.firstEntry().getValue() <= number && fills.lastEntry().getValue() >= number;
    }

    private Fill toFill(TimeWindow range, int number) {
        return toFills(navigableMapFrom(Collections.singletonMap(range, number))).iterator().next();
    }

    private List<Fill> toFills(NavigableMap<TimeWindow, Integer> fillsByRange) {
        if (fillsByRange.isEmpty()) {
            return Collections.emptyList();
        }
        Map<TimeWindow, NavigableMap<TimeWindow, String>> modesForTimeWindows = fillProvider
                .findBeamModesByTimeWindows(fillsByRange.keySet());
        return fillsByRange.entrySet().stream()
                .map(e -> new Fill(e.getValue(), e.getKey(), toBeamModes(modesForTimeWindows.get(e.getKey()))))
                .collect(Collectors.toList());
    }

    private List<BeamMode> toBeamModes(Map<TimeWindow, String> beamModesByRange) {
        if (MapUtils.isEmpty(beamModesByRange)) {
            return Collections.emptyList();
        }
        return beamModesByRange.entrySet().stream()
                .map(bm -> new BeamMode(bm.getKey(), bm.getValue())).collect(Collectors.toList());
    }

    /**
     * Fetch fills in raw format and process them according to {@link FillProcessor} logic.
     * This method call utilizes the internal caching functionality as available on {@link FillProvider}.
     * Meaning that subsequent calls are expected to be faster after the initial loading of raw data (depending on
     * the provided search time window)
     * NOTE: Favor this method and it's result, if your intention is to operate on top of processed fill data
     *
     * @param startTime the search start time
     * @param endTime   the search end time
     * @return a {@link NavigableMap}, representing the processed view of available fill numbers to their
     * respective time windows
     * @see {@link FillProcessor#process} for details on the applied processing logic
     */
    private NavigableMap<TimeWindow, Integer> fetchFills(Instant startTime, Instant endTime) {
        return processor.process(fillProvider.findFillsByRange(startTime, endTime));
    }

}
