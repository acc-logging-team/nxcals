package cern.nxcals.api.custom.service.aggregation;

import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.utils.TimeUtils;
import lombok.NonNull;
import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.expressions.Window;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static cern.nxcals.api.custom.service.aggregation.AggregationConstants.INTERVAL_GROUP_BY_FIELD;
import static cern.nxcals.api.custom.service.aggregation.AggregationConstants.INTERVAL_RANGE_FIELDS;
import static org.apache.spark.sql.functions.coalesce;
import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.lead;
import static org.apache.spark.sql.functions.lit;
import static org.apache.spark.sql.types.DataTypes.LongType;
import static org.apache.spark.sql.types.DataTypes.createStructField;
import static org.apache.spark.sql.types.DataTypes.createStructType;

@UtilityClass
final class AggregationUtils {
    private static final StructType INTERVALS_SCHEMA = createStructType(new StructField[] {
            createStructField(INTERVAL_RANGE_FIELDS.getLeft(), LongType, false),
            createStructField(INTERVAL_RANGE_FIELDS.getRight(), LongType, false)
    });

    static Dataset<Row> extractIntervals(@NonNull SparkSession sparkSession,
            @NonNull DatasetAggregationProperties properties,
            @NonNull Pair<Instant, Instant> rangeLimits) {
        final Instant start = properties.getTimeWindow().getStartTime();
        final Instant end = properties.getTimeWindow().getEndTime();
        List<Long[]> edgeIntervals = new ArrayList<>();
        edgeIntervals.add(buildIntervalFrom(rangeLimits.getLeft(), start.minusNanos(1)));
        edgeIntervals.add(buildIntervalFrom(end.plusNanos(1), rangeLimits.getRight()));
        Dataset<Row> edgeIntervalsDataset = toDataset(edgeIntervals, INTERVALS_SCHEMA, sparkSession)
                .where(col(INTERVAL_RANGE_FIELDS.getLeft())
                        .leq(col(INTERVAL_RANGE_FIELDS.getRight()))); //normalize rows

        String timestampField = properties.getTimestampField();
        Dataset<Row> intervalsDataset = properties.getDrivingDataset()
                .withColumn(INTERVAL_RANGE_FIELDS.getLeft(), col(timestampField))
                .withColumn(INTERVAL_RANGE_FIELDS.getRight(), coalesce(lead(timestampField, 1).over(Window
                        .orderBy(timestampField)).minus(lit(1)), col(timestampField)))
                .select(INTERVAL_RANGE_FIELDS.getLeft(), INTERVAL_RANGE_FIELDS.getRight());

        return edgeIntervalsDataset.union(intervalsDataset)
                .withColumn(INTERVAL_GROUP_BY_FIELD, col(INTERVAL_RANGE_FIELDS.getLeft()));
    }

    static Dataset<Row> generateFixedIntervals(@NonNull SparkSession sparkSession,
            @NonNull WindowAggregationProperties properties, @NonNull Pair<Instant, Instant> rangeLimits) {
        final Instant start = properties.getTimeWindow().getStartTime();
        final Instant end = properties.getTimeWindow().getEndTime();
        final long intervalMillis = adjustInterval(properties);

        List<Long[]> intervals = new ArrayList<>();
        intervals.addAll(generateIntervals(rangeLimits.getLeft(), start.minusNanos(1), intervalMillis));
        intervals.addAll(generateIntervals(start, end, intervalMillis));
        intervals.addAll(generateIntervals(end.plusNanos(1), rangeLimits.getRight(), intervalMillis));
        return toDataset(intervals, INTERVALS_SCHEMA, sparkSession).withColumn(INTERVAL_GROUP_BY_FIELD,
                col(INTERVAL_RANGE_FIELDS.getLeft()));
    }

    private static List<Long[]> generateIntervals(Instant start, Instant end, long intervalMillis) {
        List<Long[]> intervals = new ArrayList<>();
        long endNanos = TimeUtils.getNanosFromInstant(end);
        Instant t = start;
        while (t.isBefore(end)) {
            Instant nextIntervalStart = t.plusMillis(intervalMillis);
            long intervalEndNanos = nextIntervalStart.equals(end) ? endNanos :
                    Math.min(TimeUtils.getNanosFromInstant(nextIntervalStart) - 1, endNanos);
            intervals.add(buildIntervalFrom(TimeUtils.getNanosFromInstant(t), intervalEndNanos));
            t = nextIntervalStart;
        }
        return intervals;
    }

    private static Long[] buildIntervalFrom(@NonNull Instant start, @NonNull Instant stop) {
        return buildIntervalFrom(TimeUtils.getNanosFromInstant(start), TimeUtils.getNanosFromInstant(stop));
    }

    private static Long[] buildIntervalFrom(long start, long stop) {
        return new Long[] { start, stop };
    }

    private static Dataset<Row> toDataset(Collection<Long[]> rows, StructType schema, SparkSession sparkSession) {
        List<Row> dsRows = rows.parallelStream().map(RowFactory::create).collect(Collectors.toList());
        return sparkSession.createDataFrame(dsRows, schema);
    }

    private static long adjustInterval(@NonNull WindowAggregationProperties properties) {
        TimeWindow timeWindow = properties.getTimeWindow();
        long duration = Duration.between(timeWindow.getStartTime(), timeWindow.getEndTime()).toMillis();
        return Math.min(duration, properties.getIntervalMillis());
    }

    /**
     * Creates an instance of {@link Duration}, based on the provided {@code amount} and time {@code unit} for data aggregation actions
     *
     * @param amount the amount of the interval duration, measured in terms of the unit.
     *               Parameter should be a positive value and would be validated against that expectation
     * @param unit   the unit that the duration is measured in, must have an exact duration, not null.
     *               Parameter should point to a unit greater or equal to millisecond and would be validated against that expectation
     * @return a {@code Duration}, as constructed by the provided parameters
     */
    static Duration toDuration(long amount, @NonNull TemporalUnit unit) {
        Duration duration = unit.getDuration().multipliedBy(amount);
        Validate.isTrue(!duration.isNegative(), "Duration must not be negative!");
        if (amount != 0) {
            Validate.isTrue(duration.toMillis() > 0,
                    "Duration for aggregation actions should not be less than a millisecond!");
        }
        return duration;
    }

    static Instant getFirstTimestampOrDefault(@NonNull Dataset<Row> dataset, @NonNull String timestampField,
            @NonNull Instant defaultInstant) {
        return dataset.limit(1).collectAsList().stream().findFirst()
                .map(r -> r.getLong(r.fieldIndex(timestampField)))
                .map(TimeUtils::getInstantFromNanos)
                .orElse(defaultInstant);
    }

}
