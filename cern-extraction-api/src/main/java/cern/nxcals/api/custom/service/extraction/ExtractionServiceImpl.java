package cern.nxcals.api.custom.service.extraction;

import cern.nxcals.api.custom.domain.util.QueryDatasetProvider;
import cern.nxcals.api.custom.service.ExtractionService;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.extraction.data.ExtractionUtils;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.function.Function;
import java.util.function.Supplier;

import static cern.nxcals.common.SystemFields.NXC_EXTR_TIMESTAMP;

@RequiredArgsConstructor
final class ExtractionServiceImpl implements ExtractionService {
    @NonNull
    private final Supplier<SparkSession> sessionSupplier;
    @NonNull
    private final QueryDatasetProvider<Variable> variableDataProvider;
    @NonNull
    private final QueryDatasetProvider<Entity> entityDataProvider;

    @Override
    public Dataset<Row> getData(@NonNull Variable variable, @NonNull ExtractionProperties properties) {
        return getDataWithLookup(properties, NXC_EXTR_TIMESTAMP.getValue(),
                tw -> variableDataProvider.get(sessionSupplier.get(), tw, variable));
    }

    @Override
    public Dataset<Row> getData(@NonNull Entity entity, @NonNull ExtractionProperties properties) {
        String timestampFieldName = ExtractionUtils.getTimestampFieldName(entity.getSystemSpec());
        return getDataWithLookup(properties, timestampFieldName,
                tw -> entityDataProvider.get(sessionSupplier.get(), tw, entity));
    }

    private Dataset<Row> getDataWithLookup(@NonNull ExtractionProperties properties, @NonNull String timestampField,
            @NonNull Function<TimeWindow, Dataset<Row>> dataProvider) {
        return properties.getLookupStrategy().apply(dataProvider, timestampField, properties);
    }

}
