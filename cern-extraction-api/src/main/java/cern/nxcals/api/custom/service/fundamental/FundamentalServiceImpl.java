package cern.nxcals.api.custom.service.fundamental;

import cern.accsoft.commons.util.CollectionUtils;
import cern.nxcals.api.custom.domain.FundamentalFilter;
import cern.nxcals.api.custom.domain.util.QueryDatasetProvider;
import cern.nxcals.api.custom.service.FundamentalService;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.Validate;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;

import java.util.Collection;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static cern.nxcals.common.SystemFields.NXC_EXTR_TIMESTAMP;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VARIABLE_NAME;
import static org.apache.spark.sql.functions.col;

@RequiredArgsConstructor
final class FundamentalServiceImpl implements FundamentalService {
    private static final Column FALLBACK_COLUMN_CONDITION = col(NXC_EXTR_TIMESTAMP.getValue()).isNotNull();

    @NonNull
    private final Supplier<SparkSession> sessionSupplier;
    @NonNull
    private final QueryDatasetProvider<Set<String>> variablesDataProvider;
    @NonNull
    private final VariableService variableService;

    @Override
    public Dataset<Row> getAll(@NonNull TimeWindow timeWindow, @NonNull Set<FundamentalFilter> fundamentalFilters) {
        fundamentalFilters.forEach(this::validateFilter);
        Set<Variable> variables = findVariablesFrom(fundamentalFilters);
        Set<String> variableNames = variables.stream().map(Variable::getVariableName).collect(Collectors.toSet());
        Dataset<Row> fundamentalDataset = variablesDataProvider.get(sessionSupplier.get(), timeWindow, variableNames);
        return fundamentalDataset.where(buildJoinedColumnConditionFrom(fundamentalFilters));
    }

    @Override
    public Dataset<Row> getFor(@NonNull TimeWindow timeWindow, @NonNull Collection<String> machines) {
        if (machines.isEmpty()) {
            throw new IllegalArgumentException("Accelerator collection can't be empty");
        }
        Set<String> variableNames = machines.stream().map(FundamentalContext::getVariableNameFrom)
                .collect(Collectors.toSet());
        Dataset<Row> result = variablesDataProvider.get(sessionSupplier.get(), timeWindow, variableNames);
        return functions.broadcast(result);
    }

    private Set<Variable> findVariablesFrom(Set<FundamentalFilter> fundamentalFilters) {
        Set<Variable> variables = variableService.findAll(buildVariableSearchConditionFrom(fundamentalFilters));
        if (CollectionUtils.isEmpty(variables)) {
            throw new IllegalArgumentException("No fundamental variables found! Cannot apply filtering action");
        }
        return variables;
    }

    private Condition<Variables> buildVariableSearchConditionFrom(Set<FundamentalFilter> fundamentalFilters) {
        return fundamentalFilters.stream().map(FundamentalContext::getVariableNameFrom).distinct()
                .map(variableName -> Variables.suchThat().variableName().like(variableName))
                .collect(Collectors.collectingAndThen(Collectors.toList(),
                        conditions -> Variables.suchThat().or(conditions)));
    }

    private Column buildJoinedColumnConditionFrom(@NonNull Set<FundamentalFilter> fundamentalFilters) {
        return fundamentalFilters.stream().map(this::createColumnConditionFrom).reduce(Column::or)
                .orElse(FALLBACK_COLUMN_CONDITION);
    }

    private Column createColumnConditionFrom(@NonNull FundamentalFilter filter) {
        Column condition = col(NXC_EXTR_VARIABLE_NAME.getValue()).like(FundamentalContext.getVariableNameFrom(filter));
        if (filter.getLsaCycle() != null) {
            condition = condition.and(col(FundamentalContext.VIRTUAL_LSA_CYCLE_FIELD).like(filter.getLsaCycle()));
        }
        if (filter.getTimingUser() != null) {
            condition = condition.and(col(FundamentalContext.USER_FIELD).like(filter.getTimingUser()));
        }
        if (filter.getDestination() != null) {
            condition = condition.and(col(FundamentalContext.DESTINATION_FIELD).like(filter.getDestination()));
        }
        return condition;
    }

    /**
     * Defines context validation specific logic for {@link FundamentalFilter} instances
     *
     * @param filter the fundamental filter instance to be validated
     */
    private void validateFilter(@NonNull FundamentalFilter filter) {
        Validate.notBlank(filter.getAccelerator());
    }

}
