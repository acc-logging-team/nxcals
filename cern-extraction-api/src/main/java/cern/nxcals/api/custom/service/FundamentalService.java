package cern.nxcals.api.custom.service;

import cern.nxcals.api.custom.domain.FundamentalFilter;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.common.annotation.Experimental;
import com.google.common.collect.Lists;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface FundamentalService {

    /**
     * Return a {@link Dataset} for a given {@link TimeWindow time window} that contains data
     * for fundamental variables that are matching the provided set of {@link FundamentalFilter fundamental filters}.
     * Please, note that variables are obtained from each filter separately and then joined/unioned together.
     * I.e. variable will be included if it satisfies any of the provided filters.
     *
     * @param timeWindow         a {@link TimeWindow} that defines time bounds for the data search
     * @param fundamentalFilters a set of {@link FundamentalFilter} based on which fundamental variables are selected.
     * @return a {@link Dataset} with data for fundamental variables based on the fundamental filters
     */
    Dataset<Row> getAll(TimeWindow timeWindow, Set<FundamentalFilter> fundamentalFilters);

    /**
     * Return a {@link Dataset} for a given {@link TimeWindow time window} that contains data
     * of fundamental variables for specified {@link String machine}.
     *
     * @param timeWindow  a {@link TimeWindow} that defines time bounds for the data search
     * @param accelerator a {@link String} with machine name.
     * @return a {@link Dataset} with data for fundamental variable for given machine
     */
    @Experimental
    default Dataset<Row> getFor(TimeWindow timeWindow, String accelerator) {
        return getFor(timeWindow, List.of(accelerator));
    }

    /**
     * Return a {@link Dataset} for a given {@link TimeWindow time window} that contains data
     * of fundamental variables for specified {@link Collection machines}.
     *
     * @param timeWindow   a {@link TimeWindow} that defines time bounds for the data search
     * @param accelerators a {@link Collection} with machine names.
     * @return a {@link Dataset} with data for fundamental variable for given machine
     */
    @Experimental
    Dataset<Row> getFor(TimeWindow timeWindow, Collection<String> accelerators);

    /**
     * Return a {@link Dataset} for a given {@link TimeWindow time window} that contains data
     * of fundamental variables for specified {@link Collection machines}.
     *
     * @param timeWindow   a {@link TimeWindow} that defines time bounds for the data search
     * @param accelerators a {@link Collection} with machine names.
     * @return a {@link Dataset} with data for fundamental variable for given machine
     */
    @Experimental
    default Dataset<Row> getFor(TimeWindow timeWindow, String accelerator, String... accelerators) {
        List<String> acceleratorList = Lists.asList(accelerator, accelerators);
        return getFor(timeWindow, acceleratorList);
    }
}
