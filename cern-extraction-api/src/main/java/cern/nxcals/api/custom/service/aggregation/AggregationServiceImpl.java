package cern.nxcals.api.custom.service.aggregation;

import cern.nxcals.api.custom.domain.util.DataLookupUtils;
import cern.nxcals.api.custom.domain.util.QueryDatasetProvider;
import cern.nxcals.api.custom.service.AggregationService;
import cern.nxcals.api.custom.service.FundamentalService;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.extraction.data.ExtractionUtils;
import cern.nxcals.common.spark.MoreFunctions;
import com.google.common.collect.Sets;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.DataTypes;
import scala.reflect.ClassTag;

import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;

import static cern.nxcals.api.custom.service.aggregation.AggregationConstants.AGGREGATION_SOURCE_TIMESTAMP_FIELD;
import static cern.nxcals.api.custom.service.aggregation.AggregationConstants.AGGREGATION_SOURCE_VALUE_FIELD;
import static cern.nxcals.api.custom.service.aggregation.AggregationConstants.AGGREGATION_TIMESTAMP_FIELD;
import static cern.nxcals.api.custom.service.aggregation.AggregationConstants.AGGREGATION_VALUE_FIELD;
import static cern.nxcals.api.custom.service.aggregation.AggregationConstants.INTERVAL_GROUP_BY_FIELD;
import static cern.nxcals.api.custom.service.aggregation.AggregationConstants.INTERVAL_RANGE_FIELDS;
import static cern.nxcals.api.custom.service.aggregation.AggregationConstants.INTERVAL_START_RANGE_JOIN_FIELD;
import static cern.nxcals.common.SystemFields.NXC_EXTR_TIMESTAMP;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VALUE;
import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.lit;

@RequiredArgsConstructor
final class AggregationServiceImpl implements AggregationService {
    private static final String DATASET_LEFT_OUTER_JOIN = "leftouter";
    private static final String DATASET_LEFT_SEMI_JOIN = "leftsemi";

    private static final Set<String> MANDATORY_AGGREGATION_COLUMN_NAMES = Sets.newHashSet(
            AGGREGATION_SOURCE_VALUE_FIELD, AGGREGATION_SOURCE_TIMESTAMP_FIELD);
    private static final String DATASET_VALIDATION_MESSAGE = String
            .format("Dataframe to scale should at least contain the following fields: [%s]",
                    String.join(" ,", MANDATORY_AGGREGATION_COLUMN_NAMES));

    @NonNull
    private final Supplier<SparkSession> sessionSupplier;
    @NonNull
    private final QueryDatasetProvider<Variable> variableDataProvider;
    @NonNull
    private final QueryDatasetProvider<Entity> entityDataProvider;
    @NonNull
    private final FundamentalService fundamentalService;

    @Override
    public Dataset<Row> getData(@NonNull Variable variable, @NonNull WindowAggregationProperties properties) {
        Function<TimeWindow, Dataset<Row>> dataProvider = tw -> variableDataProvider.get(sessionSupplier.get(), tw,
                variable);
        TimeWindow timeWindow = expandQueryTimeWindow(properties.getTimeWindow(), dataProvider,
                NXC_EXTR_TIMESTAMP.getValue(), properties.getFunction());
        Dataset<Row> dataset = variableDataProvider.get(sessionSupplier.get(), timeWindow, variable);
        String aggregationField = Objects.toString(properties.getAggregationField(), NXC_EXTR_VALUE.getValue());
        return aggregateOnFixedIntervals(
                createAggregationColumns(dataset, aggregationField, NXC_EXTR_TIMESTAMP.getValue()),
                properties);
    }

    @Override
    public Dataset<Row> getData(@NonNull Entity entity, @NonNull WindowAggregationProperties properties) {
        String aggregationField = Objects.requireNonNull(properties.getAggregationField(),
                "Aggregation field must not be null for entity data scaling process!");
        String timestampField = ExtractionUtils.getTimestampFieldName(entity.getSystemSpec());

        Function<TimeWindow, Dataset<Row>> dataProvider = tw -> entityDataProvider.get(sessionSupplier.get(), tw,
                entity);
        TimeWindow timeWindow = expandQueryTimeWindow(properties.getTimeWindow(), dataProvider, timestampField,
                properties.getFunction());
        Dataset<Row> dataset = entityDataProvider.get(sessionSupplier.get(), timeWindow, entity);
        return aggregateOnFixedIntervals(createAggregationColumns(dataset, aggregationField, timestampField),
                properties);
    }

    @Override
    public Dataset<Row> getData(@NonNull Variable variable, @NonNull DatasetAggregationProperties properties) {
        Function<TimeWindow, Dataset<Row>> dataProvider = tw -> variableDataProvider.get(sessionSupplier.get(), tw,
                variable);
        TimeWindow timeWindow = expandQueryTimeWindow(properties.getTimeWindow(), dataProvider,
                NXC_EXTR_TIMESTAMP.getValue(), AggregationFunctions.REPEAT);

        Dataset<Row> dataset = variableDataProvider.get(sessionSupplier.get(), timeWindow, variable);
        String aggregationField = Objects.toString(properties.getAggregationField(), NXC_EXTR_VALUE.getValue());
        return aggregateOnDataset(createAggregationColumns(dataset, aggregationField, NXC_EXTR_TIMESTAMP.getValue()),
                properties);
    }

    @Override
    public Dataset<Row> getData(@NonNull Entity entity, @NonNull DatasetAggregationProperties properties) {
        String aggregationField = Objects.requireNonNull(properties.getAggregationField(),
                "Aggregation field must not be null for entity data scaling process!");
        String timestampField = ExtractionUtils.getTimestampFieldName(entity.getSystemSpec());

        Function<TimeWindow, Dataset<Row>> dataProvider = tw -> entityDataProvider.get(sessionSupplier.get(), tw,
                entity);
        TimeWindow timeWindow = expandQueryTimeWindow(properties.getTimeWindow(), dataProvider, timestampField,
                AggregationFunctions.REPEAT);
        Dataset<Row> dataset = entityDataProvider.get(sessionSupplier.get(), timeWindow, entity);
        return aggregateOnDataset(createAggregationColumns(dataset, aggregationField, timestampField), properties);
    }

    private Dataset<Row> aggregateOnFixedIntervals(@NonNull Dataset<Row> dataset,
            @NonNull WindowAggregationProperties properties) {
        if (CollectionUtils.isNotEmpty(properties.getFundamentalFilters())) {
            dataset = filterDatasetByFundamentals(dataset, properties);
        }
        DrivingDatasetProvider fixedIntervalsDatasetProvider = lookupLimits -> AggregationUtils
                .generateFixedIntervals(sessionSupplier.get(), properties, lookupLimits);
        return aggregate(dataset, fixedIntervalsDatasetProvider, properties.getTimeWindow(), properties.getFunction());
    }

    private Dataset<Row> aggregateOnDataset(@NonNull Dataset<Row> dataset,
            @NonNull DatasetAggregationProperties properties) {
        DrivingDatasetProvider drivingDatasetProvider = lookupLimits -> AggregationUtils
                .extractIntervals(sessionSupplier.get(), properties, lookupLimits);
        return aggregate(dataset, drivingDatasetProvider, properties.getTimeWindow(), AggregationFunctions.REPEAT);
    }

    private Dataset<Row> aggregate(@NonNull Dataset<Row> dataset,
            @NonNull DrivingDatasetProvider drivingDatasetProvider,
            @NonNull TimeWindow timeWindow, @NonNull AggregationFunction function) {
        validateDataset(dataset);
        // limit data lookup just up to the first element on both sides
        Pair<Instant, Instant> lookupLimits = extractDataLookupLimits(dataset, timeWindow, function);
        Dataset<Row> dataToAggregate = rangeJoin(drivingDatasetProvider.apply(lookupLimits), dataset);
        return limitToOriginalTimeWindow(function.prepare(dataToAggregate), timeWindow)
                .where(col(INTERVAL_GROUP_BY_FIELD).isNotNull())
                .groupBy(col(INTERVAL_GROUP_BY_FIELD).as(AGGREGATION_TIMESTAMP_FIELD))
                .agg(function.getAggregationExpr().as(AGGREGATION_VALUE_FIELD))
                .orderBy(AGGREGATION_TIMESTAMP_FIELD);
    }

    private Dataset<Row> rangeJoin(@NonNull Dataset<Row> drivingDataset, @NonNull Dataset<Row> dataset) {
        Broadcast<long[]> rangeStart = sessionSupplier.get().sparkContext().broadcast(
                drivingDataset.select(INTERVAL_RANGE_FIELDS.getLeft()).distinct().collectAsList()
                        .stream().mapToLong(x -> x.getLong(0)).sorted().toArray(), ClassTag.apply(long[].class));

        dataset = dataset.withColumn(INTERVAL_START_RANGE_JOIN_FIELD,
                functions.udf(MoreFunctions.findFirstLessThanOrEqual(rangeStart), DataTypes.LongType)
                        .apply(dataset.col(AGGREGATION_SOURCE_TIMESTAMP_FIELD)));

        Column joinCondition = drivingDataset.col(INTERVAL_RANGE_FIELDS.getLeft())
                .eqNullSafe(dataset.col(INTERVAL_START_RANGE_JOIN_FIELD));
        return drivingDataset.join(dataset, joinCondition, DATASET_LEFT_OUTER_JOIN);
    }

    private Dataset<Row> filterDatasetByFundamentals(@NonNull Dataset<Row> dataset,
            @NonNull WindowAggregationProperties properties) {
        Dataset<Row> fundamentalDataset = fundamentalService.getAll(properties.getTimeWindow(),
                properties.getFundamentalFilters());
        return dataset.join(fundamentalDataset, dataset.col(AGGREGATION_SOURCE_TIMESTAMP_FIELD)
                .equalTo(fundamentalDataset.col(NXC_EXTR_TIMESTAMP.getValue())), DATASET_LEFT_SEMI_JOIN);
    }

    // helper methods

    private Pair<Instant, Instant> extractDataLookupLimits(Dataset<Row> dataset,
            TimeWindow timeWindow, AggregationFunction function) {
        Pair<Duration, Duration> rangeExpansion = function.getTimeWindowExpansion();
        Instant minLookupTime = rangeExpansion.getLeft().isZero() ? timeWindow.getStartTime() :
                AggregationUtils.getFirstTimestampOrDefault(dataset.where(col(AGGREGATION_SOURCE_TIMESTAMP_FIELD)
                                .lt(timeWindow.getStartTimeNanos())).orderBy(col(AGGREGATION_SOURCE_TIMESTAMP_FIELD).desc()),
                        AGGREGATION_SOURCE_TIMESTAMP_FIELD, timeWindow.getStartTime());
        Instant maxLookupTime = rangeExpansion.getRight().isZero() ? timeWindow.getEndTime() :
                AggregationUtils.getFirstTimestampOrDefault(dataset.where(col(AGGREGATION_SOURCE_TIMESTAMP_FIELD)
                                .gt(timeWindow.getEndTimeNanos())).orderBy(col(AGGREGATION_SOURCE_TIMESTAMP_FIELD)),
                        AGGREGATION_SOURCE_TIMESTAMP_FIELD, timeWindow.getEndTime());
        return Pair.of(minLookupTime, maxLookupTime);
    }

    private Dataset<Row> limitToOriginalTimeWindow(@NonNull Dataset<Row> dataset, @NonNull TimeWindow timeWindow) {
        return dataset.where(col(INTERVAL_RANGE_FIELDS.getLeft()).geq(lit(timeWindow.getStartTimeNanos()))
                .and(col(INTERVAL_RANGE_FIELDS.getRight()).leq(lit(timeWindow.getEndTimeNanos()))));
    }

    private TimeWindow expandQueryTimeWindow(@NonNull TimeWindow timeWindow, @NonNull Function<TimeWindow, Dataset<Row>>
            dataProvider, String timestampField, @NonNull AggregationFunction function) {
        Pair<Duration, Duration> lookupBounds = function.getTimeWindowExpansion();
        Optional<Instant> expandBefore = DataLookupUtils
                .getAdjacentTimestampBefore(timeWindow.getStartTime(), dataProvider, timestampField,
                        lookupBounds.getLeft());
        Optional<Instant> expandAfter = DataLookupUtils
                .getAdjacentTimestampAfter(timeWindow.getEndTime(), dataProvider, timestampField,
                        lookupBounds.getRight());

        return TimeWindow.between(expandBefore.orElse(timeWindow.getStartTime()),
                expandAfter.orElse(timeWindow.getEndTime()));
    }

    private void validateDataset(Dataset<Row> datasetToScale) {
        ensureContainsColumns(datasetToScale, MANDATORY_AGGREGATION_COLUMN_NAMES, DATASET_VALIDATION_MESSAGE);
    }

    private Dataset<Row> createAggregationColumns(Dataset<Row> dataset, String actualDataColumnName,
            String actualTimestampColumnName) {
        ensureContainsColumns(dataset, Collections.singleton(actualDataColumnName), String.format(
                "Dataframe does not contain data column: [%s].%nAvailable columns to select: [%s]",
                actualDataColumnName, String.join(" ,", Arrays.asList(dataset.columns()))));
        ensureContainsColumns(dataset, Collections.singleton(actualTimestampColumnName),
                "Could not identify valid timestamp field in dataset for aggregation!");
        return dataset.withColumn(AGGREGATION_SOURCE_VALUE_FIELD, col(actualDataColumnName))
                .withColumn(AGGREGATION_SOURCE_TIMESTAMP_FIELD, col(actualTimestampColumnName));
    }

    private void ensureContainsColumns(Dataset<Row> dataset, Collection<String> columnNames, String exceptionMessage) {
        if (!Arrays.asList(dataset.columns()).containsAll(columnNames)) {
            throw new IllegalStateException(exceptionMessage);
        }
    }

    @FunctionalInterface
    private interface DrivingDatasetProvider extends Function<Pair<Instant, Instant>, Dataset<Row>> {
    }

}
