package cern.nxcals.api.custom.service.aggregation;

import lombok.NonNull;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.spark.sql.expressions.Window;
import org.apache.spark.sql.expressions.WindowSpec;

import java.time.Duration;
import java.time.temporal.TemporalUnit;

import static cern.nxcals.api.custom.service.aggregation.AggregationConstants.INTERVAL_RANGE_FIELDS;

public abstract class ExpandableAggregationFunction extends AggregationFunction {
    protected static final WindowSpec RECORDS_ROW_WINDOW = Window
            .orderBy(INTERVAL_RANGE_FIELDS.getLeft(), TIMESTAMP_COLUMN_NAME);

    ExpandableAggregationFunction(@NonNull Pair<Duration, Duration> timeWindowExpansion) {
        super(timeWindowExpansion);
    }

    /**
     * Replaces the default data query time window expansion associated with this {@link AggregationFunctions} implementation.
     *
     * @param amountBefore the amount of the duration to expand, before the start time of query time window, measured in terms of the unit.
     *                     Parameter should be a positive value and would be validated against that expectation
     * @param amountAfter  the amount of the duration to expand, after the end time of query time window, measured in terms of the unit.
     *                     Parameter should be a positive value and would be validated against that expectation
     * @param unit         the unit that the duration is measured in, must have an exact duration, not null
     * @return a new {@link ExpandableAggregationFunction} based on this function containing the specified time window expansion settings
     */
    public ExpandableAggregationFunction expandTimeWindowBy(long amountBefore, long amountAfter,
            @NonNull TemporalUnit unit) {
        return buildExpanded(ExpandTimeWindow.buildFrom(amountBefore, amountAfter, unit));
    }

    abstract ExpandableAggregationFunction buildExpanded(Pair<Duration, Duration> expandTimeWindow);

}
