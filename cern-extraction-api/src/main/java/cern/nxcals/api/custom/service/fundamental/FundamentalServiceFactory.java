package cern.nxcals.api.custom.service.fundamental;

import cern.nxcals.api.custom.service.FundamentalService;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import lombok.NonNull;
import lombok.experimental.UtilityClass;
import org.apache.spark.sql.SparkSession;

import java.util.function.Supplier;

@UtilityClass
public final class FundamentalServiceFactory {
    public static FundamentalService getInstance(@NonNull Supplier<SparkSession> sessionSupplier) {
        return new FundamentalServiceImpl(sessionSupplier, FundamentalContext.VARIABLE_LIST_DATASET_PROVIDER,
                ServiceClientFactory.createVariableService());
    }

    public static FundamentalService getInstance(@NonNull SparkSession sparkSession) {
        return getInstance(() -> sparkSession);
    }
}
