package cern.nxcals.api.custom.domain.util;

import cern.nxcals.api.domain.TimeWindow;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

@FunctionalInterface
public interface QueryDatasetProvider<T> {
    Dataset<Row> get(SparkSession session, TimeWindow timeWindow, T domain);
}
