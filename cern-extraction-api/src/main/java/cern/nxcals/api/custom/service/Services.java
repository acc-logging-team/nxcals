package cern.nxcals.api.custom.service;

import cern.nxcals.api.config.SparkProperties;
import cern.nxcals.api.custom.service.aggregation.AggregationServiceFactory;
import cern.nxcals.api.custom.service.extraction.ExtractionServiceFactory;
import cern.nxcals.api.custom.service.fill.FillServiceFactory;
import cern.nxcals.api.custom.service.fundamental.FundamentalServiceFactory;
import cern.nxcals.api.utils.SparkUtils;
import com.google.common.annotations.VisibleForTesting;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.apache.spark.sql.SparkSession;

import java.util.function.Supplier;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Services {
    private static final String DEFAULT_APP_NAME = "nxcals-cern-app";
    private final Supplier<SparkSession> sessionSupplier;

    public static Services newInstance() {
        return newInstance(SparkProperties.defaults(DEFAULT_APP_NAME));
    }

    public static Services newInstance(@NonNull SparkProperties props) {
        return new Services(SparkUtils.createSparkSessionSupplier(props));
    }

    public static Services newInstance(@NonNull SparkSession session) {
        return new Services(() -> session);
    }

    public static Services newInstance(@NonNull Supplier<SparkSession> sessionSupplier) {
        return new Services(addNullCheckToSupplier(sessionSupplier));
    }

    @VisibleForTesting
    static Supplier<SparkSession> addNullCheckToSupplier(Supplier<SparkSession> sessionSupplier) {
        return () -> {
            SparkSession session = sessionSupplier.get();
            if (session == null) {
                throw new IllegalArgumentException(
                        "SparkSession supplier returned null! Please, provide a proper supplier when creating the service.");
            }
            return session;
        };
    }

    public FillService fillService() {
        return FillServiceFactory.getInstance(sessionSupplier);
    }

    public FundamentalService fundamentalService() {
        return FundamentalServiceFactory.getInstance(sessionSupplier);
    }

    public AggregationService aggregationService() {
        return AggregationServiceFactory.getInstance(sessionSupplier, fundamentalService());
    }

    public ExtractionService extractionService() {
        return ExtractionServiceFactory.getInstance(sessionSupplier);
    }


}
