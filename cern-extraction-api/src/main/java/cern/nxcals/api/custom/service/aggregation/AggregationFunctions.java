package cern.nxcals.api.custom.service.aggregation;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.apache.spark.sql.functions;

/**
 * Defines all available data aggregation functions
 *
 * @see AggregationFunction
 * @see ExpandableAggregationFunction
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public final class AggregationFunctions {
    public static final AggregationFunction MIN = new DefaultAggregationFunction(functions::min);
    public static final AggregationFunction MAX = new DefaultAggregationFunction(functions::max);
    public static final AggregationFunction AVG = new DefaultAggregationFunction(functions::avg);
    public static final AggregationFunction SUM = new DefaultAggregationFunction(functions::sum);
    public static final AggregationFunction COUNT = new DefaultAggregationFunction(functions::count);

    public static final ExpandableAggregationFunction REPEAT = new RepeatAggregationFunction();
    public static final ExpandableAggregationFunction INTERPOLATE = new InterpolateAggregationFunction();

}
