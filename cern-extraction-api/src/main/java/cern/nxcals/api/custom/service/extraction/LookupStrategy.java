package cern.nxcals.api.custom.service.extraction;

import cern.nxcals.api.custom.domain.util.DataLookupUtils;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.common.utils.TriFunction;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.function.Function;

@Value
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class LookupStrategy
        implements TriFunction<Function<TimeWindow, Dataset<Row>>, String, ExtractionProperties, Dataset<Row>> {
    private static final Duration DEFAULT_LOOKUP_DURATION = ChronoUnit.YEARS.getDuration(); // 1 year

    /**
     * Perform data lookup to extract the last available datapoint (if any) prior the actual data that belong
     * to a given time window. If such datapoint exists, we query the dataset with the timestamp of that datapoint
     * as the startTime to get the dataset of the time window plus that datapoint.
     */
    public static final LookupStrategy LAST_BEFORE_START = new LookupStrategy(Handler.LAST_BEFORE_START);

    /**
     * Perform data lookup to extract the last available datapoint (if any) prior the given time window,
     * only if the actual data do not exist.
     * In that case a single row dataset would be returned, containing that last datapoint.
     * Otherwise, if data exist for the given time window, this strategy will not perform any data lookup action,
     * thus, only data points matching the query time window would be included on the result.
     */
    public static final LookupStrategy LAST_BEFORE_START_IF_EMPTY = new LookupStrategy(
            Handler.LAST_BEFORE_START_IF_EMPTY);

    /**
     * No data lookup logic should be applied.
     * NOTE: This is a protected strategy used/resolved internally when no strategy is specified
     */
    static final LookupStrategy NONE = new LookupStrategy(Handler.NO_LOOKUP, Duration.ZERO);

    /**
     * Perform data lookup to extract the first available datapoint (if any) after the given time window.
     * A single row dataset would be returned, containing that first datapoint.
     */
    public static final LookupStrategy NEXT_AFTER_START_ONLY = new LookupStrategy(Handler.NEXT_AFTER_START_ONLY);

    /**
     * Perform data lookup to extract the first available datapoint (if any) prior the given time window.
     * A single row dataset would be returned, containing that first datapoint.
     */
    public static final LookupStrategy LAST_BEFORE_START_ONLY = new LookupStrategy(Handler.LAST_BEFORE_START_ONLY);

    @NonNull
    @EqualsAndHashCode.Include
    Handler handler;

    @NonNull Duration duration;

    private LookupStrategy(@NonNull Handler handler) {
        this(handler, DEFAULT_LOOKUP_DURATION);
    }

    public LookupStrategy withLookupDuration(long amount, @NonNull TemporalUnit unit) {
        return new LookupStrategy(this.getHandler(), unit.getDuration().multipliedBy(Math.abs(amount)));
    }

    /**
     * This method retrieves the dataset containing the datapoint accordingly to the lookup strategy.
     * All three parameters are needed because when the strategy is LAST_BEFORE_START_IF_EMPTY we cannot know beforehand
     * what the startTime will be.
     *
     * @param dataProvider This is used to extract the data
     * @param properties   This is used to retrieve the end time of time window we are querying on
     * @return Dataset&lt;Row&gt; The dataset
     */
    @Override
    public Dataset<Row> apply(Function<TimeWindow, Dataset<Row>> dataProvider, String timestampField,
            ExtractionProperties properties) {
        return this.getHandler().apply(dataProvider, timestampField, properties);
    }

    /**
     * Apply the lookup logic enforced by the selected strategy and query with the appropriate time window to get the dataset
     */
    private enum Handler
            implements TriFunction<Function<TimeWindow, Dataset<Row>>, String, ExtractionProperties, Dataset<Row>> {
        LAST_BEFORE_START {
            @Override
            public Dataset<Row> apply(@NonNull Function<TimeWindow, Dataset<Row>> dataProvider,
                    @NonNull String timestampField, @NonNull ExtractionProperties properties) {
                Instant startTime = Handler.getBackwardLookupTimeWindow(properties, timestampField, dataProvider)
                        .getStartTime();
                Instant endTime = properties.getTimeWindow().getEndTime();
                return dataProvider.apply(TimeWindow.between(startTime, endTime));
            }
        }, LAST_BEFORE_START_IF_EMPTY {
            @Override
            public Dataset<Row> apply(@NonNull Function<TimeWindow, Dataset<Row>> dataProvider,
                    @NonNull String timestampField, @NonNull ExtractionProperties properties) {
                Dataset<Row> dataset = dataProvider.apply(properties.getTimeWindow());
                if (dataset.isEmpty()) {
                    TimeWindow tw = Handler.getBackwardLookupTimeWindow(properties, timestampField, dataProvider);
                    return dataProvider.apply(tw);
                }
                return dataset;
            }
        }, NO_LOOKUP {
            @Override
            public Dataset<Row> apply(@NonNull Function<TimeWindow, Dataset<Row>> dataProvider,
                    @NonNull String timestampField, @NonNull ExtractionProperties properties) {
                return dataProvider.apply(properties.getTimeWindow());
            }
        }, NEXT_AFTER_START_ONLY {
            @Override
            public Dataset<Row> apply(@NonNull Function<TimeWindow, Dataset<Row>> dataProvider,
                    @NonNull String timestampField, @NonNull ExtractionProperties properties) {
                TimeWindow tw = Handler.getForwardLookupTimeWindow(properties, timestampField, dataProvider);
                return dataProvider.apply(tw);
            }
        }, LAST_BEFORE_START_ONLY {
            @Override
            public Dataset<Row> apply(@NonNull Function<TimeWindow, Dataset<Row>> dataProvider,
                    @NonNull String timestampField, @NonNull ExtractionProperties properties) {
                TimeWindow tw = Handler.getBackwardLookupTimeWindow(properties, timestampField, dataProvider);
                return dataProvider.apply(tw);
            }
        },
        ;

        private static TimeWindow getBackwardLookupTimeWindow(ExtractionProperties properties, String timestampField,
                Function<TimeWindow, Dataset<Row>> dataProvider) {
            Instant startTime = properties.getTimeWindow().getStartTime();
            Instant endTime = startTime.minusNanos(1); // avoid including start point again
            Instant timestampBefore = DataLookupUtils.getAdjacentTimestampBefore(startTime, dataProvider,
                    timestampField, properties.getLookupStrategy().getDuration()).orElse(endTime);
            return TimeWindow.between(timestampBefore, endTime);
        }

        private static TimeWindow getForwardLookupTimeWindow(ExtractionProperties properties, String timestampField,
                Function<TimeWindow, Dataset<Row>> dataProvider) {
            Instant nextTime = properties.getTimeWindow().getStartTime();
            Instant startTime = nextTime.plusNanos(1); // avoid including start point again
            Instant timestampAfter = DataLookupUtils.getAdjacentTimestampAfter(nextTime, dataProvider, timestampField,
                    properties.getLookupStrategy().getDuration()).orElse(startTime);
            return TimeWindow.between(startTime, timestampAfter);
        }

    }

}
