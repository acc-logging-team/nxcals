package cern.nxcals.api.custom.service.fill;

import cern.nxcals.api.domain.TimeWindow;
import com.google.common.collect.ImmutableSortedMap;
import lombok.experimental.UtilityClass;

import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

@UtilityClass
class FillUtils {

    public static final Comparator<TimeWindow> TIME_WINDOW_COMPARATOR = TimeWindow.compareByStartAndEndTime();

    private static final NavigableMap<TimeWindow, ?> EMPTY_MAP = ImmutableSortedMap.copyOfSorted(
            new TreeMap<>(TIME_WINDOW_COMPARATOR));

    public static <T> NavigableMap<TimeWindow, T> navigableMapFrom(Map<TimeWindow, T> input) {
        NavigableMap<TimeWindow, T> map = new TreeMap<>(TIME_WINDOW_COMPARATOR);
        map.putAll(input);
        return map;
    }

    public static <T> NavigableMap<TimeWindow, T> emptyNavigableMap() {
        @SuppressWarnings("unchecked")
        NavigableMap<TimeWindow, T> t = (NavigableMap<TimeWindow, T>) Collections.unmodifiableNavigableMap(EMPTY_MAP);
        return t;
    }
}
