package cern.nxcals.api.custom.service.aggregation;

import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

@UtilityClass
public final class AggregationConstants {
    public static final String AGGREGATION_VALUE_FIELD = "value";
    public static final String AGGREGATION_TIMESTAMP_FIELD = "timestamp";

    static final Pair<String, String> INTERVAL_RANGE_FIELDS = new ImmutablePair<>("start_interval_nanos",
            "end_interval_nanos");
    static final String INTERVAL_GROUP_BY_FIELD = "group_by_timestamp";

    static final String AGGREGATION_SOURCE_VALUE_FIELD = "aggregation_source_value";
    static final String AGGREGATION_SOURCE_TIMESTAMP_FIELD = "aggregation_source_timestamp";

    static final String INTERVAL_START_RANGE_JOIN_FIELD = String.join("_", INTERVAL_RANGE_FIELDS.getLeft(),
            "join_matching_range");
}
