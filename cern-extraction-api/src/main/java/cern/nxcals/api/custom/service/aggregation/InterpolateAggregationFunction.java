package cern.nxcals.api.custom.service.aggregation;

import lombok.NonNull;
import lombok.Value;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.expressions.Window;
import org.apache.spark.sql.expressions.WindowSpec;

import java.time.Duration;

import static cern.nxcals.api.custom.service.aggregation.AggregationConstants.INTERVAL_RANGE_FIELDS;
import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.first;
import static org.apache.spark.sql.functions.last;
import static org.apache.spark.sql.functions.when;

@Value
final class InterpolateAggregationFunction extends ExpandableAggregationFunction {
    static final String PREV_VALUE_COLUMN_NAME = "prev_value";
    static final String PREV_TIMESTAMP_COLUMN_NAME = "prev_timestamp";

    static final String NEXT_VALUE_COLUMN_NAME = "next_value";
    static final String NEXT_TIMESTAMP_COLUMN_NAME = "next_timestamp";

    static final String INTERPOLATED_VALUE_COLUMN_NAME = "interpolated_value";

    static final WindowSpec PREV_RECORDS_ROW_WINDOW = RECORDS_ROW_WINDOW
            .rowsBetween(Window.unboundedPreceding(), Window.currentRow() - 1);
    static final WindowSpec NEXT_RECORDS_ROW_WINDOW = RECORDS_ROW_WINDOW
            .rowsBetween(Window.currentRow() + 1, Window.unboundedFollowing());

    InterpolateAggregationFunction() {
        this(ExpandTimeWindow.START_AND_END_TIME);
    }

    private InterpolateAggregationFunction(@NonNull Pair<Duration, Duration> timeWindowExpansion) {
        super(timeWindowExpansion);
    }

    @Override
    ExpandableAggregationFunction buildExpanded(@NonNull Pair<Duration, Duration> expandTimeWindow) {
        return new InterpolateAggregationFunction(expandTimeWindow);
    }

    @Override
    Dataset<Row> prepare(@NonNull Dataset<Row> dataset) {
        Column coefficient = col(NEXT_VALUE_COLUMN_NAME).minus(col(PREV_VALUE_COLUMN_NAME))
                .divide(col(NEXT_TIMESTAMP_COLUMN_NAME).minus(col(PREV_TIMESTAMP_COLUMN_NAME)));

        Column interpolateColumn = col(PREV_VALUE_COLUMN_NAME).plus(
                coefficient.multiply(col(INTERVAL_RANGE_FIELDS.getLeft()).minus(col(PREV_TIMESTAMP_COLUMN_NAME))));

        return dataset.withColumn(PREV_TIMESTAMP_COLUMN_NAME,
                when(col(TIMESTAMP_COLUMN_NAME).equalTo(col(INTERVAL_RANGE_FIELDS.getLeft())),
                        col(TIMESTAMP_COLUMN_NAME))
                        .otherwise(last(TIMESTAMP_COLUMN_NAME, true).over(PREV_RECORDS_ROW_WINDOW)))
                .withColumn(PREV_VALUE_COLUMN_NAME,
                        when(col(TIMESTAMP_COLUMN_NAME).equalTo(col(INTERVAL_RANGE_FIELDS.getLeft())),
                                col(VALUE_COLUMN_NAME))
                                .otherwise(last(VALUE_COLUMN_NAME, true).over(PREV_RECORDS_ROW_WINDOW)))
                .withColumn(NEXT_TIMESTAMP_COLUMN_NAME,
                        first(TIMESTAMP_COLUMN_NAME, true).over(NEXT_RECORDS_ROW_WINDOW))
                .withColumn(NEXT_VALUE_COLUMN_NAME, first(VALUE_COLUMN_NAME, true).over(NEXT_RECORDS_ROW_WINDOW))

                .withColumn(INTERPOLATED_VALUE_COLUMN_NAME, when(col(PREV_TIMESTAMP_COLUMN_NAME)
                        .equalTo(col(NEXT_TIMESTAMP_COLUMN_NAME)), col(VALUE_COLUMN_NAME))
                        .otherwise(interpolateColumn));
    }

    @Override
    Column getAggregationExpr() {
        return first(INTERPOLATED_VALUE_COLUMN_NAME, true);
    }

}
