package cern.nxcals.api.custom.service.aggregation;

import lombok.NonNull;
import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;

@UtilityClass
final class ExpandTimeWindow {
    static final Pair<Duration, Duration> NONE = buildFrom(0, 0, ChronoUnit.SECONDS);
    static final Pair<Duration, Duration> START_TIME = buildFrom(1, 0, ChronoUnit.YEARS);
    static final Pair<Duration, Duration> START_AND_END_TIME = buildFrom(1, 1, ChronoUnit.YEARS);

    static Pair<Duration, Duration> buildFrom(long amountBefore, long amountAfter,
            @NonNull TemporalUnit unit) {
        Duration expandBefore = AggregationUtils.toDuration(amountBefore, unit);
        Duration expandAfter = AggregationUtils.toDuration(amountAfter, unit);
        return ImmutablePair.of(expandBefore, expandAfter);
    }

}
