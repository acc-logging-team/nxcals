package cern.nxcals.api.custom.service.extraction;

import cern.nxcals.api.custom.service.ExtractionService;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import lombok.NonNull;
import lombok.experimental.UtilityClass;
import org.apache.spark.sql.SparkSession;

import java.util.function.Supplier;

@UtilityClass
public final class ExtractionServiceFactory {
    public static ExtractionService getInstance(@NonNull Supplier<SparkSession> sessionSupplier) {
        return new ExtractionServiceImpl(sessionSupplier, DataQuery::getFor, DataQuery::getFor);
    }

    public static ExtractionService getInstance(@NonNull SparkSession sparkSession) {
        return getInstance(() -> sparkSession);
    }
}
