package cern.nxcals.api.custom.service.aggregation;

import cern.nxcals.api.custom.service.AggregationService;
import cern.nxcals.api.custom.service.FundamentalService;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import lombok.NonNull;
import lombok.experimental.UtilityClass;
import org.apache.spark.sql.SparkSession;

import java.util.function.Supplier;

@UtilityClass
public final class AggregationServiceFactory {
    public static AggregationService getInstance(@NonNull Supplier<SparkSession> sessionSupplier,
            @NonNull FundamentalService fundamentalService) {
        return new AggregationServiceImpl(sessionSupplier, DataQuery::getFor, DataQuery::getFor, fundamentalService);
    }

    public static AggregationService getInstance(@NonNull SparkSession sparkSession,
            @NonNull FundamentalService fundamentalService) {
        return getInstance(() -> sparkSession, fundamentalService);
    }
}
