package cern.nxcals.api.custom.service.fill;

import cern.nxcals.api.domain.TimeWindow;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;

import java.util.Collections;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.stream.Collectors;

import static cern.nxcals.api.custom.service.fill.FillUtils.TIME_WINDOW_COMPARATOR;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
class BeamModeAlignment {

    public NavigableMap<TimeWindow, Integer> getFillsForTimeRange(NavigableMap<TimeWindow, Integer> allFills,
            TimeWindow range) {
        return takeFrom(allFills, range);
    }

    public NavigableMap<TimeWindow, String> getBeamModesForTimeRange(NavigableMap<TimeWindow, String> allModes,
            TimeWindow range) {
        return takeFrom(allModes, range).entrySet().stream().
                collect(Collectors.toMap(key -> key.getKey().intersect(range), Map.Entry::getValue, (a, b) -> a,
                        () -> new TreeMap<>(
                                TIME_WINDOW_COMPARATOR)));
    }

    private <T> NavigableMap<TimeWindow, T> takeFrom(NavigableMap<TimeWindow, T> data, TimeWindow range) {
        if (data.isEmpty()) {
            return FillUtils.emptyNavigableMap();
        }

        TimeWindow key1 = range; // left-side closed
        TimeWindow key2 = TimeWindow.after(range.getEndTimeNanos() - 1); // right-side open

        key1 = TIME_WINDOW_COMPARATOR.compare(data.firstKey(), key1) >= 0 ? key1 : data.lowerKey(key1);
        key2 = TIME_WINDOW_COMPARATOR.compare(data.firstKey(), key2) >= 0 ? key2 : data.lowerKey(key2);

        if (TIME_WINDOW_COMPARATOR.compare(key1, key2) > 0) {
            return Collections.emptyNavigableMap();
        }

        NavigableMap<TimeWindow, T> result = data.subMap(key1, true, key2, true);
        while (!result.isEmpty() && result.firstKey().getEndTimeNanos() <= range.getStartTimeNanos()) {
            result.pollFirstEntry();
        }
        return result;
    }
}
