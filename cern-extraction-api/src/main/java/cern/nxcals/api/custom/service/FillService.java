package cern.nxcals.api.custom.service;


import cern.nxcals.api.custom.domain.Fill;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

public interface FillService {

    Optional<Fill> findFill(int number);

    List<Fill> findFills(Instant startTime, Instant endTime);

    Optional<Fill> getLastCompleted();

}
