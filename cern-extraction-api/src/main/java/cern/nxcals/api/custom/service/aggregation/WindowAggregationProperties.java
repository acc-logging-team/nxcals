package cern.nxcals.api.custom.service.aggregation;

import cern.nxcals.api.custom.domain.FundamentalFilter;
import cern.nxcals.api.domain.TimeWindow;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.Singular;
import lombok.Value;
import org.apache.commons.lang3.Validate;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.TemporalUnit;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Value
@Builder
public final class WindowAggregationProperties {
    @NonNull
    private final TimeWindow timeWindow;
    @NonNull
    @Getter(AccessLevel.NONE)
    private final Duration interval;
    @NonNull
    private final AggregationFunction function;
    private final String aggregationField;
    @NonNull
    @Singular
    private final Set<FundamentalFilter> fundamentalFilters;

    /**
     * @deprecated please use {@link WindowAggregationProperties#getIntervalMillis()} instead
     */
    @Deprecated
    public long getIntervalSeconds() {
        return this.interval.getSeconds();
    }

    public long getIntervalMillis() {
        return this.interval.toMillis();
    }

    public static class WindowAggregationPropertiesBuilder {

        public WindowAggregationPropertiesBuilder timeWindow(@NonNull Instant startTime, @NonNull Instant endTime) {
            Validate.isTrue(startTime.isBefore(endTime), "Invalid time window! Start time must be before end time.");
            this.timeWindow = TimeWindow.between(startTime, endTime);
            return this;
        }

        public WindowAggregationPropertiesBuilder timeWindow(@NonNull TimeWindow tw) {
            this.timeWindow = tw;
            return this;
        }

        /**
         * Defines the interval duration for the time window of the data aggregation action.
         *
         * @param amount the amount of the interval duration, measured in terms of the unit.
         *               Parameter should be a positive value and would be validated against that expectation
         * @param unit   the unit that the duration is measured in, must have an exact duration, not null
         */
        public WindowAggregationPropertiesBuilder interval(long amount,
                @NonNull TemporalUnit unit) {
            this.interval = AggregationUtils.toDuration(amount, unit);
            return this;
        }

        public WindowAggregationProperties build() {
            return new WindowAggregationProperties(this.timeWindow, this.interval, this.function,
                    this.aggregationField, emptyIfNull(this.fundamentalFilters));
        }

        /**
         * Returns an immutable empty set if the argument is <code>null</code>,
         * or the argument itself, converted to set, otherwise.
         *
         * @param fundamentalFilters the list of fundamentals, possibly <code>null</code>
         * @return an empty set if the argument is <code>null</code>
         * or a set containing the elements of the provided list
         */
        static Set<FundamentalFilter> emptyIfNull(List<FundamentalFilter> fundamentalFilters) {
            return fundamentalFilters == null ? Collections.emptySet() : new HashSet<>(fundamentalFilters);
        }
    }

}
