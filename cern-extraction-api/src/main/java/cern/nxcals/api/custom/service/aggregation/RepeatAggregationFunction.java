package cern.nxcals.api.custom.service.aggregation;

import lombok.NonNull;
import lombok.Value;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.expressions.Window;
import org.apache.spark.sql.expressions.WindowSpec;

import java.time.Duration;

import static cern.nxcals.api.custom.service.aggregation.AggregationConstants.INTERVAL_RANGE_FIELDS;
import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.first;
import static org.apache.spark.sql.functions.last;
import static org.apache.spark.sql.functions.when;

@Value
final class RepeatAggregationFunction extends ExpandableAggregationFunction {
    static final String REPEATED_VALUE_COLUMN_NAME = "repeated_value";

    static final WindowSpec REPEAT_RECORDS_ROW_WINDOW = RECORDS_ROW_WINDOW.rowsBetween(
            Window.unboundedPreceding(), Window.currentRow() - 1);

    RepeatAggregationFunction() {
        this(ExpandTimeWindow.START_TIME);
    }

    private RepeatAggregationFunction(@NonNull Pair<Duration, Duration> timeWindowExpansion) {
        super(timeWindowExpansion);
    }

    @Override
    ExpandableAggregationFunction buildExpanded(@NonNull Pair<Duration, Duration> expandTimeWindow) {
        return new RepeatAggregationFunction(expandTimeWindow);
    }

    @Override
    Dataset<Row> prepare(@NonNull Dataset<Row> dataset) {
        return dataset.withColumn(REPEATED_VALUE_COLUMN_NAME,
                when(col(TIMESTAMP_COLUMN_NAME).equalTo(col(INTERVAL_RANGE_FIELDS.getLeft())), col(VALUE_COLUMN_NAME))
                        .otherwise(last(VALUE_COLUMN_NAME, true).over(REPEAT_RECORDS_ROW_WINDOW)));
    }

    @Override
    Column getAggregationExpr() {
        return first(REPEATED_VALUE_COLUMN_NAME, true);
    }

}
