package cern.nxcals.api.custom.service.fill;

import cern.nxcals.api.custom.service.FillService;
import lombok.NonNull;
import lombok.experimental.UtilityClass;
import org.apache.spark.sql.SparkSession;

import java.util.function.Supplier;

@UtilityClass
public final class FillServiceFactory {
    public static FillService getInstance(@NonNull Supplier<SparkSession> sparkSessionSupplier) {
        return new FillServiceImpl(new FillProvider(sparkSessionSupplier));
    }

    public static FillService getInstance(@NonNull SparkSession sparkSession) {
        return new FillServiceImpl(new FillProvider(() -> sparkSession));
    }
}
