package cern.nxcals.api.custom.service.aggregation;

import cern.nxcals.api.domain.TimeWindow;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.Validate;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.time.Instant;
import java.util.Objects;

import static cern.nxcals.common.SystemFields.NXC_EXTR_TIMESTAMP;
import static org.apache.spark.sql.functions.col;

@Value
@Builder
public final class DatasetAggregationProperties {
    @NonNull
    private final TimeWindow timeWindow;
    @NonNull
    private final Dataset<Row> drivingDataset;

    private final String aggregationField;
    @NonNull
    private final String timestampField;

    public static class DatasetAggregationPropertiesBuilder {
        //hidden from public API - it's extracted internally from the provided dataset
        private DatasetAggregationPropertiesBuilder timeWindow(@NonNull TimeWindow timeWindow) {
            this.timeWindow = timeWindow;
            return this;
        }

        public DatasetAggregationProperties build() {
            timestampField(Objects.toString(timestampField, NXC_EXTR_TIMESTAMP.getValue()));
            Validate.isTrue(ArrayUtils.contains(drivingDataset.columns(), timestampField),
                    "Could not identify timestamp field for the provided driving dataset! Please specify it explicitly");
            timeWindow(extractTimeWindow(drivingDataset, timestampField));
            return new DatasetAggregationProperties(timeWindow, drivingDataset, aggregationField, timestampField);
        }

        private static TimeWindow extractTimeWindow(@NonNull Dataset<Row> drivingDataset,
                @NonNull String timestampField) {
            if (drivingDataset.isEmpty()) {
                throw new IllegalArgumentException("Driving dataset must not be empty");
            }
            Instant startTime = AggregationUtils
                    .getFirstTimestampOrDefault(drivingDataset.orderBy(col(timestampField)),
                            timestampField, TimeWindow.empty().getStartTime());
            Instant endTime = AggregationUtils
                    .getFirstTimestampOrDefault(drivingDataset.orderBy(col(timestampField).desc()),
                            timestampField, TimeWindow.empty().getEndTime());
            Validate.isTrue(!startTime.isAfter(endTime),
                    "Extracted time window is invalid! Please, verify the provided driving dataset.");
            return TimeWindow.between(startTime, endTime);
        }
    }

}
