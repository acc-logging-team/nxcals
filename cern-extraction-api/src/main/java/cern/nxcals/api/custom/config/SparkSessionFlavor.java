package cern.nxcals.api.custom.config;

import cern.nxcals.api.config.SparkYarnConfig;
import cern.nxcals.api.utils.YarnProperties;
import lombok.Getter;

import java.util.Collections;
import java.util.Map;

@Getter
public enum SparkSessionFlavor implements YarnProperties {

    SMALL(SparkYarnConfig.getConfig("nxcals_session_small")),
    MEDIUM(SparkYarnConfig.getConfig("nxcals_session_medium")),
    LARGE(SparkYarnConfig.getConfig("nxcals_session_large"));

    private Map<String, String> properties;

    SparkSessionFlavor(Map<String, String> config){
        this.properties = Collections.unmodifiableMap(config);
    }
}
