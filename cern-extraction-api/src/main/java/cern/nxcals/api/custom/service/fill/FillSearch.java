package cern.nxcals.api.custom.service.fill;

import cern.nxcals.api.domain.TimeWindow;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;

import java.util.Map;
import java.util.NavigableMap;
import java.util.Optional;
import java.util.stream.Collectors;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
/*
Previously this was a binary search but since the fill numbers might not be monotonic this approach does not work.
It is now changed to a simple map revert and get (jwozniak)
 */
class FillSearch {

    public Optional<TimeWindow> getFillForNumber(NavigableMap<TimeWindow, Integer> fills, int number) {
        //This is strange but it was assumed in tests, so maintained (jwozniak)
        if(fills == null) {
            return Optional.empty();
        }

        Map<Integer, TimeWindow> revert = fills.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey,
                        //If there are duplicated fill numbers the later one is kept.
                        (timeWindow1,timeWindow2)->
                                timeWindow1.getStartTime().isAfter(timeWindow2.getStartTime()) ? timeWindow1 : timeWindow2));
        return Optional.ofNullable(revert.get(number));
    }
}
