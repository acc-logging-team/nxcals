package cern.nxcals.api.custom.service;

import lombok.RequiredArgsConstructor;
import org.apache.spark.sql.SparkSession;

import java.util.function.Supplier;

/**
 * {@link SparkSession} configured for testing
 */
@RequiredArgsConstructor
public enum TestSparkSession implements Supplier<SparkSession> {
    JUNIT(SparkSession.builder().master("local")
            .appName("extraction-junit-tests")
            .config("spark.sql.shuffle.partitions", "1")
            .config("spark.rdd.compress", false)
            .config("spark.shuffle.compress", false)
            .getOrCreate());

    private final SparkSession session;

    @Override
    public SparkSession get() {
        return session;
    }
}
