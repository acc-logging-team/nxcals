package cern.nxcals.api.custom.service;

import lombok.NonNull;
import lombok.experimental.UtilityClass;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.StructType;

import java.util.ArrayList;
import java.util.List;

@UtilityClass
public final class TestUtils {

    public static Dataset<Row> buildDataset(@NonNull SparkSession session, @NonNull StructType schema,
            @NonNull Object[]... data) {
        List<Row> rows = new ArrayList<>(data.length);
        for (Object[] datum : data) {
            rows.add(RowFactory.create(datum));
        }
        return session.createDataFrame(rows, schema);
    }

    public static Dataset<Row> buildDataset(@NonNull StructType schema, @NonNull Object[]... data) {
        return buildDataset(TestSparkSession.JUNIT.get(), schema, data);
    }
}
