package cern.nxcals.api.custom.extraction.data.builders.expanded;

import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.EntityQuery;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.common.testutils.CartesianProduct;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertThrows;

class DataQueryTest {

    private static final SparkSession[] sparkSessions = { Mockito.mock(SparkSession.class), null };
    private static final TimeWindow[] timeWindows = {
            TimeWindow.between(Instant.ofEpochSecond(10), Instant.ofEpochSecond(50)), null };
    private static final String[] systems = { "SYSTEM", null };
    private static final Variable[] variables = { Mockito.mock(Variable.class), null };
    private static final Entity[] entities = { Mockito.mock(Entity.class), null };
    private static final String[] variablesNames = { "var", null };
    private static final ArrayList<Map<String, Object>> keyValues = new ArrayList<>(0);

    static {
        keyValues.add(Map.of("var", "var"));
        keyValues.add(null);
    }

    private static Stream<Arguments> getForVariableNPETestArgs() {
        return CartesianProduct.compose(sparkSessions, timeWindows, variables);
    }

    private static Stream<Arguments> getForEntityNPETestArgs() {
        return CartesianProduct.compose(sparkSessions, timeWindows, entities);
    }

    private static Stream<Arguments> getForVariableNameNPETestArgs() {
        return CartesianProduct.compose(sparkSessions, timeWindows, systems, variablesNames);
    }

    private static Stream<Arguments> getEntityWithKeyValueNPETestArgs() {
        return CartesianProduct.compose(sparkSessions, timeWindows, systems, keyValues.toArray());
    }

    private static Stream<Arguments> getEntityWithKeysInMapNPETestArgs() {
        return CartesianProduct.compose(sparkSessions, timeWindows, systems, keyValues.toArray(),
                keyValues.toArray());
    }

    @ParameterizedTest
    @MethodSource("getForVariableNPETestArgs")
    void shouldThrowNPEIfOneOfParamsIsNullOnGetForVariable(SparkSession session, TimeWindow timeWindow,
            Variable variable) {
        if (Stream.of(session, timeWindow, variable).anyMatch(Objects::isNull)) {
            assertThrows(NullPointerException.class,
                    () -> DataQuery.getFor(session, timeWindow, variable));
        }
    }

    @ParameterizedTest
    @MethodSource("getForEntityNPETestArgs")
    void shouldThrowNPEIfOneOfParamsIsNullOnGetForEntity(SparkSession session, TimeWindow timeWindow,
            Entity entity) {
        if (Stream.of(session, timeWindow, entity).anyMatch(Objects::isNull)) {
            assertThrows(NullPointerException.class,
                    () -> DataQuery.getFor(session, timeWindow, entity));
        }
    }

    @ParameterizedTest
    @MethodSource("getForVariableNameNPETestArgs")
    void shouldThrowNPEIfOneOfParametersIsNullOnGetForSingleVariableName(SparkSession sparkSession,
            TimeWindow timeWindow, String system, String variable) {
        if (Stream.of(sparkSession, timeWindow, system, variable).anyMatch(Objects::isNull)) {
            assertThrows(NullPointerException.class,
                    () -> DataQuery
                            .getFor(sparkSession, timeWindow, system, variable));
        }
    }

    @ParameterizedTest
    @MethodSource("getForVariableNameNPETestArgs")
    void shouldThrowNPEIfOneOfParametersIsNullOnGetForMultipleVariableNames(SparkSession sparkSession,
            TimeWindow timeWindow, String system, String variable) {
        if (Stream.of(sparkSession, timeWindow, system, variable).anyMatch(Objects::isNull)) {
            assertThrows(NullPointerException.class,
                    () -> DataQuery
                            .getFor(sparkSession, timeWindow, system, "name", variable));
        }
    }

    @ParameterizedTest
    @MethodSource("getForVariableNameNPETestArgs")
    void shouldThrowNPEIfOneOfParametersIsNullOnGetForListNames(SparkSession sparkSession, TimeWindow timeWindow,
            String system, String variable) {
        if (Stream.of(sparkSession, timeWindow, system, variable).anyMatch(Objects::isNull)) {
            assertThrows(NullPointerException.class,
                    () -> DataQuery
                            .getFor(sparkSession, timeWindow, system, toList(variable)));
        }
    }

    @ParameterizedTest
    @MethodSource("getForVariableNameNPETestArgs")
    void shouldThrowNPEIfOneOfParametersIsNull(SparkSession sparkSession,
            TimeWindow timeWindow, String system, String variable) {
        if (Stream.of(sparkSession, timeWindow, system, variable).anyMatch(Objects::isNull)) {
            assertThrows(NullPointerException.class,
                    () -> DataQuery
                            .getFor(sparkSession, timeWindow, system, List.of("VAR_NAME"), toList(variable)));
        }
    }

    @Test
    void shouldIllegalArgumentIfVariablesListIsEmpty() {
        TimeWindow timeWindow = TimeWindow.between(0, 1);
        SparkSession sessionMock = Mockito.mock(SparkSession.class);
        assertThrows(IllegalArgumentException.class,
                () -> DataQuery.getFor(sessionMock, timeWindow, "SYSTEM", List.of()));
    }

    @ParameterizedTest
    @MethodSource("getEntityWithKeysInMapNPETestArgs")
    void getForWithKeyValuesAsMapShouldAndRestOfParamsThrowNPEIfOneOfParametersIsNull(SparkSession sparkSession,
            TimeWindow timeWindow, String system, Map<String, Object> keyValue, Map<String, Object> keyValueLike) {
        if (Stream.of(sparkSession, timeWindow, system, keyValue, keyValueLike).anyMatch(Objects::isNull)) {
            assertThrows(NullPointerException.class,
                    () -> DataQuery
                            .getFor(sparkSession, timeWindow, system, new EntityQuery(keyValue, keyValueLike)));
        }
    }

    @ParameterizedTest
    @MethodSource("getEntityWithKeyValueNPETestArgs")
    void shouldThrowNPEIfOneOfParametersIsNullWithKeyValuesAsMap(SparkSession sparkSession,
            TimeWindow timeWindow, String system, Map<String, Object> keyValue) {
        if (Stream.of(sparkSession, timeWindow, system, keyValue).anyMatch(Objects::isNull)) {
            assertThrows(NullPointerException.class,
                    () -> DataQuery
                            .getFor(sparkSession, timeWindow, system, keyValue));
        }
    }

    @Test
    void shouldThrowIllegalArgumentIfKeyValueMapIsEmpty() {
        TimeWindow timeWindow = TimeWindow.between(0, 1);
        SparkSession sessionMock = Mockito.mock(SparkSession.class);
        assertThrows(IllegalArgumentException.class,
                () -> DataQuery.getFor(sessionMock, timeWindow, "SYSTEM", new HashMap<>()));
    }

    private static <E> List<E> toList(E element) {
        if (element == null) {
            return null;
        }
        return List.of(element);
    }
}
