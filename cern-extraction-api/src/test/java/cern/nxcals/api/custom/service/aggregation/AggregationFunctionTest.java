package cern.nxcals.api.custom.service.aggregation;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class AggregationFunctionTest {

    @Test
    public void shouldBuildWithCustomSearchRangeExpansionWhenAllowedByFunction() {
        ExpandableAggregationFunction aggregationFunction = AggregationFunctions.INTERPOLATE
                .expandTimeWindowBy(2, 1, ChronoUnit.WEEKS);

        assertNotEquals(ExpandTimeWindow.NONE, aggregationFunction.getTimeWindowExpansion());
        Pair<Duration, Duration> expectedQueryExpansionRange = ImmutablePair.of(
                Duration.of(2 * 7, ChronoUnit.DAYS), Duration.of(7, ChronoUnit.DAYS));
        assertEquals(expectedQueryExpansionRange, aggregationFunction.getTimeWindowExpansion());
    }
}
