package cern.nxcals.api.custom.service.fill;

import cern.nxcals.api.domain.TimeWindow;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.NavigableMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FillProcessorTest {

    private final FillProcessor processor = new FillProcessor();

    @Test
    public void shouldThrowOnNullInput() {
        assertThrows(NullPointerException.class, () ->  processor.process(null));
    }

    @Test
    public void shouldNotProcessEmptyInput() {
        NavigableMap<TimeWindow, Integer> fills = Collections.emptyNavigableMap();

        NavigableMap<TimeWindow, Integer> result = processor.process(fills);
        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    @Test
    public void shouldProcessWithoutModifyingSingleEntryInput() {
        NavigableMap<TimeWindow, Integer> fills = this.newDataMap();
        fills.put(TimeWindow.between(10, 20), 1);

        NavigableMap<TimeWindow, Integer> result = processor.process(fills);
        assertNotNull(result);
        assertEquals(1, result.size());

        assertEquals(1, result.get(TimeWindow.between(10, 20)).intValue());
    }

    @Test
    public void shouldProcessWithoutModifyingInputWhenNoDuplicateFills() {
        NavigableMap<TimeWindow, Integer> fills = this.newDataMap();

        fills.put(TimeWindow.between(0, 10), 1);
        fills.put(TimeWindow.between(10, 20), 2);
        fills.put(TimeWindow.between(20, 30), 3);
        fills.put(TimeWindow.between(30, 40), 5);
        fills.put(TimeWindow.between(45, 50), 4); // 4 sent intentionally after 5

        NavigableMap<TimeWindow, Integer> result = processor.process(fills);
        assertNotNull(result);
        assertEquals(5, result.size());

        assertEquals(1, result.get(TimeWindow.between(0, 10)).intValue());
        assertEquals(2, result.get(TimeWindow.between(10, 20)).intValue());
        assertEquals(3, result.get(TimeWindow.between(20, 30)).intValue());
        assertEquals(5, result.get(TimeWindow.between(30, 40)).intValue());
        assertEquals(4, result.get(TimeWindow.between(45, 50)).intValue());
    }

    @Test
    public void shouldMergeAdjacentEntriesOnInputStart() {
        NavigableMap<TimeWindow, Integer> fills = this.newDataMap();

        fills.put(TimeWindow.between(10, 20), 1);
        fills.put(TimeWindow.between(20, 30), 1);
        fills.put(TimeWindow.between(30, 35), 1);
        fills.put(TimeWindow.between(35, 40), 2);

        NavigableMap<TimeWindow, Integer> result = processor.process(fills);
        assertNotNull(result);
        assertEquals(2, result.size());

        assertEquals(1, result.get(TimeWindow.between(10, 35)).intValue());
        assertEquals(2, result.get(TimeWindow.between(35, 40)).intValue());
    }

    @Test
    public void shouldMergeAdjacentEntriesOnInputEnd() {
        NavigableMap<TimeWindow, Integer> fills = this.newDataMap();

        fills.put(TimeWindow.between(0, 10), 1);
        fills.put(TimeWindow.between(10, 20), 2);
        fills.put(TimeWindow.between(20, 30), 2);
        fills.put(TimeWindow.between(30, 35), 2);

        NavigableMap<TimeWindow, Integer> result = processor.process(fills);
        assertNotNull(result);
        assertEquals(2, result.size());

        assertEquals(1, result.get(TimeWindow.between(0, 10)).intValue());
        assertEquals(2, result.get(TimeWindow.between(10, 35)).intValue());
    }

    private NavigableMap<TimeWindow, Integer> newDataMap() {
        return FillUtils.navigableMapFrom(Collections.emptyMap());
    }

}
