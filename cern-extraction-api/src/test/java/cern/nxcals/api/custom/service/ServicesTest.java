package cern.nxcals.api.custom.service;

import cern.nxcals.api.config.SparkProperties;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.utils.SparkUtils;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;

class ServicesTest {

    @Test
    void shouldThrowWhenUserProvidedSupplierReturnsNull() {
        Supplier<SparkSession> nullSupplier = () -> null;
        Supplier<SparkSession> enhancedSupplier = Services.addNullCheckToSupplier(nullSupplier);
        assertThrows(IllegalArgumentException.class, enhancedSupplier::get);
    }

    @Test
    void shouldCreateServicesWithDefaultSparkSessionSupplier() {
        try (MockedStatic<SparkUtils> utilities = Mockito.mockStatic(SparkUtils.class)) {
            Services.newInstance();
            utilities.verify(() -> SparkUtils.createSparkSessionSupplier(any(SparkProperties.class)));
        }
    }

    @Test
    void shouldCreateExtractionService() {
        Services.newInstance().extractionService();
    }

    @Test
    void shouldCreateFillService() {
        Services.newInstance().fillService();
    }

    @Test
    void shouldCreateFundamentalService() {
        try (MockedStatic<ServiceClientFactory> serviceClientFactory = Mockito.mockStatic(ServiceClientFactory.class)) {
            VariableService variableServiceMock = mock(VariableService.class);
            serviceClientFactory.when(() -> ServiceClientFactory.createVariableService())
                    .thenReturn(variableServiceMock);
            Services.newInstance().fundamentalService();
        }
    }

    @Test
    void shouldCreateAggregationService() {
        try (MockedStatic<ServiceClientFactory> serviceClientFactory = Mockito.mockStatic(ServiceClientFactory.class)) {
            VariableService variableServiceMock = mock(VariableService.class);
            serviceClientFactory.when(() -> ServiceClientFactory.createVariableService())
                    .thenReturn(variableServiceMock);
            Services.newInstance().aggregationService();
        }
    }

}