package cern.nxcals.api.custom.service.extraction;

import cern.nxcals.api.domain.TimeWindow;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

import static cern.nxcals.api.custom.service.TestUtils.buildDataset;
import static cern.nxcals.api.custom.service.extraction.LookupStrategy.LAST_BEFORE_START;
import static cern.nxcals.api.custom.service.extraction.LookupStrategy.LAST_BEFORE_START_IF_EMPTY;
import static cern.nxcals.api.custom.service.extraction.LookupStrategy.NEXT_AFTER_START_ONLY;
import static cern.nxcals.api.domain.TimeWindow.between;
import static java.time.Duration.ofDays;
import static java.time.temporal.ChronoUnit.DAYS;
import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LookupStrategyTest {
    private static final StructType DS_SCHEMA = createSchema();

    private static final String TIMESTAMP_FIELD = "timestamp";

    private static final Supplier<TimeWindow> LOOKUP_TIME_WINDOW_SUPPLIER = () -> between(0L, 1L);

    private static StructType createSchema() {
        StructField timestamp = new StructField(TIMESTAMP_FIELD, DataTypes.LongType, true, Metadata.empty());
        StructField field = new StructField("field", DataTypes.LongType, true, Metadata.empty());
        return new StructType(new StructField[] { timestamp, field });
    }

    @Mock
    private Function<TimeWindow, Dataset<Row>> dataProvider;

    @Test
    void shouldThrowOnExtendLookupWhenUnitIsNull() {
        assertThrows(NullPointerException.class, () -> LAST_BEFORE_START.withLookupDuration(1, null));
    }

    @Test
    void lastBeforeStartShouldEnhanceDatasetWithLookupData() {
        Dataset<Row> lookupDataset = buildDataset(DS_SCHEMA, new Object[][] { { 1L, 1L } });
        Dataset<Row> dataset = buildDataset(DS_SCHEMA, new Object[][] { { 2L, 1L }, { 3L, 1L } });

        Instant from = Instant.ofEpochSecond(0L, 2L);
        Instant to = Instant.ofEpochSecond(0L, 3L);
        ExtractionProperties properties = ExtractionProperties.builder().lookupStrategy(LAST_BEFORE_START)
                .timeWindow(from, to).build();

        when(dataProvider.apply(between(0L, 1L))).thenReturn(lookupDataset);
        when(dataProvider.apply(between(1L, 3L))).thenReturn(lookupDataset.union(dataset));

        List<Long> rows = properties.getLookupStrategy().apply(dataProvider, TIMESTAMP_FIELD, properties)
                .select(TIMESTAMP_FIELD).collectAsList().stream().map(r -> r.getLong(0)).collect(toList());
        assertEquals(3, rows.size());
        assertEquals(Arrays.asList(1L, 2L, 3L), rows);
    }

    @Test
    void lastBeforeStartIfEmptyShouldNotEnhanceDatasetWithLookupData() {
        LookupStrategy strategy = LAST_BEFORE_START_IF_EMPTY;

        Dataset<Row> dataset = buildDataset(DS_SCHEMA, new Object[][] { { 2L, 1L }, { 3L, 1L } });

        Instant from = Instant.ofEpochSecond(0L, 2L);
        Instant to = Instant.ofEpochSecond(0L, 3L);
        ExtractionProperties properties = ExtractionProperties.builder().lookupStrategy(strategy).timeWindow(from, to)
                .build();

        when(dataProvider.apply(between(2L, 3L))).thenReturn(dataset);

        List<Long> rows = strategy.apply(dataProvider, TIMESTAMP_FIELD, properties).select(TIMESTAMP_FIELD)
                .collectAsList().stream().map(r -> r.getLong(0)).collect(toList());
        assertEquals(2, rows.size());
        assertEquals(Arrays.asList(2L, 3L), rows);
    }

    @Test
    void lastBeforeStartIfEmptyWhenDatasetEmptyShouldReturnOnlyLookupData() {

        Dataset<Row> lookupDataset = buildDataset(DS_SCHEMA, new Object[][] { { 1L, 1L } });
        Dataset<Row> empty = buildDataset(DS_SCHEMA); //empty

        Instant from = Instant.ofEpochSecond(0L, 2L);
        Instant to = Instant.ofEpochSecond(0L, 3L);
        ExtractionProperties properties = ExtractionProperties.builder().lookupStrategy(LAST_BEFORE_START_IF_EMPTY)
                .timeWindow(from, to).build();

        doAnswer(a -> {
            TimeWindow tw = a.getArgument(0);
            if (properties.getTimeWindow().equals(tw)) {
                return empty;
            }
            return lookupDataset;
        }).when(dataProvider).apply(any());

        List<Long> rows = properties.getLookupStrategy().apply(dataProvider, TIMESTAMP_FIELD, properties)
                .select(TIMESTAMP_FIELD).collectAsList().stream().map(r -> r.getLong(0)).collect(toList());
        assertEquals(1, rows.size());
        assertEquals(Collections.singletonList(1L), rows);
    }

    @Test
    void NoneShouldReturnPlainDataset() {
        LookupStrategy strategy = LookupStrategy.NONE;

        Dataset<Row> dataset = buildDataset(DS_SCHEMA, new Object[][] { { 2L, 1L }, { 3L, 1L } });

        Instant from = Instant.ofEpochSecond(0L, 2L);
        Instant to = Instant.ofEpochSecond(0L, 3L);
        ExtractionProperties properties = ExtractionProperties.builder().timeWindow(from, to).build();

        when(dataProvider.apply(properties.getTimeWindow())).thenReturn(dataset);

        List<Long> rows = strategy.apply(dataProvider, TIMESTAMP_FIELD, properties).select(TIMESTAMP_FIELD)
                .collectAsList().stream().map(r -> r.getLong(0)).collect(toList());
        assertEquals(2, rows.size());
        assertEquals(List.of(2L, 3L), rows);
    }

    @Test
    void nextAfterStartOnly() {
        Dataset<Row> dataset = buildDataset(DS_SCHEMA, new Object[][] { { 3L, 1L }, { 4L, 1L } });
        Dataset<Row> singleRowDS = buildDataset(DS_SCHEMA, new Object[][] { { 3L, 1L }});

        Instant from = Instant.ofEpochSecond(0L, 1L);
        Instant to = Instant.ofEpochSecond(0L, 2L);
        ExtractionProperties properties = ExtractionProperties.builder().timeWindow(from, to)
                .lookupStrategy(NEXT_AFTER_START_ONLY).build();

        when(dataProvider.apply(between(from.plusNanos(1), to.truncatedTo(DAYS).plus(ofDays(1L))))).thenReturn(dataset);
        when(dataProvider.apply(between(2, 3))).thenReturn(singleRowDS);

        List<Long> rows = properties.getLookupStrategy().apply(dataProvider, TIMESTAMP_FIELD, properties)
                .select(TIMESTAMP_FIELD).collectAsList().stream().map(r -> r.getLong(0)).collect(toList());
        assertEquals(1, rows.size());
        assertEquals(List.of(3L), rows);

    }
}
