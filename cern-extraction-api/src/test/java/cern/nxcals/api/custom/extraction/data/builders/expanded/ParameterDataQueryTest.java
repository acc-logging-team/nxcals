package cern.nxcals.api.custom.extraction.data.builders.expanded;

import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.extraction.data.builders.fluent.v2.DeviceStage;
import cern.nxcals.api.extraction.data.builders.fluent.v2.SystemStage;
import cern.nxcals.api.extraction.metadata.InternalServiceClientFactory;
import cern.nxcals.common.domain.ExtractionUnit;
import cern.nxcals.internal.extraction.metadata.InternalEntityResourceService;
import org.apache.spark.SparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Instant;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * This class is just a set of not so useful tests, to make Sonar happy.
 * The real tests are in the integration module.
 */
@ExtendWith(MockitoExtension.class)
class ParameterDataQueryTest {

    @Mock
    private SparkSession spark;

    @Mock
    private SparkContext sparkContext;

    @Mock
    private InternalEntityResourceService resourceService;

    @Test
    void builder() {
        SystemStage<DeviceStage<List<Dataset<Row>>>, List<Dataset<Row>>> builder = ParameterDataQuery.builder(spark);
        assertNotNull(builder);
    }

    @Test
    void shouldThrowIfSparkSessionIsNullOnGetFor() {
        TimeWindow timeWindow = TimeWindow.between(Instant.ofEpochSecond(10), Instant.ofEpochSecond(50));
        assertThrows(NullPointerException.class, () -> ParameterDataQuery.getFor(null, "cmw",
                "testDevice", "testProperty", TimeWindow.empty()));
    }

    @Test
    void shouldThrowIfSystemIsNullOnGetFor() {

        TimeWindow timeWindow = TimeWindow.between(Instant.ofEpochSecond(10), Instant.ofEpochSecond(50));
        assertThrows(NullPointerException.class, () -> ParameterDataQuery.getFor(spark,
                null, "testDevice", "testProperty", TimeWindow.empty()));
    }

    @Test
    void shouldThrowIfSystemIsNullOnGetForParameter() {
        TimeWindow timeWindow = TimeWindow.between(Instant.ofEpochSecond(10), Instant.ofEpochSecond(50));
        assertThrows(NullPointerException.class, () -> ParameterDataQuery.getFor(null,
                "cmw", "testDevice/testProperty", TimeWindow.empty()));
    }

    @Test
    void shouldNotConnect() {
        assertThrows(RuntimeException.class,
                () -> ParameterDataQuery.getFor(spark, "CMW", "DEV/PROP", TimeWindow.empty()));
    }

    @Test
    void shouldGetAListOfDatasetsForParameter() {
        try (MockedStatic<InternalServiceClientFactory> factory = Mockito
                .mockStatic(InternalServiceClientFactory.class)) {
            factory.when(InternalServiceClientFactory::createEntityResourceService).thenReturn(resourceService);

            when(resourceService.findBy(argThat(x -> true))).thenReturn(mock(ExtractionUnit.class));
            when(spark.sparkContext()).thenReturn(sparkContext);
            when(sparkContext.appName()).thenReturn("app");

            assertNotNull(ParameterDataQuery.getFor(spark, "CMW", "DEV/PROP", TimeWindow.empty()));

        }

    }

    @Test
    void shouldGetAListOfDatasetsForDevProp() {
        try (MockedStatic<InternalServiceClientFactory> factory = Mockito
                .mockStatic(InternalServiceClientFactory.class)) {
            factory.when(InternalServiceClientFactory::createEntityResourceService).thenReturn(resourceService);

            when(resourceService.findBy(argThat(x -> true))).thenReturn(mock(ExtractionUnit.class));
            when(spark.sparkContext()).thenReturn(sparkContext);
            when(sparkContext.appName()).thenReturn("app");

            assertNotNull(ParameterDataQuery.getFor(spark, "CMW", "DEV", "PROP", TimeWindow.empty()));

        }

    }

}