package cern.nxcals.api.custom.service.fill;

import cern.nxcals.api.custom.service.fill.FillProvider.Source;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.utils.TimeUtils;
import com.google.common.collect.ImmutableList;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.Map;
import java.util.NavigableMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import static cern.nxcals.api.custom.service.fill.FillProvider.FIRST_FILL;
import static cern.nxcals.common.SystemFields.NXC_EXTR_TIMESTAMP;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VALUE;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class FillProviderTest {

    @Mock
    private SparkSession session;

    private FillProvider fillProvider;

    @BeforeEach
    public void setUp() {
        fillProvider = Mockito.spy(new FillProvider(() -> session));
    }

    @Test
    public void shouldWorkForEmptyData() {
        doReturn(Collections.emptyList()).when(fillProvider).extractData(any(), any(), any());

        assertTrue(fillProvider.findFillsByRange(Instant.ofEpochMilli(100L), Instant.ofEpochMilli(200L)).isEmpty());
        TimeWindow.between(100, 200);
        assertFalse(fillProvider.findBeamModesByTimeWindows(Collections.singleton(TimeWindow.infinite())).isEmpty());
    }

    @Test
    public void getFillsAndBeamModesInTimeWindow() {
        Instant from = TimeUtils.getInstantFromNanos(100);
        Instant to = TimeUtils.getInstantFromNanos(500);

        TimeWindow fetchTimeWindow = TimeWindow.between(from, to);

        doReturn(ImmutableList.of(
                mockPaddingRow(1),
                mockRow(100, 2),
                mockRow(200, 3),
                mockRow(300, 4))
        ).when(fillProvider).extractData(any(), any(), eq(Source.FILL));

        doReturn(ImmutableList.of(
                mockRow(0, "X"),
                mockRow(100, "A"),
                mockRow(150, "B"),
                mockRow(180, "C"),
                mockRow(280, "D"))
        ).when(fillProvider).extractData(any(), any(), eq(Source.BEAM_MODE));

        NavigableMap<TimeWindow, Integer> foundFills = fillProvider.findFillsByRange(from, to);
        assertEquals(3, foundFills.size());

        Integer fillNumber = foundFills.get(TimeWindow.between(100,200));
        assertNotNull(fillNumber);
        assertEquals(2, fillNumber.intValue());

        fillNumber = foundFills.get(TimeWindow.between(TimeUtils.getInstantFromNanos(300),null));
        assertNotNull(fillNumber);
        assertEquals(4, fillNumber.intValue());

        Map<TimeWindow, NavigableMap<TimeWindow, String>> modesPerFetchTimeWindow = fillProvider
                .findBeamModesByTimeWindows(Collections.singleton(fetchTimeWindow));
        NavigableMap<TimeWindow, String> foundModes = modesPerFetchTimeWindow.get(fetchTimeWindow);
        assertEquals(4, foundModes.size());
        String[] expectedModes = { "A", "B", "C", "D" };
        assertArrayEquals(expectedModes, foundModes.values().toArray(new String[0]));
    }

    @Test
    public void shouldNotLoadTestFills() {
        doReturn(ImmutableList.of(
                mockPaddingRow(0),
                mockPaddingRow(1),
                mockRow(300, 2),
                mockRow(400, 3),
                mockRow(500, 4))
        ).when(fillProvider).extractData(any(), any(), eq(Source.FILL));

        Instant from = Instant.ofEpochMilli(0);
        Instant to = Instant.ofEpochMilli(500);
        NavigableMap<TimeWindow, Integer> fills = fillProvider.findFillsByRange(from, to);
        assertEquals(3, fills.size());

        assertNull(fills.get(TimeWindow.between(100, 200)));
        assertNull(fills.get(TimeWindow.between(200, 300)));
        assertEquals(2, (int) fills.get(TimeWindow.between(300, 400)));
        assertEquals(3, (int) fills.get(TimeWindow.between(400, 500)));
        assertTrue(fills.containsValue(4));
    }

    @Test
    public void shouldEnsureFillProviderIsThreadSafe() throws InterruptedException {
        Instant from = FIRST_FILL;
        Instant to = from.plus(1, ChronoUnit.DAYS);

        doReturn(ImmutableList.of(
                mockPaddingRow(1),
                mockRow(TimeUtils.getNanosFromInstant(from), 2),
                mockRow(TimeUtils.getNanosFromInstant(to), 3),
                mockRow(TimeUtils.getNanosFromInstant(to.plus(10, ChronoUnit.DAYS)), 4))
        ).when(fillProvider).extractData(any(), any(), eq(Source.FILL));

        CountDownLatch threadBlockLatch = new CountDownLatch(1);

        int numberOfThreads = 40;
        ExecutorService executorService = Executors.newFixedThreadPool(numberOfThreads);
        for (int i = 0; i < (numberOfThreads * 3); i++) {
            executorService.submit(() -> {
                try {
                    threadBlockLatch.await(1, TimeUnit.MINUTES); // block and collect all threads here
                } catch (InterruptedException e) {
                    fail(e.getMessage());
                }
                fillProvider.findFillsByRange(from, to); // entry point to test thread safety
            });
        }
        threadBlockLatch.countDown();

        // extra invocation to ensure that component internals were not scrambled by parallel access
        NavigableMap<TimeWindow, Integer> foundFills = fillProvider.findFillsByRange(from, to);

        Integer fillNumber = foundFills.get(TimeWindow.between(from, to));
        assertNotNull(fillNumber);
        assertEquals(2, fillNumber.intValue());

        // if thread safety not correctly implemented some of the actions inside fillProvider, would be invoked
        // more times than they are supposed to! We have the following verification to test against that
        verify(fillProvider, times(1)).extractData(any(), any(), eq(Source.FILL));
    }

    // helper methods

    // this row is intended to verify that only actual rows would be selected and not this one (based on target value)
    private <T> Row mockPaddingRow(T value) {
        return mockRow(row -> {
            doReturn(value).when(row).getAs(NXC_EXTR_VALUE.getValue());
        });
    }

    private <T> Row mockRow(long timestamp, T value) {
        return mockRow(row -> {
            doReturn(timestamp).when(row).getAs(NXC_EXTR_TIMESTAMP.getValue());
            doReturn(value).when(row).getAs(NXC_EXTR_VALUE.getValue());
        });
    }

    private <T> Row mockRow(Consumer<Row> mockingContextConfig) {
        Row row = mock(Row.class);
        mockingContextConfig.accept(row);
        return row;
    }

}
