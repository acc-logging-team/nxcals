package cern.nxcals.api.custom.service.extraction;

import cern.nxcals.api.custom.domain.util.DataLookupUtils;
import cern.nxcals.api.custom.service.TestSparkSession;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.utils.TimeUtils;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.lit;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DataLookupUtilsTest {
    private static final SparkSession SPARK_SESSION = TestSparkSession.JUNIT.get();

    private static final StructType TEST_SCHEMA = createSchema();

    private static final String TIMESTAMP_FIELD = "timestamp";
    private static final Duration LOOKUP_DURATION = Duration.of(2, ChronoUnit.DAYS);
    private static final TimeWindow TIME_WINDOW = TimeWindow.between(Instant.ofEpochSecond(10),
            Instant.ofEpochSecond(20));
    private static final Function<TimeWindow, Dataset<Row>> DATASET_PROVIDER = tw -> buildDataset(TEST_SCHEMA, tw);

    private static StructType createSchema() {
        StructField timestampField = new StructField(TIMESTAMP_FIELD, DataTypes.LongType, true, Metadata.empty());
        StructField valueField = new StructField("value", DataTypes.LongType, true, Metadata.empty());
        return new StructType(new StructField[] { timestampField, valueField });
    }

    private static Dataset<Row> buildDataset(StructType schema, TimeWindow queryTimeWindow) {
        Object[] r1 = { TimeUtils.getNanosFromInstant(TIME_WINDOW.getStartTime()), 1L };
        Object[] r2 = { TimeUtils.getNanosFromInstant(TIME_WINDOW.getEndTime()), 2L };
        List<Row> rows = new ArrayList<>();
        for (Object[] rowData : Arrays.asList(r1, r2)) {
            rows.add(RowFactory.create(rowData));
        }
        return SPARK_SESSION.createDataFrame(rows, schema)
                .where(col(TIMESTAMP_FIELD).between(
                        lit(queryTimeWindow.getStartTimeNanos()), lit(queryTimeWindow.getEndTimeNanos())));
    }

    // getAdjacentTimestampBefore

    @Test
    public void shouldThrowWhenGetAdjacentTimestampBeforeWhenBeginTimestampIsNull() {
        assertThrows(NullPointerException.class, () ->
                DataLookupUtils.getAdjacentTimestampBefore(null, DATASET_PROVIDER, TIMESTAMP_FIELD, LOOKUP_DURATION));
    }

    @Test
    public void shouldThrowWhenGetAdjacentTimestampBeforeWhenProviderIsNull() {
        assertThrows(NullPointerException.class, () ->
                DataLookupUtils.getAdjacentTimestampBefore(TIME_WINDOW.getEndTime(), null, TIMESTAMP_FIELD, LOOKUP_DURATION));
    }

    @Test
    public void shouldThrowWhenGetAdjacentTimestampBeforeWhenTimestampFieldIsNull() {
        assertThrows(NullPointerException.class, () ->
                DataLookupUtils.getAdjacentTimestampBefore(TIME_WINDOW.getEndTime(), DATASET_PROVIDER, null, LOOKUP_DURATION));
    }

    @Test
    public void shouldThrowWhenGetAdjacentTimestampBeforeWhenLookupDurationIsNull() {
        assertThrows(NullPointerException.class, () ->
                DataLookupUtils.getAdjacentTimestampBefore(TIME_WINDOW.getEndTime(), DATASET_PROVIDER, TIMESTAMP_FIELD, null));
    }

    @Test
    public void shouldReturnEmptyOnGetAdjacentTimestampBeforeIfLookupDurationIsZero() {
        Optional<Instant> timestampBefore = DataLookupUtils
                .getAdjacentTimestampBefore(TIME_WINDOW.getEndTime(), DATASET_PROVIDER, TIMESTAMP_FIELD, Duration.ZERO);
        assertFalse(timestampBefore.isPresent());
    }

    @Test
    public void shouldReturnEmptyOnGetAdjacentTimestampBeforeIfTimestampIsOutsideTheLookupDuration() {
        Optional<Instant> timestampBefore = DataLookupUtils
                .getAdjacentTimestampBefore(TIME_WINDOW.getEndTime(), DATASET_PROVIDER, TIMESTAMP_FIELD,
                        Duration.of(1, ChronoUnit.MILLIS));
        assertFalse(timestampBefore.isPresent());
    }

    @Test
    public void shouldReturnEmptyIfNoAdjacentBeforeProvidedTimestamp() {
        Optional<Instant> timestampBefore = DataLookupUtils
                .getAdjacentTimestampBefore(TIME_WINDOW.getStartTime().minusSeconds(1), DATASET_PROVIDER,
                        TIMESTAMP_FIELD, LOOKUP_DURATION);
        assertFalse(timestampBefore.isPresent());
    }

    @Test
    public void shouldReturnAdjacentTimestampBeforeProvidedTimestamp() {
        Optional<Instant> timestampBefore = DataLookupUtils
                .getAdjacentTimestampBefore(TIME_WINDOW.getEndTime().plusSeconds(1), DATASET_PROVIDER, TIMESTAMP_FIELD,
                        LOOKUP_DURATION);
        assertTrue(timestampBefore.isPresent());
        Instant before = timestampBefore.get();
        assertEquals(TIME_WINDOW.getEndTime(), before);
    }

    // getAdjacentTimestampAfter

    @Test
    public void shouldThrowWhenGetAdjacentTimestampAfterWhenBeginTimestampIsNull() {
        assertThrows(NullPointerException.class, () ->
                DataLookupUtils.getAdjacentTimestampAfter(null, DATASET_PROVIDER, TIMESTAMP_FIELD, LOOKUP_DURATION));
    }

    @Test
    public void shouldThrowWhenGetAdjacentTimestampAfterWhenProviderIsNull() {
        assertThrows(NullPointerException.class, () ->
                DataLookupUtils.getAdjacentTimestampAfter(TIME_WINDOW.getStartTime(), null, TIMESTAMP_FIELD, LOOKUP_DURATION));
    }

    @Test
    public void shouldThrowWhenGetAdjacentTimestampAfterWhenTimestampFieldIsNull() {
        assertThrows(NullPointerException.class, () ->
                DataLookupUtils.getAdjacentTimestampAfter(TIME_WINDOW.getStartTime(), DATASET_PROVIDER, null, LOOKUP_DURATION));
    }

    @Test
    public void shouldThrowWhenGetAdjacentTimestampAfterWhenLookupDurationIsNull() {
        assertThrows(NullPointerException.class, () ->
                DataLookupUtils.getAdjacentTimestampAfter(TIME_WINDOW.getStartTime(), DATASET_PROVIDER, TIMESTAMP_FIELD, null));
    }

    @Test
    public void shouldReturnEmptyOnGetAdjacentTimestampAfterIfLookupDurationIsZero() {
        Optional<Instant> timestampAfter = DataLookupUtils
                .getAdjacentTimestampAfter(TIME_WINDOW.getStartTime(), DATASET_PROVIDER, TIMESTAMP_FIELD,
                        Duration.ZERO);
        assertFalse(timestampAfter.isPresent());
    }

    @Test
    public void shouldReturnEmptyOnGetAdjacentTimestampAfterIfTimestampIsOutsideTheLookupDuration() {
        Optional<Instant> timestampAfter = DataLookupUtils
                .getAdjacentTimestampAfter(TIME_WINDOW.getStartTime(), DATASET_PROVIDER, TIMESTAMP_FIELD,
                        Duration.of(1, ChronoUnit.MILLIS));
        assertFalse(timestampAfter.isPresent());
    }

    @Test
    public void shouldReturnEmptyOnGetAdjacentTimestampAfterIfNoAdjacentAfterProvidedTimestamp() {
        Optional<Instant> timestampAfter = DataLookupUtils
                .getAdjacentTimestampAfter(TIME_WINDOW.getEndTime().plusSeconds(1), DATASET_PROVIDER, TIMESTAMP_FIELD,
                        LOOKUP_DURATION);
        assertFalse(timestampAfter.isPresent());
    }

    @Test
    public void shouldReturnAdjacentTimestampAfterProvidedTimestamp() {
        Optional<Instant> timestampAfter = DataLookupUtils
                .getAdjacentTimestampAfter(TIME_WINDOW.getStartTime().plusSeconds(1), DATASET_PROVIDER, TIMESTAMP_FIELD,
                        LOOKUP_DURATION);
        assertTrue(timestampAfter.isPresent());
        Instant after = timestampAfter.get();
        assertEquals(TIME_WINDOW.getEndTime(), after);
    }

}
