package cern.nxcals.api.custom.service.aggregation;

import avro.shaded.com.google.common.collect.Sets;
import cern.nxcals.api.custom.domain.FundamentalFilter;
import cern.nxcals.api.custom.domain.util.QueryDatasetProvider;
import cern.nxcals.api.custom.service.AggregationService;
import cern.nxcals.api.custom.service.FundamentalService;
import cern.nxcals.api.custom.service.TestSparkSession;
import cern.nxcals.api.custom.service.fundamental.FundamentalContext;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.Partition;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableDeclaredType;
import cern.nxcals.api.utils.TimeUtils;
import org.apache.avro.SchemaBuilder;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static cern.nxcals.api.custom.service.TestUtils.buildDataset;
import static cern.nxcals.api.custom.service.aggregation.AggregationConstants.AGGREGATION_TIMESTAMP_FIELD;
import static cern.nxcals.api.custom.service.aggregation.AggregationConstants.AGGREGATION_VALUE_FIELD;
import static cern.nxcals.common.SystemFields.NXC_EXTR_TIMESTAMP;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VALUE;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VARIABLE_NAME;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AggregationServiceTest {
    private static final Supplier<SparkSession> SESSION_SUPPLIER = TestSparkSession.JUNIT;

    private static final Instant START = TimeUtils.getInstantFromString("2020-01-01 00:00:00.000");
    private static final String ENTITY_TIMESTAMP_FIELD_NAME = "__record_timestamp__";
    private static final String ENTITY_DATA_FIELD_NAME = "entityField";

    private static final StructType VARIABLE_SCHEMA = createVariableSchema();
    private static final StructType ENTITY_SCHEMA = createEntitySchema();

    private static final StructType FUNDAMENTAL_VARIABLE_SCHEMA = createFundamentalVariableSchema();

    private static final String SYSTEM_TIME_KEY_STRING = SchemaBuilder.record("test").fields()
            .name(ENTITY_TIMESTAMP_FIELD_NAME).type().longType().noDefault().endRecord().toString();

    private static final SystemSpec SYSTEM = SystemSpec.builder()
            .timeKeyDefinitions(SYSTEM_TIME_KEY_STRING)
            .partitionKeyDefinitions("")
            .entityKeyDefinitions("")
            .name("TEST_SYSTEM")
            .build();

    private static final Partition PARTITION = Partition.builder()
            .keyValues(Collections.emptyMap())
            .systemSpec(SYSTEM)
            .build();

    private static StructType createVariableSchema() {
        StructField timestampField = new StructField(NXC_EXTR_TIMESTAMP.getValue(), DataTypes.LongType, true,
                Metadata.empty());
        StructField valueField = new StructField(NXC_EXTR_VALUE.getValue(), DataTypes.LongType, true, Metadata.empty());
        return new StructType(new StructField[] { timestampField, valueField });
    }

    private static StructType createEntitySchema() {
        StructField timestampField = new StructField(ENTITY_TIMESTAMP_FIELD_NAME, DataTypes.LongType, true,
                Metadata.empty());
        StructField valueField = new StructField(ENTITY_DATA_FIELD_NAME, DataTypes.LongType, true, Metadata.empty());
        return new StructType(new StructField[] { timestampField, valueField });
    }

    private static StructType createFundamentalVariableSchema() {
        StructField timestampField = new StructField(NXC_EXTR_TIMESTAMP.getValue(), DataTypes.LongType, true,
                Metadata.empty());
        StructField cycleField = new StructField(FundamentalContext.VIRTUAL_LSA_CYCLE_FIELD, DataTypes.StringType, true,
                Metadata.empty());
        StructField userField = new StructField(FundamentalContext.USER_FIELD, DataTypes.StringType, true,
                Metadata.empty());
        StructField destinationField = new StructField(FundamentalContext.DESTINATION_FIELD, DataTypes.StringType, true,
                Metadata.empty());
        StructField variableNameField = new StructField(NXC_EXTR_VARIABLE_NAME.getValue(), DataTypes.StringType, true,
                Metadata.empty());
        return new StructType(
                new StructField[] { timestampField, cycleField, userField, destinationField, variableNameField });
    }

    private static final Set<String> EXPECTED_COLUMNS = Sets
            .newHashSet(AGGREGATION_TIMESTAMP_FIELD, AGGREGATION_VALUE_FIELD);

    @Mock
    private QueryDatasetProvider<Variable> variableDataProvider;
    @Mock
    private QueryDatasetProvider<Entity> entityDataProvider;

    @Mock
    private FundamentalService fundamentalService;

    private AggregationService aggregationService;

    @BeforeEach
    public void setUp() {
        aggregationService = new AggregationServiceImpl(SESSION_SUPPLIER, variableDataProvider,
                entityDataProvider, fundamentalService);
    }

    @Test
    public void shouldThrowOnGetDataForVariableWhenWindowPropertiesIsNull() {
        WindowAggregationProperties properties = null;
        assertThrows(NullPointerException.class, () -> aggregationService.getData(getVariable(), properties));
    }

    @Test
    public void shouldThrowOnGetDataForVariableWhenDatasetPropertiesIsNull() {
        DatasetAggregationProperties properties = null;
        assertThrows(NullPointerException.class, () -> aggregationService.getData(getVariable(), properties));
    }

    @Test
    public void shouldThrowOnGetDataForEntityWhenWindowPropertiesIsNull() {
        WindowAggregationProperties properties = null;
        assertThrows(NullPointerException.class, () -> aggregationService.getData(getEntity(), properties));
    }

    @Test
    public void shouldThrowOnGetDataForEntityWhenDatasetPropertiesIsNull() {
        DatasetAggregationProperties properties = null;
        assertThrows(NullPointerException.class, () -> aggregationService.getData(getEntity(), properties));
    }

    @Test
    public void shouldGetDataForVariableWhenDatasetContainsNullValues() {
        Variable variable = getVariable();
        WindowAggregationProperties properties = WindowAggregationProperties.builder()
                .timeWindow(START, START.plus(1, ChronoUnit.HOURS))
                .interval(20, ChronoUnit.SECONDS)
                .function(AggregationFunctions.MAX)
                .build();
        when(variableDataProvider.get(any(), any(), eq(variable)))
                .thenReturn(getDatasetWithNullEntries(VARIABLE_SCHEMA));
        Dataset<Row> result = aggregationService
                .getData(variable, properties);

        long expectedResultSize = 3600 / 20;
        assertResult(result, expectedResultSize, Arrays.asList(2L, 3L), Duration.of(20, ChronoUnit.SECONDS));
    }

    @Test
    public void shouldGetEmptyPlaceholderDatasetWithIntervalsForVariableWhenDatasetEmpty() {
        Variable variable = getVariable();
        WindowAggregationProperties properties = WindowAggregationProperties.builder()
                .timeWindow(START, START.plus(1, ChronoUnit.HOURS))
                .interval(20, ChronoUnit.SECONDS)
                .function(AggregationFunctions.INTERPOLATE)
                .build();
        when(variableDataProvider.get(any(), any(), eq(variable)))
                .thenReturn(getEmptyDataset(VARIABLE_SCHEMA));
        Dataset<Row> result = aggregationService
                .getData(variable, properties);

        long expectedResultSize = 3600 / 20;
        assertResult(result, expectedResultSize, Collections.emptyList(), Duration.of(20, ChronoUnit.SECONDS));
    }

    @Test
    public void shouldGetDataForVariable() {
        Variable variable = getVariable();
        WindowAggregationProperties properties = WindowAggregationProperties.builder()
                .timeWindow(START, START.plus(1, ChronoUnit.HOURS))
                .interval(20, ChronoUnit.SECONDS)
                .function(AggregationFunctions.MAX)
                .build();
        when(variableDataProvider.get(any(), any(), eq(variable)))
                .thenReturn(getDataset(VARIABLE_SCHEMA));
        Dataset<Row> result = aggregationService
                .getData(variable, properties);
        long expectedResultSize = 3600 / 20;
        assertResult(result, expectedResultSize, Arrays.asList(1L, 2L, 3L), Duration.of(20, ChronoUnit.SECONDS));
    }

    @Test
    public void shouldGetDataForVariableFilteredByFundamentals() {
        Variable variable = getVariable();
        Set<FundamentalFilter> fundamentalFilters = Collections.singleton(
                FundamentalFilter.builder().accelerator("TEST").lsaCycle("TEST_CYCLE").build());
        WindowAggregationProperties properties = WindowAggregationProperties.builder()
                .timeWindow(START, START.plus(1, ChronoUnit.HOURS))
                .interval(20, ChronoUnit.SECONDS)
                .function(AggregationFunctions.MAX)
                .fundamentalFilters(fundamentalFilters)
                .build();
        when(variableDataProvider.get(any(), any(), eq(variable))).thenReturn(getDataset(VARIABLE_SCHEMA));
        when(fundamentalService.getAll(any(), eq(fundamentalFilters))).thenReturn(getFundamentalDataset());
        Dataset<Row> result = aggregationService.getData(variable, properties);
        long expectedResultSize = 3600 / 20;
        assertResult(result, expectedResultSize, Arrays.asList(1L, 2L, 2L), Duration.of(20, ChronoUnit.SECONDS));
    }

    @Test
    public void shouldGetDataForEntity() {
        Entity entity = getEntity();
        WindowAggregationProperties properties = WindowAggregationProperties.builder()
                .timeWindow(START, START.plus(1, ChronoUnit.HOURS))
                .interval(20, ChronoUnit.SECONDS)
                .function(AggregationFunctions.MAX)
                .aggregationField(ENTITY_DATA_FIELD_NAME)
                .build();
        when(entityDataProvider.get(any(), any(), eq(entity)))
                .thenReturn(getDataset(ENTITY_SCHEMA));

        Dataset<Row> result = aggregationService
                .getData(entity, properties);

        long expectedResultSize = 3600 / 20;

        assertResult(result, expectedResultSize, Arrays.asList(1L, 2L, 3L), Duration.of(20, ChronoUnit.SECONDS));

    }

    @Test
    public void shouldGetDataForEntityFilteredByFundamentals() {
        Entity entity = getEntity();
        Set<FundamentalFilter> fundamentalFilters = Collections.singleton(
                FundamentalFilter.builder().accelerator("TEST").lsaCycle("TEST_CYCLE").build());
        WindowAggregationProperties properties = WindowAggregationProperties.builder()
                .timeWindow(START, START.plus(1, ChronoUnit.HOURS))
                .interval(20, ChronoUnit.SECONDS)
                .function(AggregationFunctions.MAX)
                .aggregationField(ENTITY_DATA_FIELD_NAME)
                .fundamentalFilters(fundamentalFilters)
                .build();
        when(entityDataProvider.get(any(), any(), eq(entity)))
                .thenReturn(getDataset(ENTITY_SCHEMA));
        when(fundamentalService.getAll(any(), eq(fundamentalFilters))).thenReturn(getFundamentalDataset());

        Dataset<Row> result = aggregationService
                .getData(entity, properties);

        long expectedResultSize = 3600 / 20;

        assertResult(result, expectedResultSize, Arrays.asList(1L, 2L, 2L), Duration.of(20, ChronoUnit.SECONDS));
    }

    @Test
    public void shouldThrowOnBuildPropertiesWhenDrivingDatasetIsEmpty() {
        assertThrows(IllegalArgumentException.class, () -> DatasetAggregationProperties.builder()
                .drivingDataset(getEmptyDataset(VARIABLE_SCHEMA))
                .build());
    }

    @Test
    public void shouldGetEmptyPlaceholderDatasetWithDrivingDatasetForVariableWhenDatasetEmpty() {
        Variable variable = getVariable();
        Dataset<Row> drivingDataset = getDataset(VARIABLE_SCHEMA);
        DatasetAggregationProperties properties = DatasetAggregationProperties.builder()
                .drivingDataset(drivingDataset)
                .build();
        when(variableDataProvider.get(any(), any(), eq(variable)))
                .thenReturn(getEmptyDataset(VARIABLE_SCHEMA));
        Dataset<Row> result = aggregationService
                .getData(variable, properties);

        assertResult(result, Collections.emptyList(), drivingDataset, properties.getTimestampField());
    }

    @Test
    public void shouldGetDataForVariableAlignedToProvidedDrivingDataset() {
        Variable variable = getVariable();

        Dataset<Row> drivingDataset = buildDataset(VARIABLE_SCHEMA, new Object[][] {
                { TimeUtils.getNanosFromInstant(START), 1L },
                { TimeUtils.getNanosFromInstant(START.plusSeconds(4)), 1L },
                { TimeUtils.getNanosFromInstant(START.plusSeconds(8)), 2L },
                { TimeUtils.getNanosFromInstant(START.plusSeconds(12)), 2L },
                { TimeUtils.getNanosFromInstant(START.plusSeconds(50)), 2L }
        });
        DatasetAggregationProperties properties = DatasetAggregationProperties.builder()
                .drivingDataset(drivingDataset)
                .build();

        when(variableDataProvider.get(any(), any(), eq(variable)))
                .thenReturn(getDataset(VARIABLE_SCHEMA));
        Dataset<Row> result = aggregationService.getData(variable, properties);
        assertResult(result, Arrays.asList(1L, 1L, 1L, 1L, 3L), drivingDataset, properties.getTimestampField());
    }

    @Test
    public void shouldThrowIfDrivingDatasetHasNoExtractionTimestampPresent() {
        assertThrows(IllegalArgumentException.class, () -> DatasetAggregationProperties.builder()
                .drivingDataset(getDataset(ENTITY_SCHEMA))
                .build());
    }

    @Test
    public void shouldThrowOnGetDataForEntityAlignedOnDatasetIfAggregationFieldNotPresent() {
        DatasetAggregationProperties properties = DatasetAggregationProperties.builder()
                .drivingDataset(getDataset(ENTITY_SCHEMA))
                .timestampField(ENTITY_TIMESTAMP_FIELD_NAME)
                .build();
        assertThrows(NullPointerException.class, () -> aggregationService.getData(getEntity(), properties));
    }

    @Test
    public void shouldGetEmptyPlaceholderDatasetWithDrivingDatasetForEntityWhenDatasetEmpty() {
        Entity entity = getEntity();
        Dataset<Row> drivingDataset = getDataset(ENTITY_SCHEMA);
        DatasetAggregationProperties properties = DatasetAggregationProperties.builder()
                .drivingDataset(drivingDataset)
                .timestampField(ENTITY_TIMESTAMP_FIELD_NAME)
                .aggregationField(ENTITY_DATA_FIELD_NAME)
                .build();
        when(entityDataProvider.get(any(), any(), eq(entity)))
                .thenReturn(getEmptyDataset(ENTITY_SCHEMA));
        Dataset<Row> result = aggregationService
                .getData(entity, properties);

        assertResult(result, Collections.emptyList(), drivingDataset, properties.getTimestampField());
    }

    @Test
    public void shouldGetDataForEntityAlignedToProvidedDrivingDataset() {
        Entity entity = getEntity();

        Dataset<Row> drivingDataset = buildDataset(VARIABLE_SCHEMA, new Object[][] {
                { TimeUtils.getNanosFromInstant(START), 1L },
                { TimeUtils.getNanosFromInstant(START.plusSeconds(4)), 1L },
                { TimeUtils.getNanosFromInstant(START.plusSeconds(8)), 2L },
                { TimeUtils.getNanosFromInstant(START.plusSeconds(12)), 2L },
                { TimeUtils.getNanosFromInstant(START.plusSeconds(50)), 2L }
        });
        DatasetAggregationProperties properties = DatasetAggregationProperties.builder()
                .drivingDataset(drivingDataset)
                .aggregationField(ENTITY_DATA_FIELD_NAME)
                .build();

        when(entityDataProvider.get(any(), any(), eq(entity)))
                .thenReturn(getDataset(ENTITY_SCHEMA));
        Dataset<Row> result = aggregationService.getData(entity, properties);
        assertResult(result, Arrays.asList(1L, 1L, 1L, 1L, 3L), drivingDataset, properties.getTimestampField());
    }

    // helper methods

    private void assertResult(Dataset<Row> result, long expectedResultSize, List<Long> expectedResults,
            Duration intervalDuration) {
        assertNotNull(result);

        List<String> actualColumns = Arrays.asList(result.columns());
        assertEquals(EXPECTED_COLUMNS.size(), actualColumns.size());
        assertTrue(EXPECTED_COLUMNS.containsAll(actualColumns));

        List<Pair<Instant, Long>> collectedResult = collect(result);
        assertEquals(expectedResultSize, collectedResult.size());
        Iterator<Long> expectedResultsIterator = expectedResults.iterator();
        for (int i = 0; i < expectedResults.size(); i++) {
            Pair<Instant, Long> row = collectedResult.get(i);
            if (row.getRight() == null) {
                continue;
            }
            assertNotNull(row.getLeft());
            assertEquals(START.plus(intervalDuration.multipliedBy(i)), row.getLeft());
            assertEquals(expectedResultsIterator.next(), row.getRight());
        }

    }

    private void assertResult(Dataset<Row> result, List<Long> expectedResults, Dataset<Row> drivingDataset,
            String timestampField) {
        assertNotNull(result);

        List<String> actualColumns = Arrays.asList(result.columns());
        assertEquals(EXPECTED_COLUMNS.size(), actualColumns.size());
        assertTrue(EXPECTED_COLUMNS.containsAll(actualColumns));

        List<Pair<Instant, Long>> collectedResult = collect(result);
        List<Instant> drivingDatasetRows = collectTimestamps(drivingDataset, timestampField);
        assertEquals(drivingDatasetRows.size(), collectedResult.size());
        Iterator<Long> expectedResultsIterator = expectedResults.iterator();
        for (int i = 0; i < expectedResults.size(); i++) {
            Pair<Instant, Long> row = collectedResult.get(i);
            if (row.getRight() == null) {
                continue;
            }
            assertNotNull(row.getLeft());
            assertEquals(drivingDatasetRows.get(i), row.getLeft());
            assertEquals(expectedResultsIterator.next(), row.getRight());
        }

    }

    private Dataset<Row> getDataset(StructType schema) {
        return buildDataset(schema, new Object[][] {
                { TimeUtils.getNanosFromInstant(START), 1L },
                { TimeUtils.getNanosFromInstant(START.plusSeconds(3)), 1L },
                { TimeUtils.getNanosFromInstant(START.plusSeconds(25)), 2L },
                { TimeUtils.getNanosFromInstant(START.plusSeconds(40)), 2L },
                { TimeUtils.getNanosFromInstant(START.plusSeconds(50)), 3L }

        });
    }

    private Dataset<Row> getFundamentalDataset() {
        return buildDataset(FUNDAMENTAL_VARIABLE_SCHEMA, new Object[][] {
                { TimeUtils.getNanosFromInstant(START), "TEST_CYCLE", "TEST_USER", "TEST_DEST",
                        "TEST:NXCALS_FUNDAMENTAL" },
                { TimeUtils.getNanosFromInstant(START.plusSeconds(2)), "SUPER_TEST_CYCLE", "TEST_USER", "TEST_DEST",
                        "SUPER_TEST:NXCALS_FUNDAMENTAL" },
                { TimeUtils.getNanosFromInstant(START.plusSeconds(25)), "TEST_CYCLE", "TEST_USER", "TEST_DEST",
                        "TEST:NXCALS_FUNDAMENTAL" },
                { TimeUtils.getNanosFromInstant(START.plusSeconds(30)), "SUPER_TEST_CYCLE", "TEST_USER", "TEST_DEST",
                        "SUPER_TEST:NXCALS_FUNDAMENTAL" },
                { TimeUtils.getNanosFromInstant(START.plusSeconds(40)), "TEST_CYCLE", "TEST_USER", "TEST_DEST",
                        "TEST:NXCALS_FUNDAMENTAL" },
                { TimeUtils.getNanosFromInstant(START.plusSeconds(60)), "OTHER_CYCLE", "SUPER_USER", "OTHER_DEST",
                        "OTHER_TEST:NXCALS_FUNDAMENTAL" },
        });
    }

    private Dataset<Row> getEmptyDataset(StructType schema) {
        return buildDataset(schema);
    }

    private Dataset<Row> getDatasetWithNullEntries(StructType schema) {
        return buildDataset(schema, new Object[][] {
                { TimeUtils.getNanosFromInstant(START), null },
                { TimeUtils.getNanosFromInstant(START.plusSeconds(3)), null },
                { TimeUtils.getNanosFromInstant(START.plusSeconds(25)), 2L },
                { TimeUtils.getNanosFromInstant(START.plusSeconds(40)), null },
                { TimeUtils.getNanosFromInstant(START.plusSeconds(50)), 3L },
        });
    }

    private Variable getVariable() {
        return Variable.builder()
                .variableName("test")
                .configs(Collections.emptySortedSet())
                .systemSpec(SYSTEM)
                .declaredType(VariableDeclaredType.NUMERIC)
                .build();
    }

    private Entity getEntity() {
        return Entity.builder()
                .entityKeyValues(Collections.emptyMap())
                .systemSpec(SYSTEM)
                .partition(PARTITION)
                .entityHistory(Collections.emptySortedSet())
                .build();
    }

    private List<Pair<Instant, Long>> collect(Dataset<Row> dataset) {
        return dataset.collectAsList().stream()
                .map(r -> {
                    int timestampFieldIndex = r.fieldIndex(AGGREGATION_TIMESTAMP_FIELD);
                    int valueFieldIndex = r.fieldIndex(AGGREGATION_VALUE_FIELD);
                    return ImmutablePair.of(TimeUtils.getInstantFromNanos(r.getLong(timestampFieldIndex)),
                            r.isNullAt(valueFieldIndex) ? null : r.getLong(valueFieldIndex));
                }).collect(Collectors.toList());
    }

    private List<Instant> collectTimestamps(Dataset<Row> dataset, String timestampField) {
        return dataset.collectAsList().stream()
                .map(r -> TimeUtils.getInstantFromNanos(r.getLong(r.fieldIndex(timestampField))))
                .collect(Collectors.toList());
    }

}
