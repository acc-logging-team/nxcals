package cern.nxcals.api.custom.service.aggregation;

import cern.nxcals.api.domain.TimeWindow;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class WindowAggregationPropertiesTest {

    @Test
    public void shouldThrowWhenRangeNotSet() {
        assertThrows(NullPointerException.class, () -> WindowAggregationProperties.builder()
                .interval(1, ChronoUnit.SECONDS)
                .function(AggregationFunctions.AVG)
                .build());
    }

    @Test
    public void shouldThrowWhenRangeFromIsAfterEnd() {
        Instant from = Instant.now();
        Instant to = from.minus(1, ChronoUnit.HOURS);
        assertThrows(IllegalArgumentException.class, () -> WindowAggregationProperties.builder()
                .timeWindow(from, to)
                .interval(1, ChronoUnit.SECONDS)
                .function(AggregationFunctions.AVG)
                .build());
    }

    @Test
    public void shouldThrowWhenRangeFromIsSameAsEnd() {
        Instant from = Instant.now();
        Instant to = from;
        assertThrows(IllegalArgumentException.class, () -> WindowAggregationProperties.builder()
                .timeWindow(from, to)
                .interval(1, ChronoUnit.SECONDS)
                .function(AggregationFunctions.AVG)
                .build());
    }

    @Test
    public void shouldThrowIntervalDurationNotSet() {
        Instant from = Instant.now();
        Instant to = from.plus(1, ChronoUnit.HOURS);
        assertThrows(NullPointerException.class, () -> WindowAggregationProperties.builder()
                .timeWindow(from, to)
                .function(AggregationFunctions.AVG)
                .build());
    }

    @Test
    public void shouldThrowWhenIntervalDurationHasNegativeAmount() {
        Instant from = Instant.now();
        Instant to = from.plus(1, ChronoUnit.HOURS);
        assertThrows(IllegalArgumentException.class, () -> WindowAggregationProperties.builder()
                .timeWindow(from, to)
                .interval(-10, ChronoUnit.SECONDS)
                .function(AggregationFunctions.AVG)
                .build());
    }

    @Test
    public void shouldThrowWhenIntervalDurationIsLessThanAMillisecond() {
        Instant from = Instant.now();
        Instant to = from.plus(1, ChronoUnit.HOURS);
        assertThrows(IllegalArgumentException.class, () -> WindowAggregationProperties.builder()
                .timeWindow(from, to)
                .interval(300, ChronoUnit.NANOS)
                .function(AggregationFunctions.AVG)
                .build());
    }

    @Test
    public void shouldThrowWhenFunctionNotSet() {
        Instant from = Instant.now();
        Instant to = from.plus(1, ChronoUnit.HOURS);
        assertThrows(NullPointerException.class, () -> WindowAggregationProperties.builder()
                .timeWindow(from, to)
                .interval(1, ChronoUnit.SECONDS)
                .build());
    }

    @Test
    public void shouldBuildWithNoSearchRangeExpansionWhenNotSetAndNotAllowedByFunction() {
        Instant from = Instant.now();
        Instant to = from.plus(1, ChronoUnit.HOURS);
        WindowAggregationProperties properties = WindowAggregationProperties.builder()
                .timeWindow(from, to)
                .interval(1, ChronoUnit.SECONDS)
                .function(AggregationFunctions.AVG)
                .build();

        assertEquals(ExpandTimeWindow.NONE, properties.getFunction().getTimeWindowExpansion());
    }

    @Test
    public void shouldBuildWithPredefinedSearchRangeExpansionWhenNotSetAndAllowedByFunction() {
        Instant from = Instant.now();
        Instant to = from.plus(1, ChronoUnit.HOURS);
        WindowAggregationProperties properties = WindowAggregationProperties.builder()
                .timeWindow(from, to)
                .interval(1, ChronoUnit.SECONDS)
                .function(AggregationFunctions.REPEAT)
                .build();

        assertEquals(ExpandTimeWindow.START_TIME, properties.getFunction().getTimeWindowExpansion());
    }

    @Test
    public void shouldBuildPropertiesWithoutAggregationField() {
        Instant from = Instant.now();
        Instant to = from.plus(1, ChronoUnit.HOURS);
        WindowAggregationProperties properties = WindowAggregationProperties.builder()
                .timeWindow(from, to)
                .interval(1, ChronoUnit.DAYS)
                .function(AggregationFunctions.AVG)
                .build();
        assertNull(properties.getAggregationField());
    }

    @Test
    public void shouldBuildAggregationProperties() {
        Instant from = Instant.now();
        Instant to = from.plus(1, ChronoUnit.HOURS);
        String aggregationField = "myTestField";
        AggregationFunction repeat = AggregationFunctions.REPEAT
                .expandTimeWindowBy(1, 1, ChronoUnit.HOURS);
        WindowAggregationProperties properties = WindowAggregationProperties.builder()
                .timeWindow(from, to)
                .interval(2, ChronoUnit.WEEKS)
                .function(repeat)
                .aggregationField(aggregationField)
                .build();

        assertEquals(TimeWindow.between(from, to), properties.getTimeWindow());
        assertEquals(Duration.of(14, ChronoUnit.DAYS).toMillis(), properties.getIntervalMillis());
        assertEquals(repeat, properties.getFunction());

        Duration expectedExpandDuration = Duration.ofHours(1);
        assertEquals(ImmutablePair.of(expectedExpandDuration, expectedExpandDuration),
                properties.getFunction().getTimeWindowExpansion());
        assertEquals(aggregationField, properties.getAggregationField());
    }

}
