package cern.nxcals.api.custom.service.fundamental;

import cern.nxcals.api.custom.domain.CmwSystemConstants;
import cern.nxcals.api.custom.domain.FundamentalFilter;
import cern.nxcals.api.custom.domain.util.QueryDatasetProvider;
import cern.nxcals.api.custom.service.TestSparkSession;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.domain.Variable;
import cern.nxcals.api.domain.VariableDeclaredType;
import cern.nxcals.api.extraction.metadata.VariableService;
import cern.nxcals.api.extraction.metadata.queries.Variables;
import cern.nxcals.api.utils.TimeUtils;
import cern.nxcals.common.utils.RSQLUtils;
import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import lombok.NonNull;
import org.apache.avro.SchemaBuilder;
import org.apache.commons.lang.ArrayUtils;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;

import static cern.nxcals.api.custom.service.TestUtils.buildDataset;
import static cern.nxcals.api.custom.service.fundamental.FundamentalContext.VARIABLE_NAME_SUFFIX;
import static cern.nxcals.common.SystemFields.NXC_EXTR_ENTITY_ID;
import static cern.nxcals.common.SystemFields.NXC_EXTR_TIMESTAMP;
import static cern.nxcals.common.SystemFields.NXC_EXTR_VARIABLE_NAME;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class FundamentalServiceTest {
    private static final Supplier<SparkSession> SESSION_SUPPLIER = TestSparkSession.JUNIT;
    private static final String SYSTEM_TIME_KEY_STRING = SchemaBuilder.record("test").fields()
            .name(CmwSystemConstants.RECORD_TIMESTAMP).type().longType().noDefault().endRecord().toString();
    private static final SystemSpec FUNDAMENTAL_SYSTEM_SPEC = SystemSpec.builder()
            .timeKeyDefinitions(SYSTEM_TIME_KEY_STRING)
            .partitionKeyDefinitions("")
            .entityKeyDefinitions("")
            .name(FundamentalContext.SYSTEM)
            .build();

    private static final StructType FUNDAMENTAL_VARIABLE_SCHEMA = createFundamentalVariableSchema();
    private static final StructType FUNDAMENTAL_RAW_VARIABLE_SCHEMA = createRawFundamentalVariableSchema();

    private static final String TEST_CYCLE = "TEST_CYCLE";
    private static final String TEST_USER = "TEST_USER";
    private static final String TEST_DEST = "TEST_DEST";
    private static final String TEST_ACCELERATOR = "TEST";
    private static final String OTHER_CYCLE = "OTHER_CYCLE";
    private static final String OTHER_TEST_ACCELERATOR = "OTHER_TEST";
    private static final String SUPER_USER = "SUPER_USER";
    private static final String OTHER_DEST = "OTHER_DEST";

    private static StructType createFundamentalVariableSchema() {
        StructField timestampField = new StructField(NXC_EXTR_TIMESTAMP.getValue(), DataTypes.LongType, true,
                Metadata.empty());
        StructField cycleField = new StructField(FundamentalContext.VIRTUAL_LSA_CYCLE_FIELD, DataTypes.StringType, true,
                Metadata.empty());
        StructField userField = new StructField(FundamentalContext.USER_FIELD, DataTypes.StringType, true,
                Metadata.empty());
        StructField destinationField = new StructField(FundamentalContext.DESTINATION_FIELD, DataTypes.StringType, true,
                Metadata.empty());
        StructField variableNameField = new StructField(NXC_EXTR_VARIABLE_NAME.getValue(), DataTypes.StringType, true,
                Metadata.empty());
        return new StructType(
                new StructField[] { timestampField, cycleField, userField, destinationField, variableNameField });
    }

    private static StructType createRawFundamentalVariableSchema() {
        StructField entityIdField = new StructField(NXC_EXTR_ENTITY_ID.getValue(), DataTypes.LongType, true,
                Metadata.empty());
        StructField selector = new StructField("selector", DataTypes.StringType, true,
                Metadata.empty());
        StructField device = new StructField("device", DataTypes.StringType, true,
                Metadata.empty());
        StructField property = new StructField("property", DataTypes.StringType, true,
                Metadata.empty());
        StructField clazz = new StructField("class", DataTypes.StringType, true,
                Metadata.empty());
        StructField acqStamp = new StructField("acqStamp", DataTypes.LongType, true,
                Metadata.empty());
        StructField cyclestamp = new StructField("cyclestamp", DataTypes.LongType, true,
                Metadata.empty());
        StructField version = new StructField("__record_version__", DataTypes.LongType, true,
                Metadata.empty());

        return new StructType((StructField[]) ArrayUtils.addAll(createFundamentalVariableSchema().fields(),
                new StructField[] { entityIdField, selector, device, property, clazz, acqStamp, cyclestamp, version }));
    }

    @Mock
    private VariableService variableService;
    @Mock
    private QueryDatasetProvider<Set<String>> variablesDatasetProvider;

    private FundamentalServiceImpl fundamentalService;

    @BeforeEach
    void setUp() {
        fundamentalService = new FundamentalServiceImpl(SESSION_SUPPLIER, variablesDatasetProvider, variableService);
    }

    @Test
    void shouldThrowWhenTimeWindowIsNull() {
        TimeWindow timeWindow = null;
        Set<FundamentalFilter> fundamentalFilters = Collections.emptySet();
        assertThrows(NullPointerException.class, () -> fundamentalService.getAll(timeWindow, fundamentalFilters));
    }

    @Test
    void shouldThrowWhenFundamentalFiltersIsNull() {
        TimeWindow timeWindow = TimeWindow.between(Instant.EPOCH, Instant.EPOCH.plus(1, ChronoUnit.MINUTES));
        Set<FundamentalFilter> fundamentalFilters = null;
        assertThrows(NullPointerException.class, () -> fundamentalService.getAll(timeWindow, fundamentalFilters));
    }

    @Test
    void shouldThrowWhenFundamentalFiltersIsAcceleratorInfoIsNull() {
        TimeWindow timeWindow = TimeWindow.between(Instant.EPOCH, Instant.EPOCH.plus(1, ChronoUnit.MINUTES));
        Set<FundamentalFilter> fundamentalFilters = Collections.singleton(
                FundamentalFilter.builder().destination("whatever").build());
        assertThrows(NullPointerException.class, () -> fundamentalService.getAll(timeWindow, fundamentalFilters));
    }

    @Test
    void shouldThrowWhenFundamentalFiltersIsAcceleratorInfoIsBlank() {
        TimeWindow timeWindow = TimeWindow.between(Instant.EPOCH, Instant.EPOCH.plus(1, ChronoUnit.MINUTES));
        Set<FundamentalFilter> fundamentalFilters = Collections.singleton(
                FundamentalFilter.builder().accelerator("   ").build());
        assertThrows(IllegalArgumentException.class, () -> fundamentalService.getAll(timeWindow, fundamentalFilters));
    }

    @Test
    void shouldThrowIfFundamentalFilterDoesNotCorrespondToVariables() {
        TimeWindow timeWindow = TimeWindow.between(Instant.EPOCH, Instant.EPOCH.plus(1, ChronoUnit.MINUTES));
        String wrongAcceleratorName = "I_DONT_EXIST";
        FundamentalFilter wrongFundamentalFilter = FundamentalFilter.builder()
                .accelerator(wrongAcceleratorName).build();

        when(variableService.findAll(any(Condition.class))).thenReturn(Collections.emptySet());

        assertThrows(IllegalArgumentException.class, () -> fundamentalService.getAll(timeWindow,
                Collections.singleton(wrongFundamentalFilter)));

        ArgumentCaptor<Condition<Variables>> searchConditionCaptor = ArgumentCaptor.forClass(Condition.class);
        verify(variableService).findAll(searchConditionCaptor.capture());

        Condition<Variables> expectedSearchCondition = Variables.suchThat().or(Collections.singletonList(
                Variables.suchThat().variableName()
                        .like(wrongAcceleratorName + VARIABLE_NAME_SUFFIX)));

        assertEquals(RSQLUtils.toRSQL(expectedSearchCondition), RSQLUtils.toRSQL(searchConditionCaptor.getValue()));
    }

    @Test
    void shouldReturnEmptyFundamentalDatasetIfFilterDoesNotMatchData() {
        TimeWindow timeWindow = TimeWindow.between(Instant.EPOCH, Instant.EPOCH.plus(1, ChronoUnit.MINUTES));
        FundamentalFilter fundamentalFilter = FundamentalFilter.builder()
                .accelerator(TEST_ACCELERATOR).lsaCycle(TEST_CYCLE).timingUser(TEST_USER).destination(TEST_DEST)
                .build();

        Set<Variable> fundamentalVariables = Collections.singleton(getFundamentalVariableFor(fundamentalFilter));
        Set<String> fundamentalVariableNames = Set.of(FundamentalContext.getVariableNameFrom(fundamentalFilter));
        when(variableService.findAll(any(Condition.class))).thenReturn(fundamentalVariables);
        when(variablesDatasetProvider.get(any(), any(), eq(fundamentalVariableNames))).thenReturn(getEmptyDataset());

        Dataset<Row> fundamentalsDataset = fundamentalService.getAll(timeWindow,
                Collections.singleton(fundamentalFilter));
        assertTrue(fundamentalsDataset.isEmpty());
    }

    @Test
    void shouldReturnFundamentalDatasetReducedByProvidedFilter() {
        TimeWindow timeWindow = TimeWindow.between(Instant.EPOCH, Instant.EPOCH.plus(1, ChronoUnit.MINUTES));
        FundamentalFilter fundamentalFilter = FundamentalFilter.builder()
                .accelerator(TEST_ACCELERATOR).lsaCycle(TEST_CYCLE).timingUser(TEST_USER).destination(TEST_DEST)
                .build();

        Set<Variable> fundamentalVariables = Collections.singleton(getFundamentalVariableFor(fundamentalFilter));
        Set<String> fundamentalVariableNames = Set.of(FundamentalContext.getVariableNameFrom(fundamentalFilter));
        when(variableService.findAll(any(Condition.class))).thenReturn(fundamentalVariables);
        when(variablesDatasetProvider.get(any(), any(), eq(fundamentalVariableNames))).thenReturn(getDataset());

        Dataset<Row> fundamentalsDataset = fundamentalService.getAll(timeWindow,
                Collections.singleton(fundamentalFilter));
        assertEquals(1, fundamentalsDataset.count());
        Row row = fundamentalsDataset.collectAsList().iterator().next();
        assertEquals(TEST_CYCLE, row.getString(1));
        assertEquals(TEST_USER, row.getString(2));
        assertEquals(TEST_DEST, row.getString(3));
    }

    @Test
    void shouldReturnFundamentalDatasetReducedByTheProvidedWildcardFilter() {
        TimeWindow timeWindow = TimeWindow.between(Instant.EPOCH, Instant.EPOCH.plus(1, ChronoUnit.MINUTES));

        String accelerator = OTHER_TEST_ACCELERATOR;
        FundamentalFilter fundamentalFilter = FundamentalFilter.builder()
                .accelerator(accelerator).destination("OTHER_%").build();

        Set<Variable> fundamentalVariables = Collections.singleton(getFundamentalVariableFor(fundamentalFilter));
        Set<String> fundamentalVariableNames = Set.of(FundamentalContext.getVariableNameFrom(fundamentalFilter));
        when(variableService.findAll(any(Condition.class))).thenReturn(fundamentalVariables);
        when(variablesDatasetProvider.get(any(), any(), eq(fundamentalVariableNames))).thenReturn(getDataset());

        Dataset<Row> fundamentalsDataset = fundamentalService.getAll(timeWindow,
                Collections.singleton(fundamentalFilter));
        assertEquals(1, fundamentalsDataset.count());
        Row row = fundamentalsDataset.collectAsList().iterator().next();
        assertEquals(OTHER_CYCLE, row.getString(1));
        assertEquals(SUPER_USER, row.getString(2));
        assertEquals(OTHER_DEST, row.getString(3));
        assertEquals(accelerator + VARIABLE_NAME_SUFFIX, row.getString(4));
    }

    @Test
    void shouldReturnFundamentalDatasetForCollection() {
        // given
        TimeWindow timeWindow = TimeWindow.between(Instant.EPOCH, Instant.EPOCH.plus(1, ChronoUnit.MINUTES));
        when(variablesDatasetProvider.get(any(), any(), eq(Set.of("CPS" + VARIABLE_NAME_SUFFIX)))).thenReturn(
                getRawFundamentalDataset());

        // when
        Dataset<Row> ds = fundamentalService.getFor(timeWindow, List.of("CPS"));

        // then
        assertNotNull(ds);
        Set<String> columns = new HashSet<>(List.of(ds.columns()));
        assertTrue(columns.contains("device"));
        assertTrue(columns.contains("property"));
        assertTrue(columns.contains("class"));
        assertTrue(columns.contains(NXC_EXTR_ENTITY_ID.getValue()));
        assertTrue(columns.contains("cyclestamp"));
        assertTrue(columns.contains("acqStamp"));
        assertTrue(columns.contains(NXC_EXTR_VARIABLE_NAME.getValue()));
    }

    @Test
    void shouldReturnFundamentalDatasetByAcceleratorName() {
        // given
        TimeWindow timeWindow = TimeWindow.between(Instant.EPOCH, Instant.EPOCH.plus(1, ChronoUnit.MINUTES));
        when(variablesDatasetProvider.get(any(), any(), eq(Set.of("CPS" + VARIABLE_NAME_SUFFIX)))).thenReturn(
                getRawFundamentalDataset());

        // when
        Dataset<Row> ds = fundamentalService.getFor(timeWindow, "CPS");

        // then
        assertNotNull(ds);
        Set<String> columns = new HashSet<>(List.of(ds.columns()));
        assertTrue(columns.contains("device"));
        assertTrue(columns.contains("property"));
        assertTrue(columns.contains("class"));
        assertTrue(columns.contains(NXC_EXTR_ENTITY_ID.getValue()));
        assertTrue(columns.contains("cyclestamp"));
        assertTrue(columns.contains("acqStamp"));
        assertTrue(columns.contains(NXC_EXTR_VARIABLE_NAME.getValue()));
    }

    @Test
    void shouldReturnFundamentalDatasetByAcceleratorNames() {
        // given
        TimeWindow timeWindow = TimeWindow.between(Instant.EPOCH, Instant.EPOCH.plus(1, ChronoUnit.MINUTES));
        when(variablesDatasetProvider.get(any(), any(),
                eq(Set.of("CPS" + VARIABLE_NAME_SUFFIX, "SPS" + VARIABLE_NAME_SUFFIX)))).thenReturn(
                getRawFundamentalDataset());

        // when
        Dataset<Row> ds = fundamentalService.getFor(timeWindow, "CPS", "SPS");

        // then
        assertNotNull(ds);
        Set<String> columns = new HashSet<>(List.of(ds.columns()));
        assertTrue(columns.contains("device"));
        assertTrue(columns.contains("property"));
        assertTrue(columns.contains("class"));
        assertTrue(columns.contains(NXC_EXTR_ENTITY_ID.getValue()));
        assertTrue(columns.contains("cyclestamp"));
        assertTrue(columns.contains("acqStamp"));
        assertTrue(columns.contains(NXC_EXTR_VARIABLE_NAME.getValue()));
    }

    @Test
    void shouldThrowIfPassedEmptyListOfAccelerators() {
        TimeWindow timeWindow = TimeWindow.between(Instant.EPOCH, Instant.EPOCH.plus(1, ChronoUnit.MINUTES));
        assertThrows(IllegalArgumentException.class,
                () -> fundamentalService.getFor(timeWindow, Collections.emptyList()));
    }

    //helper methods

    private Variable getFundamentalVariableFor(@NonNull FundamentalFilter fundamentalFilter) {
        return Variable.builder()
                .variableName(FundamentalContext.getVariableNameFrom(fundamentalFilter))
                .configs(Collections.emptySortedSet())
                .systemSpec(FUNDAMENTAL_SYSTEM_SPEC)
                .declaredType(VariableDeclaredType.FUNDAMENTAL)
                .build();
    }

    private Dataset<Row> getEmptyDataset() {
        return buildDataset(FUNDAMENTAL_VARIABLE_SCHEMA);
    }

    private Dataset<Row> getDataset() {
        return buildDataset(FUNDAMENTAL_VARIABLE_SCHEMA, new Object[][] {
                { TimeUtils.getNanosFromInstant(Instant.EPOCH), TEST_CYCLE, TEST_USER, TEST_DEST,
                        TEST_ACCELERATOR + VARIABLE_NAME_SUFFIX },
                { TimeUtils.getNanosFromInstant(Instant.EPOCH.plusSeconds(1)), "SUPER_TEST_CYCLE", TEST_USER,
                        TEST_DEST, TEST_ACCELERATOR + VARIABLE_NAME_SUFFIX },
                { TimeUtils.getNanosFromInstant(Instant.EPOCH.plusSeconds(2)), OTHER_CYCLE, SUPER_USER,
                        OTHER_DEST, TEST_ACCELERATOR + VARIABLE_NAME_SUFFIX },
                { TimeUtils.getNanosFromInstant(Instant.EPOCH.plusSeconds(3)), OTHER_CYCLE, SUPER_USER,
                        OTHER_DEST, OTHER_TEST_ACCELERATOR + VARIABLE_NAME_SUFFIX },
        });
    }

    //new StructField[] { entityIdField, selector, device, property, clazz, acqStamp, cyclestamp, version }
    private Dataset<Row> getRawFundamentalDataset() {
        return buildDataset(FUNDAMENTAL_RAW_VARIABLE_SCHEMA, new Object[][] {
                { TimeUtils.getNanosFromInstant(Instant.EPOCH), TEST_CYCLE, TEST_USER, TEST_DEST,
                        TEST_ACCELERATOR + VARIABLE_NAME_SUFFIX, 10L, "selector", "dev", "prop", "class", 10L, 10L,
                        10L },
                { TimeUtils.getNanosFromInstant(Instant.EPOCH.plusSeconds(1)), "SUPER_TEST_CYCLE", TEST_USER,
                        TEST_DEST, TEST_ACCELERATOR + VARIABLE_NAME_SUFFIX, 10L, "selector", "dev", "prop", "class",
                        10L, 10L, 10L },
                { TimeUtils.getNanosFromInstant(Instant.EPOCH.plusSeconds(2)), OTHER_CYCLE, SUPER_USER,
                        OTHER_DEST, TEST_ACCELERATOR + VARIABLE_NAME_SUFFIX, 10L, "selector", "dev", "prop", "class",
                        10L, 10L, 10L },
                { TimeUtils.getNanosFromInstant(Instant.EPOCH.plusSeconds(3)), OTHER_CYCLE, SUPER_USER,
                        OTHER_DEST, OTHER_TEST_ACCELERATOR + VARIABLE_NAME_SUFFIX, 10L, "selector", "dev", "prop",
                        "class", 10L, 10L, 10L },
        });
    }

}
