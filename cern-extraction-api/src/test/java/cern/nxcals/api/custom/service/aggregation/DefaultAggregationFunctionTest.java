package cern.nxcals.api.custom.service.aggregation;

import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.functions;
import org.junit.jupiter.api.Test;

import static cern.nxcals.api.custom.service.aggregation.AggregationFunction.VALUE_COLUMN_NAME;
import static org.apache.spark.sql.functions.col;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class DefaultAggregationFunctionTest extends BaseAggregationFunctionTest {

    private final AggregationFunction function = new DefaultAggregationFunction(
            functions::count);

    @Test
    public void shouldThrowOnCreateWhenAggregationColumnIsNull() {
        assertThrows(NullPointerException.class, () -> new DefaultAggregationFunction(null));
    }

    @Test
    public void shouldThrowOnPrepareWhenDatasetIsNull() {
        assertThrows(NullPointerException.class, () -> function.prepare(null));
    }

    @Test
    public void shouldPrepareWithoutChangingDataset() {
        Dataset<Row> input = buildDataset(AGGREGATION_SCHEMA, new Object[][] {
                { 1L, 1L, 0L, 3L },
                { 2L, 2L, 0L, 3L },
        });
        Dataset<Row> result = function.prepare(input);
        assertEquals(input, result);
    }

    @Test
    public void shouldGetAggregationExpression() {
        Column aggregationExpr = function.getAggregationExpr();
        assertNotNull(aggregationExpr);
        assertEquals(functions.count(col(VALUE_COLUMN_NAME)).toString(), aggregationExpr.toString());
    }

}
