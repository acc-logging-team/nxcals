package cern.nxcals.api.custom.service.aggregation;

import cern.nxcals.api.custom.service.TestSparkSession;
import cern.nxcals.api.domain.TimeWindow;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static cern.nxcals.common.SystemFields.NXC_EXTR_TIMESTAMP;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class DatasetAggregationPropertiesTest {
    private static final SparkSession SPARK_SESSION = TestSparkSession.JUNIT.get();

    @Test
    public void shouldThrowWhenDrivingDatasetNotSet() {
        assertThrows(NullPointerException.class, () -> DatasetAggregationProperties.builder().build());
    }

    @Test
    public void shouldThrowWhenCannotIdentityDrivingDatasetTimestamp() {
        StructField timestampField = new StructField("myTimestamp", DataTypes.LongType, true,
                Metadata.empty());
        assertThrows(IllegalArgumentException.class, () -> DatasetAggregationProperties.builder()
                .drivingDataset(buildDataset(getTimestampSchema(timestampField)))
                .build());
    }

    @Test
    public void shouldThrowOnExtractForInvalidTimeWindowWhenDrivingDatasetIsEmpty() {
        StructField timestampField = new StructField(NXC_EXTR_TIMESTAMP.getValue(), DataTypes.LongType, true,
                Metadata.empty());
        Dataset<Row> drivingDataset = buildDataset(getTimestampSchema(timestampField));
        assertTrue(drivingDataset.isEmpty());
        assertThrows(IllegalArgumentException.class, () -> DatasetAggregationProperties.builder()
                .drivingDataset(drivingDataset)
                .build());
    }

    @Test
    public void shouldGetDatasetAggregationProperties() {
        String aggregationFieldName = "testValue";
        String timestampFieldName = "testTimestamp";
        TimeWindow timeWindow = TimeWindow.between(0, 1000);
        StructField timestampField = new StructField(timestampFieldName, DataTypes.LongType, true,
                Metadata.empty());
        Dataset<Row> drivingDataset = buildDataset(getTimestampSchema(timestampField),
                timeWindow.getStartTimeNanos(), timeWindow.getEndTimeNanos());
        assertFalse(drivingDataset.isEmpty());

        DatasetAggregationProperties properties = DatasetAggregationProperties.builder()
                .drivingDataset(drivingDataset)
                .timestampField(timestampFieldName)
                .aggregationField(aggregationFieldName)
                .build();
        assertNotNull(properties);
        assertEquals(drivingDataset, properties.getDrivingDataset());
        assertEquals(timestampFieldName, properties.getTimestampField());
        assertEquals(aggregationFieldName, properties.getAggregationField());
        assertEquals(timeWindow, properties.getTimeWindow());
    }

    // helper methods

    private StructType getTimestampSchema(StructField timestampField) {
        return new StructType(new StructField[] { timestampField });
    }

    private static Dataset<Row> buildDataset(StructType schema, Long... data) {
        List<Row> rows = new ArrayList<>(data.length);
        for (Long datum : data) {
            rows.add(RowFactory.create(datum));
        }
        return SPARK_SESSION.createDataFrame(rows, schema);
    }
}
