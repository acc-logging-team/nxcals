package cern.nxcals.api.custom.service.aggregation;

import org.apache.commons.lang.ArrayUtils;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.junit.jupiter.api.Test;

import static cern.nxcals.api.custom.service.aggregation.AggregationConstants.INTERVAL_RANGE_FIELDS;
import static cern.nxcals.api.custom.service.aggregation.AggregationFunction.TIMESTAMP_COLUMN_NAME;
import static cern.nxcals.api.custom.service.aggregation.AggregationFunction.VALUE_COLUMN_NAME;
import static cern.nxcals.api.custom.service.aggregation.RepeatAggregationFunction.REPEATED_VALUE_COLUMN_NAME;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RepeatAggregationFunctionTest extends BaseAggregationFunctionTest {

    private static final StructType REPEAT_SCHEMA = createRepeatSchema();

    private static StructType createRepeatSchema() {
        StructField timestampField = new StructField(TIMESTAMP_COLUMN_NAME, DataTypes.LongType, true, Metadata.empty());
        StructField valueField = new StructField(VALUE_COLUMN_NAME, DataTypes.LongType, true, Metadata.empty());
        StructField intervalStartField = new StructField(INTERVAL_RANGE_FIELDS.getLeft(), DataTypes.LongType, true,
                Metadata.empty());
        StructField intervalsEndField = new StructField(INTERVAL_RANGE_FIELDS.getRight(), DataTypes.LongType, true,
                Metadata.empty());

        StructField repeatValueField = new StructField(REPEATED_VALUE_COLUMN_NAME, DataTypes.LongType, true,
                Metadata.empty());
        return new StructType(new StructField[] { timestampField, valueField, intervalStartField, intervalsEndField,
                repeatValueField });
    }

    private final ExpandableAggregationFunction repeatFunction = new RepeatAggregationFunction();

    @Test
    public void shouldThrowOnApplyWhenDatasetIsNull() {
        assertThrows(NullPointerException.class, () -> repeatFunction.prepare(null));
    }

    @Test
    public void shouldAdaptDatasetAddingNullAsRepeatedColumnOnStartWhenNoPreviousDataPoint() {
        long rangeStartTimestamp = 1L;
        long rangeEndTimestamp = 3L;
        Dataset<Row> inputDs = buildDataset(AGGREGATION_SCHEMA, new Object[][] {
                { 2L, 1L, rangeStartTimestamp, rangeEndTimestamp },
                { 3L, 2L, rangeStartTimestamp, rangeEndTimestamp },
        });
        Dataset<Row> result = repeatFunction.prepare(inputDs);
        assertNotNull(result);
        assertTrue(ArrayUtils.contains(result.columns(), REPEATED_VALUE_COLUMN_NAME));

        Dataset<Row> expectedAdaptedDs = buildDataset(REPEAT_SCHEMA, new Object[][] {
                { 2L, 1L, rangeStartTimestamp, rangeEndTimestamp, null },
                { 3L, 2L, rangeStartTimestamp, rangeEndTimestamp, 1L },
        });
        assertArrayEquals(expectedAdaptedDs.collectAsList().toArray(), result.collectAsList().toArray());
    }

    @Test
    public void shouldAdaptDatasetAddingCurrentValueAsRepeatedColumnWhenRowMatchingIntervalStart() {
        long rangeStartTimestamp = 1L;
        long rangeEndTimestamp = 2L;
        Dataset<Row> inputDs = buildDataset(AGGREGATION_SCHEMA, new Object[][] {
                { 1L, 1L, rangeStartTimestamp, rangeEndTimestamp }, //row matching interval start
                { 2L, 2L, rangeStartTimestamp, rangeEndTimestamp },
        });
        Dataset<Row> result = repeatFunction.prepare(inputDs);
        assertNotNull(result);
        assertTrue(ArrayUtils.contains(result.columns(), REPEATED_VALUE_COLUMN_NAME));

        Dataset<Row> expectedAdaptedDs = buildDataset(REPEAT_SCHEMA, new Object[][] {
                { 1L, 1L, rangeStartTimestamp, rangeEndTimestamp, 1L },
                { 2L, 2L, rangeStartTimestamp, rangeEndTimestamp, 1L },
        });
        assertArrayEquals(expectedAdaptedDs.collectAsList().toArray(), result.collectAsList().toArray());
    }

    @Test
    public void shouldAdaptDatasetAddingRepeatedColumn() {
        long rangeStartTimestamp = 1L;
        long rangeEndTimestamp = 3L;
        Dataset<Row> inputDs = buildDataset(AGGREGATION_SCHEMA, new Object[][] {
                { 0L, 1L, null, null }, //previous row (before range start)
                { 1L, 1L, rangeStartTimestamp, rangeEndTimestamp },
                { 2L, 2L, rangeStartTimestamp, rangeEndTimestamp },
        });
        Dataset<Row> result = repeatFunction.prepare(inputDs);
        assertNotNull(result);
        assertTrue(ArrayUtils.contains(result.columns(), REPEATED_VALUE_COLUMN_NAME));

        Dataset<Row> expectedAdaptedDs = buildDataset(REPEAT_SCHEMA, new Object[][] {
                { 0L, 1L, null, null, null },
                { 1L, 1L, rangeStartTimestamp, rangeEndTimestamp, 1L },
                { 2L, 2L, rangeStartTimestamp, rangeEndTimestamp, 1L },
        });
        assertArrayEquals(expectedAdaptedDs.collectAsList().toArray(), result.collectAsList().toArray());
    }

    @Test
    public void shouldGetAggregationExpression() {
        Column repeatExpr = repeatFunction.getAggregationExpr();
        assertNotNull(repeatExpr);
        assertEquals(functions.first(REPEATED_VALUE_COLUMN_NAME, true).toString(), repeatExpr.toString());
    }
}
