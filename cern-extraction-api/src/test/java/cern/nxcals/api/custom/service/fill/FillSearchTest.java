package cern.nxcals.api.custom.service.fill;

import cern.nxcals.api.domain.TimeWindow;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.Comparator;
import java.util.NavigableMap;
import java.util.Optional;
import java.util.Random;
import java.util.TreeMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class FillSearchTest {
    private final Comparator<TimeWindow> byStart = TimeWindow.compareByStartTime()
            .thenComparing(TimeWindow.compareByEndTime());

    private final FillSearch fillSearch = new FillSearch();

    @Test
    public void shouldReturnEmptyTimeWindowWithNullFillMap() {
        Optional<TimeWindow> fillForNumber = fillSearch.getFillForNumber(null, 1);
        assertFalse(fillForNumber.isPresent());
    }

    @Test
    public void shouldWorkForEmptyData() {
        assertFalse(fillSearch.getFillForNumber(Collections.emptyNavigableMap(), 100).isPresent());
    }

    @Test
    public void shouldWorkForLeftDataPoint() {
        NavigableMap<TimeWindow, Integer> fillData = randomFillNumberData(100, 300, 1);
        assertFalse(fillSearch.getFillForNumber(fillData, 80).isPresent());
    }

    @Test
    public void shouldWorkForRightDataRight() {
        NavigableMap<TimeWindow, Integer> fillData = randomFillNumberData(100, 300, 1);
        assertFalse(fillSearch.getFillForNumber(fillData, 320).isPresent());
    }

    @Test
    public void shouldDeduplicateFills() {
        //given - duplicates
        NavigableMap<TimeWindow,Integer> fillData = new TreeMap<>(byStart);
        fillData.put(TimeWindow.between(1,2), 1);
        TimeWindow input = TimeWindow.between(3,4);
        fillData.put(input, 1);
        //when
        TimeWindow timeWindow = fillSearch.getFillForNumber(fillData, 1).orElseThrow();

        //then
        assertEquals(timeWindow, input);
    }

    @Test
    public void shouldWorkForAllData() {
        for (int j = 0; j < 100; j++) {
            NavigableMap<TimeWindow, Integer> fills = randomFillNumberData(110, 290, (new Random()).nextInt(5) + 1);
            for (int i = 0; i < 1000; i++) {
                int fillNumber = i;
                if (fills.values().stream().anyMatch(t -> t == fillNumber)) {
                    assertTrue(fillSearch.getFillForNumber(fills, i).isPresent());
                } else {
                    assertFalse(fillSearch.getFillForNumber(fills, i).isPresent());
                }
            }
        }
    }

    private NavigableMap<TimeWindow, Integer> randomFillNumberData(int minNumber, int maxNumber, int jump) {
        NavigableMap<TimeWindow, Integer> fills = new TreeMap<>(byStart);
        long t1 = 1000;
        for (; minNumber <= maxNumber; minNumber += jump) {
            long d = (new Random()).nextInt(20) + 1;
            fills.put(TimeWindow.between(t1, t1 + d), minNumber);
            t1 += d;
        }
        return fills;
    }
}
